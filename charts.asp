<%
'	#$#Author#$#
'	#$#Date#$#
'	#$#VEPVER#$#
Response.Buffer = TRUE
Response.ContentType = "application/pdf"
%>

<%
 ' aspEasyPDF Object
 set PDF = Server.Createobject("aspPDF.EasyPDF")
 'Between the VEP_BEGIN and VEP_END it will insert the code automatically
 ' VEP_BEGIN_AUTO_CODE                 
 SUB DoPageVEP_1
   PDF.Page "A4", 0
   PDF.SetMargins 10, 10, 10, 10
   PDF.SetFont "F2", 20, "#000000"
   PDF.SetProperty 104, 1
   PDF.AddTextPos 152, 32, "Chart Report in Real Time"
   
   Randomize
   
   ' Ramdom chart
   vr_chart = PDF.AddChart("", "")
   PDF.SetPropObj vr_chart, 800, "#FFFFFF"
   PDF.SetPropObj vr_chart, 801, "#000000"
   PDF.SetPropObj vr_chart, 805, 1
   PDF.SetPropObj vr_chart, 806, "#99CCFF"
   PDF.SetPropObj vr_chart, 807, "#0000FF"
   PDF.SetPropObj vr_chart, 808, 1
   PDF.SetPropObj vr_chart, 820, 1
   PDF.SetPropObj vr_chart, 821, 345
   PDF.SetPropObj vr_chart, 823, 16
   PDF.SetPropObj vr_chart, 824, 350
   PDF.SetPropObj vr_chart, 825, 0
   PDF.SetPropObj vr_chart, 827, 55
   PDF.SetPropObj vr_chart, 826, 91
   PDF.SetPropObj vr_chart, 880, 1
   PDF.SetPropObj vr_chart, 881, "#000080"
   PDF.SetPropObj vr_chart, 882, "#000000"
   PDF.SetPropObj vr_chart, 883, "#FFFFFF"
   PDF.SetPropObj vr_chart, 884, 0
   PDF.SetPropObj vr_chart, 885, 0
   PDF.SetPropObj vr_chart, 886, 0
   PDF.AddChartSeries vr_chart, "", Int((5 * Rnd))
   ' Add some random values ;-)
   for f = 0 to 11
   	PDF.AddChartYValue vr_chart, 0, Int(Rnd() * 10000), MonthName(f+1), ""
   next
   PDF.DrawChartPos vr_chart, 48, 112, 489, 313
   
   
   
   vr_chart = PDF.AddChart("", "")
   PDF.SetPropObj vr_chart, 800, "#FFFFFF"
   PDF.SetPropObj vr_chart, 801, "#000000"
   PDF.SetPropObj vr_chart, 869, 0
   PDF.SetPropObj vr_chart, 820, 1
   PDF.SetPropObj vr_chart, 822, 0
   PDF.SetPropObj vr_chart, 827, 49
   PDF.SetPropObj vr_chart, 826, 91
   PDF.AddChartSeries vr_chart, "", 5
   PDF.AddChartXYValue vr_chart, 0, 0, 25, "aspEasyPDF", ""
   PDF.AddChartXYValue vr_chart, 0, 0, 50, "aspEasyPDF PRO", ""
   PDF.AddChartXYValue vr_chart, 0, 0, 10, "EasyReportPDF", ""
   PDF.AddChartXYValue vr_chart, 0, 0, 15, "VisualEasyPDF", ""
   PDF.DrawChartPos vr_chart, 48, 496, 489, 313
   PDF.SetFont "F2", 15, "#808080"
   PDF.SetProperty 104, 0
   PDF.AddTextWidth 168, 52, 217, "New feature from 3.0 version"
   PDF.SetFont "F1", 10, "#000000"
   PDF.AddTextPos 168, 432, "Random values which can be taken from a dataset"
   PDF.AddTextPos 240, 824, "Not real data ;-)"
   PDF.SetFont "F2", 12.5, "#000000"
   PDF.AddTextPos 224, 98, "Orders from 2004"
   PDF.AddTextPos 240, 482, "Product sales"
 END SUB

 'INFORMACIÓN DE LA LICENCIA PARA LA VERSIÓN REGISTRADA
 PDF.License "C:\WINDOWS\System32\easypdf.lic"
 'INFORMACIÓN DEL DOCUMENTO
 PDF.SetProperty 303, "VisualEasyPDF 3.00"
 Call DoPageVEP_1
 ' VEP_END_AUTO_CODE                              
 ' Now insert your code here before the BinaryWrite
 ' It will not be destroyed
 PDF.BinaryWrite
 set PDF = Nothing
%>