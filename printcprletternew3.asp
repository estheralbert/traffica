<!--#include file="openconn.asp"-->
<!--#include file="common.asp"-->
<%
    server.ScriptTimeout=100000
    batchId =  request("batchid")
    idebug = Request("debug")

    if request("debug")="true" or request("debug")=1 then idebug = 1 else idebug = 0 end if

    Set ws = server.CreateObject("WScript.Network.1")
    strUser = "DSQ8003-12\office"
    strPass = "Creative48"

    on error resume next
    ws.RemoveNetworkDrive "k:",true
    on error goto 0

   ' on error resume next
    ws.MapNetworkDrive "k:", "\\fileserver.ccarpark.nsdsl.net\incomingletters",False,struser,strpass

    Set dicTemplates    = Server.CreateObject("Scripting.Dictionary")

    Set Pdf             = Server.CreateObject("Persits.PDF")
    Set objParam        = Pdf.CreateParam("size=11; alignment=left; color=black; spacing=1.1;")
    Set objParamWhite   = Pdf.CreateParam("size=15; alignment=left; color=white; spacing=1.1;")

    Set objDocfinal     = Pdf.CreateDocument()

    '--- Add Blank Page ---
    'call docLetter.Pages.Add(PageWidth, PageHeight)

    PrintBatch(batchId) '5441

    function PrintBatch(batchId)
        StartTime = Timer()

        isBatch = true

        if batchid <> "" then
	        sqlSelect = "select * from dbo.cprletternew cpr where cpr.batch = " & batchid

	        sql = "insert into dbo.reportsscheduled(sql,emailto,subject) " & _
                    "values('batchSummary @batchId = " & batchid & ",@tablename	= ''cprletternew'''," &_
                    "'Simon.a@bemrosemobile.co.uk,esther@creativecarpark.co.uk,ross@creativecarpark.co.uk'," &_
                    "'CPR Letters new Batch Summary for batch " & batchid & " ' )"
        else
            sqlSelect = "select * from cprletternew where pcn='6307451801'"
            isBatch = false
        end if

        if idebug =  1 then Response.write "<br />" & sqlSelect

        n=1
        objconn.cursorlocation = 3 ''adUseClient
        set  rsB = objconn.execute(sqlSelect)
        do while not rsB.eof
            pcn         = rsB("pcn")
            datesent    = rsB("dateadded")

            if idebug =  1 then Response.Write "<br/>" & n & " - Processing PCN: " & pcn & "<br>"

            fullpath = PrintCurrentLetter(pcn, datesent)

            if idebug =  1 then Response.Write "<br/>------ Finished Processing PCN: " & pcn & "-----------<br/>"
            n = n + 1
            rsB.movenext
        loop

        if isBatch then
            fileName = "cpnewletter_" & batchid & ".pdf"
            objDocfinal.Save  "C:\inetpub\wwwroot\traffica\cprletters\" & fileName
            finalUrl = "cprletters\" & fileName

            if idebug = 1 Then
                Response.write "<hr><a href=""" & finalUrl & """ target=""_blank"">View Complete PDF for <strong>Batch Id: " & batchId & "</strong></a>" 
            Else
                Response.redirect  finalUrl
            End If
        else
            fileName = "cpnewletter_.pdf"
            objDocfinal.Save  "C:\inetpub\wwwroot\traffica\cprletters\" & fileName
            finalUrl = "cprletters\" & fileName

            'relPath = replace(mid(fullpath,3),"\","/")

            Response.write "<hr><a href=""" & finalUrl & """ target=""_blank"">View Complete PDF for <strong>PCN: " & pcn & "</strong></a>" 
        end if
        EndTime = Timer()
        if idebug=1 then  Response.Write("<hr>Total Time taken in Seconds to 2 decimal places: " & FormatNumber(EndTime - StartTime, 2))
    
    End Function

    Function PrintCurrentLetter(pcn,datesent)

	    sql = "exec getCprLetterDetails @pcn=" & tosql(pcn,"text")
	    if idebug = 1 Then Response.Write sql & "<br>"
	    openrs rs,sql

	    owner           = rs("owner")
	    address1        = rs("address1")
	    address2        = rs("address2")
	    address3        = rs("address3")
	    town            = rs("town")
	    postcode        = rs("postcode")
	    ticketprice     = rs("ticketprice")
        registration    = rs("registration")
        dateOfViolation = rs("date of violation")
        ticketprice     = FormatNumber(rs("ticketprice"),2)
        siteName        = getsitename(rs("site"))

        longDate    = Day(datesent) & " " & MonthName(Month(datesent)) & " " & Year(datesent) 

        Set objDoc          = Pdf.OpenDocument("C:\inetpub\wwwroot\traffica\LBAv5.pdf")  
	    Set objFont         = objDoc.Fonts("Times-Roman")
	    Set objFontBold     = objDoc.Fonts("Times-Bold")

        Response.Write "<br>Pages: " & objDoc.Pages.Count

        'set objTemplate1 = objDoc.CreateGraphicsFromPage( docLetter, 1 )
        'set objTemplate2 = objDoc.CreateGraphicsFromPage( docLetter, 2 )
        
        '--- Page 1 -----
        Set objPage         = objDoc.Pages(1)
        Set objCanvas       = objPage.Canvas

        PageWidth   = objPage.Width
        PageHeight  = objPage.Height

        If idebug = 1 Then
            Response.Write "<br>PageWidth: " & PageWidth
            Response.Write "<br>PageHeight: " & PageHeight
            Set objRect = objPage.CropBox
            Response.Write "<br>objRect: " & objRect.Left & " " & objRect.Bottom & " " & objRect.Right & " " & objRect.Top 
        End If

        'objCanvas.DrawGraphics objTemplate1, "x=0; y=0"

        If idebug = 1 Then
            Response.Write "<br>pcn: " & pcn
        End If

        '---- Keep needed by printer ----
        objCanvas.DrawText   pcn, "x=5, y=820; size=15; alignment=left; color=white", objDoc.Fonts("Arial") 
        '---------------------------------

	    owner           = rs("owner")
	    address1        = rs("address1")
	    address2        = rs("address2")
	    address3        = rs("address3")
	    town            = rs("town")
	    postcode        = rs("postcode")
	    ticketprice     = rs("ticketprice")
        registration    = rs("registration")
        dateOfViolation = rs("date of violation")
        ticketprice     = FormatNumber(rs("ticketprice"),2)
        siteName        = getsitename(rs("site"))

        longDate    = Day(datesent) & " " & MonthName(Month(datesent)) & " " & Year(datesent)

	    Dim ownerAddress(5)
	    ownerAddress(0) = owner
	    ownerAddress(1) = address1
	    ownerAddress(2) = address2
	    ownerAddress(3) = address3
	    ownerAddress(4) = town
	    ownerAddress(5) = postcode

	    ownerNameAddress = ""

	    For Each rowVal In ownerAddress

		    If rowVal <> "" Then
			    ownerNameAddress = ownerNameAddress & rowVal & vbCrLf
		    End If

	    Next

        Call drawMultilineText(ownerNameAddress, 73, 670, 360, objParam, objCanvas, objFont, 1)

        '400,700
        Dim arrX
        Dim arrText
        arrText = Array( _
                registration, _
                pcn, _
                dateOfViolation,_
                "�" & ticketprice, _
                "Car Park at " & siteName, _
                "Parking in breach of Terms and Conditions on Notice"  )

       ' Go over all items in arrays
        objParam("x") = 210
        objParam("y") = 545
        For i = 0 to UBound(arrText)
            Response.Write "<br>" & i & " - " & arrText(i) 
            if i = 5 Then lineHeight = 26 else lineHeight = 13.5
            objParam("y") = objParam("y") - lineHeight

            ' Draw text on canvas
            objCanvas.DrawText arrText(i), objParam, objFontBold
        Next ' i

        objParam("x") = 120
        objParam("y") = 428
        objCanvas.DrawText owner & ",", objParam, objFont

        objParam("x") = 165
        objParam("y") = 631
        objParam("Alignment") = 1
        objCanvas.DrawText longDate, objParam, objFont

        objParam("Alignment") = 0

        '--- Add New Page At End ---
        Set objPage         = objDoc.Pages.Add(pageWidth, pageHeight)

        '--- save files 
        newpath     = "k:\cprlettersnew\" & year(datesent) 
        checkdirectory newpath

        path        = newpath & "\cpcn" & pcn & ".pdf"

        if idebug = 1 then Response.Write "<br>Saving to memory"
        Set objDocBinary = Pdf.OpenDocumentBinary(objDoc.SaveToMemory )

        if idebug = 1 then Response.Write "<br>Appending to final"
        objDocfinal.AppendDocument objDocBinary  

        if idebug = 1 then Response.Write "<br>Saving to file"
        FileName = objDocBinary.Save(path, true)

        Set objPage = Nothing
        Set objDoc = Nothing

        if idebug =  1 then Response.write "<br>filename: " & filename

        PrintCurrentLetter = path    
    End Function
        
%>