  <% response.buffer=true
Server.ScriptTimeout=200000
pStr = "private, no-cache, must-revalidate" 


       if session("admin")<>"ok" then response.redirect "admin.asp"
%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src=https://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js></script>
<%
    if request("s")="" then
    %>
<script type="text/javascript">
    function validateForm() {
        if (document.getElementById('txtReason').value == "") {
            alert("Please enter a reason.");
            return false;
        }
        else if (document.getElementById('txtAuthby').value == "") {
            alert("Please enter Auth By.");
            return false
        }
        else if (document.getElementById('txtPCN').value == "") {
            alert("Please enter PCN's.");
            return false
        }
        else { return true;}
    }

    function checkrequest(data) {
        $('.datashow').hide();
        $('#reason' + data).show();
        $('#reqReason').val(data);
    }


    $(document).ready(function () {
        $('input[type=radio][name=reason]').on('change', function () {
            $('textarea[data-show=issuederrorother],textarea[data-show=goodwillother]').hide().val('');
            if ($(this).data('show') == 'goodwillother') {
                $('textarea[data-show=goodwillother]').show();
            } else if ($(this).data('show') == 'issuederrorother') {
                $('textarea[data-show=issuederrorother]').show();
            }
        });
    });
</script>
<style type="text/css">
    .frm {width:300px; margin:10px auto;}
    .frm .lbl {display:inline-block;min-width:100px;max-width:100px;}
    .frm input {margin:10px 0px;}
</style>
<form method="post" action="cancelpcns.asp" class="frm">    
    <div>
        <span class="lbl">Reason:</span><br />
        <input type=radio name=requestedby value="Admin error" onClick="checkrequest('admin_error');">Admin error <br>
        <input type=radio name=requestedby value="Not at the address" onClick="checkrequest('notaddress');">Not at the address<br>
        <input type=radio name=requestedby value="NON POFA" onClick="checkrequest('nonpofa');">NON POFA<br>
        <input type=radio name=requestedby value="Entered incorrect VRM" onClick="checkrequest('paidvrm');">Entered incorrect VRM<br>
        <input type=radio name=requestedby value="OTHER" onClick="checkrequest('other');">OTHER<br>
        <input type=radio name=requestedby value="Goodwill" onClick="checkrequest('goodwill');">Goodwill <br>
        <input type=radio name=requestedby value="Issued in error" onClick="checkrequest('issuederror');">Issued in error <br>
        <input type=radio name=requestedby value="Legal" onClick="checkrequest('legal');">Legal<br>
        <input type="hidden" id="reqReason" />
     </div>
       
    <div id=reasonother style="display:none;" class="datashow">
        <hr /> 
        <span class="lbl">Reason Detail:</span><br />
        <input type=radio name=reason value="Reissued in error / too late"> Reissued in error / too late <br>
        <input type=radio name=reason value="After 14 days">After 14 days<br>
        <input type=radio name=reason value="Cloned VRM">Cloned VRM<br />  
        <input type=radio name=reason value="DVLA/POLICE confirmation">DVLA/POLICE confirmation<br />  
        <input type=radio name=reason value="Insolvency  / Deceased / Bankrupt">Insolvency  / Deceased / Bankrupt<br />  
        <input type=radio name=reason value="International driver">International driver<br />  
        <input type=radio name=reason value="No Keeper Details">No Keeper Details<br />
        <input type=radio name=reason value="Poor quality photos">Poor quality photos<br />
        <input type=radio name=reason value="POPLA lost">POPLA lost<br />
        <input type=radio name=reason value="Unmarked vehicle (Police / Ambulance / Cash in transit)">Unmarked vehicle (Police / Ambulance/ Cash in Transit)<br /> 
    </div>
    <div id="reasongoodwill" style="display:none;" class="datashow">
        <hr /> 
        <span class="lbl">Reason Detail:</span><br />
        <input type=radio name=reason value="Goodwill - client">Goodwill - client<br>
        <input type=radio name=reason value="Goodwill - driver">Goodwill - driver<br>
        <input type=radio name=reason value="Breakdown">Breakdown<br />  
        <input type=radio name=reason value="Customer (with receipt)">Customer (with receipt)<br />  
        <input type=radio name=reason value="Disabled">Disabled<br /> 
        <input type=radio name=reason value="BPA">BPA<br>
        <input type=radio name=reason value="Health">Health<br>
        <input type=radio name=reason value="Hospital appointment letter">Hospital appointment letter<br />  
        <input type=radio name=reason value="Other" data-show="goodwillother">Other<br />  
        <input type=radio name=reason value="NHS Staff - Provided copy of voucher/permit">NHS Staff - Provided copy of voucher/permit<br />
        <input type=radio name=reason value="Leeway <12 minutes">  Leeway <12 minutes<br />
        <input type=radio name=reason value="Paid for full stay, after 10 min">Paid for full stay, after 10 min<br />
        <input type=radio name=reason value="COVID-19 NHS Staff">COVID-19 NHS Staff<br />
        <input type=radio name=reason value="COVID-19 Non NHS">COVID-19 Non NHS<br />
        <textarea name=reasonother style="display: none;"  data-show="goodwillother" placeholder="Reason"></textarea>         
    </div>
    <div id="reasonissuederror" style="display:none;" class="datashow">
        <hr /> 
        <span class="lbl">Reason Detail:</span><br />
        <input type=radio name=reason value="Double Entry">Double Entry<br>
        <input type=radio name=reason value="Exemption not uploaded">Exemption not uploaded<br>
        <input type=radio name=reason value="Issued outside of enforcement hours">Issued outside of enforcement hours<br />  
        <input type=radio name=reason value="Issued prior to live date">Issued prior to live date<br />  
        <input type=radio name=reason value="Misread">Misread<br /> 
        <input type=radio name=reason value="Not added to Permit">Not added to Permit<br>
        <input type=radio name=reason value="Paid for parking">Paid for parking<br>
        <input type=radio name=reason value="Phone and Pay error">Phone and Pay error<br />  
        <input type=radio name=reason value="PM down">PM down<br />  
        <input type=radio name=reason value="Stolen tablet">Stolen tablet<br />            
        <input type=radio name=reason value="Other" data-show="issuederrorother">Other<br /> 
        <textarea name=reasonother style="display: none;" data-show="issuederrorother" placeholder="Reason"></textarea>
    </div>
    <div id="reasonlegal" style="display:none;" class="datashow">
        <hr /> 
        <span class="lbl">Reason Detail:</span><br />
        <input type=radio name=reason value="Claim stayed">Claim stayed<br>
        <input type=radio name=reason value="Discontinued">Discontinued<br>
        <input type=radio name=reason value="Dismissed">Dismissed<br />  
        <input type=radio name=reason value="Struck out">Struck out<br />         
    </div>
    <span class="lbl">Auth By:</span><input type="text" name="authby" id="txtAuthby"/><br />
    <span class="lbl">Create Letters</span><input type="checkbox" value="1" name="createletters" /><br />   <span class="lbl">Add Apology</span><input type="checkbox" value="1" name="addapology" /><br />

    Enter in  pcn's one per line<br /><br />
    <textarea name="s" cols="100" rows="50" id="txtPCN"></textarea><br />
    <input type="submit" name="submit" value="Cancel" onclick="return validateForm();"/>

</form>

<%

    else
    createletters=request("createletters")
    if isnull(createletters) or createletters="" then createletters=0

    s=request("s")
    SQL="exec cancelpcns @s='<keys>"
      asa=split(s,vbcrlf)
        for i=lbound(asa) to ubound(asa)
        sql=sql &  "<key>" & asa(i) & "</key>"
        next
        sql=sql & "</keys>',@reason=" & tosql(request("reason"),"text") & ",@authby=" & tosql(request("authby"),"text")  & ",@requestedby=" & tosql(request("requestedby"),"text")

  'response.write sql
   ' response.end
 
    openrs rst,sql

    response.write "The following PCN''s were cancelled already:<br>"
    do  while not rst.eof
        response.write rst("pcn") & "<br>"
    rst.movenext
    loop
    response.write "<hr>"


    Set rst = rst.NextRecordset()


if createletters=1 then
        response.write "pcn:" & rst("pcn")
    batchid=0
    sqlcl="select max(batchid) as batchid from cancelledletters"
    openrs rscl,sqlcl
  batchid=rscl("batchid")
    batchid=batchid +1 
    closers rscl
    do while not rst.eof
    call createcancelled(rst("pcn"),batchid)
    rst.movenext
    loop
    closers rst


    response.redirect "printletterbatch.asp?batchid=" & batchid 

    else
    response.write "PCN's Cancelled"


    end if




    end if




    
      sub createcancelled(spcn,batchid)

    sqlv="select * from violators where pcn+site="  & tosql(spcn,"text")
    openrs rsv,sqlv
    id=rsv("id")
   myaddress=getaddress(id)
           


myletter=""
			
myletter=myletter & "<br><br><br><br><p>" & myaddress & "<br></p>"
myletter=myletter &   "<P><img src=""http://traffica2.cleartoneholdings.co.uk/images/spacer.gif"" alt=""spacer"" width=""590"" height=""10"">" & date()+1 & "</p>"                 
myletter=myletter & "<p>" & getpcndetails(id)  & "<br></p>"	
myletter=myletter & "<p align=center><b>CONFIRMATION OF CANCELLATION</b></p>" 
myletter=myletter & "<p align=justify>We refer to recent communications with this office in respect of the Parking Contravention Enforcement Notice.<BR><br>We now confirm that this Notice has been cancelled."
    
    if request("addapology")=1 then myletter=myletter & "<br><br>Please accept our apologies for any inconvenience caused."
    myletter=myletter & "<br><br>Yours faithfully,<br><br> <b><u>Representations Team</u></b><br><br>"
    
    closers rsv

    sql="insert into cancelledletters(violatorid,letter,date,batchid) values (" & id & ",'" & Replace(myletter, "'", "''") &  "',getdate()," & batchid & ")"
    response.write sql
    objconn.execute sql
    end sub

%>