<!--#include file="openconntriangle.asp"-->

<!--#include file="common.asp"-->
<%
server.ScriptTimeout=1000000000



strDatabaseUser="protxUser" '** Change this if you created a different user name to access the database **
strDatabasePassword="[your database user password]" '** Set the password for the above user here **

strVirtualDir="VSPDirect-kit" '** Change if you've created a Virtual Directory in IIS with a different name **

	'** IMPORTANT.  Set the strYourSiteFQDN value to the Fully Qualified Domain Name of your server. **
	'** This should start http:// or https:// and should be the name by which our servers can call back to yours **
	'** i.e. it MUST be resolvable externally, and have access granted to the Protx servers **
	'** examples would be https://www.mysite.com or http://212.111.32.22/ **
	'** NOTE: You should leave the final / in place. **
strYourSiteFQDN="http://[your web site]/"  

 '** Set this value to the VSPVendorName assigned to you by protx or chosen when you applied **
strCurrency="GBP" '** Set this to indicate the currency in which you wish to trade. You will need a merchant number in this currency **
strTransactionType="PAYMENT" '** This can be DEFERRED or AUTHENTICATE if your Protx account supports those payment types **

'**************************************************************************************************
' Global Definitions for this site
'**************************************************************************************************
strProtxDSN = "DRIVER={MySQL ODBC 3.51 Driver}; SERVER=localhost; DATABASE=protx; " & _
	"UID=" & strDatabaseUser & ";PASSWORD=" & strDatabasePassword & "; OPTION=3" 

strProtocol="2.22"

if strConnectTo="LIVE" then
  strAbortURL="https://ukvps.protx.com/vspgateway/service/abort.vsp"
       strAuthoriseURL="https://live.sagepay.com/gateway/service/authorise.vsp"
  strCancelURL="https://ukvps.protx.com/vspgateway/service/cancel.vsp"
  strPurchaseURL="https://live.sagepay.com/gateway/service/vspdirect-register.vsp"
  strRefundURL="https://ukvps.protx.com/vspgateway/service/refund.vsp"
  strReleaseURL="https://ukvps.protx.com/vspgateway/service/release.vsp"
  strRepeatURL="https://ukvps.protx.com/vspgateway/service/repeat.vsp"
  strVoidURL="https://ukvps.protx.com/vspgateway/service/void.vsp"
  strVSPDirect3DCallbackPage="https://ukvps.protx.com/vspgateway/service/direct3dcallback.vsp"
elseif strConnectTo="TEST" then
  strAbortURL="https://ukvpstest.protx.com/vspgateway/service/abort.vsp"
  strAuthoriseURL="https://ukvpstest.protx.com/vspgateway/service/authorise.vsp"
  strCancelURL="https://ukvpstest.protx.com/vspgateway/service/cancel.vsp"
  strPurchaseURL="https://ukvpstest.protx.com/vspgateway/service/vspdirect-register.vsp"
  strRefundURL="https://ukvpstest.protx.com/vspgateway/service/refund.vsp"
  strReleaseURL="https://ukvpstest.protx.com/vspgateway/service/abort.vsp"
  strRepeatURL="https://ukvpstest.protx.com/vspgateway/service/repeat.vsp"
  strVoidURL="https://ukvpstest.protx.com/vspgateway/service/void.vsp"
  strVSPDirect3DCallbackPage="https://ukvpstest.protx.com/vspgateway/service/direct3dcallback.vsp"
else
  strAbortURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorAbortTx"
  strAuthoriseURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorAuthoriseTx"
  strCancelURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorCancelTx"
  strPurchaseURL="https://ukvpstest.protx.com/VSPSimulator/VSPDirectGateway.asp"
  strRefundURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorRefundTx"
  strReleaseURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorReleaseTx"
  strRepeatURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorRepeatTx"
  strVoidURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorVoidTx"
  strVSPDirect3DCallbackPage="https://ukvpstest.protx.com/VSPSimulator/VSPDirectCallback.asp"
end if

'**************************************************************************************************
' Useful functions for all pages in this kit
'**************************************************************************************************

'** Filters unwanted characters out of an input string.  Useful for tidying up FORM field inputs
public function cleanInput(strRawText,strType)

	if strType="Number" then
		strClean="0123456789."
		bolHighOrder=false
	elseif strType="VendorTxCode" then
		strClean="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_."
		bolHighOrder=false
	else
  		strClean=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,'/{}@():?-_&�$=%~<>*+""" & vbCRLF
		bolHighOrder=true
	end if

	strCleanedText=""
	iCharPos = 1

	do while iCharPos<=len(strRawText)
    	'** Only include valid characters **
		chrThisChar=mid(strRawText,iCharPos,1)

		if instr(StrClean,chrThisChar)<>0 then 
			strCleanedText=strCleanedText & chrThisChar
		elseif bolHighOrder then
			'** Fix to allow accented characters and most high order bit chars which are harmless **
			if asc(chrThisChar)>=191 then strCleanedText=strCleanedText & chrThisChar
		end if

      	iCharPos=iCharPos+1
	loop       
  
	cleanInput = trim(strCleanedText)

end function

'** Doubles up single quotes to stop breakouts from SQL strings **
public function SQLSafe(strRawText)

	strCleanedText=""
	iCharPos = 1

	do while iCharPos<=len(strRawText)
    	'** Double up single quotes, but only if they aren't already doubled **
		if mid(strRawText,iCharPos,1)="'" then 
			strCleanedText=strCleanedText & "''"
			if iCharPos<>len(strRawText) then
				if mid(strRawText,iCharPos+1,1)="'" then iCharPos=iCharPos+1
			end if
		else
			strCleanedText=strCleanedText & mid(strRawText,iCharPos,1)
		end if
        
		iCharPos=iCharPos+1
	loop       
  
	SQLSafe = trim(strCleanedText)

end function

'** Counts the number of : in a string.  Used to validate the basket fields
public function countColons(strSource)

	if strSource="" then
		iNumCol=0
	else
		iCharPos=1
		iNumCol=0
		do while iCharPos<>0
			iCharPos=instr(iCharPos+1,strSource,":")
			if iCharPos<>0 then iNumCol=iNumCol+1
		loop 
	
	end if
	
	countColons = iNumCol

end function

'** Checks to ensure a Basket Field is correctly formatted **
public function validateBasket(strThisBasket)

	bolValid=false
	if len(strThisBasket)>0 and (instr(strThisBasket,":")<>0) then
		iRows=left(strThisBasket,instr(strThisBasket,":")-1)
		if isnumeric(iRows) then
			iRows=Cint(iRows)
			if countColons(strThisBasket)=((iRows*5)+iRows) then bolValid=true
		end if
	end if	
	validateBasket = bolValid

end function


'** ASP has no inbuild URLDecode function, so here's on in case we need it **
public function URLDecode(strString)
	For lngPos = 1 To Len(strString)
    	If Mid(strString, lngPos, 1) = "%" Then
            strUrlDecode = strUrlDecode & Chr("&H" & Mid(strString, lngPos + 1, 2))
            lngPos = lngPos + 2
        elseif Mid(strString, lngPos, 1) = "+" Then
            strUrlDecode = strUrlDecode & " "
        Else
            strUrlDecode = strUrlDecode & Mid(strString, lngPos, 1)
        End If
    Next
    UrlDecode = strUrlDecode
End Function

'** There is a URLEncode function, but wrap it up so keep the code clean **
public function URLEncode(strString)
	strEncoded=Server.URLEncode(strString)
	URLEncode=strEncoded
end function

'** MySQL can't handle ASP format dates.  It needs values separated by - signed, so create a MySQL valid date from that passed **
public function mySQLDate(dateASP)
	mySQLDate=DatePart("yyyy",dateASP) & "-" & right("00" & DatePart("m",dateASP),2) & "-" &_
		right("00" & DatePart("d",dateASP),2) & " " & right("00" & DatePart("h",dateASP),2) & ":" &_
		right("00" & DatePart("n",dateASP),2) & ":" & right("00" & DatePart("s",dateASP),2)
end function

'** Used to split out the fields returned from the VSP systems in the response part of the POSTs **
function findField( strFieldName, strThisResponse )
	arrItems = split( strThisResponse, vbCRLF )
	for iItem = LBound( arrItems ) to UBound( arrItems )
		if InStr(arrItems(iItem), strFieldName & "=" ) = 1 then
			findField = mid(arrItems(iItem),len(strFieldName)+2)
			exit For
		end if
	next 
end function

'************ Global variables and Database Connections ***************

dim dbProtx
dim rsPrimary
dim rsSecondary
dim strSQL

'Open the VPS database
if strDatabasePassword<>"[your database user password]" then
	set dbProtx=server.createobject("adodb.connection") 
	dbProtx.CommandTimeOut=180
	dbProtx.Open strProtxDSN
end if

'sqlp="select * from trianglepayment where status is null and datetime>'20101001' order by datetime"
sqlp="select * from trianglepayment where datetime>getdate()-21 and status<>'successful' and c is not null order by datetime"
 set rsck=objconnt.execute(sqlp)
do while not rsck.eof 
               
                    id=rsck("id")
                    amount=rsck("amount")
                     paymentreference=rsck("paymentreference")
                   c=rsck("c")
                   ce=rsck("ce")
                   
                   telephone=rsck("telephone")
                  
                   ''charge card here 
                   takepayment=1
                   if takepayment=1 then
                    strVSPVendorName="redfunnel"
                       strConnectTo="LIVE" 
                        strPurchaseURL="https://live.sagepay.com/gateway/service/vspdirect-register.vsp"
                    charge=amount
                    
                    cc=c
                    
                    cardexp=ce
                  
                     cardtype=getcardtype(cc)
                     if nameoncard="" or isnull(nameoncard) then nameoncard="Unknown"
                     strCardHolder=left(nameoncard,100)
		                                    strCardType=cleaninput(cardtype,"Text")	
		                                    strCardNumber=cleaninput(cc,"Number")
                    		          
		                                   ' strStartDate=cleaninput(request.form("StartDate"),"Number")
		                                   ' response.Write "<br>start  date:" & strstartdate
		                                    strExpiryDate=cleaninput(cardexp,"Number")
		                                    strIssueNumber=cleaninput(issuenumber,"Number")
		                                    strCV2=cleaninput(cvv,"Number")
                                    		
                    		            
			                                    '** All required field are present, so first store the order in the database then format the POST to VSP Direct **
			                                    '** First we need to generate a unique VendorTxCode for this transaction **
			                                    '** We're using VendorName, time stamp and a random element.  You can use different mehtods if you wish **
			                                    '** but the VendorTxCode MUST be unique for each transaction you send to VSP Server **
			                                    Randomize
			                                
                    			                strvendortxcode=strvspvendorname & "TP_" & telephone & "_" &   paymentreference
			                                    sngTotal=charge
                    			               
                    			               
			                                    '** Now create the VSP Direct POST **
                                    				
			                                    '** Now to build the VSP Server POST.  For more details see the VSP Server Protocol 2.22 **
			                                    '** NB: Fields potentially containing non ASCII characters are URLEncoded when included in the POST **
			                                    strPost="VPSProtocol=" & strProtocol
			                                    strPost=strPost & "&TxType=" & strTransactionType '** PAYMENT by default.  You can change this in the includes file **
			                                    strPost=strPost & "&Vendor=" & strVSPVendorName
			                                    strPost=strPost & "&VendorTxCode=" & strVendorTxCode '** As generated above **
			                                    strPost=strPost & "&Amount=" & FormatNumber(sngTotal,2,-1,0,0) '** Formatted to 2 decimal places with leading digit but no commas or currency symbols **
			                                    strPost=strPost & "&Currency=" & strCurrency
			                                    '** Up to 100 chars of free format description **
			                                    strPost=strPost & "&Description=" & URLEncode("Authentication request " & strVSPVendorName)
			                                    '** The Notification URL is the page to which VSP Server calls back when a transaction completes **
			                                    '** You can change this for each transaction, perhaps passing a session ID or state flag if you wish **
			                                    strPost=strPost & "&CardHolder=" & strCardHolder
			                                    strPost=strPost & "&CardNumber=" & strCardNumber
			                                    if len(strStartDate)>0 then
				                                    strPost=strPost & "&StartDate=" & strStartDate
			                                    end if
			                                    strPost=strPost & "&ExpiryDate=" & strExpiryDate
			                                    if len(strIssueNumber)>0 then
				                                    strPost=strPost & "&IssueNumber=" & strIssueNumber
			                                    end if
			                                    strPost=strPost & "&CV2=" & strCV2
			                                    strPost=strPost & "&CardType=" & strCardType
			                                    strPost=strPost & "&BillingAddress=" & URLEncode(strBillingAddress)
			                                    strPost=strPost & "&BillingPostCode=" & URLEncode(strBillingPostCode)
			                                    if bolDeliverySame then
				                                    strPost=strPost & "&DeliveryAddress=" & URLEncode(strBillingAddress)
				                                    strPost=strPost & "&DeliveryPostCode=" & URLEncode(strBillingPostCode)
			                                    else
				                                    strPost=strPost & "&DeliveryAddress=" & URLEncode(strDeliveryAddress)
				                                    strPost=strPost & "&DeliveryPostCode=" & URLEncode(strDeliveryPostCode)
			                                    end if
			                                    strPost=strPost & "&CustomerName=" & server.URLEncode(strCustomerName)
                                         '** Allow fine control over AVS/CV2 checks and rules by changing this value. 0 is Default **
			                                    '** It can be changed dynamically, per transaction, if you wish.  See the VSP Direct Protocol document **
			                                    if strTransactionType<>"AUTHENTICATE" then strPost=strPost & "&ApplyAVSCV2=0"
                                    			
			                                    '** Send the IP address of the person entering the card details **
			                                    strPost=strPost & "&ClientIPAddress=" & Request.ServerVariables("REMOTE_HOST")

			                                    '** Allow fine control over 3D-Secure checks and rules by changing this value. 0 is Default **
			                                    '** It can be changed dynamically, per transaction, if you wish.  See the VSP Server Protocol document **
			                                    strPost=strPost & "&Apply3DSecure=0"
                                    			
			                                    '** Send the account type to be used for this transaction.  Web sites should us E for e-commerce **
			                                    '** If you are developing back-office applications for Mail Order/Telephone order, use M **
			                                    '** If your back office application is a subscription system with recurring transactions, use C **
			                                    '** Your Protx account MUST be set up for the account type you choose.  If in doubt, use E **
			                                    strPost=strPost & "&AccountType=E"

			                                    '** The full transaction registration POST has now been built **
			                                    '** Use the Windows WinHTTP object to POST the data directly from this server to Protx **
			                                    '** Data is posted to strPurchaseURL which is set depending on whether you are using SIMULATOR, TEST or LIVE **
			                                    set httpRequest = CreateObject("WinHttp.WinHttprequest.5.1")
                		                    response.Write "<hr>" & strpost & "<hr>"
			                                   ' on error resume next
			                                    httpRequest.Open "POST", CStr(strPurchaseURL), false
			                                    httpRequest.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
			                                    httpRequest.send strPost
			                                    strResponse = httpRequest.responseText
                                    			
			                                    if Err.number <> 0 then    
				                                    valid=false
                                    			  
			                                    else
				                                    '** No transport level errors, so the message got the Protx **
				                                    '** Analyse the response from VSP Direct to check that everything is okay **
				                                    '** Registration results come back in the Status and StatusDetail fields **
				                                    strStatus=findField("Status",strResponse)
				                                    strStatusDetail=findField("StatusDetail",strResponse)
                		                            response.Write strresponse
                		                            fullccstring=strresponse
				                                    if strStatus="3DAUTH" then
					                                    '** This is a 3D-Secure transaction, so we need to redirect the customer to their bank **
					                                    '** for authentication.  First get the pertinent information from the response **
					                                    strMD=findField("MD",strResponse)
					                                    strACSURL=findField("ACSURL",strResponse)
					                                    strPAReq=findField("PAReq",strResponse)
					                                    strPageState="3DRedirect"

				                                    else

					                                    '** If this isn't 3D-Auth, then this is an authorisation result (either successful or otherwise) **
					                                    '** Get the results form the POST if they are there **
					                                    strVPSTxId=findField("VPSTxId",strResponse)
					                                    strSecurityKey=findField("SecurityKey",strResponse)
					                                    strTxAuthNo=findField("TxAuthNo",strResponse)
					                                    strAVSCV2=findField("AVSCV2",strResponse)
					                                    strAddressResult=findField("AddressResult",strResponse)
					                                    strPostCodeResult=findField("PostCodeResult",strResponse)
					                                    strCV2Result=findField("CV2Result",strResponse)
					                                    str3DSecureStatus=findField("3DSecureStatus",strResponse)
					                                    strCAVV=findField("CAVV",strResponse)
                                    					
					                                    '** Great, the signatures DO match, so we can update the database and redirect the user appropriately **
					                                    if strStatus="OK" then
						                                    strDBStatus="AUTHORISED - The transaction was successfully authorised with the bank."
					                                    elseif strStatus="MALFORMED" then
						                                    strDBStatus="MALFORMED - The StatusDetail was:" & SQLSafe(left(strStatusDetail,255))
					                                    elseif strStatus="INVALID" then
						                                    strDBStatus="INVALID - The StatusDetail was:" & SQLSafe(left(strStatusDetail,255))
					                                    elseif strStatus="NOTAUTHED" then
						                                    strDBStatus="DECLINED - The transaction was not authorised by the bank."
					                                    elseif strStatus="REJECTED" then
						                                    strDBStatus="REJECTED - The transaction was failed by your 3D-Secure or AVS/CV2 rule-bases."
					                                    elseif strStatus="AUTHENTICATED" then
						                                    strDBStatus="AUTHENTICATED - The transaction was successfully 3D-Secure Authenticated and can now be Authorised."
					                                    elseif strStatus="REGISTERED" then
						                                    strDBStatus="REGISTERED - The transaction was could not be 3D-Secure Authenticated, but has been registered to be Authorised."
					                                    elseif strStatus="ERROR" then
						                                    strDBStatus="ERROR - There was an error during the payment process.  The error details are: " & SQLSafe(strStatusDetail)
					                                    else
						                                    strDBStatus="UNKNOWN - An unknown status was returned from Protx.  The Status was: " & SQLSafe(strStatus) &_
									                                     ", with StatusDetail:" & SQLSafe(strStatusDetail)
					                                    end if					
                                               

					                                    '** Work out where to send the customer **
					                                    Session("VendorTxCode")=strVendorTxCode
					                                    if strStatus="OK" or strStatus="AUTHENTICATED" or strStatus="REGISTERED" then
						                                    'strCompletionURL="orderSuccessful.asp"
						                                  '  response.Write strstatus & " we need to add payment record here"	
						                           if err.number<>0 then 
						                           '     response.Write err.number & err.Description
						                           else     
						                                 valid=true
                    						             
                            						              
						                            end if      
                    					               
                    						              
                                    		
                    			             
			                                    set httpRequest=nothing
                	                    	end if
                	                    	end if 
                	                    	end if
                   
                   if valid=true then
                     status="successful"
                  else
                    status="Declined" & strdbstatus
                   end if 
                  end if
                  
                  sql="update trianglepayment set vendortxcode=" & tosql(strvendortxcode,"text") & ",status=" & tosql(status,"text") & " where id=" & rsck("id")
                  
                  '  sql="insert into trianglepayment(vendortxcode,c,ce,datetime,startdate,paymentreference,amount,type,status,duration,period,telephone,method,sitecode,test) values(" & tosql(strvendortxcode,"text")  & "," & tosql(c,"text") & "," & tosql(ce,"text") & ",'"
                   ' sql=sql & cisodate(mydate) & " " & mytime & "','" & cisodate(mydate) & " " & mytime & "'," & tosql(paymentreference,"text") & "," & tosql(amount,"Number") & "," & tosql(mytype,"text") & "," & tosql(status,"text") & "," & tosql(duration,"Number") & "," & tosql(period,"text") & "," &  tosql(telephone,"text") & ",'phone','" & sitecode & "','from checkaccesstoupdatesqlxx')"
                   response.write i & ":" & sql & "<br>"
                   i=i+1
                    objconnt.execute sql
               
               
                	response.flush	
           rsck.movenext
           loop     	
           

sub openrstr(rs, sql)
on error resume next
  Set rs = Server.CreateObject("ADODB.Recordset")
   'on error resume next
  rs.Open sql, objConnt,,,adCmdText
 
 
  call myerror
end sub


function getperiod(mydate)
getperiod="offpeak"
sqlgp="SELECT *  fROM peakcalendar WHERE '" & cisodate(mydate)& "' BETWEEN datefrom AND dateto"
'response.write "<!--" & sqlgp & "-->"
openrstr rsgp,sqlgp
if not rsgp.eof and not rsgp.bof then getperiod="peak"
closers rsgp
end function
function c2(mystring)
if len(mystring)<2 then
    mystring="0" & mystring
 end if

end function
function getduration(period,amount)
sqld="select duration from triangletariff where amount=" & tosql(amount,"Number") & " and period=" & tosql(period,"text")
response.Write sqld
openrstr rsd,sqld
if rsd.eof and rsd.bof then
getduration=""
message="amount of " & amount & " for period:" & period & " was posted but doesn't exist"
response.Write message
call SendEmail ( "webmaster@creativecarpark.co.uk", "ec@awebforyou.com", "Triangle Paymentx", message & sqld ) 
call smssimon(message)
else
getduration=rsd("duration")
message= "found duration"
end if

'call SendEmail ( "webmaster@creativecarpark.co.uk", "ec@awebforyou.com", "Triangle Paymentx", message & sqld ) 

closers rsd

end function
function getcardtype(cc)



if left(cc,1)="4" then getcardtype="VISA"
if left(cc,2)="37" then getcardtype="AMEX"
if left(cc,1)="5" then getcardtype="MC"
if left(cc,1)="6" then getcardtype="DISCOVER"
if left(cc,4)="6759" then getcardtype="MAESTRO"

end function
    if err.number<>0 then 
postto="http://clients.creativecarpark.co.uk/errors.asp?errortype=system&errordescription=error updating triangle from access&codeerrordescription=" & err.number& "-" &  err.Description
 set xmlhttp = CreateObject("MSXML2.ServerXMLHTTP") 
    xmlhttp.open "GET", postto, false 
    xmlhttp.send
    
    set xmlhttp = nothing 

end if

%>