<% Server.ScriptTimeout = 3600000 %>
<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%
 pStr = "private, no-cache, must-revalidate" 
 Response.ExpiresAbsolute = #2000-01-01# 
 Response.AddHeader "pragma", "no-cache" 
 Response.AddHeader "cache-control", pStr 
%>
<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="image.asp"-->
<html>
<head>
<LINK href="adminstylesheet.css" rel=stylesheet type=text/css>
<script language="JavaScript" src="sorttable.js"></script>
<!-- calendar stylesheet -->
<link rel="stylesheet" type="text/css" media="all" href="jscalendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />
<!-- main calendar program -->
<script type="text/javascript" src="jscalendar/calendar.js"></script>
<!-- language for the calendar -->
<script type="text/javascript" src="jscalendar/lang/calendar-en.js"></script>
<!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
<script type="text/javascript" src="jscalendar/calendar-setup.js"></script>
</head>
<body margintop=0 marginright=0 marginleft=0>
  <% sqlm="select * from usermenu where userid='" & session("userid") & "' and menu='analyze'"
	openrs rsm,sqlm
	if rsm.eof and rsm.bof then response.redirect "admin.asp"
	closers rsm
 if request("listsubmit") = "Load" then
  call showplateimagesnew
 else
  call showplateimagesmenu
 end if

 sub showplateimagesmenu
%>
<form action="" method="POST">
  <div id="plates" style="width: 1000px; position: relative; background: black; height: 900px;"> 
    <div id="plateleft" style="height: 800px; width: 420px; position: absolute; left:5px; top:5px; background: black; color: white;" align="left"> 
      <div align=left style="margin-bottom: 1em; width:230px;" id="calendar-container"></div>
      <input type="text" name="dateselect" id="dateselect">
      <br>
      <div align=left style="margin-bottom: 1em; width:230px;" id="calendar-container2"></div>
      <input type="text" name="dateend" id="dateend">
      <br>
      Note: Even though the calendars show today's date by default, the value 
      is actually blank until you click on it, allowing you to have it return 
      all dates.<br>
      Site: 
      <select id="site" name="site">
        <option value="-1" selected="selected">All</option>
        <% sql = "SELECT sitecode from [site information] ORDER BY sitecode"
         openrs rs2,sql
         do while not rs2.EOF
%>
        <option value="<%=rs2("sitecode")%>"><%=rs2("sitecode")%></option>
        <%
           rs2.MoveNext
         loop
      %>
      </select>
      Accuracy: 
      <select id="acctype" name="acctype">
        <option value="&lt;">&lt;</option>
        <option value="&lt;=" selected="selected">&lt;=</option>
        <option value="=">=</option>
        <option value="&gt;">&gt;</option>
        <option value="&gt;=">&gt;=</option>
      </select>
      <input type="text" name="accvalue" value="100">
      <br>
      Only show plates (check options that apply)<br>
      <input type=checkbox name=apermit value="yes">
      Exclude plates with permits <br>
      <input type=checkbox name=violator14 value="yes">
      Exclude plates sent a ticket within 14 days <br>
      <input type=checkbox name=dvlarep14 value="yes">
      Exclude plates sent for dvla info within 14 days <br>
      Number to Pull: 
      <input type="text" name="limit" id="limit" value="all">
      <input type="submit" name="listsubmit" id="listsubmit" value="Load">
    </div>
    <div id="plateimages" align="center" style="height: 650px; width: 450px; position: absolute; top:5px; right: 5px; background: black; color: white;"> 
    </div>
  </div>
</form>
 <script type="text/javascript">
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth()+1;     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      if(m<10) m='0'+m;
      document.getElementById("dateselect").value=y+'-'+m+'-'+d;
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged           // our callback function
    }
  );

  function dateChanged2(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth()+1;     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      if(m<10) m='0'+m;
      document.getElementById("dateend").value=y+'-'+m+'-'+d;
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container2", // ID of the parent element
      flatCallback : dateChanged2           // our callback function
    }
  );
</script>
<%
 end sub
 
 sub showplateimagesnew
 response.write "<!--" & request("dateselect") & " " & request("dateend") & "-->"
 limit = "All"
 if(request("limit") <> "" and isNumeric(request("limit"))) then
	limit = request("limit")
	sql = "SELECT top " & limit
 else
	sql = "SELECT"
 end if
 sql = sql & " plate, date, convert(varchar,[date],108) as rtime, picture,  picture2, picture3,  lane, accuracy, site FROM plateimages WHERE  imagesexist=1 and accuracy " & request("acctype") & " '" & request("accvalue") & "'"
' imagesexist=1 and
 if(request("dateselect") <> "" and request("dateend") <> "") then
	sql = sql & " AND [date] BETWEEN '" & request("dateselect") & "' AND '" & request("dateend") & "'"
 end if
 if(request("site") > 0) then
	sql = sql & " AND site = '" & request("site") & "'"
 end if
if request("apermit")="yes" then
sql=sql &" and plate not in(select registration from apermits where registration is not null)"
end if
if request("violator14")="yes" then
sql=sql & " and plate not in(select registration from violators where pcnsent>getdate()-14)"
end if
if request("dvlarep14")="yes" then
sql=sql &" and plate not in(select dvla from dvlarep where dategenerated>getdate()-14)"
end if
 sql = sql & " ORDER BY plate ASC"
 response.write("<!--" & sql & "-->")
 openrs rs,sql
 if not rs.EOF then
  i=0
%>
<div id="plates" style="width: 1000px; position: relative; background: black; height: 700px;"> 
  <div id="plateleft" style="height: 700px; width: 500px; position: absolute; left:5px; top:5px; background: black; color: white;" align="left"> 
    <div align=left style="margin-bottom: 1em; width:230px;" id="calendar-container"></div>
    <input type="hidden" name="dateselect" id="dateselect">
    <% if request("site") = "-1" then %>
    Site: 
    <select id="site" name="site" onChange="searchPlates()">
      <option value="-1" selected="selected">All</option>
      <% sql = "SELECT sitecode from [site information] ORDER BY sitecode"
         openrs rs2,sql
         do while not rs2.EOF
%>
      <option value="<%=rs2("sitecode")%>"><%=rs2("sitecode")%></option>
      <%           rs2.MoveNext
         loop
      %>
    </select>
    <br>
    <% end if %>
    Search: 
    <input type="text" name="searchbox" id="searchbox">
    <input type="button" name="Search" value="Search" onclick="searchPlates()" ID="Search">
    <br>
    <a href="viewplateimages.asp">Back to Search Again</a><br>
    <span id="resultspan"></span><br>
    <div id="platelist" align=left style="height: 350px; overflow-y: scroll; width: 500px; position: absolute; left:1px; bottom: 0;"> 
    </div>
  </div>
  <div id="plateimages" align="center" style="height: 650px; width: 550px; position: absolute; top:5px; right: 5px; background: black; color: white;"> 
  </div>
</div>
 <script type="text/javascript">
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth()+1;     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      if(m<10) m='0'+m;
      if(d<10) d='0'+d;
      document.getElementById("dateselect").value=d+"/"+m+"/"+y;
      searchPlates();
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged           // our callback function
    }
  );
</script>
 <script type="text/javascript" language="JavaScript">
 <!--
 bigarray = new Array();
 
 function preLoad()
 {
  //imgObjects = new Array();
<%
  do while not rs.EOF
   filename = year(rs("date")) & "/" & month(rs("date")) & "/" & day(rs("date")) & "/" & hour(rs("rtime")) & "/" & rs("picture")
   filename2 = year(rs("date")) & "/" & month(rs("date")) & "/" & day(rs("date")) & "/" & hour(rs("rtime")) & "/" & rs("picture2")
   filename3 = year(rs("date")) & "/" & month(rs("date")) & "/" & day(rs("date")) & "/" & hour(rs("rtime")) & "/" & rs("picture3")
   'itemimageurl= "http://cleartonecommunications.com/plateimages/server1/"
 

 itemimageurl="t:\plateimages/server1/" 
itemimage= "t:\plateimages/server1/"
   itemimagesrc="<img src='" & itemimageurl & filename & "' " & Replace(ImageResize(itemimage & filename,400, 120),"""","'") & " border='0' align='middle' valign='middle'><br><br>"
   itemimagesrc2="<img src='" & itemimageurl & filename2 & "' " & Replace(ImageResize(itemimage & filename2,500, 250),"""","'") & " border='0' align='middle' valign='middle'><br><br>"
   itemimagesrc3="<img src='" & itemimageurl & filename3 & "' " & Replace(ImageResize(itemimage & filename3,500, 250),"""","'") & " border='0' align='middle' valign='middle'><br><br>"
   
%>
  //imgObjects[<%=(3*i)%>] = "<%=itemimageurl & filename%>";
  //imgObjects[<%=(3*i+1)%>] = "<%=itemimageurl & filename2%>";
  //imgObjects[<%=(3*i+2)%>] = "<%=itemimageurl & filename3%>";
  bigarray[<%=i%>]= new Array("<%=rs("plate")%>","<%=FormatDateTime(rs("date"),2)%>","<%=itemimagesrc%>","<%=itemimagesrc2%>","<%=itemimagesrc3%>","<%=rs("rtime")%>","<%=rs("accuracy")%>","<%=rs("lane")%>","<%=rs("site")%>");
<%   rs.MoveNext
   i=i+1
  loop
%>
 
  //preImages = new Array();
  //for(i=0; i < imgObjects.length; i++)
  //{
  // preImages[i] = new Image();
  // preImages[i].src = imgObjects[i];
  //}
 }
 
 function searchPlates()
 {
  innerHTML = "<table class='sortable' width=400 style='color: white; padding: 0;' id='plateresult'><TR><TH class='sorttable_alpha sorttable_sorted'>Plate</TH><TH>Date</TH><TH class='sorttable_alpha'>Time</TH><TH>Accuracy</TH><TH>Lane ID</TH><TH>Site</TH></TR>";
  searchvalue = document.getElementById("searchbox").value;
  searchdate = document.getElementById("dateselect").value;
  count = 0;
  for(i=0; i<bigarray.length; i++)
  {
   display= true;
   if(searchdate != '' && bigarray[i][1] != searchdate)
   {
    display = false;
   }
   if(searchvalue != '' && bigarray[i][0].toLowerCase().indexOf(searchvalue) < 0)
   {
    display = false;
   }
<% if request("site") = "-1" then %>
   if(document.getElementById("site").value > 0 && bigarray[i][8] != document.getElementById("site").value)
   {
    display = false;
   }
<% end if %>
   if(display)
   {
    count++;
    innerHTML += "<tr onclick='displayPlates("+i+");alternateRowColors();this.style.background=\"#ffff00\"'><td align='left'>"+bigarray[i][0] + "</td>";
    innerHTML += "<td align='center'>"+bigarray[i][1] + "</td>";
    innerHTML += "<td align='center'>"+bigarray[i][5] + "</td>";
    innerHTML += "<td align='center'>"+bigarray[i][6] + "</td>";
    innerHTML += "<td align='center'>"+bigarray[i][7] + "</td>";
    innerHTML += "<td align='center'>"+bigarray[i][8] + "</tr>";
   }
  }
  document.getElementById("resultspan").innerHTML = "Results: " + count;
  document.getElementById("platelist").innerHTML = innerHTML + "</table>";
  sorttable.makeSortable(document.getElementById("plateresult"));
  alternateRowColors();
 }
 
 function displayPlates(id)
 {
  if(id < bigarray.length)
  {
  // document.getElementById("plateimages").innerHTML = bigarray[id][4] + bigarray[id][3] + bigarray[id][2];
document.getElementById("plateimages").innerHTML = bigarray[id][4] + bigarray[id][3] + bigarray[id][2] + "<font size=10>" + bigarray[id][0] + "</font>";

  }
 }
 
 preLoad();
 searchPlates();
 
 //-->
 </script>
<%
 else
  response.write "No plates found in that date range. Please hit the back button and select a new range." & vbcrlf
 end if
end sub
%>
 </body>
 </html>
