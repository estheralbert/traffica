
/*-------------------------------- Ajax Function ---------------------------*/
var _url;
var _data;
var _dynCont;

function loadDynData(url,data,dynCont){

	_url=url;
	_data=data;
	_dynCont=dynCont;
	loadData();
}
function loadData(){
	
	if(window.XMLHttpRequest){
		
		//used for netscape firefox and other browsers of this genere
		request = new XMLHttpRequest();
	}else if (window.ActiveXObject){
		
		try{
			request = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
		}catch (e){
			try{
				request = new ActiveXObject("Msxml2.XMLHTTP");
			}catch (e){
				//used for internet explorer // we are not supporting this for now as its not fully compatable with that
				request = new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
	}
	if (request){
		
		d = new Date();  //as it always uses cache so u must include timestamp with url
		request.onreadystatechange = pageLoaded;

		//sending data with get method  // will be working with posts latter
		request.open("get", _url+"?"+_data+"&"+ d.toString(), true); 
		request.send("");
	}
}

//this function will be called when requested pages starts executing
function pageLoaded(){ 
	if (request.readyState == 4){ // Request Sent
	
		//request.status 200 means page is executed and downloaded completed
		if (request.status == 200){
			
			//all the data returned from the page is stored in "data" variable
			//now you can use this data either to refresh anypart of this page or to make any decision
			data=request.responseText;

			// we will put data received from server in html object for which request was sent
			document.getElementById(_dynCont).innerHTML=data;
			try{
				afterAjax();
			}catch(t){}
			//document.getElementById(_dynCont).value=data;
		}
	}
}