<!--#include file="openconn.asp"-->
<!--#include file="config.asp"-->
<!--#include file="common.asp"-->
<%
    id = request("id")
    If id = "" Then id=613788

    x = printMobileTicket(id,"1")

    Function printMobileTicket(id, debug)  '' this is live
        'sqlpl = "exec spreprintpcn @ID =" & id
        If debug=1 Then Response.Write "SQL: " &sqlpl & "<br />"

        'Set  rspl = objconn.execute(sqlpl)

        'Init AspPdf
        Set Pdf = Server.CreateObject("Persits.Pdf")
        Set docTicket = Pdf.CreateDocument()
        Set Page = docTicket.Pages.Add(312,567)

		' Create empty param object to be used throughout application
		Set objParam30 = Pdf.CreateParam("size=12; width=300; alignment=left; color=black")
		Set objParam27 = Pdf.CreateParam("size=27; width=300; alignment=center; color=black")
		Set objParam25 = Pdf.CreateParam("size=25; width=300; alignment=center; color=black")
		Set objParam40 = Pdf.CreateParam("size=40; width=300; alignment=center; color=black")

		' Create font object
		Set objFont = docTicket.Fonts.LoadFromFile("c:\windows\fonts\NewsCycle-Regular.ttf")

        ' Create Canvas
        Set objCanvas = Page.Canvas
            
        x = 24: y = 500: lineHeight = 14
    
        'Write violation location
        objParam30("y") = y: objParam30("x") = x
        objCanvas.DrawText "PARKING ENFORCEMENT NOTICE", objParam30, objFont

        objParam30("y") = y - (lineHeight * 2): objParam30("x") = x
        objCanvas.DrawText "We require payment of this Parking Charge ", objParam30, objFont
        objParam30("y") = y - (lineHeight * 3): objParam30("x") = x
        objCanvas.DrawText "Notice(PCN), in accordance with the Parking ", objParam30, objFont
        objParam30("y") = y - (lineHeight * 4): objParam30("x") = x
        objCanvas.DrawText "Terms and Conditions clearly stated on the ", objParam30, objFont
        objParam30("y") = y - (lineHeight * 5): objParam30("x") = x
        objCanvas.DrawText "signage in the car park.", objParam30, objFont
    
        mapPath "v:", "\\fileserver.ccarpark.nsdsl.net\violatortickets" 
        path="v:\mobile" & id & ".pdf"
        Response.Write "<hr />temp path: " & path & "<br />"

        FileName = docTicket.Save(Path, true)

        Set Page = Nothing
        Set docTicket = Nothing

        'Response.Write path & "<br />"
        'Response.Write filename & "<br />"

        printticketcopynewimage = filename
    End Function

    Function imageDateFormat(myDate)
        d = Day(myDate)
        m = Month(myDate) 
        y = Year(myDate)
        imageDateFormat = d & "." & m & "." & y-2000
    End Function
    
    Function drawMultilineText(str, x, y, width, objParam, objCanvas, objFont)
	    lineHeight = 0
	    yoffset = 0
	    lines = Split(str, vbCrLf)

        objParam("x") = x
        objParam("y") = y
        objParam("width") = width

	    ' draw each line
	    For Each line In lines
            objParam("y") = y - yoffset
		    objCanvas.drawText line, objParam, objFont
            lineHeight = CInt(objFont.GetParagraphHeight (line, objParam) * 1.05)
			yoffset = yoffset + lineHeight
            'Response.Write "line: " & line & "<br />"
            'Response.Write "yoffset: " & yoffset & "<br />"
	    Next
    End Function

    Function mapPath(drive, folder)
        Set ws = server.CreateObject("WScript.Network.1")
        strUser = "DSQ8003-12\office"
        strPass = "Creative48"
        On Error Resume Next
        ws.RemoveNetworkDrive drive,true
        If ecdebug=1 Then On Error Goto 0
        ws.MapNetworkDrive drive, folder,false,struser,strpass
    End Function

    Function cleanImageName(filename)
        retval = filename
        If Not IsNull(filename) Then
            pos = InStrRev(filename,"/") 
            If pos > 0 Then
                retval = Right(filename, Len(filename) - pos)
            End If
        End If
        cleanImageName = retval
    End Function

    Function fileExists(filename)
        Set fso = CreateObject("Scripting.FileSystemObject")

        retval = (fso.FileExists(filename))
        fileExists = retval
    End Function

%>