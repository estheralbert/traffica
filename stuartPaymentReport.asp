<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="admininclude.asp"-->
<!--#include file="adminheader.asp"-->

<!-- jQuery -->
<script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>  
<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css"> 
<style type="text/css">
    
</style>
<%
    if request("sbmt")<>"" then
        
        if request("year1")&"" <> "" and request("month1")&"" <> "" then
            mnth1 = protectsqlinjection(request("year1") & request("month1") & "01")
            prms = " @month01 = '"&mnth1&"'"
        else 
                 prms = " @month01 = NULL"
        end if
        if request("year2")&"" <> "" and request("month2")&"" <> "" then
            mnth2 = protectsqlinjection(request("year2") & request("month2") & "01")
            prms = prms & " ,@month02 = '"&mnth2&"'"
    else
          prms = prms & " ,@month02 = NULL"
        end if
        if request("year3")&"" <> "" and request("month3")&"" <> "" then
            mnth3 = protectsqlinjection(request("year3") & request("month3") & "01")
            prms = prms & " ,@month03 = '"&mnth3&"'"
        else
           prms = prms & " ,@month03 = NULL"
        end if

        sql = "exec pcnPaymentStagesReport" & prms
    
      call outputtable(sql)
    call outputexcel(sql)
    end if  
    
    monthOptions = "<option value=''>Select</option>"
    For i = 1 To 12
        j = Right("0" & i, 2)
        monthOptions = monthOptions & "<option value='"&j&"'>"&j&"</option>"
    Next

    yearOptions = "<option value=''>Select</option>"
    For i = 2010 To 2030
        yearOptions = yearOptions & "<option value='"&i&"'>"&i&"</option>"
    Next
%>

<div>
    <script type="text/javascript">
        function validate() {
            var vld = true;
            if ($('select[name="month1"]').val() == '' || $('select[name="year1"]').val() == '') {
                alert('Please select a month and year.');
                vld = false;
            }
            return vld;
        }
    </script>
    <form method="post">
        Month 1: <select name="month1"><%=monthOptions%></select> &nbsp;&nbsp; <select name="year1"><%=yearOptions%></select>
        <br /><br />
        Month 2: <select name="month2"><%=monthOptions%></select> &nbsp;&nbsp; <select name="year2"><%=yearOptions%></select>
        <br /><br />
        Month 3: <select name="month3"><%=monthOptions%></select> &nbsp;&nbsp; <select name="year3"><%=yearOptions%></select>
        <br /><br />
        <input type="submit" name="sbmt" value="Submit" onclick="return validate();"/>
    </form>
</div>
    


