  <!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="admininclude.asp"-->
<!--#include file="image.asp"-->
<LINK href="adminstylesheet.css" rel=stylesheet type=text/css>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
  $(function() {
    $( ".datepicker").datepicker({ dateFormat: "yy-mm-dd" }).val();    
  });
  </script>

<span class=noPrint>
<div id=header><a href=admin.asp?cmd=adminhome><img src="logo.jpg"  border="0" class=logo></a>
    <%if session("admin")="ok" then %>
        <div class=hright><a href="admin.asp?cmd=adminlogout" class=button>Log Out</a></div>
        <div class=welcome>Welcome <%= session("username") %></div>
        <div class=pcnheader>
            <form method=post action=admin.asp?cmd=search2>
            Enter PCN/Plate:<input type=text name=pcn><input type=submit  value="Go" class=button>
            </form>
        </div>
    <%end if%>
</div>
</span>
<% 
call checklogin 
if session("admin")="ok" and showadminmenu<>1 then
    displaymenu = 1
	 call getadminmenu
	showadminmenu=1
end if

    if request("sbmt")="" then
    %>
<script type="text/javascript">
    function validateForm() {
        return true;
    }
</script>
<style type="text/css">
    .frm {width:400px; margin:10px auto;}
    .frm .lbl {display:inline-block;min-width:100px;max-width:100px;}
    .frm input {margin:10px 0px;}
</style>
<form method="post" class="frm">
    <h1>Search Site Issues</h1>
    <span class="lbl">Open Issues:</span> <input type="checkbox" name="searchOpen" value="1" />
    <br />
    <span class="lbl">Closed Issues:</span> <input type="checkbox" name="searchClosed" value="1" />
    <br />
    <span class="lbl">From Date:</span> <input type="text" name="searchFrom" class="datepicker" />
    <br />
    <span class="lbl">To Date:</span> <input type="text" name="searchTo" class="datepicker" />
    <br />
    <span class="lbl">Site:</span> 
    <select name ="searchSite">
        <option value="">Select</option>
    <%
        openrs rss, "SELECT sitecode FROM [site information] WHERE ISNULL(disabled,0) = 0 ORDER BY sitecode"
        do while not rss.eof                
    %>
        <option value="<%=rss("sitecode")%>"><%=rss("sitecode") %></option>
    <%
            rss.movenext
        loop
        closers rss
    %>
    </select>
    <br /><br />
    <span class="lbl">Issue:</span>
    <select name ="searchIssue">
        <option value="">Select</option>
    <%
        openrs rss, "select issuename as issue from issues order by issuename"
        do while not rss.eof                
    %>
        <option value="<%=rss("issue")%>"><%=rss("issue") %></option>
    <%
            rss.movenext
        loop
        closers rss
    %>
    </select>
    <br /><br />
    <span class="lbl">Category:</span>
    <select name ="searchCategory">
        <option value="">Select</option>
    <%
        openrs rss, "SELECT DISTINCT category FROM [site information] WHERE category != '' ORDER BY category"
        do while not rss.eof                
    %>
        <option value="<%=rss("category")%>"><%=rss("category") %></option>
    <%
            rss.movenext
        loop
        closers rss
    %>
    </select>
    <br /><br />
    <span class="lbl">Duration:</span> <input type="number" name="searchDuration" min="0"/>
    <br /><br />
    <span class="lbl">Number of Hours:</span> <input type="number" name="searchHours" min="0"/>
    <br /><br />
    <span class="lbl">Opened By:</span>
    <select name ="searchOpenedBy">
        <option value="">Select</option>
    <%
        openrs rsu, "SELECT DISTINCT ua.ID, ua.UserID FROM [useraccess] ua inner join siteissues si on si.useridstart = ua.id WHERE isnull(UserID,'') != '' ORDER BY UserID"
        do while not rsu.eof                
    %>
        <option value="<%=rsu("ID")%>"><%=rsu("UserID") %></option>
    <%
            rsu.movenext
        loop 
        closers rsu
    %>
    </select>
    <br /><br />
    <span class="lbl">Closed By:</span>
    <select name ="searchClosedBy">
        <option value="">Select</option>
    <%
        openrs rsu, "SELECT DISTINCT ua.ID, ua.UserID FROM [useraccess] ua inner join siteissues si on si.useridend = ua.id WHERE isnull(UserID,'') != '' ORDER BY UserID"
        do while not rsu.eof              
    %>
        <option value="<%=rsu("ID")%>"><%=rsu("UserID") %></option>
    <%
            rsu.movenext
        loop    
        closers rsu
    %>
    </select>
    <br />
    <input type="submit" name="sbmt" value="Search" onclick="return validateForm();"/>

</form>

<%

    else
        searchOpen = request("searchOpen")
        searchClosed = request("searchClosed")
        searchFrom = request("searchFrom")
        searchTo = request("searchTo")
        searchSite = request("searchSite")
        searchIssue = request("searchIssue")
        searchCategory = request("searchCategory")
        searchDuration = request("searchDuration")
        searchOpenedBy  = request("searchOpenedBy")
        searchClosedBy = request("searchClosedBy")
        searchHours = request("searchHours")
        
        if searchHours&"" <> "" and isnumeric(searchHours) then
            sql = "SELECT COUNT(*) AS cnt FROM siteissues siss LEFT JOIN [site information] sinfo ON sinfo.sitecode = siss.site WHERE 1=1"
            if searchOpen = "1" then sql = sql & " AND siss.enddate IS NULL"
            if searchClosed = "1" then sql = sql & " AND siss.enddate IS NOT NULL"
            if isdate(searchFrom) then sql = sql & " AND siss.startdate >= " & tosql(searchFrom, "Date")
            'if isdate(searchTo) then sql = sql & " AND siss.enddate <= " & tosql(searchTo, "Date") --this would clash with searchOpen clause
            if isdate(searchTo) then sql = sql & " AND siss.startdate <= " & tosql(searchTo, "Date")
            if searchSite&"" <>"" then sql = sql & " AND LOWER(siss.site) = LOWER(" & tosql(searchSite, "Text") & ")"
            if searchIssue&"" <>"" then sql = sql & " AND LOWER(siss.issue) like  '%" &  lcase(searchIssue) & "%'"
            if searchCategory&"" <>"" then sql = sql & " AND LOWER(sinfo.category) = LOWER(" & tosql(searchCategory, "Text") & ")"
            if searchDuration <> "" and isnumeric(searchDuration) then sql = sql & " AND DATEDIFF(day, siss.startdate, siss.enddate) = " & tosql(searchDuration,"Number")
            if searchOpenedBy <> "" and isnumeric(searchOpenedBy) then sql = sql & " AND useridstart = " & tosql(searchOpenedBy,"Number")
            if searchClosedBy <> "" and isnumeric(searchClosedBy) then sql = sql & " AND useridend = " & tosql(searchClosedBy,"Number")
            
            openrs rs, sql
            ttl = rs("cnt")
            closers rs

            sql = "SELECT COUNT(*) AS cnt FROM siteissues siss LEFT JOIN [site information] sinfo ON sinfo.sitecode = siss.site WHERE 1=1"
        
            if searchOpen = "1" then sql = sql & " AND siss.enddate IS NULL"
            if searchClosed = "1" then sql = sql & " AND siss.enddate IS NOT NULL"
            if isdate(searchFrom) then sql = sql & " AND siss.startdate >= " & tosql(searchFrom, "Date")
            'if isdate(searchTo) then sql = sql & " AND siss.enddate <= " & tosql(searchTo, "Date") --this would clash with searchOpen clause
            if isdate(searchTo) then sql = sql & " AND siss.startdate <= " & tosql(searchTo, "Date")
            if searchSite&"" <>"" then sql = sql & " AND LOWER(siss.site) = LOWER(" & tosql(searchSite, "Text") & ")"
            if searchIssue&"" <>"" then sql = sql & " AND LOWER(siss.issue) like  '%" &  lcase(searchIssue) & "%'"
            if searchCategory&"" <>"" then sql = sql & " AND LOWER(sinfo.category) = LOWER(" & tosql(searchCategory, "Text") & ")"
            if searchDuration <> "" and isnumeric(searchDuration) then sql = sql & " AND DATEDIFF(day, siss.startdate, siss.enddate) = " & tosql(searchDuration,"Number")
            if searchOpenedBy <> "" and isnumeric(searchOpenedBy) then sql = sql & " AND useridstart = " & tosql(searchOpenedBy,"Number")
            if searchClosedBy <> "" and isnumeric(searchClosedBy) then sql = sql & " AND useridend = " & tosql(searchClosedBy,"Number")
            sql = sql & " AND DATEDIFF(hh, siss.startdate, siss.enddate) <= " & tosql(searchHours, "Number")
            
            openrs rs, sql
            cnt = rs("cnt")
            closers rs
            
            msg = cnt & " out of " & ttl & " Site Issues fixed within " & searchHours & " hours. " & formatNumber(100*cdbl(cnt / ttl),0) & "%"
        end if
        sql = "SELECT siss.date,siss.site,siss.issue,sinfo.category,siss.startdate,siss.enddate FROM siteissues siss LEFT JOIN [site information] sinfo ON sinfo.sitecode = siss.site WHERE 1=1"
        
        if searchOpen = "1" then sql = sql & " AND siss.enddate IS NULL"
        if searchClosed = "1" then sql = sql & " AND siss.enddate IS NOT NULL"
        if isdate(searchFrom) then sql = sql & " AND siss.startdate >= " & tosql(searchFrom, "Date")
        'if isdate(searchTo) then sql = sql & " AND siss.enddate <= " & tosql(searchTo, "Date") --this would clash with searchOpen clause
        if isdate(searchTo) then sql = sql & " AND siss.startdate <= " & tosql(searchTo, "Date")
        if searchSite&"" <>"" then sql = sql & " AND LOWER(siss.site) = LOWER(" & tosql(searchSite, "Text") & ")"
        if searchIssue&"" <>"" then sql = sql & " AND LOWER(siss.issue) like  '%" &  lcase(searchIssue) & "%'"
        if searchCategory&"" <>"" then sql = sql & " AND LOWER(sinfo.category) = LOWER(" & tosql(searchCategory, "Text") & ")"
        if searchDuration <> "" and isnumeric(searchDuration) then sql = sql & " AND DATEDIFF(day, siss.startdate, siss.enddate) = " & tosql(searchDuration,"Number")
        if searchOpenedBy <> "" and isnumeric(searchOpenedBy) then sql = sql & " AND useridstart = " & tosql(searchOpenedBy,"Number")
        if searchClosedBy <> "" and isnumeric(searchClosedBy) then sql = sql & " AND useridend = " & tosql(searchClosedBy,"Number")
        sql = sql & " ORDER BY siss.date DESC"
  '  response.write sql
        call outputexcel(sql)
        Response.write "<br>"
        if msg <> "" then response.write "<br/><h2>" & msg & "</h2><br/>"
        call outputtable(sql)
           
    end if


%>