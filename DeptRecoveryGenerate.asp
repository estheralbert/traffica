<% Server.ScriptTimeout = 3600000 %>
<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%
 	pStr = "private, no-cache, must-revalidate" 
    	Response.ExpiresAbsolute = #2000-01-01# 
    	Response.AddHeader "pragma", "no-cache" 
    	Response.AddHeader "cache-control", pStr 
   
%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->

<!--#include file="image.asp"-->


<html>
<head>
<style type="text/css">
	body {
		font-family:"Times New Roman";serif;
		font-size:10px;		
	}
	h1 {
		font-family:Arial;sans-serif;
		text-align:right;
		padding-right: 20px;
		font-size: 18px;
	}
	table {
		font-size:10px;
	}
	table.detail th {
		padding: 5px;		
	}
	table.detail td {
		padding: 5px;		
	}
	table.detail {
		border-width: 2px;
		border-spacing: 0px;
		border-style: solid;
		border-color: black;
		border-collapse: collapse;
	}
	table.detail th {
		border-width: 1px;
		border-style: inset;
		border-color: black;
	}
	table.detail td {
		border-width: 1px;
		border-style: inset;
		border-color: black;
	}
	th {
		text-align: left;
	}
	div.address {
		height: 105px;
		vertical-align: top;
		padding-left: 10px;
		width: 450px;
	}
	.addressname {
		font-weight: bold;
		font-size: 12px;		
	}
	table.main {
		width: 670px;
	}
	ol {
		margin: 0px;
  		padding-left: 20px;
	}
	li {
		padding-left: 5px;
		padding-bottom: 12px;
		font-size: 1em;
	}
	.num {
		text-align: right;
	}
	.center {
		text-align: center;
	}
	p.bolded {
		font-weight: bold;
	}
	#coverletter, #coverletter table {
		font-family: Arial;sans-serif;
	}
	#coverletter li {
		padding-bottom: 3px;
	}
	#debtrecovery, #debtrecovery table {
		font-size: 12px;
	}
	
</style>
</head>
<body>
	<% 
	call viewrecord(request("id"))

	sub viewrecord(id)
		pcncan=0
		color="black"
		sql="SELECT VIOLATORS.*, DVLA.Make, DVLA.Model, DVLA.Colour, DVLA.Owner, DVLA.Address1, DVLA.Address2, DVLA.Address3, DVLA.Town, DVLA.Postcode, DVLA.[DVLA Status], DVLA.[DateReceived] FROM DVLA RIGHT JOIN VIOLATORS ON DVLA.id = VIOLATORS.dvlaid where violators.id=" &  id
		openrs rs,sql
		
		sqlcs="select sitecode from [site information] where sitecode='" & rs("site") & "'"
		response.write "<!--" & sqlcs & "-->"
		openrs rscs,sqlcs
		if  rscs.eof and  rscs.bof then
			response.write "no site information available"
			exit sub
			closers rscs
		end if
				
		sqlccan="select * from cancelled where pcn='" & rs("pcn") & rs("site") & "'"
		openrs rsccan,sqlccan
		if not rsccan.eof and not rsccan.bof then
		%>
		
			<div align=center class=canbig>
				CANCELLED <%=rsccan("cancelled")%><a href="admin.asp?cmd=printcancelpcn&pcn=<%=rsccan("pcn")%>" class="button" target="_new">View Cancellation Form</a>
			</div>
		<%
		cancelleddate=rsccan("cancelled")
		reason=rsccan("reason")
		requestedby=rsccan("requestedby")
		authby=rsccan("authby")
		reasonother=rsccan("reasonother")
		
		color="red"
		pcncan=1
		end if
		closers rsccan
		
		'checkdebtrecorery
		sqld="select * from debtrecovery where pcn=" & tosql(rs("pcn") & rs("site"),"text")
		openrs rsd,sqld
		
		if not rsd.eof and not rsd.bof then response.Write "<b>Sent to Debt Recovery on " & rsd("debtrecoverydate") & "</b>"
		closers rsd
		
		'checkdebtrecorery
		sqld="select * from debtrecoverycancelled where pcn=" & tosql(rs("pcn") & rs("site"),"text")
		openrs rsd,sqld
		
		if not rsd.eof and not rsd.bof then response.Write "<b>Sent to Debt Recovery on " & rsd("debtrecoverydate")  & " cancelled on " & rsd("datecancelled") & "</b>"
		closers rsd
		sqlp="select amount,received,method,bookreference from payments where pcn=" & tosql(rs("pcn") & rs("site"),"text") '& " and right([pcn],3)= " & tosql(rs("site"),"text")
		recpayment=0
		response.write "<!--" & sqlp & "-->"
		openrs rsp,sqlp
		if rsp.eof and rsp.bof then
			response.write "<!--norecord-->"
			amountpaid=0
			received=date()
		else
			tamountpaid=0
			recpayment=1
			numpayment=0
			do while not rsp.eof
				amountpaid=rsp("amount")
				received=rsp("received")
				strpaidh=strpaidh & "on " & rsp("received")
				strpaid=strpaid & "Paid on " & rsp("received") & "<br>Amount:" & rsp("amount") & "<br>Method of Payment:" & rsp("method") & "<br>Reference #:" & rsp("bookreference") & "<br><br>"
				numpayment=numpayment+1
				tamountpaid=tamountpaid+amountpaid						
				rsp.movenext
			loop
				
			if numpayment>1 then strpaid=strpaid &  "Total Paid:" & tamountpaid
		end if
		response.write  "<!--" & amountpaid & "-->"
		closers rsp
			
		'sqlt="select * from tariff where site=" & tosql(rs("site"),"text")
		'openrs rst,sqlt
		'response.write received
		days=datediff("d",rs("pcnsent"),received)
		'response.write "days:" & days & "+"
		if days>14 then 
			'response.write rss("pcn") & " is more then 14 datys<br>"
			amountowed=rs("ticketprice")
		else
			amountowed=rs("reducedamount")
		end if
		response.write  "<!--amountowed" & amountowed & "-->"
		'response.write "amount owed for " & rss("pcn") & " is:" & amountowed-amountpaid  & "<bR><hr>"
		amountowed=amountowed-tamountpaid
		if recpayment=1 then 
			response.write "<div align=center class=paidbig>PAID " & strpaidh & "</div>"
			color="blue"
		end if
		sqlper="select registration,dateentered from apermits where registration='" & rs("registration") & "'"
		openrs rsper,sqlper
		if not rsper.eof and not rsper.bof then 
			response.write "<div align=center class=paidbig>PERMIT added " & rsper("dateentered") & "</div>"
		end if
		closers rsper
		%>
		
		
		<div id="debtrecovery">
		<table>
		<%
		if checkifletteroutstanding(rs("pcn")& rs("site"))=1 then response.write "<tr><td colspan=2><b>Status:Outstanding Communications</b></td></tr>"
		if rs("Arrival Time")<>"" then arrivaltime=formatdatetime(rs("Arrival Time"),4)
		if rs("Departure Time")<>"" then departuretime=formatdatetime(rs("Departure Time"),4)
		if rs("Duration of Stay")<>"" then durationofstay=formatdatetime( rs("Duration of Stay"),4)
		%>
		
			<tr><td colspan=2><b>Site Information</b></td></tr>
			<tr><th align=left>Plate(Registration)</th><td><%= rs("registration") %></td></tr>
			<tr><th align=left>Date of Violation</th><td><%= rs("Date of Violation") %></td></tr>
			<tr><th align=left>Site</th><td><%= DLookUp("[Site Information]", "[Name of Site]","sitecode='" & rs("site") & "'") %></td></tr>
			<tr><th align=left>Arrival Time</th><td><%= arrivaltime %></td></tr>
			<tr><th align=left>Departure Time</th><td><%= departuretime %></td></tr>
			<tr><th align=left>Duration of Stay</th><td><%=durationofstay %></td></tr>
			<tr><th align=left>PCN</th><td><%= rs("pcn") %><%= rs("site") %></td></tr>
			<tr><td colspan=2><hr><b>Vehicle Information</b></td></tr>
			<% if rs("site")=290 then %>
				<tr><th align=left>Make</th><td>*****</td></tr>
				<tr><th align=left>Colour</th><td>*****</td></tr>
				<tr><td colspan=2><hr><b>Owner Information</b></td></tr>
				<tr><th align=left>Owner</th><td>*****</td></tr>
				<tr><th align=left>Address1</th><td>*****</td></tr>
				<tr><th align=left>Address2</th><td>*****</td></tr>
				<tr><th align=left>Address3</th><td>*****</td></tr>
				<tr><th align=left>town</th><td>*****</td></tr>
				<tr><th align=left>PostCode</th><td>*****</td></tr>
			<% else %>
				<tr><th align=left>Make</th><td><%= rs("Make") %></td></tr>
				<tr><th align=left>Colour</th><td><%= rs("Colour") %></td></tr>
				<% if not isnull(rs("driversname")) and rs("driversname")<>"" then %>
					<tr><td colspan=2><hr><b>Registered Keeper Information</b></td></tr>
					<tr><th align=left>Owner</th><td><%= pcase(rs("Owner")) %></td></tr>
					<tr><th align=left>Address1</th><td><%= pcase(rs("Address1")) %></td></tr>
					<tr><th align=left>Address2</th><td><%= pcase(rs("Address2")) %></td></tr>
					<tr><th align=left>Address3</th><td><%= pcase(rs("Address3")) %></td></tr>
					<tr><th align=left>town</th><td><%= pcase(rs("town")) %></td></tr>
					<tr><th align=left>PostCode</th><td><%= rs("Postcode") %></td></tr>
				<% else %>
					<tr><td colspan=2><hr><b>Owner Information</b></td></tr>
					<tr><th align=left>Owner</th><td><%= pcase(rs("Owner")) %></td></tr>
					<tr><th align=left>Address1</th><td><%= pcase(rs("Address1")) %></td></tr>
					<tr><th align=left>Address2</th><td><%= pcase(rs("Address2")) %></td></tr>
					<tr><th align=left>Address3</th><td><%= pcase(rs("Address3")) %></td></tr>
					<tr><th align=left>town</th><td><%= pcase(rs("town")) %></td></tr>
					<tr><th align=left>PostCode</th><td><%= rs("Postcode") %></td></tr>
				<% end if %>
		
			<% end if %>
			<% if rs("dvla status")<>"" then %>
				<tr><th align=left>DVLA Status</th><td><%= rs("dvla status") %></td></tr>
			<% end if %>
			<tr><th align=left>PCN Sent Date</th><td><%= rs("pcnsent") %></td></tr>
			<% if not isnull(rs("driversname")) and rs("driversname")<>"" then %>
				<tr><td colspan=2><hr /><b>Drivers Information</b></td></tr>
				<tr><th align=left>Drivers Name</th><td><%= pcase(rs("driversname")) %></td></tr>
				<tr><th align=left>Drivers Address</th><td><%= pcase(rs("driversaddress")) %></td></tr>
				<tr><th align=left>Drivers Address 2</th><td><%= pcase(rs("driversaddress2")) %></td></tr>
				<tr><th align=left>Drivers Town</th><td><%= pcase(rs("driverstown")) %></td></tr>
				<tr><th align=left>Drivers Post code</th><td><%= rs("driverspostcode") %></td></tr>
			<% end if  %>
		
			<tr><td colspan=2><hr><strong>Paid</strong><br><%= strpaid %></td></tr>
		<% 
		sqldp="select * from declinedpayments where pcn='" & rs("pcn") &  rs("site") & "'"
		'response.Write sqldp
		openrs rsdp,sqldp
		if not rsdp.eof and not rsdp.bof then
			do while rsdp.eof
				response.Write "<tr><td colspan=2>Declined Payment:" & rsdp("reason") & "</td></tr>"
				rsdp.movenext
			loop
		end if 
		closers rsdp
		
		'check if reminder sent
		reminder=0
		sqlrem="select * from reminders where pcn='" & rs("pcn") & "'"
		openrs rsrem,sqlrem
		if not rsrem.eof and not rsrem.bof then
			reminder=1
			response.write "<tr><th align=left class=highlight>Reminder</th><td class=highlight>" & rsrem("REMINDER") & "</td></tr>"
		end if
		
		'' check if owes and if amount outstading and how many days
		
			
		if amountowed>0 then
			 response.write "<tr><th align=left>Amount Owed:</td><td> " & amountowed & "</tD></tr>"
			 response.write "<tr><th align=left>Days Overdue:</td><td> " & days & "</tD></tr>"
		else
			response.write "<tr><th colspan=2>Paid in Full</td></tr>"
		end if	
		'closers rst
			
			
		sqlre="select * from re_issue_data where pcn='" & rs("pcn") & "'"
		openrs rsre,sqlre
		if not rsre.eof and not rsre.bof then
			do while not rsre.eof
				response.write "<tr><th><b>Reissued on </b></th><td>" & rsre("pcn_sent") & "<a href=""javascript:popUp('admin.asp?cmd=viewreissue&id=" & rsre("id") & "')"" class=button>View</a></td></tr>"
				rsre.movenext
			loop
		end if
		closers rsre
		sqlre="select * from reissues where reissuedate is not null and violatorid=" & rs("id")
		openrs rsre,sqlre
		if not rsre.eof and not rsre.bof then
			do while not rsre.eof
				response.write "<tr><th><b>Reissued on </b></th><td>" & rsre("reissuedate") & "<a href=""javascript:popUp('admin.asp?cmd=viewreissue2&p=1&id=" & rsre("id") & "')"" class=button>View</a></td></tr>"
				rsre.movenext
			loop
		end if
		closers rsre
		'lookup users other pcn according to plate within users access site
		sqlpc="select * from violators where registration=" & tosql(rs("registration"),"text")
		if session("adminsite")=true then
			sqlsites="select * from [site information]"
		else
			sqlsites="select * from usersites where userid=3" '& session("userid")
		end if
		openrs rssites,sqlsites  ' check the sites he can see 
		if rssites.eof and rssites.bof then
			 response.write "YOu do not have access to any site information"
			 response.End
		end if
		
		i=1
		sqlpc=sqlpc & " and ("
		do while not rssites.eof
			if i>1 then sqlpc=sqlpc & " OR "
		 	sqlpc=sqlpc & "(site='" &  rssites("sitecode") & "')" 
			i=i+1
			rssites.movenext
		loop 
		closers rssites
		
		sqlpc=sqlpc & ") and pcn<>" &tosql(rs("pcn"),"text")
		
		openrs rspc,sqlpc
		if not rspc.eof and not rspc.bof then
			response.write "<tr><td colspan=2><b>Other PCN's with this plate: </b><br><font class=small>Click on pcn to view details</font>"
			do while not rspc.eof
				response.write "<br /><a class=link href=admin.asp?cmd=viewrecord&id=" & rspc("id") & ">" & rspc("pcn") & "</a>"
				rspc.movenext
			loop
			response.write "</td></tr>"
		end If 
		closers rspc
		
		
		
		if pcncan=1 then
			response.Write "<tr><td colspan=2>"
			Response.Write "Cancelled on " & cancelleddate & "<br>Reason:" & reason & "<br>Requested by:" & requestedby & "<br>Auth By:" & authby
			if reasonother<>"" then response.Write "<br>Reason Other:" & reasonother
			response.Write "</td></tr>"
		end if%>
		
		<tr><td><b>Communications:</b></td></tr>
		 <%
		 sqlap="select * from vwcommunications where violatorid=" & id & " order by date desc"
		 'response.write sqlap
		openrs rsap,sqlap
		 if not rsap.eof and not rsap.bof then
		 do while not rsap.eof
		 slettertype=rsap("type")
		 if slettertype="Letter" then
		 'response.write "here"
		 sqll="select * from generalletters left join lettertypes on lettertypes.id=generalletters.lettertypeid where generalletters.id=" & rsap("id")
		 openrs rsl,sqll
		slettertype="Letter(" & rsl("type") & ")"
		closers rsl 
		 end if
		 link=rsap("link")
		 showlink=1
		 
		 	%>
			<tr><td colspan=2 align=left>Letter <%=slettertype%> dated <%=rsap("date") %>
			
			<%
			'end if
			if rsap("statusid")<>0 and rsap("statusid")<>1 then
				sstatus=getletterstatus(rsap("statusid"))
				response.write "Status:" & sstatus
			
			end if
			%>
			</td></tr>
		
			<%
			 rsap.movenext
		loop
		 end if 
		 closers rsap
		 %>
		
		<%
		spcn=rs("pcn") & rs("site")
		sqlap="select * from appeals where pcn=" & tosql(spcn,"text")
		'response.write sqlap
		openrs rsap,sqlap
		
		if not rsap.eof and not rsap.bof then
			response.write "<tr><Td colspan=2><table><tr><th colspan=2>Voice Messages</th></tr>"
		 	do while not rsap.eof
		%>
				<tr><td colspan=2 align=center>Voice Message on  <%=rsap("mydatetime") %> <a href=admin.asp?cmd=viewappeal&id=<%=rsap("id") %> class=button target=_new>View Message</a></td></tr>
		
		<%
				rsap.movenext
			loop
			response.write "</table></td></tr>"
		end if 
		closers rsap
		
		 
		sqlp="select * from manualpayments where pcn='" & rs("pcn") & rs("site") & "' order by date"
		'response.Write sqlp
		openrs rsp,sqlp
		
		do while not rsp.eof
			response.Write "<tr><td colspan=2>Manual Payment entered on "  & rsp("date") & " " & rsp("errorcode") & " Amount:" & rsp("amount") &"</td></tr>"
			rsp.movenext
		loop
		 
		closers rsp
		 
		sqlck="select * from declinedpayments where pcn='" & rs("pcn") & rs("site") & "'"
		openrs rsck,sqlck
		if not rsck.eof and not rsck.bof then
			response.write "<tr><td colspan=2><b>Declined Payments</b><br>"
			do while not rsck.eof
				response.write "Amount of " & rsck("amount") & " was declined on " & rsck("mydate") & " reason:" & rsck("reason") & " Phone:" & rsck("phone") &"<br>"
		 		rsck.movenext
			loop
		end if
		closers rsck
		%>
			<tr><td colspan=2>
				<%
				'response.Write "aa"
				sqlsigns="select signs from [site information] where sitecode=" & tosql(rs("site"),"text")
				openrs rssigns,sqlsigns
				if not rssigns.bof and not rssigns.eof then response.Write "Signs:" & rssigns("signs")
				closers rssigns
		%>
			</td></tr>
			<tr><td colspan=2>
		<%
				call shownotes(rs("pcn") & rs("site"))
		%>
		 	</td></tr>
		
			</table>
		</div>
		<%
		'call showsiterules(rs("site")) 
		closers rs
		sql="select * from violators where id=" & id 
		openrs rs,sql
		sqldvla="select * from dvla where ID=" & rs("dvlaid")
		openrs rsdvla,sqldvla
		owner=rsdvla("owner")
		address1=rsdvla("address1")
		address2=rsdvla("address2")
		address3=rsdvla("address3")
		town=rsdvla("town")
		postcode=rsdvla("postcode")
		
		closers rsdvla
		
		sqlrem="select reminder from reminders where pcn=" & tosql(rs("pcn"),"text")
		openrs rsrem,sqlrem
		reminderdate=rsrem("reminder")
		closers rsrem
		%>
			<BR style="page-break-before: always">
			<div id="coverletter">
				<p class="bolded">
					<%=owner%><br />
					<%if address1<>"" then response.Write address1 & "<br>"%>
					<%if address2<>"" then response.Write address2 & "<br>"%>
					<%if address3<>"" then response.Write address3 & "<br>"%>
					<%if town<>"" then response.Write town & "<br>"%>
					<%if postcode<>"" then response.Write postcode & "<br>"%>
				</p>
				<p class="bolded">CIVIL ENFORCEMENT LIMITED -v- <%=owner %></p> 
				<p>Despite sending you a Reminder Notice on <b><%=formatdatetime(reminderdate,2) %></b> your debt remains 
				outstanding.<b> </b>As a result of your continued failure to settle the amount, 
				we now confirm that your liability to us has now increased as follows:- 
				<table>
				<tr>
					<td>Debt as of <%=date %></td>
					<td class="num" width="75px">&pound;<%=formatnumber(rs("ticketprice"),2) %></td>
				</tr>
				<tr>
					<td>Additional Costs</td>
					<td class="num">&pound;115.00</td>
				</tr>
				<tr>
					<th>Payment Now due</th>
					<th class="num">&pound;<%= formatnumber(rs("ticketprice")+115,2)%></th>
				</tr>
				</table>
				<p>In view of the above we have unfortunately been left with no alternative but 
				to instruct solicitors to draft Particulars of Claim (see attached) which we 
				intend to submit to Northampton County Court on <b>
				<%paymentdate=dateadd("d",8,date())
				paymentdate1=dateadd("d",7,date())
				if weekday(paymentdate)=1 then paymentdate=dateadd("d",9,date())
				if weekday(paymentdate)=7 then paymentdate=dateadd("d",10,date())
				response.Write paymentdate%>.</b></p> 
				<p clas="bolded">Please be aware that our representatives will be seeking full costs of the 
				application together with all our legal fees in dealing with this matter.</p>
				<p>Once a Judgment is obtained the options available to us is as follows:-</p>
				<ul>
					<li>A Warrant of Execution</li> 
					<li>Appoint Court Bailiffs to attend your property </li> 
					<li>Seize your vehicle / goods </li> 
					<li>Apply for an attachment of earning (see information below)</li>  
					<li>Third Party debt Order (see information below) </li> 
				</ul>
				<br />
				<p>If you are employed we may make an application for an <b>attachment of 
				earnings order </b>which will be sent to your employer. It will ensure that your 
				employer will take an amount from your earnings each pay day and send it to a 
				collection office which will then be forwarded to us.</p>
				<p>We may also make an application for a <b>thirdparty debt order </b>which is 
				usually made to stop the debtor from taking money out of his or her bank or 
				building society account. The money we are owed will be paid to us from your 
				account.</p>
				<p>As this matter remains unresolved costs are simply going to escalate. 
				However, we appreciate the level of your debt and are therefore prepared to 
				accept without prejudiced a reduced amount of &pound;<%=formatnumber(rs("ticketprice"),2) %> as full and final 
				settlement subject to such payment being made by no later than  <%=paymentdate1 %></p>
				<p>Please note, should you not accept our reduced offer we intend to show this 
				letter to the Court in respect of our costs. <b>Payments of the reduced amount 
				can only be made by calling 0115 822 5023. </b></p>
			</div>
		
			<br style="page-break-before: always">
		<%
		closers rs
		sql="select * from violators where id=" & id 
		openrs rs,sql
		%>
			<table class="main">
			<tr>
				<td width="450">
					<h1>C l a i m&nbsp;&nbsp;F o r m</h1>
				</td>
				<td>
					<table class="detail" width="100%">
					<tr>
						<th>In The</th>
						<td class="center">Northampton County Court</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<td class="center">For court use only</td>
					</tr>
					<tr>
						<th>Claim No.</th>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<th>Issue Date</th>
						<td>&nbsp;</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
			<p>&nbsp;</p>
			<p>Claimant(s)</p>
			
			<div class="address">
				<img style="float:right;" src="seal.jpg"><p><span class="addressname">CIVIL ENFORCEMENT LIMITED</span><br />
				Horton House<br />
				Exchange Flags<br />
				Liverpool<br />
				L2 3PF </p>
			</div>
			<p>Defendant(s)</p>
			<div class="address">
				&nbsp;
			</div>
			<p>Brief details of claim</p>
			<div class="address">
				<br />
				Damages arising from a breach of contract<br />
				<br />
				See Particulars of Claim overleaf
			</div>
			<p>Value</p>
			<div class="address">
				&nbsp;
			</div>
			<table class="main">
			<tr>
				<td valign="top" width="450">
					<b>Defendantís name and address</b>
						<div>
							&nbsp;
						</div>
				</td>
				<td>
					<table class="detail" width="100%">
					<tr>
						<td width="100px">Amount Claimed</td>
						<td class="num">&pound;<%= formatnumber(rs("ticketprice")+115,2)%></td>
					</tr>
					<tr>
						<td>Court Fee</td>
						<td class="num">&pound;35.00</td>
					</tr>
					<tr>
						<td>Solicitorís Costs</td>
						<td class="num">&pound;110.00</td>
					</tr>
					<tr>
						<th>Total Amount</th>
						<th class="num">&pound;<%= formatnumber(rs("ticketprice")+115+35+110,2)%></th>
					</tr>
					</table>
				</td>
			</tr>
			</table>
			<hr />
			The court office at <br />
			<br />
			Is open between 10 am and 4 pm Monday to Friday.  When corresponding with the court please address forms or letters to the Court Manager and quote the claim number
			<hr />
			<table class="main">
			<tr>
				<td><b>N1</b> Claim form (CPR Part 7) (January 2002)</td>
				<td width ="100%" align="right">Crown Copyright. Reproduced by Sweet & Maxwell Ltd</td>
			</tr>
			</table>
			<br style="page-break-before: always">
		
		<% closers rs 
		sql="select * from VIOLATORS v left join [Site Information] s  on s.sitecode=v.site left join dvla d on d.id=v.dvlaid where v.ID=" & id 
		openrs rs,sql
		%>
			<table class="main">
			<tr>
				<td valign="top" width="450">&nbsp;</td>
				<td>
					<table class="detail" width="100%">
					<tr>
						<td width="70px">Claim No.</td>
						<td>&nbsp;</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
			<p>&nbsp;</p>
			<p>Does, or will, your claim include any issues under the Human Rights Act 1998? Yes  <input type=checkbox />&nbsp;No  <input type=checkbox checked /></p>
			<p>&nbsp;</p>
			<p>Particulars of Claim (<s>attached</s>)(<s>to follow</s>)</p>
			<ol>
				<li>At all material times the Claimant carried out business as a car park management company.</li>
				<li>The Defendant is contracted to manage the car park located at <%=pcase(rs("name of site")) %> 
					("the Car Park"). The Car Park is private property and is monitored by the Claimant with automatic 
					number plate recognition ("ANPR") cameras. </li>
				<li>Drivers are permitted to park in the car park for <%=rs("vtime") %> minutes free of charge; thereafter a charge of 
					&pound;<%=rs("ticketprice") %> is payable for any further parking, however this charge is reduced to &pound;
					<%=rs("reducedamount") %> if a driver makes payment within 14 days. </li>
				<li>In the Car Park there are many clear and visible signs displayed advising drivers of the terms and charges 
					applicable when parking in the Car Park, as set out above in paragraph 3. These signs constitute an offer 
					by the Claimant to enter into a contract with drivers. </li>
				<li>On <%=rs("date of violation") %> the Claiman's APNR cameras recorded the Defendant's vehicle, registration 
					number <%=rs("registration") %>, in the Car Park. At <%=rs("arrival time") %> the Defendant's vehicle 
					was parked. The Defendant's vehicle left the Car Park at <%=rs("departure time") %>. The vehicle had been 
					parked in the Car Park for <%= formatdatetime(rs("duration of stay"),4) %>. </li>
				<li>When the Defendant parked their vehicle in the Car Park they accepted, by their conduct, the Claimant's 
					offer as set out in the signs. Consequently, a contract was formed between the Claimant and Defendant. </li>
				<li>As the Defendant parked their vehicle for longer than <%=rs("vtime") %> minutes a charge was incurred, 
					as set out in paragraph 3 above. </li>
				<li>The DVLA provided the Claimant with the Defendant&rsquo;s address on <%=rs("datereceived") %> and on 
					<%=rs("pcnsent") %> the Claimant sent the Defendant a parking charge notice, reference number 
					<%=rs("pcn") & rs("site") %>, requesting payment of the charge incurred. </li>
				<li>The Defendant has not paid the outstanding charge. </li>
				<li>Further the Claimant claims interest pursuant to section 69 of the County Courts Act 1984 on the amount 
					found to be due to the Claimant at such rate and for such period as the court thinks fit. </li>
			</ol>
			<p>AND the Claimant claims</p>
				<p>&nbsp;(1)&nbsp;&nbsp;&nbsp;Damages in the sum of &pound;<%=rs("ticketprice") %> </p>
				<p>&nbsp;(2)&nbsp;&nbsp;&nbsp;Interest pursuant to section 69 of the County Courts Act 1984 to be assessed.</p>
			<p>&nbsp;</p>
			<img src=claimpage2.jpg />
		
		<% closers rs

	end sub
	%>
</body>
</html>

<%

function checkifletteroutstanding(pcn)
	checkifletteroutstanding=0
	sql="select *  from incomingletters  where pcn='" & pcn & "' and (statusid=5 or statusid=6)"
	openrs rscl,sql
	if not rscl.eof and not rscl.bof then checkifletteroutstanding=1
	closers rscl
end function

function getletterstatus(statusid)

	sql="select * from letterstatus where statusid=" & statusid
	openrs rs,sql
	if not rs.eof and not rs.bof then	
		getletterstatus=rs("status")
	else
		getletterstatus=""
	end if
	closers rs
	
end function
	
sub shownotes(pcn)
	'respone.write "here"
	sql="select pcnnotes.id,pcn,mydate,ip,pcnnote,filereference,pdffile,type,useraccess.userid,clientusers.userid as clientuserid from pcnnotes left join useraccess on useraccess.id=pcnnotes.userid left join clientusers on clientusers.id=pcnnotes.userid where pcn=" & tosql(pcn,"text") & " order by id desc"
	openrs rs,sql
	if not rs.eof and not rs.bof then response.write "<table class=maintable2><tr><td colspan=2 align=center><b>Notes:</b></td></tr><tr><td><b>Date</b></td><td>Note</td><td>UserName</td><td>File Reference</td><td>Type</td></tr>"
	
	do while not rs.eof
		pcnnote= rs("pcnnote") 
		fileref=rs("filereference")
		st=rs("type")
		userid=rs("userid")
		if userid="" then userid=rs("clientuserid")
		response.write "<tr><td valign=top>" & rs("mydate") & "</td><td valign=top>" &pcnnote&  "</td><td>" &   userid & "</td><td>" & fileref & "</td><td>" &st & "</td></tr>"
		if rs("pdffile")<>"" then
			response.write "<tr><td colspan=4>"
			%>
			<EMBED src="pdfnotesfiles/<%=rs("pdffile")%>" width="450" height="350" "pdfnotesfiles/<%=rs("pdffile")%>"></EMBED>
			<%
			response.write "</td></tr>"
		end if
		rs.movenext
	loop
	
	
	
	sql="select mydatetime,comments,fileno,useraccess.userid as clientuserid from appeals left join useraccess on useraccess.id=appeals.userid  where comments is not null and  pcn=" & tosql(pcn,"text") & " order by appeals.id desc"
	'resposne.write sql 
	openrs rs,sql
	'if not rs.eof and not rs.bof then response.write "
	
	do while not rs.eof
		pcnnote= rs("comments") 
		fileref=rs("fileno")
		st="appeal"
		'userid=rs("userid")
		' if userid="" then 
		userid=rs("clientuserid")
		response.write "<tr><td valign=top>" & rs("mydatetime") & "</td><td valign=top>" &pcnnote&  "</td><td>" &   userid & "</td><td>" & fileref & "</td><td>" &st & "</td></tr>"
		response.write "</td></tr>"
		rs.movenext
	loop	
	
	if not rs.eof and not rs.bof then response.write "</table>"
	closers rs
		
	sqlap="select * from rm where pcn=" & tosql(pcn,"text")
	'response.write sqlap
	openrs rsap,sqlap
	if not rsap.eof and not rsap.bof then
		response.write "<table><tr><th colspan=2>Recorded Calls</th></tr>"
		do while not rsap.eof
			%>
			<tr><td colspan=2 align=center>Recorded Call on  <%=rsap("mydatetime") %> <a href=admin.asp?cmd=viewrm&id=<%=rsap("id") %> class=button target=_new>View Message</a></td></tr>
			<%
		rsap.movenext
		loop
	end if 
	closers rsap
end sub

%>