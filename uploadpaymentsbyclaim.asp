<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title></title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $("#txtPD").datepicker({
        "dateFormat": "dd-mm-yy",
        "minDate": -30,
        "maxDate": new Date()
    })
    .attr("readonly", true);
  });
      function validateForm(){
        if ($('#txtPD').val() != '') {
            return true;
        }
        else {
            alert ("Please enter a payment date.");
            return false;
          }
      }
  </script>
</head>
    <body>
<form method="post" ENCTYPE="multipart/form-data" action="uploadpaymentslistbyclaim.asp">

  <input type="file" name="File1"><br>
    <br>
    Date: &nbsp;<input type="text" name="paymentDate" id="txtPD" />
    <input type="submit" value="Upload and ADD Payments" onclick="return validateForm();"/>
</form>
     </body>
    </html>