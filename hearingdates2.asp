﻿<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="admininclude.asp"-->

<!doctype html>
<html lang="en">
<head>

<meta http-equiv="Content-type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" href="img/favicon.ico">
<link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/table.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables.js"></script>
<script type="text/javascript" src="js/jquery.jeditable.js"></script>
<script type="text/javascript" src="js/jquery.blockui.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $("#celebs");
        var oTable = table.dataTable({ "sPaginationType": "full_numbers",  "bStateSave": true });

        $(".editable", oTable.fnGetNodes()).editable("updatehearings.asp?cmd=edit", {
            "callback": function (sValue, y) {
                var fetch = sValue.split(",");
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(fetch[1], aPos[0], aPos[1]);
            },
            "submitdata": function (value, settings) {
                return {
                    "row_id": this.parentNode.getAttribute("id"),
                    "column": oTable.fnGetPosition(this)[2]
                };
            },
            "height": "14px"
        });

        $(document).on("click", ".delete", function () {
            var celeb_id = $(this).attr("id").replace("delete-", "");
            var parent = $("#" + celeb_id);
            $.ajax({
                type: "get",
                url: "updatenotice.asp?cmd=delete&id=" + celeb_id,
                data: "",
                beforeSend: function () {
                    table.block({
                        message: "",
                        css: {
                            border: "none",
                            backgroundColor: "none"
                        },
                        overlayCSS: {
                            backgroundColor: "#fff",
                            opacity: "0.5",
                            cursor: "wait"
                        }
                    });
                },
                success: function (response) {
                    table.unblock();
                    var get = response.split(",");
                    if (get[0] == "success") {
                        $(parent).fadeOut(200, function () {
                            $(parent).remove();
                        });
                    }
                }
            });
        });
    });
</script>
</head>
<body>

		Click in field directly to edit  - press enter to save the change  <div style="float:right"><a href="admin.asp" class="button">Back to Admin</a></div>
<br /><br />
	
        <%
		sql="select c.id, c.claim,c.pcn,date,court,dateofhearing from courthearings c left join violators v on v.pcn+v.site=c.pcn where v.pcn is null"
        openrs rs,sql

        %>
    
     <table class="table" id="celebs"><thead><tr><th>Claim</th><th>Date</th><th>Court</th><th>Date of Hearing</th><th>Time</th><th>Pcn</th><th>Printed Date</th><th>Owner</th></tr></thead><tbody>
        
        <% sql="select  c.id,c.claim,date,court,dateofhearing,time,c.pcn,printeddate,owner from  courthearings c left join violators v on v.pcn+v.site=c.pcn left join dvla d on d.id=v.dvlaid where v.pcn is not null"
            openrs rs,sql
            do while not rs.eof
            response.write "<tr id='" & rs("id") & "'><td class='editable'>" & rs("claim") & "</td><td>" & rs("date") & "</td><td>" & rs("court") & "</td><td>" & rs("dateofhearing") & "</td><td>" & rs("time") & "</td><td>" & rs("pcn") & "</td><td>" & rs("printeddate") & "</td><td>" & rs("owner") & "</td></tr>"
            rs.movenext
            loop
            closers rs
             

            %>


</tbody>

    </table>



       

	
</body>
</html>