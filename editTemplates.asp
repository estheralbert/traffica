  <!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="admininclude.asp"-->
<!--#include file="image.asp"-->
<%
    sql="exec gettags"

    openrs rstemplate,sql
	Dim Tags
	Tags = ""
	Dim OldTagTableName, FirstRun
	OldTagTableName=""
	TagTableName=""
	FirstRun = True
	
	If Not (rsTemplate.EOF And rsTemplate.BOF) Then
		While Not rsTemplate.EOF
			TagTableId = rsTemplate("Id")
			OldTagTableName = TagTableName
			TagTableName = rsTemplate("TagTableName")
			
			If OldTagTableName <> TagTableName Then
				If Not FirstRun Then
					Tags = Tags & "]},"
				End If
				FirstRun = False
				Tags = Tags & "{ text: '" &  rsTemplate("TagTableName") & "', menu: ["
			End If
			
			Tags = Tags & "{ text: '" & rsTemplate("TagName") & "', onclick: function(){ editor.insertContent('" & Replace(Replace(rsTemplate("TagName"),"<<","&lt;&lt;"),">>","&gt;&gt;") & "'); }},"
			
			rsTemplate.MoveNext
			
			
		Wend
		Tags = Tags & "]},"
	End If
	rsTemplate.Close

    PageTitle = "Template Editor"
%>
<!--#include file="adminheader.asp"-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script src="temjs/tinymce.js"></script>
	<script type="text/javascript">
        
        function fConfirmDelete(button) {
            var userAction = confirm("This will permanently remove this template\nOK to confirm");
            if (userAction)
                return true;
            else
                return false;
        }
        function validate() {
            
            txt = tinymce.get('edtrText').getContent();
            
            if ($('#txtName').val() == "") {
                alert("Please enter a template name.");
                return false;
            }
            if ($('#slctType').val() == "") {
                alert("Please select a letter type.");
                return false;
            }
            if ($('#slctBg').val() == "" && $('#lnkBg').length == 0) {
                alert("Please select a pdf file for the template background.");
                return false;
            }
            if (txt == "") {
                alert("Please enter a text for the template.");
                return false;
            }
            
            return true;
        }

        function openBgPopup(msg, bgid, sbmt) {
            qs = "?";
            if (bgid > 0) {
                qs = qs + "id=" + bgid + "&sbmt="+sbmt+"&";
            }
            qs = qs + "msg=" + msg;
            $('#myModal').modal('show').find('.modal-body').load('editTemplateBackgrounds.asp'+qs);            
        }

        $(document).ready(function () {
            tinymce.init({
                selector: "textarea",
                toolbar: "code | undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor  | tags fontsizeselect",
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: '', // Needed for 3.x
                plugins: "textcolor,code",
                textcolor_map: [
                    "000000", "Black",
                    "993300", "Burnt orange",
                    "333300", "Dark olive",
                    "003300", "Dark green",
                    "003366", "Dark azure",
                    "000080", "Navy Blue",
                    "333399", "Indigo",
                    "333333", "Very dark gray",
                    "800000", "Maroon",
                    "FF6600", "Orange",
                    "808000", "Olive",
                    "008000", "Green",
                    "008080", "Teal",
                    "0000FF", "Blue",
                    "666699", "Grayish blue",
                    "808080", "Gray",
                    "FF0000", "Red",
                    "FF9900", "Amber",
                    "99CC00", "Yellow green",
                    "339966", "Sea green",
                    "33CCCC", "Turquoise",
                    "3366FF", "Royal blue",
                    "800080", "Purple",
                    "999999", "Medium gray",
                    "FF00FF", "Magenta",
                    "FFCC00", "Gold",
                    "FFFF00", "Yellow",
                    "00FF00", "Lime",
                    "00FFFF", "Aqua",
                    "00CCFF", "Sky blue",
                    "993366", "Red violet",
                    "FFFFFF", "White",
                    "FF99CC", "Pink",
                    "FFCC99", "Peach",
                    "FFFF99", "Light yellow",
                    "CCFFCC", "Pale green",
                    "CCFFFF", "Pale cyan",
                    "99CCFF", "Light sky blue",
                    "CC99FF", "Plum",
                    "E7030D", "Custom Red"
                ],
               // valid_elements: '*',
             //  valid_styles: '*',
                setup: function (editor) {
                    editor.addButton('tags', {
                        type: 'menubutton',
                        text: 'Tags',
                        icon: false,
                        menu: [<%=Tags %>],
                    });
                }
            })
        });
</script>

<style type="text/css">
    .lbl {display:inline-block;margin-left:20px;clear:left;}
    .inpt {display:inline-block;clear:right;}
    .btn {    }
</style>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>          
        </div>
        <div class="modal-body" style="padding:40px 50px;"></div>
        <div class="modal-footer"></div>
      </div>      
    </div>
  </div> 
<div style="width:1000px;margin:50px auto;">
<%
popupmsg = request("popupmsg")
bgid = request("backgroundid")
sbmt = request("sbmt")
if bgid = "" then bgid = 0
if popupmsg <> "" or cint(bgid) > 0 then
    %>
        <script type="text/javascript">openBgPopup('<%=popupmsg %>',<%=bgid%>, '<%=sbmt%>');</script>
    <%
end if
msg = request("msg")

if request("sbmt") = "Delete" and request("id")&"" <> "" and isnumeric(request("id")) then
    sql = "delete from lettertemplates where id = " & tosql(request("id"),"Number")
    objconn.execute sql
    msg = "deleted"
end if

if request("sbmt") = "Update" and request("id")&"" <> "" and isnumeric(request("id")) then
        templateID = request("id")
        templateName = request("name")
        typeID = request("typeid")
        fontsize = request("fontsize")
        templateTxt = request("editor")
        bgID = request("bg")
        sql = "UPDATE letterTemplates SET templateName = " & tosql(templateName,"Text") & ", templateLetterTypeId = " & tosql(typeID,"Number") & ",fontsize = " & tosql(fontsize,"Number") & ", letterText = " & tosqlta(templateTxt,"Text") & ", backgroundid = " & tosql(bgID,"Number") & " WHERE id = " & tosql(templateID,"Number")
        objconn.execute sql
        msg = "updated"
end if

if request("sbmt") = "Add" then
        templateName = request("name")
        typeID = request("typeid")
        fontsize = request("fontsize")
        templateTxt = request("editor")
        bgID = request("bg")
        sql = "INSERT INTO letterTemplates (templateName, templateLetterTypeId, backgroundid, fontsize, letterText) VALUES (" & tosql(templateName,"Text") & "," & tosql(typeID,"Number") & "," & tosql(bgID,"Number") & "," & tosql(fontsize, "Number") & "," & tosql(templateTxt,"Text") & ")"
        'response.write "Adding: " & sql 
        objconn.execute sql
        msg = "added"
end if

if msg&"" <> "" then
%>
    <h3>Template <%=msg%>.</h3>
<%
end if

if request("sbmt") = "Edit" and request("id")&"" <> "" and isnumeric(request("id")) then
    sql = "select lt.id,templatelettertypeid,templatename,filename,backgroundname,backgroundid,isnull(fontsize,0) as fontsize,lettertext,lettertype,tablename,fieldname  from lettertemplates lt left join templateletters tl on lt.templatelettertypeid=tl.id left join templateBackgrounds tbg on tbg.id = lt.backgroundid where lt.id = " & tosql(request("id"),"Number")
    openrs rs, sql
    lettertext=rs("lettertext")
    'response.write  "LT:" & lettertext

    if not isnull(lettertext) then lettertext=Server.HTMLEncode(" " & lettertext)
    'lettertext=rs("lettertext")
    lettertext=replace(lettertext,"&lt;","<")
    lettertext=replace(lettertext,"&gt;",">")
    tablename=rs("tablename")

    sqlpc="select  max(pcn) as pcn from " & tablename
    'response.write sqlpc
    openrs rspc,sqlpc
    if isnull(rspc("pcn")) then
        response.write "<strong>You must Add a PCN using import before you can test print</strong>"
    else
    'lettertext=rs("lettertext")
    %>
    <form method="get" target="_new" action="http://traffica2.cleartoneholdings.co.uk/printlettern.asp" style="width:400px;padding:20px;border:1px solid #000;margin-bottom:50px;">
        <input type="hidden" name="pcn" value="<%=rspc("pcn")%>" />
        <input type="hidden" name="templateid" value="<%=request("id") %>" />
      
        <input type="submit" value="Print Demo" />
        Please note you must save this first below befoere you Print the Demo
    </form>
    <% 
        end if
        closers rspc %>
    <form method="post" action="editTemplates.asp">
        <input type="hidden" name="id" value="<%=rs("id")%>" />
        <span>Template Name</span>
        <input type="text" name="name" class="inpt" value="<%=rs("templateName")%>" id="txtName"/>
        <span class="lbl">Letter Type</span>
        <select name="typeid" class="inpt" id="slctType">
            <option value="">Select</option>
            <%
                sql = "SELECT id,lettertype FROM templateletters ORDER BY lettertype"
                openrs rsTypes, sql
                do while not rsTypes.EOF
                if rs("templateLetterTypeId") = rsTypes("id") then slct = "selected" else slct = ""
                %>
                <option value="<%=rsTypes("id")%>" <%=slct%>><%=rsTypes("lettertype")%></option>
                <%
                rsTypes.movenext
                loop
                closers rsTypes
            %>
        </select>
        <span class="lbl">Font size</span>
        <select name="fontsize">
            <option value="">Select</option>
            <%
                For i=8 To 36 Step 2                    
                    if rs("fontsize") = i then slctd = "selected" else slctd = ""
                    %>
                        <option value="<%=i%>" <%=slctd%>><%=i%></option>
                    <%
                Next
            %>
        </select>
        <br /><br />
        Template Background:&nbsp;&nbsp;
        <%if rs("filename")&"" <> "" then response.write "<a id='lnkBg' target='_blank' href='\pdfbackgrounds\" & rs("filename") & "'>"&rs("backgroundname")&"</a>"%> 
        <select name="bg" id="slctBg">
            <option value="">Select</option>
            <%
                openrs rsbg, "SELECT * FROM templateBackgrounds ORDER BY backgroundname"
                do while not rsbg.eof
                    if rsbg("id") = rs("backgroundid") then slctd = "selected" else slctd = ""
                    %>
                        <option value="<%=rsbg("id")%>" <%=slctd%>><%=rsbg("backgroundname")%></option>
                    <%
                    rsbg.movenext
                loop
                closers rsbg
            %>
        </select>&nbsp;&nbsp;
        <a onclick="openBgPopup('',0,'');">Edit Template Backgrounds</a>
        <br /><br />
        Letter Text: <br />
        <textarea name="editor" style="width:1000px;height:1000px;" id="edtrText"><%=lettertext%></textarea>
        <br />
        <input type="submit" name="sbmt" value="Update" class="btn" onclick="return validate();"/>
        <input type="submit" name="sbmt" value="Delete" class="btn" onclick="return confirm('Are you sure?');"/>
    </form>



    <br /><br />
    <a class="button" href="uploadletterpcn.asp?templateid=<%=request("id") %>">Upload PCN's for this letter</a>
    <%
    closers rs
else
    %>
    <form method="post" action="editTemplates.asp">
        Select a Template to Edit:
        <select name="id">
            <option value="">Select</option>
            <%
            sql = "SELECT id,templateName FROM letterTemplates ORDER BY templateName"
            openrs rs, sql
            do while not rs.EOF
            %>
            <option value="<%=rs("id")%>"><%=rs("templateName")%></option>  
            <%
            rs.movenext
            loop
            %>
        </select>
        <input type="submit" name="sbmt" value="Edit" />&nbsp;&nbsp;
        <input type="submit" name="sbmt" value="Delete" onclick="return confirm('Are you sure?');"/>
       </form>
    Or Add New:
    <hr />
    <form method="post" action="editTemplates.asp" >
        <span>Template Name</span>
        <input type="text" name="name" class="inpt" id="txtName"/>
        <span class="lbl">Letter Type</span>
        <select name="typeid" class="inpt" id="slctType">
            <option value="">Select</option>
            <%
                sql = "SELECT id,lettertype FROM templateletters ORDER BY lettertype"
                openrs rsTypes, sql
                do while not rsTypes.EOF
                %>
                <option value="<%=rsTypes("id")%>"><%=rsTypes("lettertype")%></option>
                <%
                rsTypes.movenext
                loop
                closers rsTypes
            %>
        </select>
        <span class="lbl">Font size</span>
        <select name="fontsize">
            <option value="">Select</option>
            <%
                For i=8 To 36 Step 2                    
                    %>
                        <option value="<%=i%>" <%=slctd%>><%=i%></option>
                    <%
                Next
            %>
        </select>
        <br /><br />
        Template Background:&nbsp;&nbsp;
        <select name="bg" id="slctBg">
            <option value="">Select</option>
            <%
                openrs rsbg, "SELECT * FROM templateBackgrounds ORDER BY backgroundname"
                do while not rsbg.eof
                    %>
                        <option value="<%=rsbg("id")%>"><%=rsbg("backgroundname")%></option>
                    <%
                    rsbg.movenext
                loop
                closers rsbg
            %>
        </select>&nbsp;&nbsp;
        <a onclick="openBgPopup('',0,'');">Edit Template Backgrounds</a>        
        <br /><br />
        Letter Text: <br />
        <textarea name="editor" style="width:1000px;height:1000px;" id="edtrText"></textarea>
        <br />
        <input type="submit" name="sbmt" value="Add" class="btn" onclick="return validate();"/>
    </form>   
    
</div>
<%end if %>

</body>
</html>
    