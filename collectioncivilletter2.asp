﻿<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<%
id=request("id")
sql="select * from VIOLATORS v left join [Site Information] s  on s.sitecode=v.site left join dvla d on d.id=v.dvlaid where v.ID=" & id 
openrs rs,sql



 %>
<BR style="page-break-before: always"> 

<table align=right width=300 border=1>

<tr><td><strong>Claim No</strong></td><td width=75%>&nbsp;</td></tr>

</table>

<br /><br />
Does, or will, your claim include any issues under the Human Rights Act 1998? Yes  <input type=checkbox /> &nbsp;
No  <input type=checkbox checked />
<br /><br />
Particulars of Claim (attached)(to follow)<br />

<br>1. At all material times the Claimant carried out business as a car park 
management company. 
<br>
<br>2. The Defendant is contracted to manage the car park located at <%=pcase(rs("name of site")) %> ("the Car Park"). The Car Park is private property and is monitored by 
the Claimant with automatic number plate recognition ("ANPR") cameras. 
<br>
<br>3. Drivers are permitted to park in the car park for <%=rs("vtime") %> minutes 
free of charge; thereafter a charge of &pound;<%=rs("ticketprice") %> is payable for any 
further parking, however this charge is reduced to &pound;<%=rs("reducedamount") %> if 
a driver makes payment within 14 days. 
<br>
<br>4. In the Car Park there are many clear and visible signs displayed advising 
drivers of the terms and charges applicable when parking in the Car Park, as set 
out above in paragraph 3. These signs constitute an offer by the Claimant to 
enter into a contract with drivers. 
<br>
<br>5. On <%=rs("date of violation") %> the Claiman's APNR cameras recorded the 
Defendant's vehicle, registration number <%=rs("registration") %>, in the 
Car Park. At <%=rs("arrival time") %> the Defendant's vehicle was parked. The Defendant's 
vehicle left the Car Park at <%=rs("departure time") %>. The vehicle had been parked in the 
Car Park for <%= formatdatetime(rs("duration of stay"),4) %>. 
<br>
<br>6. When the Defendant parked their vehicle in the Car Park they accepted, by 
their conduct, the Claimant's offer as set out in the signs. Consequently, a 
contract was formed between the Claimant and Defendant. 
<br>
<br>7. As the Defendant parked their vehicle for longer than <%=rs("vtime") %>
minutes a charge was incurred, as set out in paragraph 3 above. 
<br>
<br>8. The DVLA provided the Claimant with the Defendant&rsquo;s address on <%=rs("datereceived") %> and on <%=rs("pcnsent") %> the Claimant sent the Defendant a parking charge 
notice, reference number <%=rs("pcn") & rs("site") %>, requesting payment of the charge 
incurred. 
<br>
<br>9. The Defendant has not paid the outstanding charge. 
<br>
<br>10. Further the Claimant claims interest pursuant to section 69 of the County 
Courts Act 1984 on the amount found to be due to the Claimant at such rate and 
for such period as the court thinks fit. 
<br>
<br>AND the Claimant claims 
<br>
<br>(1) Damages in the sum of &pound;<%=rs("ticketprice") %> 
<br>(2) Interest pursuant to section 69 of the County Courts Act 1984 to be 
assessed.

<br /><br /><Br>
<img src=claimpage2.jpg />


<% closers rs %>