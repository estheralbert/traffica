﻿<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<%


id=request("id")
sql="select * from violators where id=" & id 
openrs rs,sql

    sqldvla="select * from dvla where ID=" & rs("dvlaid")
    openrs rsdvla,sqldvla
    owner=rsdvla("owner")
    address1=rsdvla("address1")
    address2=rsdvla("address2")
    address3=rsdvla("address3")
    town=rsdvla("town")
    postcode=rsdvla("postcode")
    
    closers rsdvla
    
    sqlrem="select reminder from reminders where pcn=" & tosql(rs("pcn"),"text")
    openrs rsrem,sqlrem
    reminderdate=rsrem("reminder")
    closers rsrem

 %>
<div style="font-size:13px;font-family:Calibri;">
<P>
<P><B>
<%
response.Write owner & "<bR>"
if address1<>"" then response.Write address1 & "<br>"
if address2<>"" then response.Write address2 & "<br>"
if address3<>"" then response.Write address3 & "<br>"
if town<>"" then response.Write town & "<br>"
if postcode<>"" then response.Write postcode & "<br>"

 %>




</B> 
<P><B>CIVIL ENFORCEMENT LIMITED -v- <%=owner %></B> 
<P>Despite sending you a Reminder Notice on <B><%=formatdatetime(reminderdate,2) %></B> your debt remains 
outstanding.<B> </B>As a result of your continued failure to settle the amount, 
we now confirm that your liability to us has now increased as follows:- 
<P>Debt as of <%=date %> &pound;<%=formatnumber(rs("ticketprice"),2) %> 
<P>Additional Costs &pound;115.00 
<P><B>Payment Now due &pound;<%= formatnumber(rs("ticketprice")+115,2)%></B> 
<P>In view of the above we have unfortunately been left with no alternative but 
to instruct solicitors to draft Particulars of Claim (see attached) which we 
intend to submit to Northampton County Court on <B><%

paymentdate=dateadd("d",8,date())
paymentdate1=dateadd("d",7,date())
if weekday(paymentdate)=1 then paymentdate=dateadd("d",9,date())
if weekday(paymentdate)=7 then paymentdate=dateadd("d",10,date())
response.Write paymentdate

 %>.</B> 
<P><B>Please be aware that our representatives will be seeking full costs of the 
application together with all our legal fees in dealing with this matter.</B> 
<P>Once a Judgment is obtained the options available to us is as follows:- 
<P>&middot; A Warrant of Execution 
<P>&middot; Appoint Court Bailiffs to attend your property 
<P>&middot; Seize your vehicle / goods 
<P>&middot; Apply for an attachment of earning (see information below) 
<P>&middot; Third Party debt Order (see information below) 
<P>If you are employed we may make an application for an <B>attachment of 
earnings order </B>which will be sent to your employer. It will ensure that your 
employer will take an amount from your earnings each pay day and send it to a 
collection office which will then be forwarded to us. 
<P>
<P>We may also make an application for a <B>thirdparty debt order </B>which is 
usually made to stop the debtor from taking money out of his or her bank or 
building society account. The money we are owed will be paid to us from your 
account. 
<P>
<P>As this matter remains unresolved costs are simply going to escalate. 
However, we appreciate the level of your debt and are therefore prepared to 
accept without prejudiced a reduced amount of &pound;<%=formatnumber(rs("ticketprice"),2) %> as full and final 
settlement subject to such payment being made by no later than  <%=paymentdate1 %>
<P>Please note, should you not accept our reduced offer we intend to show this 
letter to the Court in respect of our costs. <B>Payments of the reduced amount 
can only be made by calling 0115 822 5023. </B>
</div>
<% closers rs %>