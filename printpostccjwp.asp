﻿<!--#include file="openconn.asp"-->
<!--#include file="common.asp"-->
<%
    server.ScriptTimeout=100000
    batchId =  request("batchid")
    idebug = Request("debug")

    if request("debug")="true" or request("debug")=1 then idebug = 1 else idebug = 0 end if

    Set ws = server.CreateObject("WScript.Network.1")
    strUser = "DSQ8003-12\office"
    strPass = "Creative48"

    on error resume next
    ws.RemoveNetworkDrive "k:",true
    on error goto 0

   ' on error resume next
    ws.MapNetworkDrive "k:", "\\fileserver.ccarpark.nsdsl.net\incomingletters",False,struser,strpass

    Set dicTemplates    = Server.CreateObject("Scripting.Dictionary")

    Set Pdf             = Server.CreateObject("Persits.PDF")
    Set objParam        = Pdf.CreateParam("size=12; alignment=left; color=black; spacing=1.1;")

    Set objDocfinal     = Pdf.CreateDocument()

    Set docLetter       = Pdf.OpenDocument("C:\inetpub\wwwroot\traffica\postccjwpLetter.pdf" )
    Set docAttachment   = Pdf.OpenDocument("C:\inetpub\wwwroot\traffica\AttachmentCcjEnforcementLetterFlat.pdf")  

    PageWidth   = 595.32
    PageHeight  = 841.92

    '--- Add Blank Page ---
    call docLetter.Pages.Add(PageWidth, PageHeight)

    PrintBatch(batchId) '5441

    function PrintBatch(batchId)
        StartTime = Timer()

        isBatch = true

        if batchid <> "" then
	        sqlSelect = "select * from postccjwp where batchid = " & batchid

	        sql = "insert into dbo.reportsscheduled(sql,emailto,subject) " & _
                    "values('batchSummary @batchId = " & batchid & ",@tablename	= ''wplletters'''," &_
                    "'Simon.a@bemrosemobile.co.uk,esther@creativecarpark.co.uk,ross@creativecarpark.co.uk'," &_
                    "'WPL Letters new Batch Summary for batch " & batchid & " ' )"
        else
            sqlSelect = "select top(1) * from postccjwp where pcn='9752453652'" '9866003223'" '3531431884 
            isBatch = false
        end if

        if idebug =  1 then Response.write "<br />" & sqlSelect

        n=1
        objconn.cursorlocation = 3 ''adUseClient
        set  rsB = objconn.execute(sqlSelect)
        do while not rsB.eof
            pcn         = rsB("pcn")
            datesent    = rsB("datesent")

            if idebug =  1 then Response.Write "<br/>" & n & " - Processing PCN: " & pcn & "<br>"

            fullpath = PrintCurrentLetter(pcn, datesent)

            if idebug =  1 then Response.Write "<br/>------ Finished Processing PCN: " & pcn & "-----------<br/>"
            n = n + 1
            rsB.movenext
        loop

        if isBatch then
            fileName = "pwpletter_" & batchid & ".pdf"
            objDocfinal.Save  "C:\inetpub\wwwroot\traffica\cprletters\" & fileName
            finalUrl = "cprletters\" & fileName

            if idebug = 1 Then
                Response.write "<hr><a href=""" & finalUrl & """ target=""_blank"">View Complete PDF for <strong>Batch Id: " & batchId & "</strong></a>" 
            Else
                Response.redirect  finalUrl
            End If
        else
            fileName = "pwpletter_.pdf"
            objDocfinal.Save  "C:\inetpub\wwwroot\traffica\cprletters\" & fileName
            finalUrl = "cprletters\" & fileName

            'relPath = replace(mid(fullpath,3),"\","/")

            Response.write "<hr><a href=""" & finalUrl & """ target=""_blank"">View Complete PDF for <strong>PCN: " & pcn & "</strong></a>" 
        end if
        EndTime = Timer()
        if idebug=1 then  Response.Write("<hr>Total Time taken in Seconds to 2 decimal places: " & FormatNumber(EndTime - StartTime, 2))
    
    End Function

    Function PrintCurrentLetter(pcn,datesent)

	    sql = "exec getpostccjwpdetails @pcn=" & tosql(pcn,"text")
	    if idebug = 1 Then Response.Write sql & "<br>"
	    openrs rs,sql

    	siteCode        = rs("siteCode")
	    owner           = rs("owner")
	    address1        = rs("address1")
	    address2        = rs("address2")
	    address3        = rs("address3")
	    town            = rs("town")
	    postcode        = rs("postcode")
	    ticketprice     = rs("ticketPrice")
	    priceBefore     = rs("priceBeforeLetter")
        registration    = rs("registration")
        dateOfViolation = rs("dateOfViolation")
        dateOfJudgement = rs("dateJudgementObtained")
        claim           = rs("claim")

	    priceBeforeFmt  = FormatNumber(priceBefore,2)
        ticketpriceFmt  = FormatNumber(ticketprice,2)
        siteName        = getsitename(rs("siteCode"))

        longDate    = Day(datesent) & " " & MonthName(Month(datesent)) & " " & Year(datesent) 
        shortDateJudgement    = FormatDateTime(dateOfJudgement,2) 

        set objDoc          = Pdf.CreateDocument()
	    Set objFont         = objDoc.Fonts("Times-Roman")
	    Set objFontBold     = objDoc.Fonts("Times-Bold")

        set objTemplate1 = objDoc.CreateGraphicsFromPage( docLetter, 1 )
        set objTemplate2 = objDoc.CreateGraphicsFromPage( docLetter, 2 )
        set objTemplate3 = objDoc.CreateGraphicsFromPage( docAttachment, 1 )
        set objTemplate4 = objDoc.CreateGraphicsFromPage( docAttachment, 2 )
        
        '--- Page 1 -----
        Set objPage         = objDoc.Pages.Add(pageWidth, pageHeight)
        Set objCanvas       = objPage.Canvas

        objCanvas.DrawGraphics objTemplate1, "x=0; y=0"

        '---- Keep needed by printer ----
        objCanvas.DrawText   pcn, "x=5, y=820; size=15; alignment=left; color=white", docLetter.Fonts("Arial") 
        '---------------------------------

	    Dim ownerAddress(5)
	    ownerAddress(0) = owner
	    ownerAddress(1) = address1
	    ownerAddress(2) = address2
	    ownerAddress(3) = address3
	    ownerAddress(4) = town
	    ownerAddress(5) = postcode

	    ownerNameAddress = ""

	    For Each rowVal In ownerAddress

		    If rowVal <> "" Then
			    ownerNameAddress = ownerNameAddress & rowVal & vbCrLf
		    End If

	    Next

        Call drawMultilineText(ownerNameAddress, 73, 756, 377, objParam, objCanvas, objFont, 1)

        '400,700
        Dim arrX
        Dim arrY
        Dim arrText
        arrText = Array( _
                "Case No. " & claim, _
                "PCN: " & pcn )

        ' Go over all items in arrays
        objParam("x") = 73
        For i = 0 to UBound(arrText)
            if idebug = 1 Then Response.Write i & " - " & arrText(i) & "<br>"
            objParam("y") = 624 - (i * 30)

            ' Draw text on canvas
            objCanvas.DrawText arrText(i), objParam, objFontBold
        Next ' i

        objParam("x") = 99
        objParam("y") = 566
        objParam.Add("html=true")
        objCanvas.DrawText owner & ",", objParam, objFont

        objParam("x") = 88
        objParam("y") = 522
        objCanvas.DrawText shortDateJudgement & " in the sum of <b>£" & priceBeforeFmt & "</b>." , objParam, objFont

        objParam("x") = 341
        objParam("y") = 405
        objCanvas.DrawText "<b>£265.00</b>, which will be added to the", objParam, objFont

        objParam("x") = 233
        objParam("y") = 317
        objCanvas.DrawText "<b>£" & ticketprice & "</b> in full and final settlement of the outstanding Judgment", objParam, objFont


        objParam("x") = 145
        objParam("y") = 682
        objParam.Add("html=false")
        'objParam.Add("color=black")
        objParam("Alignment") = 1
        objCanvas.DrawText longDate, objParam, objFont

        If mydebug = 1 Then
            Response.Write "Page.Height: " & Page.Height & "<br>"
            Response.Write "Page.Width: " & Page.Width & "<br>"
        End If

        '--- Page 2 ---
        call objDoc.Pages.Add(pageWidth, pageHeight)


        '--- Page 3 ---
        Set objPage         = objDoc.Pages.Add(pageWidth, pageHeight)
        Set objCanvas       = objPage.Canvas

        objCanvas.DrawGraphics objTemplate3, "x=0; y=0"

        objParam("Alignment") = 0

        courtName           = ""
        feeAcctNum          = ""
        AppnNum             = ""
        ClaimantName        = "CIVIL ENFORCEMENT LIMITED" 
        JudgementDayMonth   = Day(dateOfJudgement) & " " & MonthName(Month(dateOfJudgement))  
        JudgementYear       = Right(Year(dateOfJudgement),2)
        Unknown             = "Unknown"
        blank               = ""

        Dim street(2)
        street(0) = address1
        street(1) = address2
        street(2) = address3

        streetText = ""

	    For Each rowVal In street

		    If rowVal <> "" Then
                if len(streetText) > 0 Then streetText = streetText & ", "
			    streetText = streetText & rowVal 
		    End If

	    Next

        Dim arrText2
        arrText2 = Array( _
                courtName, _    
                claim, _
                feeAcctNum, _
                AppnNum, _
                ClaimantName & " / " & pcn, _
                Owner, _
                JudgementDayMonth, _
                JudgementYear, _
                ClaimantName, _
                claim, _
                Owner, _
                streetText, _
                town, _
                postcode, _
                priceBeforeFmt, _
                priceBeforeFmt _
         )

        arrX = Array( _
                300, _    
                448, _
                300, _
                425, _
                300, _
                300, _
                330, _
                475, _
                70, _
                345, _
                170, _
                142, _
                60, _
                425, _
                350, _
                234 _
         )

        arrY = Array( _
                807, _    
                807, _
                763, _
                740, _
                702, _
                659, _
                585, _
                585, _
                567, _
                567, _
                515, _
                495, _
                478, _
                478, _
                441, _
                423 _
         )

        For i = 0 to UBound(arrX)
          objParam("x") = arrX(i)
          objParam("y") = arrY(i) 

          if idebug = 1 Then Response.Write arrText2(i) & "<br>"

          ' Draw text on canvas
          objCanvas.DrawText arrText2(i), objParam, objFont
        Next ' i

        'Draw lines
        Y = 611
        objCanvas.DrawLine 99,Y,150,Y
        objCanvas.DrawLine 474,Y,520,Y

        '---- Page 4 ----
        Set objPage         = objDoc.Pages.Add(pageWidth, pageHeight)
        Set objCanvas       = objPage.Canvas

        objCanvas.DrawGraphics objTemplate4, "x=0; y=0"

        '--- Add watermark ---
        Set WaterMark       = objDoc.OpenImage("C:\inetpub\wwwroot\traffica\DraftWaterMark.gif")
        p = 1
        For Each Page in objDoc.Pages
            If p > 2 Then Page.Background.DrawImage WaterMark, "x=1, y=1; scalex=1; scaley=1"
            p = p + 1
        Next

        '--  finished filling in data

        '--- save files 
        newpath     = "k:\cprlettersnew\" & year(datesent) 
        checkdirectory newpath

        path        = newpath & "\pwp" & pcn & ".pdf"

        if idebug = 1 then Response.Write "<br>Saving to memory"
        Set objDocBinary = Pdf.OpenDocumentBinary(objDoc.SaveToMemory )

        if idebug = 1 then Response.Write "<br>Appending to final"
        objDocfinal.AppendDocument objDocBinary  

        if idebug = 1 then Response.Write "<br>Saving to file"
        FileName = objDocBinary.Save(path, true)

        Set objPage = Nothing
        Set objDoc = Nothing

        if idebug =  1 then Response.write "<br>filename: " & filename

        PrintCurrentLetter = path    
    End Function
        
%>