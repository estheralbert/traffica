<%Response.Buffer = true%> 

<!--#include file ="openconn.asp"-->
<!--#include file ="config.asp"-->
<!--#include file ="common.asp"-->

<%
    
    
    sql="select * from Reminderspofa r left join violators v on v.pcn=r.pcn left join dvla d on d.id=v.dvlaid left join [site information] s on s.sitecode=r.site where r.id=" & request("id")
    openrs rs,sql


    i=0
     ' do while not rs.eof 
    
     %>

<% if i>0  then  %>
<BR style="page-break-before: always"> 
<% 
    i=i+1
    end if %>
<br />
<P><%=formatdatetime(rs("reminderdate"),2) %>
<P>
<P>
<P>
<P>Dear <%=rs("owner") %>, 
<P>
<P>PCN Number: <%=rs("pcn") & rs("site") %>
   <br />Vehicle: <%=rs("registration") %>
<br>Location: <%=rs("shortdesc") %>
<br />Date of Event: <%=rs("date of violation") %>
<br />Date of PCN Issue: <%=formatdatetime(rs("pcnsent"),2) %> 
<br />Amount Due: &pound;<%=rs("ticketprice") %> 
    </P>
<P>
<P>It has been <B><%=datediff("d",rs("pcnsent"),date()) %> </B>days 
since we sent you the above Parking Charge Notice (PCN). 
<P>
<P>In accordance with Schedule 4 of the Protection of Freedoms Act as the 
Registered Keeper of the vehicle you are now liable for the unpaid part of the 
PCN. 
<P>
<P><B><U>Reasons </U></B>
<P>
&middot; You have not provided the correct name together with a current postal 
address for the driver; or 
<br />&middot; No information has been provided; or 
<br />&middot; You have identified someone who has denied they were the driver at the time 
of the parking event or has not responded at the address given; or 
<br />&middot; You are a hire or leasing company and you have not provided the full 
documentation and information required by the Act in order for us to reissue the 
Parking Charge. 
</P>
<P>We require full payment of <B>&pound;<%=rs("ticketprice") %></B> from you within 14 
days. To make a payment, please call 0115 822 5020, go online to 
www.ce-service.co.uk, or send a cheque to the above address. 
</P>
<P>Please be advised that the independent appeals service (POPLA) is no longer 
available for this PCN. If you wish to appeal and/or raise any concerns with us 
directly please contact us directly within the next 14 days. 
</P>
<P>If this letter is ignored, further action may be taken to secure payment, 
which could include issuing court proceedings or instructing a debt recovery 
company to recover the debt due &ndash; both of which could incur further costs. 
</P>
<P>
<P>Yours faithfully, 
<P>
<P>
<P>Civil Enforcement Ltd
<%
'rs.movenext 
 '   loop
%>