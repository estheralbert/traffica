  <!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="admininclude.asp"-->
<!--#include file="image.asp"-->
<%  
    PageTitle = "Template Background Editor"

msg = request("msg")

if request("sbmt") = "Delete" and request("id")&"" <> "" and isnumeric(request("id")) then
    sql = "delete from templateBackgrounds where id = " & tosql(request("id"),"Number")
    objconn.execute sql
    msg = "deleted"
end if

if msg&"" <> "" then
%>
    <h3>Background <%=msg%>.</h3>
<%
end if
if request("sbmt") = "Edit" and request("id")&"" <> "" and isnumeric(request("id")) then

    openrs rs, "SELECT * FROM templateBackgrounds WHERE id = " & tosql(request("id"),"Number")
%>
	<script type="text/javascript">
       
        function validateBG() {
            
            if ($('#txtBgName').val() == "") {
                alert("Please enter a template name.");
                return false;
            }
            
            if ($('#flPdfBg').val() == "" && $('#lnkBg').length == 0) {
                alert("Please select a pdf file for the template background.");
                return false;
            }
            
            return true;
        }

        $(document).ready(function () {
            
        });
</script>

<style type="text/css">
    .lbl {display:inline-block;margin-left:20px;clear:left;}
    .inpt {display:inline-block;clear:right;}
    .btn {    }
</style>

    <form method="post" action="processBackgroundUpload.asp" ENCTYPE="multipart/form-data">
        <input type="hidden" name="id" value="<%=rs("id")%>" />
        <span>Background Name</span>
        <input type="text" name="bgname" class="inpt" value="<%=rs("backgroundname")%>" id="txtBgName"/>        
        <span class="lbl">PDF File</span>
        <%if rs("filename")&"" <> "" then response.write "<a id='lnkBg' target='_blank' href='\pdfbackgrounds\" & rs("filename") & "'>"&rs("filename")&"</a>"%> 
        <input type="file" name="pdfBG" class="inpt" id="flPdfBg"/> 
        <br /><br />
        <input type="submit" name="sbmt" value="Update" class="btn" onclick="return validateBG();"/>
        <input type="submit" name="sbmt" value="Delete" class="btn" onclick="return confirm('Are you sure?');"/>
    </form>

    
    <%
    closers rs
else
    %>
    <form method="post" action="editTemplates.asp">
        Select a Background to Edit:
        <select name="backgroundid">
            <option value="">Select</option>
            <%
            sql = "SELECT id,backgroundname FROM templateBackgrounds ORDER BY backgroundname"
            openrs rs, sql
            do while not rs.EOF
            %>
            <option value="<%=rs("id")%>"><%=rs("backgroundname")%></option>  
            <%
            rs.movenext
            loop
            %>
        </select>
        <input type="submit" name="sbmt" value="Edit" />&nbsp;&nbsp;
        <input type="submit" name="sbmt" value="Delete" onclick="return confirm('Are you sure?');"/>        
       </form>
        <hr />
    Or Add New:    
    <form method="post" ENCTYPE="multipart/form-data" action="processBackgroundUpload.asp" >
        <span>Background Name</span>
        <input type="text" name="bgname" class="inpt" id="txtBgName"/>
        <br /><br />
        <span>PDF File</span>
        <input type="file" name="pdfBG" class="inpt" id="flPdfBg"/>
        <br />
        <input type="submit" name="sbmt" value="Add" class="btn" onclick="return validateBG();"/>
    </form>   
    
</div>
<%end if %>

</body>
</html>
    