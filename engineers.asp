  <% Server.ScriptTimeout = 3600000 %>
		<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%
 pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 



%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<html>
<title>Traffic Guard Engineers</title>
<LINK 
href="engstylesheet.css" rel=stylesheet type=text/css>
<body>
<div id="engineers">
<%
cmd=request("cmd")
select case cmd
'case "adminmenu"
'	call adminmenu
case "validate"
	call validatelogin
case "siteissues"
	call checklogin
	call siteissues
case "openissue"
	call checklogin
	call openissue
case "startissue"
	call checklogin
	call startissue
case "fixed"
	call checklogin
	call fixed
case "addnote"
	call checklogin
	call addnote		
case "updatevisit"
    call checklogin
    call updatevisit		
case else
    call loginform	
end select	



%>
</div>
</body>
</html>


<%
sub checklogin
if session("eng")<>"ok" then
'
'response.Write "amdin not ok"
call loginform
response.end
end if
end sub

sub loginform %>
<form name=login method=post action=engineers.asp?cmd=validate>
<table class=maintable2>
<tr><td colspan=2 class=loginheader>Please Enter your Username <br>and Password</td></tr>
<tr><td>Username:</td><td><input type=text name=username size=8></td></tr>
<tr><td>Password:</td><td><input type=password name=password size=8></td></tr>
<tr><td colspan=2 align=center><input type=submit name=submit value=Submit class=button></td></tr>
</table>

</form>
<%end sub









sub validatelogin 'validate login and only let 3 tried.  Insert ip into log.

sql="exec spvalidate @username=" & tosql(request.form("username"),"text") & ",@password=" & tosql(request.form("password"),"text")
sql="select * from issueengineers where  username=" & tosql(request.form("username"),"text") & " and password=" & tosql(request.form("password"),"text")
'response.write sql
openrs rsadmin,sql
if rsadmin.eof and rsadmin.bof then
	response.write "<br>bad password"
	call loginform
else
	
	
	session("eng")="ok"
	session("id")=rsadmin("engineerid")
	session("engineername")=rsadmin("engineername")
	
	''check if has a open ticket and if so return go to that issue 
	sql="exec engcheckopen " & rsadmin("engineerid")
	openrs rs,sql
    if rs("visitid")<>0  then 
        session("visitid")=rs("visitid")
        response.redirect("engineers.asp?cmd=openissue&id=" & rs("issueid"))
    
    
    end if 
	closers rs
	
	
	call siteissues
end if	
end sub
sub siteissues
    sql="select siteissues.id,site,[Name of Site] as sitename,issue,startdate,startnotes,endnotes,notes, DATEDIFF(day,startdate, getdate()) as numdays from siteissues left join [site information] on siteissues.site=[site information].sitecode where enddate is null order by issue,site"
openrs rs,sql
if not rs.eof and not rs.bof then
 '  mytable= "<table class=maintable2 border=1><tr><th>Site</th><th>Name of site</th><th>Start Date</th><th>Issue</th><th>Notes</th></tr>"
   response.Write  "<table class=maintable2 border=1><tr><th>Site</th><th width=20>Name of Site</th><th>Issue</th><th>Days</th></tr>"
   i=0
    do while not rs.eof
      startnotes=rs("startnotes")
      
      if i=0 then
        mytable=mytable & "<tr>"
        i=1
      else
        mytable=mytable & "<tr bgcolor=#DDDDDD>"  
        i=0    
      end if
      response.write "<tr class=myrow>"
       mynotes=showsiteissuenotes(rs("id"))
       myrow= "<td><a href=engineers.asp?cmd=openissue&id=" & rs("id") & ">" & rs("site") & "</a></td><td width=10><a href=engineers.asp?cmd=openissue&id=" & rs("id") & ">" & rs("sitename") & "</a></td><td><a href=engineers.asp?cmd=openissue&id=" & rs("id") & ">" & rs("issue")& "</a></td><td><a href=engineers.asp?cmd=openissue&id=" & rs("id") & ">" & rs("numdays") & "</a></td>"
        response.Write myrow
        mytable=mytable & myrow
       
    
    rs.movenext
    loop

end if
closers rs
mytable=mytable & "</table>"
response.Write "</table>"
end sub
sub openissue
id=request("id")
session("issueid") = id
sql="select siteissues.id,site,[Name of Site] as sitename,issue,startdate,startnotes,endnotes,notes, DATEDIFF(day,startdate, getdate()) as numdays from siteissues left join [site information] on siteissues.site=[site information].sitecode where siteissues.id=" & id
'response.write sql
openrs rs,sql
%>
<table class=maintable2>
<tr><th>Issue</th><td><%=rs("issue")%></td></tr>
<tr><th>Site</th><td><%=rs("site")%></td></tr>
<tr><th>Notes</th></tr><tr><td colspan=2><%=showsiteissuenotes(rs("id"))%></td></tr>
<% if session("visitid")="" then %>
<tr><td><a href=engineers.asp?cmd=startissue&id=<%=rs("id")%> class=button onclick="return confirm('Have you arrived on site?')">Start Issue</a></td></tr> 
<% else %>
<tr><td><a href=engineers.asp?cmd=fixed&id=<%=rs("id")%> class=button>Fix</a></td><td><a href=engineers.asp?cmd=addnote&id=<%=rs("id")%> class=button>Add Note</td></tr>
<tr><td colspan=2>
<% sqlc="select * from engineervisits where visitid=" & session("visitid")
openrs rsc,sqlc
%>
<form method=post action=engineers.asp?cmd=updatevisit>
<input type=hidden name=id value="<%=request("id")  %>">
<table>
<tr><td>Hard Disk Space</td><td><input type=text name=harddiskspace value="<%=rsc("harddiskspace")%>"></td></tr>
<tr><td>Amount of Signs</td><td><input type=text name=amountofsigns value="<%=rsc("amountofsigns")%>"></td></tr>
<tr><Td><input type=submit name=submit value="Update" class="button"></td></tr></table></form>
<% closers rsc
%>

</td></tr>
<% end if %>
<tr><td><a href=engineers.asp?cmd=siteissues class=button>Back to Site Issues</a></td></tr>
</table>
<%
closers rs 
end sub
sub updatevisit
    harddiskspace=request("harddiskspace")
    amountofsigns=request("amountofsigns")
    visitid=session("visitid")
    sql="update engineervisits set harddiskspace=" & tosql(harddiskspace,"text") & ",amountofsigns=" & tosql(amountofsigns,"text") & " where visitid=" & visitid  
    objconn.execute sql
    call openissue
end sub
sub startissue
id=request("id")
sql="exec insertengineervisit @engineerid=" & tosql(session("id"),"Number") & ",@issueid=" & id
'response.write sql
openrs rs,sql
session("visitid")=rs("visitid")
countme=rs("countme")
closers rs
if countme=0 then
    subject="On site - " & session("engineername")
    'body="Engineer " & session("engineername") & " has arrived at site at " & date()
    body="Arrival Time:" & date() & vbcrlf

    sql="select siteissues.id,site,[Name of Site] as sitename,issue,startdate,startnotes,endnotes,notes, DATEDIFF(day,startdate, getdate()) as numdays from siteissues left join [site information] on siteissues.site=[site information].sitecode where enddate is null and siteissues.id=" & session("issueid")
    'response.Write sql
    openrs rs,sql
    body=body &  vbcrlf
    body=body & "Site:" & rs("site") &  "-" & rs("sitename") & vbcrlf
    body=body & "Issue:" & rs("issue") & vbcrlf
    body=body & "Notes:" & rs("startnotes") & vbcrlf & rs("notes") & vbcrlf & rs("endnotes")
    call sendfaultreportingemail(subject,body)

end if
closers rs
response.write "issue opened"
call openissue
end sub
sub fixed
    if request("cmd2")="" then
    sql="select * from siteissues where id=" & request("id")
  
    openrs rs1,sql
    response.Write "<form method=post action=engineers.asp?cmd=fixed&cmd2=fix2confirm><table class=maintable2><tr><td>Site</td><td>" & rs1("site") & "</td></tr>"
    response.Write "<input type=hidden name=id value=" & request("id") & "><input type=hidden name=issue value='" & rs1("issue") & "'>"
    response.Write "<tr><td>Issue</td><td>" & rs1("issue") & "</td></tr>"
    response.Write "<tr><td>Start Date</td><td>" & rs1("startdate") & "</td></tr>"
    response.Write "<tr><td>Start Note</td><td>" & rs1("startnotes") & "</td></tr>"
    response.Write "<tr><td>End Note</td><td><textarea name=endnotes></textarea></td></tr>"
    response.write "<input type=hidden name=site value=" & rs1("site") & ">"
    response.Write "<tr><td colspan=2 align=center><input type=submit name=submit value=""Fix""></form>"
    closers rs1


end if
if request("cmd2")="fix2confirm" then
      sql="exec closeengineervisit @visitid=" & session("visitid")
    openrs rs,sql  
    response.Write "Please click below to close this ticket.  You were at this site for " & rs("duration") & " minutes"
    closers rs
    response.Write "<form method=post action=engineers.asp?cmd=fixed&cmd2=fix2>"
    response.Write "<input type=hidden name=id value=" & request("id") & "><input type=hidden name=issue value='" & request("issue") & "'>"
    response.Write "<input type=hidden name=endnotes value=""" & request("endnotes") & """>"
    response.Write "<input type=submit name=submit value='Close Ticket'></form>"
end if
if request("cmd2")="fix2" then
    sql="update siteissues set enddate=getdate(),endnotes=" & tosql(request("endnotes"),"text") & " where id=" & request("id")
   ' response.Write sql
    objconn.execute sql
    sql="insert into siteissuenotes(siteissueid,note,type,engineerid) values(" & request("id")  & "," & tosql(request("endnotes"),"text") & ",'end'," & tosql(session("id"),"Number") & " )"
    'response.Write sql 
    objconn.execute sql
    
    ''must close engineer ticket here 
       
        sql="exec closeengineervisit @visitid=" & session("visitid")
    openrs rs,sql    
    
    Subject="Off Site - " & session("engineername")
    ' request("site") & " � Engineer has left " '  -" & request("issue")
   body="Departure Time:" & rs("closeddate") & vbcrlf
   body=body & "Duration:" & rs("duration") & " minutes" & vbcrlf
  body=body & "Site: " & rs("site") & "-" & rs("sitename")      & vbcrlf      
    body=body & "Issue:   " & rs("issue") & vbcrlf 
      
   sql="select * from siteissuenotes where siteissueid=" & rs("issueid")
    body=body & "Notes:" & vbcrlf
    openrs rs2,sql
    do while not rs2.eof
    body=body & rs2("date") & "-" &  rs2("note") & vbcrlf 
    rs2.movenext
    loop
    closers rs2
    closers rs
   call sendfaultreportingemail(subject,body)
    
    session("visitid")=""
    jsalert("Issue Closed")
    jsredirect("engineers.asp?cmd=siteissues")
    
end if



end sub
sub addnote
    if request("cmd2")="" then
    sql="select * from siteissues where id=" & request("id")
  
    openrs rs1,sql
    site =rs1("site")
    response.Write "<form method=post action=engineers.asp?cmd=addnote&cmd2=savenote><table class=maintable2><tr><td>Site</td><td>" & rs1("site") & "</td></tr>"
    response.Write "<input type=hidden name=id value=" & request("id") & "><input type=hidden name=site value=" & site & ">"
        response.Write "<input type=hidden name=issue value=" & rs1("issue") & ">"
    response.Write "<tr><td>Issue</td><td>" & rs1("issue") & "</td></tr>"
    response.Write "<tr><td>Start Date</td><td>" & rs1("startdate") & "</td></tr>"
    response.Write "<tr><td>Start Note</td><td>" & rs1("startnotes") & "</td></tr>"
    response.Write "<tr><td>Notes</td><td><textarea name=notes></textarea></td></tr>"
    response.Write "<tr><td colspan=2 align=center><input type=submit name=submit value=""Save""></form>"
    closers rs1


end if
if request("cmd2")="savenote" then
    sql="insert into siteissuenotes (siteissueid,note,engineerid) values(" & request("id")  & "," &tosql(request("notes"),"text")  & "," & tosql(session("id"),"Number") & " )"
    

    objconn.execute sql
       Subject= request("site") & " � Note Added-" & request("issue")
    body=request("notes")
   call sendfaultreportingemail(subject,body)
    jsalert("Issue Updated")
    'jsredirect("engineers.asp?cmd=siteissues")
     sql="update engineervisits set closeddate=getdate() where visitid=" & session("visitid")
    objconn.execute sql
    session("visitid")=""
    call siteissues
    
end if


end sub
%>