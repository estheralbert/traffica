<!--#include file ="easypdf.inc"-->
<!--#include file ="config.asp"-->
<!--#include file ="common.asp"-->
<!--#include file ="openconn.asp"-->
<%
Response.ContentType = "application/pdf" 

dim PDF 
    set PDF = server.createobject("aspPDF.EasyPDF") 

    PDF.License("$2127810041;'Clearetoneholdings';PDF;1;0-94.229.136.4;0-109.109.225.138")
    PDF.debug=0
    ' Set margins for the page 
    'left right top bottom

    PDF.SetMargins 30,30,40,30 
    PDF.Page "A4" , 1
    
    PDF.AddHTML "http://traffica2.cleartoneholdings.co.uk/MovementsReportp.asp"
    
    strHTML = " <p align=""left""><b>Engineers Movement Report</b></p> "
    PDF.AddPattern 0, 2, 0, 20, strHTML
    strHTML = " <p align=""right""><b>Page <!-PDF.PAGENUMBER> of <!-PDF.PAGECOUNT></b></p> "
    PDF.AddPattern 1, 0, 0, 0, strHTML

    PDF.BinaryWrite
    set PDF = nothing
 %>