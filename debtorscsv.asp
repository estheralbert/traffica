<!--#include file="openconn.asp"-->

<!--#include file="common.asp"-->
<%
    if session("admin")<>"ok" then response.redirect "admin.asp"
    server.scripttimeout=1000
    ''2007
%>
<html>
<head>
    <script src="sorttable.js"></script>
    <style>
/* Sortable tables */
body
{
    font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
}
table.sortable thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
}
table.sortable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}
.oddrowcolor{
	background-color:#d4e3e5;
}
.evenrowcolor{
	background-color:#c3dde0;
}


.red
{
 
 font-weight:bold;
 color:Red;   
}

table.sortable2 thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable2 {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
}
table.sortable2 th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable2 td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}
</style>
  
</head>

<body>

    <%
    if request("spcn")="" then
        response.write "Please enter in PCN's below separated by commas"
    %>
        <form method="post" actin="debtorscsv.asp">
            Start Number <input type="text" name="startnumber" /><br />
            <textarea rows="10" cols="100" name="spcn"></textarea><br />
            <input type="submit" name="submit" value="Submit" />
        </form>   
    <%
    else
        spcn = request("spcn")
        spcn = replace(spcn," ","")
        spcn = replace(spcn,vbcrlf,"")
        sql = "exec [getDebtorDetails] @debug=0,@type=1,@pcnlist= " & tosql(spcn,"text")
       ' response.write sql
        call outputdcsv(sql)
        response.write "<br>"
        sql = "exec [getDebtorDetails] @debug=0,@type=2,@pcnlist= " & tosql(spcn,"text")
        'response.write sql
        call outputd2csv(sql)
        response.write "<br>"



            sql = "exec [getDebtorDetails] @debug=0,@type=3,@pcnlist= " & tosql(spcn,"text")
        'response.write sql
       call outputd3csv(sql)
        response.write "<br>"


    end if
    %>

</body></html>
<%
    sub outputd2csv(sql)
	    on error resume next
	    'response.write sql
	    randomize
        nRandom = (100-1)*Rnd+1
        fileExcel = monthname(month(date)) &  cstr(year(date)) & "_" & CStr(nRandom) & ".csv"
        filePath = Server.mapPath("admin.asp")
	    filepath = mid(filepath,1,len(filepath)-9)
	    'response.write "filepath:" & filepath & "<br>"
	    filename=filePath & "excelfiles\" & fileExcel
        'response.write filename
	    Set fs = Server.CreateObject("Scripting.FileSystemObject")
        Set MyFile = fs.CreateTextFile(filename, True)
        Set rs = objconn.Execute(sql)

        strLine = "" 'Initialize the variable for storing the filednames

        For each x in rs.fields
            strLine = strLine & "" & x.name & ","
        Next
	 	'strline=strline& vb
        strline = left(strline,len(strline)-1)
        MyFile.writeline strLine
        strline = ""
        id = 1
        n = request("startnumber")
        Do while Not rs.EOF and i<=50000
            'dcoll,accno,dname,tstyle,status,uno,addr1,addr2,addr3,addr4,apcode,tele,email,totdue
            cell = n
            strLine = strLine & "" & cell & ","
            cell = rs("accno")
            strLine = strLine & "" & cell & ","
            cell = rs("dname")
            strLine = strLine & "" & cell & ","
            cell = rs("tstyle")
            strLine = strLine & "" & cell & ","
            cell = rs("status")
            strLine = strLine & "" & cell & ","
            cell = rs("uno")
            strLine = strLine & "" & cell & ","
            cell = cleanstr(rs("addr1"))   
            add2 = cleanstr(rs("addr2"))
            add3 = cleanstr(rs("addr3"))
            add4 = cleanstr(rs("addr4"))
            if cell = "" and add2 <> "" then 
                cell = add2
                add2 = add4
                add4 = ""
            end if
            strLine = strLine & "" & cell & ","
            if add2="" and not isnull(add4) then
                add2 = add4
                add4 = ""
            end if

            cell = add2 'rs("addr2")
            strLine = strLine & "" & cell & ","
            cell = add3
            strLine = strLine & "" & cell & ","
            cell = add4
            strLine = strLine & "" & cell & ","
            cell = rs("apcode")
            strLine = strLine & "" & cell & ","
            cell = rs("tele")
            strLine = strLine & "" & cell & ","
            cell = rs("email")
            strLine = strLine & "" & cell & ","
            cell = rs("totdue")

            strLine = strLine & "" & formatnumber(cell,2)
            MyFile.writeline strLine

            strline = ""
	        err.clear
            n=n+1
            rs.MoveNext
        Loop
	    'myfile.writeline "</table>"
        MyFile.Close
        Set MyFile = Nothing
        Set fs = Nothing

        link="<A HREF=excelfiles/" & fileExcel & " class=adminmenu target=_new>Click Here to Open CSV</a>"
        Response.write "<br>CSV File Created for type=2<br><br>" &  link
        'if i>=50000 then response.write "Excel is only showing the first 50,000 records"
    end sub



    
    sub outputd3csv(sql)
	    on error resume next
	    'response.write sql
	    randomize
        nRandom = (100-1)*Rnd+1
        fileExcel = monthname(month(date)) &  cstr(year(date)) & "_" & CStr(nRandom) & ".csv"
        filePath = Server.mapPath("admin.asp")
	    filepath = mid(filepath,1,len(filepath)-9)
	    'response.write "filepath:" & filepath & "<br>"
	    filename=filePath & "excelfiles\" & fileExcel
        'response.write filename
	    Set fs = Server.CreateObject("Scripting.FileSystemObject")
        Set MyFile = fs.CreateTextFile(filename, True)
        Set rs = objconn.Execute(sql)

        strLine = "" 'Initialize the variable for storing the filednames

        For each x in rs.fields
            strLine = strLine & "" & x.name & ","
        Next
	 	'strline=strline& vb
        strline = left(strline,len(strline)-1)
        MyFile.writeline strLine
        strline = ""
        id = 1
        n = request("startnumber")
        Do while Not rs.EOF and i<=50000
            
            strLine = rs("pcn") & "," & rs("reason")
            
            MyFile.writeline strLine

            strline = ""
	        err.clear
            n=n+1
            rs.MoveNext
        Loop
	    'myfile.writeline "</table>"
        MyFile.Close
        Set MyFile = Nothing
        Set fs = Nothing

        link="<A HREF=excelfiles/" & fileExcel & " class=adminmenu target=_new>Click Here to Open CSV</a>"
        Response.write "<br>CSV File Created for deleted records<br><br>" &  link
        'if i>=50000 then response.write "Excel is only showing the first 50,000 records"
    end sub

    function cleanstr(mystring)
        mystring=replace(mystring,",","")
        cleanstr=mystring
    end function
    
    sub outputdcsv(sql)
	    on error resume next
	    'response.write sql
	    randomize
        nRandom = (100-1)*Rnd+1
        fileExcel = monthname(month(date)) &  cstr(year(date)) & "_" & CStr(nRandom) & ".csv"
        filePath = Server.mapPath("admin.asp")
	    filepath = mid(filepath,1,len(filepath)-9)
	    'response.write "filepath:" & filepath & "<br>"
	    filename=filePath & "excelfiles\" & fileExcel
        'response.write filename

	    Set fs = Server.CreateObject("Scripting.FileSystemObject")
        Set MyFile = fs.CreateTextFile(filename, True)
        Set rs = objconn.Execute(sql)

        strLine = "" 'Initialize the variable for storing the filednames

        For each x in rs.fields
             strLine= strLine & "" & x.name & ","
        Next
	 	'strline=strline& vb
        strline = left(strline,len(strline)-1)
        MyFile.writeline strLine
        strline=""
        id = 1
        n = request("startnumber")
        Do while Not rs.EOF and i<=50000
            'strLine="<tr>"
            for each x in rs.Fields
	  	        cell = x.value
		        if x.name="totdue" or x.name="amount" then cell=formatnumber(cell,2)
                if x.name="dcoll" then cell=n & "01"
                strLine = strLine & "" & cell & ","
            next
	        'strline=strline & "</tr>"
            j = i
            i = i+1

            strline = left(strline,len(strline)-1)
		    'response.write strline & vbcrlf
            MyFile.writeline strLine

            strline=""
	        err.clear
            n = n+1
            rs.MoveNext
        Loop
	    'myfile.writeline "</table>"
        MyFile.Close
        Set MyFile = Nothing
        Set fs = Nothing

        link = "<A HREF=excelfiles/" & fileExcel & " class=adminmenu target=_new>Click Here to Open CSV</a>"
        Response.write "<br>CSV File Created for type=1<br><br>" &  link
        'if i>=50000 then response.write "Excel is only showing the first 50,000 records"
    end sub

%>