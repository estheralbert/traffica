<!--#include file="openconn.asp"-->
<!--#include file="common.asp"-->
<%
    server.ScriptTimeout=1000000000
    
    Dim PDF, DocTicket, DocFinal, Page, Canvas, debug, startTime, endtime, duration, count

    debug=request("debug")
    starttime = Timer()
    
	Set PDF = Server.CreateObject("Persits.PDF")
    Set DocFinal = PDF.CreateDocument

    batchid=request("batchid")
    count = 0
    sql="select d.id,offence,dvla,d.site,s.shortdesc,address1,address2,address3,town,postcode " & _
        "from dbo.dvlarep d left join dbo.[site information] s on s.sitecode=d.site where " & _
        "batch=" & batchid & " order by d.id"

    openrs rsaa,sql
    Do While Not rsaa.eof
        id=rsaa("id")
        sqlcd="exec spgetcardetails2 @reg=" & tosql(rsaa("dvla"),"text")
        openrs rscd,sqlcd
        cardetails=rscd("cardetails")
        closers rscd

        ' Open an existing document, form W-9
        Set DocTicket = PDF.OpenDocument("C:\inetpub\wwwroot\traffica\versatiledvla.pdf")

        ' Obtain Page object for page 1
        Set Page = DocTicket.Pages(1)

        ' Create Canvas object to be used throughout application
        Set Canvas = Page.Canvas 

        ' Create font object
        Set FontAriel = DocTicket.Fonts("Arial")
		
        y=-30
        If Not IsNull(rsaa("dvla"))     Then Canvas.DrawText    rsaa("dvla"),  "x=440, y=" & y+625 & "; size=11; alignment=left; color=black", FontAriel
        If Not IsNull(cardetails)       Then Canvas.DrawText    cardetails,    "x=387, y=" & y+605 & "; size=8; alignment=left; color=black", FontAriel
		 
        Canvas.DrawText    rsaa("id"), "x=190, y=" & y+183 & "; size=11; alignment=left; color=black", FontAriel
        Canvas.DrawText    FormatDateTime(rsaa("offence"),2) , "x=387, y=" & y+321 & "; size=11; alignment=left; color=black", FontAriel
    
        If Not IsNull(rsaa("address1")) Then Canvas.DrawText   rsaa("address1"), "x=335, y=" & y+289 & "; size=11; alignment=left; color=black", FontAriel
        If Not IsNull(rsaa("address2")) Then Canvas.DrawText   rsaa("address2"), "x=335, y=" & y+274 & "; size=11; alignment=left; color=black", FontAriel
        If Not IsNull(rsaa("address3")) Then Canvas.DrawText   rsaa("address3"), "x=335, y=" & y+259 & "; size=11; alignment=left; color=black", FontAriel
    
        Canvas.DrawText  date() , "x=505, y=" & y+55 & "; size=11; alignment=left; color=black", FontAriel
      myd=cisodate(date())
 
        Path ="C:\inetpub\wwwroot\traffica\excelfiless\"  & myd & "\versatiledvlaa" & id & ".pdf"
	
        Set DocAdd = Pdf.OpenDocumentBinary( DocTicket.SaveToMemory )
        FileName = DocAdd.Save( Path, true)
       ' Response.write path & "<br/>"

        DocFinal.AppendDocument DocAdd 

        'Response.Write "<P><B>Success. Your PDF file <font color=gray>" & FileName & "</font> can be downloaded <A HREF=excelfiles/versatiledvla" &id & ".pdf>here</A></B>."
        Set Page = Nothing
        Set DocTicket = Nothing
        count = count + 1
        rsaa.movenext 
    Loop
    closers rsaa
  '  Response.write "<hr>"
    FileName = Docfinal.Save ("C:\inetpub\wwwroot\traffica\excelfiless\" & myd & "\versatiledvla" & batchid & ".pdf", true)

    endTime = Timer()
    duration = (endTime - startTime)

    Response.Write  "<P><B>Success. Your PDF file <font color=gray>" & FileName & _
                    "</font> can be downloaded <A HREF=excelfiless/"  & myd & "/" & FileName & ">here</A></B>."
    If debug = 1 Then
        Response.write "<hr>"
        Response.Write  "<p><b>Total Files: </b>" & count & _
                        "<br/><b>Duration: </b>" & FormatNumber(duration/60.00, 2) & _
                        " minutes<br/><b>Average Time: </b>" & (duration/60.00)/count & " minutes (" & duration/count & " seconds)</p>"
    End If
%>