<% Server.ScriptTimeout = 3600000 %>
		<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%
 pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 

%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="openconntriangle.asp"-->


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Traffic Guard Summary</title>
    <% if request("print")="1" then %>
    <style>
  body{
  background-color:#ffffff;
  
  }
  table{
  width:500px;
  }
    th{
      color:#000000;
       font-family: Arial, Verdana, sans-serif; 
    font-size:12px;
    text-align:left;
    font-weight:bold;
   
    }
    td{
    color:#000000;
     font-family: Arial, Verdana, sans-serif; 
    font-size:10px;
  
    }
    .big{
    font-size:150%;
    }
    .purple{
    color:gray;
    }
    
    </style>
    
    <% else %>
    <style>
  body{
  background-color:#000000;
  
  }
    th{
      color:#fff800;
       font-family: Arial, Verdana, sans-serif; 
    font-size:28px;
    text-align:left;
   
    }
    td{
    color:#33ccff;
     font-family: Arial, Verdana, sans-serif; 
    font-size:28px;
  
    }
    .big{
    font-size:150%;
    }
    .purple{
    color:#6600cc;
    }
    </style>
   
    <script>
    <!--

        /*
        Auto Refresh Page with Time script
        By JavaScript Kit (javascriptkit.com)
        Over 200+ free scripts here!
        */

        //enter refresh time in "minutes:seconds" Minutes should range from 0 to inifinity. Seconds should range from 0 to 59
        var limit="0:300"

        if (document.images){
            var parselimit=limit.split(":")
            parselimit=parselimit[0]*60+parselimit[1]*1
        }
        function beginrefresh(){
            if (!document.images)
                return
            if (parselimit==1)
                window.location.reload()
            else{ 
                parselimit-=1
                curmin=Math.floor(parselimit/60)
                cursec=parselimit%60
                if (curmin!=0)
                    curtime=curmin+" minutes and "+cursec+" seconds left until page refresh!"
                else
                    curtime=cursec+" seconds left until page refresh!"
            window.status=curtime
            setTimeout("beginrefresh()",1000)
            }
        }

        window.onload=beginrefresh
        //-->
    </script>


<% end if %>
</head>
<body>
<%sql="exec summary2"
    openrs rs,sql
        messagesover1hour=rs("messagesover1hour")
        communicationover3days=rs("communicationover3days")
        lastmessagereceived=rs("lastmessagereceived")
        lastpaymentreceived=rs("lastpaymentreceived")
        messageslastweek=rs("messageslastweek")
        messagesthisweek=rs("messagesthisweek")
        messagestoday=rs("messagestoday")
        datarequired7=rs("datarequired7")
    closers rs

    dim asiteswritten(500)
    sitewritten=0

    sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from dvlarep)"

    openrs rssites,sqlsites
        numsiteswaiting=0
        siteswaiting=""
        do while not  rssites.eof
            numsiteswaiting=numsiteswaiting+1
            siteswaiting=siteswaiting & rssites("sitecode") & ", "
            asiteswritten(sitewritten)=rssites("sitecode")
            sitewritten=sitewritten+1
            rssites.movenext
        loop
    closers rssites
    asiteswaiting=split(siteswaiting,",")
    for i=lbound(asiteswaiting) to ubound(asiteswaiting)
        asiteswaiting(i)=trim(asiteswaiting(i))
    next
    
    sqlt="select max([datetime]) as trianglepayment from trianglepayment"
    set rst=objconnt.execute(sqlt)
    ltrianglepayment=rst("trianglepayment")
    rst.close
    set rst=nothing
 %>
<table cellpadding=8>
<tr><th>Messages over 1 Hour old</th><td><%=messagesover1hour %></td></tr>
<tr><th>Communication older than 4 days</th><td><%=communicationover3days%></td></tr>
<tr><th>Data Required older than 7 days</th><td><%=datarequired7%></td></tr>
<tr><th>Last Message Received</th><td><%=lastmessagereceived%></td></tr>
<tr><th>Last Payment Received [a]</th><td><%=lastpaymentreceived%></td></tr>
<tr><th>Last Payment Received [b]</th><td><%=ltrianglepayment%></td></tr>

<tr><th>No Access</th></tr>
<% 
    call GetSiteIssues("No Access", true)
%>

<tr><th>Double Entry</th></tr>
<% 
    call GetSiteIssues("Double Entry", true)
%>

<tr><th>Suspended</th></tr>
<% 
    call GetSiteIssues("Suspended", true)
%>

<tr><th>Critical Alarms (21 days)</th>
<%
    dim asites(200)
    GetCriticalAlarm(21)
%>
</tr> 
<tr><th>Critical Alarms (14 days)</th>
<%
    GetCriticalAlarm(14)
%>
</tr>
<tr><th>Critical Alarms (6 days)</th>
<%
    GetCriticalAlarm(6)
%>
</tr>

<tr><th>Sites Waiting</th>
<% 
    if siteswaiting<>"" then siteswaiting=left(siteswaiting,len(siteswaiting)-2)
    Response.Write "<td>" & numsiteswaiting & "</td>"
%>
</tr>
<tr><td colspan=2 width=500><%=siteswaiting %></td></tr>

<tr><th>Communication error</th>
<% 
    sqlc="exec GetCommErr"
    openrs rsc,sqlc
    do while not rsc.eof 
        if rsc("platecount")=0 then 
            cerrorsites=cerrorsites & rsc("sitecode") & ","
        end if
        rsc.movenext
    loop
    if cerrorsites>"" then cerrorsites=left(cerrorsites,len(cerrorsites)-1)
    acerrorsites=split(cerrorsites,",")
    Response.Write "<td>" & ubound(acerrorsites)+1 & "</td></tr>"
    Response.Write "<tr><td colspan=2 width=500>" & cerrorsites & "</td></tr>"
%>
<tr><th>Messages</th><td><%=messageslastweek %>&nbsp;&nbsp;<%=messagesthisweek %> &nbsp;&nbsp;<%=messagestoday %></td></tr>
</table>
</body>
</html>
<%

function GetSiteIssues(issue, updatesitewritten)
    sqlna="exec GetSiteIssues @issue='" & issue & "'"
    openrs rsna,sqlna
        if not rsna.eof and not rsna.bof then
            response.write "<tr><td colspan=2><table cellpadding=10><tr>"
            x=0
            
            do while not rsna.eof
                response.write   "<td>" & rsna("site") & "(" & rsna("daysstage2") & ") </td> "
                if updatesitewritten then
                    asiteswritten(sitewritten)=rsna("site")
                    sitewritten=sitewritten+1
                end if
                x=x+1
                if x=4 then response.Write "</tr><tr>"
                rsna.movenext
            loop
            response.Write "</tr></table>"
            response.Write "</td></tr>"
        end if    
    closers rsna
end function

function GetCriticalAlarm(days)
    sqlsites="exec GetCriticalAlarms @days=" & days
    openrs rssites,sqlsites
        numsites=0
        i=0
        criticalsites=""
        do while not rssites.eof
            if not elementinarray(asiteswritten,rssites("sitecode"),0) then 
                asiteswritten(sitewritten)=rssites("sitecode")
                sitewritten=sitewritten+1
                numsites=numsites+1
                if rssites("inpv")=1 then
                    bbold=0
                    if rssites("inpvstage2")=1 then bbold=1
                    criticalsites=criticalsites & "<font class=purple><b>"
                    if bbold=1 then criticalsites=criticalsites & "</b><del>"
                    criticalsites=criticalsites  & rssites("sitecode") 
                    if bbold=1 then criticalsites=criticalsites  & "</del>"
                    criticalsites=criticalsites & "</b></font>" &", "
                else
                    criticalsites=criticalsites & rssites("sitecode") & ", "
                end if
                asites(i)=rssites("sitecode")
                i=i+1
            end if
            rssites.movenext
        loop
    closers rssites
    criticalsites=left(criticalsites,len(criticalsites)-2)
    response.Write "<td>" & numsites & "</td></tr>"
    response.Write "<tr><td colspan=2 width=500>" & criticalsites & "</td>"
end function
 
 %>