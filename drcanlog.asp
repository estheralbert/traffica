<!--#include file="openconn.asp"-->

<!--#include file="common.asp"-->
<%
server.scripttimeout=1000
''2007
    response.buffer=false
    on error goto 0
 

%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Debt Recovery</title>

    
  
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">
  
<!-- jQuery -->
<script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
  
<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.js"></script>

	<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script type="text/javascript">
	    $(document).ready(function () {
	        $("#select-all").change(function () {
	            $("input:checkbox").prop('checked', $(this).prop("checked"));
	        });

	        $('tbody').on('change', 'input[type="checkbox"]', function () {
	            if (!this.checked) {
	                var el = $('#select-all').get(0);
	                if (el && el.checked && ('indeterminate' in el)) {
	                    el.indeterminate = true;
	                }
	            }
	        });
	    })
	</script>
	
</head>
<body>
	<form id="frmZZPS" name="frmZZPS" method="post">
		<p>
			<select id="cboZZPS" name="cboZZPS" onchange="this.form.submit()">ZZPS 
				<%	If Request.Form("cboZZPS") = 0 Then %>
						<option selected value="0">Not Sent</option>
						<option value="1">Sent</option>
				<%	Else %>
						<option value="0">Not Sent</option>
						<option selected value="1">Sent</option>
				<%	End If %>
			</select>
		</p>
		<%
				Dim zzps_Sent
				zzps_Sent = Request.Form("cboZZPS")
				
				If zzps_Sent = "" Then zzps_Sent = 0
				
				If Request.Form("chkSendZZPS") <> "" Then
					SqlUpdate = "UPDATE DebtRecovery SET SentZZPS = 1 WHERE Id IN (" & Request("chkSendZZPS") & ")"
          '  response.write sqlupdate
					objconn.Execute(SqlUpdate)
				End If
				
				SqlSelect = "select d.id,d.pcn,debtrecoverydate,sentzzps,cancelled,reason,authby   from debtrecovery d left join cancelled c on d.pcn=c.pcn  WHERE c.pcn is not null and SentZZPS=@zzps" 
				SqlSelect = Replace(SqlSelect,"@zzps",zzps_Sent)
				'response.write sqlselect 
				Set rsObj = objconn.Execute(SqlSelect)
				If Not (rsObj.BOF And rsObj.EOF) Then
					If zzps_Sent = 0 Then %>
						<p>
							<input type="submit" id="btnSave" name="btnSave" value="Mark Checked as sent"></input>
						</p>
				 <% End If %>


          <script language="Javascript">




              $(document).ready(function () {

                  $('#datatables').DataTable();
              });







                </script>
		<table id="datatables" class="display" cellspacing="0" width="100%"> 
			<thead>
				<tr>
					<th style="max-width:90px;"><% If zzps_Sent = 0 Then %><input name="select_all" value="1" id="select-all" type="checkbox">&nbsp;&nbsp;<% End If%>Sent ZZPS</th>
					<th>PCN</th>
					<th>Debt Recovery</th>
					<th>Reason</th>
					<th>Authorised By</th>
					<th>Cancelled</th>
				</tr>
			</thead>
			<tbody>
					<%
					While NOT rsObj.EOF
					%>
						<tr>
							<td><%
								If rsObj("SentZZPS") Then
									Response.Write "Sent"
								Else %>
									<input type="checkbox" name="chkSendZZPS" value="<%=rsObj("Id")%>">Send
							<%
								End If
							%></td>
                            <td><%=rsobj("pcn")%></td>
							<td><%=rsObj("DebtRecoveryDate") %></td>
							<td><%=rsObj("Reason")%></td>
							<td><%=rsObj("AuthBy")%></td>
							<td><%=rsObj("Cancelled")%></td>
							
						</tr>
						
					<%
						rsObj.MoveNext
					Wend
					%>
			</tbody>
		</table> 
		<%
				Else
		%>
		<table id="Table1"  cellspacing="0" width="100%"> 
			<thead>
				<tr>
					<th>No records</th>
				</tr>
			</thead>
		</table>
			<%
				End If
			%>
	</form>
</body>
</html>

