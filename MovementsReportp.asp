﻿<% Server.ScriptTimeout = 3600000 %>
<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%  
    pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 
%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Movements Summary</title>
    <style type="text/css">
        table {adjust-width:0}
    </style>
</head>
<body>
    <h3><u><b>Engineers Movement Report</b></u></h3>
    <!-PDF.SETPROPERTY ID=101 Value="8"> 
    <%
        on error resume next
        dim arr1
        dim arr2
        dim arr3
        dim arr4
        dim arr5
        
        dim weeknum
        dim skipcolumns
        
        dim btwcol
        dim tablesetup
        dim HeaderRow1
        dim HeaderRow2
        dim tdReg
        dim tdAlert
        dim tdsetupCur
        
        skipcolumns = 5
        
        btwcol = "<td bgcolor=""black""></td>"
        tdReg = "<td align=""center"">"
        tdAlert = "<td align=""center"" bgcolor=""#FF6347""><font color=""#000000"">"
        tdRegend = "</td>"
        tdAlertend = "</font></td>"
        tablesetup = "<table border=""1"" cellpadding=""4"" cellspacing=""0"">" & vbCrLf
        
        sql="exec MovementSummary"
        openrs rs1,sql
        arr1 = rs1.GetRows()
                
        set rs1 = rs1.NextRecordset()
        arr2 = rs1.GetRows()

        set rs1 = rs1.NextRecordset()
        arr3 = rs1.GetRows()

        set rs1 = rs1.NextRecordset()
        arr4 = rs1.GetRows()
        
        set rs1 = rs1.NextRecordset()
        arr5 = rs1.GetRows()
        
            colscount = UBound(arr1, 1)
                  
            fieldcount = rs1.Fields.Count 
            dim arrtot2()
            dim arrtot3()
            dim arrtot4()
            redim arrtot2(fieldcount-skipcolumns-1,0)
            redim arrtot3(fieldcount-skipcolumns-1,0)
            redim arrtot4(fieldcount-skipcolumns-1,0)
            
            'Create the top header row
            HeaderRow1 = "<tr>" & vbCrLf & _
                "<th align=""center"" colspan=""" & fieldcount-skipcolumns+2 & """><font size=""3""><b>Arrival Times</b></font></th>" & vbCrLf & _
                btwcol & vbCrLf & _
                "<th align=""center"" colspan=""" & fieldcount-skipcolumns & """><font size=""3""><b>Sites Visited</b></font></th>" & vbCrLf & _
                btwcol & vbCrLf & _
                "<th align=""center"" colspan=""" & fieldcount-skipcolumns & """><font size=""3""><b>Time Spent on Site</b></font></th>" & vbCrLf & _
                btwcol & vbCrLf & _
                "<th align=""center"" colspan=""" & fieldcount-skipcolumns & """><font size=""3""><b>Time Spent Between Sites</b></font></th>" & vbCrLf & _
            "</tr>" & vbCrLf
            
            'Create the second header row
            HeaderRow2 = "<tr>" & vbCrLf & _
                "<th align=""left""><b>Date</b></th><th align=""center""><b>Day</b></th>" & vbCrLf
                For i = skipcolumns To fieldcount - 1
                    HeaderRow2 = HeaderRow2 & "<th align=""center""><b>" & rs1.Fields(i).Name & "</b></th>" & vbCrLf
                Next 
                
                HeaderRow2 = HeaderRow2 & btwcol & vbCrLf
                
                For i = skipcolumns To fieldcount - 1
                    HeaderRow2 = HeaderRow2 & "<th align=""center""><b>" & rs1.Fields(i).Name & "</b></th>" & vbCrLf
                Next 
                
                HeaderRow2 = HeaderRow2 & btwcol & vbCrLf
                
                For i = skipcolumns To fieldcount - 1
                    HeaderRow2 = HeaderRow2 & "<th align=""center""><b>" & rs1.Fields(i).Name & "</b></th>" & vbCrLf
                Next 
                
                HeaderRow2 = HeaderRow2 & btwcol & vbCrLf
                
                For i = skipcolumns To fieldcount - 1
                    HeaderRow2 = HeaderRow2 & "<th align=""center""><b>" & rs1.Fields(i).Name & "</b></th>" & vbCrLf
                Next                 
            HeaderRow2 = HeaderRow2 & "</tr>" & vbCrLf
            
            Response.Write tablesetup
            Response.Write HeaderRow1
            Response.Write HeaderRow2

            weeknum = 1
            for irow = 0 to UBound(arr1, 2)
                
                if (irow+1 - ((weeknum-1)*7)) mod 2 = 0 then
                    trCur = "<tr bgcolor=""#D3D3D3"">"
                else
                    trCur = "<tr>"
                end if
                Response.Write trCur                                        
                    Response.Write "<td>" & arr1(0, irow) & "</td>"
                    Response.Write "<td align=""center"">" & arr1(2, irow) & "</td>"
                                        
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = tdAlert
                            tdsetupCurend = tdAlertend
                       else
                            tdsetupCur = tdReg
                            tdsetupCurend = tdRegend 
                        end if
                        Response.Write tdsetupCur & fmtdash(arr1(icol, irow)) & tdsetupCurend
                    Next
                    Response.Write btwcol
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = tdAlert
                            tdsetupCurend = tdAlertend
                       else
                            tdsetupCur = tdReg
                            tdsetupCurend = tdRegend 
                        end if
                        Response.Write tdsetupCur & fmtdash(arr2(icol, irow)) & tdsetupCurend
                        
                        'Keep a running total for the week
                        arrtot2(icol-skipcolumns,0) = CLng(arrtot2(icol-skipcolumns,0))+CLng(arr2(icol, irow))
                    Next
                    Response.Write btwcol
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = tdAlert
                            tdsetupCurend = tdAlertend
                       else
                            tdsetupCur = tdReg
                            tdsetupCurend = tdRegend 
                        end if
                        Response.Write replace(tdsetupCur,"center","right") & fmtdash(Minutes2Hours(arr3(icol, irow))) & tdsetupCurend
                        
                        'Keep a running total for the week
                        arrtot3(icol-skipcolumns,0) = CLng(arrtot3(icol-skipcolumns,0))+CLng(arr3(icol, irow))
                    Next
                    Response.Write btwcol
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = tdAlert
                            tdsetupCurend = tdAlertend
                       else
                            tdsetupCur = tdReg
                            tdsetupCurend = tdRegend 
                        end if
                        Response.Write replace(tdsetupCur,"center","right") & fmtdash(Minutes2Hours(arr4(icol, irow))) & tdsetupCurend

                        'Keep a running total for the week
                        arrtot4(icol-skipcolumns,0) = CLng(arrtot4(icol-skipcolumns,0))+CLng(arr4(icol, irow))
                    Next                    
                Response.Write "</tr>"
                
                if (irow+1) mod 7 = 0 then
                    Response.Write "<tr>"
                        Response.Write "<td colspan=""2""><b>Week Total</b></td>"
                        
                        For icol = 0 To UBound(arrtot2,1)
                            Response.Write "<td>&nbsp;</td>"
                        Next
                        Response.Write btwcol
                        For icol = 0 To UBound(arrtot2,1)
                            Response.Write tdReg & "<b>" & arrtot2(icol, 0) & "</b></td>"
                            arrtot2(icol, 0) = 0
                        Next
                        Response.Write btwcol
                        For icol = 0 To UBound(arrtot3,1)
                            Response.Write replace(tdReg,"center","right") & "<b>" & Minutes2Hours(arrtot3(icol, 0)) & "</b></td>"
                            arrtot3(icol, 0) = 0
                        Next
                        Response.Write btwcol
                        For icol = 0 To UBound(arrtot4,1)
                            Response.Write replace(tdReg,"center","right") & "<b>" & Minutes2Hours(arrtot4(icol, 0)) & "</b></td>"
                            arrtot4(icol, 0) = 0
                        Next
                    Response.Write "</tr>"
                    if weeknum < 4 then
                        if weeknum = 2 then
                            Response.Write "</table>"
                            Response.Write "<!-PAGE BREAK>"
                            Response.Write tablesetup
                            Response.Write HeaderRow1
                            Response.Write HeaderRow2
                        else
                            Response.Write "<tr class=""btwrow""><td colspan=""" & (fieldcount + ((fieldcount-skipcolumns)*3)) & """>&nbsp;</td></tr>"
                        end if
                    else
                        Response.Write "</table>"
                    end if
                    weeknum = weeknum + 1
                end if
            Next
       
        closers rs1
     %>
</body>
</html>
<%
function iif(psdStr, trueStr, falseStr)
  if psdStr then
    iif = trueStr
  else 
    iif = falseStr
  end if
end function

function Minutes2Hours(minutes)
    dim hours
    dim mins
    dim retval
    
    On Error Resume Next
    hours = minutes \ 60
    mins = minutes mod 60
    
    retval = CStr(hours) & ":" & Right("0" & CStr(mins),2)
    if len(hours)=0 then 
        retval = minutes
    else
        if len(retval) < 5 then retval = Right("00" & retval, 5)
    end if
    Minutes2Hours = retval
end function

function fmtdash(val)
    if val = "-" then
        val = "&#150;"
    end if
    fmtdash = val
end function
%>