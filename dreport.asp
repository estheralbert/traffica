 <!--#include file="openconn.asp"-->

<!--#include file="common.asp"-->
<%
server.scripttimeout=100000
''2007

 

%>
<html>
<head>
<script src="sorttable.js"></script>
 <style>
/* Sortable tables */
body
{
    font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
}
table.sortable thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
}
table.sortable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}
.oddrowcolor{
	background-color:#d4e3e5;
}
.evenrowcolor{
	background-color:#c3dde0;
}


.red
{
 
 font-weight:bold;
 color:Red;   
}

table.sortable2 thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable2 {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
	float:left;
	margin:10px
}
table.sortable2 th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable2 td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}





#div {
  width: 200px;
}
 
h2 {
  font: 400 40px/1.5 Helvetica, Verdana, sans-serif;
  margin: 0;
  padding: 0;
}
 
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
 
li {
  font: 200 20px/1.5 Helvetica, Verdana, sans-serif;
  border-bottom: 1px solid #ccc;
}
 
li:last-child {
  border: none;
}
 
li a {
  text-decoration: none;
  color: #000;
  display: block;
  width: 200px;
 
  -webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
  -moz-transition: font-size 0.3s ease, background-color 0.3s ease;
  -o-transition: font-size 0.3s ease, background-color 0.3s ease;
  -ms-transition: font-size 0.3s ease, background-color 0.3s ease;
  transition: font-size 0.3s ease, background-color 0.3s ease;
}
 
li a:hover {
  font-size: 30px;
  background: #f6f6f6;
}


</style>
  
</head>

<body>
    <h2>Daily Report</h2>

    <%
        cmd2=request("cmd2")   
mydate=date()-1
myhtml=myhtml & "<P align=center><B>" & day(mydate) & " " & monthname(month(mydate)) & " " & year(mydate) & "</B></p>"
        response.write myhtml
        
        
        if cmd2="" then %>
    <ul>
   <li><a href="dreport.asp?cmd2=exan">Excel Analysis for the last 20 days</a></li> 

 <li><a href="dreport.asp?cmd2=critical">  21 days critical, 14 days critical</a></li> 

 <li><a href="dreport.asp?cmd2=sitenv">  Sites with no violators</a></li> 

 <li><a href="dreport.asp?cmd2=employee">    Employee Report</a></li> 
    </ul>    
<%
    end if
 
  ' 1.      Excel

    if cmd2="sitenv" then
        
    snvsite=""
    sqlsnv="exec sitesnoviolations"
openrs rssnv,sqlsnv
snv= "<table border=0 class=sortable><tr><td>Site</td><td>Site Description<td>Last Date of Violation</td></tr>"
do while not rssnv.eof 
snv=snv &  "<tr><td>" & rssnv("sitecode") & "</td><td>" & getsitedesc(rssnv("sitecode")) & "</td><td>" & rssnv("lastviolation") & "</td></tr>"
snvsite=snvsite & rssnv("sitecode") & ","

rssnv.movenext
loop

closers rssnv
snv=snv &  "</table>"


closers rssnv
    response.write snv
    end if
    if cmd2="critical" then


         
    snvsite=""
    sqlsnv="exec sitesnoviolations"
openrs rssnv,sqlsnv
snv= "<table border=0 class=sortable><tr><td>Site</td><td>Site Description<td>Last Date of Violation</td></tr>"
do while not rssnv.eof 
snv=snv &  "<tr><td>" & rssnv("sitecode") & "</td><td>" & getsitedesc(rssnv("sitecode")) & "</td><td>" & rssnv("lastviolation") & "</td></tr>"
snvsite=snvsite & rssnv("sitecode") & ","

rssnv.movenext
loop

closers rssnv
snv=snv &  "</table>"


closers rssnv

     snvsite=left(snvsite,len(snvsite)-1)
 asnv=split(snvsite,",")
     dim asiteswritten(2000)
    sitewritten=0
    'sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from violators)"
    sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from dvlarep)" ' and sitecode<700"
    openrs rssites,sqlsites
    numsiteswaiting=0
    siteswaiting=""
    do while not  rssites.eof
    numsiteswaiting=numsiteswaiting+1
    siteswaiting=siteswaiting & rssites("sitecode") & ", "
    asiteswritten(sitewritten)=rssites("sitecode")
    sitewritten=sitewritten+1
    rssites.movenext
    loop
    closers rssites
    asiteswaiting=split(siteswaiting,",")
    for i=lbound(asiteswaiting) to ubound(asiteswaiting)
    asiteswaiting(i)=trim(asiteswaiting(i))

    next

'myhtml=myhtml & "<P>Overview <br>Site Issues <br>Cancellation Summary  </p>"

     'criticcalalrm 21 days
    sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from violators where [date of violation]>getdate()-14 group by site)"
    sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from dvlarep where  CONVERT(DATETIME,dvlarep.offence,105)>getdate()-21 group by site) and sitecode not in(select site from anpossibleviolators where d>getdate()-21 and stage=2)and sitecode<700"
    sqlsites="exec GetCriticalAlarms @days=21"
  
    openrs rssites,sqlsites
    numsites=0
    dim asites(800)
    i=0
    criticalsites=""
    cs=""
    do while not  rssites.eof
    if not elementinarray(asiteswritten,rssites("sitecode"),0) and not elementinarray(asnv,rssites("sitecode"),0) then 
         asiteswritten(sitewritten)=rssites("sitecode")
        sitewritten=sitewritten+1
    numsites=numsites+1
    'if checkifinpv(rssites("sitecode"),21)=true then
     '   bbold=0
      '  if checkifinpvstage2(rssites("sitecode"),21)=true then bbold=1
       '     criticalsites=criticalsites & "<font class=purple><b>"
        '    if bbold=1 then  criticalsites=criticalsites & "</b><del>"
         '        criticalsites=criticalsites  & rssites("sitecode") 
          '       cs=cs & rssites("sitecode") 
           'if bbold=1 then  criticalsites=criticalsites  & "</del>"
           'criticalsites=criticalsites & "</b></font>" &", "
           'cs=cs & ","
        'else
            criticalsites=criticalsites & rssites("sitecode") & ", "
            cs=cs & rssites("sitecode") &", "
        'end if
       
    asites(i)=rssites("sitecode")
    i=i+1
    end if
    rssites.movenext
    loop
    closers rssites
    if criticalsites<>"" then   criticalsites=left(criticalsites,len(criticalsites)-2)
    if cs<>"" then cs=left(cs,len(cs)-2)
    criticalsites21=split(cs,",")
    ca21=numsites
    'criticcalalrm 14 days
    sqlsites="select sitecode from [site information] where disabled=0 and sitecode<700 and sitecode not in(select site from violators where [date of violation]>getdate()-14 group by site)"
    sqlsites="select sitecode from [site information] where disabled=0 and sitecode<700 and sitecode not in(select site from dvlarep where  CONVERT(DATETIME,dvlarep.offence,105)>getdate()-14 group by site) and sitecode not in(select site from anpossibleviolators where d>getdate()-14 and stage=2)"
    sqlsites="exec GetCriticalAlarms @days=14"
    openrs rssites,sqlsites
    numsites=0
   
    i=0
    criticalsites=""
    cs=""
    do while not  rssites.eof
    if not elementinarray(asiteswritten,rssites("sitecode"),0) and not elementinarray(asnv,rssites("sitecode"),0) then 
         asiteswritten(sitewritten)=rssites("sitecode")
        sitewritten=sitewritten+1
    numsites=numsites+1
    'if checkifinpv(rssites("sitecode"),14)=true then
     '   bbold=0
      '  if checkifinpvstage2(rssites("sitecode"),14)=true then bbold=1
       '     criticalsites=criticalsites & "<font class=purple><b>"
        '    if bbold=1 then  criticalsites=criticalsites & "</b><del>"
         '        criticalsites=criticalsites  & rssites("sitecode") 
          '       cs=cs& rssites("sitecode") & ", "
          ' if bbold=1 then  criticalsites=criticalsites  & "</del>"
           'criticalsites=criticalsites & "</b></font>" &", "
       ' else
            criticalsites=criticalsites & rssites("sitecode") & ", "
             cs=cs& rssites("sitecode") & ", "
       ' end if
       
    asites(i)=rssites("sitecode")
    i=i+1
    end if
    rssites.movenext
    loop
    closers rssites
     cs=left(cs,len(cs)-2)
    criticalsites14=split(cs,",")
    criticalsites=left(criticalsites,len(criticalsites)-2)
    cafourteen=numsites
    '6days
    sqlsites="select sitecode from [site information] where disabled=0  and sitecode not in(select site from dvlarep where  CONVERT(DATETIME,dvlarep.offence,105)>getdate()-6 group by site) and sitecode not in(select site from anpossibleviolators where d>getdate()-6 and stage=2)"
  sqlsites="exec GetCriticalAlarms @days=6"
    openrs rssites,sqlsites
    numsites=0
    criticalsites=""
    'sitewit
    cs=""
    do while not  rssites.eof
    'chec array asites
    if not elementinarray(asites,rssites("sitecode"),0) and not elementinarray(asiteswaiting,rssites("sitecode"),0) and not elementinarray(asnv,rssites("sitecode"),0) and not elementinarray(asiteswritten,rssites("sitecode"),0) then 

    'response.write "sitewritten:" & sitewritten
     asiteswritten(sitewritten)=rssites("sitecode")
        sitewritten=sitewritten+1
    numsites=numsites+1
       ' if checkifinpv(rssites("sitecode"),6)=true then
        '
             bbold=0
       ' if checkifinpvstage2(rssites("sitecode"),6)=true then bbold=1
        '    criticalsites=criticalsites & "<font class=purple><b>"
         '   if bbold=1 then  criticalsites=criticalsites & "</b><del>"
          ' criticalsites=criticalsites  & rssites("sitecode") 
           '     cs=cs& rssites("sitecode") & ", "
           'if bbold=1 then  criticalsites=criticalsites  & "</del>"
           'criticalsites=criticalsites & "</b></font>" &", "
       ' else
            criticalsites=criticalsites & rssites("sitecode") & ", "
                 cs=cs& rssites("sitecode") & ", "
       ' end if
    end if
    rssites.movenext
    loop
    closers rssites
    criticalsites=left(criticalsites,len(criticalsites)-2)
     cs=left(cs,len(cs)-2)
    criticalsites6=split(cs,",")
    casix=numsites
    'comerror
   myf=myf &  "<table width=520 class=sortable><tr><th></th><th>Site Description</th><th>Days since violation printed</th><tr><td><b>21 day Critical:</b>" & ubound(criticalsites21)+1 & "</td></tr>"
for i=0 to ubound(criticalsites21)

myf=myf &  "<tr><td>" & criticalsites21(i) & "</td><td>" & getsitedesc(criticalsites21(i)) & "</td><td>" & getsitelastprinted(criticalsites21(i)) & "</td> </tr>"
next

myf=myf &  "<tr><td><br><b>14 day Critical:</b>" & ubound(criticalsites14)+1 & "</td></tr>"
for i=0 to ubound(criticalsites14)

myf=myf &  "<tr><td>" & criticalsites14(i) & "</td><td>" & getsitedesc(criticalsites14(i)) & "</td><td>" & getsitelastprinted(criticalsites14(i)) & "</td> </tr>"
next

myf=myf &  "<tr><td><br><b>6 day Critical:</b>" & ubound(criticalsites6)+1 & "</td></tr>"
for i=0 to ubound(criticalsites6)

myf=myf &  "<tr><td>" & criticalsites6(i) & "</td><td>" & getsitedesc(criticalsites6(i)) & "</td><td>" & getsitelastprinted(criticalsites6(i)) & "</td> </tr>"
next

myf=myf &  "</table>"
    response.write myf


    end if
    if cmd2="exan" then

mybody=  "<b>Excel Analysis for the last 30 days</b><br>"
      
      
      
		totale=0
		total1=0
		total2=0
		totalred=0
		totalcrossed=0
		waitingdata=0
		count2=0
		count1=0
		countr=0
		counte=0
		'mymonth=request("month")
		'myear=request("year")
		fromdate=dateadd("d",-30,mydate)
		todate=mydate
		'daysinm=getdaysinmonth(mymonth,myear)
	'	daysinm=2
		'if len(mymonth)<2 then mymonth="0" & mymonth
		mytable= "<b>Data Analysis Overview From:" & fromdate & " To: " &  todate &  "</b>"
		
mytable=mytable & "<table border=1 cellpadding=0 cellspacing=0>"
mytable=mytable & "<tr><td>site <br>code</td><td>Desc</td>"
dfrom=fromdate
do while dfrom<=todate
mytable=mytable & "<td><b>" & day(dfrom) & "</b></td>"
dfrom=dateadd("d",1,dfrom)
loop

mytable=mytable & "</tr>"
'response.write mytable
 
sqlsite="select * from siteinformationanal  order by sitecode"
sqlsite="select s.SiteCode, s.[Name of Site], s.[Borough], s.Address1, s.Address2, s.Address3, s.Town, s.Postcode, s.siteref, s.RequestType, s.telephone, s.fax, s.email, s.storemanager, s.contactnumber, s.contactemail, s.adsltelephone, s.adslip, s.adslusername, s.adslpassword, s.adsllinebyclient, s.disabled,s.vtime, s.disabledletter, s.shortdesc, sa.notes from [Site Information] s left join siteinformationanal sa on s.SiteCode=sa.sitecode where disabled=0 and s.sitecode not in(select site from cancelanal where alldays=1 )order by s.sitecode"
openrs rssite,sqlsite
do while not rssite.eof
snotes=rssite("notes")
'response.write "<hr>" & snotes
	' sqlcc="select * from cancelanal where alldays=1 and site=" & tosql(rssite("sitecode"),"text")

     '   response.write sqlcc
        noshowrow=0
	 'openrs rscc,sqlcc
	 'noshowrow=0
	 'if not rscc.eof and not rscc.bof then noshowrow=1
	 'closers rscc
	 if noshowrow=0 then
	 mytable=mytable & "<tr><td><b>'" & rssite("sitecode") & "</b></td><td>" & rssite("shortdesc") & "</td>"
	' response.write "<tr><!--<td>" & rssite("shortdesc") & "</td>-->"
	 
	 ' response.write "<td><textarea name=notes-" & rssite("sitecode") & ">" & snotes  & "</textarea></td>"

dfrom=fromdate
do while dfrom<=todate
	 			 myear=year(dfrom)
				 mymonth=month(dfrom)
				 myday=day(dfrom)

	if len(myday)<2 then myday="0" & myday
	if len(mymonth)<2 then mymonth="0" & mymonth
	sqlwhere= "site='" & rssite("sitecode") & "' and d >="
	sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
	sqlwhere=sqlwhere & " and d<=cast('" & myear & mymonth & myday & " 23:59' as datetime)"
		sites="'" & rssite("sitecode") & "'"
		df="'" & myear & mymonth & myday & " 00:00'"
		dt="'" & myear & mymonth & myday & " 23:59'"
		
	
			sqlcc="select * from cancelanal where date='" & myear & mymonth & myday & "' and site='" & rssite("sitecode") & "'"
			'response.write sqlcc
			openrs rscc,sqlcc
			if  rscc.eof and rscc.bof then
				acan=0
			else
				acan=1
		end if 
			closers rscc
			
			sqlc="exec spanal @site=" & sites & ",@df=" & df & ",@dt=" & dt
		'	response.write sqlc
      '  response.flush
			'	sqlc="select count(id) as mycount from anpossibleviolators where " & sqlwhere
	'response.write sqlc
	'	mytable=mytable & sqlc
'	response.write sqlc
		openrs rsc,sqlc
		countpv=rsc("countpv")
		countred=rsc("countred")
		countstage1=rsc("countstage1")
		countstage2=rsc("countstage2")
		closers rsc
			'			sqlc="select count(id) as mycount from anpossibleviolators where possibleviolator=0 and stage=1 and " & sqlwhere
	'response.write sqlc
	'	mytable=mytable & sqlc
		'openrs rsc,sqlc
	'countred=rsc("mycount")
	'closers rsc	
	'countred=0
	
				'sqlc="select count(id) as mycount from anpossibleviolators where possibleviolator=1 and " & sqlwhere
	'	mytable=mytable & sqlc
		'openrs rsc,sqlc
	'countstage1=rsc("mycount")
	'closers rsc	
	'	sqlc="select count(id) as mycount from anpossibleviolators where confirmedviolator=1 and " & sqlwhere
		'mytable=mytable & sqlc
	'	openrs rsc,sqlc
	'	countstage2=rsc("mycount")
	'	closers rsc
		if acan=1 then 
			 mytable=mytable & "<td bgcolor=gray>&nbsp;</td>"
			 totalcrossed=totalcrossed+1
		else
    		if countstage2>0 then
    			  mytable=mytable & "<td bgcolor=#0000ff><font color=#ffffff>" & countstage2 & "</font></td>"
						ps1="<td bgcolor=#0000ff><font color=#ffffff>" & countstage2 
						ps2= "</font></td>"
    				total2=total2+ countstage2
						count2=count2+1
    		'elseif countstage1>0 then 
    		'			 mytable=mytable & "<td bgcolor=#339966><font color=#ffffff>" & countstage1 & "</font></td>"
    		'			 ps1="<td bgcolor=#339966><font color=#ffffff>" & countstage1
							
			'				 total1=total1+ countstage1
			'				 count1=count1+1
				elseif countred>0 then 
    					 mytable=mytable & "<td bgcolor=red><font color=#ffffff>" & countred & "</font></td>"
    					 ps1="<td bgcolor=red><font color=#ffffff>" & countred
							
							 totalred=totalred+ countred
							 countr=countr+1
    		else
    				if countpv>0 then
    				mytable=mytable & "<td bgcolor=#ffff00>" & countpv & "</td>"
    				totale=totale+ countpv
						counte=counte+1
						ps1="<td bgcolor=#ffff00>" & countpv 
    				else
    						mytable=mytable & "<td>&nbsp;</td>"
							
							mydate=dateserial(myear,mymonth,myday)
							if cdate(mydate)<date() then
								  waitingdata=waitingdata+1
								'	else
									'		response.write "<hr>didn't put " & mydate & "<hr>"
											 
									end if
								ps1="<td>"
    				end if
    		end if 	
		end if
		 'if ecdebug=1 then wscript.echo "638"
	'response.write ps1 & "<br><input type=checkbox value=" & rssite("sitecode") & "_" & myear & mymonth & myday & " name=cancelled"
	'if acan=1 then response.write " checked"
	'pdf.AddHTML "</td>"
	'response.write "></font></td>"
		 
	'	else
		'		mytable=mytable & "<td bgcolor=#0000ff>"
		'end if
	'	if countpv>0 then
			'  mytable=mytable & "<p bgcolor=#ffff00>" & countpv & "</p><br>"
			'	totale=totale+countpv
		'	end if
		'	if countstage1>0 then
		'	mytable=mytable & "<span bgcolor=#339966>" & countstage1 & "</span><br>"
		'	total1=total1+countstage1
		'	end if
'			mytable=mytable & "stage2 checked:" & countstage2 & "<br><br>"
		'mytable=mytable & "backlog:" & countpv & "<br>"
		'mytable=mytable &  "</td>"
		
  '  dt = dateadd("D", 1, dt)
dfrom=dateadd("d",1,dfrom)
loop
  mytable=mytable & "</tr>"
	'response.write "</tr>"
end if
response.flush
rssite.movenext

loop
closers rssite

mytable=mytable & "<tr bgcolor=#ffff00><td colspan=2>Extracted</td><td> " & totale & "</td><td>" & counte & "</td></tr>"
mytable=mytable & "<tr bgcolor=red><td colspan=2 ><font color=#ffffff>Extracted and passed through 1st stage with no pv</td><td><font color=#ffffff> " & totalred & "</font></td><td><font color=#ffffff> " & countr & "</font></td></tr>"
'mytable=mytable & "<tr bgcolor=#339966><td colspan=2 ><font color=#ffffff>1st stage completed</font></td><td><font color=#ffffff> " & total1 & "</font></td><td><font color=#ffffff> " & count1 & "</font></td></tr>"
mytable=mytable & "<tr bgcolor=#0000ff><td colspan=2 ><font color=#ffffff>2nd stage completed </td><td><font color=#ffffff>" & total2 & "</font></td><td><font color=#ffffff> " & count2 & "</font></td></tr>"
'mytable=mytable & "<tr bgcolor=#000000><td colspan=2 ><font color=#ffffff>Total Crossed off </font></td><td><font color=#ffffff>" & totalcrossed & "</font></td></tr>"
'if totalcrossed>0 then
'newtotal=totale+totalred+total1+total2
'mytable=mytable & "<tr><td colspan=4 bgcolor=#000000><font color=#ffffff>Percentage Crossed off: " & totalcrossed/newtotal & "</font></td></tr>"
'end if
'mytable=mytable & "<tr bgcolor=#000000><td colspan=2 ><font color=#ffffff>Waiting Data: </font></td><td><font color=#ffffff>" & waitingdata & "</font></td></tr>"
mytable=mytable & "</table>"
'response.write "</table>"
'call writetoexcel(mytable)
mybody=mybody &  mytable

        response.write mybody
'Doc.ImportFromUrl mybody, "scale=0.6; hyperlinks=true; drawbackground=true"

      

end if

    if cmd2="employee" then
    response.write  "<b>Employee Report<br>"
  
		sql="select count(id) as mycount from anpossibleviolators where " 
		sqlwhere = " mydatestage2 >='" & cisodate(mydate)
	
	sqlwhere=sqlwhere & "' and mydatestage2<='"  & cisodate(mydate) &  " 23:59' and possibleviolator=1 and confirmedviolator=0"
	sql1=sql & sqlwhere
	
			sqlwhere= " mydatestage2>='" & cisodate(mydate)
	

	sqlwhere=sqlwhere & "' and mydatestage2<='"  & cisodate(mydate) & " 23:59'"
	sql2=sql & sqlwhere
	mytable=   "Yesterday<br><table border=1><tr><td>Employee</td><td>PV but not CV</td><td>stage 2 checked</td></tr>"
	sqlemp="select * from useraccess"
	openrs rsemp,sqlemp
	i=0
	tstage1=0
	tstage2=0
	do while not rsemp.eof 
		 sqlstage1=sql1 & " and useridpv=" & rsemp("id")
		 
		 openrs rsstage1,sqlstage1
		 stage1count=rsstage1("mycount")
		 tstage1=tstage1+rsstage1("mycount")
		 closers rsstage1
		 sqlstage2=sql2 & " and useridcv=" & rsemp("id")
		 
		 openrs rsstage2,sqlstage2
		 stage2count=rsstage2("mycount")
		 tstage2=tstage2+rsstage2("mycount")
		 closers rsstage2
		'mytable=mytable &  "<tr><td>x</td><td>1</td><td>2</td></tr>"
		if stage1count<>0 or stage2count<>0 then mytable=mytable &    "<tr><td>" & rsemp("userid") & "</td><td>" & stage1count & "</td><td>" & stage2count & "</td></tr>"
	rsemp.movenext
	i=i+1
	loop	
	mytable=mytable & "<tr><td>Totals</td><td>" & tstage1 & "</td><td>" & tstage2 & "</td></tr>"
	mytable=mytable & "</table>"
	response.write  mytable 
	closers rsemp  
	
	response.write "<Br>Employee report - last 2 weeks"
	sql="select count(id) as mycount from anpossibleviolators where " 
	mydatefrom=dateadd("ww",-2,mydate)
		sqlwhere = " mydatestage2 >='" & cisodate(mydatefrom)
	
	sqlwhere=sqlwhere & "' and mydatestage2<='"  & cisodate(mydate) &  " 23:59' and possibleviolator=1 and confirmedviolator=0"
	sql1=sql & sqlwhere
	
			sqlwhere= " mydatestage2>='" & cisodate(mydatefrom)
	

	sqlwhere=sqlwhere & "' and mydatestage2<='"  & cisodate(mydate) & " 23:59'"
	sql2=sql & sqlwhere
	'wscript.echo sql2
	mytable=   "<br><br><table border=1><tr><td>Employee</td><td>PV but not CV</td><td>stage 2 checked</td></tr>"
	sqlemp="select * from useraccess"
	openrs rsemp,sqlemp
	i=0
	tstage1=0
	tstage2=0
	do while not rsemp.eof 
		 sqlstage1=sql1 & " and useridpv=" & rsemp("id")
		 
		 openrs rsstage1,sqlstage1
		 stage1count=rsstage1("mycount")
		 tstage1=tstage1+rsstage1("mycount")
		 closers rsstage1
		 sqlstage2=sql2 & " and useridcv=" & rsemp("id")
		 
		 openrs rsstage2,sqlstage2
		 stage2count=rsstage2("mycount")
		 tstage2=tstage2+rsstage2("mycount")
		 closers rsstage2
		'mytable=mytable &  "<tr><td>x</td><td>1</td><td>2</td></tr>"
		if stage1count<>0 or stage2count<>0 then mytable=mytable &    "<tr><td>" & rsemp("userid") & "</td><td>" & stage1count & "</td><td>" & stage2count & "</td></tr>"
	rsemp.movenext
	i=i+1
	loop	
	mytable=mytable & "<tr><td>Totals</td><td>" & tstage1 & "</td><td>" & tstage2 & "</td></tr>"
	mytable=mytable & "</table>"
	response.write   mytable 
	closers rsemp  
	sql="select deletedreason,datedeleted,u.userid as userid2, i.userid as userid1,d.userid as deleteduser from anpossibleviolators left join useraccess u on u.id=anpossibleviolators.useridcv left join useraccess i on i.id=anpossibleviolators.useridpv left join useraccess d on d.id=anpossibleviolators.deletedby  where deletedby is not null and anpossibleviolators.datedeleted>=DATEDIFF(dd,0,'" & cisodate(mydate) & "') order by u.userid"
   openrs rs,sql
   if not rs.EOF and not rs.BOF then
   mytable="<br>Deleted<table border=1><tr><th>Employee First Stage</th><th>Employee 2nd stage</th><th>Reason Deleted</th><th>Deleted By</th></tr>"
   do while not rs.EOF 
   mytable=mytable & "<tr><td>" & rs("userid1") & "</td><td>" & rs("userid2") & "</td><td>" & rs("deletedreason") & "</td><td>" & rs("deleteduser") & "</td></tr>"
      
   rs.movenext
   loop
   mytable=mytable & "</table><br><br>"
   response.write mytable
   end if
   
   closers rs
    end if
         %>
</body></html> 
