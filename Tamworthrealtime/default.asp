﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">



    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <style>
    body {
        padding-top: 70px;
        /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
    }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
        <title>Real Time Data</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script src="//code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>
		<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css"/>
        <style>
            *{
                font-family: arial;
            }
        </style>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
				var table = $('#datatables').DataTable( {
					ajax: "dataset.asp",
					"sPaginationType":"full_numbers",
					"aaSorting": [[1, "desc"]],
					"iDisplayLength": 25,
					"bJQueryUI": true,
                    responsive:true
				} );
				 
				setInterval( function () {
					table.ajax.reload( null, false ); // user paging is not reset on reload
				}, 60000 );

			
            })
        </script>
    </head>
    <body>
       <div class="container">

        <div class="row">
            <table id="datatables" class="display dt-responsive nowrap">
                <thead>
                    <tr>
						
						
						<th>Plate</th>
                         <th>Car Details</th>
						<th>Site</th>
						<th>Start Date</th>
						
						<th>Minutes</th>
                        <th>Reason</th>
                       
                    </tr>
                </thead>
                <tbody>
				</tbody>
            </table>
        </div>
           </div>

            <script src="js/bootstrap.min.js"></script>
    </body>
</html>