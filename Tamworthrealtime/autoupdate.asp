<!DOCTYPE html>
<html>
    <head>
        <title>DataTables</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script src="//code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>
		<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css"/>
        <style>
            *{
                font-family: arial;
            }
        </style>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
				var table = $('#datatables').DataTable( {
					ajax: "dataset.asp",
					"sPaginationType":"full_numbers",
                    "aaSorting":[[2, "desc"]],
                    "bJQueryUI":true
				} );
				 
				setInterval( function () {
					table.ajax.reload( null, false ); // user paging is not reset on reload
				}, 60000 );

			
            })
        </script>
    </head>
    <body>
        <div>
            <table id="datatables" class="display">
                <thead>
                    <tr>
						<th>DateInput</th>
						<th>IdIn</th>
						<th>IdOut</th>
						<th>Plate</th>
						<th>Site</th>
						<th>StartDate</th>
						<th>EndDate</th>
						<th>Duration</th>
                    </tr>
                </thead>
                <tbody>
				</tbody>
            </table>
        </div>
    </body>
</html>