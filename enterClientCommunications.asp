  <!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="admininclude.asp"-->
<!--#include file="image.asp"-->
<LINK href="adminstylesheet.css" rel=stylesheet type=text/css>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
  $(function() {
    $( ".datepicker").datepicker({ dateFormat: "yy-mm-dd" }).val();    
  });
  </script>

<span class=noPrint>
<div id=header><a href=admin.asp?cmd=adminhome><img src="logo.jpg"  border="0" class=logo></a>
    <%if session("admin")="ok" then %>
        <div class=hright><a href="admin.asp?cmd=adminlogout" class=button>Log Out</a></div>
        <div class=welcome>Welcome <%= session("username") %></div>
        <div class=pcnheader>
            <form method=post action=admin.asp?cmd=search2>
            Enter PCN/Plate:<input type=text name=pcn><input type=submit  value="Go" class=button>
            </form>
        </div>
    <%end if%>
</div>
</span>
<% 
call checklogin 
if session("admin")="ok" and showadminmenu<>1 then
    displaymenu = 1
	 call getadminmenu
	showadminmenu=1
end if

    if request("sbmt")<>"" then
        clientid = request("clientid")
        note = request("note")
        
        if clientid <> "All" and clientid <> "" and isnumeric(clientid) then whereClause = " WHERE id = " & tosql(clientid,"Number") else whereClause = ""
        sql = "INSERT INTO clientcommunications (note,userid) VALUES (" & tosql(note,"Text") & "," & session("userid") & ")"
        objconn.execute sql
        openrs rs, "SELECT SCOPE_IDENTITY()"
        commid = rs(0)
        sql = "INSERT INTO map_client_communications (clientid, communicationid,userid) SELECT id, " & tosql(commid,"Number") & "," & session("userid") & " FROM clientusers" & whereClause
        objconn.execute sql
    end if
    %>
<script type="text/javascript">
    function validateForm() {
        return true;
    }
</script>
<style type="text/css">
    .frm {width:400px; margin:10px auto;}
    .frm .lbl {display:inline-block;min-width:100px;max-width:100px;}
    .frm input {margin:10px 0px;}
</style>
<form method="post" class="frm">
    <h1>Enter Client Communications</h1>
    <span class="lbl">Client:</span> 
    <select name ="clientID">
        <option value="">Select</option>
        <option value="All">All</option>
    <%
        openrs rss, "SELECT * FROM [clientusers] ORDER BY userid"
        do while not rss.eof                
    %>
        <option value="<%=rss("id")%>"><%=rss("userid") %></option>
    <%
            rss.movenext
        loop
        closers rss
    %>
    </select>
    <br /><br />
    <span class="lbl">Note:</span>
    <textarea name="note"></textarea>
    <br />
    <input type="submit" name="sbmt" value="Send" />

</form>

<%
    sql =  "SELECT m.date, u.userid, c.note FROM clientcommunications c  INNER JOIN map_client_communications m ON c.id = m.communicationid INNER JOIN clientusers u ON m.clientid = u.id ORDER BY m.Date"
    %><div style="width:400px;max-width:95%;margin:10px auto;"><%
    outputtable(sql)
    %></div><%
%>