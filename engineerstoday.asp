  <% Server.ScriptTimeout = 3600000 %>
		<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%
 pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 



%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<html>
<title>Traffic Guard Engineers</title>
<script type="text/javascript">
<!-- Begin

function reFresh() {
  location.reload(true)
}
/* Set the number below to the amount of delay, in milliseconds,
you want between page reloads: 1 minute = 60000 milliseconds. */
window.setInterval("reFresh()",60000);
// End -->
</script>
<style>
p{
	font-family: Arial, Helvetica, sans-serif;
}
</style>
<body>
<div id="engineers2">
<%
sql="select engineername,e.engineerid,arrivaldate,closeddate,sitecode,[name of site] as sitename,visitid from engineervisits e left join issueengineers ie on ie.engineerid=e.engineerid left join siteissues si on si.id=e.issueid left join [site information] s on s.sitecode=si.site where arrivaldate>DATEADD(dd, DATEDIFF(dd,0,getdate()), 0)  order by engineername,arrivaldate desc,issueid"
openrs rs,sql
lastengineer=""
j=1
do while not rs.eof 
engineername=rs("engineername")
if engineername<>lastengineer then 
%>
 <p>
        <b><span style="font-weight: bold; font-size: 11pt"><%=engineername %>
          <% if rs("closeddate")="" or isnull(rs("closeddate")) then %>
           � <%=rs("sitecode") %>  &nbsp; <%=rs("sitename") %>
           
           
           <% end if %>
           </span></b><br /><br />
   <% enddate=now()
   if rs("closeddate")="" or isnull(rs("closeddate")) then %>
      Time on Site                      <%=datediff("n",rs("arrivaldate"),enddate) %> minutes
<br />
Arrival Time                       <%= formatdatetime(rs("arrivaldate"),3) %> 
<br />
<% 
end if
prevclosetime=getprevclosetime(rs("visitid"),rs("engineerid"))
if prevclosetime<>"" and isnull(rs("closeddate")) then
    
 %>

Time to get to site          <%=datediff("n",prevclosetime,rs("arrivaldate")) %> minutes
<br />
   <% end if %>
   
        <%  ' only show if not at first site that still open
        if j=1 and isnull(rs("closeddate")) then
            ' don't show
        else     
       
         %>
Total time on sites           <%=gettotaltimesonsites (rs("engineerid")) %> minutes
   <br /> 
<% end if %>
</p>
<%
lastengineer=engineername
end if
j=j+1
rs.movenext 
loop
'response.Write "<br>Current Time:" & now()

closers rs
function getprevclosetime(visitid,engineerid)
getprevclosetime=""
'Previous site close time � Arrival time at current site
sqlc="select closeddate from engineervisits where engineerid=" & engineerid & " and visitid<" & visitid & " and  arrivaldate>DATEADD(dd, DATEDIFF(dd,0,getdate()), 0) order by arrivaldate desc"
openrs rsc,sqlc
if not rsc.eof and not rsc.bof then getprevclosetime=rsc("closeddate")
closers rsc
end function
function gettotaltimesonsites(engineerid)
sqlt="select arrivaldate,closeddate from engineervisits where arrivaldate>DATEADD(dd, DATEDIFF(dd,0,getdate()), 0) and engineerid=" & engineerid
openrs rst,sqlt
gettotaltimesonsites=0
i=0
do while not rst.eof

arrivaldate=rst("arrivaldate")
closeddate=rst("closeddate")
if closeddate="" or isnull(closeddate) then closeddate=now()
tm=datediff("n",arrivaldate,closeddate)
gettotaltimesonsites=gettotaltimesonsites + tm
i=i+1
rst.movenext
loop
closers rst
end function
%>
</div>
</body>
</html>


<%

%>