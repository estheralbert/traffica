<%

' Example that read an html file and renders it
' to PDF.
' This version is to show the Professional feature
' to read html files.	

' Clear out the existing HTTP header information
Response.Expires = 0
Response.Buffer = TRUE
Response.Clear
Response.ContentType = "application/pdf"

dim PDF

set PDF = server.createobject("aspPDF.EasyPDF")

PDF.License("EasyPDF.lic")

' Set margins for the page
PDF.SetMargins 20,20,20,20

FileName = Server.MapPath("pdf_html_version.htm")
PDF.AddHtml( "file:///" & FileName )

' BinaryWrite is quite slow, if you can use Save Method you will
' get an increase of speed. 

PDF.BinaryWrite
' Generate the PDF document in a file
'PDF.Save Server.MapPath("pdf_html.pdf")

set pdf = nothing

%>

