<!--#include file="openconn.asp"-->

<!--#include file="common.asp"-->
<%
server.scripttimeout=1000
''2007

 

%>
<html>
<head>
<script src="sorttable.js"></script>
 <style>
/* Sortable tables */
body
{
    font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
}
table.sortable thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
}
table.sortable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}
.oddrowcolor{
	background-color:#d4e3e5;
}
.evenrowcolor{
	background-color:#c3dde0;
}


.red
{
 
 font-weight:bold;
 color:Red;   
}

table.sortable2 thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable2 {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
}
table.sortable2 th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable2 td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}
</style>
  
</head>

<body>



<%
userid="147"


sql="exec [summaryoverview]  @summarytype = 'allsites'"

openrs rs,sql



m=month(date)


'response.write "m:" & m
d1=cint(m)-1
if d1<=0 then d1=12+d1

d2=cint(m)-2
'response.write d2 & "<hr>"
if d2<=0 then d2=12+d2

d3=cint(m)-3
if d3<=0 then d3=12+d3
'response.write d2 & "<hr>"
'response.write d1
 %>
 <h4>Overview</h4>
<table class=sortable2><tr><td></td><th>12 Months</th><th><%= monthname(d3) %></th><th><%= monthname(d2) %></th><th><%= monthname(d1) %></th><th><%= monthname(month(date)) %></th><th><%=weekdayname(weekday(rs("last5"))) %></th><th><%=weekdayname(weekday(rs("last4"))) %></th><th><%=weekdayname(weekday(rs("last3"))) %></th><th><%=weekdayname(weekday(rs("last2"))) %></th><th><%=weekdayname(weekday(rs("last1"))) %></th></tr>
<tr><th>DVLA Sent</th><td><%=fn(rs("dvlasentyear")) %></td><td><%=fn(rs("dvlasentm3")) %></td><td><%=fn(rs("dvlasentm2")) %></td><td><%=fn(rs("dvlasentm1")) %></td><td><%=fn(rs("dvlasentm")) %></td><td><%=fn(rs("dvlasentl5")) %></td><td><%=fn(rs("dvlasentl4")) %></td><td><%=fn(rs("dvlasentl3")) %></td><td><%=fn(rs("dvlasentl2")) %></td><td><%=fn(rs("dvlasentl1")) %></td></tr>
<tr><th>PCN Issued</th><td><%=formatnumber((rs("pcnprintedyear")/rs("dvlasentyear"))*100,0) %> %  (<%=fn(rs("pcnprintedyear")) %>)</td><td><%=fn(rs("pcnprintedm3")) %></td><td><%=fn(rs("pcnprintedm2")) %></td><td><%=fn(rs("pcnprintedm1")) %></td><td><%=fn(rs("pcnprintedm")) %></td><td><%=fn(rs("pcnprintedl5")) %></td><td><%=fn(rs("pcnprintedl4")) %></td><td><%=fn(rs("pcnprintedl3")) %></td><td><%=fn(rs("pcnprintedl2")) %></td><td><%=fn(rs("pcnprintedl1")) %></td></tr>
<tr><th>Paid</th><td><%=formatnumber((rs("totalpaidyear")/rs("pcnprintedyear"))*100,0) %> % (<%=fn(rs("totalpaidyear")) %>)</td><td><%=fn(rs("totalpaidm3")) %></td><td><%=fn(rs("totalpaidm2")) %></td><td><%=fn(rs("totalpaidm1")) %></td><td><%=fn(rs("totalpaidm")) %></td><td><%=fn(rs("totalpaidl5")) %></td><td><%=fn(rs("totalpaidl4")) %></td><td><%=fn(rs("totalpaidl3")) %></td><td><%=fn(rs("totalpaidl2")) %></td><td><%=fn(rs("totalpaidl1")) %></td></tr>
<tr><th>Paid �</th><td><%=formatprice(rs("totalpaidamountyear")) %></td><td><%=formatprice(rs("totalpaidamountm3")) %></td><td><%=formatprice(rs("totalpaidamountm2")) %></td><td><%=formatprice(rs("totalpaidamountm1")) %></td><td><%=formatprice(rs("totalpaidamountm")) %></td><td><%=formatprice(rs("totalpaidamountl5")) %></td><td><%=formatprice(rs("totalpaidamountl4")) %></td><td><%=formatprice(rs("totalpaidamountl3")) %></td><td><%=formatprice(rs("totalpaidamountl2")) %></td><td><%=formatprice(rs("totalpaidamountl1")) %></td></tr>
<tr><th>Cancelled</th><td><%=formatnumber((rs("totalcancelledyear")/rs("pcnprintedyear"))*100,0) %> % (<%=fn(rs("totalcancelledyear")) %>)</td><td><%=fn(rs("totalcancelledm3")) %></td><td><%=fn(rs("totalcancelledm2")) %></td><td><%=fn(rs("totalcancelledm1")) %></td><td><%=fn(rs("totalcancelledm")) %></td><td><%=fn(rs("totalcancelledl5")) %></td><td><%=fn(rs("totalcancelledl4")) %></td><td><%=fn(rs("totalcancelledl3")) %></td><td><%=fn(rs("totalcancelledl2")) %></td><td><%=fn(rs("totalcancelledl1")) %></td></tr>
<tr><th>Incoming Correspondence</th><td><%=formatnumber((rs("totalincomingyear")/rs("pcnprintedyear"))*100,0) %> % (<%=fn(rs("totalincomingyear")) %>)</td><td><%=fn(rs("totalincomingm3")) %></td><td><%=fn(rs("totalincomingm2")) %></td><td><%=fn(rs("totalincomingm1")) %></td><td><%=fn(rs("totalincomingm")) %></td><td><%=fn(rs("totalincomingl5")) %></td><td><%=fn(rs("totalincomingl4")) %></td><td><%=fn(rs("totalincomingl3")) %></td><td><%=fn(rs("totalincomingl2")) %></td><td><%=fn(rs("totalincomingl1")) %></td></tr>
<tr><th>Outgoing Correspondence</th><td><%=formatnumber((rs("totaloutgoingyear")/rs("pcnprintedyear"))*100,0) %> % (<%=fn(rs("totaloutgoingyear")) %>)</td><td><%=fn(rs("totaloutgoingm3")) %></td><td><%=fn(rs("totaloutgoingm2")) %></td><td><%=fn(rs("totaloutgoingm1")) %></td><td><%=fn(rs("totaloutgoingm")) %></td><td><%=fn(rs("totaloutgoingl5")) %></td><td><%=fn(rs("totaloutgoingl4")) %></td><td><%=fn(rs("totaloutgoingl3")) %></td><td><%=fn(rs("totaloutgoingl2")) %></td><td><%=fn(rs("totaloutgoingl1")) %></td></tr>
<tr><th>Notes</th><td><%=formatnumber((rs("totalnotesyear")/rs("pcnprintedyear"))*100,0) %> % (<%=fn(rs("totalnotesyear")) %>)</td><td><%=fn(rs("totalnotesm3")) %></td><td><%=fn(rs("totalnotesm2")) %></td><td><%=fn(rs("totalnotesm1")) %></td><td><%=fn(rs("totalnotesm")) %></td><td><%=fn(rs("totalnotesl5")) %></td><td><%=fn(rs("totalnotesl4")) %></td><td><%=fn(rs("totalnotesl3")) %></td><td><%=fn(rs("totalnotesl2")) %></td><td><%=fn(rs("totalnotesl1")) %></td></tr>
<tr><th>Voicemail</th><td><%=formatnumber((rs("totalvoicemailyear")/rs("pcnprintedyear"))*100,0) %> % (<%=fn(rs("totalvoicemailyear")) %>)</td><td><%=fn(rs("totalvoicemailm3")) %></td><td><%=fn(rs("totalvoicemailm2")) %></td><td><%=fn(rs("totalvoicemailm1")) %></td><td><%=fn(rs("totalvoicemailm")) %></td><td><%=fn(rs("totalvoicemaill5")) %></td><td><%=fn(rs("totalvoicemaill4")) %></td><td><%=fn(rs("totalvoicemaill3")) %></td><td><%=fn(rs("totalvoicemaill2")) %></td><td><%=fn(rs("totalvoicemaill1")) %></td></tr>
<tr><th>Reminders</th><td><%=formatnumber((rs("totalremindersyear")/rs("pcnprintedyear"))*100,0) %> % (<%=fn(rs("totalremindersyear")) %>)</td><td><%=fn(rs("totalremindersm3")) %></td><td><%=fn(rs("totalremindersm2")) %></td><td><%=fn(rs("totalremindersm1")) %></td><td><%=fn(rs("totalremindersm")) %></td><td><%=fn(rs("totalremindersl5")) %></td><td><%=fn(rs("totalremindersl4")) %></td><td><%=fn(rs("totalremindersl3")) %></td><td><%=fn(rs("totalremindersl2")) %></td><td><%=fn(rs("totalremindersl1")) %></td></tr>
<tr><th>Debt Letters</th><td><%=formatnumber((rs("totaldebtlettersyear")/rs("pcnprintedyear"))*100,0) %> % (<%=fn(rs("totaldebtlettersyear")) %>)</td><td><%=fn(rs("totaldebtlettersm3")) %></td><td><%=fn(rs("totaldebtlettersm2")) %></td><td><%=fn(rs("totaldebtlettersm1")) %></td><td><%=fn(rs("totaldebtlettersm")) %></td><td><%=fn(rs("totaldebtlettersl5")) %></td><td><%=fn(rs("totaldebtlettersl4")) %></td><td><%=fn(rs("totaldebtlettersl3")) %></td><td><%=fn(rs("totaldebtlettersl2")) %></td><td><%=fn(rs("totaldebtlettersl1")) %></td></tr>

<tr><th>Total Outstanding �</th><td><%=formatprice(rs("totaloutstandingyear")) %></td><td><%=formatprice(rs("totaloutstandingm3")) %></td><td><%=formatprice(rs("totaloutstandingm2")) %></td><td><%=formatprice(rs("totaloutstandingm1")) %></td><td><%=formatprice(rs("totaloutstandingm")) %></td><td><%=formatprice(rs("totaloutstandingl5")) %></td><td><%=formatprice(rs("totaloutstandingl4")) %></td><td><%=formatprice(rs("totaloutstandingl3")) %></td><td><%=formatprice(rs("totaloutstandingl2")) %></td><td><%=formatprice(rs("totaloutstandingl1")) %></td></tr>
<tr><th>Total Outstanding</th><td><%=formatnumber((rs("totaloutstandingnyear")/rs("pcnprintedyear"))*100,0) %> % (<%=fn(rs("totaloutstandingnyear")) %>)</td><td><%=fn(rs("totaloutstandingnm3")) %></td><td><%=fn(rs("totaloutstandingnm2")) %></td><td><%=fn(rs("totaloutstandingnm1")) %></td><td><%=fn(rs("totaloutstandingnm")) %></td><td><%=fn(rs("totaloutstandingnl5")) %></td><td><%=fn(rs("totaloutstandingnl4")) %></td><td><%=fn(rs("totaloutstandingnl3")) %></td><td><%=fn(rs("totaloutstandingnl2")) %></td><td><%=fn(rs("totaloutstandingnl1")) %></td></tr>

</table>


<br />
<h4>12-Month Breakdown</h4>
<%

sqlsites="exec summarybreakdownall @debug=0"
openrs rssites,sqlsites
response.write "<table class=sortable><tr><th>Sitecode</th><th>Name</th><th>Start Date</th><th>Last Date</th><th>Backlog</th><th>PCN Issued</th><th>Off-Line %</th><th>% Paid</th><th>% Cancelled</th><th>Total Paid</th><th>Average Daily Income</th><th>Estimated Loss</th></tr>"

do while not rssites.eof
response.write "<tr><td>" &  rssites("sitecode") & "</td><td>" & rssites("name") & "</td><td>"

if  rssites("morethen1year")=true then
    response.write "Plus one year"
else

   if rssites("startdate")<>"" then response.write formatdatetime(rssites("startdate"),2)
end if
response.write  "</td><td>"
if datediff("d",rssites("lastdate"),now())<=1 then 
    response.write "Today"
else
     if rssites("lastdate")<>"" then response.write formatdatetime(rssites("lastdate"),2)

end if

response.write  "</td><td>" & datediff("d",rssites("lastdate"),date()) & "</td><td>" & rssites("pcnissued") & "</td><td>"
if rssites("lastdate")<>"" then  response.write  formatnumber(100 * (rssites("offline")/datediff("d",rssites("startdate"),rssites("lastdate"))),0)  & "% [" & rssites("offline") & "]"

response.write "</td><td>"  & fn(100*rssites("numberpaid")/rssites("pcnissued")) & "%</td><td>"  & fn(100* rssites("numbercancelled")/rssites("pcnissued")) & "%</td><td>" & formatprice(rssites("totalpaid")) & "</td><td>" & formatpricena(rssites("avgdaily")) & "</td><td>" & formatpricena(rssites("estloss")) & "</td></tr>"

rssites.movenext
loop
closers rssites
response.write "</table>"


 %>


<% 
function fn(number)
if number<>"" then  
   fn=formatnumber(number,2)
else
    fn=0
end if
end function


function gettotalpayments(sitecode)
sql="select count(id) as paymenttotal from payments where right(pcn,3)=" & tosql(sitecode,"text")
openrs rsp,sql
    gettotalpayments=rsp("paymenttotal")
closers rsp
end function
function fn(mystr)
if mystr="" or isnull(mystr) then mystr=0
fn=formatnumber(mystr,0)

end function
function formatprice(namount)
if isnull(namount) or namount="" then namount=0
formatprice="�" & formatnumber(namount,0)
end function
function getavgmonth (npyear)
if npyear="" or isnull(npyear) then npyear=0
da=datediff("d","01/01/" & year(date),date())

getavgmonth=formatnumber((npyear/da)*30,0)
end function


function formatpricena(namount)
if isnull(namount) or namount="" then namount=0
if namount=0 then
    formatpricena="N/A"
else
    formatpricena="�" & formatnumber(namount,0)
end if
end function

%>


</body></html>
