
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 
</head>
<body>

  <% response.buffer=true
Server.ScriptTimeout=200000
pStr = "private, no-cache, must-revalidate"
     ' if session("admin")<>"ok" then response.end
%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->

<%
    if request("sbmt")="" then
    %>
<script type="text/javascript">
    function validateForm() {
        return true;
    }
</script>
<style type="text/css">
    .frm {width:300px; margin:10px auto;}
    .frm .lbl {display:inline-block;min-width:100px;max-width:100px;}
    .frm input {margin:10px 0px;}
</style>

 
<script type="text/javascript">
       $(function() {
               $("#startdate").datepicker({ dateFormat: "yy-mm-dd" }).val()
           $("#enddate").datepicker({ dateFormat: "yy-mm-dd" }).val()
       });
     
   </script>
 



<form method="post" class="frm">
    <span class="lbl">Site:</span>
        <select name="site" id="slctSite">
        <option value="">Select a Site</option>
        <% openrs rs, "select s.sitecode,s.shortdesc from [site information] s left join sitedvlalimit dl on dl.sitecode=s.sitecode where dl.limit is null and s.disabled=0"
            do while not rs.EOF
         %>
            <option value="<%=rs("sitecode")%>"><%=rs("sitecode")%> - <%=rs("shortdesc")%></option>
        <%
            rs.movenext
            loop
         %>
         </select><br />
    <span class="lbl">DVLA Limit:</span><input type="number" name="dvlaLimit" id="txtDvlaLimit"/><br />
       <span class="lbl">Start Date:</span><input type="text" name="startdate" id="startdate"  /><br />
     <span class="lbl">End Date:</span><input type="text" name="enddate" id="enddate"  /><br />
    <input type="submit" name="sbmt" value="Add" onclick="return validateForm();"/>

</form>

<%

    else
        site = request("site") 
        limit = request("dvlaLimit")
        

    startdate=request("startdate")
    enddate=request("enddate")
    startdate=replace(startdate,"-","")
    enddate=replace(enddate,"-","")

    sql="insert into  sitedvlalimit(limit,startdate,enddate,sitecode) values(" & tosql(limit,"Number") & "," & tosql(startdate,"text") & "," & tosql(enddate,"text")  & "," &tosql(site,"Text") & ")"
    response.write sql
    objconn.execute sql
     '   objconn.execute "update [site information] set dvlalimit="&tosql(dvla,"Number")&" where sitecode="&tosql(site,"Text")
        %>
        <script type="text/javascript">
            alert("Site added successfully.");
            window.opener.location.reload();
            window.close();
        </script>
        <%    
    end if


%>