﻿
'on error resume next
' This demostrates the easy to add it on the VBS
ecdebug=0
Dim objconn ,mserver,dbName,UserID_db, PassWD
Dim strCon 
Set objconn=createObject("ADODB.Connection") 
strcon="Driver={SQL Server};Server=db-traffica;Database=traffica;UID=office;PWD=kk4NUPik"
objconn.commandtimeout=10000
objconn.Open strCon

set PDF = createObject("aspPDF.EasyPDF")
PDF.License("EasyPDF.lic") 

'direc="D:\Shared Folders and Data\wwwroot\traffica\dailyreport\"

'if 1=0 then 
PDF.Page "A4", 0
PDF.SetMargins 10, 5, 15,5	
mydate=date()-1


  PDF.AddPDF "D:\Shared Folders and Data\wwwroot\traffica\dailyreportcover.pdf", 0,0, ""
   
      PDF.SetFont "F2", 25, "#000000"
    pdf.AddTextpos 200,400, day(mydate) & " " & monthname(month(mydate)) & " " & year(mydate)  
  pdf.addpage
   strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
   pdf.AddHTML strhtml
	if ecdebug=1 then wscript.echo "28"

myhtml="<P align=center><B><FONT size=6>Daily Report</FONT></B></p> "
myhtml=myhtml & "<P align=center><B>" & day(mydate) & " " & monthname(month(mydate)) & " " & year(mydate) & "</B></p>"
'myhtml=myhtml & "<P>Overview <br>Site Issues <br>Cancellation Summary  </p>"

myhtml=myhtml & "<font size=2><p><B>Summary </B></p><br>"
if ecdebug=1 then wscript.echo "34"
sql="exec spdailysummary @startdate=" & tosql(cisodate(mydate),"text") & ", @enddate=" & tosql(cisodate(mydate)&" 23:59","text")
openrs rs,sql
numtickets=rs("numtickets")
numticketswaiting=rs("numticketswaiting")
numcancelled=rs("numcancelled")
numpaid=rs("numpaid")
ndvlareq=rs("numdvla")
nnewfaults=rs("numfaultsstarted")
numappealsaction=rs("numappealsaction")
nfixedfaults=rs("numfaultsfixed")
numnotes=rs("numnotes")
numvoicemails=rs("numvoicemail")
numincoming=rs("numincoming")
numletters=rs("numletters")
outstandingfaults=rs("outstandingfaults")
cafourteen=rs("ca14")
casix=rs("ca6")
siteswaiting=rs("siteswaiting")
cpaid=rs("cpaid")
manualpaid=rs("manualpaid")
otherpaid=rs("otherpaid")
waitingresponse3=rs("waitingresponse3")
waitingreissue3=rs("waitingreissue3")
waitingreissue8=rs("waitingreissue8")
waitingdata4=rs("waitingdata4")
outstandingsemails3=rs("outstandingemails3")
outstandingsemails1=rs("outstandingemails1")
outstandingsemails2h=rs("outstandingemails2h")
pvchanged=rs("pvchanged")
closers rs
if ecdebug=1 then wscript.echo "num tikctes: " & numtickets
myhtml=myhtml & "<table><tr><b></b><td>&nbsp;</td></tr></table><table style='font-weight:normal'><tr><Td>DVLA Requested</td><td>" & ndvlareq & "</td></tr>"
myhtml=myhtml & "<tr><td>Tickets Printed</td><td> " &numtickets & "</td></tr>"
myhtml=myhtml & "<tr><td>Tickets Waiting to be Printed</td><td> " &numticketswaiting & "</td></tr>"
myhtml=myhtml & "<tr><td>Paid Tickets</td><td>" & numpaid & "</td></tr>"
myhtml=myhtml & "<tr><td>Cancelations</td><td>" & numcancelled & "</td></tr>"

myhtml=myhtml & "<tr><td>Notes added</td><td>" &numnotes& "</td></tr>"
myhtml=myhtml & "<tr><td>Voicemails</td><td>" & numvoicemails& "</td></tr>"
myhtml=myhtml & "<tr><td>Outstanding emails 3 days old</td><td>" & outstandingsemails3& "</td></tr>"
myhtml=myhtml & "<tr><td>Outstanding emails 1 day old</td><td>" & outstandingsemails1& "</td></tr>"
myhtml=myhtml & "<tr><td>Outstanding emails 2 hours old</td><td>" &outstandingsemails2h& "</td></tr>"
myhtml=myhtml & "<tr><td>Possible Violator Plates changed</td><td>" &pvchanged& "</td></tr>"

'myhtml=myhtml & "</table>"
myhtml=myhtml & "<tr><td colspan=2><br><br><P><B>Faults & Critical Summary </B></p></td></tr>"
'myhtml=myhtml & "<table>"
myhtml=myhtml & "<tr><td>New Faults Opened</td><td>" & nnewfaults& "</td></tr>"
myhtml=myhtml & "<tr><td>Faults Fixed</td><td>" & nfixedfaults& "</td></tr>"
myhtml=myhtml & "<tr><td>Outstanding Faults</td><td>" & outstandingfaults& "</td></tr>"
    myhtml=myhtml & "<tr><td><p>Appeals waiting to be actions</td><td>" & numappealsaction& "</td></tr>"
    dim asiteswritten(500)
    sitewritten=0
    'sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from violators)"
    sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from dvlarep) and sitecode<700"
    openrs rssites,sqlsites
    numsiteswaiting=0
    siteswaiting=""
    do while not  rssites.eof
    numsiteswaiting=numsiteswaiting+1
    siteswaiting=siteswaiting & rssites("sitecode") & ", "
    asiteswritten(sitewritten)=rssites("sitecode")
    sitewritten=sitewritten+1
    rssites.movenext
    loop
    closers rssites
    asiteswaiting=split(siteswaiting,",")
    for i=lbound(asiteswaiting) to ubound(asiteswaiting)
    asiteswaiting(i)=trim(asiteswaiting(i))

    next
    
    snvsite=""
    sqlsnv="exec sitesnoviolations"
openrs rssnv,sqlsnv
snv= "<table border=0><tr><td>Site</td><td>Site Description<td>Last Date of Violation</td></tr>"
do while not rssnv.eof 
snv=snv &  "<tr><td>" & rssnv("sitecode") & "</td><td>" & getsitedesc(rssnv("sitecode")) & "</td><td>" & rssnv("lastviolation") & "</td></tr>"
snvsite=snvsite & rssnv("sitecode") & ","

rssnv.movenext
loop

closers rssnv
snv=snv &  "</table>"


closers rssnv
 snvsite=left(snvsite,len(snvsite)-1)
 asnv=split(snvsite,",")
    
    'if ecdebug=1 then wscript.echo "100"
   'noaccess
    sqlna="select site,startdate,DATEDIFF(d, startdate, getdate()) as days from siteissues where site<700 and issue='No Access' and enddate is null order by site"
openrs rsna,sqlna
if not rsna.eof and not rsna.bof then
    'response.write "<tr><td colspan=2><table cellpadding=10><tr>"
    x=0
    
    do while not rsna.eof
     'response.write   "<td>" & rsna("site") & "(" &getdaysstage2(rsna("site")) & ") </td> "
   ' asiteswritten(sitewritten)=rsna("site")
   ' sitewritten=sitewritten+1
    x=x+1
    'if x=4 then   response.Write "</tr><tr>"
    
    rsna.movenext
    loop
     '  response.Write "</tr></table>"
end if    
closers rsna
    'doubleentry
    sqlna="select site,startdate,DATEDIFF(d, startdate, getdate()) as days from siteissues where site<700 and issue='Double Entry' and enddate is null order by site"
    openrs rsna,sqlna
    if not rsna.eof and not rsna.bof then
       
        x=0
        
        do while not rsna.eof
        
       asiteswritten(sitewritten)=rsna("site")
        sitewritten=sitewritten+1
       x=x+1
       ' if x=4 then   response.Write "</tr><tr>"
        
        rsna.movenext
        loop
        '   response.Write "</tr></table>"
    end if    
    closers rsna
   
     sqlna="select site,startdate,DATEDIFF(d, startdate, getdate()) as days from siteissues where site<700 and issue='Suspended' and enddate is null order by site"
    openrs rsna,sqlna
    if not rsna.eof and not rsna.bof then
       ' response.write "<tr><td colspan=2><table cellpadding=10><tr>"
        x=0
        
        do while not rsna.eof
       '  response.write   "<td>" & rsna("site") & "(" &getdaysstage2(rsna("site")) & ") </td> "
        asiteswritten(sitewritten)=rsna("site")
        sitewritten=sitewritten+1
        x=x+1
       ' if x=4 then   response.Write "</tr><tr>"
        
        rsna.movenext
        loop
        '   response.Write "</tr></table>"
    end if    
    closers rsna
     'criticcalalrm 21 days
    sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from violators where [date of violation]>getdate()-14 group by site)"
    sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from dvlarep where  CONVERT(DATETIME,dvlarep.offence,105)>getdate()-21 group by site) and sitecode not in(select site from anpossibleviolators where d>getdate()-21 and stage=2)and sitecode<700"
    sqlsites="exec GetCriticalAlarms @days=21"
  
    openrs rssites,sqlsites
    numsites=0
    dim asites(200)
    i=0
    criticalsites=""
    cs=""
    do while not  rssites.eof
    if not elementinarray(asiteswritten,rssites("sitecode"),0) and not elementinarray(asnv,rssites("sitecode"),0) then 
         asiteswritten(sitewritten)=rssites("sitecode")
        sitewritten=sitewritten+1
    numsites=numsites+1
    'if checkifinpv(rssites("sitecode"),21)=true then
     '   bbold=0
      '  if checkifinpvstage2(rssites("sitecode"),21)=true then bbold=1
       '     criticalsites=criticalsites & "<font class=purple><b>"
        '    if bbold=1 then  criticalsites=criticalsites & "</b><del>"
         '        criticalsites=criticalsites  & rssites("sitecode") 
          '       cs=cs & rssites("sitecode") 
           'if bbold=1 then  criticalsites=criticalsites  & "</del>"
           'criticalsites=criticalsites & "</b></font>" &", "
           'cs=cs & ","
        'else
            criticalsites=criticalsites & rssites("sitecode") & ", "
            cs=cs & rssites("sitecode") &", "
        'end if
       
    asites(i)=rssites("sitecode")
    i=i+1
    end if
    rssites.movenext
    loop
    closers rssites
    if criticalsites<>"" then   criticalsites=left(criticalsites,len(criticalsites)-2)
    if cs<>"" then cs=left(cs,len(cs)-2)
    criticalsites21=split(cs,",")
    ca21=numsites
    'criticcalalrm 14 days
    sqlsites="select sitecode from [site information] where disabled=0 and sitecode<700 and sitecode not in(select site from violators where [date of violation]>getdate()-14 group by site)"
    sqlsites="select sitecode from [site information] where disabled=0 and sitecode<700 and sitecode not in(select site from dvlarep where  CONVERT(DATETIME,dvlarep.offence,105)>getdate()-14 group by site) and sitecode not in(select site from anpossibleviolators where d>getdate()-14 and stage=2)"
    sqlsites="exec GetCriticalAlarms @days=14"
    openrs rssites,sqlsites
    numsites=0
   
    i=0
    criticalsites=""
    cs=""
    do while not  rssites.eof
    if not elementinarray(asiteswritten,rssites("sitecode"),0) and not elementinarray(asnv,rssites("sitecode"),0) then 
         asiteswritten(sitewritten)=rssites("sitecode")
        sitewritten=sitewritten+1
    numsites=numsites+1
    'if checkifinpv(rssites("sitecode"),14)=true then
     '   bbold=0
      '  if checkifinpvstage2(rssites("sitecode"),14)=true then bbold=1
       '     criticalsites=criticalsites & "<font class=purple><b>"
        '    if bbold=1 then  criticalsites=criticalsites & "</b><del>"
         '        criticalsites=criticalsites  & rssites("sitecode") 
          '       cs=cs& rssites("sitecode") & ", "
          ' if bbold=1 then  criticalsites=criticalsites  & "</del>"
           'criticalsites=criticalsites & "</b></font>" &", "
       ' else
            criticalsites=criticalsites & rssites("sitecode") & ", "
             cs=cs& rssites("sitecode") & ", "
       ' end if
       
    asites(i)=rssites("sitecode")
    i=i+1
    end if
    rssites.movenext
    loop
    closers rssites
     cs=left(cs,len(cs)-2)
    criticalsites14=split(cs,",")
    criticalsites=left(criticalsites,len(criticalsites)-2)
    cafourteen=numsites
    '6days
    sqlsites="select sitecode from [site information] where disabled=0 and sitecode<700 and sitecode not in(select site from dvlarep where  CONVERT(DATETIME,dvlarep.offence,105)>getdate()-6 group by site) and sitecode not in(select site from anpossibleviolators where d>getdate()-6 and stage=2)"
  sqlsites="exec GetCriticalAlarms @days=6"
    openrs rssites,sqlsites
    numsites=0
    criticalsites=""
    cs=""
    do while not  rssites.eof
    'chec array asites
    if not elementinarray(asites,rssites("sitecode"),0) and not elementinarray(asiteswaiting,rssites("sitecode"),0) and not elementinarray(asnv,rssites("sitecode"),0) and not elementinarray(asiteswritten,rssites("sitecode"),0) then 
     asiteswritten(sitewritten)=rssites("sitecode")
        sitewritten=sitewritten+1
    numsites=numsites+1
       ' if checkifinpv(rssites("sitecode"),6)=true then
        '
             bbold=0
       ' if checkifinpvstage2(rssites("sitecode"),6)=true then bbold=1
        '    criticalsites=criticalsites & "<font class=purple><b>"
         '   if bbold=1 then  criticalsites=criticalsites & "</b><del>"
          ' criticalsites=criticalsites  & rssites("sitecode") 
           '     cs=cs& rssites("sitecode") & ", "
           'if bbold=1 then  criticalsites=criticalsites  & "</del>"
           'criticalsites=criticalsites & "</b></font>" &", "
       ' else
            criticalsites=criticalsites & rssites("sitecode") & ", "
                 cs=cs& rssites("sitecode") & ", "
       ' end if
    end if
    rssites.movenext
    loop
    closers rssites
    criticalsites=left(criticalsites,len(criticalsites)-2)
     cs=left(cs,len(cs)-2)
    criticalsites6=split(cs,",")
    casix=numsites
    'comerror
    cerrorsites=""
    
    sqlc="select distinct(site) as site from plateimages  where date>DATEADD(m, -1, getdate()) "
    openrs rsc,sqlc
        do while not rsc.eof 

    sqls="SELECT count(id) as count from ftpimages where site='" & rsc("site")  & "' and datetime>DATEADD(hh, -2, getdate())"
    if hour(now)>17 then    sqls="SELECT count(id) as count from ftpimages where site='" & rsc("site")  & "' and datetime>DATEADD(hh, -2, '" & cisodate(date) & " 17:00')"
if hour(now)<9 then  sqls="SELECT count(id) as count from ftpimages where site='" & rsc("site")  & "' and datetime>DATEADD(hh, -2, '" & cisodate(date-1) & " 17:00')"

    openrs rss,sqls
    count=rss("count")
    if count=0 then 
       'if not elementinarray(asiteswritten,rsc("site"),0) then 
        cerrorsites=cerrorsites & rsc("site") & ","
        asiteswritten(sitewritten)=rsc("site")
        sitewritten=sitewritten+1
     '   end if
    end if
    closers rss
    rsc.movenext
    loop

    if cerrorsites<>"" then cerrorsites=left(cerrorsites,len(cerrorsites)-1)
    'wscript.echo cerrorsites
    
    acerrorsites=split(cerrorsites,",")
    'wscript.echo acerrorsites
    comerror=ubound(acerrorsites)+1 
myhtml=myhtml & "<tr><td>Critical Alarms over 21 days</td><td>" & ca21 & "</td></tr>"
    
myhtml=myhtml & "<tr><td>Critical Alarms over 14 days</b></td><td>" & cafourteen & "</td></tr>"
myhtml=myhtml & "<tr><td><b>Critical Alarms over 6 days</td><td>" & casix & "</td></tr>"

        
 myhtml=myhtml & "<tr><td>Communication error</td><td>" & comerror &  "</td></tr>"
 myhtml=myhtml & "<tr><td>Sites waiting to go-live</td><td>" & numsiteswaiting &  "</td></tr>"
'myhtml=myhtml & "</table>"
myhtml=myhtml & "<tr><td colspan=2><P><B>Financials</B></p></td></tr>"
myhtml=myhtml & "<tr><td>Automated Payments</td><td>" & cpaid & "</td></tr>"
myhtml=myhtml & "<tr><td>Manual Payments</td><td>" & manualpaid & "</td></tr>"
myhtml=myhtml & "<tr><td>Other Payments</td><td>" & otherpaid & "</td></tr>"
'Set objconnt=CreateObject("ADODB.Connection") 
'strcont="Driver={SQL Server};Server=94.229.136.6;Database=triangle;UID=triangle;PWD=tresther55"
'objconnt.commandtimeout=90
'objconnt.Open strCont
'sqlt="select count(id) as trianglepayments from trianglepayment where datetime>'" & cisodate(mydate) & "' and datetime <='" & cisodate(mydate)& " 23:59'"
'wscript.echo sqlt

'set rst=objconnt.Execute(sqlt)
'trianglepayments=rst("trianglepayments")
'wscript.echo trianglepayments
'rst.close
'set rst=nothing
'objconnt.Close 
'myhtml=myhtml & "<tr><td><b>Triangle Payments</b></td><td>" & trianglepayments & "</td></tr>"


myhtml=myhtml & "</table>"

myhtml=myhtml & "<br><b>Communication</b><table>"

myhtml=myhtml & "<tr><td>Scanned</td><td>" & numincoming& "</td></tr>"
myhtml=myhtml & "<tr><td>Created</td><td>" & numletters& "</td></tr>"
myhtml=myhtml & "<tr><td>Waiting for response (over 3-days)</td><td>" & waitingresponse3& "</td></tr>"
myhtml=myhtml & "<tr><td>Waiting for re-Issue (over 3-days)</td><td>" & waitingreissue3 & "</td></tr>"
myhtml=myhtml & "<tr><td>Waiting for re-Issue (over 8-days)</td><td>" & waitingreissue8 & "</td></tr>"




myhtml=myhtml & "<tr><td>Waiting for data (over 4-days)</td><td>" &waitingdata4 & "</td></tr></table>"
 
 



myhtml=myhtml & "<br><b>Monthly Statistics</b><table>"
sql="exec spmonthlyreport " &  month(mydate) & "," & year(mydate)
openrs rs,sql
myhtml=myhtml & "<tr><th>Total Dvla Sent</th><td>" & rs("dvla") & "</td></tr>"
myhtml=myhtml & "<tr><th> Total Tickets Issued</th><td>" & rs("tickets") & "</td></tr>"
myhtml=myhtml & "<tr><th>Total Cancellation</th><td>" & rs("cancellations") & "</td></tr>"

closers rs
   pdf.addhtml myhtml
myhtml = "<!-PAGE BREAK>"
   pdf.addhtml myhtml
   
'IF 1=0 THEN    
   
    strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
   pdf.AddHTML strhtml

'if ecdebug then wscript.echo "372"
myf= "<b>Fault Report Summary</b>"
myf=myf &  "<table width=520><tr><th></th><th>Site Description</th><th>Days since violation printed</th><tr><td><b>21 day Critical:</b>" & ubound(criticalsites21)+1 & "</td></tr>"
for i=0 to ubound(criticalsites21)

myf=myf &  "<tr><td>" & criticalsites21(i) & "</td><td>" & getsitedesc(criticalsites21(i)) & "</td><td>" & getsitelastprinted(criticalsites21(i)) & "</td> </tr>"
next

myf=myf &  "<tr><td><br><b>14 day Critical:</b>" & ubound(criticalsites14)+1 & "</td></tr>"
for i=0 to ubound(criticalsites14)

myf=myf &  "<tr><td>" & criticalsites14(i) & "</td><td>" & getsitedesc(criticalsites14(i)) & "</td><td>" & getsitelastprinted(criticalsites14(i)) & "</td> </tr>"
next

myf=myf &  "<tr><td><br><b>6 day Critical:</b>" & ubound(criticalsites6)+1 & "</td></tr>"
for i=0 to ubound(criticalsites6)

myf=myf &  "<tr><td>" & criticalsites6(i) & "</td><td>" & getsitedesc(criticalsites6(i)) & "</td><td>" & getsitelastprinted(criticalsites6(i)) & "</td> </tr>"
next

myf=myf &  "</table>"

'wscript.echo myf
pdf.AddHTML myf

if ecdebug then wscript.echo "397"
i=0
myf=""
sql=" select distinct site,DATEDIFF(day,contractdate,getdate()) as contractdate   from siteissues si left join [Site Information] s on si.site=s.SiteCode  where ( issue='New Installation') and enddate is null order by site"
openrs rs,sql
myf=myf & "<table><tr><th>Site</th><th>Site Description</th><th>Days since Contract</th></tr>"
do while not rs.eof 
myf=myf & "<tr><td>" & rs("site") & "</td><td>" & getsitedesc(rs("site")) & "</td><td>" & rs("contractdate") & "</td></tr>"
i=i+1
rs.movenext
loop

closers rs
myf=myf & "</table>"

myf1="<br><br><b>Sites Waiting to be installed:" & i & " </b><br>"



pdf.addhtml myf1 &  myf
i=0

myf=""
sql="select distinct site,DATEDIFF(day,startdate,getdate()) as dayssuspended   from siteissues si  where ( issue='Suspended') and enddate is null order by site"
openrs rs,sql
myf=myf & "<table><tr><th>Site</th><th>Site Description</th><th>Days since Suspended</th></tr>"
do while not rs.eof 
myf=myf & "<tr><td>" & rs("site") & "</td><td>" & getsitedesc(rs("site")) & "</td><td>" & rs("dayssuspended") & "</td></tr>"
i=i+1
rs.movenext
loop

closers rs
myf=myf & "</table>"
myf1="<br><br><b>Sites Suspended:" & i & " </b><br>"

pdf.addhtml myf1 & myf
if ecdebug then wscript.echo "425"

i=ubound(asnv)
myf1="<br><br><b>Sites with no violations:" & i & " </b><br>"
pdf.AddHTML myf1 & snv
if 1=0 then  ''ec commented

'wscript.echo "376"
myhtml = "<!-PAGE BREAK>"
   pdf.addhtml myhtml
    strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
      pdf.AddHTML strhtml

   
    pdf.AddHTML "http://traffica2.cleartoneholdings.co.uk/top10.asp"
  
   
  ' pdf.AddHTML "<b>Overview</b><br>"
   'pdf.AddHTML "http://traffica2.cleartoneholdings.co.uk/trafficsummary2.asp?print=1"
   
  end if 
   pdf.AddHTML "<!-PAGE BREAK>"
    strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
      pdf.AddHTML strhtml

   
   if 1=1 then  '' ec commented
   

pdf.addhtml "<b>Excel Analysis for the last 20 days</b><br>"
      
      
      
		totale=0
		total1=0
		total2=0
		totalred=0
		totalcrossed=0
		waitingdata=0
		count2=0
		count1=0
		countr=0
		counte=0
		'mymonth=request("month")
		'myear=request("year")
		fromdate=dateadd("d",-20,mydate)
		todate=mydate
		'daysinm=getdaysinmonth(mymonth,myear)
	'	daysinm=2
		'if len(mymonth)<2 then mymonth="0" & mymonth
		mytable= "<b>Data Analysis Overview From:" & fromdate & " To: " &  todate &  "</b>"
		
mytable=mytable & "<table border=1 cellpadding=0 cellspacing=0>"
mytable=mytable & "<tr><td>site <br>code</td><td>Desc</td>"
dfrom=fromdate
do while dfrom<=todate
mytable=mytable & "<td><b>" & day(dfrom) & "</b></td>"
dfrom=dateadd("d",1,dfrom)
loop

mytable=mytable & "</tr>"
'response.write mytable
 if ecdebug=1 then wscript.echo "428"
sqlsite="select * from siteinformationanal where sitecode<700 order by sitecode"
sqlsite="select s.SiteCode, s.[Name of Site], s.[Borough], s.Address1, s.Address2, s.Address3, s.Town, s.Postcode, s.siteref, s.RequestType, s.telephone, s.fax, s.email, s.storemanager, s.contactnumber, s.contactemail, s.adsltelephone, s.adslip, s.adslusername, s.adslpassword, s.adsllinebyclient, s.disabled,s.vtime, s.disabledletter, s.shortdesc, sa.notes from [Site Information] s left join siteinformationanal sa on s.SiteCode=sa.sitecode where s.SiteCode<700  and disabled=0 order by s.sitecode"

if ecdebug=1 then wscript.echo "524"
openrs rssite,sqlsite
do while not rssite.eof
snotes=rssite("notes")
'response.write "<hr>" & snotes
	 sqlcc="select * from cancelanal where alldays=1 and site=" & rssite("sitecode")
	 openrs rscc,sqlcc
	 noshowrow=0
	 if not rscc.eof and not rscc.bof then noshowrow=1
	 closers rscc
	 if noshowrow=0 then
	 mytable=mytable & "<tr><td><b>'" & rssite("sitecode") & "</b></td><td>" & rssite("shortdesc") & "</td>"
	' response.write "<tr><!--<td>" & rssite("shortdesc") & "</td>-->"
	 
	 ' response.write "<td><textarea name=notes-" & rssite("sitecode") & ">" & snotes  & "</textarea></td>"

dfrom=fromdate
do while dfrom<=todate
	 			 myear=year(dfrom)
				 mymonth=month(dfrom)
				 myday=day(dfrom)

	if len(myday)<2 then myday="0" & myday
	if len(mymonth)<2 then mymonth="0" & mymonth
	sqlwhere= "site='" & rssite("sitecode") & "' and d >="
	sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
	sqlwhere=sqlwhere & " and d<=cast('" & myear & mymonth & myday & " 23:59' as datetime)"
		sites="'" & rssite("sitecode") & "'"
		df="'" & myear & mymonth & myday & " 00:00'"
		dt="'" & myear & mymonth & myday & " 23:59'"
		
	
			sqlcc="select * from cancelanal where date='" & myear & mymonth & myday & "' and site='" & rssite("sitecode") & "'"
			'response.write sqlcc
			openrs rscc,sqlcc
			if  rscc.eof and rscc.bof then
				acan=0
			else
				acan=1
		end if 
			closers rscc
			
			sqlc="exec spanal @site=" & sites & ",@df=" & df & ",@dt=" & dt
			 if ecdebug=1 then wscript.echo sqlc
			'	sqlc="select count(id) as mycount from anpossibleviolators where " & sqlwhere
	'response.write sqlc
	'	mytable=mytable & sqlc
'	response.write sqlc
		openrs rsc,sqlc
		countpv=rsc("countpv")
		countred=rsc("countred")
		countstage1=rsc("countstage1")
		countstage2=rsc("countstage2")
		closers rsc
			'			sqlc="select count(id) as mycount from anpossibleviolators where possibleviolator=0 and stage=1 and " & sqlwhere
	'response.write sqlc
	'	mytable=mytable & sqlc
		'openrs rsc,sqlc
	'countred=rsc("mycount")
	'closers rsc	
	'countred=0
	
				'sqlc="select count(id) as mycount from anpossibleviolators where possibleviolator=1 and " & sqlwhere
	'	mytable=mytable & sqlc
		'openrs rsc,sqlc
	'countstage1=rsc("mycount")
	'closers rsc	
	'	sqlc="select count(id) as mycount from anpossibleviolators where confirmedviolator=1 and " & sqlwhere
		'mytable=mytable & sqlc
	'	openrs rsc,sqlc
	'	countstage2=rsc("mycount")
	'	closers rsc
		if acan=1 then 
			 mytable=mytable & "<td bgcolor=gray>&nbsp;</td>"
			 totalcrossed=totalcrossed+1
		else
    		if countstage2>0 then
    			  mytable=mytable & "<td bgcolor=#0000ff><font color=#ffffff>" & countstage2 & "</font></td>"
						ps1="<td bgcolor=#0000ff><font color=#ffffff>" & countstage2 
						ps2= "</font></td>"
    				total2=total2+ countstage2
						count2=count2+1
    		'elseif countstage1>0 then 
    		'			 mytable=mytable & "<td bgcolor=#339966><font color=#ffffff>" & countstage1 & "</font></td>"
    		'			 ps1="<td bgcolor=#339966><font color=#ffffff>" & countstage1
							
			'				 total1=total1+ countstage1
			'				 count1=count1+1
				elseif countred>0 then 
    					 mytable=mytable & "<td bgcolor=red><font color=#ffffff>" & countred & "</font></td>"
    					 ps1="<td bgcolor=red><font color=#ffffff>" & countred
							
							 totalred=totalred+ countred
							 countr=countr+1
    		else
    				if countpv>0 then
    				mytable=mytable & "<td bgcolor=#ffff00>" & countpv & "</td>"
    				totale=totale+ countpv
						counte=counte+1
						ps1="<td bgcolor=#ffff00>" & countpv 
    				else
    						mytable=mytable & "<td>&nbsp;</td>"
							
							mydate=dateserial(myear,mymonth,myday)
							if cdate(mydate)<date() then
								  waitingdata=waitingdata+1
								'	else
									'		response.write "<hr>didn't put " & mydate & "<hr>"
											 
									end if
								ps1="<td>"
    				end if
    		end if 	
		end if
		 'if ecdebug=1 then wscript.echo "638"
	'response.write ps1 & "<br><input type=checkbox value=" & rssite("sitecode") & "_" & myear & mymonth & myday & " name=cancelled"
	'if acan=1 then response.write " checked"
	'pdf.AddHTML "</td>"
	'response.write "></font></td>"
		 
	'	else
		'		mytable=mytable & "<td bgcolor=#0000ff>"
		'end if
	'	if countpv>0 then
			'  mytable=mytable & "<p bgcolor=#ffff00>" & countpv & "</p><br>"
			'	totale=totale+countpv
		'	end if
		'	if countstage1>0 then
		'	mytable=mytable & "<span bgcolor=#339966>" & countstage1 & "</span><br>"
		'	total1=total1+countstage1
		'	end if
'			mytable=mytable & "stage2 checked:" & countstage2 & "<br><br>"
		'mytable=mytable & "backlog:" & countpv & "<br>"
		'mytable=mytable &  "</td>"
		
  '  dt = dateadd("D", 1, dt)
dfrom=dateadd("d",1,dfrom)
loop
  mytable=mytable & "</tr>"
	'response.write "</tr>"
end if
'response.flush
rssite.movenext

loop
closers rssite

mytable=mytable & "<tr bgcolor=#ffff00><td colspan=2>Extracted</td><td> " & totale & "</td><td>" & counte & "</td></tr>"
mytable=mytable & "<tr bgcolor=red><td colspan=2 ><font color=#ffffff>Extracted and passed through 1st stage with no pv</td><td><font color=#ffffff> " & totalred & "</font></td><td><font color=#ffffff> " & countr & "</font></td></tr>"
'mytable=mytable & "<tr bgcolor=#339966><td colspan=2 ><font color=#ffffff>1st stage completed</font></td><td><font color=#ffffff> " & total1 & "</font></td><td><font color=#ffffff> " & count1 & "</font></td></tr>"
mytable=mytable & "<tr bgcolor=#0000ff><td colspan=2 ><font color=#ffffff>2nd stage completed </td><td><font color=#ffffff>" & total2 & "</font></td><td><font color=#ffffff> " & count2 & "</font></td></tr>"
'mytable=mytable & "<tr bgcolor=#000000><td colspan=2 ><font color=#ffffff>Total Crossed off </font></td><td><font color=#ffffff>" & totalcrossed & "</font></td></tr>"
'if totalcrossed>0 then
'newtotal=totale+totalred+total1+total2
'mytable=mytable & "<tr><td colspan=4 bgcolor=#000000><font color=#ffffff>Percentage Crossed off: " & totalcrossed/newtotal & "</font></td></tr>"
'end if
'mytable=mytable & "<tr bgcolor=#000000><td colspan=2 ><font color=#ffffff>Waiting Data: </font></td><td><font color=#ffffff>" & waitingdata & "</font></td></tr>"
mytable=mytable & "</table>"
'response.write "</table>"
'call writetoexcel(mytable)
pdf.AddHTML mytable
'response.write "<br><br><input type=submit name=submit value='Add Notes Cacel Analysis for checked sites/dates'></form>"


if ecdebug=1 then wscript.echo "593"
   'wscript.echo "421"

   end if
    pdf.AddHTML "<!-PAGE BREAK>"
	 strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
      pdf.AddHTML strhtml

	
	pdf.addhtml "<b>Cancellations Summary</b><br>"
   	mytable="<table border=1><tr><td>Reasons</td><td>12 Months</td><td>6 Months</td><td>	3 Months</td><td> 	30 Days</td><td>1 day</td></tr>"
   	sql="select reason,count(id) as count from cancelled where cancelled>DATEADD (mm ,-12, '" & cisodate(mydate) & "' ) group by reason order by reason"
    openrs rs,sql
    c12=0
    c6=0
    c3=0
    c30=0
    do while not rs.eof
        mytable=mytable & "<tr><td>" & rs("reason") & "</td><td>" 
        sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (mm ,-12, '" & cisodate(mydate) & "' )"
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td><td>"
        c12=c12+rs2("mycount") 
        closers rs2
        
        sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (mm ,-6, '" & cisodate(mydate) & "' )"
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td><td>"
         c6=c6+rs2("mycount") 
        closers rs2
           sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (mm ,-3, '" & cisodate(mydate) & "' )"
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td><td>"
         c3=c3+rs2("mycount") 
        closers rs2
        sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (dd ,-30, '" & cisodate(mydate) & "' )"
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td><td>"
         c30=c30+rs2("mycount") 
        closers rs2
         sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (dd ,-1, '" & cisodate(mydate) & "' )"
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td>"
         c1=c1+rs2("mycount") 
        closers rs2
        mytable=mytable & "</tr>"
    rs.movenext
    loop
    mytable=mytable & "<tr><td></td><td>" & c12 & "</td><td>" & c6 & "</td><td>" & c3 & "</td><td>" & c30 & "</td><td>" & c1 & "</td></tr>"
    mytable=mytable & "</table>"
    closers rs
   pdf.AddHTML mytable
   if ecdebug=1 then wscript.echo "638"
     
    
   

   pdf.AddHTML "<!-PAGE BREAK>"
    strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
      pdf.AddHTML strhtml

pdf.addhtml "<b>Site Issues - Opened today</b><br>"
   sql="select siteissues.id,site,issue,startdate,startnotes,endnotes,notes,[Name of Site] as sitename, DATEDIFF(day,startdate, getdate()) as numdays from siteissues  left join [site information] on siteissues.site=[site information].sitecode  where startdate >=" &  tosql(mydate,"date") & " and startdate<='" & cisodate(mydate) & " 23:59' order by site"
  ' response.Write sql & "<hr>"
openrs rs,sql
if not rs.eof and not rs.bof then
   mytable= "<table border=1 width=520><tr><th width=10>Site</th><th>Site Name</th><th width=30>Issue</th><th width=230>Note</th></tr>"
  
   i=0
    do while not rs.eof
      startnotes=rs("startnotes")
      
      'if i=0 then
        mytable=mytable & "<tr>"
       ' i=1
     ' else
      '  i=0
       ' mytable=mytable & "<tr bgcolor=#DDDDDD>"  
           
      'end if
           mynotes=showsiteissuenotes(rs("id"))  
       myrow= "<td>" & rs("site") & "</td><td>" & rs("sitename") & "</td><td>" & rs("issue")& "</td><td width=150>" & mynotes & "</td>"
          mytable=mytable & myrow & "</tr>"'rad
      
      ' mytable=mytable &  "<Td>" & getnumtickets(rs("site")) & "</td></tr>"
        
    rs.movenext
    loop
    mytable=mytable & "</table>"
else
    mytable="No issues started today"
end if
closers rs

   pdf.AddHTML mytable
   
   
    pdf.AddHTML "<!-PAGE BREAK>"
	 strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
      pdf.AddHTML strhtml

	pdf.addhtml "<b>Site Issues - Fixed today</b><br>"
   sql="select siteissues.id,site,issue,startdate,startnotes,endnotes,notes,[Name of Site] as sitename, DATEDIFF(day,startdate, getdate()) as numdays from siteissues  left join [site information] on siteissues.site=[site information].sitecode  where enddate >=" &  tosql(mydate,"date") & " and enddate<='" & cisodate(mydate) & " 23:59' order by site"
  '   response.Write sql & "<hr>"
openrs rs,sql
if not rs.eof and not rs.bof then
     mytable= "<table border=1 width=520><tr><th width=10>Site</th><th>Site Name</th><th width=30>Issue</th><th width=230>Note</th></tr>"
  
   i=0
    do while not rs.eof
      startnotes=rs("startnotes")
      
      'if i=0 then
        mytable=mytable & "<tr>"
       ' i=1
     ' else
      '  i=0
       ' mytable=mytable & "<tr bgcolor=#DDDDDD>"  
           
      'end if
           mynotes=showsiteissuenotes(rs("id"))  
       myrow= "<td>" & rs("site") & "</td><td>" & rs("sitename") & "</td><td>" & rs("issue")& "</td><td width=150>" & mynotes & "</td>"
          mytable=mytable & myrow & "</tr>"'rad
      
      ' mytable=mytable &  "<Td>" & getnumtickets(rs("site")) & "</td></tr>"
        
    rs.movenext
    loop
    mytable=mytable & "</table>"
else
    mytable="No issues fixed today"
end if
closers rs
 if ecdebug=1 then wscript.echo "711"
   pdf.AddHTML mytable
     
   
     pdf.AddHTML "<!-PAGE BREAK>"
	  strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
      pdf.AddHTML strhtml

	pdf.addhtml "<b>Employee Report<br>"
  
		sql="select count(id) as mycount from anpossibleviolators where " 
		sqlwhere = " mydatestage2 >='" & cisodate(mydate)
	
	sqlwhere=sqlwhere & "' and mydatestage2<='"  & cisodate(mydate) &  " 23:59' and possibleviolator=1 and confirmedviolator=0"
	sql1=sql & sqlwhere
	
			sqlwhere= " mydatestage2>='" & cisodate(mydate)
	

	sqlwhere=sqlwhere & "' and mydatestage2<='"  & cisodate(mydate) & " 23:59'"
	sql2=sql & sqlwhere
	mytable=   "Yesterday<br><table border=1><tr><td>Employee</td><td>PV but not CV</td><td>stage 2 checked</td></tr>"
	sqlemp="select * from useraccess"
	openrs rsemp,sqlemp
	i=0
	tstage1=0
	tstage2=0
	do while not rsemp.eof 
		 sqlstage1=sql1 & " and useridpv=" & rsemp("id")
		 
		 openrs rsstage1,sqlstage1
		 stage1count=rsstage1("mycount")
		 tstage1=tstage1+rsstage1("mycount")
		 closers rsstage1
		 sqlstage2=sql2 & " and useridcv=" & rsemp("id")
		 
		 openrs rsstage2,sqlstage2
		 stage2count=rsstage2("mycount")
		 tstage2=tstage2+rsstage2("mycount")
		 closers rsstage2
		'mytable=mytable &  "<tr><td>x</td><td>1</td><td>2</td></tr>"
		if stage1count<>0 or stage2count<>0 then mytable=mytable &    "<tr><td>" & rsemp("userid") & "</td><td>" & stage1count & "</td><td>" & stage2count & "</td></tr>"
	rsemp.movenext
	i=i+1
	loop	
	mytable=mytable & "<tr><td>Totals</td><td>" & tstage1 & "</td><td>" & tstage2 & "</td></tr>"
	mytable=mytable & "</table>"
	pdf.addhtml   mytable 
	closers rsemp  
	 if ecdebug=1 then wscript.echo "870"
	pdf.AddHTML "<Br>Employee report - last 2 weeks"
	sql="select count(id) as mycount from anpossibleviolators where " 
	mydatefrom=dateadd("ww",-2,mydate)
		sqlwhere = " mydatestage2 >='" & cisodate(mydatefrom)
	
	sqlwhere=sqlwhere & "' and mydatestage2<='"  & cisodate(mydate) &  " 23:59' and possibleviolator=1 and confirmedviolator=0"
	sql1=sql & sqlwhere
	
			sqlwhere= " mydatestage2>='" & cisodate(mydatefrom)
	

	sqlwhere=sqlwhere & "' and mydatestage2<='"  & cisodate(mydate) & " 23:59'"
	sql2=sql & sqlwhere
	'wscript.echo sql2
	mytable=   "<br><br><table border=1><tr><td>Employee</td><td>PV but not CV</td><td>stage 2 checked</td></tr>"
	sqlemp="select * from useraccess"
	openrs rsemp,sqlemp
	i=0
	tstage1=0
	tstage2=0
	do while not rsemp.eof 
		 sqlstage1=sql1 & " and useridpv=" & rsemp("id")
		 
		 openrs rsstage1,sqlstage1
		 stage1count=rsstage1("mycount")
		 tstage1=tstage1+rsstage1("mycount")
		 closers rsstage1
		 sqlstage2=sql2 & " and useridcv=" & rsemp("id")
		 
		 openrs rsstage2,sqlstage2
		 stage2count=rsstage2("mycount")
		 tstage2=tstage2+rsstage2("mycount")
		 closers rsstage2
		'mytable=mytable &  "<tr><td>x</td><td>1</td><td>2</td></tr>"
		if stage1count<>0 or stage2count<>0 then mytable=mytable &    "<tr><td>" & rsemp("userid") & "</td><td>" & stage1count & "</td><td>" & stage2count & "</td></tr>"
	rsemp.movenext
	i=i+1
	loop	
	mytable=mytable & "<tr><td>Totals</td><td>" & tstage1 & "</td><td>" & tstage2 & "</td></tr>"
	mytable=mytable & "</table>"
	pdf.addhtml   mytable 
	closers rsemp  
	sql="select deletedreason,datedeleted,u.userid as userid2, i.userid as userid1,d.userid as deleteduser from anpossibleviolators left join useraccess u on u.id=anpossibleviolators.useridcv left join useraccess i on i.id=anpossibleviolators.useridpv left join useraccess d on d.id=anpossibleviolators.deletedby  where deletedby is not null and anpossibleviolators.datedeleted>=DATEDIFF(dd,0,'" & cisodate(mydate) & "') order by u.userid"
   openrs rs,sql
   if not rs.EOF and not rs.BOF then
   mytable="<br>Deleted<table border=1><tr><th>Employee First Stage</th><th>Employee 2nd stage</th><th>Reason Deleted</th><th>Deleted By</th></tr>"
   do while not rs.EOF 
   mytable=mytable & "<tr><td>" & rs("userid1") & "</td><td>" & rs("userid2") & "</td><td>" & rs("deletedreason") & "</td><td>" & rs("deleteduser") & "</td></tr>"
      
   rs.movenext
   loop
   mytable=mytable & "</table><br><br>"
   pdf.AddHTML mytable
   end if
   
   closers rs
    if ecdebug=1 then wscript.echo "927"
   
   'myhtml = "<!-PAGE BREAK>"
   'pdf.addhtml myhtml
   pdf.AddHTML "http://traffica2.cleartoneholdings.co.uk/cvbyuser.asp"
    pdf.AddHTML "<!-PAGE BREAK>"
	 strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
      pdf.AddHTML strhtml

	pdf.addhtml "<b>Deleted temp Violators</b><br>"
    mytable="<table border=1>"
    sqld="select registration,[date of violation],reasondeleted,stage,username from delviolators where delviolators.dateadded>='" & cisodate(mydate) & "' and delviolators.dateadded<='" & cisodate(mydate) & " 23:59'"
    openrs rsd,sqld
    do while not rsd.eof 
        mytable=mytable &  "<tr><td>" & rsd("registration") & "</td><td>" & rsd("date of violation") & "</td><td>" & rsd("reasondeleted") & "</td><td>" & rsd("stage") & "</td><td>" & rsd("username") & "</td></tr>"
    rsd.movenext
    loop
    closers rsd
    mytable=mytable & "</table>"
    pdf.AddHTML mytable 
        pdf.AddHTML "<!-PAGE BREAK>"
		

		'pdf.addhtml "<b>Unmatched</b><br>"
 
 
        if 1=1 then  ' ec temporarlyu commented
        
          PDF.SetMargins 30,30,40,30 
    PDF.Page "A4" , 1
        
		 strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
      pdf.AddHTML strhtml
    fromdate=dateadd("d",-20,date())
todate=date()  	
  	 if ecdebug=1 then wscript.echo "962"
mytable= "<b>Raw Data Analysis  From:" & fromdate & " To: " &  todate &  "</b>"
		
mytable=mytable & "<table border=1 cellpadding=0 cellspacing=0>"
mytable=mytable & "<tr><td>site <br>code</td><td>Desc</td>"
dfrom=fromdate
do while dfrom<=todate
mytable=mytable & "<td><b>" & day(dfrom) & "</b></td>"
dfrom=dateadd("d",1,dfrom)
loop
mytable=mytable & "<td>Percentage</td>"
mytable=mytable & "</tr>"

''now loop through sites

sqlsites="select sitecode,shortdesc from [Site Information]  where SiteCode in(select site from siteextracted where mydate>DATEADD(MONTH,-1,GETDATE()))  order by sitecode"
openrs rssites,sqlsites

do while not rssites.eof 
mytable=mytable & "<tr><td>" & rssites("sitecode") & "</td><td>" & rssites("shortdesc") & "</td>"
tsingle=0
tdouble=0
dfrom=fromdate
do while dfrom<=todate
sqle="select singlecount,totalcount from siteextracted where site='" & rssites("sitecode") & "' and mydate='" & cisodate(dfrom) & "'"
openrs rse,sqle
if not rse.eof and not rse.bof then
	singlecount= rse("singlecount")
	totalcount=rse("totalcount")
	if totalcount<>0 then
		tsingle=tsingle+singlecount
		tdouble=tdouble+totalcount

	end if
else
	singlecount=0
end if
mytable=mytable & "<td>&nbsp;" &singlecount & "</td>"
dfrom=dateadd("d",1,dfrom)
closers rse
loop
myper=""
if tdouble>0 then
	myper=formatnumber(tdouble/tsingle,2) & "%"
end if
mytable =mytable  & "<td>" &  myper & "</td>" 

mytable=mytable &  "</tr>"


rssites.movenext
loop
mytable=mytable & "</table>"
pdf.addhtml mytable


     pdf.AddHTML "<!-PAGE BREAK>"
       end if
        
		' strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
      
     if ecdebug=1 then wscript.echo "1023"
    PDF.AddHTML "http://traffica2.cleartoneholdings.co.uk/MovementsReportp.asp"
    
  '  strHTML = " <p align=""left""><b>Engineers Movement Report</b></p> "
  '  PDF.AddPattern 0, 2, 0, 20, strHTML
   ' strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
    'PDF.AddPattern 1, 0, 0, 0, strHTML
   'fromdate=dateadd("d",-15,mydate)
		'todate=mydate
		
	'	mytable= "<b>Unmatched Data From:" & fromdate & " To: " &  todate &  "</b>"
		
'mytable=mytable & "<table border=1 cellpadding=0 cellspacing=0>"
'mytable=mytable & "<tr><td>site <br>code</td>"
'do while dfrom<=todate
'mytable=mytable & "<td><b>" & day(dfrom) & "</b></td>"
'dfrom=dateadd("d",1,dfrom)
'loop

'mytable=mytable & "</tr>"


'sql="select sitecode from [site information] where sitecode in(select site from anunmatched where d>getdate()-15)  order by sitecode"
'openrs rssite,sql
'do while not rssite.eof
' mytable=mytable & "<tr><td><b>" & rssite("sitecode") & "</b></td>"
'dfrom=fromdate

'do while dfrom<=todate
	 			


'sqla="select count(plate) as count from anunmatched where  "
 'myear=year(dfrom)
'				 mymonth=month(dfrom)
'				 myday=day(dfrom)

'	if len(myday)<2 then myday="0" & myday
'	if len(mymonth)<2 then mymonth="0" & mymonth
'	sqlwhere= "site='" & rssite("sitecode") & "' and d >="
'	sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
'	sqlwhere=sqlwhere & " and d<=cast('" & myear & mymonth & myday & " 23:59' as datetime)"
'	openrs rsc,sqla & sqlwhere
	
	'mytable=mytable & "<td>" & myear & mymonth & myday & "</td>"
'		mytable=mytable & "<td>"  & rsc("count") & "</td>"
'	closers rsc	

'dfrom=dateadd("d",1,dfrom)
'loop
'mytable=mytable & "</tr>"
'rssite.movenext
'loop
'closers rssite


'pdf.AddHTML mytable 
'END IF     



direc="D:\Shared Folders and Data\secureweb\db\rivergatedata\dailyreport\" & day(mydate) & "_" & month(mydate) & "_" & year(mydate) & "\"
 checkdirectory direc
 
 
 
 pdfname1="Daily Report" & day(mydate) & "_" & month(mydate) & "_" & year(mydate) &  ".pdf"  
    
PDF.Save Direc & pdfname1

    
 ' end if   
set PDFl = createObject("aspPDF.EasyPDF")



PDFl.Page "A4", 0
PDFl.SetMargins 10, 5, 15,5	
    'wscript.echo mytable 
    


   PDFl.AddHTML "<b>Site Issues</b><br>"
   sql="select siteissues.id,site,issue,startdate,startnotes,endnotes,notes,[Name of Site] as sitename, DATEDIFF(day,startdate, getdate()) as numdays from siteissues  left join [site information] on siteissues.site=[site information].sitecode where enddate is null and issue<>'New Installation' order by issue,site"
openrs rs,sql
if not rs.eof and not rs.bof then
   mytable= "<table border=1 width=520><tr><th width=10>Site</th><th>Site Name</th><th width=30>Issue</th><th width=330>Note</th><th width=10>Days</th><Th width=10>tickets</th></tr>"
  
   i=0
    do while not rs.eof
      startnotes=rs("startnotes")
      
      'if i=0 then
        mytable=mytable & "<tr>"
       ' i=1
     ' else
      '  i=0
       ' mytable=mytable & "<tr bgcolor=#DDDDDD>"  
           
      'end if
           mynotes=showsiteissuenotes(rs("id"))  
       myrow= "<td>" & rs("site") & "</td><td>" & rs("sitename") & "</td><td>" & rs("issue")& "</td><td width=330>" & mynotes & "</td><td>" & rs("numdays") & "</td>"
          mytable=mytable & myrow
      
       mytable=mytable &  "<Td>" & getnumtickets(rs("site")) & "</td></tr>"
        ''check if fixed and show fixed if fixed
        
    rs.movenext
    loop

end if
closers rs
mytable=mytable & "</table>"

   PDFl.AddHTML mytable
   
     pdfname2="siteissues" & day(mydate) & "_" & month(mydate) & "_" & year(mydate) &  ".pdf"  
    
PDFl.Save Direc & pdfname2


set PDF3 = createObject("aspPDF.EasyPDF")

'direc="D:\Shared Folders and Data\secureweb\db\rivergatedata\dailyreport\"


PDF3.Page "A4", 0
PDF3.SetMargins 10, 5, 15,5	
    'wscript.echo mytable 
    


   PDF3.AddHTML "<b>Site Issues - New Installation</b><br>"
   sql="select siteissues.id,site,issue,startdate,startnotes,endnotes,notes,[Name of Site] as sitename, DATEDIFF(day,startdate, getdate()) as numdays from siteissues  left join [site information] on siteissues.site=[site information].sitecode where enddate is null and issue='New Installation' order by issue,site"
openrs rs,sql
if not rs.eof and not rs.bof then
   mytable= "<table border=1 width=520><tr><th width=10>Site</th><th>Site Name</th><th width=30>Issue</th><th width=330>Note</th><th width=10>Days</th><Th width=10>tickets</th></tr>"
  
   i=0
    do while not rs.eof
      startnotes=rs("startnotes")
      
      'if i=0 then
        mytable=mytable & "<tr>"
       ' i=1
     ' else
      '  i=0
       ' mytable=mytable & "<tr bgcolor=#DDDDDD>"  
           
      'end if
           mynotes=showsiteissuenotes(rs("id"))  
       myrow= "<td>" & rs("site") & "</td><td>" & rs("sitename") & "</td><td>" & rs("issue")& "</td><td width=330>" & mynotes & "</td><td>" & rs("numdays") & "</td>"
          mytable=mytable & myrow
      
       mytable=mytable &  "<Td>" & getnumtickets(rs("site")) & "</td></tr>"
        ''check if fixed and show fixed if fixed
        
    rs.movenext
    loop

end if
closers rs
mytable=mytable & "</table>"

   PDF3.AddHTML mytable
   
     pdfname3="newinstallation" & day(mydate) & "_" & month(mydate) & "_" & year(mydate) &  ".pdf"  
    
PDF3.Save Direc & pdfname3

set PDF4 = createObject("aspPDF.EasyPDF")




PDF4.Page "A4", 0
PDF4.SetMargins 10, 5, 15,5	
    'wscript.echo mytable 
 
   PDF4.AddHTML "<b>PCN notes</b><br>"
   mytable="<table cellpadding=10><tr><td>date</td><td>pcn</td><td>pcnnote</td><td>user</td></tr>"
   sql="select mydate,pcn,pcnnote,useraccess.userid from pcnnotes left join useraccess on useraccess.id=pcnnotes.userid where mydate>=" & tosql(mydate,"date") & " and mydate<='" & cisodate(mydate) & " 23:59'"
  'response.write sql 
   openrs rs,sql
   do while not rs.eof
    mytable=mytable & "<tr><td>" & rs("mydate") & "</td><td>" & rs("pcn") & "</td><td width=200>" & rs("pcnnote") & "</td><td>" & rs("userid") & "</td></tr>"
  
   rs.movenext
   loop
   mytable=mytable & "</table>"
   pdf4.AddHTML mytable
   closers rs
   
   
   'random 3 letters
    pdf4.AddHTML "<!-PAGE BREAK>"
     strHTML = "<p align='center'>Page <!-PDF.PAGENUMBER><br></p>" 
      pdf.AddHTML strhtml
    pdf4.AddHTML "<b>Random 3 letters today</b><br>"
  ' sqlr="select top 3 type,id  from vwoutletters where date>='" & cisodate(mydate) & "' and date<='" & cisodate(mydate) & " 23:59' ORDER BY NEWID()"
  sqlr="select top 3 type,vwoutletters.id,i.pcn,site,date,i.id as incomingletterid  from vwoutletters left join violators v on vwoutletters.violatorid=v.id left join incomingletters i on i.pcn=v.pcn+v.site and vwoutletters.date>i.mydate where date>='" & cisodate(mydate) & "' and date<='" & cisodate(mydate) & " 23:59' and i.pcn is not null ORDER BY NEWID()"
    openrs rsr,sqlr
    i=0
    dim apcn(2)
    dim adate(2)
   ' dim adate(2)
    do while not rsr.eof
        ltype=rsr("type")
        sqll="select letter,date,violatorid from " & ltype & "letters where id=" & rsr("id")
        'response.Write sqll
        
        
        openrs rsl,sqll
        letter=rsl("letter")
       ' response.Write letter
             pdf4.AddHTML ltype & " letter- " & rsl("date")& "<br>" & letter & "<br><br><!-PAGE BREAK>"
              
             pcn=getviolatorpcn(rsl("violatorid"))
             apcn(i)=pcn
             
             adate(i)=rsl("date")
             
            
'response.Write "D:\Shared Folders and Data\wwwroot\traffica\incomingletters\" & iyear & "\" & imonth & "\"& ifilename
           '  response.Write rsl("letter")
           
        closers rsl
        i=i+1
    rsr.movenext
    loop
    closers rsr
   ' pdf.AddHTML "<!--PAGE BREAK>"
     pdf4.AddHTML "Last incoming letter for pcn" & apcn (0)
             '
             sqli="select filename,mydate from incomingletters where  pcn='" & apcn(0) & "' and  mydate<'" & cisodate(adate(0)) & "' order by mydate desc"
           ' response.Write sqli
             openrs rsi,sqli
             if not rsi.eof and not rsi.bof then
                 ifilename=rsi("filename")   
                 iyear=year(rsi("mydate"))
                 imonth=month(rsi("mydate"))
                ' response.Write "<hr>" & ifilename & "<hr>"
             else
                ifilename=""
              end if                
             closers rsi
             NPage = PDF4.PageCount
        
        if ifilename<>"" then  PDF4.AddPDF "D:\Shared Folders and Data\wwwroot\traffica\incomingletters\" & iyear & "\" & imonth & "\"& ifilename , 0,0, ""
  
    pdf4.AddHTML "<!--PAGE BREAK>"
     pdf4.AddHTML "Last incoming letter for pcn" & apcn (1)
             '
             sqli="select filename,mydate from incomingletters where  pcn='" & apcn(1) & "' and  mydate<'" & cisodate(adate(1)) & "' order by mydate desc"
           ' response.Write sqli
             openrs rsi,sqli
             if not rsi.eof and not rsi.bof then
                 ifilename=rsi("filename")   
                 iyear=year(rsi("mydate"))
                 imonth=month(rsi("mydate"))
                ' response.Write "<hr>" & ifilename & "<hr>"
             else
                ifilename=""
              end if                
             closers rsi
             NPage = PDF4.PageCount
        
        if ifilename<>"" then  PDF4.AddPDF "D:\Shared Folders and Data\wwwroot\traffica\incomingletters\" & iyear & "\" & imonth & "\"& ifilename , 0,0, ""
  
  
    pdf4.AddHTML "<!--PAGE BREAK>"
     pdf4.AddHTML "Last incoming letter for pcn" & apcn (2)
             '
             sqli="select filename,mydate from incomingletters where  pcn='" & apcn(2) & "' and  mydate<'" & cisodate(adate(2)) & "' order by mydate desc"
           ' response.Write sqli
             openrs rsi,sqli
             if not rsi.eof and not rsi.bof then
                 ifilename=rsi("filename")   
                 iyear=year(rsi("mydate"))
                 imonth=month(rsi("mydate"))
                ' response.Write "<hr>" & ifilename & "<hr>"
             else
                ifilename=""
              end if                
             closers rsi
             NPage = PDF4.PageCount
        
        if ifilename<>"" then  PDF4.AddPDF "D:\Shared Folders and Data\wwwroot\traffica\incomingletters\" & iyear & "\" & imonth & "\"& ifilename , 0,0, ""
  
  
  
  
  pdfname4="correspondence" & day(mydate) & "_" & month(mydate) & "_" & year(mydate) &  ".pdf"  
    
PDF4.Save Direc & pdfname4


if 1=0 then 
Const cdoSendUsingMethod        = _
"http://schemas.microsoft.com/cdo/configuration/sendusing"
Const cdoSendUsingPort          = 2
Const cdoSMTPServer             = _
"http://schemas.microsoft.com/cdo/configuration/smtpserver"
Const cdoSMTPServerPort         = _
"http://schemas.microsoft.com/cdo/configuration/smtpserverport"
Const cdoSMTPConnectionTimeout  = _
"http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout"
Const cdoSMTPAuthenticate       = _
"http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"
Const cdoBasic                  = 1
Const cdoSendUserName           = _
"http://schemas.microsoft.com/cdo/configuration/sendusername"
Const cdoSendPassword           = _
"http://schemas.microsoft.com/cdo/configuration/sendpassword"
Const smtpServer = "localhost"

Dim objConfig  ' As CDO.Configuration
Dim objMessage ' As CDO.Message
Dim Fields     ' As ADODB.Fields

' Get a handle on the config object and it's fields
Set objConfig =CreateObject("CDO.Configuration")
Set Fields = objConfig.Fields

' Set config fields we care about
With Fields
.Item(cdoSendUsingMethod)       = cdoSendUsingPort
.Item(cdoSMTPServer)            = smtpServer
.Item(cdoSMTPServerPort)        = 25
.Item(cdoSMTPConnectionTimeout) = 10
.Item(cdoSMTPAuthenticate)      = cdoBasic
.Item(cdoSendUserName)          = "username"
.Item(cdoSendPassword)          = "password"

.Update
End With

Set objMessage = CreateObject("CDO.Message")

Set objMessage.Configuration = objConfig

With objMessage
.To       ="esther@creativecarpark.co.uk"
.From     = "dailyreport@creativecarpark.co.uk"
.Subject  = "Daily Report"

.HtmlBody = mailBody

.AddAttachment direc & pdfname1
.AddAttachment direc & pdfname2
.AddAttachment direc & pdfname3
.AddAttachment direc & pdfname4
.Send
End With

Set Fields = Nothing
Set objMessage = Nothing
Set objConfig = Nothing
end if 

postto="http://traffica/senddailysummaryemail.asp?pdfname1=" & pdfname1 & "&pdfname2=" & pdfname2& "&pdfname3=" & pdfname3& "&pdfname4=" & pdfname4
wscript.echo postto
 set xmlhttp = CreateObject("MSXML2.ServerXMLHTTP") 
    xmlhttp.open "GET", postto, false 
    xmlhttp.send
    
    set xmlhttp = nothing 

  

 wscript.echo "daily report saved"
     if err.number<>0 then 
postto="http://clients.creativecarpark.co.uk/errors.asp?errortype=system&errordescription=error running daily report&codeerrordescription=" & err.number& "-" &  err.Description
 set xmlhttp = CreateObject("MSXML2.ServerXMLHTTP") 
    xmlhttp.open "GET", postto, false 
    xmlhttp.send
    
    set xmlhttp = nothing 

end if
function showsiteissuenotes(siteissueid)
	
	
	sqlss="select u.userid,note,sn.date,e.engineername,siteissues.site from siteissuenotes sn left join useraccess u on sn.userid=u.id left join issueengineers e on sn.engineerid=e.engineerid left join siteissues on siteissues.id=sn.siteissueid where siteissueid=" & siteissueid & " order by date"
	openrs rsss,sqlss
	notest=""
	sqll="exec sitelastanal '" & rsss("site") & "'"
	openrs rsl,sqll
	notest="Last violation ticket printed:" & rsl("datelast") & "<br>" 
	closers rsl
	if not rsss.eof and  not rsss.bof then
	notest=notest & "<table><tr><th>Date</th><th>User</th><th>Note</th></tr>"
	do while not rsss.eof
	userid= rsss("userid")
	if isnull(userid) then  
	userid=rsss("engineername")
	'userid="xxx"
	
	
	end if
	notest=notest & "<tr><td>" & rsss("date") & "</td><td>" &userid & "</td><td>" & rsss("note") & "</td></tr>"
	rsss.movenext
	loop	
	notest=notest& "</table>"
	end if
	closers rsss
	showsiteissuenotes=notest
	end function
	function checkifpaid(mypcn)
	sqlcp="select pcn from payments where pcn=" & tosql(mypcn,"text")
	openrs rscp,sqlcp
	if not rscp.eof and not rscp.bof then
	    checkifpaid=true
	else
	    checkifpaid=false
    end if	        
	
	closers rscp
	
	end function
	
	function checkifpaid(mypcn)
	sqlcp="select pcn from payments where pcn=" & tosql(mypcn,"text")
	openrs rscp,sqlcp
	if not rscp.eof and not rscp.bof then
	    checkifpaid=true
	else
	    checkifpaid=false
    end if	        
	
	closers rscp
	
	end function

function getnumtickets(site)
sqls="select count(id) as count from siteissues where site=" & tosql(site,"text")
openrs rss,sqls
getnumtickets=rss("count")
closers rss
end function
sub openrs(rs, sql)
  Set rs =CreateObject("ADODB.Recordset")
   'on error resume next
'response.write "<!--" &  sql& "-->"
  'wscript.echo sql
  rs.Open sql, objConn,,,adCmdText
 
 'if err.number>0 then 
 '	response.write err.number & err.description
'	response.write "<hr>" & sql
 'end if
'on error goto 0  
  'call myerror
end sub
Function cIsoDate(dteDate)

   If IsDate(dteDate) = True and not isnull(dtedate) Then
      DIM dteDay, dteMonth, dteYear
      dteDay = Day(dteDate)
      dteMonth = Month(dteDate)
      dteYear   = Year(dteDate)
      cIsoDate = dteYear & _
          Right(Cstr(dteMonth + 100),2) & _
          Right(Cstr(dteDay + 100),2)
   Else
      cIsoDate = Null
   End If
End Function

Function ToSQL(Value, sType)
  Param = Value
  if trim(Param) = "" then
    ToSQL = "Null"
  else
    if sType = "Number" then
      ToSQL = CDbl(Param)
     '   tosql=param
else
    	
			if stype="date" then
			 'ToSQL = "#" & Replace(Param, "'", "''") & "#"
			 tosql="'" & cisodate(param) & "'"
		else
	'  Param=Killchars(Param)
if param<>"" then 
	   ToSQL = "'" & Replace(Param, "'", "''") & "'"
else
		tosql="NULL"
end if
	  	end if
    end if
  end if
end function
sub closers(rs)
	on error resume next
	rs.close
	on error goto 0
  Set rs = nothing
 ' on error goto 0
 end sub
 
 function getviolatorpcn(id)
	sqlv="select pcn+site as pcn from violators where id=" & id
	openrs rsv,sqlv
	getviolatorpcn=rsv("pcn")
	closers rsv 
	
	end function
	
	
	function checkifinpv(site,days)
checkifinpv=false
sqlcp="select count(id) as mycount from anpossibleviolators where site='" & site & "' and d>getdate()-" & days 
openrs rscp,sqlcp

if rscp("mycount")>0 then checkifinpv=true


closers rscp
end function

function checkifinpvstage2(site,days)
checkifinpvstage2=false
sqlcp="select count(id) as mycount from anpossibleviolators where site='" & site & "' and d>getdate()-" & days & " and stage=2"
openrs rscp,sqlcp
if rscp("mycount")>0 then checkifinpvstage2=true

closers rscp
end function

 function getdaysstage2(site)
    sqlgd="select datediff(d,max(d),getdate()) as day from anpossibleviolators where stage=2 and site=" & tosql(site,"text")
    openrs rsgd,sqlgd
    getdaysstage2=rsgd("day")
    closers rsgd
 end function
 
 Function ElementInArray(aMyArray, strLookingFor, compare)
'Returns True if strLookingFor is in aMyArray, False otherwise
  ElementInArray = (PositionInArray(aMyArray, strLookingFor, compare) > -1)
End Function

Function PositionInArray(aMyArray, strLookingFor, compare)
'Return the position strLookingFor is found in aMyArray... if
'strLookingFor is not found, -1 is returned.

  Dim iUpper, iLoop
  iUpper = UBound(aMyArray)

  For iLoop = LBound(aMyArray) to iUpper
    If compare = vbTextCompare then
      If CStr(UCase(aMyArray(iLoop))) = CStr(UCase(strLookingFor)) then
        PositionInArray = iLoop
        Exit Function
      End If
    Else
      If CStr(aMyArray(iLoop)) = CStr(strLookingFor) then
        PositionInArray = iLoop
        Exit Function
      End If
    End If
  Next

  'Didn't find the element in the array...
  PositionInArray = -1

End Function
function getengineerinfo(issueid)
mystr=""
sql="select * from engineervisits left join issueengineers on issueengineers.engineerid=engineervisits.engineerid where issueid=" & issueid
openrs rsi,sql
if not rsi.eof and not rsi.bof then 
do while not rsi.eof 
mystr=mystr & "<br>" &  rsi("engineername") & " arrived on " & rsi("arrivaldate") & " and left on " & rsi("closeddate") & "<br> Hard diskspace" & rsi("harddiskspace") & "<br> Amount of Signs: " & rsi("amountofsigns") 
rsi.movenext
loop
closers rsi
end if
getengineerinfo=mystr
end function 

function getsitedesc(site)
site=trim(site)
sqld="select shortdesc from [site information] where sitecode=" & tosql(site,"text")
openrs rsd,sqld
if not rsd.eof and not rsd.bof then getsitedesc=rsd("shortdesc")

closers rsd
end function

function getsitelastprinted(site)
site=trim(site)
sql="select max([date of violation]) as mydate from violators where site=" & tosql(site,"text")
'wscript.echo sql 
openrs rsgs,sql
getsitelastprinted=datediff("d",rsgs("mydate"),date())
closers rsgs
end function



Function CheckDirectory( path )

      Checkdirectory = False    '// Default return value

      folders = Split(path, "\")

      '// Check if the drive is in the path, assume C: if it's not
      If InStr(folders(0), ":") Then
          currentPath = folders(0) & "\"
          startIdx = 1
      Else
          currentPath = "C:\"
          startIdx = 0
      End If

      '// server.create the file system object
      Set fso = createObject("Scripting.FileSystemObject")

     ' On Error Resume Next

      For i = startIdx To UBound(folders)

         currentPath = currentPath & folders(i) & "\"
         If Not fso.FolderExists(currentPath) Then
             fso.createFolder(currentPath)
         End If

         If Err.number <> 0 Then
            '// An error occured - probably permissions - so exit
            Exit For
         End If

      Next

     ' On Error Goto 0

      '// Final check that the path was server.created
      If fso.FolderExists(currentPath) Then
          path = currentPath      '// Return our path in case the drive has changed
          CheckDirectory = True
      End If      

   End Function