<%
    '1)include this file after common.asp, openconn.asp and admincommon.asp
    '2)define PageTitle in containing page 
    '3)close body and html tag in containing page
%>

<!DOCTYPE html>
<html>
<head>
	<title><%=PageTitle%></title>
	<script type="text/javascript" src="temjs/jquery-1.11.1.min.js"></script>
	<script type="text/javascript">

        //SuckerTree Horizontal Menu (Sept 14th, 06)
        //By Dynamic Drive: http://www.dynamicdrive.com/style/

        var menuids = ["treemenu1"] //Enter id(s) of SuckerTree UL menus, separated by commas

        function buildsubmenus_horizontal() {
            for (var i = 0; i < menuids.length; i++) {
                var ultags = document.getElementById(menuids[i]).getElementsByTagName("ul")
                for (var t = 0; t < ultags.length; t++) {
                    if (ultags[t].parentNode.parentNode.id == menuids[i]) { //if this is a first level submenu
                        ultags[t].style.top = ultags[t].parentNode.offsetHeight + "px" //dynamically position first level submenus to be height of main menu item
                        ultags[t].parentNode.getElementsByTagName("a")[0].className = "mainfoldericon"
                    }
                    else { //else if this is a sub level menu (ul)
                        ultags[t].style.left = ultags[t - 1].getElementsByTagName("a")[0].offsetWidth + "px" //position menu to the right of menu item that activated it
                        ultags[t].parentNode.getElementsByTagName("a")[0].className = "subfoldericon"
                    }
                    ultags[t].parentNode.onmouseover = function () {
                        this.getElementsByTagName("ul")[0].style.visibility = "visible"
                    }
                    ultags[t].parentNode.onmouseout = function () {
                        this.getElementsByTagName("ul")[0].style.visibility = "hidden"
                    }
                }
            }
        }

        if (window.addEventListener)
            window.addEventListener("load", buildsubmenus_horizontal, false)
        else if (window.attachEvent)
            window.attachEvent("onload", buildsubmenus_horizontal)
        function toggle_visibility(id) {
            var e = document.getElementById(id);
            if (e.style.display == 'block')
                e.style.display = 'none';
            else
                e.style.display = 'block';
        }

    </script>
    <LINK href="adminstylesheet.css" rel=stylesheet type=text/css>

</head>
<body>

<span class=noPrint>
<div id=header><a href=admin.asp?cmd=adminhome><img src="logo.jpg"  border="0" class=logo></a>
    <%if session("admin")="ok" then %>
        <div class=hright><a href="admin.asp?cmd=adminlogout" class=button>Log Out</a></div>
        <div class=welcome>Welcome <%= session("username") %></div>
        <div class=pcnheader>
            <form method=post action=admin.asp?cmd=search2>
            Enter PCN/Plate:<input type=text name=pcn><input type=submit  value="Go" class=button>
            </form>
        </div>
    <%end if%>
</div>
</span>

<% 
call checklogin 
if session("admin")="ok" and showadminmenu<>1 then
    displaymenu = 1
	 call getadminmenu
	showadminmenu=1
end if
%>
