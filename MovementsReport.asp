﻿<% Server.ScriptTimeout = 3600000 %>
<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%
 pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 
%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Movements Summary</title>
    <style type="text/css">
        body {
            font-family: Arial,sans-serif ;
            font-size: 12px;           
        }
        h1 {
            font-weight:bold;
            font-size:18px;
            text-decoration:underline;
        }
        table.summary {
	        border-width: 0px;
	        border-spacing: 0px;
	        border-style: solid;
	        border-color: gray;
	        border-collapse: collapse;
	        text-align: center;
        }
        table.summary th {
	        border-width: 1px;
	        padding: 7px;
	        border-style: solid;
	        border-color: gray;
	        -moz-border-radius: ;
        }
        table.summary td {
	        border-width: 0px;
	        padding: 7px;
	        border-style: solid;
	        border-color: gray;
	        -moz-border-radius: ;
        }
        tr.totals td {
            font-weight: bold;            
            border-bottom-width: 5px;
            border-top-width: 1px;
            border-bottom-style: double;
        }
        tr td.btwcol {
            background-color:white;
	        border-style: solid;
	        border-color: gray;
	        border-left-width: 2px;
	        border-right-width: 2px;
            border-bottom-width: 0px;
            border-top-width: 0px;
	        padding: 2px;
        }
        tr td.endcol {
	        border-style: solid;
	        border-color: gray;
	        border-left-width: 2px;
            border-bottom-width: 0px;
            border-top-width: 0px;
	        padding: 0px;
        }
        tr td.toptable {
	        border-style: solid;
	        border-color: gray;
	        border-left-width: 0px;
            border-right-width: 0px;
            border-top-width: 0px;
            border-bottom-width: 2px;
	        padding: 0px;
        }
        tr.even {
            background-color:#d0d0d0;
        }
        tr td.topborder{
            border-top-width:2px;
        }
        tr td.alert{
            border-color: firebrick;
            border-width: 1px;
            background-color:salmon;
        }
        tr.btwrow td{
	        padding: 1px;
        }
    </style>
</head>
<body>
    <h1>Engineers Movement Report</h1>
    <table class='summary'>
    <%
        on error resume next
        dim arr1
        dim arr2
        dim arr3
        dim arr4
        dim arr5
        
        dim btwcol
        dim topborder
        dim shade
        dim weeknum
        dim alert
        dim tdsetup
        dim skipcolumns
        
        skipcolumns = 5
        
        btwcol = "<td class='btwcol'></td>"
        endcol = "<td class='endcol'></td>"       
        
        sql="exec MovementSummary"
        openrs rs1,sql
        arr1 = rs1.GetRows()
                
        set rs1 = rs1.NextRecordset()
        arr2 = rs1.GetRows()

        set rs1 = rs1.NextRecordset()
        arr3 = rs1.GetRows()

        set rs1 = rs1.NextRecordset()
        arr4 = rs1.GetRows()
        
        set rs1 = rs1.NextRecordset()
        arr5 = rs1.GetRows()
        
            colscount = UBound(arr1, 1)
                  
            fieldcount = rs1.Fields.Count 
            dim arrtot2()
            dim arrtot3()
            dim arrtot4()
            redim arrtot2(fieldcount-skipcolumns-1,0)
            redim arrtot3(fieldcount-skipcolumns-1,0)
            redim arrtot4(fieldcount-skipcolumns-1,0)
            
            Response.Write "<tr>"
                Response.Write endcol
                Response.Write "<th class='toptable' colspan='" & fieldcount-skipcolumns+2 & "'>Arrival Times</th>"
                Response.Write btwcol
                Response.Write "<th class='toptable' colspan='" & fieldcount-skipcolumns & "'>Sites Visited</th>"
                Response.Write btwcol
                Response.Write "<th class='toptable' colspan='" & fieldcount-skipcolumns & "'>Time Spent on Site (minutes)</th>"
                Response.Write btwcol
                Response.Write "<th class='toptable' colspan='" & fieldcount-skipcolumns & "'>Time Spent Between Sites (minutes)</th>"
                Response.Write endcol
            Response.Write "</tr>"

            Response.Write "<tr>"
                Response.Write endcol
                Response.Write "<th>Date</th><th>Day</th>"
                For i = skipcolumns To fieldcount - 1
                    Response.Write "<th>" & rs1.Fields(i).Name & "</th>"
                Next 
                
                Response.Write btwcol
                
                For i = skipcolumns To fieldcount - 1
                    Response.Write "<th>" & rs1.Fields(i).Name & "</th>"
                Next 
                
                Response.Write btwcol
                
                For i = skipcolumns To fieldcount - 1
                    Response.Write "<th>" & rs1.Fields(i).Name & "</th>"
                Next 
                
                Response.Write btwcol
                
                For i = 5 To fieldcount - 1
                    Response.Write "<th>" & rs1.Fields(i).Name & "</th>"
                Next 
                Response.Write endcol
                
            Response.Write "</tr>"
            
            weeknum = 1
            for irow = 0 to UBound(arr1, 2)
                
                if (irow+1 - ((weeknum-1)*7)) mod 2 = 0 then
                    shade = "even"
                else
                    shade = "odd"
                end if
                Response.Write replace("<tr class='shade'>","shade",shade)
                   if (irow+1) mod 7 = 1 then
                        tdsetup = "<td class='topborder zzz'>"
                    else
                        tdsetup = "<td class='zzz'>"
                    end if
                    
                    Response.Write endcol                       'replace(endcol,"'endcol'","'endcol' style='" & topborder & "'")
                    
                    Response.Write tdsetup & arr1(0, irow) & "</td>"
                    Response.Write tdsetup & arr1(2, irow) & "</td>"
                                        
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = replace(tdsetup,"zzz","alert")
                        else
                            tdsetupCur = tdsetup
                        end if
                        Response.Write tdsetupCur & fmtdash(arr1(icol, irow)) & "</td>"
                    Next
                    Response.Write btwcol
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = replace(tdsetup,"zzz","alert")
                        else
                            tdsetupCur = tdsetup
                        end if
                        Response.Write tdsetupCur & fmtdash(arr2(icol, irow)) & "</td>"
                        
                        'Keep a running total for the week
                        arrtot2(icol-skipcolumns,0) = CLng(arrtot2(icol-skipcolumns,0))+CLng(arr2(icol, irow))
                    Next
                    Response.Write btwcol
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = replace(tdsetup,"zzz","alert")
                        else
                            tdsetupCur = tdsetup
                        end if
                        Response.Write tdsetupCur & fmtdash(Minutes2Hours(arr3(icol, irow))) & "</td>"
                        
                        'Keep a running total for the week
                        arrtot3(icol-skipcolumns,0) = CLng(arrtot3(icol-skipcolumns,0))+CLng(arr3(icol, irow))
                    Next
                    Response.Write btwcol
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = replace(tdsetup,"zzz","alert")
                        else
                            tdsetupCur = tdsetup
                        end if
                        Response.Write tdsetupCur & fmtdash(Minutes2Hours(arr4(icol, irow))) & "</td>"

                        'Keep a running total for the week
                        arrtot4(icol-skipcolumns,0) = CLng(arrtot4(icol-skipcolumns,0))+CLng(arr4(icol, irow))
                    Next
                    
                    Response.Write endcol
                Response.Write "</tr>"
                
                if (irow+1) mod 7 = 0 then
                    Response.Write "<tr class='totals'>"
                        Response.Write endcol
                        Response.Write "<td colspan='2' style='text-align:left;'>Week Total</td>"
                        
                        For icol = 0 To UBound(arrtot2,1)
                            Response.Write "<td>&nbsp;</td>"
                        Next
                        Response.Write btwcol
                        For icol = 0 To UBound(arrtot2,1)
                            Response.Write "<td>" & arrtot2(icol, 0) & "</td>"
                            arrtot2(icol, 0) = 0
                        Next
                        Response.Write btwcol
                        For icol = 0 To UBound(arrtot3,1)
                            Response.Write "<td>" & Minutes2Hours(arrtot3(icol, 0)) & "</td>"
                            arrtot3(icol, 0) = 0
                        Next
                        Response.Write btwcol
                        For icol = 0 To UBound(arrtot4,1)
                            Response.Write "<td>" & Minutes2Hours(arrtot4(icol, 0)) & "</td>"
                            arrtot4(icol, 0) = 0
                        Next
                        Response.Write endcol
                        weeknum = weeknum + 1
                    Response.Write "</tr>"
                    Response.Write "<tr class='btwrow'><td colspan='" & (fieldcount + ((fieldcount-skipcolumns)*3)) & "'>&nbsp;</td></tr>"
                end if
            Next
       
        closers rs1
     %>
    </table>
</body>
</html>
<%
function iif(psdStr, trueStr, falseStr)
  if psdStr then
    iif = trueStr
  else 
    iif = falseStr
  end if
end function

function Minutes2Hours(minutes)
    dim hours
    dim mins
    dim retval
    
    On Error Resume Next
    hours = minutes \ 60
    mins = minutes mod 60
    
    retval = CStr(hours) & ":" & Right("0" & CStr(mins),2)
    if len(hours)=0 then 
        retval = minutes
    else
        if len(retval) < 5 then retval = Right("00" & retval, 5)
    end if
    Minutes2Hours = retval
end function

function fmtdash(val)
    if val = "-" then
        val = "&#150;"
    end if
    fmtdash = val
end function

%>