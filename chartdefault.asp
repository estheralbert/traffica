<%@ language=vbscript %>
<%
  Response.Expires = 0
  Response.Buffer = true
  Response.Clear

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'  This shows all the properties used by csDrawGraph, and their default values.
'  It also plots a pie chart with 3 data items.
'  Replace "Chart.GIFPie" with "Chart.GIFBar", "Chart.GIFStackedBar or 
'  "Chart.GIFLine" to display a different type of graph.
'  Properties that use the default settings do not actually need to be specified.
'  This file may be useful as a template when trying different property values.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


  'Set Chart = Server.CreateObject("csDrawGraph.Draw")
 ' Set Chart = Server.CreateObject("csDrawGraphTrial.Draw")
  'Set Chart = Server.CreateObject("csDrawGraph64.Draw")
  Set Chart = Server.CreateObject("csDrawGraph64Trial.Draw")

  'Pie Chart Properties
  Chart.CenterX = 175
  Chart.CenterY = 150
  Chart.PieDia = 200
  Chart.Offset = 10
  Chart.ShowPercent = false
  Chart.StartAngle = 0

  'Bar Chart Properties (some also apply to line graphs)
  Chart.OriginX = 50
  Chart.OriginY = 250
  Chart.MaxX = 250
  Chart.MaxY = 200
  Chart.BarWidth = 0
  Chart.BarGap = 0
  Chart.YTop = 0
  Chart.YGrad = 0
  Chart.XAxisText = ""
  Chart.YAxisText = ""
  Chart.AxisTextFont = "Arial"
  Chart.AxisTextSize = 8
  Chart.AxisTextBold = false
  Chart.AxisTextBGColor = "ffffff"
  Chart.AxisTextColor = "000000"
  Chart.LabelVertical = false
  Chart.ShowGrid = false
  Chart.GridStyle = 1
  Chart.GridColor = "000000"
  Chart.ShowBarTotal = false
  Chart.BarTotalVertical = false
  Chart.VerticalBars = true
  Chart.XValuesVertical = true
  Chart.ShowTrendLine = false
  Chart.TrendLineColor = "000000"
  Chart.TrendLineWidth = 1
  Chart.TrendLineName = ""
  Chart.ShowLinesWithBars = false
  Chart.ShowTotalIfZero = true

  'Stacked Bar Chart Properties
  Chart.ShowStackedValue = false
  Chart.StackedTextFont = "Arial"
  Chart.StackedTextSize = 8
  Chart.StackedTextBGColor = "ffffff"
  Chart.StackedTextColor = "000000"
  Chart.StackedTextTransparent = true
  Chart.StackedTextAlign = 0

  'Line Graph Properties
  Chart.XTop = 0
  Chart.XGrad = 0
  Chart.LineWidth = 1
  Chart.PointStyle = 0
  Chart.PointSize = 2
  Chart.UseXAxisLabels = false
  Chart.UseYAxisLabels = false
  Chart.XAxisNegative = 0
  Chart.YAxisNegative = 0
  Chart.XOffset = 0
  Chart.YOffset = 0
  Chart.XMarkSize = 4
  Chart.YMarkSize = 4
  Chart.HideVGrid = false
  Chart.HideHGrid = false
  Chart.PrefixX = ""
  Chart.PrefixY = ""
  Chart.SuffixX = ""
  Chart.SuffixY = ""
  Chart.ShowSeparatorX = false
  Chart.ShowSeparatorY = false
  Chart.UseXAxisDates = false
  Chart.UseYAxisDates = false
  Chart.DateTimeFormat = 0
  Chart.DateFormatString = ""
  Chart.TimeFormatString = ""

  'Line Graph Text Properties
  Chart.LineGraphTextFont = "Arial"
  Chart.LineGraphTextBGColor = "ffffff"
  Chart.LineGraphTextColor = "000000"
  Chart.LineGraphTextSize = 8
  Chart.LineGraphTextBold = false
  Chart.LineGraphTextAlign = 0
  Chart.LineGraphTextX = 0
  Chart.LineGraphTextY = 0
  Chart.LineGraphTextBorder = false
  Chart.LineGraphTextLeader = false
  Chart.LineGraphBorderColor = "000000"
  Chart.LineGraphLeaderColor = "000000"

  'Extra text properties
  Chart.TextFont = "Arial"
  Chart.TextSize = 8
  Chart.TextBold = false
  Chart.TextItalic = false
  Chart.TextUnderline = false
  Chart.TextTransparent = false
  Chart.TextColor = "000000"
  Chart.TextBGColor = "ffffff"

  'General Properties
  Chart.Width = 400
  Chart.Height = 300
  Chart.BGColor = "ffffff"
  Chart.PlotAreaColor = "ffffff"
  Chart.ShowPlotBorder = false
  Chart.GraphPen = "000000"
  Chart.LegendX = 320
  Chart.LegendY = 20
  Chart.Square = 8
  Chart.Padding = 5
  Chart.ShowLegend = true
  Chart.ShowLegendBox = true
  Chart.LegendTextSize = 8
  Chart.LegendFont = "Arial"
  Chart.LegendBGColor = "ffffff"
  Chart.LegendColor = "000000"
  Chart.LegendVertical = true
  Chart.LegendAlign = 0
  Chart.LegendInvert = false
  Chart.LegendHideEmptyNames = false
  Chart.ShowNumbers = true
  Chart.ShowLabel = true
  Chart.LabelFont = "Arial"
  Chart.LabelSize = 8
  Chart.LabelBold = false
  Chart.LabelBGColor = "ffffff"
  Chart.LabelColor = "000000"
  Chart.Title = ""
  Chart.TitleX = 0
  Chart.TitleY = 0
  Chart.TitleFont = "Arial"
  Chart.TitleSize = 8
  Chart.TitleBold = false
  Chart.TitleBGColor = "ffffff"
  Chart.TitleColor = "000000"
  Chart.TitleTextAlign = 0
  Chart.JpegQuality = 100
  Chart.RNDColor = 0
  Chart.Decimals = 0
  Chart.ShowSeparator = false
  Chart.Prefix = ""
  Chart.Suffix = ""
  Chart.Transparent = false
  Chart.TransColor = "ffffff"
  Chart.ShowPlotBorder = false
  Chart.UseLZW = true

  'These 3 lines add some data for pie or bar charts
  Chart.AddData "Item1", 17, "ff0000"
  Chart.AddData "Item2", 28, "00ff00"
  Chart.AddData "Item3", 5, "0000ff"

  'These 4 lines add some points for a sample line graph
  Chart.AddPoint 0, 0, "ff0000", "Line1"
  Chart.AddPoint 30, 30, "ff0000", ""
  Chart.AddPoint 0, 10, "00ff00", "Line2"
  Chart.AddPoint 30, 25, "00ff00", ""

  'These 4 lines add some data for a stacked bar chart
  Chart.AddGroupedData "January", "Red things", 17, "ff0000"
  Chart.AddGroupedData "January", "Blue things", 28, "0000ff"
  Chart.AddGroupedData "February", "Red things", 5, "ff0000"
  Chart.AddGroupedData "February", "Blue things", 14, "0000ff"

  Response.ContentType = "Image/Gif"

  'Use one of the following lines to display a sample in GIF format

  Response.BinaryWrite Chart.GIFPie
  'Response.BinaryWrite Chart.GIFBar
  'Response.BinaryWrite Chart.GIFLine
  'Response.BinaryWrite Chart.GIFStackedBar
%>

