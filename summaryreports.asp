<!--#include file="openconn.asp"-->

<!--#include file="common.asp"-->
<%
server.scripttimeout=1000
''2007

 

%>
<html>
<head>
<script src="sorttable.js"></script>
 <style>
/* Sortable tables */
body
{
    font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
}
table.sortable thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
}
table.sortable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}
.oddrowcolor{
	background-color:#d4e3e5;
}
.evenrowcolor{
	background-color:#c3dde0;
}


.red
{
 
 font-weight:bold;
 color:Red;   
}

table.sortable2 thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable2 {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
	float:left;
	margin:10px
}
table.sortable2 th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable2 td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}
</style>
  
</head>

<body>
    <% 
        
        report=request("report")
        if report="" then
        
         %>

<table class="sortable2">
<tr><th>Summary</th></tr>
<tr><td><a href="summaryallsites.asp" target="_new">All Sites</a></td></tr>
<tr><td><a href="summaryalbert.asp" target="_new">Albert</a></td></tr>
<tr><td><a href="summarycoop.asp" target="_new">Coop</a></td></tr>
    <tr><td><a href="summarystarpark.asp" target="_new">StarPark</a></td></tr>
        <tr><td><a href="summaryandrew.asp" target="_new">Andrew</a></td></tr>
        <tr><td><a href="summaryother.asp" target="_new">Other</a></td></tr>
<tr><td><a href="cancellationsummary.asp" target="_new">Cancellation Summary</a></td></tr>
<tr><td><a href="oldsummary.asp" target="_new">Old Summary</a></td></tr>
    <tr><td><a href="newsummary.asp" target="_new">New Summary</a></td></tr>
        <tr><td><a href="newsummarycoop.asp" target="_new">New Summary Coop</a></td></tr>
       <tr><td><a href="transcriptionreport.asp" target="_new">Transcription Report</a></td></tr>
     <tr><td><a href="http://www.phoneandpay.co.uk/transcriptionreportday.asp" target="_new">Transcription Report day</a></td></tr>
    <tr><td><a href="reportticketsissued.asp">Tickets Issued</a></td></tr>
     <tr><td><a href="summarywaitingreminders.asp">Waiting Reminders</a></td></tr>

    <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/dvlarepstatus.asp">Dvlarep status</a></td></tr>
    <tr><td><a href="summarypinodata.asp">Plate Images site/date with no data</a></td></tr>
    <tr><td><a href="pimagesoverviewnew.asp">Plate Images Overview</a></td></tr>
       <tr><td><a href="summarygraphs.asp">Summary Graphs</a></td></tr>
    <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/plateimageinfo.asp">667 Plate Report</a></td></tr>
     <tr><td><a href="http://www.phoneandpay.co.uk/summary.asp">PP Summary</a></td></tr>
    <tr><td><a href="http://www.phoneandpay.co.uk/summaryBournemouth.asp">Bournemouth Summary</a></td></tr>
      <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/summarypopla.asp">Popla Summary</a></td></tr>

    <tr><td><a href="http://phoneandpay.co.uk/bookingcalllog.asp"> Parking Booking Call Log</a></td></tr>
   <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/summarydvla.asp">DVLA Summary</a></td></tr>
   <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/summarydvlayear.asp">DVLA Summary Year</a></td></tr>
     <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/summarysitenoticket.asp">Sites with no tickets </a></td></tr>

     <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/summarytop15.asp">Top 15 </a></td></tr>  
      <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/summarypv.asp">PV from plateimages </a></td></tr>  
    <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/summarylivesites.asp">Live Sites </a></td></tr>  
    <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/summaryallstages.asp">All Stages - ANPR </a></td></tr>  
        <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/summarypcnstages.asp">PCN stages by day/month/year </a></td></tr> 

      <tr><td><a href="http://traffica2.cleartoneholdings.co.uk/ppletter.asp"> Payment profile by letter  </a></td></tr> 
      <tr><td><a href="summaryreports.asp?report=paymentoverview"> Payment Overview  </a></td></tr> 
     <tr><td><a href="summaryreports.asp?report=cancellationoverview">Cancellation Overview  </a></td></tr> 

   
</table>



<table class="sortable2">
<tr><th>Last 30 days</th></tr>
<tr><td><a href="summaryallsites2.asp" target="_new">All Sites</a></td></tr>
<tr><td><a href="summaryalbert2.asp" target="_new">Albert</a></td></tr>
<tr><td><a href="summarycoop2.asp" target="_new">Coop</a></td></tr>
        <tr><td><a href="summarystarpark2.asp" target="_new">StarPark</a></td></tr>
        <tr><td><a href="summaryandrew2.asp" target="_new">Andrew</a></td></tr>
        <tr><td><a href="summaryother2.asp" target="_new">Other</a></td></tr>
</table>

<br style="clear:both">
<table class="sortable2"><form method=post action=printclaimform.asp target="_new">
<tr><th>PCN</th></tr>
<tr><td><input type=text name=pcn /></td></tr>
<tr><td><input type=submit name=submit value="Print Claim" /></td></tr>

</form></table>

    <% else %>

    <%  if report="paymentoverview" then
        
        
          sql="insert into reportsscheduled(sql,emailto,subject,filename)	values('paymentsoverview','esther@creativecarpark.co.uk,Simon.a@bemrosemobile.co.uk','Payment Overview " & date() & " ','PaymentOverview" & cisodate(date() )& "' )"	
       ''Simon.a@bemrosemobile.co.u;
        
     '   response.write sql
         objconn.execute sql
        '
      
        response.write "Report added to the schedule" 
        
        
        end if
        
        
         if report="cancellationoverview" then
        
        
          sql="insert into reportsscheduled(sql,emailto,subject,filename)	values('cancellationoverview','esther@creativecarpark.co.uk,Simon.a@bemrosemobile.co.uk','Cancellation Overview " & date() & " ','CancellationOverview" & cisodate(date() )& "' )"	
       ''
        
     '   response.write sql
         objconn.execute sql
        '
      
        response.write "Report added to the schedule" 
        
        
        end if
        
         %>



    <% end if %>
</body></html>
