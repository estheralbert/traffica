﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
li {
	margin: 20px 25px 20px 20px;	
}
</style>
</head>

<body>

<p><strong><u>[DAVID MARTIN ELDERS]</u></strong><strong><u> </u></strong><br />
  [9 BOUNDS CROFT] <br />
  [GREENLEYS]<br />
  [MILTON KEYNES]<br />
[MK12 6AW]</p>
<p> <br />
  [15/10/2014]</p>
<p>Our  Ref: The Co-operative/AJD/[4293956062<strong>]</strong></p>
<p>Dear  Sir/Madam,<br />
  <strong>Re:              [Civil Enforcement Limited -v- DAVID MARTIN ELDERS</strong>]<br />
  <strong>	                  [Claim Number: A93YM587]</strong></p>
<p>Please find attached our Witness  Statement together with an updated proposed cost schedule to include the  Hearing.  Unless matters are resolved  within the next 7-days we will be forced to incur further legal costs. </p>
<p>The original claim was for £165.00 (including court issuing fees). We have  also incurred legal costs of an additional £115.00 for drafting the Witness  Statement, general handling of this file.</p>
<p>As mentioned above, unless this matter  is resolved, we will be required to pay a Hearing fee plus there will be the  costs of preparation, attendance and travel which we estimate to circa £215.00,  totalling <strong>£495.00</strong></p>
<p>Notwithstanding the above, we are  prepared to accept £165.00 within the  next 7-days as full and final settlement. Payment can be made through our  website at <a href="http://www.ce-service.co.uk">www.ce-service.co.uk</a>. </p>
<p>Finally, should you wish to discuss  any aspect of the case please do not hesitate to contact us on <strong>0870 919 2001</strong></p>
<p>&nbsp;</p>
<p>Yours faithfully,</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong><u>For and on behalf of </u></strong><br />
  <strong><u>Civil Enforcement Limited </u></strong></p>
<br style="page-break-after: always" />

<div align="center">
	<table width="75%">
		<tr>
       	  <td align="left"><strong><u>IN [NORTHAMPTON COUNTY COURT] </u></strong></td>
            <td align="right"><strong>Claim no. [A93YM587]</strong></td>
      </tr>
 	</table>   
</div>
<p></p>
<p><strong>&nbsp;</strong></p>
<p style="letter-spacing:5px"><strong>BETWEEN</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div align="center">
<table border="0" cellspacing="0" cellpadding="0" width="75%">
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>CIVIL ENFORCEMENT LIMITED</strong></p></td>
    <td width="92" valign="top"><p align="right"><strong><u>Claimant</u></strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>&nbsp;</strong></p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>- V-</strong></p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top">
     </td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>[DAVID MARTIN ELDERS]</strong><strong>   </strong><strong> </strong></p></td>
    <td width="92" valign="top"><p align="right"><strong><u>Defendant</u></strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center">&nbsp;</p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" align="center" valign="middle" style="border-bottom:3px #000 solid; border-top: 3px #000 solid; padding:20px 0 20px 0;">
    	<p align="center">
           <strong>FIRST WITNESS STATEMENT OF ASHLEY COHEN</strong><br />
      </p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
</table>
</div>
<p><strong>&nbsp;</strong></p>
<p>I Ashley Cohen of Horton House, Exchange Flags, Liverpool  L2 3PF for Civil Enforcement Limited will say as follows:</p>
<ol style="margin:5px 0 5px 0;">
  <li>I       am duly authorised by the Claimant and I make this statement in support of       the claim brought against the Defendant.</li>
  <li>As       set out in the Particulars of Claim, the Claimant has the right to manage       the Car Park through a contract with The Co-operative Group Limited of 1       Angel Square, Manchester, M60 0AG.</li>
  <li>The       Claimant uses automatic number plate recognition (&ldquo;ANPR&rdquo;) cameras within       the Car Park. APNR cameras work by capturing images of the text displayed       on vehicles&rsquo; number plates.</li>
  <li>There       are many signs displayed in the Car Park which explain the terms and       conditions of parking. These signs are clear and visible for all drivers       to see. See attached Exhibit marked bundle 1,</li>
  <li>The       Claimant&rsquo;s APNR cameras recorded the Defendant&rsquo;s vehicle arriving and       leaving the Car. The Defendant by keeping their vehicle in the Car Park       accepted the terms and conditions by way of conduct and consequently       incurred a charge.</li>
  <li>The       Claimant sent the Defendant a parking charge notice (&ldquo;PCN&rdquo;), requesting       payment. A reminder was thereafter sent.</li>
  <li>The       matter was then passed to our collection agents and further three letters       were sent to the Defendant</li>
  <li>A       valid contract between the defendant and the claimant has been made       because acceptance of the claimant&rsquo;s unilateral offer was communicated by       the defendant&rsquo;s conduct through parking their car in the parking spaces       provided.</li>
  <li>The       car park has sufficient lighting and warnings for the Defendant to have       acknowledged the signs.</li>
  <li>We       would refer the court to Roch LJ ratio in <em><u>Vine</u> v <u>Waltham Forest London Borough Council</u> [2000]       4 All ER 169</em> and <em><u>Vine</u></em> where he states that:- </li>
<blockquote><em>&ldquo;the presence of notices which are posted where they are  bound to be seen, for example at the entrance to a private car park, which are  of a type which the car driver would be bound to have read, will lead to a  finding that the car driver had knowledge of and appreciated the warning&rdquo;</em>.  </blockquote>
  <li>The  nature of the relationship between the Claimant and the Defendant is  contractual; provided that the drivers have seen the signs (see above). The car  park is private land and consequently drivers require permission before parking  on the land. The Claimant granted permission by way of making a unilateral  offer on their signs displayed in the car parks and the Defendant accepted this  offer and its terms by way conduct by parking on the land. The consideration  for this agreement is the provision of a parking space and the forbearance to  sue (i.e. trespass to land). The driver will be in breach of the contract if he  refuses to pay the charges due.</li>
  <li>We  have attached a copy of our signs which are clearly plain and intelligible. It  is abundantly clear that the contract terms relating to charges is a core term.</li>
  <li>We  would also draw to Court attention <em>Combined  Parking Solutions (&quot;CPS&quot;) v Stephen James Thomas</em><strong> </strong>whereby the<strong></strong>Judge ruled that &ldquo;on the  balance of probabilities&rdquo; the driver was the Registered Keeper and that he had therefore  knowingly entered into a contract with the Car Park Operator. The Court ordered  that the Registered Keeper pay the charges plus interest, court fees and  expenses. Moreover, The Protection of Freedoms Act 2012 effectively adds  credence to the Judgment as in certain circumstances if the registered keeper  fails to identify the driver the registered keeper becomes liable to pay the  parking charge.</li>
  <li>The  Claimant manages car parks authorising drivers to park at the site on a  contractual basis. The different options are outlined on the signage. The  amount sought at Parking Charge Notice level is a term of contract optioned by  the Defendants conduct. The legal basis of this contract is outlined below:</li>
  <li>The  scope of the rule against penalties is narrow.   As the Court of Appeal stated in <em>Euro  London Appointments Ltd v Claessens International Ltd</em> [2006] EWCA Civ 385  at para 16-17, a penalty clause is &ldquo;<em>a sum  which, by the terms of a contract, a promisor agrees to pay to the promisee in  the event of non-performance by the promisor of one or more of the obligations  and which is excess of the damage caused by such non-performance</em>.&quot;  </li>
  <li>The PCN was for the sum due by the Defendant  in consideration for the Claimant making parking facilities available to  them.   To borrow the words of Lord  Roskill (with whom the other members of the House of Lords agreed) in <em>Export Credits Guarantee Department v  Universal Oil Products Co and others</em> [1983] 1 WLR 399, 402H: <em>&ldquo;The clause was not a penalty clause because  it provided for payment of money on the happening of a specified event other  than a breach of contractual duty owed by the contemplated payer to the  contemplated payee.&quot;        </em></li>
  <li>Further by concluding that the sum claimed must  be a genuine pre-estimation of the loss is an error.  Such a limitation arises only in respect of  penalty clauses, not in respect of the contractually agreed consideration for  performance of an obligation. </li>
  <li>The <u>Unfair Terms in       Consumer Contracts Regulations 1999</u> (&ldquo;UTCCR&rdquo;) provide that where terms       of a contract have not been individually negotiated, as is the case here,       there is a requirement that those terms be fair. However, the assessment       of fairness of a term does not apply to the &lsquo;core terms&rsquo; of the contract,       including the <em>&ldquo;…adequacy of the       price or remuneration, as against the goods or services supplied in       exchange&rdquo;</em> (Regulation 6(2)). UTCCR also require that the contract       terms be in plain and intelligible language (Regulation 7). </li>
  <li>The       Defendant is acting unreasonably in defending this claim. We would refer       the Court to CPR rule 27.14(2)(g) – &ldquo;unreasonable behaviour costs&rdquo;.  The effect of this rule is that where a       party has behaved unreasonably, the usual costs restrictions for small       claims do not apply and full costs are recoverable.  </li>
  <li>As a result of the above the Claimant has been       compelled to incur unnecessary legal costs in pursuing this debt and would       request a cost order in accordance with CPR to be awarded in favour of the       Claimant.</li>
</ol>
<p>&nbsp;</p>
<p><strong><u>Statement of Truth</u></strong><br />
  I confirm that the contents of this statement are true.</p>
<p><strong>SIGNED:</strong> ……<img src="ACsig.jpg" alt="sig" width="139" height="62" align="baseline" />………..<strong></strong><br />
  <strong>DATED:         [</strong>15/10/2014] </p>

<br style="page-break-after: always" />

<div align="center">
	<table width="75%">
		<tr>
       	  <td align="left"><strong><u>IN [NORTHAMPTON COUNTY COURT] </u></strong></td>
            <td align="right"><strong>Claim no. [A93YM587]</strong></td>
      </tr>
  </table>   
</div>
<p></p>
<p><strong>&nbsp;</strong></p>
<p style="letter-spacing:5px"><strong>BETWEEN</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div align="center">
<table border="0" cellspacing="0" cellpadding="0" width="75%">
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>CIVIL ENFORCEMENT LIMITED</strong></p></td>
    <td width="92" valign="top"><p align="right"><strong><u>Claimant</u></strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>&nbsp;</strong></p>
      <p align="center"><strong>&nbsp;</strong></p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>- V-</strong></p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>&nbsp;</strong></p>
      <p align="center"><strong>&nbsp;</strong></p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>[DAVID MARTIN ELDERS]</strong><strong>   </strong><strong> </strong></p></td>
    <td width="92" valign="top"><p align="right"><strong><u>Defendant</u></strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center">&nbsp;</p>
      <p align="center"><strong>&nbsp;</strong></p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" align="center" valign="middle" style="border-bottom:3px #000 solid; border-top: 3px #000 solid; padding:20px 0 20px 0;">
    	<p align="center">
           <strong>CLAIMANTS SCHEDULE OF COSTS</strong><br />
      </p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
</table>
</div>
<p>&nbsp;</p>
<div align="center">
<table border="1" cellspacing="0" cellpadding="10px" width="75%">
  <tr>
    <td width="308">
      Drafting of    Claim / Witness Statement </td>
    <td width="308"><p align="right">£70.00</p></td>
  </tr>
  <tr>
    <td width="308"><p>General File    Attendance</p></td>
    <td width="308"><p align="right">£45.00</p></td>
  </tr>
  <tr>
    <td width="308"><p>Hearing Fee</p></td>
    <td width="308"><p align="right">£25.00</p></td>
  </tr>
  <tr>
    <td width="308"><p>Preparation for    Hearing</p></td>
    <td width="308"><p align="right">£65.00</p></td>
  </tr>
  <tr>
    <td width="308"><p>Claimants    Attending Court (including travel)</p></td>
    <td width="308"><p align="right">£125.00</p></td>
  </tr>
  <tr>
    <td width="308"><p><strong>TOTAL</strong></p></td>
    <td width="308"><p align="right"><strong>£330.00</strong></p></td>
  </tr>
</table>
</div>
<p>&nbsp;</p>
<br style="page-break-after: always" />
<div align="center">
	<table width="75%">
		<tr>
       	  <td align="left"><strong><u>IN [NORTHAMPTON COUNTY COURT] </u></strong></td>
            <td align="right"><strong>Claim no. [A93YM587]</strong></td>
      </tr>
 	</table>   
</div>
<p></p>
<p><strong>&nbsp;</strong></p>
<p style="letter-spacing:5px"><strong>BETWEEN</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div align="center">
<table border="0" cellspacing="0" cellpadding="0" width="75%">
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>CIVIL ENFORCEMENT LIMITED</strong></p></td>
    <td width="92" valign="top"><p align="right"><strong><u>Claimant</u></strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>&nbsp;</strong></p>
      <p align="center"><strong>&nbsp;</strong></p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>- V-</strong></p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>&nbsp;</strong></p>
      <p align="center"><strong>&nbsp;</strong></p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center"><strong>[DAVID MARTIN ELDERS]</strong><strong>   </strong><strong> </strong></p></td>
    <td width="92" valign="top"><p align="right"><strong><u>Defendant</u></strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" valign="top"><p align="center">&nbsp;</p>
      <p align="center"><strong>&nbsp;</strong></p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="67" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="431" align="center" valign="middle" style="border-bottom:3px #000 solid; border-top: 3px #000 solid; padding:20px 0 20px 0;">
    	<p align="center">
           <strong>EXHIBIT -- BUNDLE 1</strong><br />
      </p></td>
    <td width="92" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
</table>

</div>
<p>&nbsp;</p><p>&nbsp;</p>
</body>
</html>
