jQuery.fn.dataTableExt.oSort['it_date-asc']  = function(a,b) {
            var itDatea = a.split('/');
            var itDateb = b.split('/');
             
            var itYearTimeA = itDatea[2].split(' ');
            var itTimeA = itYearTimeA[1].split(':');
             
            var itYearTimeB = itDateb[2].split(' ');
            var itTimeB = itYearTimeB[1].split(':');
             
            var itHourA = itTimeA[0];
            var itMinuteA = itTimeA[1];
            var itSecondA = itTimeA[2];
            var itDayA = itDatea[0];
            var itMonthA = itDatea[1];
             
            var itHourB = itTimeB[0];
            var itMinuteB = itTimeB[1];
            var itSecondB = itTimeB[2];
            var itDayB = itDateb[0];
            var itMonthB = itDateb[1];
             
            var itYearA = itYearTimeA[0];
            var itYearB = itYearTimeB[0];
             
            var x = (itYearA + itMonthA+ itDayA + itHourA + itMinuteA + itSecondA) * 1;
            var y = (itYearB + itMonthB + itDayB + itHourB + itMinuteB + itSecondB) * 1;
      
            return ((x < y) ? -1 : ((x > y) ?  1 : 0));
            };
  
            jQuery.fn.dataTableExt.oSort['it_date-desc'] = function(a,b) {
            var itDatea = a.split('/');
            var itDateb = b.split('/');
                 
            var itYearTimeA = itDatea[2].split(' ');
            var itTimeA = itYearTimeA[1].split(':');
             
            var itYearTimeB = itDateb[2].split(' ');
            var itTimeB = itYearTimeB[1].split(':');
             
            var itHourA = itTimeA[0];
            var itMinuteA = itTimeA[1];
            var itSecondA = itTimeA[2];
            var itDayA = itDatea[0];
            var itMonthA = itDatea[1];
             
            var itHourB = itTimeB[0];
            var itMinuteB = itTimeB[1];
            var itSecondB = itTimeB[2];
            var itDayB = itDateb[0];
            var itMonthB = itDateb[1];
             
            var itYearA = itYearTimeA[0];
            var itYearB = itYearTimeB[0];
             
            var x = (itYearA + itMonthA+ itDayA + itHourA + itMinuteA + itSecondA) * 1;
            var y = (itYearB + itMonthB + itDayB + itHourB + itMinuteB + itSecondB) * 1;
 
     
            return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
            };
