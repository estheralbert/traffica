
<!--#include file="openconn.asp"-->

<!--#include file="common.asp"-->
<%
    spcn=request("pcn")
    sql="select v.pcnsent,r.origsentdate,reason,registration,make,model,color,ti.username,shortdesc,v.ticketprice,v.reducedamount from violators v left join reissues r on r.violatorid=v.id  left join [site information] si on si.sitecode=v.site left join ticketusers ti on ti.id=v.ticketuserid where fullpcn=" & tosql(spcn,"text")
    openrs rs,sql
    pcnsent=rs("pcnsent")
    origpcnsent=rs("origsentdate")
    if not isnull(origpcnsent) then pcnsent=origpcnsent
    reason=rs("reason")
    registration=rs("registration")
    make=rs("make")
    model=rs("model")
    username=rs("username")
    scolor=rs("color")
    site=rs("shortdesc")
    ticketprice=rs("ticketprice")
    reducedamount=rs("reducedamount")
    %>
<html>
    <head>
        <title>Mobile Ticket</title>
        <link href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed" rel="stylesheet">
        <style type="text/css">
            body{font-family:'Barlow Semi Condensed', sans-serif;width:950px;margin:0px auto;border:1px solid grey;}
            .contentWrapper {max-width:950px;margin:5px 15px 10px 25px}
            .box{border:1px solid #000; padding:5px;text-align:center;margin:15px 0px 0px 0px;}
            .box h1 {margin:0px;}
            h1 {font-size:24pt;text-transform: uppercase;font-weight:normal;}
            h2 {font-size:20pt;text-transform: uppercase;font-weight:bold;margin:20px 0px 0px 0px;}
            .data{display:inline-block;height:36px;text-transform: uppercase;float:left;text-align:left;font-size:18pt;word-spacing:5px;}
            .lbl {width:260px;font-weight:bold;clear:left;padding-left:35px;}            
            .caps {text-transform: uppercase;}
            p {font-size:14pt;margin:30px 0px;}
            p.closeContent {margin:0px 0px 0px 0px;}            
        </style>        
    </head>
    <body>
        <div class="contentWrapper">
        <div id="logoWrapper">
            <img src="mtlogo.jpg" alt="Civil Enforcement LTD" />
        </div>
        <div class="box">
            <h1>PARKING ENFORCEMENT NOTICE</h1>
        </div>
        <p>We require payment of this <b>Parking Charge Notice</b><br />(PCN), in accordance with the Parking Terms and<br />Conditions clearly stated on the signage in the car park.</p>
        <div class="box">
            <span class="data lbl">PCN NUMBER:</span><span class="data"><%=spcn %></span>
            <span class="data lbl">PCN ISSUE DATE:</span><span class="data"><%=formatdatetime(pcnsent,2) %></span>
            <span class="data lbl">PCN ISSUE TIME:</span><span class="data"><%=formatdatetime(pcnsent,4) %></span>
            <span class="data lbl">PCN ISSUED BY:</span><span class="data"><%=username %></span>
            <span class="data lbl">PCN ISSUED REASON:</span><span class="data"><%=reason %></span>
            <div style="height:20px;clear:both;"></div>
            <span class="data lbl">LOCATION:</span><span class="data"><%=site %></span>
            <span class="data lbl">VEHICLE REG NO:</span><span class="data"><%=registration %></span>
            <span class="data lbl">MAKE:</span><span class="data"><%=make %></span>
            <span class="data lbl">MODEL:</span><span class="data"><%=model %></span>
            <span class="data lbl">COLOUR:</span><span class="data"><%=scolor %></span>
            <div style="clear:both;"></div>
        </div>
        <p class="caps">
            AMOUNT DUE IF PAID WITHIN 14 DAYS: �<%=formatnumber(reducedamount,2) %>
            <br />
            AMOUNT DUE IF PAID WITHIN 28 DAYS: �<%=formatnumber(ticketprice,2) %>
        </p>
        <div class="box">
            <h1>How To Pay</h1>
        </div>
        <h2>By Web</h2>
        <p class="closeContent">Payment can be made by debit or credit card online at <b>www.ce-service.co.uk</b></p>
        <h2>By Phone</h2>
        <p class="closeContent">Payment can be made by debit or credit card by telephoning <b>0115 822 5020</b></p>
        <h2>By Post</h2>
        <p class="closeContent">Send a cheque payable to Civil Enforcement Ltd (Creditor), to Civil Enforcement Ltd, Horton House, Exchange Flags, Liverpool, L2 3PF Company Reg. No. 05645677</p>
        <h2>REPRESENTATIONS</h2>
        <p class="closeContent">In writing only, within 28 days to the address shown above or at <b>www.ce-service.co.uk</b>, stating the PCN Number.</p>
        <p>Our full appeals process can be found at<br />www.ce-service.co.uk including details of POPLA<i>(the independent appeals service)</i>.</p>
        <div class="box">
            <h1>Payment Slip</h1>
        </div>
        <p >Please detach this payment slip and return it with your payment. You should write the PCN Number on the reverse of your cheque.</p>
        <p>If the amount remains unpaid we may apply to the DVLA to request the Registered Keeper's details to enable the collection of relevant fees.</p>
        <p>This PCN has been issued under Schedule 4 of the Protection of Freedoms Act 2012.</p>
        <p >In order to enforce the parking contract and/or to protect legitimate interests, your personal data may be processed
            as follows: passed to the DVLA to request Registered Keeper's details to send a Parking Charge Notice (PCN);
            shared with police or security organisations in order to detect or prevent crimes; shared with third parties in
            order to collect any sums due and with analytics organisations to understand vehicle and consumer
            behaviour and improve parking. You have the right to make the following requests about personal data we may hold:
            to inform you how and why it is processed; to give you access to it; to rectify any incorrect information; to
            delete it; to restrict our use of it; to ask us to transfer a copy to a third party; to object to our use of>
            it. Data protection law requires us to verify your identity before providing information, respond to your
            request and to tell you why, if we do not agree with it. dataprotectionofficer@ce-service.co.uk
            www.ce-service.co.uk/privacypolicy</p>
        <p style="text-align:center;"><i>Please tear off</i></p>
        </div>
    </body>
    </html>

  