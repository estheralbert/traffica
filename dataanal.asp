<!--#include file="openconn.asp"-->

<!--#include file="common.asp"-->

<html><head>
<style>
body{
font:Verdana;
}
h1{
font-weight:bold;
font-size:140%;
}

</style>
</head>
<body>




<%
server.scripttimeout=10000

sql="select site,avg(datediff(n,entrydate,exitdate)) as avgduration from dataanal group by site "
response.Write "<h1>Average Stay</h1><table>"
openrs rs,sql
ssite=""
sduration=""
do while not rs.eof 
response.Write "<tr><td>" & rs("site") & "</td><td>" & rs("avgduration") & "</td></tr>"
ssite=ssite & rs("site") & ","
sduration=sduration & rs("AVGduration") & ","
rs.movenext
loop

closers rs
response.Write "</table>"
ssite=left(ssite,len(ssite)-1)
sduration=left(sduration,len(sduration)-1)
asites=split(ssite,",")
adurations=split(sduration,",")
%>
<APPLET CODE="HanengCharts_SampleApplet.class" WIDTH=460 HEIGHT=260>
<%  
for i=lbound(adurations) to ubound(adurations)
 %>
<param name="value_<%=i+1 %>" value="<%=adurations(i) %>">
<% next %>
<%for i=lbound(asites) to ubound(asites) %>

<param name="text_<%=i+1 %>" value="<%=asites(i) %>">
<% next %>
</APPLET>



<h1>Repeat Vehicles</h1>
These are vehicles showing more then 20 times <br />

<table><tr><td>Registration</td><td>Number of Times</td></tr>

<% sql="select registration,count(registration) as count from dataanal group by registration having count(registration)>20 order by count(registration) desc"

openrs rs,sql
do while not rs.eof 
response.Write "<tr><td>" & rs("registration") & "</td><td>" & rs("count") & "</td></tr>"
rs.movenext
loop

closers rs
 %>
 </table>
<h1>Occupancy - cars in the carpark</h1>
<% 
sql="exec spoccupancy"
openrs rs,sql
response.write "<table><tr><th>Site</th><th>1 AM</th><th>3 AM</th><th>8-12 PM</th><th>12-4 PM</th><th>4-8 PM</th><th>11 PM</th></tr>"

do while not rs.eof 
response.write "<tr><td>" & rs("site") & "</td><td>" & rs("1AM") & "</td><td>" & rs("3AM") & "</td><td>" & rs("8-12PM") & "</td><td>" & rs("12-4PM") & "</td><td>" & rs("4-8PM") & "</td><td>" & rs("11PM") & "</td></tr>"
rs.movenext



loop




%>
</table>
<h1>Businest Entrance times (by half hour)</h1>




<%

Sub ShowStackedBarChart(ByRef aValues, ByRef aLabels, ByRef strTitle, _
	ByRef strXAxisLabel, ByRef strYAxisLabel, ByRef aColors, ByRef aToolTip, ByRef aBars)

	' Some user changable graph defining constants
	' All units are in screen pixels
	Const GRAPH_WIDTH  = 400  ' The width of the body of the graph
	Const GRAPH_HEIGHT = 300  ' The heigth of the body of the graph
	Const GRAPH_BORDER = 4    ' The size of the black border
	Const GRAPH_SPACER = 2    ' The size of the space between the bars

	Const TABLE_BORDER = 0
	'Const TABLE_BORDER = 10

	' Declare our variables
	Dim i, ii, iTmp, aTmp()
	Dim iMaxValue
	Dim iBarWidth
	Dim iBarHeight
	Dim strColumnColor

	' Get the maximum value in the data set
	iMaxValue = 0
	For I = 0 To UBound(aValues)
		If iMaxValue < aValues(I) Then iMaxValue = aValues(I)
	Next 'I

	If iMaxValue  = 0 Then Exit Sub
	'Response.Write iMaxValue & "&nbsp;&nbsp;" ' Debugging line

	ReDim Preserve aTmp(UBound(aColors))

	' Calculate the width of the bars
	' Take the overall width and divide by number of items and round down.
	' I then reduce it by the size of the spacer so the end result
	' should be GRAPH_WIDTH or less!
	iBarWidth = (GRAPH_WIDTH \ (UBound(aValues) + 1)) - GRAPH_SPACER
	'Response.Write iBarWidth ' Debugging line

	' Start drawing the graph
	%>
	<TABLE BORDER="<%= TABLE_BORDER %>" CELLSPACING="0" CELLPADDING="0">
		<TR>
			<TD COLSPAN="3" ALIGN="center"><H2><%= strTitle %></H2></TD>
		</TR>
		<TR>
			<TD VALIGN="center"><B><%= strYAxisLabel %></B></TD>
			<TD VALIGN="top">
				<TABLE BORDER="<%= TABLE_BORDER %>" CELLSPACING="0" CELLPADDING="0">
					<TR>
						<TD ROWSPAN="2">
							<IMG SRC="images/spacer.gif" BORDER="0"
								WIDTH="1" HEIGHT="<%= GRAPH_HEIGHT %>">
						</TD>
						<TD VALIGN="top" ALIGN="right"><%= iMaxValue %>&nbsp;</TD>
					</TR>
					<TR>
						<TD VALIGN="bottom" ALIGN="right">0&nbsp;</TD>
					</TR>
				</TABLE>
			</TD>
			<TD>
				<TABLE BORDER="<%= TABLE_BORDER %>" CELLSPACING="0" CELLPADDING="0">
					<TR>
						<TD VALIGN="bottom"><IMG SRC="images/spacer_black.gif"
							BORDER="0"
							WIDTH="<%= GRAPH_BORDER %>"
							HEIGHT="<%= GRAPH_HEIGHT %>"></TD>
						<%
						' We're now in the body of the chart.
						' Loop through the data showing the bars!
						For I = 0 To UBound(aValues)
							iBarHeight = Int((aValues(I) / iMaxValue) * GRAPH_HEIGHT)

							' This is a hack since browsers ignore a 0 as an image dimension!
							If iBarHeight = 0 Then iBarHeight = 1
							%>
							<TD VALIGN="bottom"><IMG SRC="images/spacer.gif"
								BORDER="0"
								WIDTH="<%= GRAPH_SPACER %>"
								HEIGHT="1"></TD>
							<TD VALIGN="bottom"><%	
								For ii = UBound(aColors) - 1 To 0 Step - 1
									aTmp(ii) = Int((aBars(ii+1,I) / iMaxValue) * GRAPH_HEIGHT)
									%><IMG SRC="images/spacer_<%= aColors(ii) %>.gif"
									BORDER="0" WIDTH="<%= iBarWidth %>"
									HEIGHT="<%= aTmp(ii) %>"
									ALT="<%= aBars(ii + 1, I) & vbTab & aToolTip(ii) %>"><br /><%  
								Next 'ii
							%></TD><%								
						Next 'I
						%>
					</TR>
				</TABLE>
				<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0">
					<!-- I was using GRAPH_BORDER + GRAPH_WIDTH but it was
					moving the last x axis label -->
					<TR>
						<TD COLSPAN="<%= (UBound(aValues) + 1) %>"><IMG SRC="images/spacer_black.gif"
							WIDTH="<%= GRAPH_BORDER + ((UBound(aValues) + 1) * (iBarWidth + GRAPH_SPACER)) %>"
							HEIGHT="<%= GRAPH_BORDER %>" BORDER="0"></TD>
					</TR>
						<%
						' The label array is optional and is really only useful
						' for small data sets with very short labels!
						%>
						<% If IsArray(aLabels) Then %>
							<TR>
							<% For I = 0 To UBound(aValues)
								iTmp = (GRAPH_BORDER + ((UBound(aValues) + 1) * _
									(iBarWidth + GRAPH_SPACER))) / (UBound(aValues) +1)
								iTmp = Int(Round(iTmp))
								%>
								<TD WIDTH="<%= iTmp %>" ALIGN="center"><FONT SIZE="2"><%= aLabels(I) %></FONT></TD>
							<% Next 'I %>;
							</TR>
						<% End If %>
				</TABLE>
			</TD>
		</TR>
	</TABLE>
	<%
End Sub


 


 %>
 
 
 </body></html>