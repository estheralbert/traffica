<!--#include file="openconn.asp"-->

<!--#include file="common.asp"-->
<%
server.scripttimeout=1000
''2007
 '   if session("admin")="" then response.Redirect "admin.asp"
 

%>
<html>
<head>
<script src="sorttable.js"></script>
 <style>
/* Sortable tables */
body
{
    font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
}
table.sortable thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
}
table.sortable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}
.oddrowcolor{
	background-color:#d4e3e5;
}
.evenrowcolor{
	background-color:#c3dde0;
}


.red
{
 
 font-weight:bold;
 color:Red;   
}

table.sortable2 thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable2 {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
}
table.sortable2 th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable2 td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}
</style>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
  $(function() {
    $( "#startdate").datepicker({ dateFormat: "dd-mm-yy" }).val()
        $("#enddate").datepicker({ dateFormat: "dd-mm-yy" }).val()
  });
  </script>
</head>

<body>
   
    <ul>
        <li><a href="reporttimectop.asp">Time from contravention to PCN issue</a></li>
        <li><a href="http://traffica2.cleartoneholdings.co.uk/summarydvlas.asp">PCN Numbers</a></li>
        <li><a href="http://traffica2.cleartoneholdings.co.uk/admin.asp?cmd=livesitesrepbypcnsent2&type=anpr">PCNs per site</a></li>
        
        <li><a href="http://traffica2.cleartoneholdings.co.uk/paymentratereport.asp">Internal collections</a></li>
        <li><a href="http://traffica2.cleartoneholdings.co.uk/paymentsbydayreport.asp">Payment by day report</a></li>
        <li><a href="http://traffica2.cleartoneholdings.co.uk/reportoutstandingpcn.asp">Outstanding payment reports</a></li>

        <li><a  href="reportpaymentdays.asp">Collections Report</a></li>

        <li><a href="stuartcancellationreport.asp">Cancellation Report</a></li>

        <li><a href="stuartPaymentReport.asp">Collections Report</a></li>
    </ul>

    </body>
    </html>