<%Response.Buffer = true %>
<!--#include file ="common.asp"-->
<!--#include file ="openconnTest.asp"-->
<!--#include file="JSON_latest.asp"-->
<%

on error resume next

cmd						= Request("cmd")
isDebug					= Request("debug")
Response.ContentType	= "application/json"

if cmd="login" then
	username	= request("username")
	password	= request("password")
	
    sql	=	"exec spAppLogin " &_
				"@username	= " & tosql(username,"text")  & _
				",@password	= " & tosql(password,"text")

    if isDebug then Response.Write(ShowSQLInJSON(sql))

    QueryToJSON(objconn, sql).Flush
end if

if cmd="getUserSites" then
	userid	= request("userid")
	
    sql	=	"exec dbo.spGetUserSites " & _
				"@userId = " & tosql(userid,"Number") 

    if isDebug then Response.Write(ShowSQLInJSON(sql))

    QueryToJSON(objconn, sql).Flush
end if

if cmd="checkCarPermits" then
	sitecode = request("sitecode")
	reg = request("reg")
	
    sql	=	"exec dbo.spCheckPermits " & _
				"@sitecode = " & tosql(sitecode,"text") & _
				",@reg = " & tosql(reg,"text") & _
				",@mydate = null"

    if isDebug then Response.Write(ShowSQLInJSON(sql))

    QueryToJSON(objconn, sql).Flush
end if

if cmd="getCarDetails" then
	reg	= request("reg")
	
    sql	=	"exec dbo.spGetCarDetailsRec " & _
				"@reg=" & tosql(reg,"text") 

    if isDebug then Response.Write(ShowSQLInJSON(sql))

    QueryToJSON(objconn, sql).Flush
end if

if cmd="getViolationReasons" then
	sitecode = request("sitecode")

    sql	=	"exec dbo.spGetViolationReasons " & _
				"@siteCode=" & tosql(sitecode,"text") 

    if isDebug then Response.Write(ShowSQLInJSON(sql))

    QueryToJSON(objconn, sql).Flush
end if

if cmd="getLatestUserTickets" then
	userid = request("userid")

    sql	=	"exec dbo.spGetLatestUserTickets " & _
				"@userId =" & tosql(userid,"Number") 

    if isDebug then Response.Write(ShowSQLInJSON(sql))

    QueryToJSON(objconn, sql).Flush
end if

if cmd="getTempTicketDetails" then
	userid = request("userid")

    sql	=	"exec dbo.spGetTempTicketDetails " & _
				"@userId =" & tosql(userid,"Number") 

    if isDebug then Response.Write(ShowSQLInJSON(sql))

    QueryToJSON(objconn, sql).Flush
end if


if cmd="printTempTicket" then
	ticketid = request("ticketid")

    sql	=	"declare @id bit; " &_
			"exec  dbo.spPrintTicket " &_ 
				 "@tempid			= " & tosql(ticketid,"Number") &_
				 ",@IsParkingApp	= 1 " &_
				 ",@id				= @id output;"

    if isDebug then Response.Write(ShowSQLInJSON(sql))

    QueryToJSON(objconn, sql).Flush
end if

if cmd="showTicketInfo" then
	violatorId = request("violatorId")

    sql	=	"exec  dbo.spShowTicketInfo " &_ 
				"@violatorId =" & tosql(violatorId,"Number")

    if isDebug then Response.Write(ShowSQLInJSON(sql))

    QueryToJSON(objconn, sql).Flush
end  if

if cmd="getTempTicketDetails" then
	ticketid = request("ticketid")

    sql	=	"exec dbo.getTempTicketDetails " & _
				"@tempTicketId =" & tosql(ticketid,"Number") 

    if isDebug then Response.Write(ShowSQLInJSON(sql))

    QueryToJSON(objconn, sql).Flush
end if

if cmd="insertTempTicket" then
	reg = request("reg")
	sitecode = request("sitecode")
	ticketuserid = request("ticketuserid")
	reason = request("reason")
	filename1 = request("filename1")
	filename2 = request("filename2")
	filename3 = request("filename3")

    sql	=	"exec spInsertTempTicketNew " & _ 
				"@vehicle		= " & tosql(reg,"text") & _
				",@sitecode		= " & tosql(sitecode,"text") & _
				",@ticketuserid	= " & tosql(ticketuserid,"Number") & _
				",@reason		= " & tosql(reason,"text") & _
				",@filename1	= " & tosql(filename,"text") & _
				",@filename2	= " & tosql(filename2,"text") & _
				",@filename3	= " & tosql(filename3,"text")
			
    if isDebug then Response.Write(ShowSQLInJSON(sql))
	
	objconn.execute sql

	Response.Write("[{""result"":""success""}]")
end if

If cmd="LogException" Then
	msg 	= request("msg")
	extype 	= request("type")
	source	= request("source")
	userId	= request("userId")

    sql = "exec dbo.mobileExceptionLoggingToDataBase " & _
                "@ExceptionMsg      = " & tosql(msg,"text") & _
                ",@ExceptionType    = " & tosql(extype,"text") & _
                ",@ExceptionSource  = " & tosql(source,"text") & _
                ",@UserId           = " & tosql(userId,"Number")

    if isDebug then Response.Write(ShowSQLInJSON(sql))

    QueryToJSON(objconn, sql).Flush
end if

'https://github.com/thecarnie/aspjson/wiki/Public-Function-QueryToJSON(objDBConnection,strSQLQuery)
Private Function QueryToJSON(dbc, sql)
	Dim rs, jsa, col
	Set rs = dbc.Execute(sql)
	Set jsa = jsArray()
	While Not (rs.EOF Or rs.BOF)
		Set jsa(Null) = jsObject()
		For Each col In rs.Fields
			jsa(Null)(col.Name) = col.Value
		Next
		rs.MoveNext
	Wend
	Set QueryToJSON = jsa
End Function

Private Function ShowSQLInJSON(sql)
	ShowSQLInJSON = "{""sql"":""" & sql & """}"
End Function


%>