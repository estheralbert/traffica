<%Response.Buffer = true %>
<!--#include file ="common.asp"-->
<!--#include file ="openconnTest.asp"-->
<!--#include file="JSON_latest.asp"-->
<%

on error resume next

cmd						= Request("cmd")
isDebug					= Request("debug")
Response.ContentType	= "application/json"

if cmd="login" then
	username	= request("username")
	password	= request("password")
	
    sql	=	"exec spAppLogin " &_
				"@username	= " & tosql(username,"text")  & _
				",@password	= " & tosql(password,"text")

    PrintSQL(sql)

    QueryToJSON(objconn, sql).Flush
end if

if cmd="updateUserSite" then
	userid	= request("userid")
	sitecode	= request("sitecode")
	
    sql	=	"exec dbo.spUpdateUserSite " &_
				"@userid	= " & tosql(userid,"Number")  & _
				",@sitecode	= " & tosql(sitecode,"text")

    PrintSQL(sql)

    QueryToJSON(objconn, sql).Flush
end if

if cmd="getUserSites" then
	userid	= request("userid")
	
    sql	=	"exec dbo.spGetUserSites " & _
				"@userId = " & tosql(userid,"Number") 

    PrintSQL(sql)

    QueryToJSON(objconn, sql).Flush
end if

if cmd="checkCarPermits" then
	sitecode = request("sitecode")
	reg = request("reg")
	
    sql	=	"exec dbo.spCheckPermits " & _
				"@sitecode = " & tosql(sitecode,"text") & _
				",@reg = " & tosql(reg,"text") & _
				",@mydate = null"

    PrintSQL(sql)

    QueryToJSON(objconn, sql).Flush
end if

if cmd="getCarDetails" then
	reg	= request("reg")
	
    sql	=	"exec dbo.spGetCarDetailsRec " & _
				"@reg=" & tosql(reg,"text") 

    PrintSQL(sql)

    QueryToJSON(objconn, sql).Flush
end if

if cmd="getViolationReasons" then
	sitecode = request("sitecode")

    sql	=	"exec dbo.spGetViolationReasons " & _
				"@siteCode=" & tosql(sitecode,"text") 

    PrintSQL(sql)

    QueryToJSON(objconn, sql).Flush
end if

if cmd="getLatestUserTickets" then
	userid = request("userid")

    sql	=	"exec dbo.spGetLatestUserTickets " & _
				"@userId =" & tosql(userid,"Number") 

    PrintSQL(sql)

    QueryToJSON(objconn, sql).Flush
end if

if cmd="getTempTicketDetails" then
	ticketid = request("ticketid")

    sql	=	"exec dbo.spGetTempTicketDetails " & _
				"@tempTicketId =" & tosql(ticketid,"Number") 

    PrintSQL(sql)

    QueryToJSON(objconn, sql).Flush
end if


if cmd="printTempTicket" then
	ticketid = request("ticketid")

    sql	=	"declare @id bit; " &_
			"exec  dbo.spPrintTicket " &_ 
				 "@tempid			= " & tosql(ticketid,"Number") &_
				 ",@IsParkingApp	= 1 " &_
				 ",@id				= @id output;"

    PrintSQL(sql)

    QueryToJSON(objconn, sql).Flush
end if

if cmd="showTicketInfo" then
	violatorId = request("violatorId")

    sql	=	"exec  dbo.spShowTicketInfoNew " &_ 
				"@violatorId =" & tosql(violatorId,"Number")

    PrintSQL(sql)

    QueryToJSON(objconn, sql).Flush
end  if

if cmd="ShowUserTickets" then
	userid = request("userid")
	sitecode = request("sitecode")

    sql	=	"exec  dbo.spShowUserTickets " &_ 
				"@userid	=" & tosql(userid,"Number") &_
				",@sitecode	=" & tosql(sitecode,"Text")

    PrintSQL(sql)

    QueryToJSON(objconn, sql).Flush
end  if

if cmd="insertTempTicket" then
	reg = request("reg")
	sitecode = request("sitecode")
	ticketuserid = request("ticketuserid")
	reason = request("reason")
	filename1 = request("filename1")
	filename2 = request("filename2")
	filename3 = request("filename3")

    sql	=	"exec spInsertTempTicketNew " & _ 
				"@vehicle		= " & tosql(reg,"text") & _
				",@sitecode		= " & tosql(sitecode,"text") & _
				",@ticketuserid	= " & tosql(ticketuserid,"Number") & _
				",@reason		= " & tosql(reason,"text") & _
				",@filename1	= " & tosql(filename,"text") & _
				",@filename2	= " & tosql(filename2,"text") & _
				",@filename3	= " & tosql(filename3,"text")
			
    PrintSQL(sql)
	
	objconn.execute sql

	If Err.Number = 0 Then
		Response.Write("[{""result"":""success""}]")
	Else
		errMsg = Err.Number & " Srce: " & Err.Source & " Desc: " &  Err.Description
		Response.Write("[{""result"":""" & errMsg & """}]")
	End If
end if

If cmd="LogException" Then
	msg 	= request("msg")
	extype 	= request("type")
	source	= request("source")
	userId	= request("userId")

    sql = "EXEC dbo.mobileExceptionLoggingToDataBase " & _
                "@ExceptionMsg = " & tosql(msg,"text") & _
                ",@ExceptionType = " & tosql(extype,"text") & _
                ",@ExceptionSource = " & tosql(source,"text")& _
                ",@UserId = " & tosql(userId,"Number")

    PrintSQL(sql)
    QueryToJSON(objconn, sql).Flush
End If

If Err.Number <> 0 Then
    'error handling:
    Response.Echo Err.Number & " Srce: " & Err.Source & " Desc: " &  Err.Description
    Err.Clear
End If

'https://github.com/thecarnie/aspjson/wiki/Public-Function-QueryToJSON(objDBConnection,strSQLQuery)
Private Function QueryToJSON(dbc, sql)
	Dim rs, jsa, col
	Set rs = dbc.Execute(sql)
	Set jsa = jsArray()
	While Not (rs.EOF Or rs.BOF)
		Set jsa(Null) = jsObject()
		For Each col In rs.Fields
			jsa(Null)(col.Name) = col.Value
		Next
		rs.MoveNext
	Wend
	Set QueryToJSON = jsa
End Function

Private Function PrintSQL(sql)
	if isDebug then Response.Write(ShowSQLInJSON(sql))
End Function

Private Function ShowSQLInJSON(sql)
	ShowSQLInJSON = "{""sql"":""" & sql & """}"
End Function

%>