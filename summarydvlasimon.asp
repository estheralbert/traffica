<!--#include file="openconn.asp"-->

<!--#include file="common.asp"-->
<%
server.scripttimeout=1000
''2007

 

%>
<html>
<head>
<script src="sorttable.js"></script>
 <style>
/* Sortable tables */
body
{
    font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
}
table.sortable thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
}
table.sortable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}
.oddrowcolor{
	background-color:#d4e3e5;
}
.evenrowcolor{
	background-color:#c3dde0;
}


.red
{
 
 font-weight:bold;
 color:Red;   
}

table.sortable2 thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}

table.sortable2 {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
	table-layout:fixed;
}
table.sortable2 th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
	font-size:11px;
	font-weight:bold;
}
table.sortable2 td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
	width:110px;
}
</style>
  
</head>

<body>

   





<%
    on error goto 0
    response.write "<h1>ANPR</h1>"

    sql="exec SummaryDvla2simon"
  
   Set rs = objconn.Execute(sql)

     strLine="<table class=sortable id=mytable><thead><tr>" 'Initialize the variable for storing the filednames
   
     For each x in rs.fields
        cap=x.name
        cap=replace(cap,"Perc","%")
       select case cap
        case "Year"
            cap="Year"
     case "Month"
            cap="Month"

    end select

         if cap<>"Year" then     strLine= strLine & "<th>" & cap  & "</th>"
     Next
	 	strline=strline&"</tr></thead><tbody>"
       response.Write  strLine

        Do while Not rs.EOF
     strLine="<tr>"
     for each x in rs.Fields
	  	cell=x.value
    if instr(x.name,"Perc")>0 then     
     if not isnull(cell)  then cell=formatnumber(cell,0) & "%"
    end if
    if x.name<>"mymonth" and  x.name<>"myyear" then
       if x.name="amount" or x.name="pcnpayment" or x.name="pmtReceived"  then
            if not isnull(cell)  then cell=formatcurrency(cell,0)
    elseif x.name="averagePCN"  or x.name="averagePCNOffence"  or  x.name= "Avg Sent Amount" or  x.name= "Avg Sent Amount Pofa"  or instr(x.name,"avgAmount")>0 then
         if not isnull(cell)  then cell=formatcurrency(cell,2)
     elseif x.name="percentcancelled" then
         if not isnull(cell)  then cell=formatnumber(cell,2) & "%"
    else
       if not isnull(cell) and isnumeric(cell)  and x.name<>"dYear" then  cell=FormatNumber(cell,0)
    end if
    end if

    if x.name="Month" then 
    cell=monthname(rs("month")) & " " & rs("year") 
    end if
	if x.name<>"Year" then 	strLine= strLine & "<td>" & cell & "</td>"
     next
	 	strline=strline & "</tr>"
		'response.write strline & vbcrlf
    response.Write strLine
	
	 'err.clear
     rs.MoveNext
  Loop
	response.Write "</tbody></table>"
   
    rs.close
    set rs=nothing

   ' response.end

     response.write "<h1>Manual</h1>"

    sql="exec SummaryDvla2manual"
   Set rs = objconn.Execute(sql)

     strLine="<table class=sortable id=mytable><thead><tr>" 'Initialize the variable for storing the filednames
   
     For each x in rs.fields
        cap=x.name
        cap=replace(cap,"Perc","%")
       select case cap
        case "dYear"
            cap="Year"
     case "dMonth"
            cap="Month"

    end select

         if cap<>"Year" then     strLine= strLine & "<th>" & cap  & "</th>"
     Next
	 	strline=strline&"</tr></thead><tbody>"
       response.Write  strLine

        Do while Not rs.EOF
     strLine="<tr>"
     for each x in rs.Fields
	  	cell=x.value
    if instr(x.name,"Perc")>0 then     
     if not isnull(cell)  then cell=formatnumber(cell,0) & "%"
    end if
    if x.name<>"mymonth" and  x.name<>"myyear" then
       if x.name="amount" or x.name="pcnpayment" or x.name="pmtReceived"  then
            if not isnull(cell)  then cell=formatcurrency(cell,0)
    elseif  x.name="Avg Sent Amount" or x.name="averagePCN"  or x.name="averagePCNOffence" then
         if not isnull(cell)  then cell=formatcurrency(cell,2)
     elseif x.name="percentcancelled" then
         if not isnull(cell)  then cell=formatnumber(cell,2) & "%"
    else
       if not isnull(cell) and isnumeric(cell)  and x.name<>"dYear" then  cell=FormatNumber(cell,0)
    end if
    end if

    if x.name="dMonth" then 
    cell=monthname(rs("dmonth")) & " " & rs("dyear") 
    end if
	if x.name<>"dYear" then 	strLine= strLine & "<td>" & cell & "</td>"
     next
	 	strline=strline & "</tr>"
		'response.write strline & vbcrlf
    response.Write strLine
	
	 err.clear
     rs.MoveNext
  Loop
	response.Write "</tbody></table>"
   
    rs.close
    set rs=nothing

    
      function cs(num)
    if isnull(num) then
    cs="(&pound;0)"
    else
    cs="(&pound;" & num & ")"
    end if
    end function
     %>