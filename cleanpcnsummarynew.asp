<!--#include file=openconntriangle.asp-->
<!--#include file="common.asp"-->
<!--#include file="openconntestnew.asp"-->
<% 
    Server.ScriptTimeout = 360000000 
    Response.buffer=false
    Response.CacheControl = "no-cache"
    Response.Expires = -1 
    
    pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 

    deletedbo=0
    'loop through and dleete blackout days
    sql = "exec CleanViolatorsPart01"
    openrs rst,sql
    if not rst.eof then
        if rst("rowcount") > 0 then 
            'send simon an emails that blackout records were deleted
            ssubject    = "Deleted Violator records due to blackout dates"
            sbody       = "Violator records were deleted due to blackout dates"
            call sendemail("web@creativecarpark.co.uk", "simon.a@cleartoneholdings.co.uk", sSubject, sBody)
            call sendemail("web@creativecarpark.co.uk", "esther@creativecarpark.co.uk", sSubject, sBody)
            b extract
        end if
    end if
    closers rst

    sql = "exec CleanViolatorsPart02 @cmd='" & request("cmd2") & "'"
    openrs rst,sql
    do while not rst.eof 
        response.write rst("msg") & "<br />"
        rst.movenext
    loop

    if request("delviolator")<>"" then
        i=0 
        delviolators=request("delviolator")
        'response.write delviolators &"<hr>"
        arrdelviolators=split(delviolators,",")
        for m=lbound(arrdelviolators) to ubound(arrdelviolators)
            if request("delreason" & trim(arrdelviolators(m)))="" then
                response.write trim(arrdelviolators(m)) & " was not deleted because you failed to enter a reason deleted<br>"
            else
                sql= "exec delviolator @id=" & trim(arrdelviolators(m)) & ",@reasondeleted='" & session("username") & " deleted(deleted in stage print) " & request("delreason" & trim(arrdelviolators(m))) & "',@stage='print',@username=" & tosql(session("username"),"text")
                'response.write sql & "<br>"
                objconn.execute sql
                i = i + 1
            end if    
            'redirect to printpcn
        next
        response.write i & " records deleted<br><br>"
    end if

    if request("submit") = "Delete Checked tickets or Mark Hold Off" then
        sql = "update tempviolators set holdoff=0"
        objconn.execute sql
    end if

    sql = "CleanViolatorsPart04 @holdoff='" & request("holdoff") & "'"
    objconn.execute sql

    i=1
    'check and only allow to print those not waiting for transcription 
    sqlpn = "select distinct(DATEADD(dd, DATEDIFF(dd,0,datetime), 0)) as datetime from trianglepayment where vehiclereg is null and sitecode='3526'"
    set rs=objconnt.execute(sqlpn)
    do while not rs.eof 
        sql = "update tempviolators set holdoffwaiting=1,holdoffwaitingreason='Waiting Transcription' where site='087' and  DATEADD(dd, DATEDIFF(dd,0,[date of violation]), 0)='" & cisodate(rs("datetime")) & "'"
        'response.Write sql 
        objconn.execute sql  
        rs.movenext
    loop

    sqlpn = "select distinct(DATEADD(dd, DATEDIFF(dd,0,datetime), 0)) as datetime from trianglepayment where vehiclereg is null and sitecode='4380'" 
    set rs = objconnt.execute(sqlpn)
    do while not rs.eof 
        sql = "update tempviolators set holdoffwaiting=1,holdoffwaitingreason='Waiting Transcription' where site='266' and DATEADD(dd, DATEDIFF(dd,0,[date of violation]), 0)='" & cisodate(rs("datetime")) & "'"
        'response.Write sql 
        objconn.execute sql  
        rs.movenext
    loop

    sqlpn = "select distinct(DATEADD(dd, DATEDIFF(dd,0,datetime), 0)) as datetime from trianglepayment where vehiclereg is null and sitecode='4586'"
    set rs = objconnt.execute(sqlpn)
    do while not rs.eof 
        sql = "update tempviolators set holdoffwaiting=1,holdoffwaitingreason='Waiting Transcription' where site='271' and DATEADD(dd, DATEDIFF(dd,0,[date of violation]), 0)='" & cisodate(rs("datetime")) & "'"
        'response.Write sql 
        objconn.execute sql  
        rs.movenext
    loop

    sql = "exec cleanforprinting"
    objconn.execute sql

    sqlca = "select count(id) as mycount from qrytempviolatorsdvla where (owner='' or owner is null or len(owner) < 3) and dvlaid is not null"
    'response.write sqlca
    openrs rsca,sqlca

    response.write rsca("mycount") & " records have no owner <a href='admin.asp?cmd=printpcnsummary&cmd2=delpcnownerb' class='button'>Delete</a><br>"
    closers rsca 

    sqlca = "select count(id) as mycount from qrytempviolatorsdvla where ([dvla status]='disabled') and dvlaid is not null"
    'response.write sqlca
    openrs rsca,sqlca
    response.write rsca("mycount") & " records have a status of diabled <a href='admin.asp?cmd=printpcnsummary&cmd2=delpcndisabled' class='button'>Delete</a><br>"
    closers rsca 
   ' response.Write "done"
%>
 </body>
 </html>
