
<%
'**************************************************************************************************
' VSP Direct ASP Kit Includes File
'**************************************************************************************************

'**************************************************************************************************
' Change history
' ==============
'
' 13/09/2007 - Mat Peck - New kit version
'**************************************************************************************************
' Description
' ===========
'
' Page with no visible content, but defines the constants and functions used in other pages in the
' kit.  It also opens connections to the database and defines record sets for later use.  It is
' included at the top of every other page in the kit and is paried with the closedown scipt.
'**************************************************************************************************

response.buffer=true
Response.Expires = -1
Response.AddHeader "PRAGMA","NO-CACHE"
Response.CacheControl = "no-cache"

'**************************************************************************************************
' Values for you to update
'**************************************************************************************************
strConnectTo="TEST" 	'** Set to SIMULATOR for the VSP Simulator expert system, TEST for the Test Server **
							'** and LIVE in the live environment **

strDatabaseUser="protxUser" '** Change this if you created a different user name to access the database **
strDatabasePassword="[your database user password]" '** Set the password for the above user here **

strVirtualDir="VSPDirect-kit" '** Change if you've created a Virtual Directory in IIS with a different name **

	'** IMPORTANT.  Set the strYourSiteFQDN value to the Fully Qualified Domain Name of your server. **
	'** This should start http:// or https:// and should be the name by which our servers can call back to yours **
	'** i.e. it MUST be resolvable externally, and have access granted to the Protx servers **
	'** examples would be https://www.mysite.com or http://212.111.32.22/ **
	'** NOTE: You should leave the final / in place. **
strYourSiteFQDN="http://[your web site]/"  

strVSPVendorName="civil" '"civil" '** Set this value to the VSPVendorName assigned to you by protx or chosen when you applied **
strCurrency="GBP" '** Set this to indicate the currency in which you wish to trade. You will need a merchant number in this currency **
strTransactionType="PAYMENT" '** This can be DEFERRED or AUTHENTICATE if your Protx account supports those payment types **

'**************************************************************************************************
' Global Definitions for this site
'**************************************************************************************************
strProtxDSN = "DRIVER={MySQL ODBC 3.51 Driver}; SERVER=localhost; DATABASE=protx; " & _
	"UID=" & strDatabaseUser & ";PASSWORD=" & strDatabasePassword & "; OPTION=3" 

strProtocol="2.22"

if strConnectTo="LIVE" then
  strAbortURL="https://ukvps.protx.com/vspgateway/service/abort.vsp"
  strAuthoriseURL="https://ukvps.protx.com/vspgateway/service/authorise.vsp"
  strCancelURL="https://ukvps.protx.com/vspgateway/service/cancel.vsp"
  strPurchaseURL="https://ukvps.protx.com/vspgateway/service/vspdirect-register.vsp"
  strRefundURL="https://ukvps.protx.com/vspgateway/service/refund.vsp"
  strReleaseURL="https://ukvps.protx.com/vspgateway/service/release.vsp"
  strRepeatURL="https://ukvps.protx.com/vspgateway/service/repeat.vsp"
  strVoidURL="https://ukvps.protx.com/vspgateway/service/void.vsp"
  strVSPDirect3DCallbackPage="https://ukvps.protx.com/vspgateway/service/direct3dcallback.vsp"
elseif strConnectTo="TEST" then
  strAbortURL="https://ukvpstest.protx.com/vspgateway/service/abort.vsp"
  strAuthoriseURL="https://ukvpstest.protx.com/vspgateway/service/authorise.vsp"
  strCancelURL="https://ukvpstest.protx.com/vspgateway/service/cancel.vsp"
  strPurchaseURL="https://ukvpstest.protx.com/vspgateway/service/vspdirect-register.vsp"
  strRefundURL="https://ukvpstest.protx.com/vspgateway/service/refund.vsp"
  strReleaseURL="https://ukvpstest.protx.com/vspgateway/service/abort.vsp"
  strRepeatURL="https://ukvpstest.protx.com/vspgateway/service/repeat.vsp"
  strVoidURL="https://ukvpstest.protx.com/vspgateway/service/void.vsp"
  strVSPDirect3DCallbackPage="https://ukvpstest.protx.com/vspgateway/service/direct3dcallback.vsp"
else
  strAbortURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorAbortTx"
  strAuthoriseURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorAuthoriseTx"
  strCancelURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorCancelTx"
  strPurchaseURL="https://ukvpstest.protx.com/VSPSimulator/VSPDirectGateway.asp"
  strRefundURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorRefundTx"
  strReleaseURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorReleaseTx"
  strRepeatURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorRepeatTx"
  strVoidURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorVoidTx"
  strVSPDirect3DCallbackPage="https://ukvpstest.protx.com/VSPSimulator/VSPDirectCallback.asp"
end if

'**************************************************************************************************
' Useful functions for all pages in this kit
'**************************************************************************************************

'** Filters unwanted characters out of an input string.  Useful for tidying up FORM field inputs
 function cleanInput(strRawText,strType)

	if strType="Number" then
		strClean="0123456789."
		bolHighOrder=false
	elseif strType="VendorTxCode" then
		strClean="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_."
		bolHighOrder=false
	else
  		strClean=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,'/{}@():?-_&�$=%~<>*+""" & vbCRLF
		bolHighOrder=true
	end if

	strCleanedText=""
	iCharPos = 1

	do while iCharPos<=len(strRawText)
    	'** Only include valid characters **
		chrThisChar=mid(strRawText,iCharPos,1)

		if instr(StrClean,chrThisChar)<>0 then 
			strCleanedText=strCleanedText & chrThisChar
		elseif bolHighOrder then
			'** Fix to allow accented characters and most high order bit chars which are harmless **
			if asc(chrThisChar)>=191 then strCleanedText=strCleanedText & chrThisChar
		end if

      	iCharPos=iCharPos+1
	loop       
  
	cleanInput = trim(strCleanedText)

end function

'** Doubles up single quotes to stop breakouts from SQL strings **
 function SQLSafe(strRawText)

	strCleanedText=""
	iCharPos = 1

	do while iCharPos<=len(strRawText)
    	'** Double up single quotes, but only if they aren't already doubled **
		if mid(strRawText,iCharPos,1)="'" then 
			strCleanedText=strCleanedText & "''"
			if iCharPos<>len(strRawText) then
				if mid(strRawText,iCharPos+1,1)="'" then iCharPos=iCharPos+1
			end if
		else
			strCleanedText=strCleanedText & mid(strRawText,iCharPos,1)
		end if
        
		iCharPos=iCharPos+1
	loop       
  
	SQLSafe = trim(strCleanedText)

end function

'** Counts the number of : in a string.  Used to validate the basket fields
 function countColons(strSource)

	if strSource="" then
		iNumCol=0
	else
		iCharPos=1
		iNumCol=0
		do while iCharPos<>0
			iCharPos=instr(iCharPos+1,strSource,":")
			if iCharPos<>0 then iNumCol=iNumCol+1
		loop 
	
	end if
	
	countColons = iNumCol

end function

'** Checks to ensure a Basket Field is correctly formatted **
 function validateBasket(strThisBasket)

	bolValid=false
	if len(strThisBasket)>0 and (instr(strThisBasket,":")<>0) then
		iRows=left(strThisBasket,instr(strThisBasket,":")-1)
		if isnumeric(iRows) then
			iRows=Cint(iRows)
			if countColons(strThisBasket)=((iRows*5)+iRows) then bolValid=true
		end if
	end if	
	validateBasket = bolValid

end function


'** ASP has no inbuild URLDecode function, so here's on in case we need it **
 function URLDecode(strString)
	For lngPos = 1 To Len(strString)
    	If Mid(strString, lngPos, 1) = "%" Then
            strUrlDecode = strUrlDecode & Chr("&H" & Mid(strString, lngPos + 1, 2))
            lngPos = lngPos + 2
        elseif Mid(strString, lngPos, 1) = "+" Then
            strUrlDecode = strUrlDecode & " "
        Else
            strUrlDecode = strUrlDecode & Mid(strString, lngPos, 1)
        End If
    Next
    UrlDecode = strUrlDecode
End Function

'** There is a URLEncode function, but wrap it up so keep the code clean **
 function URLEncode(strString)
	strEncoded=Server.URLEncode(strString)
	URLEncode=strEncoded
end function

'** MySQL can't handle ASP format dates.  It needs values separated by - signed, so create a MySQL valid date from that passed **
function mySQLDate(dateASP)
	mySQLDate=DatePart("yyyy",dateASP) & "-" & right("00" & DatePart("m",dateASP),2) & "-" &_
		right("00" & DatePart("d",dateASP),2) & " " & right("00" & DatePart("h",dateASP),2) & ":" &_
		right("00" & DatePart("n",dateASP),2) & ":" & right("00" & DatePart("s",dateASP),2)
end function

'** Used to split out the fields returned from the VSP systems in the response part of the POSTs **
function findField( strFieldName, strThisResponse )
	arrItems = split( strThisResponse, vbCRLF )
	for iItem = LBound( arrItems ) to UBound( arrItems )
		if InStr(arrItems(iItem), strFieldName & "=" ) = 1 then
			findField = mid(arrItems(iItem),len(strFieldName)+2)
			exit For
		end if
	next 
end function

'************ Global variables and Database Connections ***************

dim dbProtx
dim rsPrimary
dim rsSecondary
dim strSQL

'Open the VPS database
if strDatabasePassword<>"[your database user password]" then
	set dbProtx=server.createobject("adodb.connection") 
	dbProtx.CommandTimeOut=180
	dbProtx.Open strProtxDSN
end if
%>