﻿

<%
   If Request.ServerVariables("SERVER_PORT")=80 Then
      Dim strSecureURL
      strSecureURL = "https://"
      strSecureURL = strSecureURL & Request.ServerVariables("SERVER_NAME")
      strSecureURL = strSecureURL & Request.ServerVariables("URL")
      Response.Redirect strSecureURL
   End If
%>
	

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Civil Enforcement  - Pay Now</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script language"javascript">

    function trim(stringToTrim) {
        return stringToTrim.replace(/^\s+|\s+$/g, "");
    }



    function Validate() {

        var mypcn, myplate;
        mypcn = document.getElementById('mypcn');
        myplate = document.getElementById('myplate');
       



        if (trim(myplate.value) == "") {
            alert("Please Enter Plate");
            myplate.focus();
            return false;
        }
        

        if (trim(mypcn.value) == "") {
            alert("Please Enter PCN");
            mypcn.focus();
            return false;
        }

        var mypcnv=trim(mypcn.value);
        var pcnlen=mypcnv.length
        
        if (pcnlen<10 ) {
            alert("PCN must be 10 digits");
            mypcn.focus();
            return false;
        }
        
        
         if (pcnlen>10 ) {
            alert("PCN must be 10 digits");
            mypcn.focus();
            return false;
        }

            return true;
        }







</script>
</head>
<body>
<div id="page-wrapper">
	<div id="page">
		<div id="header">
			<a class="logo" href="./index.html"></a>
			<div class="main-menu fl">
				<a href="./how-to-pay.html">How to pay</a> 
				<img alt="slash" src="./images/sep-menu.png" /> 
				<a class="active" href="./pay-now.asp">pay now</a> 
			</div>
			<div class="main-menu fr">
				<a href="./faq.html">FAQ</a> 
				<img alt="slash" src="./images/sep-menu.png" /> 
				<a href="./contact-us.html">CONTACT US</a> 
			</div>
		</div>
		<div id="content">
			<h1 class="title">Pay Now</h1>
			<div class="content-block">
			<% showform=1
			'if request("cmd")="addccpayment" then 
			'    showform=0
			 '   call addccpaymentform
			'end if
			
			if request.form("pcn")<>"" and request.form("registration")<>"" then
			
			    sql="select * from violators where pcn+site=" & tosql(request.Form("pcn"),"text") & " and registration=" &tosql(request.Form("registration"),"text")
			    openrs rs,sql
			        if rs.eof and rs.bof then
			                response.Write "Invalid PCN or Registration"
			       else
			        
			            call addccpaymentform
			       
			            showform=0
			        end if
			    closers rs
			
			end if
			
			 %>
			
			<% if showform=1 then %>
			
				<div class="form-box reg-number">
					<form action="pay-now.asp" method="post"  id=payform>
						<p><label>Enter Vehicle Registration Number <br />
						<input type="text" class="form-text" name="registration" id="myplate" value="" />
						<p><label>PCN <br />
						<input type="text" class="form-text" name="pcn" id="mypcn" value="" />
<p>
<input class="form-submit" type="submit" value="Submit" onclick="return Validate();" /></p>
						</label></p>
					</form>
				</div>
				
				<% end if %>
							</div>
		</div>
		<div id="footer">
			<div class="content-block">
				<div class="sponsor fl">
					<a href="#"><img alt="" src="./images/bra-circle.png" /></a>  
					<a href="#"><img alt="" src="./images/bra.png" /></a>
				</div>
				<div class="credit-card fr">
					<a href="#"><img alt="" src="./images/cards.png" /></a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="copy">Civil Enforcement Ltd. Company Registration Number 05645677. Company Registered in England. Registered Office Horton House, Exchange Flags, Liverpool L2 3PF</div>
		</div>
	</div>
</div>
</body>
</html>


<%

sub addccpaymentform
    showpayform=true
                if request("cmd2")="process" then
                
                %>
                <!--include file="includes.asp" -->
                <%    
                strConnectTo="LIVE" 
                strVSPVendorName="civilenf" '"civil" '** Set this value to the VSPVendorName assigned to you by protx or chosen when you applied **
strCurrency="GBP" '** Set this to indicate the currency in which you wish to trade. You will need a merchant number in this currency **
strTransactionType="PAYMENT" '** This can be DEFERRED or AUTHENTICATE if your Protx account supports those payment types **

strProtocol="2.22"
if strConnectTo="LIVE" then
  strAbortURL="https://ukvps.protx.com/vspgateway/service/abort.vsp"
  strAuthoriseURL="https://ukvps.protx.com/vspgateway/service/authorise.vsp"
  strCancelURL="https://ukvps.protx.com/vspgateway/service/cancel.vsp"
   strPurchaseURL="https://live.sagepay.com/gateway/service/vspdirect-register.vsp"
  strRefundURL="https://ukvps.protx.com/vspgateway/service/refund.vsp"
  strReleaseURL="https://ukvps.protx.com/vspgateway/service/release.vsp"
  strRepeatURL="https://ukvps.protx.com/vspgateway/service/repeat.vsp"
  strVoidURL="https://ukvps.protx.com/vspgateway/service/void.vsp"
  strVSPDirect3DCallbackPage="https://ukvps.protx.com/vspgateway/service/direct3dcallback.vsp"
elseif strConnectTo="TEST" then
  strAbortURL="https://ukvpstest.protx.com/vspgateway/service/abort.vsp"
  strAuthoriseURL="https://ukvpstest.protx.com/vspgateway/service/authorise.vsp"
  strCancelURL="https://ukvpstest.protx.com/vspgateway/service/cancel.vsp"
  strPurchaseURL="https://ukvpstest.protx.com/vspgateway/service/vspdirect-register.vsp"
  strRefundURL="https://ukvpstest.protx.com/vspgateway/service/refund.vsp"
  strReleaseURL="https://ukvpstest.protx.com/vspgateway/service/abort.vsp"
  strRepeatURL="https://ukvpstest.protx.com/vspgateway/service/repeat.vsp"
  strVoidURL="https://ukvpstest.protx.com/vspgateway/service/void.vsp"
  strVSPDirect3DCallbackPage="https://ukvpstest.protx.com/vspgateway/service/direct3dcallback.vsp"
else
  strAbortURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorAbortTx"
  strAuthoriseURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorAuthoriseTx"
  strCancelURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorCancelTx"
  strPurchaseURL="https://live.sagepay.com/gateway/service/vspdirect-register.vsp"
  strRefundURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorRefundTx"
  strReleaseURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorReleaseTx"
  strRepeatURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorRepeatTx"
  strVoidURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorVoidTx"
  strVSPDirect3DCallbackPage="https://ukvpstest.protx.com/VSPSimulator/VSPDirectCallback.asp"
end if


                
                 strCardHolder=left(request.form("CardHolder"),100)
		                strCardType=cleaninput(request.form("CardType"),"Text")	
		                strCardNumber=cleaninput(request.form("CardNumber"),"Number")
		           '     response.Write "<br>cardnumber:" & strcardnumber
		              '   response.Write "<br>start  date:" &request.form("StartDate")
		                strStartDate=cleaninput(request.form("StartDate"),"Number")
		               ' response.Write "<br>start  date:" & strstartdate
		                strExpiryDate=cleaninput(request.form("ExpiryDate"),"Number")
		                strIssueNumber=cleaninput(request.form("IssueNumber"),"Number")
		                strCV2=cleaninput(request.form("CV2"),"Number")
                		'response.Write "cv2:"  & strcv2
		                '** Right then... check em **
		                if strCardHolder="" then
			                strPageError="You must enter the name of the Card Holder."
		                elseif strCardType="" then
			                strPageError="You must select the type of card being used."
		                elseif strCardNumber="" or not(IsNumeric(strCardNumber)) then
			                strPageError="You must enter the full card number."
		               '' elseif strStartDate<>"" and (len(strStartDate)<>4 or not(IsNumeric(strStartDate))) then
			               '' strPageError="If you provide a Start Date, it should be in MMYY format, e.g. 1206 for December 2006."
		                elseif strExpiryDate="" or len(strExpiryDate)<>4 or not(IsNumeric(strExpiryDate)) then
			                strPageError="You must provide an Expiry Date in MMYY format, e.g. 1209 for December 2009."
		                elseif (strIssueNumber<>"" and not(IsNumeric(strIssueNumber))) then
			                strPageError="If you provide an Issue number, it should be numeric."
		                elseif strCV2="" then 'or not(IsNumeric(strCV2)) then
			                strPageError="You must provide a Card Verification Value.  This is the last 3 digits on the signature strip of your card (or for American Express cards, the 4 digits printed to the right of the main card number on the front of the card.)"
		                else
			                '** All required field are present, so first store the order in the database then format the POST to VSP Direct **
			                '** First we need to generate a unique VendorTxCode for this transaction **
			                '** We're using VendorName, time stamp and a random element.  You can use different mehtods if you wish **
			                '** but the VendorTxCode MUST be unique for each transac   tion you send to VSP Server **
			                Randomize
			               ' strVendorTxCode=cleaninput(strVSPVendorName &	"-" & right(DatePart("yyyy",Now()),2) &_
							               ' right("00" & DatePart("m",Now()),2) & right("00" & DatePart("d",Now()),2) &_
							                'right("00" & DatePart("h",Now()),2) & right("00" & DatePart("n",Now()),2) &_
							                'right("00" & DatePart("s",Now()),2) & "-" & cstr(round(rnd*100000)),"VendorTxCode")
                                mypcn=request("pcn")
			                 strVendorTxCode=cleaninput(strVSPVendorName &	"_PCNwebsite" & mypcn ,"VendorTxCode") & "_" & cstr(month(date()) & year(date()))
			                
			                sngTotal=request("amount")
			               
			               
			                '** Now create the VSP Direct POST **
                				
			                '** Now to build the VSP Server POST.  For more details see the VSP Server Protocol 2.22 **
			                '** NB: Fields potentially containing non ASCII characters are URLEncoded when included in the POST **
			                strPost="VPSProtocol=" & strProtocol
			                strPost=strPost & "&TxType=" & strTransactionType '** PAYMENT by default.  You can change this in the includes file **
			                strPost=strPost & "&Vendor=" & strVSPVendorName
			                strPost=strPost & "&VendorTxCode=" & strVendorTxCode '** As generated above **
			                strPost=strPost & "&Amount=" & FormatNumber(sngTotal,2,-1,0,0) '** Formatted to 2 decimal places with leading digit but no commas or currency symbols **
			                strPost=strPost & "&Currency=" & strCurrency
			                '** Up to 100 chars of free format description **
			                strPost=strPost & "&Description=" & URLEncode("Payment " & strVSPVendorName)
			                '** The Notification URL is the page to which VSP Server calls back when a transaction completes **
			                '** You can change this for each transaction, perhaps passing a session ID or state flag if you wish **
			                strPost=strPost & "&CardHolder=" & strCardHolder
			                strPost=strPost & "&CardNumber=" & strCardNumber
			                if len(strStartDate)>0 then
				                strPost=strPost & "&StartDate=" & strStartDate
			                end if
			                strPost=strPost & "&ExpiryDate=" & strExpiryDate
			                if len(strIssueNumber)>0 then
				                strPost=strPost & "&IssueNumber=" & strIssueNumber
			                end if
			                strPost=strPost & "&CV2=" & strCV2
			                strPost=strPost & "&CardType=" & strCardType
			                strPost=strPost & "&BillingAddress=" & URLEncode(strBillingAddress)
			                strPost=strPost & "&BillingPostCode=" & URLEncode(strBillingPostCode)
			                if bolDeliverySame then
				                strPost=strPost & "&DeliveryAddress=" & URLEncode(strBillingAddress)
				                strPost=strPost & "&DeliveryPostCode=" & URLEncode(strBillingPostCode)
			                else
				                strPost=strPost & "&DeliveryAddress=" & URLEncode(strDeliveryAddress)
				                strPost=strPost & "&DeliveryPostCode=" & URLEncode(strDeliveryPostCode)
			                end if
			                strPost=strPost & "&CustomerName=" & server.URLEncode(strCustomerName)
                     '** Allow fine control over AVS/CV2 checks and rules by changing this value. 0 is Default **
			                '** It can be changed dynamically, per transaction, if you wish.  See the VSP Direct Protocol document **
			                if strTransactionType<>"AUTHENTICATE" then strPost=strPost & "&ApplyAVSCV2=0"
                			
			                '** Send the IP address of the person entering the card details **
			                strPost=strPost & "&ClientIPAddress=" & Request.ServerVariables("REMOTE_HOST")

			                '** Allow fine control over 3D-Secure checks and rules by changing this value. 0 is Default **
			                '** It can be changed dynamically, per transaction, if you wish.  See the VSP Server Protocol document **
			                strPost=strPost & "&Apply3DSecure=0"
                			
			                '** Send the account type to be used for this transaction.  Web sites should us E for e-commerce **
			                '** If you are developing back-office applications for Mail Order/Telephone order, use M **
			                '** If your back office application is a subscription system with recurring transactions, use C **
			                '** Your Protx account MUST be set up for the account type you choose.  If in doubt, use E **
			                strPost=strPost & "&AccountType=E"

			                '** The full transaction registration POST has now been built **
			                '** Use the Windows WinHTTP object to POST the data directly from this server to Protx **
			                '** Data is posted to strPurchaseURL which is set depending on whether you are using SIMULATOR, TEST or LIVE **
			           if session("bdebug")=true then response.Write strpost
			                set httpRequest = CreateObject("WinHttp.WinHttprequest.5.1")
                		'response.Write strpurchaseurl
			                on error resume next
			                httpRequest.Open "POST", CStr(strPurchaseURL), false
			                httpRequest.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
			                httpRequest.send strPost
			                strResponse = httpRequest.responseText
                			if session("bdebug")=true then response.Write"debug code" & strresponse & "<hr>"
			                if Err.number <> 0 then    
				                '** An non zero Err.number indicates an error of some kind **
				                '** Check for the most common error... unable to reach the purchase URL **  
				                if Err.number = -2147012889 then
					                strPageError="Your server was unable to register this transaction with Protx." &_
								                "  Check that you do not have a firewall restricting the POST and " &_
								                "that your server can correctly resolve the address " & strPurchaseURL
				                else
					                strPageError="An Error has occurred whilst trying to register this transaction.<BR>Please try again soon"'
					                 '&_ 
					                 
					             '    response.Write               "The Error Number is: " & Err.number & "<BR>" &_
								             '  "The Description given is: " & Err.Description
				                end If 
                			  
			                else
				                '** No transport level errors, so the message got the Protx **
				                '** Analyse the response from VSP Direct to check that everything is okay **
				                '** Registration results come back in the Status and StatusDetail fields **
				                strStatus=findField("Status",strResponse)
				                strStatusDetail=findField("StatusDetail",strResponse)
                		       ' response.Write  "<hr>:response:" & strresponse
                		        
                		        'response.write "<br>Status:" & strstatus
				                if strStatus="3DAUTH" then
					                '** This is a 3D-Secure transaction, so we need to redirect the customer to their bank **
					                '** for authentication.  First get the pertinent information from the response **
					                strMD=findField("MD",strResponse)
					                strACSURL=findField("ACSURL",strResponse)
					                strPAReq=findField("PAReq",strResponse)
					                strPageState="3DRedirect"

				                else

					                '** If this isn't 3D-Auth, then this is an authorisation result (either successful or otherwise) **
					                '** Get the results form the POST if they are there **
					                strVPSTxId=findField("VPSTxId",strResponse)
					                strSecurityKey=findField("SecurityKey",strResponse)
					                strTxAuthNo=findField("TxAuthNo",strResponse)
					                strAVSCV2=findField("AVSCV2",strResponse)
					                strAddressResult=findField("AddressResult",strResponse)
					                strPostCodeResult=findField("PostCodeResult",strResponse)
					                strCV2Result=findField("CV2Result",strResponse)
					                str3DSecureStatus=findField("3DSecureStatus",strResponse)
					                strCAVV=findField("CAVV",strResponse)
                					
					                '** Great, the signatures DO match, so we can update the database and redirect the user appropriately **
					                if strStatus="OK" then
						                strDBStatus="AUTHORISED - The transaction was successfully authorised with the bank."
					                elseif strStatus="MALFORMED" then
						                strDBStatus="MALFORMED - The StatusDetail was:" & SQLSafe(left(strStatusDetail,255))
					                elseif strStatus="INVALID" then
						                strDBStatus="INVALID - The StatusDetail was:" & SQLSafe(left(strStatusDetail,255))
					                elseif strStatus="NOTAUTHED" then
						                strDBStatus="DECLINED - The transaction was not authorised by the bank."
					                elseif strStatus="REJECTED" then
						                strDBStatus="REJECTED - The transaction was failed by your 3D-Secure or AVS/CV2 rule-bases."
					                elseif strStatus="AUTHENTICATED" then
						                strDBStatus="AUTHENTICATED - The transaction was successfully 3D-Secure Authenticated and can now be Authorised."
					                elseif strStatus="REGISTERED" then
						                strDBStatus="REGISTERED - The transaction was could not be 3D-Secure Authenticated, but has been registered to be Authorised."
					                elseif strStatus="ERROR" then
						                strDBStatus="ERROR - There was an error during the payment process.  The error details are: " & SQLSafe(strStatusDetail)
					                else
						                strDBStatus="UNKNOWN - An unknown status was returned from Protx.  The Status was: " & SQLSafe(strStatus) &_
									                 ", with StatusDetail:" & SQLSafe(strStatusDetail)
					                end if		
					                
					                	
                           mypcn=request("pcn")
                				
                				showpayform=false

					                '** Work out where to send the customer **
					                Session("VendorTxCode")=strVendorTxCode
					                if strStatus="OK" or strStatus="AUTHENTICATED" or strStatus="REGISTERED" then
						                'strCompletionURL="orderSuccessful.asp"
						              '  response.Write strstatus & " we need to add payment record here"	
						     
						  

sql="insert into payments(test,pcn,plate,amount,method,received,ip,cccode) values('manualpayments'," & tosql(mypcn,"text") & "," & tosql(request("registration"),"text") & "," & tosql(request("amount"),"Number") & "," & "'Website Credit Card',getdate(),"  & tosql( Request.ServerVariables("REMOTE_ADDR"),"text") & "," & tosql(strvendortxcode,"text") & ")"
'response.Write sql
objconn.execute sql
lsSQL = "SELECT @@IDENTITY AS NewID"
Set loRs = objConn.Execute(lsSQL)


ID = loRs.Fields("NewID").value 
sqldr="select * from debtrecovery where pcn=" & tosql(mypcn,"text")
		  '  response.Write sql
		    openrs rsdr,sqldr
		        if not rsdr.eof and not rsdr.eof then
		        'check if amount is full
		        sqla="select reducedamount from violators where pcn+site=" & tosql(mypcn,"text")
		        openrs rsa,sqla
		        reducedamount=rsa("reducedamount")
		        if amount>=reducedamount then
		              sql="deletedebtrecovery @pcn=" & tosql(mypcn,"text")
                    objconn.execute sql
                   '  call sendnewlyn(spcn)
		        
		        end if
		        
		        closers rsa
		        
		        end if
		    closers rsdr 
		    
'sql="insert into manualpayments('amount,protxcode,paymentid,pcn,errorcode) values(" & tosql(request("amount"),"Number") & "," & tosql(strVendorTxCode,"text") & "," & id & ",'" & mypcn & "','" & strdbstatus & "')"
'response.Write sql
'objconn.execute sql

response.Write "Payment received Successfully<Br><Br><br><br><br><br>"'	&strstatus

			 		              
						              
					                else
					           '  response.Write strstatus & "<hr>"   
					               ' sql="insert into manualpayments(amount,protxcode,errorcode,pcn) values(" & tosql(request("amount"),"Number") & "," & tosql(strVendorTxCode,"text") & ",'" & strStatus &  "','" & mypcn & "')"
					             '   response.Write sql
					             
					             declinedstatus="Website Payment:" & strstatus & ":" & strstatusdetail
					         '    response.write "<br>Status:" & strstatus
					       '         response.write "<br>Dec Status:" & declinedstatus	
					             sql=" insert into declinedpayments(mydate,pcn,amount,reason,ip)  values(getdate()," &tosql(mypcn,"text") & "," & tosql(request("amount"),"Number") & "," &   tosql(declinedstatus,"text") & "," & tosql( Request.ServerVariables("REMOTE_ADDR"),"text") & ")"
					      ' response.Write sql
					             objconn.execute sql
					             


						               ' strCompletionURL="orderFailed.asp"
						               ' strPageError=strDBStatus
						               if left(strStatus,8)="DECLINED" then
		                                    strReason="We regret that your payment was declined by your bank.  This could be due to insufficient funds, or incorrect card details."
	                                    elseif left(strStatus,9)="MALFORMED" or left(strStatus,7)="INVALID" then
		                                    strReason="The bank rejected some of the information your entered. Please re-enter your details again and ensure that all digits are accurate."
	                                    elseif left(strStatus,8)="REJECTED" then
		                                    strReason="We regret that your payment was declined by your bank.  This could be due to insufficient funds, or incorrect card details."
	                                    elseif left(strStatus,5)="ERROR" then
		                                    strReason="The bank rejected some of the information your entered. Please re-enter your details again and ensure that all digits are accurate."
	                                    else
		                                    strReason="The transaction process failed.  We please contact us with the date and time of your order and we will investigate.The bank rejected some of the information your entered. Please re-enter your details again and ensure that all digits are accurate."
	                                    end if
	                                    response.Write "<div class=red>" & strreason & "</div>"
                                    showpayform=true
					                end if
          
                					
				                end if
                				
			                end if
                		
			                on error goto 0
			                set httpRequest=nothing
                		
                end if                
                                
                
                end if
               response.Write "<div class=red>" & strpageerror & "</div>"
                if showpayform then
                mypcn=request("pcn")
                if mypcn="" then
                    response.Write "You must supply a PCN"
                    exit sub
                 end if
                'myerror=""
              
              
           
            
            %>
            
            
            <div class=fl2>
            <table>
            <tr><td><strong>PCN:</strong></td><td><%=mypcn %></td></tr>
            <tr><td><strong>VRM:</strong></td><td><%=ucase(request("registration")) %></td></tr>
            <%
            sqlv="select d.make,d.model,d.Colour from VIOLATORS v left join DVLA d on d.ID=v.dvlaid where v.PCN+v.Site=" & tosql(mypcn,"text")
            openrs rsv,sqlv
            make=rsv("make")
            model=rsv("model")
            color=rsv("colour")
        
            
             %>
             <tr><td><strong>Colour:</strong></td><td><%=color %></td></tr>
             <tr><td><strong>Make:</strong></td><td><%=make %></td></tr>
             <tr><td><strong>Model:</strong></td><td><%=model %></td></tr>
            </table>
            
            
            
            </div>
            <div id=formpayment>
            <form method=post action=pay-now.asp?cmd=addccpayment>
 
            <input type=hidden name=cmd2 value=process />
            <input type=hidden name=pcn value="<%=mypcn %>" />
             <input type=hidden name=registration value="<%=request("registration") %>" />
            <table border=0>
            <tr><td><b>PCN</b></td><td><%=mypcn %></td></tr>
                <tr><td><b>Amount Outstanding</b></td><td>&pound;<%=getamountowed(mypcn) %></td></tr>
         <input type=hidden name=amount  value="<%=getamountowed(mypcn) %>" />
          
				
				<tr> 
					<td  align="right" class="greybar"><strong>Card Holder Name:</strong></td>
				  	<td  width="70%" class="TDSmall"><input name="CardHolder" type="text" value="<%response.write strCardHolder%>" size="30" maxlength="100"></td>
				</tr>
				<tr> 
					<td  align="right" class="greybar"><strong>Card Type:</strong></td>
				  	<td align="left"  class="TDSmall"><SELECT NAME="CardType">
				  		<option value="VISA"<%if strCardType="VISA" then response.write " SELECTED"%>>VISA Credit</option>
				  		<option value="DELTA"<%if strCardType="DELTA" then response.write " SELECTED"%>>VISA Debit</option>
				  		<option value="UKE"<%if strCardType="UKE" then response.write " SELECTED"%>>VISA Electron</option>
				  		<option value="MC"<%if strCardType="MC" then response.write " SELECTED"%>>MasterCard</option>
				  		<option value="MAESTRO"<%if strCardType="MAESTRO" then response.write " SELECTED"%>>Maestro</option>
				  		<option value="AMEX"<%if strCardType="AMEX" then response.write " SELECTED"%>>American Express</option>
				  		<option value="DC"<%if strCardType="DC" then response.write " SELECTED"%>>Diner's Club</option>
				  		<option value="JCB"<%if strCardType="JCB" then response.write " SELECTED"%>>JCB Card</option>
					</SELECT>
			  	    
					</td>
				</tr>
				<tr> 
					<td  align="right" class="greybar"><strong>Card Number:</strong></td>
			  	  <td align="left"  class="TDSmall"><input name="CardNumber" type="text" value="" size="25" maxlength="24">
				  	  <br /><font size="1">(With no spaces or separators)</font></td>
				</tr>
				<tr> 
					<td  align="right" class="greybar"><strong>Start Date:</strong></td>
			  	  <td align="left" width="70%" class="TDSmall"><input name="StartDate" type="text" value="<%response.write strStartDate%>" size="5" maxlength="4">
				    <br /><font size="1">(Where available. Use MMYY format  e.g. 0207)</font></td>
				</tr>
				<tr> 
					<td width="30%" align="right" class="greybar"><strong>Expiry Date:</strong></td>
			  	  <td align="left" class="TDSmall"><input name="ExpiryDate" type="text" value="<%response.write strExpiryDate%>" size="5" maxlength="4">
				    <br /><font size="1">(Use MMYY format with no / or - separators e.g. 1109)</font></td>
				</tr>
				<tr> 
					<td align="right" class="greybar"><strong>Issue Number:</strong></td>
			  	  <td align="left" class="TDSmall"><input name="IssueNumber" type="text" value="<%response.write strIssueNumber%>" size="3" maxlength="2">
				    <br /><font size="1">(Older Switch cards only. 1 or 2 digits 
					  as printed on the card)</font></td>
				</tr>
				<tr> 
					<td  align="right" class="greybar"><strong>Card Verification Value:</strong></td>
			  	  <td align="left"  class="TDSmall"><input name="CV2" type="text" value="" size="5" maxlength="4">
				   <br /><font size="1">(Additional 3 digits on card signature strip, 4 on Amex cards)</font></td>
				</tr>

			
            <tr><td>&nbsp;</td><td><input type=submit name=submit value="Enter Payment" class="form-submit" /></td></tr>
            </table>
            
            
            </form>
            </div>
            <%
          
end if


end sub


function cleanInput(strRawText,strType)

	if strType="Number" then
		strClean="0123456789."
		bolHighOrder=false
	elseif strType="VendorTxCode" then
		strClean="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_."
		bolHighOrder=false
	else
  		strClean=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,'/{}@():?-_&£$=%~<>*+""" & vbCRLF
		bolHighOrder=true
	end if

	strCleanedText=""
	iCharPos = 1

	do while iCharPos<=len(strRawText)
    	'** Only include valid characters **
		chrThisChar=mid(strRawText,iCharPos,1)

		if instr(StrClean,chrThisChar)<>0 then 
			strCleanedText=strCleanedText & chrThisChar
		elseif bolHighOrder then
			'** Fix to allow accented characters and most high order bit chars which are harmless **
			if asc(chrThisChar)>=191 then strCleanedText=strCleanedText & chrThisChar
		end if

      	iCharPos=iCharPos+1
	loop       
  
	cleanInput = trim(strCleanedText)

end function


function URLEncode(strString)
	strEncoded=Server.URLEncode(strString)
	URLEncode=strEncoded
end function


function findField( strFieldName, strThisResponse )
	arrItems = split( strThisResponse, vbCRLF )
	for iItem = LBound( arrItems ) to UBound( arrItems )
		if InStr(arrItems(iItem), strFieldName & "=" ) = 1 then
			findField = mid(arrItems(iItem),len(strFieldName)+2)
			exit For
		end if
	next 
end function

 %>