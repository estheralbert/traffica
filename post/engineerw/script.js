function requestGeocode(elementId, lat, lng) {
  $("#" + elementId).html('<img src="loader.gif" width="16" height="11" alt="loading&hellip;">');
  $.post("a_geocode.asp", {longitude: lng, latitude: lat}, function(response) {
    $("#" + elementId).html(response);
  });
}

function getTravelTime(elementId, lat1, lng1, lat2, lng2) {
  $("#" + elementId).html('<img src="loader.gif" width="16" height="11" alt="loading&hellip;">');
  var postData = {latitude1: lat1, longitude1: lng1, latitude2: lat2, longitude2: lng2};
  $.post("a_traveltime.asp", postData, function(response) {
    $("#" + elementId).html(formatSecondsAsTime(response));
  });
}

function formatSecondsAsTime(seconds) {
  var hrs = Math.floor(seconds / 3600);
  var mins = padZeros(Math.floor((seconds % 3600) / 60), 2);
  var secs = padZeros((seconds % 60), 2);
  return hrs + ":" + mins + ":" + secs;
}

function padZeros(number, padding) {
  var str = "" + number;
  for (var i = 0; i < padding - str.length; i++) {
	str = "0" + str;
  }
  return str;
}