<%@ Language="VBScript" %>
<% Option explicit %>
<!--#include file="sha256.asp"-->
<!--#include file="util.asp"-->
<!--#include file="json2.asp"-->
<%
  verifyStaticLogin()
  dim issueId : issueId = Request.form("issueId")
  dim connection : set connection = Server.createObject("ADODB.Connection")
  connection.Open(CONNECTION_STRING)
  mainFunction()
  connection.Close()
%>
<script language="Javascript" runat="server">
function mainFunction() {
  var records = Server.createObject("ADODB.Recordset");
  records.cursorType = 0; // adOpenForwardOnly
  records.lockType = 1;   // adLockReadOnly
  var query = "SELECT (SELECT name FROM users WHERE id=userId) as name, note, timestamp FROM notes WHERE issueId=" + issueId + " ORDER BY timestamp ASC";
  records.open(query, connection);
  var i = 0;
  var notes = [];
  while (!records.eof) {
    notes[i] = {
	  name: records.fields("name").value,
	  note: records.fields("note").value,
	  timestamp: records.fields("timestamp").value
	};
    i++;
    records.moveNext();
  }
  records.close();
  Response.write(JSON.stringify(notes));
}
</script>