<%@ Language="VBScript" %>
<% Option explicit %>
<!--#include file="util.asp"-->
<%
  verifyLogin()
  dim connection : set connection = Server.createObject("ADODB.Connection")
  connection.Open(CONNECTION_STRING)
  dim mapLink : mapLink = "map.asp?"
  handleDBConnection(connection)
  connection.Close()
%>
<script language="Javascript" runat="server">
  function handleDBConnection(connection) {
    var userRecords = Server.createObject("ADODB.Recordset");
    userRecords.cursorType = 0; // adOpenForwardOnly
    userRecords.lockType = 1;   // adLockReadOnly
	var query = "SELECT id, name FROM users WHERE type=1";
	userRecords.open(query, connection);
	var i = 1;
	while (!userRecords.eof) {
	  if (i != 1)
	    mapLink += "&amp;";
	  
	  var userid = userRecords.fields("id").value;
	  var name = userRecords.fields("name").value;
	  
	  var movementRecords = Server.createObject("ADODB.Recordset");
	  var query = "SELECT longitude, latitude, action, siteId "
	            + "FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY timestamp DESC) AS row FROM movements WHERE userId=" + userid + ") M WHERE row=1";
      movementRecords.open(query, connection);
	  
	  if (!movementRecords.eof) {
		var actionId = movementRecords.fields("action").value, connection;
		var action = "Idle";
		if (actionId >= 1)
		  action = getActionString(actionId, connection);
		if (actionId == "1" /* heading */ || actionId == "2" /* at */ || actionId == "3" /* leaving */) {
		  var siteId = movementRecords.fields("siteId").value;
		  action += " Site " + siteId;
		}
		var lat = movementRecords.fields("latitude").value;
		var lng = movementRecords.fields("longitude").value;
		if (lng != "null" && lat != "null") {
		  mapLink += "m" + i + "=" + lat + "," + lng + "&amp;n" + i + "=" + name + "<br>" + action;
		}
	  }
	  movementRecords.close();
	  userRecords.moveNext();
	  i++;
	}
	userRecords.close();
  }

  var actionRecords;
  function getActionString(actionId, connection) {
    if (actionRecords == undefined) {
      actionRecords = Server.createObject("ADODB.Recordset");
	  query = "SELECT id, name FROM actions";
      actionRecords.cursorType = 2; // adOpenDynamic (allows moving backwards)
      actionRecords.lockType = 1;   // adLockReadOnly
	  actionRecords.open(query, connection);
	}
	actionRecords.moveFirst();
	actionRecords.find("id=" + actionId, 0, 1, 0);
	return actionRecords.fields("name");
  }
</script>
<!DOCTYPE html>
<head>
<title>Engineer Movement Report Map</title>
<script language="javascript">
function refreshMap() {
	window.location.href = "bigmap.asp";
}
</script>
<style type="text/css">
html, body {
	margin: 0;
	padding: 0;
	height: 100%;
	overflow: hidden;
}
#mapFrame {
	margin: 0;
	padding: 0;
	border: 0;
	width: 100%;
	height: 100%;
}
</style>
</head>
<body onLoad="javascript:setTimeout('refreshMap()', 240000)">
<iframe id="mapFrame" src="<%=mapLink %>" seamless></iframe>
</body>
</html>