<%
  dim CONNECTION_STRING : CONNECTION_STRING = "Driver={SQL Server};Server=CCP_SQL1;Database=engineers;UID=engineers;PWD=eng7890"
       


    ' response.End
  dim PASSWORD_SALT : PASSWORD_SALT = "RGVsaXNjaW91cyBTYWx0eSBHb29kbmVzcw=="
  
  sub verifyLogin()
    if Session("userid") = "" then
      Response.redirect("login.asp")
    end if
  end sub
  
  function attemptStaticLogin(username, password)
    'Aside from below, this should only be called directly by login.asp
    if username <> "" and password <> "" then
      dim conn : set conn = Server.createObject("ADODB.Connection")
      conn.Open(CONNECTION_STRING)
      dim recs : set recs = Server.createObject("ADODB.Recordset")
	  dim hash : hash = sha256(PASSWORD_SALT & password)
	  dim query : query = "SELECT id FROM users WHERE username='" & username & "' AND password='" & hash & "'"
	  recs.open query, conn
	  dim returnValue : returnValue = false
	  if (not recs.eof) then
		returnValue = recs.fields("id").value
	  end if
	  recs.close()
      conn.close()
	end if
	attemptStaticLogin = returnValue
  end function
  
  sub verifyStaticLogin()
    if Request.form("userid") = "" then
      Response.end()
	else
	  dim uid : uid = "" & attemptStaticLogin(Request.form("username"), Request.form("password"))
	  if (uid <> Request.form("userid")) then
	    Response.end()
	  end if
	end if
  end sub
%>
<script language="Javascript" runat="server">
  function padZeros(number, padding) {
    var str = "" + number;
	for (var i = 0; i < padding - str.length; i++) {
	  str = "0" + str;
	}
	return str;
  }
  
  function formatDate(date) {
	return padZeros(date.getDate(), 2) + "/" + padZeros(date.getMonth(), 2) + "/" + date.getYear() + " " + padZeros(date.getHours(), 2) + ":" + padZeros(date.getMinutes(), 2);
  }
  
  function formatTime(time) {
	return padZeros(time.getHours(), 2) + ":" + padZeros(time.getMinutes(), 2) + ":" + padZeros(time.getSeconds(), 2);
  }
  
  function formatSecondsAsTime(seconds) {
	var hrs = Math.floor(seconds / 3600);
    var mins = padZeros(Math.floor((seconds % 3600) / 60), 2);
	var secs = padZeros((seconds % 60), 2);
	return hrs + ":" + mins + ":" + secs;
  }
</script>

