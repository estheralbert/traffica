<script language="Javascript" runat="server">
// Technically reverse geocoding
function geocode(latitude, longitude, cacheOnly, connection) {
  var location = "";
  
  var records = Server.createObject("ADODB.Recordset");
  records.cursorType = 2; // adOpenDynamic
  records.lockType = 3;   // adLockOptimistic
  var query = "SELECT longitude, latitude, location FROM geocodeCache WHERE longitude=" + longitude + " AND latitude=" + latitude + "";
  records.open(query, connection);
  if (!records.eof) { // If found in database
	location = "" + records.fields("location");
  }
  else if (!cacheOnly) {
    // Ask Google for the geocode
    var link = Server.createObject("Microsoft.XMLHTTP");
	var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=true";
	link.open("GET", url, false);
	link.send();
	var response = link.responseText;
    try {
      jsonData = JSON.parse(response);
	  location = jsonData.results.get(0).formatted_address;
	  // Cache result
	  records.addNew();
	  records.fields("longitude") = longitude;
	  records.fields("latitude") = latitude;
	  records.fields("location") = location;
	  records.update();
    }
    catch (error) { // Will occur if connection to Google fails
	  // Do nothing, response will return blank.
    }
  }
  
  return location;
}

function traveltime(latitude1, longitude1, latitude2, longitude2, cacheOnly, connection) {
  var time = -1;
  
  var records = Server.createObject("ADODB.Recordset");
  records.cursorType = 2; // adOpenDynamic
  records.lockType = 3;   // adLockOptimistic
  var query = "SELECT latitude1, longitude1, latitude2, longitude2, time, distance FROM distanceCache"
            + " WHERE latitude1=" + latitude1 + " AND longitude1=" + longitude1
            + " AND latitude2=" + latitude2 + " AND longitude2=" + longitude2;
  records.open(query, connection);
  if (!records.eof) { // If found in database
	time = records.fields("time").value;
  }
  else if (!cacheOnly) {
    // Ask Google for the geocode
    var link = Server.createObject("Microsoft.XMLHTTP");
	var url = "http://maps.googleapis.com/maps/api/distancematrix/json?sensor=true&origins="
	        + latitude1 + "," + longitude1 + "&destinations=" + latitude2 + "," + longitude2;
	link.open("GET", url, false);
	link.send();
	var response = link.responseText;
    try {
      jsonData = JSON.parse(response);
	  var distance = jsonData.rows.get(0).elements.get(0).distance.value; // metres
	  time = jsonData.rows.get(0).elements.get(0).duration.value; // seconds
	  // Cache result
	  records.addNew();
	  records.fields("latitude1") = latitude1;
	  records.fields("longitude1") = longitude1;
	  records.fields("latitude2") = latitude2;
	  records.fields("longitude2") = longitude2;
	  records.fields("distance") = distance;
	  records.fields("time") = time;
	  records.update();
    }
    catch (error) { // Will occur if connection to Google fails
	  // Do nothing, response will return blank.
    }
  }
  
  return time;
}
</script>