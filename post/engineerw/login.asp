<%@ Language="VBScript" %>
<% Option explicit %>
<!--#include file="util.asp"-->
<!--#include file="sha256.asp"-->
<!--#include file="layout.asp"-->
<%
  if Session("userid") <> "" then
    if Session("userid") > 0 then
      Response.redirect("index.asp")
	else
	  Session.abandon()
	  Response.write("An error occurred.")
	  Response.end()
	end if
  else 'Session has not been created
    if Request.form("username") <> "" and Request.form("password") <> "" then
	  dim username : username = Request.form("username")
	  dim password : password = Request.form("password")
      dim connection : set connection = Server.createObject("ADODB.Connection")
      connection.Open(CONNECTION_STRING)
      dim records : set records = Server.createObject("ADODB.Recordset")
	  
      dim hash : hash = sha256(PASSWORD_SALT & password)
      dim query : query = "SELECT id FROM users WHERE username='" & username & "' AND password='" & hash & "'"
      records.cursorType = 0 ' adOpenForwardOnly
      records.lockType = 1   ' adLockReadOnly
      records.open query, connection
	  if (not records.eof) then
		dim userid : userid = records.fields("id")
		Session("userid") = userid
		Response.redirect("index.asp")
	  end if
	  records.close()
      connection.Close()
	end if
  end if
%>
<% doHeader("Login") %>
<form action="login.asp" method="post" class="formLayout">
<label for="username">Username:</label><input id="username" name="username"><br>
<label for="password">Password:</label><input id="password" name="password" type="password"><br>
<label></label><input type="submit" value="Login">
</form>
<% doFooter() %>
