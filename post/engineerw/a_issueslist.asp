<%@ Language="VBScript" %>
<% Option explicit %>
<!--#include file="sha256.asp"-->
<!--#include file="util.asp"-->
<!--#include file="json2.asp"-->
<%
  verifyStaticLogin()
  dim connection : set connection = Server.createObject("ADODB.Connection")
  connection.Open(CONNECTION_STRING)
  mainFunction()
  connection.Close()
%>
<script language="Javascript" runat="server">
function mainFunction() {
  var records = Server.createObject("ADODB.Recordset");
  records.cursorType = 0; // adOpenForwardOnly
  records.lockType = 1;   // adLockReadOnly
  var query = "SELECT I.id, I.siteId, I.message, I.timestamp, I.fixed, S.address, S.latitude, S.longitude "
			+ "FROM issues I INNER JOIN sites S ON S.id = I.siteId ORDER BY fixed ASC";
  records.open(query, connection);
  var issuesArray = [];
  var i = 0;
  while (!records.eof) {
    issuesArray[i] = {
      id : records.fields("id").value,
	  siteId : records.fields("siteId").value,
	  message : records.fields("message").value,
	  timestamp : records.fields("timestamp").value,
	  fixed : records.fields("fixed").value,
	  address : records.fields("address").value,
	  latitude : records.fields("latitude").value,
	  longitude : records.fields("longitude").value
	};
    records.moveNext();
	i++;
  }
  var issues = {issues : issuesArray};
  Response.write(JSON.stringify(issues));
  records.close();
}
</script>