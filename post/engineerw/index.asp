<%@ Language="VBScript" %>
<% Option explicit %>
<!--#include file="util.asp"-->
<!--#include file="geocode.asp"-->
<!--#include file="json2.asp"-->
<!--#include file="layout.asp"-->
<%
  verifyLogin()
  dim connection : set connection = Server.createObject("ADODB.Connection")
  connection.Open(CONNECTION_STRING)
  dim rowHtml : rowHtml = handleDBConnection()
  connection.Close()
%>
<script language="Javascript" runat="server">
  function handleDBConnection() {
    var userRecords = Server.createObject("ADODB.Recordset");
    userRecords.cursorType = 0; // adOpenForwardOnly
    userRecords.lockType = 1;   // adLockReadOnly
	var query = "SELECT id, name FROM users WHERE type=1";
	userRecords.open(query, connection);
	var rowHtml = "";
	var i = 1;
	while (!userRecords.eof) {
	  rowHtml += "<tr>";
	  var userid = userRecords.fields("id");
	  var name = userRecords.fields("name");
	  rowHtml += "<td><a href=\"engineer.asp?id=" + userid + "\">" + name + "</a></td>";
	  
	  var movementRecords = Server.createObject("ADODB.Recordset");
	  var query = "SELECT timestamp, siteId, longitude, latitude, action, row "
	            + "FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY timestamp DESC) AS row FROM movements WHERE userId=" + userid + ") M WHERE row=1";
      movementRecords.open(query, connection);
	  if (movementRecords.eof) {
	    rowHtml += "<td colspan=\"4\" class=\"errortext\">No data available</td>";
	  }
	  else {
	    var timestamp = movementRecords.fields("timestamp");
		var d = new Date(timestamp*1000);
	    var date = padZeros(d.getDate(), 2) + "/" + padZeros(d.getMonth(), 2) + "/" + d.getYear() + " " + padZeros(d.getHours(), 2) + ":" + padZeros(d.getMinutes(), 2);
		
		var latlng = "N/A";
		var lat = "" + movementRecords.fields("latitude");
		var lng = "" + movementRecords.fields("longitude");
		if (lng != "null" && lat != "null") {
		  latlng = "<a class=\"map\" title=\"View on map\" href=\"map.asp?m1=" + lat + "," + lng + "\">" + lat + "," + lng + "</a>";
		}
		
		var location = "";
		var siteId = "" + movementRecords.fields("siteId");
		if (siteId == "null" || movementRecords.fields("action").value == 1 /* heading */ || movementRecords.fields("action").value == 5 /* home */) {
		  // Location will be a GPS reading
		  location = geocode(lat, lng, true, connection);
		  if (location == "") {
		    location = "<a class=\"ajax\" href=\"javascript:void(0)\" onClick=\"javascript:requestGeocode('loc" + userid + "'," + lat + "," + lng + ")\" title=\"Click to lookup address\">?</a>";
		  }
		}
		else {
		  var siteRecords = Server.createObject("ADODB.Recordset");
		  var query = "SELECT address, longitude, latitude FROM sites WHERE id=" + siteId;
		  siteRecords.open(query, connection);
		  if (!siteRecords.eof) {
		    location = siteRecords.fields("address");
		  }
		  else {
			location = "Error: invalid site";
		  }
		}
		var actionId = movementRecords.fields("action").value;
		var action = getActionString(actionId, connection);
		if (actionId == "1" /* heading */ || actionId == "2" /* at */ || actionId == "3" /* leaving */) {
		  action += " Site " + siteId;
		}
	    rowHtml += "<td>" + date + "</td><td>" + latlng + "</td><td id=\"loc" + userid + "\">" + location + "</td><td>" + action + "</td>";
	  }
	  rowHtml += "</tr>";
	  userRecords.moveNext();
	  i++;
	}
	return rowHtml;
  }
  
  var actionRecords;
  function getActionString(actionId, connection) {
    if (actionRecords == undefined) {
      actionRecords = Server.createObject("ADODB.Recordset");
	  query = "SELECT id, name FROM actions";
      actionRecords.cursorType = 2; // adOpenDynamic (allows moving backwards)
      actionRecords.lockType = 1;   // adLockReadOnly
	  actionRecords.open(query, connection);
	}
	actionRecords.moveFirst();
	actionRecords.find("id=" + actionId, 0, 1, 0);
	return actionRecords.fields("name");
  }
</script>
<% doHeader("Home") %>
<h2>Current Activity<br><a href="bigmap.asp" class="map" title="The map will refresh automatically every five minutes.">View All Engineers On Map</a></h2>
<table id="engineersTable">
	<tr><th>Engineer</th><th>Updated</th><th>GPS (click for map)</th><th id="locationHeader">Location</th><th>Action</th></tr>
	<%= rowHtml %>
</table>
<% doFooter() %>