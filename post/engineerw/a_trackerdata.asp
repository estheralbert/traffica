<%@ Language="VBScript" %>
<% Option explicit %>
<!--#include file="sha256.asp"-->
<!--#include file="util.asp"-->
<!--#include file="json2.asp"-->
<%
  verifyStaticLogin()
  if Request.form("json") = "" then
    Response.write("true")
	Response.end()
  end if
  
  if (isValidJSON()) then
    ' For some unknown reason, connection to the database (at least on my development machine) only works when called in VBScript.
    dim connection : set connection = Server.createObject("ADODB.Connection")
    connection.Open CONNECTION_STRING
    handleDBConnection()
    connection.Close
  else
	Response.write("Invalid JSON.")
  end if
%>
<script language="Javascript" runat="server">
var jsonData, jsonString;
function isValidJSON() {
  jsonString = Request.form("json");
  var isValid;
  try {
    jsonData = JSON.parse(jsonString);
	isValid = true;
  }
  catch (error) {
    Response.write("JSON Parse Error");
	isValid = false;
  }
  return isValid;
}

// This function can use connection, jsonData, jsonString
function handleDBConnection() {
  var records = Server.createObject("ADODB.Recordset");
  var query = "SELECT id, userId, siteId, longitude, latitude, action, timestamp FROM movements";
  records.cursorType = 2; // adOpenDynamic
  records.lockType = 3;   // adLockOptimistic
  records.open(query, connection);
  
  var userId = Request.form("userid");
  var messageCount = 0;
  if (jsonData.messages != null)
    messageCount = jsonData.messages.length;
  for (var i = 0; i < messageCount; i++) {
    var message = jsonData.messages.get(i);
	
	records.addNew();
	records.fields("userId") = userId;
	records.fields("timestamp") = message.timestamp;
	// NB: siteId is -1 for action: home
	if (message.siteId == null) {
	  // This is a GPS message
	  if (message.longitude == 0 && message.latitude == 0) {
	    records.fields("longitude") = null;
		records.fields("latitude") = null;
	  }
	  else {
		records.fields("longitude") = message.longitude;
		records.fields("latitude") = message.latitude;
	  }
	}
	else {
	  // This is an action message
	  records.fields("siteId") = message.siteId;
	  records.fields("action") = getActionIndex(message.action, connection);
	  if (message.action == "heading" || message.action == "home") {
		records.fields("longitude") = message.longitude;
		records.fields("latitude") = message.latitude;
	  }
	  else { // action == "at" or "leaving" or "fixed"
		var latlngRecords = Server.createObject("ADODB.Recordset");
		latlngRecords.cursorType = 0; // adOpenForwardOnly
		latlngRecords.lockType = 1;   // adLockReadOnly
		var query = "SELECT latitude, longitude FROM sites WHERE id=" + message.siteId;
		latlngRecords.open(query, connection);
		if (!latlngRecords.eof) {
		  records.fields("longitude").value = latlngRecords.fields("longitude");
		  records.fields("latitude").value = latlngRecords.fields("latitude");
		}
		latlngRecords.close();
		
		if (message.action == "fixed") {
		  var fixedRecords = Server.createObject("ADODB.Recordset");
		  var query = "SELECT id, fixed FROM issues WHERE id=" + message.issueId;
		  fixedRecords.cursorType = 2; // adOpenDynamic
		  fixedRecords.lockType = 3;   // adLockOptimistic
		  fixedRecords.open(query, connection);
		  fixedRecords.fields("fixed") = true;
		  fixedRecords.update();
		  fixedRecords.close();
		}
	  }
	}
  }
  records.update();
  records.close();
  if (actionRecords != null)
    actionRecords.close();
  
  var records = Server.createObject("ADODB.Recordset");
  var query = "SELECT id, userId, issueId, note, timestamp FROM notes";
  records.cursorType = 2; // adOpenDynamic
  records.lockType = 3;   // adLockOptimistic
  records.open(query, connection);
  
  var noteCount = 0;
  if (jsonData.notes != null)
	noteCount = jsonData.notes.length;
  for (var i = 0; i < noteCount; i++) {
    var note = jsonData.notes.get(i);
	records.addNew();
	records.fields("userId") = userId;
	records.fields("issueId") = note.issueId;
	records.fields("note") = note.note;
	records.fields("timestamp") = note.timestamp;
  }
  records.update();
  records.close();
  
  Response.write("true");
}

var actionRecords;
function getActionIndex(str, connection) {
  if (actionRecords == undefined) {
	actionRecords = Server.createObject("ADODB.recordset");
	var query = "SELECT id, action FROM actions";
    actionRecords.cursorType = 2; // adOpenDynamic (allows moving backwards)
    actionRecords.lockType = 1;   // adLockReadOnly
	actionRecords.open(query, connection);
  }
  actionRecords.moveFirst();
  actionRecords.find("action='" + str + "'", 0, 1, 0);
  if (actionRecords.eof)
    return null;
  else
    return actionRecords.fields("id");
}
</script>