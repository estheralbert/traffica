<% Server.ScriptTimeout = 3600000 %>
		<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%
 pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 

%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<title>Perfomance- Today's Overview </title>
<LINK 
href="adminstylesheet.css" rel=stylesheet type=text/css>
</head>
<body>
<table width=240 height=320 border=0>
<tr>
<td colspan=2><b>Today's Overview </b> </td>
</tr>
<%
sql="exec sptodaysoverview"
openrs rs,sql
%>
<tr><td colspan=2><b>Analysis</b></td></tr>  
<!--<tr><td>Completed 1st Stage</td><td> <%=rs("anal1")%></td></tr> -->
<tr><td>Completed 2nd Stage</td><td> <%=rs("anal2")%></td></tr> 
<tr><td>Waiting (1st + 2nd)</td><td> <%=rs("analwaiting")%></td></tr> 
<tr><td colspan=2><b>Financial</b></td></tr><%
if rs("paymentscheque")="" or isnull(rs("paymentscheque")) then
	 	 paymentscheque=0
else
		paymentscheque=formatnumber(rs("paymentscheque"),2)
end if
if isnull(rs("paymentscc")) then
	 	 paymentsvv=0
else
		paymentscc=formatnumber(rs("paymentscc"),2)
end if

if isnull(rs("paymentsother")) then
	 	 paymentsother=0
else
		paymentsother=formatnumber(rs("paymentsother"),2)
end if
messagesover3=rs("messagesover3")
messagestoday=rs("messagestoday")

%>  
<tr><td>Cheque</td><td> �<%=paymentscheque%></td></tr> 
<tr><td>Credit Card</td><td> �<%=paymentscc%></td></tr> 
<tr><td>Other </td><td>�<%=paymentsother%></td></tr> 
<tr><td>Dvla Outstanding over 7 days</td><tD><%=rs("dvlaoutstanding")%></td></tr>

<tr><td>Messages  over 3hours (not responded)</td><tD><%=rs("messagesover3")%></td></tr>
<tr><td>Messages today</td><tD><%=rs("messagestoday")%></td></tr>

 <tr><td colspan=2><b>Performance</td></tr>  
<tr><td>Outgoing Letters </td><td><%=rs("appealstoday")%></td></tr> 
  <tr><td>Re-Issues Waiting (+7 days)</td><td><%=rs("totalreissuetbc")%></td></tr>
<!--<tr><td>Cancellations</td><td> <%= rs("canctoday")%></td></tr> -->
<tr><td>Notes</td><td> <%=rs("notestoday") %></td></tr> 
<tr><Td>Scanned Post  </td><td><%=rs("totaltoday")%></tD></tr>

<tr><Td>Outstanding Post     </td><td><%=rs("outstanding")%></tD></tr>
<tr><Td>Outstanding Post (+3 Days)    </td><td><%=rs("outstanding3")%></tD></tr>
 

 

</table>
<%closers rs


 %>


</body>
</html>
