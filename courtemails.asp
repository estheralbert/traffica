﻿<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="admininclude.asp"-->

<!doctype html>
<html lang="en">
<head>

<meta http-equiv="Content-type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" href="img/favicon.ico">
<link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/table.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables.js"></script>
<script type="text/javascript" src="js/jquery.jeditable.js"></script>
<script type="text/javascript" src="js/jquery.blockui.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $("#celebs");
        var oTable = table.dataTable({ "sPaginationType": "full_numbers",  "bStateSave": true });

        $(".editable", oTable.fnGetNodes()).editable("updatecourtemails.asp?cmd=updateemail", {
            "callback": function (sValue, y) {
                var fetch = sValue.split(",");
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(fetch[1], aPos[0], aPos[1]);
            },
            "submitdata": function (value, settings) {
                return {
                    "row_id": this.parentNode.getAttribute("id"),
                    "column": oTable.fnGetPosition(this)[2]
                };
            },
            "height": "14px"
        });

        $(document).on("click", ".delete", function () {
            var celeb_id = $(this).attr("id").replace("delete-", "");
            var parent = $("#" + celeb_id);
            $.ajax({
                type: "get",
                url: "updatenotice.asp?cmd=delete&id=" + celeb_id,
                data: "",
                beforeSend: function () {
                    table.block({
                        message: "",
                        css: {
                            border: "none",
                            backgroundColor: "none"
                        },
                        overlayCSS: {
                            backgroundColor: "#fff",
                            opacity: "0.5",
                            cursor: "wait"
                        }
                    });
                },
                success: function (response) {
                    table.unblock();
                    var get = response.split(",");
                    if (get[0] == "success") {
                        $(parent).fadeOut(200, function () {
                            $(parent).remove();
                        });
                    }
                }
            });
        });
    });
</script>
</head>
<body>

		Click in field directly to edit  - press enter to save the change  <div style="float:right"><a href="admin.asp" class="button">Back to Admin</a></div>
<br /><br />
	
        <%
		sql="select * from courtemails"
        openrs rs,sql

        %>
    
     <table class="table" id="celebs"><thead><tr><th>Court</th><th>Email</th></tr></thead><tbody>
        
        <% 
            do while not rs.eof
            response.write "<tr id='" & rs("id") & "'><td class='hidden'>" & rs("court") & "</td><td class='editable'>" & rs("email") & "</td></tr>"
            rs.movenext
            loop
            closers rs
             

            %>


</tbody>

    </table>



       

	
</body>
</html>