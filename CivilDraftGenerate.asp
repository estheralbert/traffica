<% Server.ScriptTimeout = 3600000 %>
<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%
	pStr = "private, no-cache, must-revalidate" 
    	Response.ExpiresAbsolute = #2000-01-01# 
    	Response.AddHeader "pragma", "no-cache" 
	Response.AddHeader "cache-control", pStr 
   
%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->

<!--#include file="image.asp"--><html>
<head>
<style type="text/css">
	body {
		font-family:"Times New Roman";serif;
		font-size:10px;		
	}
	h1 {
		font-family:Arial;sans-serif;
		text-align:right;
		padding-right: 20px;
		font-size: 18px;
	}
	table {
		font-size:10px;
	}
	table.detail th {
		padding: 5px;		
	}
	table.detail td {
		padding: 5px;		
	}
	table.detail {
		border-width: 2px;
		border-spacing: 0px;
		border-style: solid;
		border-color: black;
		border-collapse: collapse;
	}
	table.detail th {
		border-width: 1px;
		border-style: inset;
		border-color: black;
	}
	table.detail td {
		border-width: 1px;
		border-style: inset;
		border-color: black;
	}
	th {
		text-align: left;
	}
	div.address {
		height: 105px;
		vertical-align: top;
		padding-left: 10px;
		width: 450px;
	}
	.addressname {
		font-weight: bold;
		font-size: 12px;		
	}
	table.main {
		width: 670px;
	}
	ol {
		margin: 0px;
  		padding-left: 20px;
	}
	li {
		padding-left: 5px;
		padding-bottom: 12px;
		font-size: 1em;
	}
	.num {
		text-align: right;
	}
	.center {
		text-align: center;
	}
	p.bolded {
		font-weight: bold;
	}
	#coverletter, #coverletter table {
		font-family: Arial;sans-serif;
	}
	#coverletter li {
		padding-bottom: 3px;
	}
	
</style>
</head>
<body>

<%
id=request("id")

sql="select * from violators where id=" & id 
openrs rs,sql
sqldvla="select * from dvla where ID=" & rs("dvlaid")
openrs rsdvla,sqldvla
owner=rsdvla("owner")
address1=rsdvla("address1")
address2=rsdvla("address2")
address3=rsdvla("address3")
town=rsdvla("town")
postcode=rsdvla("postcode")

closers rsdvla

sqlrem="select reminder from reminders where pcn=" & tosql(rs("pcn"),"text")
openrs rsrem,sqlrem
reminderdate=rsrem("reminder")
closers rsrem
%>
	<div id="coverletter">
		<p class="bolded">
			<%=owner%><br />
			<%if address1<>"" then response.Write address1 & "<br>"%>
			<%if address2<>"" then response.Write address2 & "<br>"%>
			<%if address3<>"" then response.Write address3 & "<br>"%>
			<%if town<>"" then response.Write town & "<br>"%>
			<%if postcode<>"" then response.Write postcode & "<br>"%>
		</p>
		<p class="bolded">CIVIL ENFORCEMENT LIMITED -v- <%=owner %></p> 
		<p>Despite sending you a Reminder Notice on <b><%=formatdatetime(reminderdate,2) %></b> your debt remains 
		outstanding.<b> </b>As a result of your continued failure to settle the amount, 
		we now confirm that your liability to us has now increased as follows:- 
		<table>
		<tr>
			<td>Debt as of <%=date %></td>
			<td class="num" width="75px">&pound;<%=formatnumber(rs("ticketprice"),2) %></td>
		</tr>
		<tr>
			<td>Additional Costs</td>
			<td class="num">&pound;115.00</td>
		</tr>
		<tr>
			<th>Payment Now due</th>
			<th class="num">&pound;<%= formatnumber(rs("ticketprice")+115,2)%></th>
		</tr>
		</table>
		<p>In view of the above we have unfortunately been left with no alternative but 
		to instruct solicitors to draft Particulars of Claim (see attached) which we 
		intend to submit to Northampton County Court on <b>
		<%paymentdate=dateadd("d",8,date())
		paymentdate1=dateadd("d",7,date())
		if weekday(paymentdate)=1 then paymentdate=dateadd("d",9,date())
		if weekday(paymentdate)=7 then paymentdate=dateadd("d",10,date())
		response.Write paymentdate%>.</b></p> 
		<p clas="bolded">Please be aware that our representatives will be seeking full costs of the 
		application together with all our legal fees in dealing with this matter.</p>
		<p>Once a Judgment is obtained the options available to us is as follows:-</p>
		<ul>
			<li>A Warrant of Execution</li> 
			<li>Appoint Court Bailiffs to attend your property </li> 
			<li>Seize your vehicle / goods </li> 
			<li>Apply for an attachment of earning (see information below)</li>  
			<li>Third Party debt Order (see information below) </li> 
		</ul>
		<br />
		<p>If you are employed we may make an application for an <b>attachment of 
		earnings order </b>which will be sent to your employer. It will ensure that your 
		employer will take an amount from your earnings each pay day and send it to a 
		collection office which will then be forwarded to us.</p>
		<p>We may also make an application for a <b>thirdparty debt order </b>which is 
		usually made to stop the debtor from taking money out of his or her bank or 
		building society account. The money we are owed will be paid to us from your 
		account.</p>
		<p>As this matter remains unresolved costs are simply going to escalate. 
		However, we appreciate the level of your debt and are therefore prepared to 
		accept without prejudiced a reduced amount of &pound;<%=formatnumber(rs("ticketprice"),2) %> as full and final 
		settlement subject to such payment being made by no later than  <%=paymentdate1 %></p>
		<p>Please note, should you not accept our reduced offer we intend to show this 
		letter to the Court in respect of our costs. <b>Payments of the reduced amount 
		can only be made by calling 0115 822 5023. </b></p>
	</div>

	<br style="page-break-before: always">
<%
closers rs
sql="select * from violators where id=" & id 
openrs rs,sql
%>
	<table class="main">
	<tr>
		<td width="450">
			<h1>C l a i m&nbsp;&nbsp;F o r m</h1>
		</td>
		<td>
			<table class="detail" width="100%">
			<tr>
				<th>In The</th>
				<td class="center">Northampton County Court</td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td class="center">For court use only</td>
			</tr>
			<tr>
				<th>Claim No.</th>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<th>Issue Date</th>
				<td>&nbsp;</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<p>&nbsp;</p>
	<p>Claimant(s)</p>
	
	<div class="address">
		<img style="float:right;" src="seal.jpg"><p><span class="addressname">CIVIL ENFORCEMENT LIMITED</span><br />
		Horton House<br />
		Exchange Flags<br />
		Liverpool<br />
		L2 3PF </p>
	</div>
	<p>Defendant(s)</p>
	<div class="address">
		&nbsp;
	</div>
	<p>Brief details of claim</p>
	<div class="address">
		<br />
		Damages arising from a breach of contract<br />
		<br />
		See Particulars of Claim overleaf
	</div>
	<p>Value</p>
	<div class="address">
		&nbsp;
	</div>
	<table class="main">
	<tr>
		<td valign="top" width="450">
			<b>Defendantís name and address</b>
				<div>
					&nbsp;
				</div>
		</td>
		<td>
			<table class="detail" width="100%">
			<tr>
				<td width="100px">Amount Claimed</td>
				<td class="num">&pound;<%= formatnumber(rs("ticketprice")+115,2)%></td>
			</tr>
			<tr>
				<td>Court Fee</td>
				<td class="num">&pound;35.00</td>
			</tr>
			<tr>
				<td>Solicitorís Costs</td>
				<td class="num">&pound;110.00</td>
			</tr>
			<tr>
				<th>Total Amount</th>
				<th class="num">&pound;<%= formatnumber(rs("ticketprice")+115+35+110,2)%></th>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<hr />
	The court office at <br />
	<br />
	Is open between 10 am and 4 pm Monday to Friday.  When corresponding with the court please address forms or letters to the Court Manager and quote the claim number
	<hr />
	<table class="main">
	<tr>
		<td><b>N1</b> Claim form (CPR Part 7) (January 2002)</td>
		<td width ="100%" align="right">Crown Copyright. Reproduced by Sweet & Maxwell Ltd</td>
	</tr>
	</table>
	<br style="page-break-before: always">

<% closers rs 
sql="select * from VIOLATORS v left join [Site Information] s  on s.sitecode=v.site left join dvla d on d.id=v.dvlaid where v.ID=" & id 
openrs rs,sql
%>
	<table class="main">
	<tr>
		<td valign="top" width="450">&nbsp;</td>
		<td>
			<table class="detail" width="100%">
			<tr>
				<td width="70px">Claim No.</td>
				<td>&nbsp;</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<p>&nbsp;</p>
	<p>Does, or will, your claim include any issues under the Human Rights Act 1998? Yes  <input type=checkbox />&nbsp;No  <input type=checkbox checked /></p>
	<p>&nbsp;</p>
	<p>Particulars of Claim (<s>attached</s>)(<s>to follow</s>)</p>
	<ol>
		<li>At all material times the Claimant carried out business as a car park management company.</li>
		<li>The Defendant is contracted to manage the car park located at <%=pcase(rs("name of site")) %> 
			("the Car Park"). The Car Park is private property and is monitored by the Claimant with automatic 
			number plate recognition ("ANPR") cameras. </li>
		<li>Drivers are permitted to park in the car park for <%=rs("vtime") %> minutes free of charge; thereafter a charge of 
			&pound;<%=rs("ticketprice") %> is payable for any further parking, however this charge is reduced to &pound;
			<%=rs("reducedamount") %> if a driver makes payment within 14 days. </li>
		<li>In the Car Park there are many clear and visible signs displayed advising drivers of the terms and charges 
			applicable when parking in the Car Park, as set out above in paragraph 3. These signs constitute an offer 
			by the Claimant to enter into a contract with drivers. </li>
		<li>On <%=rs("date of violation") %> the Claiman's APNR cameras recorded the Defendant's vehicle, registration 
			number <%=rs("registration") %>, in the Car Park. At <%=rs("arrival time") %> the Defendant's vehicle 
			was parked. The Defendant's vehicle left the Car Park at <%=rs("departure time") %>. The vehicle had been 
			parked in the Car Park for <%= formatdatetime(rs("duration of stay"),4) %>. </li>
		<li>When the Defendant parked their vehicle in the Car Park they accepted, by their conduct, the Claimant's 
			offer as set out in the signs. Consequently, a contract was formed between the Claimant and Defendant. </li>
		<li>As the Defendant parked their vehicle for longer than <%=rs("vtime") %> minutes a charge was incurred, 
			as set out in paragraph 3 above. </li>
		<li>The DVLA provided the Claimant with the Defendant&rsquo;s address on <%=rs("datereceived") %> and on 
			<%=rs("pcnsent") %> the Claimant sent the Defendant a parking charge notice, reference number 
			<%=rs("pcn") & rs("site") %>, requesting payment of the charge incurred. </li>
		<li>The Defendant has not paid the outstanding charge. </li>
		<li>Further the Claimant claims interest pursuant to section 69 of the County Courts Act 1984 on the amount 
			found to be due to the Claimant at such rate and for such period as the court thinks fit. </li>
	</ol>
	<p>AND the Claimant claims</p>
		<p>&nbsp;(1)&nbsp;&nbsp;&nbsp;Damages in the sum of &pound;<%=rs("ticketprice") %> </p>
		<p>&nbsp;(2)&nbsp;&nbsp;&nbsp;Interest pursuant to section 69 of the County Courts Act 1984 to be assessed.</p>
	<p>&nbsp;</p>
	<img src=claimpage2.jpg />

<% closers rs%>

</body>
</html>