<%
' INCLUDE FILE aspEasyPDF ( www.mitdata.com )

' Global Properties definitions, see help file to known how to use it

' Text
const   csPropTextFont    = 100   
const   csPropTextSize    = 101   
const   csPropTextAlign   = 102   
const   csPropTextColor   = 103   
const   csPropTextUnderLine = 104 
const   csPropTextRender    = 105 
const   csPropTextOverGraph = 106
const   csPropText3DColor   = 107	
const   csPropText3DPos     = 108 
const   csPropTextAngle     = 109 
const   csPropCharSpacing   = 110 
const   csPropWordSpacing   = 111 
const   csPropTextEntityConv= 112
const   csPropAddTextWidth  = 113
const   csPropAddText_UpdPos= 114
const   csPropTextVertSpace = 115

' Page
const   csPageMarLeft     = 201     
const   csPageMarTop      = 202     
const   csPageMarRight    = 203     
const   csPageMarBottom   = 204     
const   csPropPosX        = 205
const   csPropPosY        = 206
const   csPageNumber      = 207
const   csPageWidth       = 208
const   csPageHeight      = 209
const   csPageBackColor   = 210	    
const   csPageAutoAdd	  = 211
const   csPageCount	  = 212

' HTML
const   csHTML_TRFullPage = 250
const   csHTML_xxx	  = 251	   ' for future use			
const   csHTML_FontName   = 252
const   csHTML_FontSize   = 253
const   csHTML_TableDraw  = 254
const 	csHTML_ImageRatio = 255

' Document information
const   csPropInfoTitle   = 300
const   csPropInfoSubject = 301
const   csPropInfoAuthor  = 302
const   csPropInfoCreator = 303
const   csPropInfoKeywords= 304

' Document
const   csDocuBackColor   = 320   

' Graphics
const   csPropGraphWidthLine = 400
const   csPropGraphXAlign = 401
const   csPropGraphYAlign = 402
const   csPropGraphWZoom  = 403
const   csPropGraphHZoom  = 404
const   csPropGraphLineColor = 405
const   csPropGraphFillColor = 406
const   csPropGraphFilled    = 407
const   csPropGraphBoxShadow   = 408
const   csPropGraphShadowColor = 409
const   csPropGraphWidthShadow = 410
const   csPropGraphBOXOnBack   = 411
const   csPropGraphZoom        = 412
const	csPropGraphBorder      = 413	
const   csPropGraphBCText      = 414 	
const   csPropGraphBCAngle     = 415
const   csPropGraphDashLine    = 416
const   csPropGraphShadowPos   = 417
const   csPropGraphJPGQuality  = 418
const   csPropGraphImageIndex  = 419
const	csPropGraphWidthImport = 420
const	csPropGraphHeightImport = 421
const	csPropGraphFullPage		= 422
const	csPropGraphBCRatio  	= 423
const	csPropGraphPDF417_Mode = 424
const	csPropGraphPDF417_SecurityLevel = 425
const	csPropGraphPDF417_Truncate = 426
const	csPropGraphInterpolate = 427
const	csPropGraphDPI = 428
const	csPropGraphAngle = 429

' Internal
const   csPropIntVer      = 500
const   csPropIntLZLevel  = 501
const   csPropIntCoord    = 502
const   csPropIntFitWin   = 503
const   csPropIntFullScreen  = 504
const   csPropIntProxyServer = 505
const   csPropIntDebugFile   = 506
const   csPropIntLZDocLevel  = 507
const   csPropIntRelativePath = 508
const   csPropIntUnicodeCheck = 509
const   csPropIntDebugTime = 510
const   csPropIntDebugTCP      = 511
const   csPropIntDebugLevel    = 512
const   csPropIntProxyUser     = 513
const   csPropIntProxyPass     = 514

' Objects
const   csPropObjDesign       = 600
const   csPropObjBorder       = 601
const   csPropObjBorderColor  = 602
const   csPropObjOptions      = 620
const   csPropObjOpened       = 621
const   csPropObjTxtMaxLen    = 690
const   csPropObjCbxValues    = 730

' Charts
const   csPropChartColor        = 800
const   csPropChartBackColor    = 801
const   csPropChartBackImage    = 802
const   csPropChartBackImageStyle = 803
const   csPropChartBackImageInside = 804
const   csPropChartGradientVisible = 805
const   csPropChartGradientStartColor = 806
const   csPropChartGradientEndColor = 807
const   csPropChartGradientType = 808
' Chart 3D
const   csPropChart3DView       = 820
const   csPropChart3DElevetation= 821
const   csPropChart3DOrthogonal = 822
const   csPropChart3DPerspective= 823
const   csPropChart3DRotation   = 824
const   csPropChart3DTilt       = 825
const   csPropChart3DZoom       = 826
const   csPropChart3DPercent    = 827
' Chart Axis
const   csPropChartAxisColor     = 830
const   csPropChartAxisGridColor = 831
const   csPropChartAxisFontName  = 832
const   csPropChartAxisFontSize  = 833
const   csPropChartAxisFontColor = 834
const   csPropChartAxisLeftAngle = 835
const   csPropChartAxisBottomAngle = 836
const   csPropChartAxisLeftType    = 837
const   csPropChartAxisBottomType  = 838
const   csPropChartAxisLeftInverted = 839
const   csPropChartAxisBottomInverted = 840
const   csPropChartAxisLeftGrid     = 841
const   csPropChartAxisBottomGrid   = 842
const   csPropChartAxisLeftVisible  = 843
const   csPropChartAxisBottomVisible = 844
' Chart Legend
const   csPropChartLegendPosition = 860
const   csPropChartLegendStyle    = 861
const   csPropChartLegendContents = 862
const   csPropChartLegendColor    = 863
const   csPropChartLegendFontName = 864
const   csPropChartLegendFontSize = 865
const   csPropChartLegendFontColor= 866
const   csPropChartLegendMaxRows  = 867
const   csPropChartLegendInverted = 868
const   csPropChartLegendVisible  = 869
' Chart Wall
const   csPropChartWallVisible    = 880
const   csPropChartWallLeftColor  = 881
const   csPropChartWallBackColor  = 882
const   csPropChartWallBottomColor= 883
const   csPropChartWallLeftSize   = 884
const   csPropChartWallBackSize   = 885
const   csPropChartWallBottomSize = 886
  
  
' Aligns
const   algLeft = 0
const   algRight = 1
const   algCenter = 2
const   algJustified = 3

' Code bars
const  csCode_2_5_interleaved = 0
const  csCode_2_5_industrial = 1
const  csCode_2_5_matrix = 2
const  csCode39 = 3
const  csCode39Extended = 4
const  csCode128A = 5
const  csCode128B = 6
const  csCode128C = 7
const  csCode93 = 8
const  csCode93Extended = 9
const  csCodeMSI = 10
const  csCodePostNet = 11
const  csCodeCodabar = 12
const  csCodeEAN8 = 13
const  csCodeEAN13 = 14
const  csCodeUPC_A = 15
const  csCodeUPC_E0 = 16
const  csCodeUPC_E1 = 17
const  csCodeUPC_Supp2 = 18
const  csCodeUPC_Supp5 = 19
const  csCodeEAN128A = 20
const  csCodeEAN128B = 21
const  csCodeEAN128C = 22
' From 3.0 version
const csCode_2_5_datalogic = 23
const csCode_2_5_IATA = 24
const csCode_2_5_Invert = 25
const csCode_2_5_Coop = 26
const csCodeABCCodabar = 27
const csCodeITF = 28
const csCodeISBN = 29
const csCodeISSN = 30
const csCodeISMN = 31
const csCodeOPC = 32
const csCode11 = 33
const csCodePZN = 34
const csCodePDF417= 35

' Link types
const lnkToURL		= 0
const lnkToPDF		= 1
const lnkDefineBookmark	= 2
const lnkToBookmark	= 3
const lnkToPosition	= 4

' Pattern types
const patHeader		= 0
const patFooter		= 1

' Note type
const noteIcon	    = 0
const noteCross	    = 1
const noteTextBoxed = 2

' Form constants
const foButton      = 0
const foCheckBox    = 1
const foRadioButton = 2
const foTextField   = 3
const foScrollList  = 4
const foComboBox    = 5

' Object Events
const aaOnMouseIn    = 0
const aaOnMouseOut   = 1
const aaOnMouseDown  = 2
const aaOnMouseUp    = 3
const aaOnFocusEnter = 4
const aaOnFocusExit  = 5
const aaOnVisible    = 6
const aaOnUnVisible  = 7
const aaOnKeyPress   = 8
const aaOnDrawField  = 9
const aaOnValueChange= 10

' Security options
const scAllowPrint     = 4
const scModifyContents = 8
const scCopyContents   = 16
const scAllowNotesForms= 32
const scAllowForms     = 256
const scExtractText    = 512
const scAssembleDoc    = 1024
const scPrintQuality   = 2048

%>