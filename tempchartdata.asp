<%@ language=vbscript %>
<%
  Response.Expires = 0
  Response.Buffer = true
  Response.Clear  
  Response.ContentType = "Image/Gif"

  'Use one of the following to create the object
  'Set Chart = Server.CreateObject("csDrawGraph.Draw")
  'Set Chart = Server.CreateObject("csDrawGraph64.Draw")
  Set Chart = Server.CreateObject("csDrawGraphTrial.Draw")
  'Set Chart = Server.CreateObject("csDrawGraph64Trial.Draw")

  Chart.AddData "Item 1", 17, "ff0000"
  Chart.AddData "Item 2", 28, "00ff00"
  Chart.AddData "Item 3", 5, "0000ff"

  If Request.QueryString("Type") = "Pie" Then
    Chart.Title = "Sample Pie Chart"
    Chart.BGColor = "eeeeee"
    Chart.LabelBGColor = "eeeeee"
    Chart.TitleBGColor = "eeeeee"
    Response.BinaryWrite Chart.GifPie
  Else
    Chart.Title = "Sample Bar Chart"
    Response.BinaryWrite Chart.GifBar
  End If
%>

