<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script src="js/jquery-1.9.1.js"></script> 
	<script src="js/jquery.dataTables.min.js"></script> 
	<script src="js/bootstrap.min.js"></script> 
	<script src="js/dataTables.altEditor.free.js"></script> 
	<script src="js/dataTables.buttons.min.js"></script> 
	<script src="js/dataTables.responsive.min.js"></script> 
	<script src="js/dataTables.select.min.js"></script> 
	
	<script type="text/javascript">
	
	$( document ).ready(function() {
	
	
		var dataSet =   [["Jena Gaines", "Office Manager", "London"],
						["Sonya Frost", "Software Engineer", "Edinburgh"]]
						
		 var columnDefs = 
		  [
			 { title: "Name" },
			 { title: "Position" },
			 { title: "Office" },
		  ];
		  
		var table = $('#example').DataTable({
		 "sPaginationType": "full_numbers",
		data: dataSet,
		columns: columnDefs,
			dom: 'Bfrtip',        // Needs button container
			  select: 'single',
			  responsive: true,
			  altEditor: true,     // Enable altEditor
			  buttons: [{
				text: 'Add',
				name: 'add'        // do not change name
			  },
			  {
				extend: 'selected', // Bind to Selected row
				text: 'Edit',
				name: 'edit'        // do not change name
			  },
			  {
				extend: 'selected', // Bind to Selected row
				text: 'Delete',
				name: 'delete'      // do not change name
			 }]
		
		});
			
		$('#example tbody').on('click', '.position', function () {
			var row = this.parentElement;
			if(!$('#example').hasClass("editing")){
				$('#example').addClass("editing");
				var data = table.row(row).data();
				var $row = $(row);
				var thisPosition = $row.find("td:nth-child(2)");
				var thisPositionText = thisPosition.text();
				thisPosition.empty().append($("<select></select>",{
					"id":"Position_" + data[0], 
					"class":"changePosition"
				}).append(function(){
					var options = [];
					$.each(Positions, function(k, v){
						options.push($("<option></option>",{
							"text":v,
							"value":v
						}))
					})
					return options;
				}));
				$("#Position_" + data[0]).val(thisPositionText)
			}
		});
		
		var Positions = [
			"System Architect", 
			"Accountant", 
			"Junior Technical Author", 
			"Senior Javascript Developer", 
			"Integration Specialist", 
			"Sales Assistant", 
			"Javascript Developer", 
			"Software Engineer", 
			"Office Manager", 
			"Support Lead", 
			"Regional Director", 
			"Senior Marketing Designer", 
			"Marketing Designer", 
			"Chief Financial Officer (CFO)", 
			"Systems Administrator", 
			"Personnel Lead", 
			"Development Lead", 
			"Chief Marketing Officer (CMO)", 
			"Pre-Sales Support", 
			"Chief Executive Officer (CEO)", 
			"Developer", 
			"Chief Operating Officer (COO)", 
			"Regional Marketing", 
			"Technical Author", 
			"Team Leader", 
			"Post-Sales support", 
			"Secretary", 
			"Financial Controller", 
			"Director", 
			"Support Engineer", 
			"Data Coordinator", 
			"Junior Javascript Developer", 
			"Customer Support"
		];
		
		$('#example tbody').on("change", ".changePosition", function(){
			var val = $(this).val();
			$(this).parent("td").empty().text(val);
			$('#example').removeClass("editing");
		});
	})
	</script>
	
	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/buttons.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="css/select.dataTables.min.css">
	<style>
		table.dataTable tbody>tr.selected,
		table.dataTable tbody>tr>.selected {
		  background-color: #A2D3F6;
		}
	</style>
</head>
<body>
	<div class="container">
		<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Site Code</th>
					<th>Tariff</th>
					<th>Duration</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="sitecode">Tiger Nixon</td>
					<td class="position">System Architect</td>
					<td class="location">Edinburgh</td>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>