<%if session("userid")="" then response.redirect "../admin.asp"
 %>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Site Tariff Durations</title>
	<script src="js/jquery-1.9.1.js"></script> 
	<script src="js/jquery.dataTables.min.js"></script> 
	<script src="js/bootstrap.min.js"></script> 
	<script src="js/dataTables.altEditor.free.js"></script> 
	<script src="js/dataTables.buttons.min.js"></script> 
	<script src="js/dataTables.responsive.min.js"></script> 
	<script src="js/dataTables.select.min.js"></script> 
	
	<script type="text/javascript">
	$( document ).ready(function() {
		 var columnDefs = 
		  [  
			{ visible: false, data: 'id'},
			{ title: 'Site Code', data: 'sitecode' },
			{ title: 'Tariff', data: 'tariff' },
			{ title: 'Duration', data: 'duration' },
		  { title: 'Price', data: 'price' }
		  ];
		  
		var table = $('#example').DataTable({
			ajax: 'dataset.asp',
			'sPaginationType': 'full_numbers',
			columns: columnDefs,
				dom: 'Bfrtip',      
				  select: 'single',
				  responsive: true,
				  altEditor: true,  
				  buttons: [{
					text: 'Add',
					name: 'add'     
				  },
				  {
					extend: 'selected', 
					text: 'Edit',
					name: 'edit'        
				  },
				  {
					extend: 'selected', 
					text: 'Delete',
					name: 'delete'      
				 }]
		});
			
		
	})
	</script>
	
	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/buttons.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="css/select.dataTables.min.css">
	<style>
		table.dataTable tbody>tr.selected,
		table.dataTable tbody>tr>.selected {
		  background-color: #A2D3F6;
		}
	</style>
</head>
<body>
	<div class="container">
		<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Id</th>
					<th>Site Code</th>
					<th>Tariff</th>
					<th>Duration</th>
                    <th>Amount</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
</body>
</html>