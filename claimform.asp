<% Server.ScriptTimeout = 3600000 %>
<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%
 	pStr = "private, no-cache, must-revalidate" 
    	Response.ExpiresAbsolute = #2000-01-01# 
    	Response.AddHeader "pragma", "no-cache" 
    	Response.AddHeader "cache-control", pStr 
   
%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->




<html>
<head>
<style type="text/css">
    #main
    {
       font-size:120%
    }
     	body {
		font-family:"Times New Roman";serif;
		font-size:11px;		
	}
	h1 {
		font-family:Arial;sans-serif;
		text-align:right;
		padding-right: 20px;
		font-size: 18px;
	}
	table {
		font-size:10px;
	}
	table.detail th {
		padding: 7px;		
	}
	table.detail td {
		padding: 5px;		
	}
	table.detail {
		border-width: 2px;
		border-spacing: 0px;
		border-style: solid;
		border-color: black;
		border-collapse: collapse;
		width:290px;
	}
	table.detail th {
		border-width: 1px;
		border-style: inset;
		border-color: black;
	}
	table.detail td {
		border-width: 1px;
		border-style: inset;
		border-color: black;
		font-size:140%;
	}
	th {
		text-align: left;
	}
	div.address {
		height: 105px;
		vertical-align: top;
		padding-left: 10px;
		width: 650px;
		font-size:140%;
	}
	.addressname {
		font-weight: bold;
		font-size: 14px;		
	}
	table.main {
		width: 650px;
		font-size:120%;
	}
	ol {
		margin: 0px;
  		padding-left: 20px;
	}
	li {
		padding-left: 5px;
		padding-bottom: 12px;
		font-size: 1em;
		padding-top:3px;
		
	}
	.num {
		text-align: right;
	}
	.center {
		text-align: center;
	}
	p.bolded {
		font-weight: bold;
	}
	#coverletter, #coverletter table {
		font-family: Arial;sans-serif;
		font-size:13px;
	}
	#coverletter li {
		padding-bottom: 3px;
	}
	#debtrecovery, #debtrecovery table {
		font-size: 12px;
	}
	p
	{
	    font-size:110%
	}
</style>
</head>
<body>
<div id="claimform">
	<% 
	spcn=request("pcn")
   
		sql="select * from violators v left join dvla d on d.id=v.dvlaid where pcn+site=" & tosql(spcn,"text")
		openrs rs,sql
        owner=rs("owner")
        address1=rs("address1")
        address2=rs("address2")
        address3=rs("address3")
        town=rs("town")
        postcode=rs("postcode")

		%>
			<table class="main">
			<tr>
				<td width="360">
					<h1>C l a i m&nbsp;&nbsp;F o r m&nbsp;&nbsp;</h1>
				</td>
				<td>
					<table class="detail">
					<tr>
						<th>In The</th>
						<td class="center">Barnet County Court</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<td class="center">For court use only</td>
					</tr>
					<tr>
						<th>Claim No.</th>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<th>Issue Date</th>
						<td>&nbsp;</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
			<p>&nbsp;</p>
			<p>Claimant(s)</p>
			
			<div class="address">
				<img style="float:right;" src="seal.jpg"><p><span class="addressname">CIVIL ENFORCEMENT LIMITED</span><br />
				Horton House<br />
				Exchange Flags<br />
				Liverpool<br />
				L2 3PF<br />
                <br />
                 Telephone  0870 919 5577<br>
PCN <%=spcn %> </p>
               
			</div>
			<p>Defendant(s)</p>
			<div class="address">
				<%=pcase(owner)%><br />
					
					<br /><br />
			</div>
			<p>Brief details of claim</p>
			<div class="address">
				<br />
				Damages arising from a breach of contract<br />
				<br />
				See Particulars of Claim overleaf
			</div>
			<p>Value</p>
			<br />
			<table class="main">
			<tr>
				<td valign="top" width="360">
					<b>Defendantís name and address</b>
						<div>
							
				<%=pcase(owner)%><br />
					<%if address1<>"" then response.Write pcase(address1) & "<br>"%>
					<%if address2<>"" then response.Write pcase(address2) & "<br>"%>
					<%if address3<>"" then response.Write pcase(address3) & "<br>"%>
					<%if town<>"" then response.Write pcase(town) & "<br>"%>
					<%if postcode<>"" then response.Write postcode & "<br>"%>
					<br /><br />
						</div>
				</td>
				<td>
					<table class="detail" width="100%">
					<tr>
						<td width="190px">Amount Claimed</td>
						<td class="num">&pound;<%= formatnumber(rs("ticketprice"),2)%></td>
					</tr>
					<tr>
						<td>Court Fee</td>
						<td class="num">&pound;50.00</td>
					</tr>
					<tr>
						<td>Solicitorís Costs</td>
						<td class="num">&pound;50.00</td>
					</tr>
					<tr>
						<td><strong>Total Amount</strong></td>
						<td class="num">&pound;<%= formatnumber(rs("ticketprice")+50+50,2)%></td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
			<hr />
			The court office at <br />
			<br />
			Is open between 10 am and 4 pm Monday to Friday.  When corresponding with the court please address forms or letters to the Court Manager and quote the claim number
			
			<br style="page-break-before: always">
		
		<% closers rs 
		sql="select * from VIOLATORS v left join [Site Information] s  on s.sitecode=v.site left join dvla d on d.id=v.dvlaid where v.pcn+v.site=" & tosql(spcn,"text")
		openrs rs,sql
		%>
			<table class="main">
			<tr>
				<td valign="top" width="360">&nbsp;</td>
				<td>
					<table class="detail" width="100%">
					<tr>
						<th>Claim No.</th>
						<td width="70%">&nbsp;</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
			<p>&nbsp;</p>
			<p>Does, or will, your claim include any issues under the Human Rights Act 1998? Yes  <input type=checkbox />&nbsp;No  <input type=checkbox checked /></p>
			<p>&nbsp;</p>
			<p>Particulars of Claim </p>
			<ol>
				<li>At all material times the Claimant carried out business as a car park management company.</li>
				<li>The Claimant is contracted to manage the car park located at <%=showns(pcase(rs("name of site"))) %> 
					("the Car Park"). The Car Park is private property and is monitored by the Claimant with automatic 
					number plate recognition ("ANPR") cameras. </li>
			

				<li>In the Car Park there are many clear and visible signs displayed advising drivers of the terms and charges applicable when parking in the Car Park.  Drivers are permitted to park in the Car Park in accordance with terms displayed on the signage. These signs constitute an offer by the Claimant to enter into a contract with drivers. </li>
				<li>The Claimant's ANPR cameras recorded the Defendant's vehicle, registration number <%=rs("registration") %> in the Car Park.
				<% if isnull(rs("arrival time")) then 
				        arrivaltime=""
				   else
				        arrivaltime=formatdatetime(rs("arrival time"),4)
				   end if
				if isnull(rs("departure time")) then 
				        departuretime=""
				        departureday=""
				   else
				         departureday=formatdatetime(rs("departure time"),2)
				        departuretime=formatdatetime(rs("departure time"),4)
				   end if
				
				
				 %>
					The Defendant's vehicle entered the Car Park on <%=rs("date of violation") %> at <%=arrivaltime %> and left the Car Park on <%=departureday %> at <%=departuretime %>. The vehicle had been parked in the  Car Park for  <%=hour(rs("duration of stay")) %> hours and <%=minute(rs("duration of stay")) %> minutes. </li>
				<li>When the Defendant parked their vehicle in the Car Park they accepted, by their conduct, the Claimant's 
					offer as set out on the signage. Consequently, a contract was formed between the Claimant and Defendant. </li>
			

          <li>	The Defendant parked their vehicle for longer than permissible  and therefore a charge was incurred.</li>

<li>	The Defendant has not paid the outstanding charge.</li>

<li>	Further the Claimant claims interest pursuant to section 69 of the County Courts Act 1984 on the amount found to be due to the Claimant at such rate and for such period as the court thinks fit.</li>




			</ol>
			<p>AND the Claimant claims</p>
				<p>&nbsp;(1)&nbsp;&nbsp;&nbsp;Damages in the sum of &pound;<%=rs("ticketprice")+100 %> </p>
				<p>&nbsp;(2)&nbsp;&nbsp;&nbsp;Interest pursuant to section 69 of the County Courts Act 1984 to be assessed.</p>
		
			<img src=claim.jpg />
		
		<% closers rs


	%>
</body>
</html>

<%
function showns(ns)
ns=trim(ns)
lns=len(ns)
nsn=left(pcase(ns),lns-8)' & "**"



nsn=nsn & mid(ucase(ns),lns-7)
showns=nsn

end function
function checkifletteroutstanding(pcn)
	checkifletteroutstanding=0
	sql="select *  from incomingletters  where pcn='" & pcn & "' and (statusid=5 or statusid=6)"
	openrs rscl,sql
	if not rscl.eof and not rscl.bof then checkifletteroutstanding=1
	closers rscl
end function

function getletterstatus(statusid)

	sql="select * from letterstatus where statusid=" & statusid
	openrs rs,sql
	if not rs.eof and not rs.bof then	
		getletterstatus=rs("status")
	else
		getletterstatus=""
	end if
	closers rs
	
end function
	
sub shownotes(pcn)
	'respone.write "here"
	sql="select pcnnotes.id,pcn,mydate,ip,pcnnote,filereference,pdffile,type,useraccess.userid,clientusers.userid as clientuserid from pcnnotes left join useraccess on useraccess.id=pcnnotes.userid left join clientusers on clientusers.id=pcnnotes.userid where pcn=" & tosql(pcn,"text") & " order by id desc"
	openrs rs,sql
	if not rs.eof and not rs.bof then response.write "<table class=maintable2><tr><td colspan=2 align=center><b>Notes:</b></td></tr><tr><td><b>Date</b></td><td>Note</td><td>UserName</td><td>File Reference</td><td>Type</td></tr>"
	
	do while not rs.eof
		pcnnote= rs("pcnnote") 
		fileref=rs("filereference")
		st=rs("type")
		userid=rs("userid")
		if userid="" then userid=rs("clientuserid")
		response.write "<tr><td valign=top>" & rs("mydate") & "</td><td valign=top>" &pcnnote&  "</td><td>" &   userid & "</td><td>" & fileref & "</td><td>" &st & "</td></tr>"
		if rs("pdffile")<>"" then
			response.write "<tr><td colspan=4>"
			%>
			<EMBED src="pdfnotesfiles/<%=rs("pdffile")%>" width="450" height="350" "pdfnotesfiles/<%=rs("pdffile")%>"></EMBED>
			<%
			response.write "</td></tr>"
		end if
		rs.movenext
	loop
	
	
	
	sql="select mydatetime,comments,fileno,useraccess.userid as clientuserid from appeals left join useraccess on useraccess.id=appeals.userid  where comments is not null and  pcn=" & tosql(pcn,"text") & " order by appeals.id desc"
	'resposne.write sql 
	openrs rs,sql
	'if not rs.eof and not rs.bof then response.write "
	
	do while not rs.eof
		pcnnote= rs("comments") 
		fileref=rs("fileno")
		st="appeal"
		'userid=rs("userid")
		' if userid="" then 
		userid=rs("clientuserid")
		response.write "<tr><td valign=top>" & rs("mydatetime") & "</td><td valign=top>" &pcnnote&  "</td><td>" &   userid & "</td><td>" & fileref & "</td><td>" &st & "</td></tr>"
		response.write "</td></tr>"
		rs.movenext
	loop	
	
	if not rs.eof and not rs.bof then response.write "</table>"
	closers rs
		
	sqlap="select * from rm where pcn=" & tosql(pcn,"text")
	'response.write sqlap
	openrs rsap,sqlap
	if not rsap.eof and not rsap.bof then
		response.write "<table><tr><th colspan=2>Recorded Calls</th></tr>"
		do while not rsap.eof
			%>
			<tr><td colspan=2 align=center>Recorded Call on  <%=rsap("mydatetime") %> <a href=admin.asp?cmd=viewrm&id=<%=rsap("id") %> class=button target=_new>View Message</a></td></tr>
			<%
		rsap.movenext
		loop
	end if 
	closers rsap
end sub

%></div>