<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="admininclude.asp"-->
<!--#include file="adminheader.asp"-->

<!-- jQuery -->
<script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>  
<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css"> 
<style type="text/css">
    table#tblEnginerAlerts {width:500px;margin:50px auto;font-family:Arial; border-collapse:collapse;}
    #tblEnginerAlerts th, #tblEnginerAlerts td {padding:10px; border:1px solid #444; border-collapse:collapse;}
    #tblEnginerAlerts form {margin:0px;}
</style>
<%
    if request("sbmt")="Add" then
        email = request("email")
        alertid = request("alertid")
        sql = "INSERT INTO engineerEmailAlerts (email, emailAlertId) VALUES (" & tosql(email,"Text") & ", " & tosql(alertid,"Number") & ")"
        objconn.execute sql
    end if
    if request("sbmt")="Delete" then
        id = request("id")
        sql = "DELETE FROM engineerEmailAlerts WHERE id = " & tosql(id,"Number")
        objconn.execute sql
    end if
    if request("sbmt") = "Update" then 
        id = request("id")
        email = request("email")
        alertid = request("alertid")
        sql = "UPDATE engineerEmailAlerts SET email = " & tosql(email,"Text") & ", emailAlertId = " & tosql(alertid,"Number") & " WHERE id = " & tosql(id,"Number")
        objconn.execute sql
    end if

    alertOptions = ""
    openrs rs, "SELECT * FROM emailAlerts ORDER BY alert"
    do while not rs.eof
        alertOptions = alertOptions & "<option value='" & rs("id") & "'>" & rs("alert") & "</option>"
        rs.movenext
    loop
    closers rs

%>

<table id="tblEnginerAlerts">
    <thead>
        <tr><th>Email</th><th>Alert</th><td></td></tr>
        <tr>
            <th><form method="post"><input type="text" name="email" /></th>
            <th><select name="alertid"><%=alertOptions%></select></th>
            <th style="width:200px;"><input type="submit" name="sbmt" value="Add" /></form></th>
        </tr>
    </thead>
    <tbody>
    <%
        sql = "SELECT * from engineerEmailAlerts ORDER BY email"
        openrs rs, sql
        do while not rs.eof
            currentAlertOptions = replace(alertOptions, "value='"&rs("emailAlertId")&"'", "value='"&rs("emailAlertId")&"' selected")
            %>
            <tr>
                <td>
                    <form method="post">
                    <input type="hidden" name="id" value="<%=rs("id")%>" />
                    <input type="text" name="email" value="<%=rs("email")%>"/>
                </td>
                <td><select name="alertid"><%=currentAlertOptions%></select></td>
                <td>
                    <input type="submit" name="sbmt" value="Update" />
                    <input type="submit" name="sbmt" value="Delete" onclick="return confirm('Are you sure?');"/>
                    </form>
                </td>
            </tr>
            <%
            rs.movenext
        loop
    %>
    </tbody>
</table>
<script type="text/javascript">
   // $(document).ready(function () {
   //     $('#tblEnginerAlerts').dataTable();
   // });
</script>
    


