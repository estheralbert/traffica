
<!--#include file="openconn.asp"-->

<!--#include file="common.asp"-->


<%
sql="select count(distinct violators.id) as count,violators.site,count(payments.id) as paid,count(cancelled.id) as cancelled,DATEPART(QUARTER, pcnsent) as  quarter, DATEPART(YEAR, pcnsent) as year from violators left join payments on violators.pcn+violators.site=payments.pcn left join cancelled on violators.pcn+violators.site=cancelled.pcn group by violators.site,DATEPART(QUARTER, pcnsent), DATEPART(YEAR, pcnsent) order by site, DATEPART(YEAR, pcnsent),DATEPART(QUARTER, pcnsent)"
openrs rs,sql
response.Write "<table border=1><th>site</th><th>Quarter</th><th>Tickets</th><th>Paid</th><th>Cancelled</th><th>% Paid</th><th>% Cancelled</th></tr>"
do while not rs.eof

response.Write "<tr><td>" & rs("site") & "</td><td>" & rs("quarter") & " " & rs("year") & "</td><td>" & rs("count") & "</td><td>" & rs("paid") & "</td><td>" & rs("cancelled") & "</td><td>" & formatnumber(100*(rs("paid")/rs("count")),2) & "%</td><td>" & formatnumber(100*(rs("cancelled")/rs("count")),2) & "%</td></tr>"

rs.movenext
loop
closers rs
response.Write "</table>"
%>