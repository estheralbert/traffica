<!--#INCLUDE FILE="openconn.asp"-->
<!--#INCLUDE FILE="common.asp"-->

<html>
    <head>
        <title></title>
        <LINK href="adminstylesheet.css" rel=stylesheet type=text/css>
        <style type="text/css">
            .recPart{display:block;float:left;padding:10px;}
            .signLbl{clear:left;}
            .signLnk{min-width:100px;}
            .signLnk a {cursor:pointer;}
            .btnDel {color:red; border:1px solid red; padding:3px;}
            .uploadLnk{cursor:pointer;}
            .uploadPanel{display:none;position:absolute;width:350px;}
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript">
            //$(document).ready(function () {

            function TogglePanel(siteCode) {
                //console.log("toggling: " + siteCode);
                $('#pnl' + siteCode).toggle();
            }

            function DeleteImg(imgName, siteCode) {
                $.ajax({
                    url: 'ajax.asp?cmd=deleteSignImg&imgName=' + imgName,
                    dataType: 'html',
                    success: function (data) {
                        if (data.replace(" ", "") != "deleted") {                           
                            console.log("ERROR:" + data);
                            $('#del_' + siteCode).html("An Error Occured.");
                        }
                        else {
                            $('#del_' + siteCode).hide();
                            $('#lnk_' + siteCode).hide();
                        }                        
                    }
                });  
            }

           // });
        </script>
    </head>
    <body>        
<%
    dim fs
    set fs=Server.CreateObject("Scripting.FileSystemObject")

    sql="SELECT sitecode FROM [site information] ORDER BY sitecode"
   
    openrs rs,sql
    
    do while not rs.eof
        siteCode = rs("sitecode")
        signPath = "/signs/"&siteCode
        signExists = false
        Response.Write "<div class='recPart signLbl'>"&siteCode&"</div>"
    
        if fs.FileExists(Server.MapPath(signPath&".pdf")) then
            signPath = signPath&".pdf"
            imgName = siteCode&".pdf"
            signExists = true
        elseif fs.FileExists(Server.MapPath(signPath&".jpg")) then
            signPath = signPath&".jpg"
            imgName = siteCode&".jpg"
            signExists = true
        end if
        
        Response.Write "<div class='recPart signLnk'>"
        if signExists then
            Response.Write "<a id='lnk_"&siteCode&"' href='"&signPath&"' target='_blank'>"&signPath&"</a> <a id='del_"&siteCode&"' class='btnDel' onclick='DeleteImg("""&imgName&""", """&siteCode&""");'>X</a>"
        end if
        Response.Write "</div>"

        %>
        <div class='recPart uploadLnk' onclick="TogglePanel('<%=siteCode%>');">Upload Image</div>
        <div class="recPart" style="position:relative;">
            <div class="uploadPanel" id="pnl<%=siteCode%>">
                <form method="post" ENCTYPE="multipart/form-data" action="uploadsign.asp?sitecode=<%=sitecode %>">
                  
                    <input type="file" name="signFile" value="browse">
                    <input type="submit" value="Upload Sign">
                </form>
            </div>
        </div>
        <br/>
        <%

        rs.movenext
    loop

    closers rs
    set fs=nothing

    if request("cmd")="save" then    
        stype=request("type")
        sql="exec printcpr @type='" & stype & "'"   
        openrs rs,sql
        batchid=rs("batchid")
        if stype="1 Page" then
            response.redirect "printcpr1page.asp?batchid=" & batchid
        else
            response.redirect "printcprletter6page.asp?batchid=" & batchid
        end if
        closers rs
    
    else
%>
    

<% end if %>

    </body>
    </html>

  