﻿<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<%
id=request("id")
sql="select * from violators where id=" & id 
openrs rs,sql


 %>




<table><tr><td align=center width=400><font size=20>Claim  Form </font></td><td align=right>

<TABLE cellSpacing="0" cellPadding="0" border="1">
<TBODY>
<TR>
<TD vAlign="top" width="76">
<P><B>In The</B></TD>
<TD vAlign="top" width="151">
<P align="center">Northampton County Court</TD>
</TR>
<TR>
<TD vAlign="top" width="76">
<P></TD>
<TD vAlign="top" width="151">
<P align="center">For court use only</TD>
</TR>
<TR>
<TD vAlign="top" width="76">
<P><B>Claim No.</B></TD>
<TD vAlign="top" width="151">
<P></TD>
</TR>
<TR>
<TD vAlign="top" width="76">
<P><B>Issue Date</B></TD>
<TD vAlign="top" width="151">
<%=date() %></TD>
</TR>
</TBODY>
</TABLE>
</td></tr>
<tr><td>Claimant(s)<br /><br />
<P><B>CIVIL ENFORCEMENT LIMITED </B>
<BR>Horton House 
<P>Exchange Flags 
<P>Liverpool 
<P>L2 3PF
<br /><br /><br /><br /><br />
Defendant(s)
<br /><br /><br /><br />
</td></tr>

<tr><td colspan=2>

Brief details of claim
<br /><br />
<blockquote>Damages arising from a breach of contract

See Particulars of Claim overleaf 
</blockquote>
<br /><br />
Value
<br /><br /><br /><br />
Defendant's name and address
</td></tr>
<table><tr><td align=center width=400></td><td align="right"><TABLE cellSpacing="0" cellPadding="0" border="1">
<TBODY>
<TR>
<TD vAlign="top" width="142">
<P>Amount Claimed</TD>
<TD vAlign="top" width="142">
<P>&pound;<%= formatnumber(rs("ticketprice")+115,2)%></TD>
</TR>
<TR>
<TD vAlign="top" width="142">
<P>Court Fee</TD>
<TD vAlign="top" width="142">
<P>&pound;35.00</TD>
</TR>
<TR>
<TD vAlign="top" width="142">
<P>Solicitor&rsquo;s Costs</TD>
<TD vAlign="top" width="142">
<P>&pound;110.00</TD>
</TR>
<TR>
<TD vAlign="top" width="142">
<P><B>Total Amount</B></TD>
<TD vAlign="top" width="142">
<%= formatnumber(rs("ticketprice")+115+35+110,2)%></TD>
</TR>
</TBODY>
</TABLE></td></tr>



</table>
<hr />
<font size=-1>


The court office at

Is open between 10 am and 4 pm Monday to Friday.  When corresponding with the court please address forms or letters to the Court Manager and quote the claim number

<br />
N1 Claim form (CPR Part 7) (January 2002)                                                                                                     Crown Copyright. Reproduced by Sweet & Maxwell Ltd
</font>



<% closers rs %>