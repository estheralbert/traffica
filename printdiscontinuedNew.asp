﻿<!--#include file="openconn.asp"-->
<!--#include file="common.asp"-->
<%
    server.ScriptTimeout=100000    
    batchId =  request("batchid")
    id=request("id")
    if batchid = "" and id="" then
        Response.Write "batchId is required"
        Response.End
    end if
    if request("debug")="true" or request("debug")=1 then idebug = 1 else idebug = 0 end if
    idebug=1

    Set Pdf = Server.CreateObject("Persits.PDF")

    Set objDocfinal = Pdf.CreateDocument()

    set docDiscontinued = Pdf.OpenDocument( "C:\inetpub\wwwroot\traffica\discontinued3.pdf" )
    pageWidth = docDiscontinued.Pages(1).Width
    pageHeight = docDiscontinued.Pages(1).Height

    myd=cisodate(date())

    destinationFolder = "C:\inetpub\wwwroot\traffica\excelfiless\"  & myd & "\"

    PrintDiscontinuedBatch(batchId)

    function PrintDiscontinuedBatch(batchId)

        sql="select d.pcn,d.dateadded,v.claim,dv.owner,ct.court from discontinuedpdf d left join violators v on v.fullpcn=d.pcn  left join dvla dv on dv.id=v.dvlaid left join courttranfers ct on ct.pcn=d.pcn where  d.batchid=" & batchid ' and printeddate is null" ' datesent is null"
        objconn.cursorlocation = 3 ''adUseClient
        set  rs = objconn.execute(sql)

        do while not rs.eof
            pcn = rs("pcn")
            if idebug =  1 then response.Write "<br/>Processing PCN: " & pcn 

            x = printCurrentRecord(rs)
            rs.movenext

            if idebug =  1 then response.Write "<br/>------ Finished Processing PCN: " & pcn & "-----------<br/>"
        loop

        fileName = "disconta_" & batchid & ".pdf"
        objDocfinal.Save  destinationFolder & fileName

        response.write "<br><a href='http://traffica2.cleartoneholdings.co.uk/excelfiless/" & myd & "/" & filename & "' target='_blank'>Click here to open file</a>"
    end function

    function printCurrentRecord(rec)
        spcn = rec("pcn")
        datesent = rec("dateadded")
        owner = rec("owner")
        sclaim = rec("claim")
        court = rec("court")

        set objDoc = Pdf.CreateDocument()

        Set objPage = objDoc.Pages.Add( pageWidth, pageHeight)
        set objTemplate1 = objDoc.CreateGraphicsFromPage( docDiscontinued, 1 )
        objPage.Canvas.DrawGraphics objTemplate1, "x=0; y=0"

        'Create param object to be used throughout application
	    Set objParam = Pdf.CreateParam
        Set objParamNormal = Pdf.CreateParam("alignment=left; color=black; spacing=1.3;")

	    ' Create font objects
	    Set objFont = objDoc.Fonts("Arial") 
        Set regFont = objDoc.Fonts.LoadFromFile("c:\windows\fonts\UKNumberPlate.ttf")

        Set objCanvas = objPage.Canvas

        objPage.Canvas.DrawText spcn &   " BEGIN" , "x=0, y=840; size=12; alignment=left; color=white",objFont

        fontSize = 11

        filename = "d" & spcn & ".pdf"
        Path = destinationFolder & filename

        if idebug = 1 then response.Write "<br>Saving to memory"
        Set objDocBinary = Pdf.OpenDocumentBinary( objDoc.SaveToMemory )

        if idebug = 1 then response.Write "<br>Appending to final"
        objDocfinal.AppendDocument objDocBinary  

        if idebug = 1 then response.Write "<br>Saving to file"
        FileName = objDocBinary.Save( path, true)

        response.write filename

        Set objPage = Nothing
        Set objDoc = Nothing

        if idebug =  1 then response.write "<br>filename: " & filename

        printCurrentRecord = filename

    end function
%>