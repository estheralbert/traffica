
<%
'**************************************************************************************************
' VSP Direct ASP Kit Includes File
'**************************************************************************************************

'**************************************************************************************************
' Change history
' ==============
'
' 13/09/2007 - Mat Peck - New kit version
'**************************************************************************************************
' Description
' ===========
'
' Page with no visible content, but defines the constants and functions used in other pages in the
' kit.  It also opens connections to the database and defines record sets for later use.  It is
' included at the top of every other page in the kit and is paried with the closedown scipt.
'**************************************************************************************************

response.buffer=true
Response.Expires = -1
Response.AddHeader "PRAGMA","NO-CACHE"
Response.CacheControl = "no-cache"

'**************************************************************************************************
' Values for you to update
'**************************************************************************************************
strConnectTo="LIVE" 	'** Set to SIMULATOR for the VSP Simulator expert system, TEST for the Test Server **
							'** and LIVE in the live environment **

strDatabaseUser="protxUser" '** Change this if you created a different user name to access the database **
strDatabasePassword="[your database user password]" '** Set the password for the above user here **

strVirtualDir="VSPDirect-kit" '** Change if you've created a Virtual Directory in IIS with a different name **

	'** IMPORTANT.  Set the strYourSiteFQDN value to the Fully Qualified Domain Name of your server. **
	'** This should start http:// or https:// and should be the name by which our servers can call back to yours **
	'** i.e. it MUST be resolvable externally, and have access granted to the Protx servers **
	'** examples would be https://www.mysite.com or http://212.111.32.22/ **
	'** NOTE: You should leave the final / in place. **
strYourSiteFQDN="http://[your web site]/"  

strVSPVendorName="phoneandpay" '"civil" '** Set this value to the VSPVendorName assigned to you by protx or chosen when you applied **
strCurrency="GBP" '** Set this to indicate the currency in which you wish to trade. You will need a merchant number in this currency **
strTransactionType="AUTHENTICATE" '** This can be DEFERRED or AUTHENTICATE if your Protx account supports those payment types **

'**************************************************************************************************
' Global Definitions for this site
'**************************************************************************************************
strProtxDSN = "DRIVER={MySQL ODBC 3.51 Driver}; SERVER=localhost; DATABASE=protx; " & _
	"UID=" & strDatabaseUser & ";PASSWORD=" & strDatabasePassword & "; OPTION=3" 



'**************************************************************************************************
' Global Definitions for this site
'**************************************************************************************************
strProtocol="2.23"

if strConnectTo="LIVE" then
  strAbortURL="https://live.sagepay.com/gateway/service/abort.vsp"
  strAuthoriseURL="https://live.sagepay.com/gateway/service/authorise.vsp"
  strCancelURL="https://live.sagepay.com/gateway/service/cancel.vsp"
    strPurchaseURL="https://live.sagepay.com/gateway/service/vspdirect-register.vsp"
  'strPurchaseURL="https://live.sagepay.com/gateway/service/vspdirect-register.vsp"
  strRefundURL="https://live.sagepay.com/gateway/service/refund.vsp"
  strReleaseURL="https://live.sagepay.com/gateway/service/release.vsp"
  strRepeatURL="https://live.sagepay.com/gateway/service/repeat.vsp"
  strVoidURL="https://live.sagepay.com/gateway/service/void.vsp"
  str3DCallbackPage="https://live.sagepay.com/gateway/service/direct3dcallback.vsp"
  strPayPalCompletionURL="https://live.sagepay.com/gateway/service/complete.vsp"
elseif strConnectTo="TEST" then
  strAbortURL="https://test.sagepay.com/gateway/service/abort.vsp"
  strAuthoriseURL="https://test.sagepay.com/gateway/service/authorise.vsp"
  strCancelURL="https://test.sagepay.com/gateway/service/cancel.vsp"
  strPurchaseURL="https://test.sagepay.com/gateway/service/vspdirect-register.vsp"
  strRefundURL="https://test.sagepay.com/gateway/service/refund.vsp"
  strReleaseURL="https://test.sagepay.com/gateway/service/abort.vsp"
  strRepeatURL="https://test.sagepay.com/gateway/service/repeat.vsp"
  strVoidURL="https://test.sagepay.com/gateway/service/void.vsp"
  str3DCallbackPage="https://test.sagepay.com/gateway/service/direct3dcallback.vsp"
  strPayPalCompletionURL="https://test.sagepay.com/gateway/service/complete.vsp"
else
  strAbortURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorAbortTx"
  strAuthoriseURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorAuthoriseTx"
  strCancelURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorCancelTx"
  strPurchaseURL="https://test.sagepay.com/simulator/VSPDirectGateway.asp"
  strRefundURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorRefundTx"
  strReleaseURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorReleaseTx"
  strRepeatURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorRepeatTx"
  strVoidURL="https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorVoidTx"
  str3DCallbackPage="https://test.sagepay.com/simulator/VSPDirectCallback.asp"
  strPayPalCompletionURL="https://test.sagepay.com/simulator/complete.asp"
end if


'**************************************************************************************************
' Database Connection
'**************************************************************************************************
dim dbSagepay
dim rsPrimary
dim rsSecondary
dim strSQL

strSagepayDSN = "DRIVER={MySQL ODBC 3.51 Driver}; SERVER=localhost; DATABASE=sagepay; UID=" & strDatabaseUser & ";PASSWORD=" & strDatabasePassword & "; OPTION=3" 

'Open the VPS database
if strDatabasePassword<>"[your database user password]" then
	set dbSagepay=server.createobject("adodb.connection") 
	dbSagepay.CommandTimeOut=180
	dbSagepay.Open strSagepayDSN
end if


%>