  <% Server.ScriptTimeout = 3600000 %>
		<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%
 pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 
   
%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->

<!--#include file="image.asp"-->


<html>
<head>
<style media="print">
.noPrint {display:none;}



</style>
<LINK 
href="adminstylesheet.css" rel=stylesheet type=text/css>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; CHARSET=windows-1255">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">

<%
cmd=request("cmd")
if session("bdebug")="" or session("bdebug")=false then on error resume next
displaymenu=1
if request("cmd")="changeadmin" then displaymenu=0
if request("cmd")="adduser" then displaymenu=0
if request("cmd")="viewreissue" then displaymenu=0
if request("p")="1" then displaymenu=0
'if session("admin")<>"ok" then displaymenu=0


showtinymce=0
'response.Write "cmd:" & cmd
if (cmd="appealletters" or cmd="cancelledletters" or cmd="generalletters") and request("cmd2")="edit" then showtinymce=1
if (cmd="emails" and request("cmd2")="viewemail") then showtinymce=1
'if (cmd="appealletters") and request("cmd2")="edit" then showtinymce=1
if showtinymce=1 then 

 %>
 
 <script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "iespell,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,insertdate,inserttime,preview,|,forecolor,backcolor",
	//theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
	//	theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",
    inline_styles : false,
		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<% end if 
 if session("admin")="ok" and  request("cmd")<>"printpcnletter" and request("cmd")<>"printpcnreminder" and request("cmd")<>"printcancellation"  and request("cmd")<>"shownotes" and request("cmd")<>"pcnnotes" and displaymenu=1 then 

%>
<% if request("cmd")="pcn" then %>
<script type="text/javascript">
window.onload = function k(){document.myform.elements[0].focus()}

</script>

<%end if
%>
 
 
 
<script language="javascript">
<!--//
function confirmreason(which) {

var a = which.value;
var b = which.id;
var n = "deletedreason" + a;
var x = document.getElementById(n).value;
x = x.replace(/^\s+|\s+$/g,"");  // strip leading and trailing spaces;
if (document.getElementById(b).checked == false) {
document.getElementById(n).value = "";
return false;
}
if (x.length <3) {
document.getElementById(n).focus();
}
}

function checkreason(which) {

var x = which.value;
b = which.id;
b = b.replace(/eletedreason/,"")

if (document.getElementById(b).checked == false) {
if (x.length > 0) {
alert ("You must check the checkbox before you enter a reason" );
which.value = "";
return false;
}
}

if (document.getElementById(b).checked) {
x = x.replace(/^\s+|\s+$/g,"");  // strip leading and trailing spaces;
if (x.length < 3) {
alert ("You must enter a reason for deleting this item \(3 characters minimum\)");
which.focus();
return false;
}
}

}

function myPopup(url,windowname,w,h,x,y){
window.open(url,windowname,"resizable=yes,toolbar=no,scrollbars=no,menubar=no,status=no,directories=no,width="+w+",height="+h+",left="+x+",top="+y+"");
}
//-->
</script>
<script type="text/javascript">
function getRandomNum(lbound, ubound) {
return (Math.floor(Math.random() * (ubound - lbound)) + lbound);
}
function getRandomChar(number, lower, upper, other, extra) {
var numberChars = "0123456789";
var lowerChars = "abcdefghijklmnopqrstuvwxyz";
var upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var otherChars = "`~!@#$%^&*()-_=+[{]}\\|;:'\",<.>/? ";
var charSet = extra;
if (number == true)
charSet += numberChars;
if (lower == true)
charSet += lowerChars;
if (upper == true)
charSet += upperChars;
if (other == true)
charSet += otherChars;
return charSet.charAt(getRandomNum(0, charSet.length));
}
function getPassword() {
var rc = "";

rc = rc + getRandomChar(0, 1, 0, 0, 0);
for (var idx = 1; idx < 6; ++idx) {
rc = rc + getRandomChar(1, 0, 0, 0,0);
}
return rc;
}

function checkrequest(str)
{
//alert(str);
if(str=='driver') {
document.getElementById('reasondriver').style.display = '';
document.getElementById('reasonclient').style.display = 'none';
document.getElementById('authby').style.display = '';
document.getElementById('addinfo').style.display = '';
document.getElementById('nocharge').style.display = 'none';
document.getElementById('submit').style.display = '';
}

if (str=='client'){
//alert('client');
document.getElementById('reasonclient').style.display = '';
document.getElementById('reasondriver').style.display = 'none';
document.getElementById('authby').style.display = '';
document.getElementById('addinfo').style.display = '';
document.getElementById('nocharge').style.display = '';
document.getElementById('submit').style.display = '';
}

}
//SuckerTree Horizontal Menu (Sept 14th, 06)
//By Dynamic Drive: http://www.dynamicdrive.com/style/

var menuids=["treemenu1"] //Enter id(s) of SuckerTree UL menus, separated by commas

function buildsubmenus_horizontal(){
for (var i=0; i<menuids.length; i++){
  var ultags=document.getElementById(menuids[i]).getElementsByTagName("ul")
    for (var t=0; t<ultags.length; t++){
		if (ultags[t].parentNode.parentNode.id==menuids[i]){ //if this is a first level submenu
			ultags[t].style.top=ultags[t].parentNode.offsetHeight+"px" //dynamically position first level submenus to be height of main menu item
			ultags[t].parentNode.getElementsByTagName("a")[0].className="mainfoldericon"
		}
		else{ //else if this is a sub level menu (ul)
		  ultags[t].style.left=ultags[t-1].getElementsByTagName("a")[0].offsetWidth+"px" //position menu to the right of menu item that activated it
    	ultags[t].parentNode.getElementsByTagName("a")[0].className="subfoldericon"
		}
    ultags[t].parentNode.onmouseover=function(){
    this.getElementsByTagName("ul")[0].style.visibility="visible"
    }
    ultags[t].parentNode.onmouseout=function(){
    this.getElementsByTagName("ul")[0].style.visibility="hidden"
    }
    }
  }
}

if (window.addEventListener)
window.addEventListener("load", buildsubmenus_horizontal, false)
else if (window.attachEvent)
window.attachEvent("onload", buildsubmenus_horizontal)
function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
    }

</script>


<% End If %>

<script language="Javascript1.2"><!-- // load htmlarea
_editor_url = "";                     // URL to htmlarea files
_editor_field = "";
var win_ie_ver = parseFloat(navigator.appVersion.split("MSIE")[1]);
if (navigator.userAgent.indexOf('Mac')        >= 0) { win_ie_ver = 0; }
if (navigator.userAgent.indexOf('Windows CE') >= 0) { win_ie_ver = 0; }
if (navigator.userAgent.indexOf('Opera')      >= 0) { win_ie_ver = 0; }
if (win_ie_ver >= 5.5) {
  document.write('<scr' + 'ipt src="' +_editor_url+ 'editorl.js"');
  document.write(' language="Javascript1.2"></scr' + 'ipt>');        
} else { document.write('<scr'+'ipt>function editor_generate() { return false; }</scr'+'ipt>'); }
var MyBgColor ='#FFFFFF';
var MyBgImg ='';
// --></script>
<script>
function updateAttributes(){
document.myform.bgcolor.value=MyBgColor;
document.myform.background.value=MyBgImg;
}
</script>
<script type="text/javascript" src="validate.js"></script>
<script language="JavaScript" src="gen_validatorv2.js" type="text/javascript"></script>
<% if cmd="emails" then %>
<script language="JavaScript" src="sorttablet.js"></script>

<% else %>
<script language="JavaScript" src="sorttable2.js"></script>
<% end if %>

<script language="javascript" type="text/javascript" src="datetimepicker.js">

//Date Time Picker script- by TengYong Ng of http://www.rainforestnet.com
//Script featured on JavaScript Kit (http://www.javascriptkit.com)
//For this script, visit http://www.javascriptkit.com 

</script>


<script langauge=javascript>
function checkValue(e) {
	var val;
	if(typeof e.which != 'undefined') {
		val = e.which;
	}
	else {
		val = e.keyCode;
	}
	//alert(e.which);
	switch (val) {
		case 0:  //navigation keys in Mozilla
		case 8:  //backspace
		case 13: //enter
		case 46: //. (period)
		case 48: //0
		case 49: //1
		case 50: //2
		case 51: //3
		case 52: //4
		case 53: //5
		case 54: //6
		case 55: //7
		case 56: //8
		case 57: //9
			return true;
		default:
			return false;
	}
}

 function getNumSelBoxes(objForm, prefix){
    var counter = 0;
    for(var i=0; i<objForm.length; i++){
       if(objForm.elements[i].type == 'select-one' && objForm.elements[i].name.indexOf(prefix)==0){
         counter++;
       }
     }
     return counter;
  }

  function setSelectBoxessite(obj){    
    var selNamePrefix = 'site';
     var numSelBoxes = getNumSelBoxes(obj.form, selNamePrefix);
    var index = obj.name.split(selNamePrefix)[1] * 1;
     for(var i=index; i<=numSelBoxes; i++){
       obj.form.elements[selNamePrefix + i].selectedIndex = obj.selectedIndex;       
     }
  }
  function setSelectBoxestype(obj){    
    var selNamePrefix = 'type';
     var numSelBoxes = getNumSelBoxes(obj.form, selNamePrefix);
    var index = obj.name.split(selNamePrefix)[1] * 1;
     for(var i=index; i<=numSelBoxes; i++){
       obj.form.elements[selNamePrefix + i].selectedIndex = obj.selectedIndex;       
     }
  }
  function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
}
function PaymentsDelete(i){ 

myForm=document.forms["payments"] 

//if(myForm["deleteme"].checked){ 

myField="amount"+i; 
myForm[myField].value=0; 
//} 

}  

function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=1,statusbar=0,menubar=0,resizable=1,width=600,height=600,left = 100,top = 0');");
}

</script>
<script language="javascript" type="text/javascript">
<!--
function popitup(url) {
	newwindow=window.open(url,'name','height=700,width=500');
	if (window.focus) {newwindow.focus()}
	return false;
}

// -->
</script>
<% if request("cmd")="showplateimagesnew" then
 %>
  <!-- calendar stylesheet -->
  <link rel="stylesheet" type="text/css" media="all"
href="jscalendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />

  <!-- main calendar program -->
  <script type="text/javascript" src="jscalendar/calendar.js"></script>

  <!-- language for the calendar -->
  <script type="text/javascript" src="jscalendar/lang/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="jscalendar/calendar-setup.js"></script>
<%end if %>
</head>
<body>

<% If request("cmd")<>"dvlareport" Then %>
<div align=center>
<% End If 
if request("cmd")<>"printpcnletter" and request("cmd")<>"printpcnreminder" and request("cmd")<>"printcancellation"  and request("cmd")<>"shownotes" and request("cmd")<>"pcnnotes" and request("cmd")<>"dvlareport" and displaymenu=1 then %>
<span class=noPrint>
<div id=header><img src="logo.jpg"  border="0" class=logo>
<%if session("admin")="ok" then %>
<div class=hright><a href="admin.asp?cmd=adminlogout" class=button>Log Out</a>
</div>
<div class=welcome>
Welcome <%= session("username") %>
</div>
<div class=pcnheader>

<form method=post action=admin.asp?cmd=search2>
Enter PCN/Plate:<input type=text name=pcn><input type=submit  value="Go" class=button>
</form>


</div>
<%end if%>
</div>
</span>
<% 
end if
if session("admin")="ok" and showadminmenu<>1 then
	 call adminmenu
	showadminmenu=1
end if%>
<% if request("cmd")<>"printpcnletter" and request("cmd")<>"printpcnreminder" and request("cmd")<>"printcancellation"  and request("cmd")<>"shownotes" and request("cmd")<>"pcnnotes" and request("cmd")<>"dvlareport" and displaymenu=1 then %>
<span class=noPrint>
<!--<img src="logo.jpg" width="153" height="91" border="0">-->
</span>
<% 
end if
allowaccess=0
ip=Request.ServerVariables("REMOTE_ADDR")
sqlck="select enabled from ipaccesssecurity"
openrs rsck,sqlck
if rsck("enabled")=true then
	'response.write "enabled"
	cip=Request.ServerVariables("REMOTE_ADDR") 
	sqlip="select * from ipaddresses where ipaddress=" & tosql(cip,"text")
	openrs rsip,sqlip
		if not rsip.eof and not rsip.bof then allowaccess=1
	closers rsip
else
allowaccess=1
end if
closers rsck
showadminmenu=0
if allowaccess=0 then response.end
cmd=request("cmd")

'and cmd<>"search" then response.write "<br><br><a href=admin.asp?cmd=search class=button>Start a New Search</a><br><br>"
'response.write cmd
select case cmd
'case "adminmenu"
'	call adminmenu
case "validate"
	call validatelogin
case "permits"
	call checklogin
	call permits
case "pcn"
	call checklogin
	call pcn
case "payments"
	call checklogin
	call payments
case "reports"
	call checklogin
	call reports
case "search"
	call checklogin
	call search
case "search2"
	call checklogin
	call search2
case "viewrecord"
	call checklogin
	call viewrecord(request("id"))
case "printpcnletter"
	
	call checklogin
	call printpcnletter
case "printpcnreminder"

	call checklogin
	call printpcnreminder
case "anmonthlyex2"
		 call anmonthlyex2
case "viewimages"

	call checklogin
	call viewimages	
case "paymentssort"

	call checklogin
	call paymentssort
case "graph"
	call checklogin
	call graph
case "graphoptions"
	call checklogin
	call graphoptions
case "paymentrepexcel"
		 call checklogin
		 call paymentrepexcel	
case "ppsendsms"
call checklogin
call ppsendsms
case "adminlogout"

	
	call adminlogout
case "extractpermits"
    call checklogin
    call extractpermits
	
case "dvlasearch2"
		 call dvlasearch2
		 
case "printtempviolators"
		 call checklogin
		 call printtempviolators
case "maddpermit"
	call checklogin
	if session("profile")=1 then call maddpermit1
	if session("profile")=2 then call maddpermit2
	if session("profile")=3 then call maddpermit3
case "menable-disabletemporary"
	call checklogin
	call checkrights("enable-disabletemporary")
	call menabledisabletemporary
case "menable-disablepermanent"
	call checklogin
	call checkrights("enable-disablepermanent")
	call menabledisablepermanent

case "mremovesiteselection"
	call checklogin
	call checkrights("removesiteselection")
	call mremovesiteselection
case "msearchpermit"
	call checklogin
	call checkrights("searchpermit")
	call msearchpermit
case "mviewexpiredpermit"
	call checklogin
	call checkrights("viewexpiredpermit")
	call mviewexpiredpermit
case "mviewcurrentpermit"
	call checklogin
	call checkrights("viewcurrentpermit")
	call mviewcurrentpermit
case "mdeletepermit"
	call checklogin
	call checkrights("deletepermit")
	call mdeletepermit
case "viewappeal"
	call checklogin
	call viewappeal
	case "viewappealbypcn"
	call checklogin
	call viewappealbypcn
case "updateappeal"
	call checklogin
	call updateappeal

case "printcancelledletter"
	call checklogin
	call printcancelledletter	
case "viewpcnappeal"
	call checklogin
	call viewpcnappeal
case "viewappeal"
	call checklogin
	call viewappeal
	case "markdatarequired"
	call checklogin
	call markdatarequired
	
case "viewrm"
	call checklogin
	call viewrm
case "viewpermit"
	call checklogin
	call viewpermit(request("id"))
case "enabletemp"
	call checklogin
	call enabletemp
case "disabletemp"
	call checklogin
	call disabletemp	
case "enableperm"
	call checklogin
	call enableperm
case "disableperm"
	call checklogin
	call disableperm
case "setsitealloff"
	call checklogin
	call setsitealloff
case "setsiteallon"
	call checklogin
	call setsiteallon
case "analyze"
	call checklogin
	call analyze
case "uploadanalyze"
	call checklogin
	call uploadanalyze
case "viewplateimages"
	call checklogin
	call viewplateimages
case "saveplateimage"
	call checklogin
	call saveplateimage
case "cancelform"
	call checklogin
	call cancelform
case "dvlafind2"
		 call checklogin
		 call dvlafind
		 case "searchpermitex"
		 call checklogin
		 call searchpermitex
		  case "viewpermitdetails"
		 call checklogin
		 call viewpermitdetails
		  case "viewexemptiondetails"
		 call checklogin
		 call viewexemptiondetails
		case "ktranscribe"
		 call checklogin
		 call ktranscribe
		 
case "showcancelform"
	call checklogin
	call showcancelform(request("id"))
case "savecancelform"
	call checklogin
	call savecancelform
case "printpcnsummary"
	call checklogin
	call printpcnsummary
case "printcancellation"
	call checklogin
	call printcancellation
case "printbatchcancel"
	call checklogin
	call printbatchcancel
case "summarycancel"
	call checklogin
	call summarycancel
case "deltables"
	call checklogin
	call deltables
case "editdvla"
		 call checklogin
		 	call editdvla
case "printcancelpcn"
	call checklogin
	call printcancelpcn
case "pcnnotes"
	call checklogin
	call pcnnotes
case "shownotes"
	call checklogin
	call shownotes(request("pcn"))
case "searchnotes"
	call checklogin
	call searchnotes
case "dvlareport"
	'call checklogin
	call dvlareport
	case "dvlareportupl"
	call checklogin
	call dvlareportupl
case "dvlareportupl2"
	call checklogin
	call dvlareportupl2
case "dvlarepfile"
	call checklogin
	call dvlarepfile
case "viewviolatorticket"
    call checklogin
    call viewviolatorticket
case "triangletranscription"
    call checklogin
    call triangletranscription  
    case "markdatarequired"
    call checklogin
    call markdatarequired 
case "listentriangle"
    call checklogin
    call listentriangle
    case "listentrianglek"
    call checklogin
    call listentrianglek
case "plateimagesfuzzy"
    call checklogin
    call plateimagesfuzzy	
case "plateimagesfuzzyex"
    call checklogin
    call plateimagesfuzzyex	
 case "pvlist"
    call checklogin2("analysisreports")
    call  pvlist
 case "pvlistfuzzy"
      call checklogin2("analysisreports")
    call pvlistfuzzy     
case "plateimagessingle"
    call checklogin
    call plateimagessingle	
    
case "outstandingappeals"
    call checklogin
    call outstandingappeals	    
case "s036exemptionssingle"
    call checklogin
    call s036exemptionssingle	
case "viewunmatched"
    call checklogin
    call viewunmatched	
        
    
case "dvlarepfile2"
	call checklogin
	call dvlarepfile2
	case "pptransactions"
	    call checklogin
	    call pptransactions
	
	case "exemptionsnoplates"
	call checklogin
	call exemptionsnoplates
	case "dvlarepsearch"
	call checklogin
	call dvlarepsearch
	case "printreminders"
		 call checklogin2("printpcn")
		 call printreminders
	case "printdebtrecovery"
		 call checklogin2("printpcn")
		 call printdebtrecovery
	case "printdebtrecoverysummary"
			 call checklogin2("printpcn")
			 call printdebtrecoverysummary
	case "reprintreminders"
		 call checklogin2("printpcn")
		 call reprintreminders
			case "reprintreminderstoexcel"
		 call checklogin2("printpcn")
		 call reprintreminderstoexcel
	case "reprintdebtrecovery"
		 call checklogin2("printpcn")
		 call reprintdebtrecovery
			case "reprintdebtrecoverytoexcel"
		 call checklogin2("printpcn")
		 call reprintdebtrecoverytoexcel
		 case "cb"
		    call checklogin2("cancellationbreakdown")
		    call cb 
case "printremindersummary"
		 call checklogin2("printpcn")
		 call printremindersummary
case "deletepcn"
		 call checklogin2("deletepcn")
		 call deletepcn
		case "triangleapprove"
		 call checklogin2("triangleapprove")
		 call triangleapprove
		 
		 case "trianglestatus"
		 call checklogin2("triangleapprove")
		 call trianglestatus
case "printreissuesummary"
		 call checklogin
		 call printreissuesummary
case "dvlasearch"
	call checklogin
	call dvlasearch
case "dvlafind"
	call checklogin
	call dvlafind
case "viewdelviolator"
call checklogin
call viewdelviolator
case "viewtempviolator"
call checklogin
call viewtempviolator
case "violatorupload"
		 call checklogin
		 call violatorupload
	case "summary"
	call checklogin
	call summary
case "dvlarepreprint"
	call checklogin
	call dvlarepreprint
	case "delpcnpermits"
			 call checklogin
			 	call delpcnpermits
	case "superadmin"
	call checklogin
	call superadmin
		case "changeadmin"
	call checklogin
	call changeadmin
	
case "showplateimagesnew"
		 call checklogin
		 call showplateimagesnew
	case "adduser"
	call checklogin
	call adduser
	case "deluser"
	call checklogin
	call deluser
	case "test"
	call test
	case "createletter"
			 call checklogin
			 call createletter
	case "violatortoexcel"
			 call checklogin
			 call violatortoexcel
	case "reprintticketsexcel"
			 call checklogin
			 call reprintticketsexcel		
	case "dvlaunknown"
			 call checklogin
			 			call dvlaunknown 
	case "searchdvla"
			 call checklogin
			 call searchdvla
	case "tempviolatorupload"
	call checklogin
	call tempviolatorupload
	case "viewreissue2"
	call checklogin
	call viewreissue2
	case "extract087"
	call checklogin
	call extract087
	case "reissueupload"
	call checklogin
	call reissueupload
	case "remindersupload"
	call checklogin
	call remindersupload
	case "generalletters"
			 call checklogin
			 call generalletters

	case "dvlaupl"
			 call checklogin
			 call dvlaupl
	case "emailadmin"
	call checklogin
	call emailadmin
	case "dvlareportnew"
	call checklogin
	call dvlareportnew
	case "viewcancelledletter"
	call checklogin
	call viewcancelledletter
	case"viewletter"
		call checklogin
		call viewletter
	case "viewreissue"
	call checklogin
	call viewreissue
	case "outstandingdvla"
	call checklogin
	call outstandingdvla	
	case "siteadmin"
	call checklogin
	call siteadmin
	case "viewdelviolators"
	call checklogin
	call viewdelviolators
case "siteoverview"
	call checklogin
	call siteoverview
case "sitedetailsoverview"
		 call checklogin
		 call sitedetailsoverview
	case "cancelledletters"
	call checklogin
	call cancelledletters
case "uploadanalysis"
		 call checklogin
		 call uploadanalysis
case "analysisstage1"
		 call checklogin
		 call analysisstage1
case "analysisstage2"
		 call checklogin
		 call analysisstage2
case "anreportbyemp"
		 call checklogin
		 call anreportbyemp
case "showfuzzy"
		 call checklogin
		 		call showfuzzy
case "analysisreport"
		 call checklogin
		 call analysisreport
case "ansitenodata"
		 call checklogin
		 call ansitenodata
case "anprintstage1"
		 call checklogin
		 call anprintstage1
case "anprintstage2"
		 call checklogin
		 call anprintstage2
case "anweekly"
		 call checklogin
		 call anweekly		 
case "analerts"
		 call checklogin
		 call analerts	
case "anpdf"
		 call checklogin	 
		 call anpdf
case "anpdf2"
		 call checklogin	 
		 call anpdf2
case "anexcel"
		 call checklogin	 
		 call anexcel
case "anexcel2"
		 call checklogin	 
		 call anexcel2
case "cancelanal"
		 call checklogin
		 	call cancelanal
		case "analysistobecopied"
		 call checklogin	 
		 call analysistobecopied
		case "adminhome"
		 call checklogin	 
		 call adminhome
				case "printpcninit"
		 call checklogin	 
		 call printpcninit
		 		case "exporttable"
		 call checklogin	 
		 call exporttable
		 		case "dvlafiletemp"
		 call checklogin	 
		 call dvlafiletemp
		 		case "dvlawaiting"
		 call checklogin	 
		 call dvlawaiting
		 case "andvlawait"
  		 call checklogin	 
  		 call andvlawait
		 case "andvlawaitsummary"
		 call checklogin	 
		 call andvlawaitsummary
		  case "sscr"
		 call checklogin	 
		 call sscr
		 case "pcnnotesexcel"
		 			call checklogin
					call pcnnotesexcel
		case "dvlarepreprintbatch"
				 call dvlarepreprintbatch 
		case "dvlasend"
				 call dvlasend
		case "confirmanalysis"
				 call checklogin
				 call confirmanalysis
		case "anmonthlyex"
		call checklogin
		call anmonthlyex
	case "adddvla"
			call checklogin
			 call adddvla
	case "dvlasearchbyid"
			 call checklogin
			 call dvlasearchbyid
	case "dvlasearchbyletter"
			 call checklogin
			 call dvlasearchbyletter
	case "dvlaexcel"
			 call dvlaexcel
			case "tarrifexcel"
					  call checklogin
					 call tarrifexcel
			case "sitedetailexcel"
					  call checklogin
					 call sitedetailexcel
	case "weeklyoverview"
			 call checklogin
			 call weeklyoverview
		case "plateanalysis"
				 call checklogin
				 call plateanalysis
	case "explateanalysis"
				 call checklogin
				 call explateanalysis
		case "platereport"
				 call checklogin
				 call platereport
		case "backupdb"
				 call checklogin
				 	call backupdb
		case "paymentstoexcel"
				 call checklogin
				 	call paymentstoexcel
		case "cancellationtoexcel"
				 call checklogin
	
				 call cancellationtoexcel
	case "showplateimages"
				 call checklogin
	
				 call showplateimages
case "permitsynch"
		 call checklogin
		 call permitsynch
case "markspermitexcel"
		 call checklogin2("markspermitexcel")
		 	call markspermitexcel
	case "reprinttickets"
		 call checklogin2("printpcn")
		 	call reprinttickets
					
			case "editpayments"
		 call checklogin2("payments")
		 	call editpayments
			
		case "appealpara"
				 call checklogin2("appeals")
				 call appealpara
		case "appealletters"
				 call checklogin2("appeals")
				 call appealletters
		case "appealconfig"
				 call checklogin2("appeals")
				 	call appealconfig
		case "viewappealletter"
		        ' response.write "here"
				 call checklogin
				 
				 call viewappealletter
		case "fintodays"
				 call checklogin
				 call fintodays
					case "reissues"
				 call checklogin2("reissues")
				 	call reissues
		case "incomingletters"
				 call checklogin2("incomingletter")
				 	call incomingletters	
					case "reprintreissues"
					call checklogin
					call reprintreissues
						
		case "searchincomingletters"
				 	call checklogin2("incomingletter")
				 	call searchincomingletters	
			case "outstandingincomingletters"
				 	call checklogin2("incomingletter")
				 	call outstandingincomingletters	
		case "marknoresponse"
				 call checklogin
				 call marknoresponse
			case "dvlatrans"
			call checklogin2("dvlatransactions")
			call dvlatrans
			case "changeletterstatus"
				 call checklogin
				 call changeletterstatus
				case "markcancelled"
				 call checklogin
				 call markcancelled
				case "markreissue"
				call checklogin
				call markreissue	 
				case "markreissuetobeconsidered"
				call checklogin
				call markreissuetobeconsidered	 
				case "declinereport"
			call checklogin2("declinedpayments")
			call declinereport
				case "triangleexemptions"
			call checklogin
			call triangleexemptions
			case "triangleexemptionsexcel"
			call checklogin
			call triangleexemptionsexcel
			case "deltmpplateimages"
			        call checklogin
			    call deltmpplateimages
			  case "changetriangledate"
			    call checklogin
			    call changetriangledate
			  case "searchexemptions"
			    call checklogin
			    call searchexemptions
			     case "searchpermits"
			    call checklogin
			    call searchpermits
			    case "rivergateexemptions"
			call checklogin
			call rivergateexemptions
			 case "analysispv"
			call checklogin
			call analysispv
			case "confirmanalysisall"
			    call checklogin
			    call confirmanalysisall
			    
			    case "addccpayment"
			        call checklogin2("addccpayment")
			        call addccpayment
			        case "siteissues"
				 call checklogin2("siteissues")
				 call siteissues
				 case "searchtriangleexemption"
				    call checklogin
				    call searchtriangleexemption
				       case "addpermituser"
				 call checklogin2("addpermituser")
				 call addpermituser
				  case "permitusers"
				 call checklogin2("addpermituser")
				 call permitusers
				 case "reportvsiteweek"
				 call checklogin2("reports")
				 
				 call reportvsiteweek
				  case "cancelledsummary"
				 call checklogin2("reports")
				 
				 call cancelledsummary
				 case "upload070"
				 call checklogin
				 call upload070
				 case "registerflats"
				 call checklogin2("registerflats")
				 call registerflats
				 case "blackoutdays"
				 call checklogin2("blackoutdays")
				 call blackoutdays
				 case "emails"
				 call checklogin
				 call subemails
case else
	call loginform
end select


sub checklogin
if session("admin")<>"ok" then
'
'response.Write "amdin not ok"
call loginform
response.end
end if
end sub

sub checklogin2(accessstr)
if session("admin")<>"ok" then
call loginform
response.end
else

		sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='" & accessstr & "'"
	openrs rsm2,sqlm2
	if  rsm2.eof and  rsm2.bof then
			response.write "no access"
			response.end
	end if
end if
end sub
function checkmenu(accessstr)
checkmenu=false
		sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='" & accessstr & "'"
	openrs rsm2,sqlm2
	if  not rsm2.eof and not rsm2.bof then
			checkmenu=true
	end if
    closers rsm2
end function


sub adminmenu
 if request("cmd")<>"printpcnletter" and request("cmd")<>"printpcnreminder" and request("cmd")<>"printcancellation"  and request("cmd")<>"shownotes"  and request("cmd")<>"pcnnotes"  and request("cmd")<>"dvlareport" and displaymenu=1 then %>
<span class=noPrint>
<div class="suckertreemenu">

<ul id="treemenu1">
	 
      
	<%
	
	'check permission and display menu
	sqlm="select * from usermenu where userid=" & session("userid") & " and menu='permits'"
'	response.write sqlm
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then 
	 %>
      <li><a href=admin.asp?cmd=maddpermit>Permits</a> 
        <ul>
          <% sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='addpermit'"
	openrs rsm2,sqlm2
	if not rsm2.eof and not rsm2.bof then %>
          <li><a href=admin.asp?cmd=maddpermit>Add Permit</a></li>
          <% End If 
	closers rsm2
	%>
          <% sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='searchpermit'"
	openrs rsm2,sqlm2
	if not rsm2.eof and not rsm2.bof then %>
          <li><a href=admin.asp?cmd=msearchpermit>Search Permit</a></li>
          <% End If 
	closers rsm2
	%>
          <% sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='viewexpiredpermit'"
	openrs rsm2,sqlm2
	if not rsm2.eof and not rsm2.bof then %>
          <li><a href=admin.asp?cmd=mviewexpiredpermit>View Expired Permit</a></li>
          <% End If 
	closers rsm2
	%>
          <% sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='viewcurrentpermit'"
	openrs rsm2,sqlm2
	if not rsm2.eof and not rsm2.bof then %>
          <li><a href=admin.asp?cmd=mviewcurrentpermit>View Current Permit</a></li>
          <% End If 
	closers rsm2
	%>
	
	    <li><a href=admin.asp?cmd=searchpermitex>Search Permit & Exemptions</a></li>
	
        </ul>
        <%  closers rsm
   End If %>
        <% 
	'check permission and display menu
	sqlm="select * from usermenu where userid=" & session("userid") & " and menu='pcn'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then 
	 %>
      <li><a href=admin.asp?cmd=pcn>PCN</a> 
        <ul>
          
        <li><a href=admin.asp?cmd=pcn>PCN</a></li>
          
        <li><a href=admin.asp?cmd=searchnotes>Search PCN Notes</a></li>
				<%sqlmp="select * from usermenu where userid=" & session("userid") & " and menu='deletepcn'"
	openrs rsmp,sqlmp
	if not rsmp.eof and not rsmp.bof then 
			%>		
				
        <li><a href="admin.asp?cmd=deletepcn">Delete Violator</a></li>	
				
		<% 
		end if
		closers rsmp %>		
				
					<%sqlmp="select * from usermenu where userid=" & session("userid") & " and menu='printpcn'"
	openrs rsmp,sqlmp
	if not rsmp.eof and not rsmp.bof then 
			%>		
				 
        <li><a href="admin.asp?cmd=reprinttickets">Reprint PCN Tickets</a></li>
						
        <li><a href="admin.asp?cmd=reprintticketsexcel">Reprint PCN Tickets to 
          excel</a></li>
						
      
				
        <li><a href="admin.asp?cmd=reprintreminders">Reprint Reminders</a></li>
			
        <li><a href="admin.asp?cmd=reprintreminderstoexcel">Reprint Reminders 
          to excel</a></li>
					
					
        <li><a href="admin.asp?cmd=reprintdebtrecovery">Reprint Debt Recovery</a></li>
			
        <li><a href="admin.asp?cmd=reprintdebtrecoverytoexcel">Reprint Debt Recovery 
          to excel</a></li>
					<%
					end if
					%>
					<%sqlmp="select * from usermenu where userid=" & session("userid") & " and menu='reissues'"
	openrs rsmp,sqlmp
	if not rsmp.eof and not rsmp.bof then 
			%>		
					
        <li><a href="admin.asp?cmd=reissues">Reissue Tickets</a> </li>
          <li><a href="admin.asp?cmd=reprintreissues">Reprint Reissues</a></li>
          <%
					end if
					%>
          <%sqlmp="select * from usermenu where userid=" & session("userid") & " and menu='incomingletter'"
	openrs rsmp,sqlmp
	if not rsmp.eof and not rsmp.bof then 
			%>
        <li><a href="admin.asp?cmd=incomingletters">Incoming Letters</a></li>
					
        <li><a href="admin.asp?cmd=searchincomingletters">Search Incoming Letters</a></li>
					
        <li><a href="admin.asp?cmd=outstandingincomingletters">Outstanding Letters</a></li>
        			
        <li><a href="admin.asp?cmd=outstandingappeals">Outstanding Appeals</a></li>
					<%
					end if
					%>
					
					       					<%sqlmp1="select * from usermenu where userid=" & session("userid") & " and menu='declinedpayments'"
	openrs rsmp1,sqlmp1
	if not rsmp1.eof and not rsmp1.bof then 
			%>		
					
        <li><a href="admin.asp?cmd=declinereport">Declined Payments Report</a></li>
				
					<%
					end if
					%>
       <%sqlmp1="select * from usermenu where userid=" & session("userid") & " and menu='viewdeletedviolators'"
	openrs rsmp1,sqlmp1
	if not rsmp1.eof and not rsmp1.bof then 
			%>		
					
        <li><a href="admin.asp?cmd=viewdelviolators">View Deleted Violators</a></li>
				
					<%
					end if
					%>
       
      </li>
    
  <% 
  closers rsmp1
 %>
					
					
        </ul>
      </li>
  <% 
  closers rsmp
  End If %>


  
<%sqlm="select * from usermenu where userid=" & session("userid") & " and (menu='payments' or menu='reports' or menu='triangledates'or menu='siteissues' or menu='cancellationbreakdown' or menu='triangleapprove')"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then %>

      <li><a href=#>Financial</a> 
        <ul>
          <% 
	'check permission and display menu
	sqlm="select * from usermenu where userid=" & session("userid") & " and menu='payments'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then 
	 %>
          <li><a href=admin.asp?cmd=payments>Enter Payments</a></li>
          <li><a href=admin.asp?cmd=editpayments>Edit Payments</a></li>
          <% closers rsm
	 End If %>
          <% 
	'check permission and display menu
	sqlm="select * from usermenu where userid=" & session("userid") & " and menu='reports'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then 
	 %>
          <li><a href=admin.asp?cmd=reports>View Reports</a></li>
          <li><a href=admin.asp?cmd=graph>View Graph</a></li>
          <li><a href="admin.asp?cmd=siteoverview">Site Overview Report</a></li>
          <li><a href="admin.asp?cmd=weeklyoverview">Weekly Overview</a></li>
          <li><a href=admin.asp?cmd=fintodays>Todays Overview</a></li>
          <li><a href="admin.asp?cmd=reportvsiteweek">Site Violators by week Report</a></li>
        <li><a href="admin.asp?cmd=cancelledsummary">Summary of Cancelled</a></li>
       
          <%closers rsm
	 End If
	 sqlm="select * from usermenu where userid=" & session("userid") & " and menu='triangledates'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then 
	 %>
	    <li><a href=admin.asp?cmd=changetriangledate>Change Triangle Dates</a></li>
	 
	 <%
	 end if
	 closers rsm
	  sqlm="select * from usermenu where userid=" & session("userid") & " and menu='triangleapprove'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then 
	 %>
	    <li><a href=admin.asp?cmd=triangleapprove>Change Triangle to Success</a></li>
	    <li><a href=admin.asp?cmd=trianglestatus>Change Triangle Status</a></li>
	 
	 <%
	 end if
	 closers rsm
	
	 sqlm="select * from usermenu where userid=" & session("userid") & " and menu='siteissues'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then 
	 %>
	    <li><a href=admin.asp?cmd=siteissues>Site Issues</a></li>
	 
	 <%
	 end if
	 closers rsm
	 
	
	 
	 
	 sqlm="select * from usermenu where userid=" & session("userid") & " and menu='cancellationbreakdown'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then 
	 %>
	    <li><a href="admin.asp?cmd=cb">Cancellation Breakdown</a></li>
	 
	 <%
	 end if
	 closers rsm
	 
	 %>
	 
	 
	 
	 
        </ul>
      </li> 
	   <% 'closers rsm
	 End If %>
 <%sqlm="select * from usermenu where userid=" & session("userid") & " and (menu='analyze' or menu='platecheck' or menu='registerflats')"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then %>	  
	  <li><a href=#>Plates</a> 
        <ul>
          <% sqlm="select * from usermenu where userid=" & session("userid") & " and menu='analyze'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then %>
	<li> <a href="javascript:myPopup('pimagesoverview.asp', 'Plate Images overview','400','400','100','100')">Plate Images Overview</a></li>
	 
	  <li><a href=admin.asp?cmd=triangleexemptions> Exemptions</a></li> 
	 
	   <li><a href=admin.asp?cmd=triangleexemptionsexcel>Triangle Exemptions Excel</a></li>
<li><a href=admin.asp?cmd=rivergateexemptions>Insert Rivergate Exemptions</a></li> 
	 
          <!--<li><a href=admin.asp?cmd=plateanalysis>Plate Analysis</a></li>-->
          <li><a href=admin.asp?cmd=platereport>Plate Report</a></li>
            <li><a href=admin.asp?cmd=exemptionsnoplates>Exemptions with no Plate images</a></li>
          <%closers rsm
	 End If %>
	 
	    <% sqlm="select * from usermenu where userid=" & session("userid") & " and menu='071exemptions'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then %>
	 
	 <li><a href=admin.asp?cmd=explateanalysis>Extract 071  Exemption Plates</a></li>
	  <%
	 
	 closers rsm
	  End If %>
	 
	 
          <% sqlm="select * from usermenu where userid=" & session("userid") & " and menu='platecheck'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then %>
	 <!-- <li><a href=admin.asp?cmd=plateimagessingle>View Plate Images (single plates)</a></li>-->
	   <li><a href=admin.asp?cmd=s036exemptionssingle>View 036 Exemptions (single plates)</a></li>
	 <!-- <li><a href=admin.asp?cmd=plateimagesfuzzy>View Plate Images with Fuzzies</a></li>-->
	   <li><a href=admin.asp?cmd=plateimagesfuzzyex>View 036 Plate Images with Fuzzies Exemptions</a></li>
         <!-- <li><a href=admin.asp?cmd=viewplateimages>View Plate Images</a></li>
          <li> <a href=viewplateimages.asp target=_new>View Plate Images new</a></li>-->
          <!--						<li> <a href="javascript:myPopup('viewplateimages.asp', 'View Plate Images','800','600','1','1')">View Plate Images new</a></li>-->
         
          <%
	 
	 closers rsm
	  End If %>
	   <% sqlm="select * from usermenu where userid=" & session("userid") & " and menu='registerflats'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then %>
	 
	 <li><a href=admin.asp?cmd=registerflats>Register Flats</a></li>
	  <%
	 
	 closers rsm
	  End If %>
	    <li><a href=admin.asp?cmd=triangletranscription> Transcription</a></li>
	    <li><a href=admin.asp?cmd=pptransactions> Phone and Pay Transactions</a></li>
	     <li><a href=admin.asp?cmd=searchexemptions>Search Exemptions</a></li>
	        <li><a href=admin.asp?cmd=searchpermits>Search Permits</a></li>
	       <li><a href=admin.asp?cmd=searchtriangleexemption>Search Triangle Exemptions</a></li>
	      <% sqlm="select * from usermenu where userid=" & session("userid") & " and menu='087extractplateimages'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then %>
	 
	 <li><a href=admin.asp?cmd=extract087>Extract 087 plate images from file</a></li>
	  <%
	 
	 closers rsm
	  End If %>
	  
	  <li><a href=admin.asp?cmd=extractpermits target=_new>Extract Permits</a></li>
	  
	  <li><a href=admin.asp?cmd=viewunmatched>View Unmatched</a></li>
        </ul>
      </li> 
	 <%
	 
	' closers rsm
	  End If %>
 <%sqlm="select * from usermenu where userid=" & session("userid") & " and (menu='issuecancel' or menu='printcancel')"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then %>	  
	  <li><a href=#>Cancellation</a> 
        <ul>
          <% sqlm="select * from usermenu where userid=" & session("userid") & " and menu='issuecancel'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then %>
          <li><a href=admin.asp?cmd=cancelform&id=<%= session("userid") %>>Issue 
            Cancellation</a></li>
          <%closers rsm
	 End If %>
          <% sqlm="select * from usermenu where userid=" & session("userid") & " and menu='printcancel'"
	openrs rsm,sqlm
	if not rsm.eof and not rsm.bof then %>
          <li><a href=admin.asp?cmd=printbatchcancel>Print Cancellation Forms</a></li>
          <li><a href=admin.asp?cmd=summarycancel>Summary of Cancellations</a></li>
          <%
	 
	 closers rsm
	  End If %>
        </ul>
      </li> 
      
     
      
      
      
	 <%
	 
	' closers rsm
	  End If %>
		
		
			<% sqlm2="select * from usermenu where userid=" & session("userid") & " and (menu='analysisstage1' or menu='analysisstage2')"
	openrs rsm2,sqlm2
	if not rsm2.eof and not rsm2.bof then %>
	  <li><a href="#">Anaylsis</a> 
        <ul>
           <li><a href="admin.asp?cmd=upload070">Upload 070</a></li>
          <li><a href="admin.asp?cmd=analysistobecopied">Analysis - Copy Files</a></li>
          <li><a href="admin.asp?cmd=analysispv">Analysis -Extract Excels</a></li>
          <%sqlma="select * from usermenu where userid=" & session("userid") & " and menu='analysisstage1'"
	openrs rsma,sqlma
	if not rsma.eof and not rsma.bof then
	%>
          <li><a href="admin.asp?cmd=analysisstage1">Analysis Stage 1</a></li>
          <li><a href=admin.asp?cmd=anexcel>Excel of Stage 1</a></li>
          <%
	closers rsma
	end if%>
          <%sqlma="select * from usermenu where userid=" & session("userid") & " and menu='analysisstage2'"
	openrs rsma,sqlma
	if not rsma.eof and not rsma.bof then
	%>
          <li><a href="admin.asp?cmd=analysisstage2">Analysis Stage 2</a></li>
          <li><a href=admin.asp?cmd=anexcel2>Excel of Stage 2</a></li>
          <%
	closers rsma
	end if%>
          <%
	end if
	closers rsm2%>
          <%sqlma="select * from usermenu where userid=" & session("userid") & " and menu='analysisreports'"
	openrs rsma,sqlma
	if not rsma.eof and not rsma.bof then
	%>
          <li><a href="admin.asp?cmd=analysisreport">Analysis Reports</a> </li>
          <li><a href="admin.asp?cmd=anmonthlyex">Overview Analysis Excel</a> 
            <li><a href="admin.asp?cmd=pvlist">Possible Violator List</a>
            <li><a href="admin.asp?cmd=pvlistfuzzy">Possible Violator List with Fuzzy Permits</a>
          
        </ul>
      </li>
	<% end if
	closers rsma %>
	  <% sqlm2="select * from usermenu where userid=" & session("userid") & " and (menu='dvlarep' or menu='dvlaedit' or menu='dvlasearch')"
	openrs rsm2,sqlm2
	if not rsm2.eof and not rsm2.bof then %>
	    
   
      <li><a href=admin.asp?cmd=dvlareportupl>DVLA </a> 
        <ul>
        <li><a href=admin.asp?cmd=dvlarepsearch>Dvla Rep Search</a></li>
          <% sqlm4="select * from usermenu where userid=" & session("userid") & " and (menu='uploaddvlaauto')"
	openrs rsm4,sqlm4
	if not rsm4.eof and not rsm4.bof then %>
          <li> <a href=adddvla.asp target=_new>Upload DVLA (put in dval dir)</a> 
        <!-- <li> <a href=admin.asp?cmd=dvlaunknown> DVLA Unknown (Plate doesn't 
            match from auto file)</a> -->
            <%
				end if
				closers rsm4
				 sqlm4="select * from usermenu where userid=" & session("userid") & " and (menu='dvlatransactions')"
	openrs rsm4,sqlm4
	if not rsm4.eof and not rsm4.bof then %>
          <li> <a href=admin.asp?cmd=dvlatrans>Dvla Transactions</a> 
            <%
	end if
	closers rsm4
	
						 sqlm4="select * from usermenu where userid=" & session("userid") & " and (menu='dvlarep')"
	openrs rsm4,sqlm4
	if not rsm4.eof and not rsm4.bof then %>
          <li> <a href=admin.asp?cmd=searchdvla>Search Dvla (sent)</a> 
          <li> <a href=admin.asp?cmd=dvlareportupl>Upload File/Show Report</a> 
            <!-- <li><a href="admin.asp?cmd=dvlarepreprintbatch">Resend DVLA Batch</a>-->
          <li> <a href=admin.asp?cmd=dvlareportupl2>Upload File/Email File - Reg 
            only</a> <a href=admin.asp?cmd=dvlarepreprint>Reprint DVLA report</a> 
          </li>
          <li> <a href=admin.asp?cmd=outstandingdvla>Outstanding Dvla</a></li>
          <li> <a href=admin.asp?cmd=adddvla>Add Dvla records</a></li>
          <%
				end if
				closers rsm4
				
				  sqlm3="select * from usermenu where userid=" & session("userid") & " and menu='dvlaedit'"
	openrs rsm3,sqlm3
	if not rsm3.eof and not rsm3.bof then %>
          <li><a href=admin.asp?cmd=dvlasearch>DVLA Edit/Search </a> 
            <ul>
              <li><a href=admin.asp?cmd=dvlasearch2>DVLA wildcard Search </a></li>
              <li><a href=admin.asp?cmd=dvlasearchbyid>DVLA search by id </a></li>
              <li><a href=admin.asp?cmd=dvlasearchbyletter>DVLA search by letter 
                </a></li>
            </ul>
          </li>
          <% End If 
	closers rsm3
	%>
          <% sqlm4="select * from usermenu where userid=" & session("userid") & " and menu='dvlasearch'"
	openrs rsm4,sqlm4
	if not rsm4.eof and not rsm4.bof then %>
          <li><a href=admin.asp?cmd=dvlasearch>DVLA Search </a> 
            <ul>
              <li><a href=admin.asp?cmd=dvlasearch>DVLA wildcard Search </a></li>
            </ul>
          </li>
          <% End If 
	closers rsm4
	%>
        </ul>
      </li>
	<% End If 
	closers rsm2
	%>
		  
	
	<%if checkmenu("appeals")=true then
			 %>
	
	  <li><a href=admin.asp?cmd=appealletters>Appeals </a> 
        <ul>
          <!-- <li><a href=admin.asp?cmd=appealletters> Appeal Letters</a></li>-->
          <li><a href=admin.asp?cmd=appealpara&cmd2=add>Add Appeal Paragraphs</a></li>
          <li><a href=admin.asp?cmd=appealpara&cmd2=edit>Edit Appeal Paragraphs 
            </a></li>
          <li><a href="admin.asp?cmd=appealconfig&cmd2=edit&id=1">Edit Appeal 
            Initial and Final Paragaphs</a> 
        </ul>
      </li>
	
	<%end if %>
	  <li><a href=#>Misc</a><ul>
	  <%  
	  sqleu="select count(id) as count from emailusers where userid=" & session("userid")
	  openrs rseu,sqleu
	    if rseu("count")>0 then 
	    %>
	      <li><a href="admin.asp?cmd=emails">Emails</a></li>
	    <%
	    end if 
	  
	  closers rseu
	   %>
	  <li><a href="admin.asp?cmd=summary">Summary</a></li>
	   <% if checkmenu("addpermituser")=true then  %>
	   <li><a href="admin.asp?cmd=addpermituser">Add Permit User</a></li>
	  <% end if %>
	  <% if checkmenu("blackoutdays")=true then  %>
	   <li><a href="admin.asp?cmd=blackoutdays">Blackout Days</a></li>
	  <% end if %>
	  </ul> </li>
	
     <!--  <li><a href=admin.asp?cmd=adminlogout&id=<%= session("userid") %>>Log Out</a></li> -->
</ul> 
<br style="clear: left;" />
</div>
</span>

<%
'response.write "temporary:" & session("temporarypermit") & "<br>"
'response.write "permanent:" & session("permanentpermit") & "<br>"
'response.write "siteall:" & session("siteall") & "<br>"
end if
end sub

sub loginform %>
<form name=login method=post action=admin.asp?cmd=validate>
<table class=maintable2>
<input type=hidden name=cmdsaved value="<%=request("cmd") %>" />
<tr><td colspan=2 class=loginheader>Please Enter your Username and Password</td></tr>
<tr><td>Username:</td><td><input type=text name=username></td></tr>
<tr><td>Password:</td><td><input type=password name=password></td></tr>
<tr><td colspan=2 align=center><input type=submit name=submit value=Submit class=button></td></tr>
</table>

</form>
<%end sub









sub validatelogin 'validate login and only let 3 tried.  Insert ip into log.
if session("loginattempt")="" then session("loginattempt")=0
	session("loginattempt")=session("loginattempt")+1
	if session("loginattempt")>3 then 
		response.write "<br>You have exceeded the maximum amount of logins"
		sendmaxtryemail
		response.end
	end if
	'response.write "validating"
'	sql="select * from UserAccess where UserID=" & tosql(request.form("username"),"text")  & " and Password=" & tosql(request.form("password"),"text")   
sql="exec spvalidate @username=" & tosql(request.form("username"),"text") & ",@password=" & tosql(request.form("password"),"text")
'response.write "<!--" & sql & "-->"
openrs rsadmin,sql
if rsadmin("exists")=0 then
	response.write "<br>bad password"
	call loginform
else
	
	session("loginattempt")=0
	session("Admin")="ok"
	session("userid")=rsadmin("id")
	session("username")=rsadmin("userid")
	session("temporarypermit")=rsadmin("temporarypermit")
	
	session("permanentpermit")=rsadmin("permanentpermit")
	session("siteall")=rsadmin("siteall")
		session("adminsite")=rsadmin("adminsite")
	session("profile")=rsadmin("profile")
'	response.write "To do --- add last date logged in and log this user to logtable with ip. <br> "
	'sql="select [date] from userlog where userid=" & rsadmin("id") & " order by [date] desc"
	'response.write sql
	'openrs rs,sql
	%>
	<script type="text/javascript">

//SuckerTree Horizontal Menu (Sept 14th, 06)
//By Dynamic Drive: http://www.dynamicdrive.com/style/

var menuids=["treemenu1"] //Enter id(s) of SuckerTree UL menus, separated by commas

function buildsubmenus_horizontal(){
for (var i=0; i<menuids.length; i++){
  var ultags=document.getElementById(menuids[i]).getElementsByTagName("ul")
    for (var t=0; t<ultags.length; t++){
		if (ultags[t].parentNode.parentNode.id==menuids[i]){ //if this is a first level submenu
			ultags[t].style.top=ultags[t].parentNode.offsetHeight+"px" //dynamically position first level submenus to be height of main menu item
			ultags[t].parentNode.getElementsByTagName("a")[0].className="mainfoldericon"
		}
		else{ //else if this is a sub level menu (ul)
		  ultags[t].style.left=ultags[t-1].getElementsByTagName("a")[0].offsetWidth+"px" //position menu to the right of menu item that activated it
    	ultags[t].parentNode.getElementsByTagName("a")[0].className="subfoldericon"
		}
    ultags[t].parentNode.onmouseover=function(){
    this.getElementsByTagName("ul")[0].style.visibility="visible"
    }
    ultags[t].parentNode.onmouseout=function(){
    this.getElementsByTagName("ul")[0].style.visibility="hidden"
    }
    }
  }
}

if (window.addEventListener)
window.addEventListener("load", buildsubmenus_horizontal, false)
else if (window.attachEvent)
window.attachEvent("onload", buildsubmenus_horizontal)

</script>
	<%
	if session("admin")="ok" and showadminmenu<>1 then
	 call adminmenu
	showadminmenu=1

end if
'response.write "<br>Welcome " & rsadmin("userid") & "<br>" & rsadmin("companyname") 
'if not rs.eof and not rs.bof then 
'if rsadmin("lastloggedin")<>"" then
'	response.write "<br>You last logged in on " & rsadmin("lastloggedin") 
'end if
	'closers rs
	'sql="insert into userlog(ip,userid) values('" & Request.ServerVariables("remote_addr") &"'," & rsadmin("id") & ")"
	'objconn.execute sql

'end if
'if request("cmdsaved")<>"" and request("cmdsaved")<>"loginform" then 
'response.redirect "admin.asp?cmd=" & cmd
'else
response.redirect "admin.asp?cmd=adminhome"
'end if
closers rsadmin
end if  ' if isonline not false
end sub
sub adminhome
		
call poutstandingappeals
if session("userid")=30 or session("userid")=116 then call ktranscribe
end sub
sub summary

sqlm2="select * from usermenu where userid=" & session("userid") & " and (menu='summary' or menu='limitedsummary')"
	
	openrs rsm2,sqlm2
	if not  rsm2.eof and  not rsm2.bof then 
	limited=1
	'' check if not limitewd
	    sqlm5="select * from usermenu where userid=" & session("userid") & " and menu='summary'"
	    openrs rsm5,sqlm5
	    if not rsm5.eof and not rsm5.bof then limited=0
	    closers rsm5
		 response.write "<table class=maintable2>"

		 sqle="exec spsummary"
		openrs rse,sqle 
  	
  	response.write "<tr><td>" &  rse("waitingdvla") & " records waiting to be sent for dvla </td><td><a href=admin.asp?cmd=dvlafiletemp&type=auto class=button>Create Automatic File</a>&nbsp;<a href=admin.asp?cmd=dvlafiletemp&type=manual class=button>Create Manual File</a></td></tr>"
			''sqlca="select count(id) as mycount from tempviolators where registration in(select registration from apermits)"
'openrs rsca,sqlca
if limited=0 then response.write "<tr><td>" & rse("countpermits") & " records have permits</tD><td> <a href=admin.asp?cmd=delpcnpermits class=button>Delete Permits</a></td></tr>"
'closers rsca 



		'sqldv="select count(id) as mycount from tempviolators where registration in(select distinct reg from dvla where reg is not null)"
  	'openrs rsdv,sqldv
  	'mycount=rsdv("mycount")
  	'closers rsdv
  	if limited=0 then response.write "<tr><Td>" &rse("waitingprinted") & " records waiting to be printed </td><td><a href=admin.asp?cmd=printpcnsummary class=button>Print PCN</a></td></tr>"
			
	
	
	'sqldr=""
	'openrs rsdr,sqldr
	'mycount=rsdr("mycount")
	'closers rsdr
	if limited=0 then response.write "<tr><td>" & rse("waitingreturn") & " records waiting for dvla response to be returned</td></tr>"
	response.write "<br>"
	
	'sqlac="select count(id) as mycount from anPossibleViolators where confirmedviolator=1 and stage=2 and inserted=0"
'	openrs rsac,sqlac
	'countac=rsac("mycount")
	response.write "<tr><td>" & rse("waitingconfirm") & " records at 2nd stage are waiting to be confirmed </td><td><a href=admin.asp?cmd=confirmanalysis class=button>Confirm 2nd Stage</a>&nbsp;<a href=admin.asp?cmd=confirmanalysisall class=button>Confirm 2nd Stage - all</a></td></tr>" 
	'closers rsac
	'sqlr="select count(vwviolators.pcn) as mycount from vwviolators where pcn not in (select pcn from payments where pcn is not null) and pcn not in(select pcn from cancelled where pcn is not null) and pcn not in (select pcn+site from reminders where pcn is not null) and vwVIOLATORS.PCNSENT<getDate()-14"
	'openrs rsr,sqlr
				 remindercount=rse("remindertobeprinted")
	'closers rs
	if limited=0 then response.write "<tr><td>" &  remindercount & " reminders waiting to be printed</td><td><a href=admin.asp?cmd=printremindersummary class=button>Print Reminders</a></td></tr>"
		drcount=rse("debtrecoverytobeprinted")
	if limited=0 then	response.write "<tr><td>" &  drcount & " debt recovery waiting to be printed</td><td><a href=admin.asp?cmd=printdebtrecoverysummary class=button>Print Debt Recovery</a></td></tr>"
	
	if limited=0 then response.write "<tr><Td>" &rse("reissuewaiting") & " reissues waiting to be printed </td><td><a href=admin.asp?cmd=printreissuesummary class=button>Print Reissues</a></td></tr>"
		
	closers rse
	response.write "</table>"
	
	
end if
closers rsm2
end sub
Sub createSortableList(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
	'	if strpagename="admin.asp?cmd=paymentssort" then
    'if strsort="PCNdesc" then strsort="PCNdesc"
	' if strsort="iddesc" then strsort="iddesc"
	'  if strsort="PCN" then strsort="payments.pcn"
	' if strsort="ID" then strsort="violators.id"
  ' response.write "aa" & strsort & "aa"
	'if strsort="Date" then response.write "zadafsdfasdkfads"	
	  if strsort="Date" then strsort="[Date of Violation]"
	    if strsort="violators.[Date" then strsort="[Date of Violation]"
		  if strsort="violators.[Datedesc" then strsort="[Date of Violation]desc"
	    if strsort="Datedesc" then strsort="[Date of Violation]desc"
	'	 if strsort="Received" then strsort="payments.[Received]"
	 '   if strsort="Received" then strsort="payments.[Received]desc"	
    
	'end if
	'response.write "strsort:" & strsort	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
  '  response.write "xx" & strsort & "xx"			
    		Set RS = server.CreateObject("adodb.recordset")
   ' response.write strsql & " order by " & replace(strSort,"desc"," desc")
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"ID" and field.name<>"id" then
				Response.Write "<Th align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</Th>"	& vbcrlf
    			end if
	
	Next
					
    	'	Response.Write "<td>Notes</td><td>&nbsp;</td></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			
					if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
					
    			For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"ID" and field.name<>"id" then
					Response.Write "<TD align=center>" & vbcrlf
    				 Response.Write field.value
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
				if rs("ID")<>"" then
			'if showshortnotes(rs("pcn"))<>"" then
			'		response.write "<td><a href="""" onclick=""return popitup('admin.asp?cmd=shownotes&pcn=" &  rs("pcn") & "')"">" &  showshortnotes(rs("pcn")) &  "</a></td>"
			'else
			'	response.write "<td></td>"
			'end if
					
				response.write "<td><a target=_new href=admin.asp?cmd=viewrecord&p=1&id=" & rs("ID") & " class=button>View</a></td>"
				end if
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "<TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing			
    	End Sub
sub payments
sqlm="select * from usermenu where userid=" & session("userid") & " and menu='payments'"
	openrs rsm,sqlm
	if rsm.eof and rsm.bof then 
		response.write "no access"
		response.end
	end if
	closers rsm
select case request("cmd2")

case "Refresh" 
response.write "Please Confirm the following:"
response.write "<form method=post name=payments action=admin.asp?cmd=payments>"
%>
<table class=maintable2>
<tr><td colspan=6>Paying in Book Reference: <input type=text name=bookreference value='<%= request("bookreference") %>'></td></tr>
<tr><td>&nbsp;</td><td>PCN</td><td>PLATE</td><td>Amount</td><td>Date</td><td>Type</td></tr>
<%
tamount=0
dim arrpcn(36)
berror=0
for i=1 to 36
	myerrort=""
	if request("amount"&i)<>"" and request("amount"&i)<>"0"   then
	j=i
	if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
	
	response.write "<td>" & i & "</td>"
	spcn=request("pcn" & i)
	plate=request("plate" & i)
	'response.write spcn
	'if len(spcn)>7 then spcn=left(spcn,7)
	if spcn="" then spcn=lookuppcn(request.form("plate" & i),myerrort)
	site=lookupsite(spcn,myerrort)
	'response.write site & "<hr>"
	'response.write spcn
	if request("amount"  & i)<1 then myerrort="Amount must be more then 1"
	if right(spcn,3)<>site then myerrort="invalid pcn site"
	if plate="" then plate=lookupplate(spcn,myerrort)
	'spcn=spcn&site
	'sqlckp="select * from payments where pcn=" & tosql(spcn,"text")
	'openrs rsckp,sqlckp
	'if myerrort="" then
	'if not rsckp.eof and not rsckp.bof  then myerrort="PCN already exists in payment table"
	'end if
		'closers rsckp
	sqlckp="select * from vwviolators where pcn=" & tosql(spcn,"text") & " and registration=" & tosql(plate,"text")
'	response.write sqlckp
	openrs rsckp,sqlckp
	if myerrort="" then
	if  rsckp.eof and  rsckp.bof  then myerrort="PCN does not match plate"
	end if
		closers rsckp
	response.write "<td><input type=text maxLength=10 size=10 name=pcn" & i & " value='" &  spcn  & "'></td>"
	
	
	
	response.write "<td><input type=text size=10 name=plate" & i & " value='" & plate&"'></td>"
	response.write "<td><input type=text size=4 name=amount" & i & " value='" &request("amount" & i) & "' onKeyPress=""return checkValue(event)""></td>"
	tamount=tamount + cdbl(request("amount" & i ))
	response.write "<td><input type=text size=10 name=date" & i & "  value='" & request("date" & i) & "'>"
	%>
	<!-- <a href="javascript:NewCal('date<%= i %>','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a> --></tD>

	<%
	response.write "<td><select name=type" & i & "><option selected value='" & request("type" & i) & "'>" & request("type" & i) & "</option><option value='Cheque'>Cheque</option><option value='Postal Order'>Postal Order</option><option value='Credit Card'>Credit Card</option><option value='Credit Card Manual'>Credit Card Manual</option><option value='Newlyn'>Newlyn</option>	<option value='Cash'>Cash</option></select></td>"
	response.write "<td><input type=button class=button name=deleteme value=delete onClick=PaymentsDelete("& i & ");></td>"
	response.write "</tr>"
	'check if duplidcate
	exists = 0
	For Each element in arrpcn
		if spcn<>"" then
		if spcn = element then
		  myerrort=myerrort & " Duplicate pcn in this list"
		end if
		end if
	Next

 	arrpcn(i)=spcn
	
	
	if myerrort<>"" then 
		response.write "<tr><td colspan=6>" & myerrort & "</td></tr>"
		berror=1
	end if	
	end if


next

response.write "<tr><td colspan=2 align=right><b>Total Amount:</b></td><td>" & tamount & "</td></tr>"
j=j+1
k=j+5
for i=j to  k
	response.write "<tr><td>" & i & "</td>"
	response.write "<td><input size=10 maxLength=10  type=text name=pcn" & i & "></td>"
	response.write "<td><input size=10 type=text name=plate" & i & "></td>"
	response.write "<td><input size=10 amount type=text name=amount" & i & " onKeyPress=""return checkValue(event)""></td>"
	response.write "<td><input size=10 type=text id=f" & i & " name=date" & i & " value='" & date() & "'>"
	mydfields=""
	if i<>30 then
		for j=i+1 to 30
			mydfields=mydfields& "'f" & j & "',"
			
		next
	if mydfields<>"" then mydfields=left(mydfields,len(mydfields)-1)
	end if
	%>
	<a href="javascript:NewCal('f<%= i %>','ddmmyyyy', null, null, [<%= mydfields %>])"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a></tD>

	
	<%
	response.write "<td><select name=type" & i & ">"
	sqlmethod="select method from paymentmethods"
	openrs rsmethod,sqlmethod
	do while not rsmethod.eof
	response.write "<option value='" & rsmethod("method") & "'>" & rsmethod("method") & "</option>" & vbcrlf
	rsmethod.movenext
	loop
'	respons.write "<option value='Cheque'>Cheque</option><option value='Postal Order'>Postal Order</option><option value='Credit Card'>Credit Card</option><option value='Credit Card Manual'>Credit Card Manual</option><option value='Newlyn'>Newlyn</option></select></td>"
	closers rsmethod
	response.write "</select></td></tr>"
	
next

if berror=0 then 
	%>
	<tr><td colspan=6 align=center><input type=submit name=cmd2 value="Refresh" class=button>&nbsp;<input type=submit name=cmd2 value="Save" class=button></td></tr>
<%
	
else
%>
<tr><td colspan=6 align=center><input type=submit name=cmd2 value="Refresh" class=button></td></tr>

<%
end if

%>


</table>
</form>
<%
case "Save" 

bookreference=request("bookreference")
ip=Request.ServerVariables("REMOTE_ADDR") 
success="You have successfully added these payments. "' ID's are "
response.write "<!--type1:" & request("type1") & "-->" & vbcrlf
for i=1 to 36
	if request("amount"&i)<>""  and request("amount"&i)<>"0"   then
		spcn=request("pcn" & i)
		plate=request("plate" & i)
		amount=request("amount"  & i)
		method=request("type" & i)
		response.write "<!--i:" & method & "-->"
		received=cisodate(request("date" & i))
		
		if spcn="" or plate="" or amount="" then 
			 response.write "error - you must go back and make sure pcn,plate, and amount are not blank"
			 exit sub
		end if
		set rsr=objconn.execute("exec lastpaymentbatch")
		batchid=rsr("batchid")+1
		n="Number"
		t="text"
		sql="insert into payments(pcn,plate,amount,method,received,bookreference,ip,batchid,employee) values(" & tosql(spcn,t) & "," & tosql(plate,t) & "," & tosql(amount,"Number") & "," & tosql(method,t) &"," & tosql(received,t) &"," & tosql(bookreference,t)& "," & tosql(ip,t) & "," & batchid & "," & session("userid") &  ")"
		response.write "<!--" & sql & "-->"
		objconn.execute sql
		'set rsi = objconn.execute("SELECT @@IDENTITY from payments")
	'	success=success &  rsi(0) & ", "
		 
	end if
next
success=left(success,len(success)-2)
response.write success
case else
	%>
<form method=post action=admin.asp?cmd=payments name=payments>
<input type=hidden name=cmd2 value=Refresh>

<table class=maintable2>
<tr><td colspan=6>Paying in Book Reference: <input type=text name=bookreference></td></tr>
<tr><th>&nbsp;</th><th>PCN</th><th>PLATE</th><th>Amount</th><th>Date</th><th>Type</th></tr>
<%
for i=1 to 30
	if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
	
	response.write "<td>" & i & "</td>"
	response.write "<td><input size=10 maxLength=10  type=text name=pcn" & i & "></td>"
	response.write "<td><input size=10 type=text name=plate" & i & "></td>"
	response.write "<td><input size=10 amount type=text name=amount" & i & " onKeyPress=""return checkValue(event)""></td>"
	response.write "<td><input size=10 type=text id=f" & i & " name=date" & i & " value='" & date() & "'>"
	mydfields=""
	if i<>30 then
		for j=i+1 to 30
			mydfields=mydfields& "'f" & j & "',"
			
		next
		mydfields=left(mydfields,len(mydfields)-1)
	end if
	%>
	<a href="javascript:NewCal('f<%= i %>','ddmmyyyy', null, null, [<%= mydfields %>])"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a></tD>

	<%
	response.write "<td><select name=type" & i & ">"
	sqlmethod="select method from paymentmethods"
	openrs rsmethod,sqlmethod
	do while not rsmethod.eof
	response.write "<option value='" & rsmethod("method") & "'>" & rsmethod("method") & "</option>" & vbcrlf
	rsmethod.movenext
	loop
'	respons.write "<option value='Cheque'>Cheque</option><option value='Postal Order'>Postal Order</option><option value='Credit Card'>Credit Card</option><option value='Credit Card Manual'>Credit Card Manual</option><option value='Newlyn'>Newlyn</option></select></td>"
	closers rsmethod
	response.write "</select></td></tr>"
	
	
next

%>
<tr><td colspan=6 align=center><input type=submit name=submit value="Submit" class=button></td></tr>
</table>
</form>
<%
	
end select
end sub



sub maddpermit1
 sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='addpermit'"
	openrs rsm2,sqlm2
	if rsm2.eof and  rsm2.bof then 
		exit sub
	End If 
	closers rsm2

select case request("cmd2")

case "Refresh" 
response.write "<table class=maintable2><tr><td>"
response.write "Please Confirm the following:"
response.write "<form method=post action=admin.asp?cmd=maddpermit>"
call SaveAsFormFields ("all", "all")
for i=1 to 15
	if request("plate"&i)<>"" then
		 response.write i & " permit will be addded to site:" & request.form("site" & i) & "<br>"
	end if
next
	%>
	</td></tr>
	<tr><td  align=center><input type=submit name=cmd2 value="Save" class=button></td></tr>


</table>
</form>
<%
case "Save" 


for i=1 to 15
	if request("plate"&i)<>"" then
		plate=request("plate"&i)
		startdate=date()
		stype=request("type" & i)
		 if stype="Temporary" then
		 	expirydate=date+7
		else
			expirydate=""
		end if
		fileno=request.form("fileno" & i)
		if request.form("disabled" & i)= "yes" then 
			disabled=1
		else
			disabled=0
		end if
		site=request.form("site" & i)
		profile=session("profile")
		userid=session("userid")
		ip=Request.ServerVariables("remote_addr")
		
		t="text"
		sql="insert into apermits(registration,valid_date,expiry_date,type,file_page,site,profile,disabled,userid,ip) values(" & tosql(plate,t) & "," & tosql(startdate,"date") & "," & tosql(expirydate,"date") & "," & tosql(stype,t) &"," & tosql(fileno,t) &"," & tosql(site,t)& "," & tosql(profile,t) & "," & tosql(disabled,"Number") & "," & tosql(userid,"Number") & "," & tosql(ip,t) & ")"
		
		'response.write sql
		objconn.execute sql
		 
		 
	end if
next
response.write "Permits added successfully"
case else
	%>
<form method=post action=admin.asp?cmd=maddpermit name=permits>
<input type=hidden name=cmd2 value=Refresh>

<table class=maintable2>


<tr><td>&nbsp;</td><td>Plate</td><td>Type</td>
<% If session("siteall")=true Then 
'don nothing
else
%>
<td>Site</td>
<% End If %>


<td>File No</td><td>Disabled</td></tr>
<%
for i=1 to 15
		
	response.write "<tr"
	if i mod 2=0 then response.write " class=rowcolor2"
	response.write "><td>" & i & "</td>"
	response.write "<td><input size=10 maxLength=10  type=text name=plate" & i & "></td>"
	response.write "<td><select name=type" & i & " onchange=""setSelectBoxestype(this);"">"
	if session("temporarypermit")=true then
		response.write "<option value=Temporary>Temporary</option>"
	end if
	if session("permanentpermit")=true then
		response.write "<option value=Permanent>Permanent</option>"
	end if
		response.write "</select></td>"
	
	if session("siteall")=true then 
		response.write "<input type=hidden name=site" & i & " value=all>"
	else
		response.write "<td><select name=site" & i & " onchange=""setSelectBoxessite(this);"">"
		response.write "<option value=all>all</option>"
		call getsiteoptions
		response.write "</select></td>"
	end if
	response.write "<td><input type=text name=fileno" & i & " size=3></td>"
	response.write "<td><input type=checkbox name=disabled" & i & " value=yes></td>"
	response.write "</tr>"
	
next

%>
<tr><td colspan=6 align=center><input type=submit name=submit value="Submit" class=button></td></tr>
</table>
</form>
<%
	
end select
end sub

sub maddpermit2
 sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='addpermit'"
	openrs rsm2,sqlm2
	if rsm2.eof and  rsm2.bof then 
		exit sub
	End If 
	closers rsm2

select case request("cmd2")

case "Refresh" 
response.write "<table class=maintable2><tr><td>"
response.write "Please Confirm the following:"
response.write "<form method=post action=admin.asp?cmd=maddpermit>"
call SaveAsFormFields ("all", "all")
for i=1 to 15
	if request("plate"&i)<>"" then
		 response.write i & " permit will be addded to site:" & request.form("site" & i) & "<br>"
	end if
next
	%>
	</td></tr>
	<tr><td  align=center><input type=submit name=cmd2 value="Save" class=button></td></tr>


</table>
</form>
<%
case "Save" 


for i=1 to 15
	if request("plate"&i)<>"" then
		plate=request("plate"&i)
		startdate=date()
		stype=request("type" & i)
		 if stype="Temporary" then
		 	expirydate=date+7
		else
			expirydate=""
		end if
		
		site=request.form("site" & i)
		profile=session("profile")
		userid=session("userid")
		ip=Request.ServerVariables("remote_addr")
		
		t="text"
		'sql="insert into permits(plate,startdate,expirydate,type,site,profile,userid,ip) values(" & tosql(plate,t) & "," & tosql(startdate,t) & "," & tosql(expirydate,t) & "," & tosql(stype,t) &","  & tosql(site,t)& "," & tosql(profile,t) & "," & tosql(userid,"Number") & "," & tosql(ip,t) & ")"
		
		sql="insert into apermits(registration,valid_date,expiry_date,type,site,profile,userid,ip) values(" & tosql(plate,t) & "," & tosql(startdate,"date") & "," & tosql(expirydate,"date") & "," & tosql(stype,t) &","  & tosql(site,t)& "," & tosql(profile,t) & "," & tosql(userid,"Number") & "," & tosql(ip,t) & ")"
		
		
	'	response.write sql
		objconn.execute sql
		 
		 
	end if
next
response.write "Permits added successfully"
case else
	%>
<form method=post action=admin.asp?cmd=maddpermit name=form1>
<input type=hidden name=cmd2 value=Refresh>

<table class=maintable2>


<tr><td>&nbsp;</td><td>Plate</td><td>Type</td>
<% if session("siteall")<>true then %>
<td>Site</td>
<% End If %>

</tr>
<%
for i=1 to 15
	response.write "<tr"
	if i mod 2=0 then response.write " class=rowcolor2"
	response.write "><td>" & i & "</td>"
	response.write "<td><input size=10 maxLength=10  type=text name=plate" & i & "></td>"
	response.write "<td><select name=type" & i & " onchange=""setSelectBoxestype(this);"">"
	if session("temporarypermit")=true then
		response.write "<option value=Temporary>Temporary</option>"
	end if
	if session("permanentpermit")=true then
		response.write "<option value=Permanent>Permanent</option>"
	end if
		response.write "</select></td>"
	if session("siteall")=true then 
		response.write "<input type=hidden name=site" & i & " value=all>"
	else
		response.write "<td><select name=site" & i & " onchange=""setSelectBoxessite(this);"">"
		response.write "<option value=all>all</option>"
		call getsiteoptions
		response.write "</select></td>"
	end if
	
	
	response.write "</tr>"
	
next

%>
<tr><td colspan=6 align=center><input type=submit name=submit value="Submit" class=button></td></tr>
</table>
</form>
<%
	
end select
end sub



sub maddpermit3
 sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='addpermit'"
	openrs rsm2,sqlm2
	if rsm2.eof and  rsm2.bof then 
		exit sub
	End If 
	closers rsm2

select case request("cmd2")

case "Refresh" 
response.write "<table class=maintable2><tr><td>"
response.write "Please Confirm the following:"
response.write "<form method=post action=admin.asp?cmd=maddpermit>"
call SaveAsFormFields ("all", "all")
for i=1 to 3
	if request("plate"&i)<>"" then
		 response.write i & " permit will be addded to site:" & request.form("site" & i) & "<br>"
	end if
next
	%>
	</td></tr>
	<tr><td  align=center><input type=submit name=cmd2 value="Save" class=button></td></tr>


</table>
</form>
<%
case "Save" 


for i=1 to 3
	if request("plate"&i)<>"" then
		plate=request("plate"&i)
		startdate=date()
		stype=request("type" & i)
		 if stype="Temporary" then
		 	expirydate=date+7
		else
			expirydate=""
		end if
		
		site=request.form("site" & i)
		profile=session("profile")
		userid=session("userid")
		ip=Request.ServerVariables("remote_addr")
	reason=request.form("reason" & i)
	vehicletype=request.form("vehicletype" & i)
	vehiclecolour=request.form("vehiclecolour" & i)
	driversname=request.form("driversname" & i)


		t="text"
		sql="insert into apermits(registration,valid_date,expiry_date,type,site,profile,userid,ip,reason,vehicletype,vehiclecolour,driversname) values(" & tosql(plate,t) & "," & tosql(startdate,"date") & "," & tosql(expirydate,"date") & "," & tosql(stype,t) &"," & tosql(site,t)& "," & tosql(profile,t) & ","  & tosql(userid,"Number") & "," & tosql(ip,t) & "," & tosql(reason,t)  &","  & tosql(vehicletype,t) &"," & tosql(vehiclecolour,t) &"," & tosql(driversname,t)    & ")"
	
	'sql="insert into apermits(registration,valid_date,expiry_date,type,file_page,site,profile,diabled,userid,ip) values(" & tosql(plate,t) & "," & tosql(startdate,"date") & "," & tosql(expirydate,"date") & "," & tosql(stype,t) &"," & tosql(fileno,t) &"," & tosql(site,t)& "," & tosql(profile,t) & "," & tosql(disabled,"Number") & "," & tosql(userid,"Number") & "," & tosql(ip,t) & ")"
		
		'response.write sql
		objconn.execute sql
		 
		 
	end if
next
response.write "Permits added successfully"
case else
	%>
<form method=post action=admin.asp?cmd=maddpermit name=permits>
<input type=hidden name=cmd2 value=Refresh>

<table class=maintable2>


<%
for i=1 to 3
	response.write "<tr><td>Plate</td>"
	response.write "<td><input size=10 maxLength=10  type=text name=plate" & i & "></td></tr>"
	response.write "<tr><td>Type</td><td><select name=type" & i & " onchange=""setSelectBoxestype(this);"">"
	if session("temporarypermit")=true then
		response.write "<option value=Temporary>Temporary</option>"
	end if
	if session("permanentpermit")=true then
		response.write "<option value=Permanent>Permanent</option>"
	end if
		response.write "</select></td></tr>"
		if session("siteall")=true then
			response.write "<input type=hidden name=site" & i & " value=all>"
		else
			response.write "<tr><td>Site</td><td><select name=site" & i & " onchange=""setSelectBoxessite(this);"">"
			response.write "<option value=all>all</option>"
			call getsiteoptions
			response.write "</select></td></tr>"
		end if
	response.write "<tr><td>Reason</td><td><textarea name=reason" & i & " rows=5 cols=30></textarea></td></tr>"
		response.write "<tr><td>Vehicle Type<td><input type=text name=vehicletype" & i & "></td></tr>"
		response.write "<tr><td>Vehicle Colour<td><input type=text name=vehiclecolour" & i & "></td></tr>"
		response.write "<tr><td>Drivers Name<td><input type=text name=driversname" & i & "></td></tr>"
	response.write "<tr><td colspan=2><hr></td></tr>"
	
next

%>
<tr><td colspan=6 align=center><input type=submit name=submit value="Submit" class=button></td></tr>
</table>
</form>
<%
	
end select
end sub

sub permits

response.write "todo -- need to find out what goes here"

end sub

sub pcn
%>
<table class=maintable2><tr><td>
<%
if session("adminsite")=true then
sqlsite="select * from [site information]"
else
sqlsite="select * from usersites where userid=" & cint(session("userid"))
end if
'response.write sqlsite

openrs rssites,sqlsite  ' check the sites he can see 
if rssites.eof and rssites.bof then
	 response.write "YOu do not have access to any site information"
	 response.End
end if

i=1
'sql="("
do while not rssites.eof
	if i>1 then sql=sql & " OR "
 	sql=sql & "(site='" &  rssites("sitecode") & "')" 
	i=i+1
	rssites.movenext
loop 
closers rssites


 
  %>

<table>
<form method=post action=admin.asp?cmd=search name=myform>
<tr><td colspan=2>Enter in a pcn or plate to pull up record</td></tr>
<tr><td>pcn:</td><td> <input type=text name=pcn></td></tr>
<tr><td>plate:</td><td> <input type=text name=plate></td></tr>
<tr><td colspan=2 align=center><input type=submit  value="Search" class=button></td></tr>


</form>
</table>

	</td></tr></table>

<%
end sub
sub reports
sqlm="select * from usermenu where userid=" & session("userid") & " and menu='reports'"
	openrs rsm,sqlm
	if rsm.eof and rsm.bof then 
		response.write "no access"
		response.end
	end if
	closers rsm
	
%>

<table class=maintable2><tr><td>
<% 
sqlwhere=""
j=0
if request.form("paymenttype")<>"" and request.form("paymenttype")<>"all" then
	if j>0 then sqlwhere=sqlwhere & " and "
	sqlwhere=sqlwhere & "method=" & tosql(request.form("paymenttype"),"text") 
	response.write "<div class=header>Payment Type searched: " & request.form("paymenttype") & "</div>"
	j=j+1
end if
datefrom=request("datefrom")
dateto=request("dateto")
if datefrom="" and dateto="" then
     datefrom=date
     dateto=date
end if
if datefrom<>"" and dateto<>"" then
	
	if request.form("dateofv")="yes" then
		 datefield="[date of violation]"
	else
			datefield="received"
	end if
	if j>0 then sqlwhere=sqlwhere & " and "
	sqlwhere=sqlwhere & "(" & datefield & " >="
	
	sqlwhere=sqlwhere & tosql(cisodate(datefrom),"text")
	'sqlwhere=sqlwhere & "cast('" & myear & mymonth& myday &"' as datetime)"
	'sqlwhere=sqlwhere & "dateadd(year, " & myear & " - 1900, dateadd(month, " & mymonth & ", dateadd(day, " & myday & ", -32)))"
		sqlwhere=sqlwhere & " and " & datefield & "<="
	
	sqlwhere=sqlwhere & tosql(cisodate(dateto) &" 23:59","text")
		sqlwhere=sqlwhere & ")"
	j=j+1 
response.write "<div class=header>Dates searched from " & datefrom & " to " &dateto  & "</div>"
end if


sqlc="SELECT Count(pcn) AS CountOfID FROM vwPAYMENTSv"' WHERE "
sqlsum="SELECT Sum(AMOUNT) AS SumOfAMOUNT from vwpaymentsv" ' where "

qrysites=0
if request.form("sites")="" or request.form("sites")="All Sites" then
	if session("adminsite")=true then
		sqlsites="select sitecode from [site information]"
	else
	qrysites=1
	if j>0 then sqlwhere=sqlwhere & " and "
	sqlsites="select sitecode from usersites where userid=" & session("userid")
	
	openrs rssites,sqlsites  ' check the sites he can see 
	if rssites.eof and rssites.bof then
		 response.write "You do not have access to any site information"
		 response.End
	end if
	
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	'arrsites=array("site1","site2","site3")
	j=j+1
	closers rssites
	end if
else
	if j>0 then sqlwhere=sqlwhere & " and "
	arrsites=split(request.form("sites"),vbcrlf,-1)
	totalsites=ubound(arrsites)
	qrysites=1
	j=j+1
end if
	if qrysites=1 then
		i=1
		
		for each site in arrsites
			if i>1 then sql=sql & " OR "
			sql=sql & "(Right(PCN,3)='" &  site & "')"
			i=i+1
			
		next
end if
if j>0 then sqlwhere=" where " & sqlwhere
if qrysites=1 then sql="(" & sql & ")"
'response.write sql
'response.write sqlc & sqlwhere &  sql & "<br>"
'response.write sqlc &sqlwhere &  sql & "<br>"
openrs rspcncount,sqlc &sqlwhere &  sql
pcnpaid=rspcncount("CountOfID")
closers rspcncount
'response.write sqlsum &sqlwhere & sql & "<br>"
openrs rspcnsum,sqlsum &sqlwhere & sql
pcnamountpaid=rspcnsum("sumofamount")
closers rspcnsum
session("where")=sqlwhere & sql

'if request.form("sites")="" or request.form("sites")="All Sites" then
'	openrs rssites,sqlsites  ' check the sites he can see 
'	if rssites.eof and rssites.bof then
'		 response.write "YOu do not have access to any site information"
'		 response.End
'	end if
	'sql=""
	'i=1
	'do while not rssites.eof
	'	if i>1 then sql=sql & " OR "
	'	sql=sql & "site='" &  rssites("sitecode") & "'" 
	'	i=i+1
	'	rssites.movenext
	'loop 
	'closers rssites
'else
'	arrsites=split(request.form("sites"),vbcrlf,-1)
		sql=""
	if qrysites=1 then
		i=1
		for each site in arrsites
				if i>1 then sql=sql & " OR "
				sql=sql & "site='" &  site & "'" 
				i=i+1
		next
sql="(" & sql & ")"
end if

'end if


'sqlc="SELECT Count([pcn]) AS countpcn from violators where left([pcn],7) not in (select pcn from payments)  and "

'response.write sqlc&sql
'openrs rspcncount,sqlc & sql
'pcnowed=rspcncount("Countpcn")
'closers rspcncount
'pcnowed=0
'totalamountowed=0
'sqls="SELECT * from violators"
'if qrysites=1 then sqls=sqls & " where " & sql
'gets all violator records for users sites
'openrs rss,sqls 

'do while not rss.eof
'	sqlp="select amount,received from payments where left([pcn],7)= " & tosql(rss("pcn"),"text")
	'response.write sqlp
'	openrs rsp,sqlp
'	if rsp.eof and rsp.bof then
'		amountpaid=0
'		received=date()
	'else
	'	amountpaid=rsp("amount")
	'	received=rsp("received")
	'end if
	'closers rsp
	'sqlt="select * from tariff where site=" & tosql(rss("site"),"text")
	'openrs rst,sqlt
		
		'days=datediff("d",rss("pcnsent"),received)
		'response.write "days:" & days & "+"
		'if days>14 then 
		'	response.write rss("pcn") & " is more then 14 datys<br>"
		'	amountowed=rst("ticket price")
		'else
		'	amountowed=rst("reduced amount")
	'	end if
		'response.write "amount owed for " & rss("pcn") & " is:" & amountowed-amountpaid  & "<bR><hr>"
	'	amountowed=amountowed-amountpaid
	'	totalamountowed=totalamountowed + amountowed
	'	if amountowed>0 then pcnowed=pcnowed+1
		
	'closers rst
'rss.movenext
'loop
'closers rss

'openrs rspcnsum,sqlsum & sql
'pcnamountpaid=rspcnsum("sumofamount")
'closers rspcnsum
 %>
<br>
PCN paid to date: <%= pcnpaid %><br>
Value (£) paid to date: £<%= pcnamountpaid %><br><br>
<div>
<span class=bfl><a href=admin.asp?cmd=paymentssort class=button>View Payments Details</a></span>
<span class=bfl>
<a href=admin.asp?cmd=paymentrepexcel class=button>View Payments Details in Excel</a></span>

</div>
<!-- PCN outstanding: <%=pcnowed %><br>
Value (£) outstanding: £<%= totalamountowed %><br> -->
<br><br>
<form method=post action=admin.asp?cmd=reports>
<table>
<tr>
<td>
Sites:</td><td> <select name=sites><option value="All Sites">All Sites</option>
<% 

'sqlsites="select * from usersites where userid=" & session("userid")
'openrs rssites,sqlsites  ' check the sites he can see 
'do while not rssites.eof
if session("adminsite")=true then
	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites
end if
for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
</td></tr>
<tr><td>
Payment type: </td><td>
<%

response.write "<select name=paymenttype><option value=''>All</option>"
	sqlmethod="select method from paymentmethods"
	openrs rsmethod,sqlmethod
	do while not rsmethod.eof
	response.write "<option value='" & rsmethod("method") & "'>" & rsmethod("method") & "</option>" & vbcrlf
	rsmethod.movenext
	loop
'	respons.write "<option value='Cheque'>Cheque</option><option value='Postal Order'>Postal Order</option><option value='Credit Card'>Credit Card</option><option value='Credit Card Manual'>Credit Card Manual</option><option value='Newlyn'>Newlyn</option></select></td>"
	closers rsmethod
	response.write "</select>"
	
	%>
</td></tr>
<tr><td>
From </td><td><input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To </td><td><input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<input type=radio name=dateofv value="yes">Date of Violation
</td></tr>
<tr><td colspan=2 align=center>
<input type=submit name=submit value="Submit" class=button>
</td></tr></table>
</form>
</td></tr></table>



<%

end sub
sub graph
sqlm="select * from usermenu where userid=" & session("userid") & " and menu='reports'"
	openrs rsm,sqlm
	if rsm.eof and rsm.bof then 
		response.write "no access"
		response.end
	end if
	closers rsm
%>
<table class=maintable2><tr><td>
<% 
sqlwhere=""
j=0

if j>0 then sqlwhere=sqlwhere & " and "

largestamount=0

if request.form("site")="" then
	if session("adminsite")=true then
		sqlsites="select sitecode from [site information]"
	else	
	sqlsites="select sitecode from usersites where userid=" & session("userid")
	end if
	openrs rssites,sqlsites  ' check the sites he can see 
	if rssites.eof and rssites.bof then
		 response.write "You do not have access to any site information"
		 response.End
	end if
	
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	'arrsites=array("site1","site2","site3")

	closers rssites
else
	arrsites=split(request.form("sites"),vbcrlf,-1)
	totalsites=ubound(arrsites)
end if
'arrsites=array("site1","site2","site3")

totalsites=totalsites+1
'response.write "total sites:" & totalsites

dim arrsitename()
dim arramount()
redim arrsitename(totalsites)
redim arramount(totalsites)
		i=1
		j=0
		for each site in arrsites
			if i>1 then sql=sql & " OR "
			sql=sql & "(Right([PCN],3)='" &  site & "')"
				sqlam="select sum(PAYMENTS.AMOUNT) AS sumofamount from payments where " & sqlwhere & " right([PCN],3)='" & site & "'"
		'response.write sqlam
		
		openrs rsam,sqlam
		arramount(j)= rsam("sumofamount")
				if arramount(j) > largestamount then largestamount=arramount(j)
				arrsitename(j)=site
		
		closers rsam 
			'response.write "<hr>" & site & "<hr>"
			i=i+1
			j=j+1
		next


'graph
if largestamount>0 then
ShowChart arramount,arrsitename, "Site Payments", "Site", "payment amount"
end if

%>
<br><br>
<h5>View graph per site:</h5>
<form method=post action=admin.asp?cmd=graphoptions>
Site: <select name=sites>
<% 
if session("adminsite")=true then
sqlsites="select * from [site information]"
else
sqlsites="select * from usersites where userid=" & session("userid")
end if
openrs rssites,sqlsites  ' check the sites he can see 
do while not rssites.eof
		response.write "<option value='" & rssites("sitecode") & "'>" & rssites("sitecode") & "</option>"
	rssites.movenext
loop 
closers rssites %>

</select>
<br>

Months to show:<br>
<select name=months size="12" multiple>
<%
j=year(date)-1
for i=1 to 12
response.write "<option value=" & i & "-" & j & ">" & monthname(i) & "-" & j & "</option>"
next
j=year(date)
for i=1 to 12
response.write "<option value=" & i & "-" & j & ">" & monthname(i) & "-" & j & "</option>"
next
%>
</select>

<br><br>
<input type=submit name=submit value="Submit" class=button>
</form>
</td></tr></table>



<%

end sub


sub graphoptions
%>
<table class=maintable2><tr><td>
<% 
site=request("sites")
dim arrmonthname()
dim arramount()
dim arrcolors()
strmonths=request("months")
arrmonths=split(strmonths,",")
redim arramount(ubound(arrmonths))
redim arrmonthname(ubound(arrmonths))
redim arrcolors(ubound(arrmonths))
tmonths= ubound(arrmonths)
'response.write "ubound of arrmonths:" & tmonths
for j=0 to tmonths
'response.write "##j:" & j & "##"
'response.write arrmonths(j) & "***"
imonth=left(arrmonths(j),len(arrmonths(j))-5)
if len(trim(imonth))<2 then imonth="0" & trim(imonth)
iyear=right(arrmonths(j),4)
k=0
'response.write "month:" & imonth
	sqlam="select sum(PAYMENTS.AMOUNT) AS sumofamount from payments where (received>=cast('" & iyear & imonth &"01' as datetime) and received<=cast('" & iyear & imonth &"30 23:59' as datetime)) and right([PCN],3)='" & site & "'"
	'	response.write  sqlam
		
		openrs rsam,sqlam
		if not rsam.eof and not rsam.bof then
		arramount(j)= rsam("sumofamount")
		end if
		if arramount(j)="" or isnull(arramount(j)) then
		arramount(j)=0
		else
		k=k+1
		end if
		'response.write "<hr>" & arramount(j) & "<hr>"
	'			if arramount(j) > largestamount then largestamount=arramount(j)
	'			arrsitename(j)=site
		arrmonthname(i)=monthname(imonth) & "<br>" & iyear
		arrcolors(i)="blue"
		closers rsam 
			'response.write "<hr>" & site & "<hr>"
			i=i+1
			'j=j+1
		next


if k>0 then 
	Response.Write makechart("Payments", arramount, arrmonthname, null, "#ccccff", 1, 200, 50, true)
else
	response.write "No payments to show"
end if
%>


<%

'graph

'ShowChart arramount,arrmonthname, "Payments", "Month", "payment amount"


%>

</td></tr></table>



<%

end sub
function makechart(title, numarray, labelarray, color, bgcolor,      bordersize, maxheight, maxwidth, addvalues) 
 'Function makechart version 3

 'Jason Borovoy
 'title: Chart Title
 'numarray: An array of values for the chart
 'labelarray: An array of labels coresponding to the values must me present
 'color If null uses different colors for bars if not null all bars color you specify
 'bgcolor Background color.
 'bordersize: border size or 0 for no border.
 'maxheight: maximum height for chart not including labels
 'maxwidth: width of each column
 'addvalues: true or false depending if you want the actual values shown on the chart
 'when you call the function use : response.write makechart(parameters)
 
 'actually returnstring would be a better name
 dim tablestring 
 'max value is maximum table value
 dim max 
 'maxlength maximum length of labels
 dim maxlength
 dim tempnumarray
 dim templabelarray
 dim heightarray
 Dim colorarray
 'value to multiplie chart values by to get relitive size 
 Dim multiplier
 'if data valid
 if maxheight > 0 and maxwidth > 0 and ubound(labelarray) = ubound(numarray) then
  'colorarray: color of each bars if more bars then colors loop through
  'if you don't like my choices change them, add them, delete them.
  colorarray = array("red","blue","yellow","navy","orange","purple","green")
  templabelarray = labelarray
  tempnumarray = numarray
  heightarray = array()
  max = 0
  maxlength = 0
  tablestring = "<TABLE bgcolor='" & bgcolor & "' border='" & bordersize & "'>" & _
    "<tr><td><TABLE border='0' cellspacing='1' cellpadding='0'>" & vbCrLf
  'get maximum value
  for each stuff in tempnumarray
   if stuff > max then max = stuff end if 
  next
  'calculate multiplier
  multiplier = maxheight/max
  'populate array
  for counter = 0 to ubound(tempnumarray)
   if tempnumarray(counter) = max then 
    redim preserve heightarray(counter)
    heightarray(counter) = maxheight
   else
    redim preserve heightarray(counter) 
    heightarray(counter) = tempnumarray(counter) * multiplier 
   end if 
  next 


   'set title 
   tablestring = tablestring & "<TR><TH colspan='" & ubound(tempnumarray)+1 & "'>" & _
     "<FONT FACE='Verdana, Arial, Helvetica' SIZE='1'><U>" & title & "</TH></TR>" & _
      vbCrLf & "<TR>" & vbCrLf
   'loop through values
   for counter = 0 to ubound(tempnumarray) 
    tablestring = tablestring & vbTab & "<TD valign='bottom' align='center' >" & _
    "<FONT FACE='Verdana, Arial, Helvetica' SIZE='1'>" & _
    "<table border='0' cellpadding='0' width='" & maxwidth & "'><tr>" & _
    "<tr><td valign='bottom' bgcolor='" 
    if not isNUll(color) then 
     'if color present use that color for bars
     tablestring = tablestring & color
    else
     'if not loop through colorarray
     tablestring = tablestring & colorarray(counter mod (ubound(colorarray)+1))
    end if
    tablestring = tablestring & "' height='" & _
     round(heightarray(counter),2) & "'><img src='chart.gif' width='1' height='1'>" & _
     "</td></tr></table>"
    if addvalues then
     'print actual values
     tablestring = tablestring & "<BR>" & tempnumarray(counter)
    end if 
    tablestring = tablestring & "</TD>" & vbCrLf
   next
 
  tablestring = tablestring & "</TR>" & vbCrLf
  'calculate max lenght of labels
  for each stuff in labelarray
   if len(stuff) >= maxlength then maxlength = len(stuff)
  next
  'print labels and set each to maxlength
  for each stuff in labelarray
   tablestring = tablestring & vbTab & "<TD align='center'><" & _
    "FONT FACE='Verdana, Arial, Helvetica' SIZE='1'><B> " 
   for count = 0 to round((maxlength - len(stuff))/2)
    tablestring = tablestring & " "
   next
   if maxlength mod 2 <> 0 then tablestring = tablestring & " "
   tablestring = tablestring & stuff 
   for count = 0 to round((maxlength - len(stuff))/2)
    tablestring = tablestring & " "
   next
   tablestring = tablestring & " </TD>" & vbCrLf
  next
   
  tablestring = tablestring & "</TABLE></td></tr></table>" & vbCrLf
  makechart = tablestring
 else
  Response.Write "Error Function Makechart: maxwidth and maxlength have to be greater " & _
  " then 0 or number of labels not equal to number of values"
 end if 
end function


function monthRange(sDate1, sDate2)
	sStartMonth = month(sDate1)
	sEndMonth = month(sDate2)
	sMonths = ""
	
	for i=sStartMonth to sEndMonth
		sMonths = sMonths & "," & i
	next
	
	aMonths = split(sMonths, ",")
	monthRange = aMonths
end function

Sub ShowChart(ByRef aValues, ByRef aLabels, ByRef strTitle, ByRef strXAxisLabel, ByRef strYAxisLabel)
	' Some user changable graph defining constants
	' All units are in screen pixels
	Const GRAPH_WIDTH  = 450  ' The width of the body of the graph
	Const GRAPH_HEIGHT = 250  ' The height of the body of the graph
	Const GRAPH_BORDER = 5    ' The size of the black border
	Const GRAPH_SPACER = 2    ' The size of the space between the bars

	' Debugging constant so I can easily switch on borders in case
	' the tables get messed up.  Should be left at zero unless you're
	' trying to figure out which table cells doing what.
	Const TABLE_BORDER = 0
	'Const TABLE_BORDER = 10

	' Declare our variables
	Dim I
	Dim iMaxValue
	Dim iBarWidth
	Dim iBarHeight

	' Get the maximum value in the data set
	iMaxValue = 0
	For I = 0 To UBound(aValues)
		If iMaxValue < aValues(I) Then iMaxValue = aValues(I)
	Next 'I
	'Response.Write iMaxValue ' Debugging line


	' Calculate the width of the bars
	' Take the overall width and divide by number of items and round down.
	' I then reduce it by the size of the spacer so the end result
	' should be GRAPH_WIDTH or less!
	iBarWidth = (GRAPH_WIDTH \ (UBound(aValues) + 1)) - GRAPH_SPACER
	'Response.Write iBarWidth ' Debugging line


	' Start drawing the graph
	%>
	<TABLE BORDER="<%= TABLE_BORDER %>" CELLSPACING="0" CELLPADDING="0">
		<TR>
			<TD COLSPAN="3" ALIGN="center"><H2><%= strTitle %></H2></TD>
		</TR>
		<TR>
			<TD VALIGN="center"><B><%= strYAxisLabel %></B></TD>
			<TD VALIGN="top">
				<TABLE BORDER="<%= TABLE_BORDER %>" CELLSPACING="0" CELLPADDING="0">
					<TR>
						<TD ROWSPAN="2"><IMG SRC="spacer.gif" BORDER="0" WIDTH="1" HEIGHT="<%= GRAPH_HEIGHT %>"></TD>
						<TD VALIGN="top" ALIGN="right"><%= iMaxValue %>&nbsp;</TD>
					</TR>
					<TR>
						<TD VALIGN="bottom" ALIGN="right">0&nbsp;</TD>
					</TR>
				</TABLE>
			</TD>
			<TD>
				<TABLE BORDER="<%= TABLE_BORDER %>" CELLSPACING="0" CELLPADDING="0">
					<TR>
						<TD VALIGN="bottom"><IMG SRC="color1.gif" BORDER="0" WIDTH="<%= GRAPH_BORDER %>" HEIGHT="<%= GRAPH_HEIGHT %>"></TD>
					<%
					' We're now in the body of the chart.  Loop through the data showing the bars!
					For I = 0 To UBound(aValues)
						if aValues(i)>0 then 
							iBarHeight = Int((aValues(I) / iMaxValue) * GRAPH_HEIGHT)
						else
							iBarHeight=0
						end if
						' This is a hack since browsers ignore a 0 as an image dimension!
						If iBarHeight = 0 Then iBarHeight = 1
					%>
						<TD VALIGN="bottom"><IMG SRC="spacer.gif" BORDER="0" WIDTH="<%= GRAPH_SPACER %>" HEIGHT="1"></TD>
						<TD VALIGN="bottom"><IMG SRC="color2.gif" BORDER="0" WIDTH="<%= iBarWidth %>" HEIGHT="<%= iBarHeight %>" ALT="<%= aValues(I) %>"></TD>
					<%
					Next 'I
					%>
					</TR>
					<!-- I was using GRAPH_BORDER + GRAPH_WIDTH but it was moving the last x axis label -->
					<TR>
						<TD COLSPAN="<%= (2 * (UBound(aValues) + 1)) + 1 %>"><IMG SRC="color1.gif" BORDER="0" WIDTH="<%= GRAPH_BORDER + ((UBound(aValues) + 1) * (iBarWidth + GRAPH_SPACER)) %>" HEIGHT="<%= GRAPH_BORDER %>"></TD>
					</TR>
				<% ' The label array is optional and is really only useful for small data sets with very short labels! %>
				<% If IsArray(aLabels) Then %>
					<TR> 
						<TD><!-- Spacing for Left Border Column --></TD>
					<% For I = 0 To UBound(aValues)  %>
						<TD><!-- Spacing for Spacer Column --></TD>
						<TD ALIGN="center"><FONT SIZE="1"><%= aLabels(I) %></FONT></TD>
					<% Next 'I %>
					</TR>
				<% End If %>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD COLSPAN="2"><!-- Place holder for X Axis label centering--></TD>
			<TD ALIGN="center"><BR><B><%= strXAxisLabel %></B></TD>
		</TR>
	</TABLE>
	<%
End Sub

sub sendmaxtryemail
	call sendemail("webmaster@cleartone.com","access@cleartoneholdings.co.uk","max login","A user has logged in incorectly after 3 tries")


end sub
function lookuppcn(byVal plate, byRef myerrort)
'this looks up the pcn according to plate
if plate="" then
	myerrort="Plate or Pcn required"
	lookuppcn=""
else
sql="select pcn,site from VIOLATORS where registration=" & tosql(plate,"text")

openrs rs,sql
if rs.bof and rs.eof then
	myerrort="invalid plate"
	lookuppcn=""
else
	lookuppcn=rs("pcn") & rs("site")
end if
closers rs
end if
'response.write myerrort
end function

function lookupsite(byVal spcn, byRef myerrort)
'this looks up the pcn according to plate
if spcn="" then

else
sql="select site from VIOLATORS where pcn=" & tosql(left(spcn,7),"text")

openrs rs,sql
if rs.bof and rs.eof then
	'myerrort="invalid plate"
	'lookuppcn="error"
	lookupsite=""
else
	lookupsite=rs("site")
end if
closers rs
end if
'response.write myerrort
end function


function lookupplate(byVal spcn, byRef myerrort)
'this looks up the plate according to pcn
'if len(spcn)>7 then spcn=left(spcn,7)
if spcn="" then
	myerrort="Plate or Pcn required"
	lookupplate=""
else
	sql="select registration from vwVIOLATORS where pcn=" & tosql(spcn,"text")
	'response.write sql
openrs rs,sql
if rs.bof and rs.eof then
	myerrort="invalid pcn"
	lookupplate=""
else
	lookupplate=rs("registration")
end if
closers rs
end if
end function

sub selectdatebet
'today=dateadd("h",8,now())
today=date()-1
response.write "From:  <select name=nmonthf>"
		j=month(today)
		i=1
		do while i<=12
			m= cstr(i)
				if len(m)<2 then m= "0" & m 
			if i<>j then
				response.write "<option value=" & m & ">" & monthname(i) & "</option>"
			else
				response.write "<option value=" &m& " selected>" & monthname(j) & "</option>"
			end if
			i=i+1	
		loop
		response.write "</select>"
		response.write "<select name=ndayf>"
	 	j=day(today)
		i=1
		do while i<=31
			d=cstr(i)
			if len(d)<2 then d="0" & d
			if i<>j then
							response.write "<option value=" & d & ">" & i & "</option>"
			else
				response.write "<option value=" & d & " selected>" & j & "</option>"
			end if
			i=i+1	
		loop
		response.write "</select>"
		response.write "<select name=nyearf>"
		j=year(today)
		i=2004
		do while i<=2010
			if i<>j then
				response.write "<option value=" & i & ">" & i & "</option>"
			else
				response.write "<option value=" & j & " selected>" & j & "</option>"
			end if
			i=i+1	
		loop
		response.write "</select>"


		response.write " To:  <select name=nmontht>"
		j=month(today)
		i=1
		do while i<=12
			m= cstr(i)
				if len(m)<2 then m= "0" & m 
			if i<>j then
				response.write "<option value=" & m& ">" & monthname(i) & "</option>"
			else
				response.write "<option value=" &m& " selected>" & monthname(j) & "</option>"
			end if
			i=i+1	
		loop
		response.write "</select>"
		response.write "<select name=ndayt>"
	 	j=day(today)
		i=1
		do while i<=31
			d=cstr(i)
			if len(d)<2 then d="0" & d
			if i<>j then
				response.write "<option value=" & d & ">" & i & "</option>"
			else
				response.write "<option value=" & d & " selected>" & j & "</option>"
			end if
			i=i+1	
		loop
		response.write "</select>"
		response.write "<select name=nyeart>"
		j=year(today)
		i=2004
		do while i<=2010
			if i<>j then
				response.write "<option value=" & i & ">" & i & "</option>"
			else
				response.write "<option value=" & j & " selected>" & j & "</option>"
			end if
			i=i+1	
		loop
		response.write "</select>"
end sub

sub search
spcn=request("pcn")
splate=request("plate")

if request("s")=1 then
		sql=session("sql")
		i=1
		rcount=2
	else
		sqlw="SELECT violators.ID, violators.Registration, violators.[Date of Violation], violators.Site, convert(varchar,[arrival time],108), convert(varchar,[departure time],108), convert(varchar,[duration of stay],108), violators.PCN+violators.site as pcn FROM violators where  "
		sqlc="select count(ID) as countc from violators where "
		i=0
		if len(spcn)=10 then
		'check that valid site
		sqlcs="select sitecode from [site information] where sitecode='" & right(spcn,3) & "'"
		
		openrs rscs,sqlcs
		if  rscs.eof and  rscs.bof then
			 response.write "no site information available"
			 exit sub
		closers rscs
		end if
		end if
		if spcn<>"" then
			sql= sql & "(pcn+site like '%" &spcn & "%'"
			'if len(spcn)>7 then sql=sql & " and site like '%" & right(spcn,3) & "%'"
			sql=sql &  ")"
			i=i+1
		end if 
		if splate<>"" then
			if i>0 then sql=sql & " OR "
			sql=sql & "registration like '%" & splate & "%'"
			i=i+1
		end if 
				if i>0 then
			if session("adminsite")=true then
				sqlsites="select * from [site information]"
			else	
			sqlsites="select * from usersites where userid=" & session("userid")
			end if
openrs rssites,sqlsites  ' check the sites he can see 
if rssites.eof and rssites.bof then
	 response.write "YOu do not have access to any site information"
	 response.End
end if

i=1
sql=sql & " and ("
do while not rssites.eof
	if i>1 then sql=sql & " OR "
 	sql=sql & "(site='" &  rssites("sitecode") & "')" 
	i=i+1
	rssites.movenext
loop 
closers rssites
	
		sql=sql & ")"		
				
		sqlc=sqlc& sql
		sql=sqlw & sql
		response.write "<!--" &  sql & "-->"
		openrs rsc,sqlc
		rcount=rsc("countc")
		closers rsc 
		end if
	end if
	if i=0 then 
		response.write "You must fill in a search field"
		call pcn
	else
		
		strpagename="admin.asp?cmd=search"
			
		if rcount=0 then 
			response.write "no records exist with this criteria"
		'elseif rcount=1 then call viewrecord
		else
			if rcount=1 then
			openrs rsa,sql
				call viewrecord(rsa("id"))
			closers rsa
		else
			createSortableList objConn,sql,"ID",25,"class=maintable2w",strpagename
		end if
		end if
		
	end if

end sub
sub search2
spcn=request("pcn")


if request("s")=1 then
		sql=session("sql")
		i=1
		rcount=2
	else
		sqlw="SELECT violators.ID, violators.Registration, violators.[Date of Violation], violators.Site, convert(varchar,[arrival time],108), convert(varchar,[departure time],108), convert(varchar,[duration of stay],108), violators.PCN+violators.site as pcn FROM violators where  "
		sqlc="select count(ID) as countc from violators where "
		i=0
		if len(spcn)=10 then
		'check that valid site
		sqlcs="select sitecode from [site information] where sitecode='" & right(spcn,3) & "'"
		
		openrs rscs,sqlcs
		if  rscs.eof and  rscs.bof then
			 response.write "no site information available"
			 exit sub
		closers rscs
		end if
		end if
		if spcn<>"" then
			sql= sql & "(pcn+site like '%" &spcn & "%'"
			'if len(spcn)>7 then sql=sql & " and site like '%" & right(spcn,3) & "%'"
			sql=sql &  ")"
			i=i+1
		end if 
		if spcn<>"" then
			if i>0 then sql=sql & " OR "
			sql=sql & "registration like '%" & spcn & "%'"
			i=i+1
		end if 
				if i>0 then
			if session("adminsite")=true then
				sqlsites="select * from [site information]"
			else	
			sqlsites="select * from usersites where userid=" & session("userid")
			end if
openrs rssites,sqlsites  ' check the sites he can see 
if rssites.eof and rssites.bof then
	 response.write "YOu do not have access to any site information"
	 response.End
end if

i=1
sql=sql & " and ("
do while not rssites.eof
	if i>1 then sql=sql & " OR "
 	sql=sql & "(site='" &  rssites("sitecode") & "')" 
	i=i+1
	rssites.movenext
loop 
closers rssites
	
		sql=sql & ")"		
				
		sqlc=sqlc& sql
		sql=sqlw & sql
		response.write "<!--" &  sql & "-->"
		openrs rsc,sqlc
		rcount=rsc("countc")
		closers rsc 
		end if
	end if
	if i=0 then 
		response.write "You must fill in a search field"
		call pcn
	else
		
		strpagename="admin.asp?cmd=search"
			
		if rcount=0 then 
			response.write "no records exist with this criteria"
		'elseif rcount=1 then call viewrecord
		else
			if rcount=1 then
			openrs rsa,sql
				call viewrecord(rsa("id"))
			closers rsa
		else
			createSortableList objConn,sql,"ID",25,"class=maintable2w",strpagename
		end if
		end if
		
	end if


end sub
sub paymentssort
where=session("where")
where=replace(where,"PCN","Payments.pcn")
sql="SELECT payments.PCN, violators.registration as plate, PAYMENTS.AMOUNT,violators.id, VIOLATORS.Site, VIOLATORS.[Date of Violation],violators.ticketprice,violators.reducedamount, PAYMENTS.Received, PAYMENTS.METHOD FROM PAYMENTS LEFT JOIN VIOLATORS ON  left(PAYMENTS.PCN,7) = VIOLATORS.PCN"

'if where<>"" then sql= aql & " where "
sql=sql& where
'response.write sql
strpagename="admin.asp?cmd=paymentssort"
createSortableList objConn,sql,"payments.pcn",25,"class=maintable2",strpagename

end sub
sub paymentrepexcel
where=session("where")
where=replace(where,"PCN","Payments.pcn")
sql="SELECT payments.PCN, violators.registration as plate, PAYMENTS.AMOUNT,violators.id, VIOLATORS.Site, VIOLATORS.[Date of Violation],violators.ticketprice,violators.reducedamount, PAYMENTS.Received, PAYMENTS.METHOD FROM PAYMENTS LEFT JOIN VIOLATORS ON  left(PAYMENTS.PCN,7) = VIOLATORS.PCN"

'if where<>"" then sql= aql & " where "
sql=sql& where
call outputexcel(sql)
end sub
sub viewpcn(spcn)
sql="select * from violators where pcn='" & left(spcn,7) & "' and site='" & right(spcn,3) & "'"
openrs rsv,sql
if not rsv.eof then call viewrecord(rsv("id"))

closers rsv

end sub
sub viewrecord(id)
pcncan=0
color="black"
sql="SELECT VIOLATORS.*, DVLA.Make, DVLA.Model, DVLA.Colour, DVLA.Owner, DVLA.Address1, DVLA.Address2, DVLA.Address3, DVLA.Town, DVLA.Postcode, DVLA.[DVLA Status], DVLA.[DateReceived] FROM DVLA RIGHT JOIN VIOLATORS ON DVLA.id = VIOLATORS.dvlaid where violators.id=" &  id
openrs rs,sql

sqlcs="select sitecode from [site information] where sitecode='" & rs("site") & "'"
		response.write "<!--" & sqlcs & "-->"
		openrs rscs,sqlcs
		if  rscs.eof and  rscs.bof then
			 response.write "no site information available"
			 exit sub
		closers rscs
		end if
		
sqlccan="select * from cancelled where pcn='" & rs("pcn") & rs("site") & "'"
openrs rsccan,sqlccan
if not rsccan.eof and not rsccan.bof then
response.write "<div align=center class=canbig>CANCELLED " & rsccan("cancelled") & "<a href=admin.asp?cmd=printcancelpcn&pcn=" & rsccan("pcn") & " class=button target=_new>View Cancellation Form</a></div>"
cancelleddate=rsccan("cancelled")
reason=rsccan("reason")
requestedby=rsccan("requestedby")
authby=rsccan("authby")
reasonother=rsccan("reasonother")

color="red"
pcncan=1
end if
closers rsccan
'checkdebtrecorery
sqld="select * from debtrecovery where pcn=" & tosql(rs("pcn") & rs("site"),"text")
openrs rsd,sqld

if not rsd.eof and not rsd.bof then response.Write "<b>Sent to Debt Recovery on " & rsd("debtrecoverydate") & "</b>"
closers rsd
sqlp="select amount,received,method,bookreference from payments where pcn=" & tosql(rs("pcn") & rs("site"),"text") '& " and right([pcn],3)= " & tosql(rs("site"),"text")
	recpayment=0
	response.write "<!--" & sqlp & "-->"
	openrs rsp,sqlp
	if rsp.eof and rsp.bof then
	response.write "<!--norecord-->"
		amountpaid=0
		received=date()
		
	else
		tamountpaid=0
		recpayment=1
		numpayment=0
do while not rsp.eof
		amountpaid=rsp("amount")
		received=rsp("received")
		strpaidh=strpaidh & "on " & rsp("received")
		strpaid=strpaid & "Paid on " & rsp("received") & "<br>Amount:" & rsp("amount") & "<br>Method of Payment:" & rsp("method") & "<br>Reference #:" & rsp("bookreference") & "<br><br>"
			numpayment=numpayment+1
			tamountpaid=tamountpaid+amountpaid						
			rsp.movenext
		loop
		
		if numpayment>1 then strpaid=strpaid &  "Total Paid:" & tamountpaid
	end if
	response.write  "<!--" & amountpaid & "-->"
	closers rsp
	
	'sqlt="select * from tariff where site=" & tosql(rs("site"),"text")
	'openrs rst,sqlt
		'response.write received
		days=datediff("d",rs("pcnsent"),received)
		'response.write "days:" & days & "+"
		if days>14 then 
		'	response.write rss("pcn") & " is more then 14 datys<br>"
			amountowed=rs("ticketprice")
		else
			amountowed=rs("reducedamount")
		end if
		response.write  "<!--amountowed" & amountowed & "-->"
		'response.write "amount owed for " & rss("pcn") & " is:" & amountowed-amountpaid  & "<bR><hr>"
		amountowed=amountowed-tamountpaid
	if recpayment=1 then 
		response.write "<div align=center class=paidbig>PAID " & strpaidh & "</div>"
		color="blue"
		
	end if
	sqlper="select registration,dateentered from apermits where registration='" & rs("registration") & "'"
	openrs rsper,sqlper
	if not rsper.eof and not rsper.bof then 
		response.write "<div align=center class=paidbig>PERMIT added " & rsper("dateentered") & "</div>"
		
		
	end if
	closers rsper
%>

<table class=maintable2 <% if color<>"" then response.write "id=" & color   %>>
<%
if checkifletteroutstanding(rs("pcn")& rs("site"))=1 then response.write "<tr><td colspan=2><b>Status:Outstanding Communications</b></td></tr>"

%>

<tr><td colspan=2><b>Site Information</b></td></tr>
<tr><th align=left>Plate(Registration)</th><td><%= rs("registration") %></td></tr>
<tr><th align=left>Date of Violation</th><td><%= rs("Date of Violation") %></td></tr>
<tr><th align=left>Site</th><td><%= DLookUp("[Site Information]", "[Name of Site]","sitecode='" & rs("site") & "'") %></td></tr>
<tr><th align=left>Arrival Time</th><td><%= formatdatetime(rs("Arrival Time"),4) %></td></tr>
<tr><th align=left>Departure Time</th><td><%= formatdatetime(rs("Departure Time"),4) %></td></tr>
<tr><th align=left>Duration of Stay</th><td><%=formatdatetime( rs("Duration of Stay"),4) %></td></tr>
<tr><th align=left>PCN</th><td><%= rs("pcn") %><%= rs("site") %></td></tr>
<tr><td colspan=2><hr><b>Vehicle Information</b></td></tr>
<tr><th align=left>Make</th><td><%= rs("Make") %></td></tr>
<tr><th align=left>Colour</th><td><%= rs("Colour") %></td></tr>
<tr><td colspan=2><hr><b>Owner Information</b></td></tr>
<tr><th align=left>Owner</th><td><%= rs("Owner") %></td></tr>
<tr><th align=left>Address1</th><td><%= rs("Address1") %></td></tr>
<tr><th align=left>Address2</th><td><%= rs("Address2") %></td></tr>
<tr><th align=left>Address3</th><td><%= rs("Address3") %></td></tr>
<tr><th align=left>town</th><td><%= rs("town") %></td></tr>
<tr><th align=left>PostCode</th><td><%= rs("Postcode") %></td></tr>
<tr><th align=left>DVLA Status</th><td><%= rs("dvla status") %></td></tr>
<tr><th align=left>DVLA Date Received</th><td><%= rs("datereceived") %></td></tr>
<tr><th align=left>PCN Sent Date</th><td><%= rs("pcnsent") %></td></tr>

<tr><Td colspan=2><hr>
<strong>Paid</strong><br>
<%= strpaid %>
</Td></tr>
<% 
sqldp="select * from declinedpayments where pcn='" & rs("pcn") &  rs("site") & "'"
	'response.Write sqldp
	openrs rsdp,sqldp
	if not rsdp.eof and not rsdp.bof then
	do while rsdp.eof
	response.Write "<tr><td colspan=2>Declined Payment:" & rsdp("reason") & "</td></tr>"
	rsdp.movenext
	loop
	end if 
	
	closers rsdp

'check if reminder sent
reminder=0
sqlrem="select * from reminders where pcn='" & rs("pcn") & "'"
openrs rsrem,sqlrem
if not rsrem.eof and not rsrem.bof then
	reminder=1
	response.write "<tr><th align=left class=highlight>Reminder</th><td class=highlight>" & rsrem("REMINDER") & "</td></tr>"
end if

'' check if owes and if amount outstading and how many days

	
		if amountowed>0 then
			 response.write "<tr><th align=left>Amount Owed:</td><td> " & amountowed & "</tD></tr>"
			 response.write "<tr><th align=left>Days Overdue:</td><td> " & days & "</tD></tr>"
		else
			response.write "<tr><th colspan=2>Paid in Full</td></tr>"
		end if	
	'closers rst
	
	
sqlre="select * from re_issue_data where pcn='" & rs("pcn") & "'"
openrs rsre,sqlre
if not rsre.eof and not rsre.bof then
do while not rsre.eof
response.write "<tr><th><b>Reissued on </b></th><td>" & rsre("pcn_sent") & "<a href=""javascript:popUp('admin.asp?cmd=viewreissue&id=" & rsre("id") & "')"" class=button>View</a></td></tr>"

rsre.movenext
loop
end if
closers rsre
sqlre="select * from reissues where reissuedate is not null and violatorid=" & rs("id")
openrs rsre,sqlre
if not rsre.eof and not rsre.bof then
do while not rsre.eof
response.write "<tr><th><b>Reissued on </b></th><td>" & rsre("reissuedate") & "<a href=""javascript:popUp('admin.asp?cmd=viewreissue2&p=1&id=" & rsre("id") & "')"" class=button>View</a></td></tr>"
rsre.movenext
loop

end if
closers rsre
'lookup users other pcn according to plate within users access site
sqlpc="select * from violators where registration=" & tosql(rs("registration"),"text")
if session("adminsite")=true then
sqlsites="select * from [site information]"
else
sqlsites="select * from usersites where userid=" & session("userid")
end if
openrs rssites,sqlsites  ' check the sites he can see 
if rssites.eof and rssites.bof then
	 response.write "YOu do not have access to any site information"
	 response.End
end if

i=1
sqlpc=sqlpc & " and ("
do while not rssites.eof
	if i>1 then sqlpc=sqlpc & " OR "
 	sqlpc=sqlpc & "(site='" &  rssites("sitecode") & "')" 
	i=i+1
	rssites.movenext
loop 
closers rssites
	
		sqlpc=sqlpc & ") and pcn<>" &tosql(rs("pcn"),"text")

openrs rspc,sqlpc
if not rspc.eof and not rspc.bof then
	response.write "<tr><td colspan=2><b>Other PCN's with this plate: </b><br><font class=small>Click on pcn to view details</font>"
	do while not rspc.eof
		response.write "<br /><a class=link href=admin.asp?cmd=viewrecord&id=" & rspc("id") & ">" & rspc("pcn") & "</a>"
		rspc.movenext
	loop
	response.write "</td></tr>"
 End If 
closers rspc
%>
<tr><td colspan=2 align=center><a href=admin.asp?cmd=viewviolatorticket&id=<%= rs("id") %> target=_new class=button>Print PCN Letter</a></td></tr>

<%if reminder=1 then %>
<tr><td colspan=2 align=center><a href=admin.asp?cmd=printpcnreminder&pcn=<%= rs("pcn") %>&id=<%=rs("id") %> target=_new class=button>Print reminder Letter</a></td></tr>
<% End If %>

<% 'check if there are images
sqlimages="select * from violatorplateimages where imagesexist=1 and violatorid=" & tosql(rs("id"),"Number")
openrs rsimages,sqlimages
if not rsimages.eof and not rsimages.bof then
	%>
	<tr><td colspan=2 align=center><a href=admin.asp?cmd=viewimages&id=<%= rs("id") %>  class=button>View images</a></td></tr>

	<%
end if
closers rsimages
if pcncan=1 then
    response.Write "<tr><td colspan=2>"
Response.Write "Cancelled on " & cancelleddate & "<br>Reason:" & reason & "<br>Requested by:" & requestedby & "<br>Auth By:" & authby
if reasonother<>"" then response.Write "<br>Reason Other:" & reasonother
response.Write "</td></tr>"

end if

%>

 <tr><td><b>Communications:</b></td></tr>
 <%
 sqlap="select * from vwcommunications where violatorid=" & id & " order by date desc"
 'response.write sqlap
openrs rsap,sqlap
 if not rsap.eof and not rsap.bof then
 do while not rsap.eof
 slettertype=rsap("type")
 if slettertype="Letter" then
 'response.write "here"
 sqll="select * from generalletters left join lettertypes on lettertypes.id=generalletters.lettertypeid where generalletters.id=" & rsap("id")
 openrs rsl,sqll
slettertype="Letter(" & rsl("type") & ")"
closers rsl 
 end if
 link=rsap("link")
 showlink=1
 if slettertype="Incoming Letter" then
     showlink=0
    set fs=Server.CreateObject("Scripting.FileSystemObject")
    if fs.FileExists(configwebdir & "traffica\" & link) = true then
        showlink=1
     else
     ''   response.Write"<tr><td colspan=2>Incoming Letter on " &  rsap("date") & " - letter is missing from " & servername & " - admin has been notified"
     
      ' call sendemail("webmaster@cleartone.com","ec@awebforyou.com","incomingletter","incoming letter seems to be missing from " & servername & ":" & link & vbcrlf & "userid:" & session("userid"))
     '  call sendemail("webmaster@cleartone.com","simon.a@cleartoneholdings.co.uk","incomingletter","incoming letter seems to be missing from " & servername & ":" & link & vbcrlf & "userid:" & session("userid"))
      link="hardcopy.html"
     showlink=1
     end if
     set fs=nothing
 end if
 if showlink=1 then
 	%>
	<tr><td colspan=2 align=left><a target=_new href=<%=link %>  class=button>View <%=slettertype%> dated <%=rsap("date") %></a>
	
	<%
	end if
	if rsap("statusid")<>0 and rsap("statusid")<>1 then
		sstatus=getletterstatus(rsap("statusid"))
	'	response.write "Status:" & sstatus
	if rsap("statusid")<>2 and rsap("statusid")<>1 then
	%>
	<a href=admin.asp?cmd=changeletterstatus&letterid=<%=rsap("id")%>&pcn=<%=rsap("pcn")%>&p=1  target=_new class=button>Change Status</a>
			
	<%
	end if
	end if
	%>
	</td></tr>

	<%
	 rsap.movenext
loop
 end if 
 closers rsap
 %>
<tr><td><a href="admin.asp?cmd=createletter&id=<%=rs("id")%>" class=button target=_new>Create Communication</a>
<%
spcn=rs("pcn") & rs("site")
sqlap="select * from appeals where pcn=" & tosql(spcn,"text")
 'response.write sqlap
openrs rsap,sqlap
 if not rsap.eof and not rsap.bof then
 response.write "<tr><Td colspan=2><table><tr><th colspan=2>Voice Messages</th></tr>"
 do while not rsap.eof
 %>
	<tr><td colspan=2 align=center>Voice Message on  <%=rsap("mydatetime") %> <a href=admin.asp?cmd=viewappeal&id=<%=rsap("id") %> class=button target=_new>View Message</a></td></tr>

	<%
 rsap.movenext
loop
response.write "</table></td></tr>"
 end if 
 closers rsap
 %>
 <tr><td colspan=2 align=center>
 <a href="#" onclick="javascript:window.open('admin.asp?cmd=pcnnotes&pcn=<%= rs("pcn") %><%= rs("site") %>','popup','width=500,height=400');" class=button>Add a Note for this PCN</a>
 <!--<a href="" onclick="return popitup('admin.asp?cmd=pcnnotes&pcn=<%= rs("pcn") %><%= rs("site") %>')" class=button>Add a Note for this PCN</a>-->
 
 </td></tr>
 
 <% 
 
 sqlp="select * from manualpayments where pcn='" & rs("pcn") & rs("site") & "' order by date"
 'response.Write sqlp
 openrs rsp,sqlp
do while not rsp.eof
    response.Write "<tr><td colspan=2>Manual Payment entered on "  & rsp("date") & " " & rsp("errorcode") & " Amount:" & rsp("amount") &"</td></tr>"
rsp.movenext
loop
 
 closers rsp
 
 
 
 sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='addccpayment'"
	openrs rsm2,sqlm2
	if  not rsm2.eof and  not rsm2.bof then
	%>
	<tr><td colspan=2 align=center><a class=button href=admin.asp?cmd=addccpayment&pcn=<%=rs("pcn") & rs("site") %>>Add Manul Credit card Payment</a></td></tr>
	
	
 <%
 end if 
 closers rsm2
 
 sqlck="select * from declinedpayments where pcn='" & rs("pcn") & rs("site") & "'"
 openrs rsck,sqlck
 if not rsck.eof and not rsck.bof then
 response.write "<tr><td colspan=2><b>Declined Payments</b><br>"
 do while not rsck.eof
 response.write "Amount of " & rsck("amount") & " was declined on " & rsck("mydate") & " reason:" & rsck("reason") & " Phone:" & rsck("phone") &"<br>"
 
 rsck.movenext
 loop
 
 end if
 closers rsck
 %><tr><td colspan=2>
 
 <%
'response.Write "aa"
sqlsigns="select signs from [site information] where sitecode=" & tosql(rs("site"),"text")
openrs rssigns,sqlsigns
if not rssigns.bof and not rssigns.eof then response.Write "Signs:" & rssigns("signs")

closers rssigns
 %>

 
 
 </td></tr>
<tr><td colspan=2>
<%
call shownotes(rs("pcn") & rs("site"))
%>
 </td></tr>

</table>
<%

 'call showsiterules(rs("site")) 
closers rs

end sub
sub viewreissue
response.write "<table class=maintable2>"
sqlre="select * from re_issue_data where id=" & request("id")
openrs rs,sqlre
response.write "<tr><th>ID</th><td>" & rs("id") & "</td></tr>"
response.write "<tr><th>PCN sent</th><td>" & rs("pcn_sent") & "</td></tr>"
response.write "<tr><th>Owner</th><td>" & rs("owner") & "</tr></tr>"
response.write "<tr><th>Address1</th><td>" & rs("address1") & "</tr></tr>"
response.write "<tr><th>Town</th><td>" & rs("town") & "</tr></tr>"
response.write "<tr><th>PostCode</th><td>" & rs("postcode") & "</tr></tr>"
closers rs
response.write "</table>"
end sub
sub viewreissue2
response.write "<table class=maintable2>"
sqlre="select * from reissues r left join violatorreissue v on r.id=v.reissueid left join dvla d on v.olddvlaid=d.id where  r.id=" & request("id")
openrs rs,sqlre
response.write "<tr><Td>Previously issued to</td></tr>"
response.write "<tr><th>ID</th><td>" & rs("id") & "</td></tr>"
response.write "<tr><th>Ref Number</th><td>" & rs("refnumber") & "</td></tr>"

response.write "<tr><th>PCN sent</th><td>" & rs("origsentdate") & "</td></tr>"
response.write "<tr><th>Owner</th><td>" & rs("owner") & "</tr></tr>"
response.write "<tr><th>Address1</th><td>" & rs("address1") & "</tr></tr>"
response.write "<tr><th>Town</th><td>" & rs("town") & "</tr></tr>"
response.write "<tr><th>PostCode</th><td>" & rs("postcode") & "</tr></tr>"
closers rs

response.write "</table>"

end sub
sub viewimages
response.write "<table class=maintable2><tr><td>"
sqlimages="select * from violatorplateimages where imagesexist=1 and violatorid=" & tosql(request("id"),"Number")
openrs rsimages,sqlimages
if not rsimages.eof and not rsimages.bof then
	response.write "<tr><td colspan=2><b>Plate Images:</b><br>"
	do while not rsimages.eof
	response.Write rsimages("plate") & "-" & rsimages("date")
	site=dlookup("violators","site","id=" & rsimages("violatorid"))
	itemimageurl=configweburl &  "violatorplateimages\" & site & "\" & year(rsimages("date")) & "\" & month(rsimages("date")) & "\" & day(rsimages("date")) & "\" & hour(rsimages("date")) & "\"
    filename=rsimages("picture")
     filename2=rsimages("picture2")
      filename3=rsimages("picture3")
      
  imagesmissing=0
       set fs=Server.CreateObject("Scripting.FileSystemObject")
    if not fs.FileExists(configwebdir & "traffica\violatorplateimages\" & site & "\" &year(rsimages("date")) &  "\" & month(rsimages("date")) & "\" & day(rsimages("date")) & "\" & hour(rsimages("date")) & "\" & filename) = true then imagesmissing=1
        if not fs.FileExists(configwebdir & "traffica\violatorplateimages\" & site & "\"&year(rsimages("date")) & "\" & month(rsimages("date")) & "\" & day(rsimages("date")) & "\" & hour(rsimages("date")) & "\" & filename2) = true then imagesmissing=1
          '  if  not fs.FileExists(configwebdir & "traffica\violatorplateimages\" & site & "\" &year(rsimages("date"))& "\" & month(rsimages("date")) & "\" & day(rsimages("date")) & "\" & hour(rsimages("date")) & "\" & filename3) = true then imagesmissing=1
    
      if imagesmissing=1  then
      call sendemail("webmaster@cleartone.com","ec@awebforyou.com","violatorplateimages","violator plate images seems to be missing from " & servername & " for violatorid:" & request("id"))
        call sendemail("webmaster@cleartone.com","simon.a@cleartoneholdings.co.uk","violatorplateimages","violator plate images seems to be missing from " & servername & " for violatorid:" & request("id"))
      
      end if
   itemimagesrc="<img src='" & itemimageurl & filename & "' " & Replace(ImageResize(itemimage & filename,400, 120),"""","'") & " border='0' align='middle' valign='middle'>"
   itemimagesrc2="<img src='" & itemimageurl & filename2 & "' " & Replace(ImageResize(itemimage & filename2,500, 250),"""","'") & " border='0' align='middle' valign='middle'>"
   if filename3<>"" then itemimagesrc3="<img src='" & itemimageurl & filename3 & "' " & Replace(ImageResize(itemimage & filename3,500, 250),"""","'") & " border='0' align='middle' valign='middle'>"
   response.Write itemimagesrc & "<br>" & itemimagesrc2 & "<br>" & itemimagesrc3 & "<br>"
	
	rsimages.movenext
	loop
end if
closers rsimages
response.write "<br><br></td></tr></table>"
end sub
sub printpcnletter
  sqlpl="SELECT VIOLATORS.Registration, VIOLATORS.[Date of Violation], [Site Information].Borough, VIOLATORS.Site, VIOLATORS.TicketPrice, VIOLATORS.ReducedAmount, [Site Information].[Name of Site], [Site Information].Address1, [Site Information].Address2, [Site Information].Address3, [Site Information].Town, [Site Information].Postcode, DVLA.Make, DVLA.Model, DVLA.Colour, DVLA.Owner, DVLA.Address1, DVLA.Address2, DVLA.Address3, DVLA.Town, DVLA.Postcode, VIOLATORS.PCN+[Site Information].SiteCode AS Expr1, VIOLATORS.[Arrival Time], VIOLATORS.[Arrival Time], VIOLATORS.[Departure Time], VIOLATORS.PCNSENT, VIOLATORS.PCN FROM (DVLA INNER JOIN VIOLATORS ON DVLA.Reg = VIOLATORS.Registration) INNER JOIN [Site Information] ON VIOLATORS.Site = [Site Information].SiteCode WHERE (((DVLA.Make) Is Not Null) AND ((VIOLATORS.PCN)='" & request("pcn") & "'))"'AND (([VIOLATORS].[PCN]+[Site Information].[SiteCode]) Is Not Null))"
'response.write sqlpl
response.write sqlpl
set  rspl=objconn.execute(sqlpl)
if not rspl.eof and not rspl.bof then
	response.redirect "pcnletter.asp?pcn=" & request("pcn")
else
	response.write "no pcn letter"
end if
end sub
sub zprintpcnletter
sqlpl="SELECT VIOLATORS.Registration, VIOLATORS.[Date of Violation], [Site Information].Borough, VIOLATORS.Site, [Site Information].[Name of Site], [Site Information].Address1, [Site Information].Address2, [Site Information].Address3, [Site Information].Town, [Site Information].Postcode, DVLA.Make, DVLA.Model, DVLA.Colour, DVLA.Owner, DVLA.Address1, DVLA.Address2, DVLA.Address3, DVLA.Town, DVLA.Postcode, Tariff.[Reduced Amount], Tariff.[Ticket Price], VIOLATORS.PCN+[Site Information].SiteCode AS Expr1, VIOLATORS.[Arrival Time], VIOLATORS.[Arrival Time], VIOLATORS.[Departure Time], VIOLATORS.PCNSENT, VIOLATORS.PCN FROM Tariff INNER JOIN ((DVLA INNER JOIN VIOLATORS ON DVLA.Reg = VIOLATORS.Registration) INNER JOIN [Site Information] ON VIOLATORS.Site = [Site Information].SiteCode) ON Tariff.Site = [Site Information].SiteCode WHERE (((DVLA.Make) Is Not Null) AND ((VIOLATORS.PCN)='" & request("pcn") & "'))"'AND (([VIOLATORS].[PCN]+[Site Information].[SiteCode]) Is Not Null))"
'response.write sqlpl
openrs rspl,sqlpl
if not rspl.eof and not rspl.bof then
response.write "<div class=pcnletter>"


response.write "<div class=mailaddress>"
response.write "<font class=bigred> " & rspl("Borough") & "</font><br>"
response.write rspl("owner")& "<br>"
response.write rspl("address1")& "<br>"
response.write rspl("address2")& "<br>"
response.write rspl("address3")& "<br>"
response.write rspl("town")& "<br>"
response.write rspl("postcode")& "<br>"
response.write "</div><div class=pcndet>"
response.write rspl("registration")& "<br>"
response.write rspl("make")& "<br>"
response.write rspl("model")& "<br>"
response.write rspl("colour")& "<br>"
response.write rspl("expr1")& "<br>"
response.write rspl("pcnsent")& "<br>"
response.write rspl("date of violation")
response.write "</div>"
response.write "<div class=box2><span class=dateofvil>"
response.write rspl("date of violation") & "</span><br>"
response.write "<span class=nameofsite>"& rspl("name of site") & "</span><br>"
response.write "<span class=from>" & rspl("arrival time") & "</span><span class=to>" & rspl("departure time") &"</span>"
response.write "<span class=lettotalamount>£" & rspl("ticketprice") &"</span>"
response.write "<span class=letreducedamount><b>£" & rspl("reducedamount") & "</b></span>"
response.write "<span class=letparkingcharge>£" & rspl("ticketprice") & "</span><span class=letreducedamount2><b>£" & rspl("reduced amount") & "</b></span>"
response.write "<span class=letslip>"
response.write rspl("registration") & "<br>"
response.write rspl("expr1") & "<br>"
response.write rspl("pcnsent") & "</span>"
response.write "</div>"
response.write "<div class=pcnprint><input type='button' value='Print' onClick='window.print()' class=noPrint></div>"
end if
closers rspl
end sub


sub printpcnreminder
 id=request("id")
set fs=Server.CreateObject("Scripting.FileSystemObject")
if fs.FileExists(configwebdir & "\remindertickets\" & id & ".pdf")=true then
response.Redirect configweburl & "\remindertickets\" & id & ".pdf"
else
    
  
   call printreminder(id)
   response.Redirect configweburl & "\remindertickets\" & id & ".pdf"
   
end if
set fs=nothing
 if 1=0 then
 sqlpl="SELECT VIOLATORS.Registration, VIOLATORS.[Date of Violation], [Site Information].Borough, VIOLATORS.Site, VIOLATORS.TicketPrice, VIOLATORS.ReducedAmount, [Site Information].[Name of Site], [Site Information].Address1, [Site Information].Address2, [Site Information].Address3, [Site Information].Town, [Site Information].Postcode, DVLA.Make, DVLA.Model, DVLA.Colour, DVLA.Owner, DVLA.Address1, DVLA.Address2, DVLA.Address3, DVLA.Town, DVLA.Postcode, VIOLATORS.PCN+[Site Information].SiteCode AS Expr1, VIOLATORS.[Arrival Time], VIOLATORS.[Arrival Time], VIOLATORS.[Departure Time], VIOLATORS.PCNSENT, VIOLATORS.PCN FROM (DVLA INNER JOIN VIOLATORS ON DVLA.Reg = VIOLATORS.Registration) INNER JOIN [Site Information] ON VIOLATORS.Site = [Site Information].SiteCode WHERE (((DVLA.Make) Is Not Null) AND ((VIOLATORS.PCN)='" & request("pcn") & "'))"'AND (([VIOLATORS].[PCN]+[Site Information].[SiteCode]) Is Not Null))"
'response.write sqlpl
openrs rspl,sqlpl

response.write "<div class=pcnreminder>"


response.write "<div class=remmailaddress>"
response.write "<font class=bigred> " & rspl("Borough") & "</font><br>"
response.write rspl("owner")& "<br>"
response.write rspl("address1")& "<br>"
response.write rspl("address2")& "<br>"
response.write rspl("address3")& "<br>"
response.write rspl("town")& "<br>"
response.write rspl("postcode")& "<br>"
response.write "</div><div class=remdet>"
response.write rspl("registration")& "<br>"
response.write rspl("make")& "<br>"
response.write rspl("model")& "<br>"
response.write rspl("colour")& "<br>"
response.write rspl("expr1")& "<br>"
response.write rspl("pcnsent") & "<br>"
response.write rspl("date of violation")
response.write "</div>"
response.write "<div class=rembox2>"
'response.write rspl("date of violation") & "<br>"
response.write formatdatetime(date,2) & "<br>"
'response.write "<span class=remnameofsite>"& rspl("name of site") & "</span><br>"
'response.write "<span class=remfrom>" & rspl("arrival time") & "</span><span class=remto>" & rspl("departure time") &"</span>"
response.write "<span class=remtotalamount>£" & rspl("ticketprice") &"</span>"
'response.write "<span class=remreducedamount><b>£" & rspl("reduced amount") & "</b></span>"
'response.write "<span class=remparkingcharge>£" & rspl("ticket price") & "</span><span class=remreducedamount2><b>£" & rspl("reduced amount") & "</b></span>"
response.write "<span class=remslip>"
response.write rspl("registration") & "<br>"
response.write rspl("expr1") & "<br>"
response.write rspl("pcnsent") 
response.write "</span>"
response.write "</div>"
response.write "<div class=pcnprint><input type='button' value='Print' onClick='window.print()' class=noPrint></div>"

closers rspl
end if
end sub

sub zprintpcnreminder
sqlpl="SELECT VIOLATORS.Registration, VIOLATORS.[Date of Violation], [Site Information].Borough, VIOLATORS.Site, [Site Information].[Name of Site], [Site Information].Address1, [Site Information].Address2, [Site Information].Address3, [Site Information].Town, [Site Information].Postcode, DVLA.Make, DVLA.Model, DVLA.Colour, DVLA.Owner, DVLA.Address1, DVLA.Address2, DVLA.Address3, DVLA.Town, DVLA.Postcode, Tariff.[Reduced Amount], Tariff.[Ticket Price], VIOLATORS!PCN+[Site Information]!SiteCode AS Expr1, VIOLATORS.[Arrival Time], VIOLATORS.[Arrival Time], VIOLATORS.[Departure Time], VIOLATORS.PCNSENT, VIOLATORS.PCN FROM Tariff INNER JOIN ((DVLA INNER JOIN VIOLATORS ON DVLA.Reg = VIOLATORS.Registration) INNER JOIN [Site Information] ON VIOLATORS.Site = [Site Information].SiteCode) ON Tariff.Site = [Site Information].SiteCode WHERE (((DVLA.Make) Is Not Null) AND ((VIOLATORS.PCN)='" & request("pcn") & "') AND (([VIOLATORS]![PCN]+[Site Information]![SiteCode]) Is Not Null))"
'response.write sqlpl
openrs rspl,sqlpl

response.write "<table border=0 width='75%'>"
response.write "<tr><td><font class=bigred>" & rspl("Borough") & "</td></tr>"
response.write "<tr><td align=left valign=top>"
response.write rspl("owner")& "<br>"
response.write rspl("dvla.address1")& "<br>"
response.write rspl("dvla.address2")& "<br>"
response.write rspl("dvla.address3")& "<br>"
response.write rspl("dvla.town")& "<br>"
response.write rspl("dvla.postcode")& "<br>"
response.write "</td><td valign=top>"
response.write rspl("registration")& "<br>"
response.write rspl("make")& "<br>"
response.write rspl("model")& "<br>"
response.write rspl("colour")& "<br>"
response.write rspl("expr1")& "<br>"
response.write formatdatetime(date,2)& "<br>"
response.write rspl("date of violation")& "<br>"
response.write "</td></tr>"
response.write "<tr><td>"
response.write rspl("date of violation") & "<br>"
response.write rspl("name of site") & "<br>"
response.write rspl("arrival time") & "&nbsp;&nbsp;&nbsp;&nbsp;" & rspl("departure time") &"<br><br><br>"
response.write rspl("ticket price") &"</td></tr>"
response.write "<tr><td></td><td><b>" & rspl("reduced amount") & "</b></td></tr>"
response.write "<tr><td colspan=2 align=center>" & rspl("ticket price") & "<br><b>" & rspl("reduced amount") & "</b></td></tr>"
response.write "<tr><td></td><td>"
response.write rspl("registration") & "<br><br>"
response.write rspl("expr1") & "<br><br>"
response.write formatdatetime(date,2) & "<br></td></tr>"
response.write "</table>"
response.write "<input type='button' value='Print' onClick='window.print()' class=noPrint>"

closers rspl
end sub

sub adminlogout
id=session("userid")
if id="" then id=request("id")
'sql="update useraccess set isonline=0 where id=" & id
'objconn.execute sql
session.Abandon()
response.write "You have logged out"
call loginform

end sub

sub getsiteoptions

if session("adminsite")=true then
sqlsite="select * from [site information]"
else
sqlsite="select * from usersites where userid=" & cint(session("userid"))
end if
'response.write sqlsite
openrs rssites,sqlsite  ' check the sites he can see 
if rssites.eof and rssites.bof then
	 response.write "YOu do not have access to any site information"
	 response.End
end if

i=1
'sql="("
do while not rssites.eof
	response.write "<option value=" & rssites("sitecode") & ">" & rssites("sitecode") & "</option>"
	rssites.movenext
loop 
closers rssites

end sub
sub poutstandingappeals
'response.write "server date" & now()
'sql="select * from appeals where outcome is null order by mydatetime" 'and right(pcn,3) in(select * from usersites where userid=" & cint(session("userid")) & ")"
sql="select pcn, count(*) as num_of_records, max(mydatetime) as mydatetime from appeals where outcome is null group by pcn order by mydatetime desc"



openrs rs,sql
i=0
if not rs.eof and not rs.bof then
s= "<table class=""sortable"" id=""mytable"">" '<tr><td colspan=4 align=center><b>Outstanding Messages</b></tr></tr>"
s=s &  "<tr><th>PCN</th><th>Date/time of call</th><th colspan=2>Time since call</th></tr>"
do while not rs.eof
	'check if valid sit
	if session("adminsite")=true then

sqlck="select * from [site information] where sitecode='" & right(rs("pcn"),3) & "'"

else
	sqlck="select * from usersites where userid=" & cint(session("userid")) & " and sitecode='" & right(rs("pcn"),3) & "'"

end if	
	openrs rsck,sqlck
	if not rsck.eof and not rsck.bof then '(site exists and is allowed)
	s=s& "<tr><td>" & rs("pcn") 
	if rs("num_of_records")>0 then s=s & "(" & rs("num_of_records") & ")"
	s=s & "</td><td>" & rs("mydatetime") & "</td><td>"  & formatminutes(DateDiff("n",rs("mydatetime"),now())) &  "</td><td><a target=_new href=admin.asp?cmd=viewappealbypcn&pcn=" & rs("pcn") & " class=button>View</a></td></tr>"
	i=i+1
	end if
	closers rsck
	rs.movenext
loop

s=s & "</table>"
if i>0 then response.write s
end if
closers rs
end sub
function formatminutes(mints)
formatminutes=Formatnumber(Mints \ 60, "0") & ":" & Formatnumber(Mints Mod 60, "00") 
end function


sub viewappeal
id=request("id")
sql="select * from appeals where id=" & id
openrs rs,sql
mydate=rs("mydatetime")
datename=year(mydate) & "-" & m2(month(mydate)) & "-" & m2(day(mydate)) & m2(hour(mydate)) & m2(minute(mydate)) & m2(second(mydate))
'response.Write rs("pcn") & "_" &  datename & ".wav"
'datename=replace(datename,".000","")
'datename=replace(datename," ","")
'datename=replace(datename,":","")
comments=rs("comments")
outcome=rs("outcome")
spcn=rs("pcn")
%>
<form method=post action=admin.asp?cmd=updateappeal><input type=hidden name=id value=<%= id %>>
<table class=maintable2>
<tr><td colspan=2>
<object
height="50"
width="100"
classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95">
<param name="AutoStart" value="1" />
<param name="FileName" value="appeals/<%= rs("pcn") %>_<%= datename %>.wav" />
</object>

</td></tr>
<tr><td valign=top>Comments:</td><td><textarea name=comments rows=15 cols=70><%=comments %></textarea></td></tr>
<tr><td valgin=top>Outcome:</td><Td><select name=outcome>
<option value="">Select</option>
<option value="Left Message" <% if outcome="Left Message" then response.write " selected" %>>Left Message</option><option value="No Answer" <% if outcome="No Answer" then response.write " selected" %>>No Answer</option><option value="Unavailable" <% if outcome="Unavailable" then response.write " selected" %>>Unavailable</option>
<option value=Spoken <% if outcome="Spoken" then response.write " selected" %>>Spoken</option>
<option value=other <% if outcome="Other" then response.write " selected" %>>Other</option>

</select></Td></tr>

<tr><td colspan=2 align=center><input type=submit name=submit value=Submit class=button></td></tr>

</table>
</form>
<%
closers rs
call viewpcn(spcn)
end sub

sub viewappealbypcn
mypcn=request("pcn")
sql="select * from appeals where outcome is null and pcn=" & tosql(mypcn,"text") & " order by mydatetime"
openrs rs,sql
do while not rs.eof
mydate=rs("mydatetime")
datename=year(mydate) & "-" & m2(month(mydate)) & "-" & m2(day(mydate)) & m2(hour(mydate)) & m2(minute(mydate)) & m2(second(mydate))
'response.Write rs("pcn") & "_" &  datename & ".wav"
'datename=replace(datename,".000","")
'datename=replace(datename," ","")
'datename=replace(datename,":","")
comments=rs("comments")
outcome=rs("outcome")
spcn=rs("pcn")
id=rs("id")
%>

<table class=maintable2>
<tr><td>
<object
height="50"
width="100"
classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95">
<param name="AutoStart" value="0" />
<param name="FileName" value="appeals/<%= rs("pcn") %>_<%= datename %>.wav" />
</object>

</td><td>Message left at <%=mydate %></td></tr>

<%
rs.movenext
loop
closers rs
%>
<form method=post action=admin.asp?cmd=updateappeal><input type=hidden name=id value=<%= id %>>
<input type=hidden name=pcn value=<%= spcn %>>
<table class=maintable2>

<tr><td valign=top>Comments:</td><td><textarea name=comments rows=15 cols=70><%=comments %></textarea></td></tr>
<tr><td valgin=top>Outcome:</td><Td><select name=outcome>
<option value="">Select</option>
<option value="Left Message" <% if outcome="Left Message" then response.write " selected" %>>Left Message</option><option value="No Answer" <% if outcome="No Answer" then response.write " selected" %>>No Answer</option><option value="Unavailable" <% if outcome="Unavailable" then response.write " selected" %>>Unavailable</option>
<option value=Spoken <% if outcome="Spoken" then response.write " selected" %>>Spoken</option>
<option value=other <% if outcome="Other" then response.write " selected" %>>Other</option>

</select></Td></tr>

<tr><td colspan=2 align=center><input type=checkbox name=action value=yes />Action<br /><input type=submit name=submit value=Submit class=button></td></tr>

</table>
</form>

<%
call viewpcn(spcn)
end sub
sub viewrm
id=request("id")
sql="select * from rm where id=" & id
openrs rs,sql
datename=replace(rs("mydatetime"),"/","-")
datename=replace(datename," ","_")
datename=replace(datename,":",".")
%>

<table class=maintable2>
<tr><td colspan=2>
<object
height="50"
width="100"
classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95">
<param name="AutoStart" value="1" />
<param name="FileName" value="rc/<%= rs("pcn") %>_<%= datename %>.wav" />
</object>

</td></tr>

</table>
</form>
<%
closers rs
end sub

sub updateappeal
id=request("id")
outcome=request("outcome")
'need to deal with cancelled outcome here

sql="update appeals set updatetoclients=1,comments=" &tosql(request("comments"),"text") & ",outcome=" & tosql(outcome,"text") & ",fileno=" & tosql(request("fileno"),"text") & ",timesaved=getdate(),userid=" & session("userid") 
if request("action")="yes" then sql=sql & ",action=1,actiondate=getdate(),actionuserid=" & session("userid")
sql=sql & " where id=" & id
objconn.execute sql
sql="update appeals set outcome='duplicate message' where outcome is null and pcn=" & tosql(request("pcn"),"text")
objconn.execute sql

response.write "appeal updated"
if outcome="Cancelled" then
%><br><br>
<a href="admin.asp?cmd=printcancelledletter&id=<%= id %>" class=button target=_new>Would you like to print a letter</a><br><br>
<%
end if

end sub
sub printcancelledletter
sql="update appeals set updatetoclients=1,printedletter=1 where id=" & request("id")
aobjconn.execute sql
sqlap="select * from appeals where id=" & request("id")
openrs rsap,sqlap
spcn=left(rsap("pcn"),7)
site=right(rsap("pcn"),3)

closers rsap



sqlpl="SELECT VIOLATORS.Registration, VIOLATORS.PCNSENT, VIOLATORS.[Date of Violation], [Site Information].Borough, VIOLATORS.Site, [Site Information].[Name of Site], [Site Information].Address1, [Site Information].Address2, [Site Information].Address3, [Site Information].Town, [Site Information].Postcode, DVLA.Make, DVLA.Model, DVLA.Colour, DVLA.Owner, DVLA.Address1, DVLA.Address2, DVLA.Address3, DVLA.Town, DVLA.Postcode, VIOLATORS!PCN+[Site Information]!SiteCode AS Expr1, VIOLATORS.[Arrival Time], VIOLATORS.[Arrival Time], VIOLATORS.[Departure Time], VIOLATORS.PCNSENT, VIOLATORS.PCN, VIOLATORS.TicketPrice, VIOLATORS.ReducedAmount FROM (DVLA INNER JOIN VIOLATORS ON DVLA.Reg = VIOLATORS.Registration) INNER JOIN [Site Information] ON VIOLATORS.Site = [Site Information].SiteCode WHERE (((DVLA.Make) Is Not Null) AND ((VIOLATORS.PCN)='" &spcn & "') AND (([VIOLATORS]![PCN]+[Site Information]![SiteCode]) Is Not Null))"

openrs rspl,sqlpl

response.write "<div class=cancletter><div align=left>"
response.write rspl("owner")& "<br>"
response.write rspl("dvla.address1")& "<br>"
response.write rspl("dvla.address2")& "<br>"
response.write rspl("dvla.address3")& "<br>"
response.write rspl("dvla.town")& "<br>"
response.write rspl("dvla.postcode")& "<br>"
%>
<div></div>
<div align=right><%= formatdatetime(date,1) %></div>
<div align=left>
<p>Dear Sir/Madam,</p>
<p>Your parking charge appeal <%=spcn %><%= site %></p>
<p>We refer to your correspondence in respect of the Parking Charge Notice that 
  we recently issued to you.</p>
<p>We are pleased to advise that this ticket has now been cancelled.</p>
<p><br>
  Yours faithfully,</p>
<p><br>
  Civil Enforcement<br>
</p>
</div></div>
<%
response.write "<div><input type='button' value='Print' onClick='window.print()' class=noPrint></div>"

end sub


sub viewpcnappeal
spcn=request("pcn")
sql="select * from appeals where pcn=" & tosql(spcn,"text")
openrs rs,sql
if not rs.eof and not rs.bof then
	datename=replace(rs("mydatetime"),"/","-")
	datename=replace(datename," ","_")
	datename=replace(datename,":",".")
	%>
	
	<table class=maintable2>
	<tr><td colspan=2>
	<object
	height="50"
	width="100"
	classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95">
	<param name="AutoStart" value="1" />
	<param name="FileName" value="appeals/<%= rs("pcn") %>_<%= datename %>.wav" />
	</object>
	
	</td></tr>
	<tr><td valign=top>Comments:</td><td><%= rs("comments") %></td></tr>
	<tr><td valgin=top>Outcome:</td><Td><%= rs("outcome") %></Td></tr>
	<tr><td>File No:</td><td><%= rs("fileno") %></td></tr>
	<% if rs("printedletter")=true then %>
	<a href="admin.asp?cmd=printcancelledletter&id=<%= rs("id") %>" class=button>Print Cancel Letter</a>
	<% End If %>
	
	</table>
	</form>
<%
else
response.write "invalid pcn"
end if
closers rs
end sub

sub zviewappeal
id=request("id")
sql="select * from appeals where id=" & id
openrs rs,sql
if not rs.eof and not rs.bof then
	datename=replace(rs("mydatetime"),"/","-")
	datename=replace(datename," ","_")
	datename=replace(datename,":",".")
	%>
	
	<table class=maintable2>
	<tr><td colspan=2>
	<object
	height="50"
	width="100"
	classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95">
	<param name="AutoStart" value="1" />
	<param name="FileName" value="appeals/<%= rs("pcn") %>_<%= datename %>.wav" />
	</object>
	
	</td></tr>
	<tr><td valign=top>Comments:</td><td><%= rs("comments") %></td></tr>
	<tr><td valgin=top>Outcome:</td><Td><%= rs("outcome") %></Td></tr>
	<tr><td>File No:</td><td><%= rs("fileno") %></td></tr>
	<% if rs("printedletter")=true then %>
	<a href="admin.asp?cmd=printcancelledletter&id=<%= rs("id") %>" class=button>Print Cancel Letter</a>
	<% End If %>
	
	</table>
	</form>
<%
else
response.write "invalid appeal"
end if
closers rs
end sub

sub viewpermit(permitid)
sql="select * from apermits where id=" & permitid
openrs rs,sql
if rs("userid")=session("userid") and rs("site")="all" then
' allowed to view
canview=1
else
	if session("adminsite")=true then
sqlsites="select * from [site information]"
else
	sqlsites="select * from usersites where userid=" & session("userid") & " and sitecode='" & rs("site") & "'"
end if	
				openrs rssites,sqlsites 
								 ' check the sites he can see 
								 
			if rssites.eof and rssites.bof then
				canview=0
			else
				canview=1
			end if
end if
if canview=0 then
	response.write "no permisison to view"
				response.end
end if
			'Registration, Valid_Date, Temp, Expiry_Date, Site, Other_Info, FILE_PAGE, Created, dateentered, ip, userid, employee, type, profile, disabled, reason, vehiclecolour, driversname, vehicletype, id			 
	%>

	<table class=maintable2 width=400>
	<tr><th valign=top>Plate</th><td><%= rs("registration") %></td></tr>
	<tr><th valign=top>Start Date</th><td><%= rs("valid_date") %></td></tr>
	<tr><th valign=top>Expiry Date</th><td><%= rs("expiry_date") %></td></tr>
	<tr><th valign=top>Date Entered</th><td><%= rs("dateentered") %></td></tr>
	<tr><th valign=top>Type</th><td><%= rs("type") %></td></tr>
	<tr><th valign=top>File Page</th><td><%= rs("file_page") %></td></tr>
	<tr><th valign=top>Site</th><td><%= rs("site") %></td></tr>
	<tr><th valign=top>Reason</th><td><%= rs("reason") %></td></tr>
	<tr><th valign=top>Vehicle Type</th><td><%= rs("vehicletype") %></td></tr>
	<tr><th valign=top>Vehicle Colour</th><td><%= rs("vehiclecolour") %></td></tr>
	<tr><th valign=top>Drivers Name</th><td><%= rs("driversname") %></td></tr>
		<tr><th valign=top>Disabled</th><td><%
		if rs("disabled")=true then 
			response.write "Yes"
		else
			response.write "No"
		end if
		
		%></td></tr>
	<% sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='deletepermit'"
	openrs rsm2,sqlm2
	if not rsm2.eof and not rsm2.bof then %>
	   <tr><td colspan=2 align=center><a href=admin.asp?cmd=mdeletepermit&id=<%= rs("id") %> class=button onclick="return confirm('Are you sure you want to delete')">Delete Permit</a></td></tr>
	
	<%
	
	 End If 
	closers rsm2
	%>
	</table>
	
	<%
closers rs
end sub
sub menabledisabletemporary
if session("temporarypermit")=true then
	response.write "Temporary Permits are currently Enabled<br><br>"
	response.write "<a href=admin.asp?cmd=disabletemp class=button>Disable Temporary Permit</a><br>"
else
	response.write "Temporary Permits are currently Disabled<br><br>"
	response.write "<a href=admin.asp?cmd=enabletemp class=button>Enable Temporary Permit</a><br>"

end if

end sub
sub disabletemp
if session("permanentpermit")=false then 
	response.write "You cannot disable temporary permits as permanaent permits are currently disabled"
else
	sql="update useraccess set temporarypermit=0 where id=" & session("userid")
	objconn.execute sql
	session("temporarypermit")=false
	response.write "Temporary Permits are now disabled"
end if

end sub

sub enabletemp

	sql="update useraccess set temporarypermit=1 where id=" & session("userid")
	objconn.execute sql
	session("temporarypermit")=true
	response.write "Temporary Permits are now enabled"


end sub


sub menabledisablepermanent
if session("permanentpermit")=true then
	response.write "permanent Permits are currently Enabled<br><br>"
	response.write "<a href=admin.asp?cmd=disableperm class=button>Disable permanent Permit</a><br>"
else
	response.write "permanent Permits are currently Disabled<br><br>"
	response.write "<a href=admin.asp?cmd=enableperm class=button>Enable permanent Permit</a><br>"

end if

end sub
sub disableperm
if session("temporarypermit")=false then 
	response.write "You cannot disable permanent permits as temporary permits are currently disabled"
else
	sql="update useraccess set permanentpermit=0 where id=" & session("userid")
	objconn.execute sql
	session("permanentpermit")=false
	response.write "permanent Permits are now disabled"
end if

end sub
sub enableperm

	sql="update useraccess set permanentpermit=1 where id=" & session("userid")
	objconn.execute sql
	session("permanentpermit")=true
	response.write "permanent Permits are now enabled"


end sub



sub mremovesiteselection
if session("siteall")=true then
	response.write "All Site selection is now showing<br><br>"
	response.write "<a href=admin.asp?cmd=setsitealloff class=button>Remove all Site selection</a><br>"
else
	response.write "All Site selection is not showing<br><br>"
	response.write "<a href=admin.asp?cmd=setsiteallon class=button>Show all site selection</a><br>"

end if

end sub
sub setsitealloff

	sql="update useraccess set siteall=0 where id=" & session("userid")
	objconn.execute sql
	session("siteall")=false
	response.write "All Site selection has not been removed"


end sub
sub setsiteallon

	sql="update useraccess set siteall=1 where id=" & session("userid")
	objconn.execute sql
	session("siteall")=true
	response.write "All site selection is now added"


end sub

sub checkrights(menu)
 sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='" & menu & "'"
	openrs rsm2,sqlm2
	if rsm2.eof and  rsm2.bof then 
		response.write "no permission"
		response.end
	End If 
	closers rsm2


end sub
sub msearchpermit
if request("cmd2")<>"search" then
%>
	<table class=maintable2>

	<form method=post action=admin.asp?cmd=msearchpermit>
	<tr><td colspan=2>Enter in a plate to pull up record</td></tr>
	<tr><td>plate:</tD><tD> <input type=text name=plate></td></tr>
	<input type=hidden name=cmd2 value="search">
	<tr><td colspan=2 align=center><input type=submit  value="Search" class=button></td></tr>
	</form>
	</table>
<%
else
	splate=request("plate")
	if request("s")=1 then
		sql=session("sql")
		i=1
		rcount=2
	else
		sqlw="SELECT id,[registration],[valid_Date],[expiry_date],[type],[site],[userid],dateentered from apermits where registration like '%" & splate & "%'"
	'	sqlc="select count(ID) as countc from apermits where registration like '%" & splate & "%'"
		
		if request("plate")="" then 
			i=0
		else
			i=1
		end if
			'i=1	
			'	if i>0 then
				
		'	sqlsites="select * from usersites where userid=" & session("userid")
		'openrs rssites,sqlsites  ' check the sites he can see 
		'if rssites.eof and rssites.bof then
		'	 response.write "YOu do not have access to any site information"
		'	 response.End
		'end if
		
		'i=1
		'sql=sql & " and ("
		''do while not rssites.eof
		'	if i>1 then sql=sql & " OR "
		'	sql=sql & "(site='" &  rssites("sitecode") & "')" 
		'	i=i+1
		'	rssites.movenext
		'loop 
		'closers rssites
	
		'sql=sql & ")"		
				
		'sqlc=sqlc& sql
		rcount=0
		sql=sqlw  '& sql
		'response.write sql
		openrs rsc,sqlw
		do while not rsc.eof
				if rsc("userid")=session("userid") and rsc("site")="all" then
					'allowed to see
					rcount=rcount+1
				else
					if session("adminsite")=true then
						sqlsites="select * from [site information]"
					else
							sqlsites="select * from usersites where userid=" & session("userid") & " and sitecode='" & rsc("site") & "'"
					end if
					openrs rssites,sqlsites  ' check the sites he can see 
					if not rssites.eof and not rssites.bof then rcount=rcount+1
					closers rssites	
				end if
			
			rsc.movenext
		loop
		'rcount=rsc("countc")
		closers rsc 
	'	end if
	end if
	if i=0 then 
		response.write "You must fill in a search field"
	'	call msearchpermit
	else
		
		strpagename="admin.asp?cmd=msearchpermit&cmd2=search"
			
		if rcount=0 then 
			response.write "no records exist with this criteria"
		'elseif rcount=1 then call viewrecord
		else
			if rcount=1 then
			openrs rsa,sql
				call viewpermit(rsa("id"))
			closers rsa
		else
		
			createpermitSortableList objConn,sql,"plate",25,"class=maintable2",strpagename
		end if
		end if
		
	end if

end if 'closes if cmd2 search
end sub

Sub createpermitSortableList(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
			
    

	'response.write "strsort:" & strsort	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
   ' response.write "x"
    ' response.write "sql:" & strsql  & "<hr>"			
    		Set RS = server.CreateObject("adodb.recordset")
			strsql=strsql & " order by " & replace(strSort,"desc"," desc")
'    response.write "sql:" & strsql 			
    		With RS
    			.CursorLocation=3
    			.Open strSQL, objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"ID" and field.name<>"id" and field.name<>"userid" then
				Response.Write "<TD align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</TD>"	& vbcrlf
    			end if
	
	Next
					
    		Response.Write "<td>&nbsp;</td></TR>"
    		showrecord=0		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    
		if rs("userid")=session("userid") and rs("site")="all" then 
			showrecord=1
		else		
				if session("adminsite")=true then
					sqlsites="select * from [site information]"
				else
				sqlsites="select * from usersites where userid=" & session("userid") & " and sitecode='" & rs("site") & "'"
				end if
				openrs rssites,sqlsites  ' check the sites he can see 
				if not rssites.eof and not rssites.bof then
					showrecord=1 
				else
					showrecord=0
				end if
				closers rssites
		end if
				if showrecord=1   then
					if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
    			For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"ID" and field.name<>"id" and field.name<>"userid" then
					Response.Write "<TD align=center>" & vbcrlf
    				 Response.Write field.value
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
				if rs("ID")<>"" then
				response.write "<td><a  href=admin.asp?cmd=viewpermit&p=1&id=" & rs("ID") & " class=button 				onclick=""return popitup('admin.asp?cmd=viewpermit&p=1&id=" & rs("ID") & "')"">View</a></td>"

				end if
				Response.Write "</TR>" & vbcrlf
    end if
	
	
				RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "<TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing			
    	End Sub

 
 sub mviewexpiredpermit

	
	if request("s")=1 then
		sql=session("sql")
		i=1
		rcount=2
	else
		mymonth=month(date)
		if len(mymonth)<2 then mymonth="0" & mymonth
		myday=day(date)
		if len(myday)<2 then myday="0" & myday
		mydate=1
		sqlw="SELECT id,[registration],[valid_Date],[expiry_date],[type],[site],[userid] from apermits where expiry_date < cast('" & year(date()) & mymonth& myday &"' as datetime) "
		'sqlc="select count(ID) as countc from permits where expirydate<CONVERT(datetime, '" & mydate & "', 101)" '<cast('" & year(date()) & mymonth& myday &"' as datetime)"
		'response.write sqlc & "xxx"
		'sqlw2="where [expirydate]<CONVERT(datetime, '" & mydate & "', 101)"
		i=1
			'i=1	
			'	if i>0 then
				
		'	sqlsites="select * from usersites where userid=" & session("userid")
		'openrs rssites,sqlsites  ' check the sites he can see 
		'if rssites.eof and rssites.bof then
		'	 response.write "YOu do not have access to any site information"
		'	 response.End
		'end if
		
		'i=1
		'sql=sql & " and ("
		''do while not rssites.eof
		'	if i>1 then sql=sql & " OR "
		'	sql=sql & "(site='" &  rssites("sitecode") & "')" 
		'	i=i+1
		'	rssites.movenext
		'loop 
		'closers rssites
	
		'sql=sql & ")"		
				
		'sqlc=sqlc& sql
	'	sql=sqlw  '& sql
		'response.write sql
		rcount=0
		'sql=sqlw  '& sql
	'	response.write "<hr>" & sql & "<hr>"
	response.write sqlw
	sql=sqlw 
		openrs rsc,sqlw 
		do while not rsc.eof
		if rsc("userid")=session("userid") and rsc("site")="all" then
					'allowed to see
					rcount=rcount+1
				else
					if session("adminsite")=true then
					sqlsites="select * from [site information]"
					else
					sqlsites="select * from usersites where userid=" & session("userid") & " and sitecode='" & rsc("site") & "'"
					end if
					openrs rssites,sqlsites  ' check the sites he can see 
					if not rssites.eof and not rssites.bof then rcount=rcount+1
					closers rssites	
				end if
				
		
			rsc.movenext
		loop
		'rcount=rsc("countc")
		closers rsc 
	'	end if
	end if
	'response.write "here"
	if i=0 then 
		'response.write "You must fill in a search field"
		'call msearchpermit
	else
		
		strpagename="admin.asp?cmd=mviewexpiredpermit"
			
		if rcount=0 then 
			response.write "no expired permits"
		'elseif rcount=1 then call viewrecord
		else
			if rcount=1 then
			openrs rsa,sql
				call viewpermit(rsa("id"))
			closers rsa
		else
		'	response.write "<hr>going - " & sql & "<hr>"
		'	response.write sql & "<hr color=blue>"
			createpermitSortableList objConn,sql,"plate",25,"class=maintable2",strpagename
		end if
		end if
		
	end if


end sub


sub mviewcurrentpermit

	
	if request("s")=1 then
		sql=session("sql")
		i=1
		rcount=2
	else
		sqlw="SELECT id,[registration],[valid_Date],[expiry_date],[type],[site],[userid] from apermits where expiry_date>getdate() or expiry_date is null"
	'	sqlc="select count(ID) as countc from permits where expirydate>getdate()"
		
		i=1
			'i=1	
			'	if i>0 then
				
		'	sqlsites="select * from usersites where userid=" & session("userid")
		'openrs rssites,sqlsites  ' check the sites he can see 
		'if rssites.eof and rssites.bof then
		'	 response.write "YOu do not have access to any site information"
		'	 response.End
		'end if
		
		'i=1
		'sql=sql & " and ("
		''do while not rssites.eof
		'	if i>1 then sql=sql & " OR "
		'	sql=sql & "(site='" &  rssites("sitecode") & "')" 
		'	i=i+1
		'	rssites.movenext
		'loop 
		'closers rssites
	
		'sql=sql & ")"		
				
		'sqlc=sqlc& sql
		sql=sqlw  '& sql
		rcount=0
		sql=sqlw  '& sql
		'response.write sql
		openrs rsc,sqlw
		do while not rsc.eof
				if rsc("userid")=session("userid") and rsc("site")="all" then
					'allowed to see
					rcount=rcount+1
				else
					if session("adminsite")=true then
					sqlsites="select * from [site information]"
					else
					sqlsites="select * from usersites where userid=" & session("userid") & " and sitecode='" & rsc("site") & "'"
					end if
					openrs rssites,sqlsites  ' check the sites he can see 
					if not rssites.eof and not rssites.bof then rcount=rcount+1
					closers rssites	
				end if
						
			rsc.movenext
		loop
		'rcount=rsc("countc")
		closers rsc 
	'	end if
	end if
	if i=0 then 
		'response.write "You must fill in a search field"
		'call msearchpermit
	else
		
		strpagename="admin.asp?cmd=mviewcurrentpermit"
			
		if rcount=0 then 
			response.write "no current permits"
		'elseif rcount=1 then call viewrecord
		else
			if rcount=1 then
			openrs rsa,sql
				call viewpermit(rsa("id"))
			closers rsa
		else
			createpermitSortableList objConn,sql,"plate",25,"class=maintable2",strpagename
		end if
		end if
		
	end if


end sub
sub mdeletepermit
	sql="delete from apermits where id=" & request("id")
	objconn.execute sql
	response.write "Permit Deleted Successfully"


end sub

sub analyze
response.redirect "upload.asp"
end sub
 function charcount(strRequest, char)
   tmp = Split(Trim(strRequest), char)
   charcount = UBOUND(tmp)
 end function
sub uploadanalyze

Response.Buffer = True
	Response.ExpiresAbsolute = 0
	
	Dim sq
	Dim nCount
	Dim objProgress, objProgress2
	Dim sTemp
	dim pn
	pn=0
	Set objProgress = New atgProgressBar
		objProgress.Name = "progressbar1"
		objProgress.MaxValue = 100
		
		
		objProgress.Insert
		
		
sfile=request("sfile")
'sfile = Server.MapPath(".") & "\files\" & sFile
'create table here
	pn=pn+2
		'response.write "<hr>pn before random:" & pn
		objProgress.Value = pn
Randomize()
t="text"
n="number"
intupperbound=1000
intlowerbound=1
intRangeSize = intUpperBound - intLowerBound + 1
sngRandomValue = intRangeSize * Rnd()
sngRandomValue = sngRandomValue + intLowerBound
intRandomInteger = Int(sngRandomValue)
tablename="temp" & intrandominteger
sql="CREATE TABLE " &  tablename & " (id autoincrement,plate varchar, mydate varchar,mytime varchar, lanenumber varchar, mystatus varchar, sitename varchar, sitecode varchar)"
objconntemp.execute sql
sfile= "C:\home\vhosts\cleartoneholdings.com\httpdocs\db\" & sFile

Set fs=Server.CreateObject("Scripting.FileSystemObject")
	Set f=fs.OpenTextFile(sfile, 1)
	
	Do While f.AtEndOfStream=False
f.Readline
numoflines=numoflines+1
Loop
f.Close
	set f=Nothing
'response.write "<hr>numoflines:" & numoflines & "<hr>"
		pn=pn+5
		'response.write "<hr>pn afte opening file:" & pn
		objProgress.Value = pn
		Set fs=Server.CreateObject("Scripting.FileSystemObject")
	Set f=fs.OpenTextFile(sfile, 1)
		
		
	i=1
	do while f.AtEndOfStream = false
		str=f.readline
		if str<>""  then
		i=i+1
		if charcount(str,",")<3 then
			response.write "<br>invalid file uploaded- error line:"  & i
			exit sub
		end if
		arrstr=split(str,",",-1,1)
		if arrstr(1)="" or arrstr(2)="" or arrstr(3)="" then 
			response.write "invalid file uploaded"
			exit sub
		end if
		sitename=dlookuptemp("sitelanes","sitename","lanecode=" & arrstr(3))
		sitecode=dlookuptemp("sitelanes","sitecode","lanecode=" & arrstr(3))

		if sitename="" then 
			response.write "invalid sitename uploaded"
			exit sub	
		end if
		sql="insert into " & tablename & "(plate,mydate,mytime,lanenumber,sitename,sitecode) values('" & arrstr(0) & "','" & arrstr(1) & "','" & arrstr(2) & "','" & arrstr(3) & "','" & sitename & "','" & sitecode & "')"
			'response.write sql & "<br>"
		objconntemp.execute sql
		'on error goto 0
		end if
		k=45/numoflines
		pn=pn+k
		objProgress.Value = pn	
	loop

	'response.write "<hr>pn afte reading file into db:" & pn

	'response.write "num of lines:" & numoflines & "pn:" & pn & "k:" & k
	f.Close
	set f=Nothing
	Set fs=Nothing
sql="select * from " & tablename
openrstemp rs,sql
nmatched=0
nunmatched=0
do while not rs.eof
	sqlc="select count(plate) as countplate from " & tablename & " where mydate='" & rs("mydate") & "' and sitecode='" & rs("sitecode") & "' and plate='" & rs("plate") & "'"
	'response.write sqlc
	openrstemp rsc,sqlc
	countplate=rsc("countplate")
	
	If countplate Mod 2 Then
		mystatus="notmatched"
		nunmatched=nunmatched+1
	else
		mystatus="matched"
		nmatched=nmatched+1
	end if
'	response.write "<hr>" & countplate & "-" & mystatus
	sqlu="update " & tablename & " set mystatus='" & mystatus & "' where id=" & rs("id")
	objconntemp.execute sqlu
	pn=pn+(5/numoflines)
	
	objProgress.Value = pn
	rs.movenext
loop
closers rs
'move to unmatched table 
sql="CREATE TABLE unmatched" &  tablename & " (id autoincrement,plate varchar, mydate varchar,mytime varchar, lanenumber varchar, mystatus varchar, sitename varchar,sitecode varchar)"
objconntemp.execute sql
sql="select * from " & tablename & " where mystatus='notmatched'"
openrstemp rs,sql
do while not rs.eof
	sql="insert into unmatched" & tablename & "(plate,mydate,mytime,lanenumber,mystatus,sitename,sitecode) values(" & tosql(rs("plate"),t) & "," & tosql(rs("mydate"),t) & "," & tosql(rs("mytime"),t) & "," & tosql(rs("lanenumber"),t)  & "," & tosql(rs("mystatus"),t) & "," & tosql(rs("sitename"),t) & "," & tosql(rs("sitecode"),t) & ")"
	'response.write sql
	objconntemp.execute sql
	pn=pn+(10/nunmatched)
	objProgress.Value = pn
rs.movenext
loop
'move matched to matched table
sql="CREATE TABLE matched" &  tablename & " (id autoincrement,plate varchar, mydate varchar,starttime varchar, endtime varchar,duration varchar, mystatus varchar, sitename varchar,sitecode varchar)"
objconntemp.execute sql

sql="CREATE TABLE fuzzy" &  tablename & " (id autoincrement,plate varchar, permitplate varchar,permitid number, mystatus varchar)"
objconntemp.execute sql

sql="select * from " & tablename & " where mystatus='matched' order by plate,sitename,mydate,mytime"
'response.write sql
'response.end
openrstemp rs,sql
do while not rs.eof
		plate=rs("plate")
		mydate=rs("mydate")
		starttime=rs("mytime")
		sitename=rs("sitename")
		sitecode=rs("sitecode")
		mystatus="matched"
		Rs.MoveNext
		endtime=Rs("mytime")
	'	response.write cdate(mydate  & " " &endtime)
	 duration = DateDiff("n", cdate(mydate  & " " &starttime), cdate(mydate &" " & endtime))
		sql="insert into matched" & tablename & "(plate,mydate,starttime,endtime,duration,sitename,sitecode,mystatus) values(" & tosql(plate,t) & "," & tosql(mydate,t) & "," & tosql(starttime,t)  & "," & tosql(endtime,t) & "," & tosql(duration,t) & "," & tosql(sitename,t) & "," & tosql(sitecode,t)& "," & tosql(mystatus,t) & ")"
		'response.write sql
	objconntemp.execute sql
	pn=pn+(23/nmatched)
	objProgress.Value = pn
 rs.movenext
loop

''check permit db for all those matched
sql="select * from matched" & tablename & " where mystatus='matched'"
openrstemp rs,sql
do while not rs.eof
	''check for nonviolators here
	sqlcks="select * from sitestay where sitename=" & tosql(rs("sitename"),t) 
	'response.write "<hr>" & sqlcks 
	openrstemp rscks,sqlcks
		smax=cint(rscks("maximumfreestay"))
		'response.write "<br>max:" & smax
		'response.write "<br>duration:" & rs("duration")
		if cint(rs("duration"))<smax then
			mystatus="matchednonviolator"
		else
			mystatus="matchedpossbileviolator"	
		end if
	closers rscks
	'response.write mystatus
	if mystatus<>"matchednonviolator" then
	
		sqlck="select * from apermits where registration='" & rs("plate") & "' and (site='" & rs("sitecode") & "' or site='all')" 
		'response.write sqlck
		openrs rsck,sqlck
			if not rsck.eof and not rsck.bof then 
				mystatus="matched100%permit"
			'	response.write "<font color=red>matched 100%</font>"
			else
				'checkfuzzy
				mystatus=checkfuzzy(rs("plate"),tablename,rs("sitecode"))
			end if
	end if
		sql="update matched" & tablename & " set mystatus='" & mystatus & "' where id=" & rs("id")
		objconntemp.execute sql
	closers rsck
	pn=pn+(45/nmatched)
	objProgress.Value = pn
rs.movenext
loop
closers rs
pn=pn+5
	objProgress.Value = pn
sqlunmatched="select plate,mydate,mytime,lanenumber,mystatus,sitename from unmatched" & tablename 
sqlmatchednon="select plate,mydate,starttime,endtime,duration,sitename,mystatus from matched" & tablename  & " where mystatus='matchednonviolator'"
sqlmatchedpos="select plate,mydate,starttime,endtime,duration,sitename,mystatus from matched" & tablename  & " where mystatus<>'matchednonviolator'"
sqlfuzzy="select plate,permitplate,permitid,mystatus from fuzzy" &tablename
response.write "Unmatched:" 
call  outputexceltemp(sqlunmatched)
response.write "<br><br>"
response.write "Matched Non violator:" 
call  outputexceltemp(sqlmatchednon)
response.write "<br><br>"
response.write "Matched Possible violator:" 
call  outputexceltemp(sqlmatchedpos)
response.write "<br><br>"
response.write "Fuzzy Matches:" 
call  outputexceltemp(sqlfuzzy)
'  response.write "<br>pn:" & pn
Set objProgress = Nothing
response.write "<br><br><a href=admin.asp?cmd=deltables&name=" & tablename & " class=button>Delete Temporary Tables</a>"

end sub
sub deltables
tablename=request("name")
sql="drop table " & tablename 
objconntemp.execute sql
sql="drop table matched" & tablename 
objconntemp.execute sql
sql="drop table unmatched" & tablename 
objconntemp.execute sql
sql="drop table fuzzy" & tablename 
objconntemp.execute sql
response.write "Temporary Tables deleted"
end sub

function checkfuzzy(plate,tablename,sitecode)
	'response.write "checkign fuzzy"
	sqlp="select * from apermits where site='" & sitecode & "' or site='all'"
	'response.write sqlp
'response.write "<font class=red>Checking fuzzy</font>"
	openrs rsp,sqlp
	fuzzyp=0
	do while not rsp.eof
		permitplate=rsp("registration")
		lenplate=len(plate)
		lenpermitplate=len(permitplate)
		if lenplate<lenpermitplate then
			j=lenplate
		else
			j=lenpermitplate
		end if	
		
		result=0
	For i = 1 To j 
		if mid(plate,i,1)=mid(permitplate,i,1) then result=result+1
	
	next
		resultb=0
		For i = j To 1 STEP -1
			if mid(plate,i,1)=mid(permitplate,i,1) then resultb=resultb+1
			next
	
	if resultb>result then result=resultb
	ifuzzyp=result*100
	ifuzzyp=ifuzzyp/j
	if fuzzyp<ifuzzyp then 
		fuzzyp=ifuzzyp
		fpermitplate=permitplate
		fpermitid=rsp("id")
	end if
		rsp.movenext
	loop
	closers rsp
'response.write "<font class=red>fuzzy" & fuzzyp & "</font>"
	if fuzzyp>60 then
		mystatus="matchedfuzzy" & fuzzyp & "-%"
		sql="insert into fuzzy" & tablename & " (plate,permitplate,permitid,mystatus) values('" & plate & "','" &  fpermitplate & "'," & fpermitid & ",'" & mystatus & "')"
		'	response.write sql
			objconntemp.execute sql
	else	
		mystatus="matchedviolator"
	end if
checkfuzzy=mystatus	
end function

Class atgProgressBar
	Private m_Granularity
	Private m_Value
	Private m_FilledImage
	Private m_EmptyImage
	Private m_ImageName
	Private m_Width
	Private m_Height
	Private m_MaxValue
	Private m_MinValue
	Private bInserted

	Private Sub Class_Initialize()
		' set the default initial values of
		' all of our private member variables
		m_Granularity = 10
		m_MaxValue = 100
		m_MinValue = 0
		m_Value = 0
		m_FilledImage = "images/redblock.gif"
		m_EmptyImage = "images/whiteblock.gif"
		m_Width = 300
		m_Height = 16
		m_ImageName = "progress"
		bInserted = False
	End Sub

	Public Property Let Name(sVal)
		m_Imagename = sVal
	End Property

	Public Property Let Granularity(nVal)
		' this is an integer in the range from 1 to 50,
		' that is evenly divisible into 100...
		' it represents the number of percentage points
		' (out of 100) that a single "tick" represents
		If bInserted = False Then
			If IsNumeric(nVal) Then
				If cLng(nVal) >= 1 And cLng(nVal) <= 50 Then
					m_Granularity = cLng(nVal)
				Else
					m_Granularity = 10
				End If
			Else
				m_Granularity = 10
			End If
		End If
	End Property

	Public Property Let Width(nVal)
		If bInserted = False Then
			If IsNumeric(nVal) Then
				If cLng(nVal) > 0 Then
					m_Width = cLng(nVal)
				Else
					m_Width = 300
				End If
			Else
				m_Width = 300
			End If
		End If
	End Property

	Public Property Let Height(nVal)
		If bInserted = False Then
			If IsNumeric(nVal) Then
				If cLng(nVal) > 0 Then
					m_Height = cLng(nVal)
				Else
					m_Height = 300
				End If
			Else
				m_Height = 300
			End If
		End If
	End Property

	Public Property Let FilledImage(sVal)
		m_FilledImage = sVal
	End Property

	Public Property Let EmptyImage(sVal)
		m_EmptyImage = sVal
	End Property

	Public Sub Insert()
		Dim i
		Dim nImages, nImageWidth

		If bInserted = False Then
			Response.Write "<p>"
			Response.Write "<img src='" & m_FilledImage & "' height='2' width='" & m_Width & "' />"
			Response.Write "<br />" & vbCrlf
			nImages = 100 \ m_Granularity
			nImageWidth = m_Width \ nImages
			Response.Write "<img src='" & m_FilledImage & "' height='" & m_Height & "' width='1' />"
			Response.Write "<nobr>"
			For i = 1 To nImages - 1
				Response.Write "<img name='" & m_ImageName & i &"' src='" & m_EmptyImage & "' height='" & m_Height & "' width='" & nImageWidth & "' />"
			Next
			Response.Write "<img name='" & m_ImageName & nImages &"' src='" & m_EmptyImage & "' height='" & m_Height & "' width='" & (m_Width - (nImageWidth * (nImages - 1)) - 2) & "' />"
			Response.Write "<img src='" & m_FilledImage & "' height='" & m_Height & "' width='1' />"
			Response.Write "</nobr><br />" & vbCrlf
			Response.Write "<img src='" & m_FilledImage & "' height='2' width='" & m_Width & "' align='top' />"
			Response.Write "</p>"
			bInserted = True
		End If
	End Sub

	Public Property Let MaxValue(nVal)
		If bInserted = False Then
			If IsNumeric(nVal) Then
				m_MaxValue = cLng(nVal)
			Else
				m_MaxValue = 100
			End If
		End If
	End Property

	Public Property Let MinValue(nVal)
		If bInserted = False Then
			If IsNumeric(nVal) Then
				m_MinValue = cLng(nVal)
			Else
				m_MinValue = 0
			End If
		End If
	End Property

	Public Property Let Value(nVal)
		Dim i
		Dim nImages, nImagesAlreadySet, nImagesToSet
		Dim nNewValue
		' only allow value to be set if progressbar was inserted
		If bInserted = True Then
			' terminate if the browser disconnected on us
			If Response.IsClientConnected Then
				' validate input
				If Not IsNumeric(nVal) Then
					nNewValue = m_MinValue
				ElseIf cLng(nVal) <= m_MinValue Then
					nNewValue = m_MinValue
				ElseIf cLng(nVal) >= m_MaxValue Then
					nNewValue = m_MaxValue
				Else
					nNewValue = cLng(nVal)
				End If
				' calculate the total number of images
				nImages = 100 \ m_Granularity
				' figure the number that were already filled
				If m_Value = m_MinValue Then
					nImagesAlreadySet = 0
				Else
					nImagesAlreadySet = nImages * m_Value \ m_MaxValue
				End If
				' figure the number that should now be filled
				If nNewValue = 0 Then
					nImagesToSet = 0
				Else
					nImagesToSet = nImages * nNewValue \ m_MaxValue
				End If
				' only change images that are now different
				If nImagesToSet <> nImagesAlreadySet Then
					' start the client-side script block
					Response.Write vbCrlf & "<scr" & "ipt language='javascript'>" & vbCrlf
					For i = 1 To nImages
						If i > nImagesAlreadySet And i <= nImagesToSet Then
							' image was empty... fill it in
							Response.Write "document." & m_imageName & i & ".src='"
							Response.Write m_FilledImage
							Response.Write "';" & vbCrlf
						ElseIf i <= nImagesAlreadySet And i > nImagesToSet Then
							' image was filled... write empty image
							Response.Write "document." & m_imageName & i & ".src='"
							Response.Write m_EmptyImage
							Response.Write "';" & vbCrlf
						End If
					Next
					' close the client-side script block
					Response.Write "</scr" & "ipt>" & vbCrlf
					' send the output to the client
					Response.Flush
				End If
				m_Value = nNewValue
			Else
				' if "IsClientConnected" returns false...
				Response.End
			End If
		End If
	End Property

End Class



sub viewplateimages

if request("s")=1 then
		sql=session("sql")
		
else
	sql="select id,convert(varchar,[date],103) as rdate,convert(varchar,[date],108) as rtime,lane,accuracy,plate,picture,permit,violation14 from plateimages where imagesexist=1"

	if request("lane")<>"" then sql=sql & " and lane='" & request("lane") & "'"
	if request("accuracy")<>"" then sql=sql & " and accuracy<" & request("accuracy") 

	'response.
	openrs rs,sql
	if rs.eof and rs.bof then 
		 response.write "no records"
		 exit sub
	end if
	closers rs
end if
	strpagename="admin.asp?cmd=viewplateimages"
	createplateSortableList objConn,sql,"id",100,"class=maintable2",strpagename
response.write "<br><Br>Search Plate images<br>"
%>
<form method=post action=admin.asp?cmd=viewplateimages>
<table><tr><td>Lane</td><td><input type=text name=lane></td></tr>
<tr><td>Accuracy</td><td>less than<input type=text name=accuracy></td></tr>
<tr><td colspan=2 align=center><input type=submit name=submit value=Search></td></tR></table>

</form>
<%
end sub

Sub createplateSortableList(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
'response.write strsql
session("sql")=strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
			
    

	'response.write "strsort:" & strsort	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
    			
    		Set RS = server.CreateObject("adodb.recordset")
    			
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		response.write "<form method=post action=admin.asp?cmd=saveplateimage>"
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"ID" and field.name<>"id" and field.name<>"userid" then
				Response.Write "<TD align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</TD>"	& vbcrlf
    			end if
	
	Next
					
    		Response.Write "<td>&nbsp;</td></TR>"
    		showrecord=0		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    
		
			
				Response.Write "<TR>" & vbcrlf
    			For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"ID" and field.name<>"id" and field.name<>"userid" then
					Response.Write "<TD align=center>" & vbcrlf
					if field.name="picture" then
						filename=field.value
						filename=replace(filename,"A.jpg","P.jpg")
						filename= year(rs("rdate")) & "/" & month(rs("rdate")) & "/" & day(rs("rdate")) & "/" & hour(rs("rtime")) & "/" & filename
					
						itemimageurl= configweburl & "plateimages/server1/" & filename
						itemimage=configwebdir & "traffica\plateimages\server1\" & filename
							response.write "<a href=""javascript:popUp('admin.asp?cmd=showplateimages&p=1&rdate=" & rs("rdate") & "&rtime=" & rs("rtime") & "&filename=" & field.value &  "')"">"
									response.write "<img src=""" & itemimageurl & """ " &ImageResize(itemimage,100, 34) & " border=""0"" align=middle valign=middle></a>"
					elseif field.name="plate" then 
						response.write "<input type=text name=pi" & rs("id") & " value='" & field.value & "'>"
					elseif field.name="permit" then 
						if field.value=true then response.write "p"
					elseif field.name="violation14" then 
						if field.value=true then response.write "v"
					else	
						 Response.Write field.value
					end if
					Response.Write "</TD>" & vbcrlf
				end if
    			Next
			response.write "<Td><a href=""javascript:popUp('admin.asp?cmd=showfuzzy&p=1&mydate=" & rs("rdate") & "&myplate=" & rs("plate") & "')"">View Fuzzy Matches</a>"
			
				Response.Write "</TR>" & vbcrlf

	
	
				RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "</TABLE>" & vbcrlf
    redirect =  strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage 	+1	
	response.write "<input type=hidden name=redirect value='" & redirect & "'>"
	response.write "<center><input type=submit name=submit class=button value=Save></center></form>"
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    			'strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    response.write "<bR>"
	for i=1 to intTotalPages
	
	response.write "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & i & " >"& i &"</A>&nbsp;" 
	next		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing			
    	End Sub
		
		sub saveplateimage
		%>
		
		<% For x = 1 to Request.Form.Count
    if Request.Form.Key(x)<>"redirect" and Request.Form.Key(x)<>"submit" then
		xid=right(request.form.key(x),len(request.form.key(x))-2)
		sqlu="update plateimages set plate='" & Request.Form.Item(x) & "' where id=" & xid
		sqlu="exec spupdateplateimage @id=" & xid & ",@plate='" & Request.Form.Item(x) & "',@userid=" & session("userid")
			
		'response.write sqlu & "<br>"
		objconn.execute sqlu
	end if
	'Response.Write Request.Form.Key(x) & " = "
    'Response.Write Request.Form.Item(x) & "<br>"
Next 
		
	response.redirect request("redirect")	
		end sub
		
		
sub cancelform
if request("cmd2")="" then
%>

<table class=maintable2>
<form method=post action=admin.asp?cmd=cancelform&cmd2=cform>
<tr><td colspan=2>Enter in a pcn or plate </td></tr>
<tr><td>pcn:</td><td> <input type=text name=pcn></td></tr>
<tr><td>plate:</tD><tD> <input type=text name=plate></td></tr>
<tr><td colspan=2 align=center><input type=submit  value="Submit" class=button></td></tr>


</form>
</table>

<%
end if
if request("cmd2")="cform" then
	spcn=request("pcn")
	splate=request("plate")
	sqlw="SELECT VIOLATORS.ID, VIOLATORS.Registration, VIOLATORS.[Date of Violation], VIOLATORS.Site, VIOLATORS.[Arrival Time], VIOLATORS.[Departure Time], VIOLATORS.[Duration of Stay], VIOLATORS.PCN+violators.site as pcn  FROM VIOLATORs where  "
	sqlc="select count(ID) as countc from violators where "
	i=0
		if spcn<>"" then
			sql= sql & "pcn like '%" & left(spcn,7) & "%'"
			i=i+1
		end if 
		if splate<>"" then
			if i>0 then sql=sql & " OR "
			sql=sql & "registration like '%" & splate & "%'"
			i=i+1
		end if 
		if i>0 then
		if session("adminsite")=true then	
			sqlsites="select * from [site information]"
		else		
		sqlsites="select * from usersites where userid=" & session("userid")
		end if
		openrs rssites,sqlsites  ' check the sites he can see 
		if rssites.eof and rssites.bof then
			 response.write "YOu do not have access to any site information"
			 response.End
		end if

		k=1
		sql=sql & " and ("
		do while not rssites.eof
			if k>1 then sql=sql & " OR "
			sql=sql & "(site='" &  rssites("sitecode") & "')" 
			k=k+1
			rssites.movenext
		loop 
		closers rssites
	
		sql=sql & ")"		
				
		sqlc=sqlc& sql
		sql=sqlw & sql
	'	response.write sql
		openrs rsc,sqlc
		rcount=rsc("countc")
		closers rsc 
		
	
	if i=0 then 
		response.write "You must fill in a search field"
		response.end
		'call cancelform
	end if
	if rcount=0 then 
		response.write "You must fill in a valid plate or pcn"
		response.end
		'call cancelform
	end if
	if rcount=1 then 
		openrs rs,sql
		showcancelform(rs("id"))
		closers rs
	else
		openrs rs,sql
		response.write "Choose a record to add cancellation<br><table class=maintable2>"
				
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"ID" and field.name<>"id" then
				Response.Write "<Th align=center>" & vbcrlf
    fieldname=field.name
	 
				
    				Response.Write field.name 	& vbcrlf		
    	
    			Response.Write "</Th>"	& vbcrlf
    			end if
	
	Next
					
    		Response.Write "<td>&nbsp;</td></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
    			For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"ID" and field.name<>"id" then
					Response.Write "<TD align=center>" & vbcrlf
    				 Response.Write field.value
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
				if rs("ID")<>"" then
				response.write "<td><a  href=admin.asp?cmd=showcancelform&id=" & rs("ID") & " class=button>Cancel</a></td>"
				end if
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "<TABLE>" & vbcrlf
    
		closers rs	
	end if		
%>



<%
end if
end if
end sub		
sub showcancelform(id)
showmyform=1
sql="select * from violators where id=" & id
openrs rs,sql
sqlc="select * from cancelled where pcn='" & rs("pcn") & rs("site") &  "'"
openrs rsc,sqlc
if not rsc.eof and not rsc.bof then
	response.write "This pcn was already cancelled"
	response.write "<br><br> <br><a href=admin.asp?cmd=printcancellation&id=" & rsc("id") & " class=button target=_new>Print Cancellation Letter</a>"
    cancelid=rsc("id")
	showmyform=0
	response.write "<Br><br><a href=admin.asp?cmd=cancelledletters&cmd2=addletter&id=" & id & "&letter=" & request("letter") & "&cancelid=" & cancelid  & " class=button target=_new>Print New Cancellation Letter</a>"

end if
closers rsc
if showmyform=1 then
    letter=request("letter")
    cletter=request("cletter")
%>
<center>
<form method=post name=cancelform action=admin.asp?cmd=savecancelform ONSUBMIT="return Validatecancel();">
<h2>Cancellation Form</h2>
<table class=maintable2>

<input type=hidden name=plate value="<%= rs("registration") %>">
<input type=hidden name=pcn value="<%= rs("pcn") %><%= rs("site") %>">

<%
'sqlcc="select cancellationcharge from [site information] where si

cancellationcharge=dlookup("[site information]","cancellationcharge","sitecode='" & rs("site") & "'")
'response.write "cC:" & cancellationcharge

%> <tr><th valign=top>Requested By</th><td><input type=radio name=requestedby value="Driver" onClick="checkrequest('driver');">Driver<br>
<input type=radio name=requestedby value="Client" onClick="checkrequest('client');">Client<br>

</td>

</tr>

<tr id=reasondriver style="display:none;"><th valign=top>Reason</th><td>
<input type=radio name=reason value="Double Entry">Double Entry<br>
<input type=radio name=reason value=Disabled>Disabled<br>
<input type=radio name=reason value=Breakdown>Breakdown<br />  
<input type=radio name=reason value=Health>Health<br />  
<input type=radio name=reason value="Triangle Error">Triangle Error<br />  
<input type=radio name=reason value="Not added to Permit">Not added to Permit<br />  
<input type=radio name=reason value=Other>Other<br />  
</td>
<td>Reason<br><textarea name=reasonother></textarea></td>
</tr>
<tr id="reasonclient" style="display:none;"><th valign=top>Reason</th><td>
<input type=radio name=reason value="Customer">Customer<br>
<input type=radio name=reason value="Double Entry">Double Entry<br>
<input type=radio name=reason value="Permit not registered by client">Permit not registered by client<br />  
<input type=radio name=reason value="Incorrect registration entered">Incorrect registration entered<br />  
<input type=radio name=reason value="Disabled">Disabled<br />  
<input type=radio name=reason value="Breakdown">Breakdown<br />  
<input type=radio name=reason value="Incorrect Triangle Transcription">Incorrect Triangle Transcription<br />  
<input type=radio name=reason value="Permit Form not received">Permit Form not received<br />  
<input type=radio name=reason value=Other>Other<br /> 

</td>
<td>Reason<br><textarea name=reasonother></textarea></td>
</tr>
<tr><td colspan=3><img src=spacer.gif height=10 width=1></td></tr>
<!--<tr><td>Additional Information<br><textarea name=rbother></textarea></td></tr>-->
<tr><td colspan=3><img src=spacer.gif height=10 width=1></td></tr>
<tr id="authby" style="display:none;"><th colspan=2>Authorised By</th><td><input type=text name=authby></td></tr>
<tr id="nocharge" style="display:none;"><th colspan=2>
<% if cancellationcharge=false then %>
<input type=checkbox value=1 name=nochargex  disabled checked />No Charge for cancellation</td></tr>
<input type=hidden name=nocharge value=1>
<% else  %>
<input type=checkbox value=1 name=nocharge   />No Charge for cancellation</td></tr>
<% end if  %>

<tr id=addinfo" style="display:none;"><th colspan=3>Additional Info<br>
<textarea name=addinfo cols=50 rows=4></textarea></td></tr>
<tr id="submit" style="display:none;"><td colspan=3 align=center><input type=submit name=submit vallue=submit class=button></td></tr>
</table>
<input type=hidden name=letter value="<%=letter %>" />
<input type=hidden name=cletter value="<%=cletter %>" />
<input type=hidden name=violatorid value="<%=id %>" />
</form>
</center>
 <%
 end if
 closers rs
 call viewrecord(id)
 end sub
 
 sub savecancelform
 
 spcn=request("pcn")
 splate=request("plate")
 cancelled=now()
 'site=right(spcn,3)
 'offencedate=request("offencedate")
 reason=request("reason")
 reasonother=request("reasonother")
 requestedby=request("requestedby")
 rbother=request("rbother")
 authby=request("authby")
 addinfo=request("addinfo")
 nocharge=request("nocharge")
 if nocharge="" then nocharge=0
 'response.Write "nocharge" & nocharge
 'response.End 
 	userid=session("userid")
		ip=Request.ServerVariables("remote_addr")
t="text"
  sql="SET NOCOUNT ON insert into cancelled (pcn,plate,reason,reasonother,requestedby,rbother,authby,addinfo,userid,ip) values(" & tosql(spcn,t) & "," & tosql(splate,t) & ","   & tosql(reason,t) & "," & tosql(reasonother,t) & "," & tosql(requestedby,t) & "," & tosql(rbother,t) & "," & tosql(authby,t) & "," & tosql(addinfo,t) & "," & tosql(userid,t) & "," & tosql(ip,t) &");"&_
        "SELECT @@IDENTITY AS NewID;"
 set rsi=objconn.execute (sql )
 cancelid=rsi(0)
 if request("letter")=""  then
 		
 
 response.write "Cancellation was successfull. <br><br> <br><a href=admin.asp?cmd=printcancellation&id=" & cancelid & " class=button target=_new>Print Cancellation Letter</a>"
 else
    response.Redirect "admin.asp?cmd=cancelledletters&cmd2=addletter&id=" & request("violatorid") & "&letter=" & request("letter") & "&cancelid=" & cancelid & "&nocharge=" & nocharge
 end if
 end sub
 sub printcancelpcn
 sql="select * from cancelled where pcn='" & request("pcn") & "'"
 openrs rs,sql
 id=rs("id")
 closers rs
 call printcancel(id)
 response.write "<input type='button' value='Print' onClick='window.print()' class=noPrint>"
 
 end sub
 sub printcancellation
 call printcancel(request("id"))
 response.write "<input type='button' value='Print' onClick='window.print()' class=noPrint>"
 end sub
 sub printcancel(id)
 	sql="select * from cancelled where id=" & id
	openrs rs,sql
	%>
<table width=600>
<tr><td  width=75% align=center><h1>Cancellation Form</h1></td><td class=filenumber>File Number<br><%= rs("id") %></td></tr>
</table>

<table class=cancelform width=600 cellpadding=0 cellspacing=0>
 
<tr><th>Site</th><td colspan=2><%= right(rs("pcn"),3)%></td></tr></Tr>
<tr><th>Plate</th><td colspan=2><%= rs("plate") %></td></tr></Tr>
<tr><th>PCN</th><td colspan=2><%= rs("pcn") %></td></tr></Tr>
<tr><th>Reason</th><td width=30>

<% if rs("reason")="Disabled" then 
	response.write "<div class=highlightb>"
	else
	response.write "<div class=b>"
	end if %>
Disabled</div><br>
<% if rs("reason")="Permit" then 
	response.write "<div class=highlightb>"
	else
	response.write "<div class=b>"
	end if %>

Permit</div><br>
<% if rs("reason")="Other" then 
	response.write "<div class=highlightb>"
	else
	response.write "<div class=b>"
	end if %>
	Other</div>

</td>

<td>Reason:<br><%= rs("reasonother") %></td>

</td></tr>
<tr><th>Requested By</th><td width=30>
<% if rs("requestedby")="Driver" then 
	response.write "<div class=highlightb>"
	else
	response.write "<div class=b>"
	end if %>
Driver</div><br>
<%if rs("requestedby")="Site" then 
	response.write "<div class=highlightb>"
	else
	response.write "<div class=b>" 
	end if%>
       Site</div><br>
   <%if rs("requestedby")="Other" then 
	response.write "<div class=highlightb>"
	else
	response.write "<div class=b>" 
    end if%>
	    Other</div>


<td>Additional Information:<br><%= rs("rbother") %></td>

</td></tr>
<tr bgcolor=#ff0000><td colspan=3><img src=spacer.gif height=5 width=1></td></tr>
<tr><th>Authorised By</th><td colspan=2><%= rs("authby") %></td></tr>
<tr><th>Date</th><td colspan=2><%= rs("cancelled") %></td></tr>


<tr><td colspan=3>Additional Info<br>
<%= rs("addinfo") %></td></tr>
</table>

</form>
 
 <%
 
 closers rs
 end sub
 sub printbatchcancel
 if request.form("cmd2")="" then
 %>
 <table class=maintable2><tr><td colspan=2>
 <form method=post action=admin.asp?cmd=printbatchcancel>
 <h1>Choose Criteria to print Cancellation Forms</h1></td></tr>
<tr><td>Sites:</td><td> <select name=sites><option value="">All Sites</option>
<% 
if session("adminsite")=true then
sqlsites="select * from [site information]"
else
sqlsites="select * from usersites where userid=" & session("userid")
end if
openrs rssites,sqlsites  ' check the sites he can see 
do while not rssites.eof
	response.write "<option value='" & rssites("sitecode") & "'>" &  rssites("sitecode") & "</option>"
	rssites.movenext
loop

closers rssites %>

</select>
</td></tr>
<tr><td>
Date From </td><td><input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
Date To </td><td><input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)

</td></tr>
<tr><td>
File Number From</tD><td> <input type=text name=fnfrom onKeyPress="return checkValue(event)">
</td></tr>
<tr><td>
File Number To</td><td><input type=text name=fnto onKeyPress="return checkValue(event)"></td></tr>
<tr><td>
Plate </td><td><input type=text name=plate></td></tr>
<tr><td colspan=2 align=center>
<input type=hidden name=cmd2 value=proccess>
<input type=submit name=submit value="Submit" class=button>
</form>
</td></tr></table>
 <%
 else
 	printbatch
 end if
 end sub
 sub printbatch
 		i=0
 	sql="select * from cancelled where "
	if request.form("sites")<>"" then
		sql=sql & "site=" & tosql(request.form("sites"),"text")
		i=i+1
	end if
 	if request.form("datefrom")<>"" and request.form("dateto")<>"" then
		if i>0 then sql=sql & " and "
		sql=sql & "(cancelled >="
		datefrom=request.form("datefrom")
		arrdatefrom=split(datefrom,"/")
	myday=arrdatefrom(0)
	'response.write "myday" & myday
	mymonth=arrdatefrom(1)
	myear=arrdatefrom(2)
	sql=sql & "cast('" & myear & mymonth & myday & "' as datetime)"
	'sql=sql & "dateserial(" & myear & "," & mymonth & "," & myday & ")"
		'sql=sql & tosql(datefrom,"date")
		sql=sql & " and cancelled<="
		dateto=request("dateto")
		arrdateto=split(dateto,"/")
			myday=arrdateto(0)
	mymonth=arrdateto(1)
	myear=arrdateto(2)
	'	sql=sql & tosql(request.form("dateto"),"date")  & ")"
'sql=sql & "dateadd(year, " & myear & " - 1900, dateadd(month, " & mymonth & ", dateadd(day, " & myday & ", -32))))"
sql=sql & "cast('" & myear & mymonth & myday & " 23:59' as datetime))"
		i=i+1 
	end if
	if request.form("fnfrom")<>"" and request.form("fnto")<>"" then
		if i>0 then sql=sql & " and "
		sql=sql & "(id >="
		fnfrom=request.form("fnfrom")
		sql=sql & fnfrom
		sql=sql & " and id<="
		sql=sql & request("fnto")  & ")"
		i=i+1 
	end if
	if request.form("plate")<>"" then
		if i>0 then sql=sql & " and "
		sql=sql & "plate=" & tosql(request.form("plate"),"text")
		i=i+1
	
	end if
	if i=0 then 
		response.write "You must fill in an option"
		response.end
	end if
'response.write sql
	openrs rs,sql
		if rs.eof and rs.bof then 
			response.write "No records matched"
		else
			do while not rs.eof
				call printcancel(rs("id"))
				response.write "<br><br><div class=newpage></div>"
			rs.movenext
			loop
			response.write "<input type='button' value='Print' onClick='window.print()' class=noPrint>"
					
		end if	
	closers rs
 
 end sub
 
 sub summarycancel
 if request.form("cmd2")="" then
 %>
 <table class=maintable2><tr><td colspan=2>
 <form method=post action=admin.asp?cmd=summarycancel>
 <h1>Choose Criteria to print Cancellation Forms</h1></td></tr>
<tr><td>Sites:</td><td> <select name=sites><option value="">All Sites</option>
<% 
if session("adminsite")=true then
sqlsites="select * from [site information]"
else
sqlsites="select * from usersites where userid=" & session("userid")
end if
openrs rssites,sqlsites  ' check the sites he can see 
do while not rssites.eof
	response.write "<option value='" & rssites("sitecode") & "'>" &  rssites("sitecode") & "</option>"
	rssites.movenext
loop

closers rssites %>

</select>
</td></tr>
<tr><td>
Date From </td><td><input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
Date To </td><td><input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
File Number From</tD><td> <input type=text name=fnfrom onKeyPress="return checkValue(event)">
</td></tr>
<tr><td>
File Number To</td><td><input type=text name=fnto onKeyPress="return checkValue(event)"></td></tr>
<tr><td>
Plate </td><td><input type=text name=plate></td></tr>
<tr><td colspan=2 align=center>
<input type=hidden name=cmd2 value=proccess>
<input type=submit name=submit value="Submit" class=button>
</form>
</td></tr></table>
 <%
 else
 
 response.write "<h1>Summary of Cancellations</h1>"
	sql="select id as [File No], PCN, PLATE, right(pcn,3) as SITE, cancelled as [CANCELLATION DATE], REASON from cancelled where "
	if request.form("sites")<>"" then
		sql=sql & "site=" & tosql(request.form("sites"),"text")
		i=i+1
	end if
 	if request.form("datefrom")<>"" and request.form("dateto")<>"" then
		if i>0 then sql=sql & " and "
		sql=sql & "(cancelled >="
		datefrom=request.form("datefrom")
		arrdatefrom=split(datefrom,"/")
	myday=arrdatefrom(0)
	'response.write "myday" & myday
	mymonth=arrdatefrom(1)
	myear=arrdatefrom(2)
	'sql=sql & "dateadd(year, " & myear & " - 1900, dateadd(month, " & mymonth & ", dateadd(day, " & myday & ", -32)))"
	sql=sql & "cast('" & myear & mymonth & myday & "' as datetime)"
		'sql=sql & tosql(datefrom,"date")
		sql=sql & " and cancelled<="
		dateto=request("dateto")
		arrdateto=split(dateto,"/")
			myday=arrdateto(0)
	mymonth=arrdateto(1)
	myear=arrdateto(2)
	'	sql=sql & tosql(request.form("dateto"),"date")  & ")"
'sql=sql & "dateadd(year, " & myear & " - 1900, dateadd(month, " & mymonth & ", dateadd(day, " & myday & ", -32))))"
sql=sql & "cast('" & myear & mymonth & myday & " 23:59' as datetime))"

		i=i+1 
	end if
	if request.form("fnfrom")<>"" and request.form("fnto")<>"" then
		if i>0 then sql=sql & " and "
		sql=sql & "(id >="
		fnfrom=request.form("fnfrom")
		sql=sql & fnfrom
		sql=sql & " and id<="
		sql=sql & request("fnto")  & ")"
		i=i+1 
	end if
	if request.form("plate")<>"" then
		if i>0 then sql=sql & " and "
		sql=sql & "plate=" & tosql(request.form("plate"),"text")
		i=i+1
	
	end if
	if i=0 then 
		response.write "You must fill in an option"
		response.end
	end if

 'sql="select id as [File No], PCN, PLATE, SITE, OFFENCEDATE, cancelled as [CANCELLATION DATE], REASON from cancelled"
 call write_html(sql)
end if
 end sub
 sub pcnnotes
 if request("cmd2")="" then
 %>
<table>
 <form method=post ENCTYPE="multipart/form-data" action=savepcnnotes.asp name=notes>
 <input type=hidden name=cmd2 value="save">
 <input type=hidden  name=pcn value="<%= request("pcn") %>">
<tr><td colspan=2>
Add a Note<br>
 <textarea name=note rows=10 cols=60></textarea>
 </td></tr>
<tr><td> File Reference:</td><Td> <input type=text name=filereference size=5></td></tr>
<tr> <td>
 Type:
 </td><Td>
 <input type=radio name=notetype value="Telephone">Telephone
 &nbsp;
  <input type=radio name=notetype value="Letter">Letter
 &nbsp;
  <input type=radio name=notetype value="Fax">Fax
 &nbsp;
  <input type=radio name=notetype value="Email">Email
 &nbsp;
  <input type=radio name=notetype value="Other" checked>Other

 

 </td></tr>
 
 <tr><td>
 Pdf </td><td><input type=file name=pdf></td></tr>
 <tr><td colspan=2 align=center> <input type=submit name=submit value="Save"></td></tr>
 </form>
 <script language="JavaScript" type="text/javascript">
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("notes");
    frmvalidator.addValidation("note","req","Please enter in Note");
  //   frmvalidator.addValidation("type","req","Please choose a  Type");
</script>
 <%
 end if
 if request("cmd2")="save" then
 	ip=Request.ServerVariables("REMOTE_ADDR") 
	t="text"
	
	sql="insert into pcnnotes(pcn,userid,ip,pcnnote,filereference,type) values(" & tosql(request("pcn"),t) & "," & tosql(session("userid"),t) & "," & tosql(ip,t) & "," & tosql(request("note"),t) & "," & tosql(request("filereference"),t)& "," & tosql(request("notetype"),t) &  ")"
	response.write sql
	objconn.execute sql
	response.write "Note successfully saved <br><a href=""javascript:window.close()"">Close</a>"
 
 
 end if
 end sub
 sub shownotes(pcn)
'respone.write "here"
 sql="select pcnnotes.id,pcn,mydate,ip,pcnnote,filereference,pdffile,type,useraccess.userid,clientusers.userid as clientuserid from pcnnotes left join useraccess on useraccess.id=pcnnotes.userid left join clientusers on clientusers.id=pcnnotes.userid where pcn=" & tosql(pcn,"text") & " order by id desc"
 openrs rs,sql
 		if not rs.eof and not rs.bof then response.write "<table class=maintable2><tr><td colspan=2 align=center><b>Notes:</b></td></tr><tr><td><b>Date</b></td><td>Note</td><td>UserName</td><td>File Reference</td><td>Type</td></tr>"
 
 	do while not rs.eof
 	    pcnnote= rs("pcnnote") 
 	    fileref=rs("filereference")
 	    st=rs("type")
 	    userid=rs("userid")
 	    if userid="" then userid=rs("clientuserid")
 	   
 	  
		response.write "<tr><td valign=top>" & rs("mydate") & "</td><td valign=top>" &pcnnote&  "</td><td>" &   userid & "</td><td>" & fileref & "</td><td>" &st & "</td></tr>"
			if rs("pdffile")<>"" then
			response.write "<tr><td colspan=4>"
			%>
			
			<EMBED src="pdfnotesfiles/<%=rs("pdffile")%>" width="450" height="350" "pdfnotesfiles/<%=rs("pdffile")%>"></EMBED>
			<%
			response.write "</td></tr>"
			end if
 	rs.movenext
	loop
	
	
	
	sql="select mydatetime,comments,fileno,useraccess.userid as clientuserid from appeals left join useraccess on useraccess.id=appeals.userid  where comments is not null and  pcn=" & tosql(pcn,"text") & " order by appeals.id desc"
 'resposne.write sql 
 openrs rs,sql
 		'if not rs.eof and not rs.bof then response.write "
 
 	do while not rs.eof
 	    pcnnote= rs("comments") 
 	    fileref=rs("fileno")
 	    st="appeal"
 	    'userid=rs("userid")
 	   ' if userid="" then 
 	   userid=rs("clientuserid")
 	   
 	  
		response.write "<tr><td valign=top>" & rs("mydatetime") & "</td><td valign=top>" &pcnnote&  "</td><td>" &   userid & "</td><td>" & fileref & "</td><td>" &st & "</td></tr>"
			
			response.write "</td></tr>"
		rs.movenext
		loop	
			
	if not rs.eof and not rs.bof then response.write "</table>"
 closers rs
  

 
 sqlap="select * from rm where pcn=" & tosql(pcn,"text")
 'response.write sqlap
openrs rsap,sqlap
 if not rsap.eof and not rsap.bof then
 response.write "<table><tr><th colspan=2>Recorded Calls</th></tr>"
 do while not rsap.eof
 %>
	<tr><td colspan=2 align=center>Recorded Call on  <%=rsap("mydatetime") %> <a href=admin.asp?cmd=viewrm&id=<%=rsap("id") %> class=button target=_new>View Message</a></td></tr>

	<%
 rsap.movenext
loop
 end if 
 closers rsap
 end sub
 
 function showshortnotes(pcn)
 sql="select * from pcnnotes where pcn=" & tosql(pcn,"text") & " order by id desc"
 openrs rs,sql
 	if not rs.eof and not rs.bof then shortnotes= left(rs("pcnnote"),300) & "..."
 closers rs
 
 sqlap="select * from appeals where pcn=" & tosql(pcn,"text")
 openrs rsap,sqlap
 if not rsap.eof and not rsap.bof then
 do while not rsap.eof
 shortnotes=shortnotes & "<br><a href=admin.asp?cmd=viewappeal&id=" & rsap("id") & " target=_new>VM on " & rsap("mydatetime") & "</a>"
	
 rsap.movenext
loop
 end if 
 closers rsap
 
 sqlap="select * from rm where pcn=" & tosql(pcn,"text")
openrs rsap,sqlap
 if not rsap.eof and not rsap.bof then
 do while not rsap.eof
 shortnotes=shortnotes & "<br><a href=admin.asp?cmd=viewrm&id=" & rsap("id") & " target=_new>Recorded Call on " & rsap("mydatetime") & "</a>"
	
 rsap.movenext
loop
 end if 
 closers rsap
 
showshortnotes=shortnotes
end function
sub searchnotes
if request.form("cmd2")="" then
%>
<form method=post action=admin.asp?cmd=searchnotes>
<input type=hidden name=cmd2 value=search>
<%
sqlusers="select * from useraccess"
openrs rsusers,sqlusers
response.write "<table class=maintable2><tr><Th>Employee:</th><td><select name=userid><option value="""">-select-</option>"
do while not rsusers.eof
	response.write "<option value=" & rsusers("id") & ">" & rsusers("userid") & "</option>"
	rsusers.movenext
loop
response.write "</select></td></tr>"
closers rsusers
%> 
<tr><th>Date Range:</th><td>
<table><tr><td>
From </td><td><input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To </td><td><input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)</td></tr>
</table>
</td></tr>
<tr><td colspan=2 align=center><input type=submit name=submit value="Search" class=button></td></tr>
</form>
<%
else
norecords=0
	numserach=0
	sql="select pcn,mydate,ip,pcnnote,filereference,type,useraccess.userid from pcnnotes left join useraccess on useraccess.id=pcnnotes.userid where"
	if request.form("userid")<>"" then
		sql=sql & " pcnnotes.userid=" & tosql(request("userid"),"text")
		numsearch=numsearch+1
	end if
	if request.form("datefrom")<>"" and request.form("dateto")<>"" then
		if numsearch>0 then sql=sql & " and"
		sql=sql & " (mydate >="
		datefrom=request.form("datefrom")
		arrdatefrom=split(datefrom,"/")
		myday=arrdatefrom(0)
		mymonth=arrdatefrom(1)
		myear=arrdatefrom(2)
	'	sql=sql & "dateserial(" & myear & "," & mymonth & "," & myday & ")"
	if len(myday)<2 then myday="0" & myday
	if len(mymonth)<2 then mymonth="0" & mymonth
	sql=sql & "cast('" & myear & mymonth& myday &"' as datetime)"
		sql=sql & " and mydate<="
		dateto=request("dateto")
		arrdateto=split(dateto,"/")
		myday=arrdateto(0)
		mymonth=arrdateto(1)
		myear=arrdateto(2)
	'	sql=sql & "dateserial(" & myear & "," & mymonth & "," & myday & ")"
	if len(myday)<2 then myday="0" & myday
	if len(mymonth)<2 then mymonth="0" & mymonth
	sql=sql & "cast('" & myear & mymonth& myday &" 23:59' as datetime)"
'	sql=sql & "dateadd(year, " & myear & " - 1900, dateadd(month, " & mymonth & ", dateadd(day, " & myday & ", -32)))"
		sql=sql & ")"
	
	end if
	'response.write sql
	openrs rs,sql
	if not rs.eof and not rs.bof then 
		response.write "<table class=maintable2><tr><td colspan=2 align=center><b>Notes:</b></td></tr><tr><td>PCN</td><td><b>Date</b></td><td>Note</td><td>UserName</td><td>File Reference</tr>"
		i=0
 	do while not rs.eof
		if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
		
		response.write "<td>"& rs("pcn") & "</td><td valign=top>" & rs("mydate") & "</td><td valign=top>" & rs("pcnnote") &  "</td><td>" &  rs("userid") & "</td><td>" & rs("filereference") & "</td></tr>"
			
		i=i+1	
 	rs.movenext
	loop
	 response.write "</table>"
	 
	
	else
	    norecords=1
	end if
	
		closers rs	
		
	noarecords=0
	 numsearch=0
	 sql="select * from appeals where"
	if request.form("userid")<>"" then
		sql=sql & " userid=" & tosql(request("userid"),"text")
		numsearch=numsearch+1
	end if
	if request.form("datefrom")<>"" and request.form("dateto")<>"" then
		if numsearch>0 then sql=sql & " and"
		sql=sql & " (timesaved >="
		datefrom=request.form("datefrom")
		arrdatefrom=split(datefrom,"/")
		myday=arrdatefrom(0)
		mymonth=arrdatefrom(1)
		myear=arrdatefrom(2)
	'	sql=sql & "dateserial(" & myear & "," & mymonth & "," & myday & ")"
	if len(myday)<2 then myday="0" & myday
	if len(mymonth)<2 then mymonth="0" & mymonth
	sql=sql & "cast('" & myear & mymonth& myday &"' as datetime)"
		sql=sql & " and timesaved<="
		dateto=request("dateto")
		arrdateto=split(dateto,"/")
		myday=arrdateto(0)
		mymonth=arrdateto(1)
		myear=arrdateto(2)
	'	sql=sql & "dateserial(" & myear & "," & mymonth & "," & myday & ")"
	if len(myday)<2 then myday="0" & myday
	if len(mymonth)<2 then mymonth="0" & mymonth
	sql=sql & "cast('" & myear & mymonth& myday &" 23:59' as datetime)"
'	sql=sql & "dateadd(year, " & myear & " - 1900, dateadd(month, " & mymonth & ", dateadd(day, " & myday & ", -32)))"
		sql=sql & ") and comments is not null"
	
	end if
	'response.write sql
	'response.end
	openrs rs,sql
	if not rs.eof and not rs.bof then 
		response.write "<table class=maintable2><tr><td colspan=2 align=center><b>Notes on Messages:</b></td></tr><tr><td>PCN</td><td><b>Date</b></td><td>Note</td><td>UserName</td><td>Appeal</tr>"
		'i=0
 	do while not rs.eof
		if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
		comments= rs("comments")
		response.write "<td>"& rs("pcn") & "</td><td valign=top>" & rs("timesaved") & "</td><td valign=top>" & comments &  "</td><td>" &  DLookUp("useraccess", "userid", "id=" & rs("userid")) & "</td><td><a href=admin.asp?cmd=viewappeal&p=1&id=" & rs("id") & " class=button target=_new>View Appeal</a></td></tr>"
			
		i=i+1	
 	rs.movenext
	loop
	 response.write "</table>"
else
	    noarecords=1
	end if
	
		closers rs		
		
		
if norecords=1 and noarecords=1 then response.Write "no records matched"
end if
end sub
sub dvlasend

'call dvlarepauto
if request("printedbatch")="" then
	 sqlmanual="select dvla,vwdvlarep.id,offence,dategenerated from vwdvlarep  where printed=0"
else
		sqlmanual="select dvla,vwdvlarep.id,offence,dategenerated from vwdvlarep  where batch=" & request("printedbatch")
end if
'response.write sqlmanual & "<hr>"
 openrs rsmanual,sqlmanual
if not rsmanual.eof and not rsmanual.eof then
'response.write "manual records"
	set fs = CreateObject("Scripting.FileSystemObject")
randomize
 nRandom = (100-1)*Rnd+1
filename =monthname(month(date)) &  cstr(year(date)) & "_" & CStr(nRandom) 
   set file = fs.CreateTextFile(configwebdir & "traffica\dvlarepfiles\" & filename & ".txt", true, false)
   
  do while not rsmanual.eof
dvla=trim(rsmanual("dvla"))
dvla=left(dvla,7)
lend=len(dvla)
'response.write lend
myline=dvla
if lend<7 then
lend=lend+1
for j=lend to 7
myline=myline & " "
next
end if

myline=myline & "H734"

id=rsmanual("id")
lend=len(id)
myline=myline & id
if lend<20 then
lend=lend+1
for j=lend to 20
myline=myline & " "
next
end if


	dategenerated=rsmanual("dategenerated")
	dmonth= month(dategenerated)
	if len(dmonth)<2 then dmonth="0" & dmonth
	dday=day(dategenerated)
if len(dday)<2 then dday= "0" & dday	

	dategenerated=dday &dmonth & year(dategenerated)
	dateevent=dategenerated
lend=len(dateevent)

myline=myline & dateevent
if lend<8 then
lend=lend+1
for j=lend to 8
myline=myline & " "
next
end if




dateg=dategenerated
lend=len(dateg)
myline=myline & dateg
if lend<8 then
lend=lend+1
for j=lend to 8
myline=myline & " "
next
end if
myline=myline & "0" 

   file.WriteLine(myline  )
      rsmanual.movenext
   loop
   file.Close 
   attachment=configwebdir & "traffica\dvlarepfiles\" & filename & ".txt"
response.write "<a href=dvlarepfiles/" & filename & ".txt>Right click and choose save as to save as text file</a>"
	'call SendattachmentEmail ( "info@cleartone-communications.com", "simon.a@cleartoneholdings.co.uk","New Dvla Report - Manual","Attached is a new dvla report for sites set to manual" ,attachment) 
	'call SendattachmentEmail ( "info@cleartone-communications.com", "ec@awebforyou.com","New Dvla Report - Manual","Attached is a new dvla report for sites set to manual" ,attachment) 
	'response.write "file sent"


end if
rsmanual.close


'update printed to true
sql="update dvlarep set printed=1 where printed=0"
objconn.execute sql

end sub

sub dvlareport
'if request("manualonly")="yes" then
	'call dvlasend
	'exit sub

'end if
if request("printedbatch")="" then
' check that no invalid sites
sqlv="select * from dvlarep where printed=0 and site not in(select sitecode from [site information])"
openrs rsv,sqlv
if not rsv.eof and not rsv.bof then 
	call adminmenu
	response.write "Invalid site in file -- dvla will not be printed"
	sql="delete  from dvlarep where printed=0"
	objconn.execute sql
	exit sub

end if
closers rsv
sql="select count(dvlarep.id) as countr from dvlarep LEFT JOIN [Site Information] ON dvlarep.site = [Site Information].Siteref where printed=0 and [Site Information].requesttype='Auto'"
sqla="select offence, site, dvla,vwdvlarep.id,dategenerated,[Site Information].Sitecode, [Site Information].requesttype from vwdvlarep LEFT JOIN [Site Information] ON vwdvlarep.site = [Site Information].SiteRef where printed=0 and [Site Information].requesttype='Auto' order by offence,site "
'response.write sqla & "<hr>"
sqlmanual="select dvla,dvlarep.id,offence,dategenerated,[Site Information].Sitecode, [Site Information].requesttype from dvlarep LEFT JOIN [Site Information] ON dvlarep.site = [Site Information].SiteRef where printed=0 and [Site Information].requesttype='Manual' order by offence,site "
'response.write sqlmanual & "<hr>"
 openrs rsmanual,sqlmanual
if not rsmanual.eof and not rsmanual.eof then
'response.write "manual records"
	set fs = CreateObject("Scripting.FileSystemObject")
randomize
 nRandom = (100-1)*Rnd+1
filename =monthname(month(date)) &  cstr(year(date)) & "_" & CStr(nRandom) 
   set file = fs.CreateTextFile(configwebdir & "traffica\dvlarepfiles\" & filename & ".txt", true, false)
   
  do while not rsmanual.eof
dvla=rsmanual("dvla")
lend=len(dvla)
myline=dvla
if lend<7 then
for j=lend to 7
myline=myline & " "
next
end if

myline=myline & "H734"

id=rsmanual("id")
lend=len(id)
myline=id
if lend<20 then
for j=lend to 20
next
myline=myline & " "
end if


dateevent=replace(rsmanual("offence"),"-","")
lend=len(dateevent)
myline=myline & dateevent
if lend<8 then
for j=lend to 8
myline=myline & " "
next
end if
   file.WriteLine(myline  )
      rsmanual.movenext
   loop
	 rsmanual.close
	
   file.Close 
   attachment=configwebdir & "traffica\dvlarepfiles\" & filename & ".txt"
	call SendattachmentEmail ( "info@cleartone-communications.com", "simon.a@cleartoneholdings.co.uk","New Dvla Report - Manual","Attached is a new dvla report for sites set to manual" ,attachment) 
	'call SendattachmentEmail ( "info@cleartone-communications.com", "ec@awebforyou.com","New Dvla Report - Manual","Attached is a new dvla report for sites set to manual" ,attachment) 
end if
'rsmanual.close

printeddate=date()
else

	sqlwhere=sqlwhere & "batch=" & request("printedbatch")

	sql="select  count(distinct dvlarep.id) as countr from dvlarep LEFT JOIN [Site Information] ON dvlarep.site = [Site Information].Siteref where [Site Information].requesttype='Auto' and " & sqlwhere 
	
	sqla="select * from vwdvlarep LEFT JOIN [Site Information] ON vwdvlarep.site = [Site Information].Siteref where [Site Information].requesttype='Auto' and " & sqlwhere  & " order by offence,site "
end if
'response.write sqla
Set rs = Server.CreateObject("ADODB.Recordset")
rs.Open sql, objconn

i=rs("countr")
rs.close 
sql=sqla
 session("numcovers")=0
 session("numregs")=0
 session("numpages")=0
 Set rs = Server.CreateObject("ADODB.Recordset")
'response.write "<hr>" & sql
   rs.Open sql, objconn
 	dim arrregs()
	dim arroffence()
	dim arrsite()
   redim arrregs(i)
   	redim arroffence(i)
	redim arrsite(i)
	dim arrid()
	redim arrid(i)
	printeddate=rs("dategenerated")'now() 'rs("dategenerated")
	j=0
	do while not rs.eof
		site=rs("site")
		offence=rs("offence")
		dvla=rs("dvla")
		id=rs("id")
		mydate=rs("dategenerated")
		if offence=loffence and site=lsite then 
			arrregs(j)=arrregs(j) & "," & dvla
			arrid(j)=arrid(j) & "," & id	
		else
		
		j=j+1
		
			'response.write "site: " & site & " offence:" & offence & "<hr>"
			arrsite(j)=site
			arroffence(j)=offence
			arrregs(j)=dvla
			arrid(j)=id
		'	response.write dvla & "<br>"
		loffence=offence
		lsite=site
		'response.write "(loffence:	" & loffence & ")"
		end if
	rs.movenext
	loop
	rs.close 
	

For i = 0 to ubound(arrregs)
if arrregs(i)<>"" then 
'	response.write arroffence(i) & "-" & arrsite(i) & ":" & arrregs(i) &"<br>"
	call createdvlarep(arroffence(i),arrsite(i),arrregs(i),arrid(i),mydate)
	
	
	
	
	

	'arrdvla=split(arrregs(i),",",-1,1)
	'numregs=numregs+ ubound(arrdvla)
	
end if
next 
	call printintrosheet(session("numcovers"),session("numregs"),session("numpages"),printeddate)
	sql="update dvlarep set printed=1 where printed=0"
objconn.execute sql
'response.write sql
end sub
function fm2(mymonth)
fm2=mymonth
if len(fm2)<2 then fm2="0" & mymonth



end function
function strformat(mystring,mynum)
'response.write "<hr>"
mynum=mynum-1
mystring=cstr(mystring)
spaces=""
lend=len(mystring)
'response.write lend
if lend<mynum then
for j=lend to mynum
spaces=spaces & "0"
next
strformat=spaces & mystring
'response.write strformat
'response.write "<hr>"
end if

end function
sub dvlareportnew
'if request("manualonly")="yes" then
	'call dvlasend
	'exit sub

'end if
'if request("printedbatch")<>"" then
' check that no invalid sites

'sqlmanual="select dvla,dvlarep.id,offence,dategenerated,[Site Information].Sitecode, [Site Information].requesttype from dvlarep LEFT JOIN [Site Information] ON dvlarep.site = [Site Information].SiteRef "  & "where batch=" & request("printedbatch") & " order by offence,site "
if request("printedbatch")="" then
	 sqlmanual="select dvla,vwdvlarep.id,offence,dategenerated from vwdvlarep  where printed=0"
else
		sqlmanual="select dvla,vwdvlarep.id,offence,dategenerated from vwdvlarep  where batch=" & request("printedbatch")
end if
	'sqlwhere=sqlwhere & "batch=" & request("printedbatch")
'response.write sqlmanual

'sqlmanual="select dvla,dvlarep.id,offence,dategenerated,[Site Information].Sitecode, [Site Information].requesttype from dvlarep LEFT JOIN [Site Information] ON dvlarep.site = [Site Information].SiteRef where printed=0 and [Site Information].requesttype='Manual' order by offence,site "
'response.write sqlmanual & "<hr>"
 openrs rsmanual,sqlmanual
if not rsmanual.eof and not rsmanual.eof then
'response.write "manual records"
	set fs = CreateObject("Scripting.FileSystemObject")
randomize
 nRandom = (100-1)*Rnd+1
filename =monthname(month(date)) &  cstr(year(date)) & "_" & CStr(nRandom) 
filename="DX-FX-VV.txt"
   set file = fs.CreateTextFile(configwebdir & "traffica\dvlarepfiles\" & filename, true, false)
   lines=1
	 todayformatted=right(year(date),2) & fm2(month(date)) & fm2(day(date)) & ":" & replace(formatdatetime(now,4),":","")
  formattedbatch=strformat(request("printedbatch"),8)

mydate=date





myline="//STX12//;;DVLA;DOCUMENTNUMBER;;VEHENQ;;"
file.WriteLine(myline  )

	myline="01;VQ3;" & right(year(date),2) & fm2(month(date)) & fm2(day(date)) & ";101"
	file.writeline(myline)
		n=1
		
	do while not rsmanual.eof 'and n<50
	adateevent=split(rsmanual("offence"),"-")
		dateevent=right(adateevent(2),2) & fm2(adateevent(1)) & fm2(adateevent(0))
		myline="02;" & strformat(n,6) & ";" & rsmanual("dvla") & ";" & dateevent 
  file.WriteLine(myline  )
	myline="03;101;" & rsmanual("id") 
	file.writeline(myline)
myline="04;H391"
	 file.WriteLine(myline  )
		
		
	 n=n+1
      rsmanual.movenext
   loop
	 rsmanual.close
	
   
	 
		
		file.close
   attachment=configwebdir & "traffica\dvlarepfiles\" & filename 
	 response.write "<a href=" & configweburl & "dvlarepfiles/" & filename & ">Right Click - save as to download</a>"
	'call SendattachmentEmail ( "info@cleartone-communications.com", "simon.a@cleartoneholdings.co.uk","New Dvla Report - Manual","Attached is a new dvla report for sites set to manual" ,attachment)
	else
			response.write "no records"
	end if 
	if request("printedbatch")="" then
	sql="update dvlarep set printed=1 where printed=0"
objconn.execute sql
end if
end sub
sub zdvlareportnew
'if request("manualonly")="yes" then
	'call dvlasend
	'exit sub

'end if
'if request("printedbatch")<>"" then
' check that no invalid sites

sqlmanual="select dvla,dvlarep.id,offence,dategenerated,[Site Information].Sitecode, [Site Information].requesttype from dvlarep LEFT JOIN [Site Information] ON dvlarep.site = [Site Information].SiteRef "  & "where batch=" & request("printedbatch") & " order by offence,site "

	'sqlwhere=sqlwhere & "batch=" & request("printedbatch")
'response.write sqlmanual

'sqlmanual="select dvla,dvlarep.id,offence,dategenerated,[Site Information].Sitecode, [Site Information].requesttype from dvlarep LEFT JOIN [Site Information] ON dvlarep.site = [Site Information].SiteRef where printed=0 and [Site Information].requesttype='Manual' order by offence,site "
'response.write sqlmanual & "<hr>"
 openrs rsmanual,sqlmanual
if not rsmanual.eof and not rsmanual.eof then
'response.write "manual records"
	set fs = CreateObject("Scripting.FileSystemObject")
randomize
 nRandom = (100-1)*Rnd+1
filename =monthname(month(date)) &  cstr(year(date)) & "_" & CStr(nRandom) 
   set file = fs.CreateTextFile(configwebdir & "traffica\dvlarepfiles\" & filename & ".txt", true, false)
   lines=1
	 todayformatted=right(year(date),2) & fm2(month(date)) & fm2(day(date)) & ":" & replace(formatdatetime(now,4),":","")
  formattedbatch=strformat(request("printedbatch"),8)
	myline="UNB+UNOA:2+CLEARTONE+DVLA+" & todayformatted & "+" & formattedbatch & "++VEHENQ'"
'response.write myline
'response.end
	  file.WriteLine(myline  )
	myline="UNH+" & strformat(request("printedbatch"),6) & "+VEHENQ:0:2:DV:DVLA'"
	  file.WriteLine(myline  )
		lines=lines+1
	myline ="BGM+VQ3:DVM:DVL:001'"
	  file.WriteLine(myline  )
		lines=lines+1
		myline="PNA+MS+H391:DVP:DVL'"
		file.writeline(myline)
		lines=lines+1
		n=1
		
	do while not rsmanual.eof 
	adateevent=split(rsmanual("offence"),"-")
		dateevent=adateevent(2) & fm2(adateevent(1)) & fm2(adateevent(0))
	myline="DTM+89:" & dateevent&":102'"
		file.writeline(myline)
		lines=lines+1
		myline="RFF+" & rsmanual("id") & ""
		file.writeline(myline)
		lines=lines+1
		myline="LIN+" & n & "+ZZZ+" & rsmanual("dvla") & ":VRM:REG:DVL'"
		file.writeline(myline)
		lines=lines+1
		
	 n=n+1
      rsmanual.movenext
   loop
	 rsmanual.close
	
   
	 myline="RFF+AAV:9843QOT/237/ZX'"
		file.writeline(myline)
		lines=lines+1
		myline="UNT+" & lines & "+" & strformat(request("printedbatch"),6) & "'"
		file.writeline(myline)
		myline="UNZ+" & n+1 & "+" & formattedbatch & "'"
		file.writeline(myline)
		file.close
   attachment=configwebdir & "traffica\dvlarepfiles\" & filename & ".txt"
	 response.write "<a href=http://cleartonecommunications.com/traffica/dvlarepfiles/" & filename & ".txt>View File</a>"
	'call SendattachmentEmail ( "info@cleartone-communications.com", "simon.a@cleartoneholdings.co.uk","New Dvla Report - Manual","Attached is a new dvla report for sites set to manual" ,attachment)
	else
			response.write "no records"
	end if 
end sub
sub createdvlarep(offence,site,regs,ids,mydate)
'on error resume next
'response.write regs

if instr(regs,",")>0 then
	
	arrdvla=split(regs,",",-1,1)
	arrid=split(ids,",",-1,1)
	totalregs=ubound(arrdvla)
	session("numregs")=session("numregs")+totalregs+1
else	
	totalregs=0
	session("numregs")=session("numregs")+1
end if
	'response.write "<font class=red>" & session("numregs") &  "</font>"
	arroffence=split(offence,"-")
	offenced=arroffence(0)
	offencem=arroffence(1)
	offencey=arroffence(2)
	if mydate<cdate("14/05/2007") then
	
	coverpage ="<div class=pageoneold> <span class=twodate><img src=spacer.gif width=5 height=1>" &  offenced & "<img src=spacer.gif width=30 height=1>" & offencem & "<img src=spacer.gif width=40 height=1>" & offencey &  "</span> <span class=twosite>"

	else
coverpage ="<div class=pageone> <span class=twodate><img src=spacer.gif width=5 height=1>" &  offenced & "<img src=spacer.gif width=30 height=1>" & offencem & "<img src=spacer.gif width=40 height=1>" & offencey &  "</span> <span class=twosite>"

 end if
 'coverpage=coverpage &  "site here"
 sqlsi="select * from [site information] where siteref='" & site & "'"
'response.write sqlsi
 openrs rssi,sqlsi
 if not rssi.eof and not rssi.bof then
' coverpage=coverpage & rssi("Name of Site") & "<br>"
 ' coverpage=coverpage & rssi("Borough") & "<br>"
   coverpage=coverpage & rssi("Address1") & "<br>"
   if rssi("Address2")<>"" then  coverpage=coverpage & rssi("Address2") & "<br>"
      if rssi("Address3")<>"" then  coverpage=coverpage & rssi("Address3") & "<br>"
	 ' coverpage=coverpage & rssi("town") & "&nbsp;" & rssi("postcode")
 end if
 closers rssi
 coverpage=coverpage & "</span><span class=fivereg>"
  if totalregs=0 then 
	coverpage=coverpage &  regs
else
	coverpage=coverpage & "see attached"
end if
	
 coverpage=coverpage & "</span> "
 ' If totalregs=0 Then 
  
	
 '  End If 
   	coverpage=coverpage & "<span class=fileref>" & site & "</span> "
   coverpage=coverpage & "<span class=make>N/A</span>"
	coverpage=coverpage & "<span class=model>N/A</span>"
		coverpage=coverpage & "<span class=dateprinted>" & formatdatetime(mydate,2) & "</span>"
  coverpage=coverpage & "</div><div class=newpage></div>"
  session("numpages")=session("numpages")+1
response.write coverpage
session("numcovers")=session("numcovers")+1
if totalregs>0 then
'response.write "<div class=pagetwo><span class=pagetworeg>"
if mydate<cdate("14/05/2007") then
response.write "<div class=pagetwo><table cellspacing=15><tr><th>Reg Mark</th><th>Make</th><th>Model</th><th>Your Reference</th></tr>"
else
response.write "<div class=pagetwonew><table cellspacing=15><tr><th>Reg Mark</th><th>Make</th><th>Model</th><th>Your Reference</th></tr>"

end if		
k=0
i=0
for each dvla in arrdvla
	if k=15 then
	k=0
	if mydate<cdate("14/05/2007") then
		response.write "<tr><td><img src=spacer.gif height=100></td></tr><tr><td>" & offence & "</td></tr></table> <div class=bottom>Creative Car Park Management Ltd. Registered Office: 19/20     Bourne Court, Southend Road, Woodford Green IG8 8HD. Registration Number 04304436.</div> </div><div class=newpage></div>" & coverpage '
	else
	response.write "<tr><td><img src=spacer.gif height=100></td></tr><tr><td>" & offence & "</td></tr></table> <div class=bottom>Creative Car Park Ltd. Registered Office: 19/20     Bourne Court, Southend Road, Woodford Green IG8 8HD. Registration Number 05571660</div> </div><div class=newpage></div>" & coverpage '
	end if	
		
		 session("numpages")=session("numpages")+1
		session("numcovers")=session("numcovers")+1
		if mydate<cdate("14/05/2007") then
		response.write "<div class=pagetwo><table cellspacing=15><tr><th>Reg Mark</th><th>Make</th><th>Model</th><th>Your Reference</th></tr>"
		else
		response.write "<div class=pagetwonew><table cellspacing=15><tr><th>Reg Mark</th><th>Make</th><th>Model</th><th>Your Reference</th></tr>"
		
		end if
		
				response.write "<tr><td>" & k+1 & ".  " & dvla & "<td>N/A</td><td>N/A</td><td>" & site & "&nbsp;" & arrid(i) & "</td></tr>" '<div class=newpage></div>"
		 session("numpages")=session("numpages")+1
		
	else
		response.write "<tr><td>" & k +1 & ".  " & dvla & "<td>N/A</td><td>N/A</td><td>" & site & "&nbsp;" & arrid(i) & "</td></tr>"
	
	end if
	k=k+1
	i=i+1
 next
 if mydate<cdate("14/05/2007") then
 response.write " <tr><td><img src=spacer.gif height=100></td></tr><tr><td>" & offence & "</td></tr></table> <div class=bottom>Creative Car Park Management Ltd. Registered Office: 19/20     Bourne Court, Southend Road, Woodford Green IG8 8HD. Registration Number 04304436.</div> </div>"
 else
 response.write " <tr><td><img src=spacer.gif height=100></td></tr><tr><td>" & offence & "</td></tr></table> <div class=bottom>Creative Car Park Ltd. Registered Office: 19/20     Bourne Court, Southend Road, Woodford Green IG8 8HD. Registration Number 05571660.</div> </div>"
 
 end if
 
 
 '05571660
 response.write "<div class=newpage></div>"
  session("numpages")=session("numpages")+1
 ' response.write "<font class=red>" & session("numpages") & "</font>"
end if
'response.write "<img src=resume.gif>"
call printresume(mydate)
session("numpages")=session("numpages")+1
end sub

sub printintrosheet(numcovers,numregs,numpages,printeddate)
if printeddate<cdate("14/05/2007") then
	 %>
	 
	 
	 
<div class=pagetwo> Vehicle Record Enquiries<br>
  Vehicle Customer Services<br>
  DVLA<br>
  Swansea<br>
  SA99 1AJ 
  <p align=right><%= day(printeddate) %>&nbsp; <%= monthname(month(printeddate)) %> 
    &nbsp;<%= year(printeddate) %></p>
  <p>Dear Sirs,</p>
  <p>Please find enclosed <%= numcovers %> V888/3 forms relating to a total of 
    <%= numregs %> vehicles that have not paid parking charges at the properties, 
    on the dates shown. </p>
  <p>Please also find attached a cheque in the sum of &pound;<%= formatnumber(numregs*2.50,2) %> 
    (i.e. &pound;2.50 per enquiry) payable to DVLA and we would request that you 
    send us a form VQ7 for each vehicle.</p>
  <p>In the event of any query, please contact us on 0870 919 8000.</p>
  <p>Yours faithfully,</p>
  <p></p>
  <p>Creative Car Park Management Ltd<br>
    T/as Civil Enforcement</p>
  <p><br>
    <%= session("numpages") %> pages enclosed<br>
  </p>
  <div class=bottom>Creative Car Park Management Ltd. Registered Office: 19/20 
    Bourne Court, Southend Road, Woodford Green IG8 8HD. Registration Number 04304436.</div>
</div>
 

	 
	 <%
else
%>
<div class=pagetwonew> Vehicle Record Enquiries<br>
  Vehicle Customer Services<br>
  DVLA<br>
  Swansea<br>
  SA99 1AJ 
  <p align=right><%= day(printeddate) %>&nbsp; <%= monthname(month(printeddate)) %> 
    &nbsp;<%= year(printeddate) %></p>
  <p>Dear Sirs,</p>
  <p>Please find enclosed <%= numcovers %> V888/3 forms relating to a total of 
    <%= numregs %> vehicles that have not paid parking charges at the properties, 
    on the dates shown. </p>
  <p>Please also find attached a cheque in the sum of &pound;<%= formatnumber(numregs*2.50,2) %> 
    (i.e. &pound;2.50 per enquiry) payable to DVLA and we would request that you 
    send us a form VQ7 for each vehicle.</p>
  <p>In the event of any query, please contact us on 0870 919 8000.</p>
  <p>Yours faithfully,</p>
  <p></p>
  <p>Creative Car Park Ltd<br>
    T/as Civil Enforcement</p>
  <p><br>
    <%= session("numpages") %> pages enclosed<br>
  </p>
  <div class=bottom>Creative Car Park Ltd. Registered Office: 19/20 Bourne Court, 
    Southend Road, Woodford Green IG8 8HD. Registration Number 05571660.</div>
</div>
 

<%
end if
end sub

sub printresume(printeddate)

if printeddate<cdate("14/05/2007") then
	 %>
	 
<div class=pagetwo> Fee Paying Enquiry Section<br>
  DVLA<br>
  Swansea<br>
  SA99 1AJ 
  <p>19 November 2006</p>
  <p><br>
    Dear Sir,</p>
  <p>Company Resume</p>
  <p>The company was established in 2001 by Gary Wayne BSc (Hons) Estate Management, 
    a qualified surveyor with 20 years car park consultancy experience.</p>
  <p>Creative Car Park is a member of the BPA (British Parking Association) and 
    all directors hold full licenses from the SIA (Security Industry Authority).</p>
  <p>The company provides car park management and consultancy services to occupiers 
    including The Co-Operative Society, Aldi Stores, BP Oil, MacDonald&#8217;s, 
    Sainsbury&#8217;s, Argos, Next, Boots and many other blue chip companies.</p>
  <p>The vehicles to which the attached registration numbers relate have all parked 
    in car parks that we manage on behalf of our client and to whom we now need 
    to send a PCN. All drivers have been made aware that we use ANPR (automatic 
    number plate recognition) cameras to monitor the car park and that we will 
    contact the DVLA to obtain their details in order to send them a PCN. We adhere 
    to all guidance issued by the information commissioner in relation to the 
    handling of our data. We feel it important to point out these facts as part 
    of our company resume as they form part of our strict company policy at all 
    sites.</p>
  <p>You are invited to visit our processing centre at your earliest convenience, 
    to see for yourself the high standards of our operation and security procedures 
    in place to protect the data that we handle.</p>
  <p>Yours faithfully<br>
    <br>
    Ashley Cohen<br>
    Creative Car Park Management Ltd<br>
    T/as Civil Enforcement</p>
  <div class=bottom>Creative Car Park Management Ltd. Registered Office: 19/20 
    Bourne Court, Southend Road, Woodford Green IG8 8HD. Registration Number 04304436.</div>
</div>
	 
	 <%
else
	 %>
<div class=pagetwonew> Fee Paying Enquiry Section<br>
  DVLA<br>
  Swansea<br>
  SA99 1AJ 
  <p>19 November 2006</p>
  <p><br>
    Dear Sir,</p>
  <p>Company Resume</p>
  <p>The company was established by Gary Wayne BSc (Hons) Estate Management, a 
    qualified surveyor with 20 years car park consultancy experience.</p>
  <p>Creative Car Park is a member of the BPA (British Parking Association) and 
    all directors hold full licenses from the SIA (Security Industry Authority).</p>
  <p>The company provides car park management and consultancy services to occupiers 
    including The Co-Operative Society, Aldi Stores, BP Oil, MacDonald&#8217;s, 
    Sainsbury&#8217;s, Argos, Next, Boots and many other blue chip companies.</p>
  <p>The vehicles to which the attached registration numbers relate have all parked 
    in car parks that we manage on behalf of our client and to whom we now need 
    to send a PCN. All drivers have been made aware that we use ANPR (automatic 
    number plate recognition) cameras to monitor the car park and that we will 
    contact the DVLA to obtain their details in order to send them a PCN. We adhere 
    to all guidance issued by the information commissioner in relation to the 
    handling of our data. We feel it important to point out these facts as part 
    of our company resume as they form part of our strict company policy at all 
    sites.</p>
  <p>You are invited to visit our processing centre at your earliest convenience, 
    to see for yourself the high standards of our operation and security procedures 
    in place to protect the data that we handle.</p>
  <p>Yours faithfully<br>
    <br>
    Ashley Cohen<br>
    Creative Car Park Ltd<br>
    T/as Civil Enforcement</p>
  <div class=bottom>Creative Car Park Ltd. Registered Office: 19/20 Bourne Court, 
    Southend Road, Woodford Green IG8 8HD. Registration Number 05571660.</div>
</div>
<%end if%>
<div class=newpage></div>
<%
end sub
sub dvlareportupl
response.redirect "dvlaupload.asp"
end sub

sub dvlaupl
response.redirect "dvlaautoupload.asp"
end sub


sub dvlareportupl2
response.redirect "dvlaupload2.asp"
end sub

sub dvlarepfile
sfile=request("sfile")
path=configwebdir & "data\"
sfile=path & sfile
Set fs=Server.CreateObject("Scripting.FileSystemObject")
	Set f=fs.OpenTextFile(sfile, 1)
	
	Do While f.AtEndOfStream=False
f.Readline
numoflines=numoflines+1
Loop
f.Close
	set f=Nothing
		sqlb="select batchnumber from batchnumbers order by batchnumber desc"
				Set rs = Server.CreateObject("ADODB.Recordset")
				rs.Open sqlb, objconn
				lastbatchnumber=rs("batchnumber")
				rs.close 
				sql="insert into  batchnumbers (mydate) values(getdate())"
				objconn.execute sql
		Set fs=Server.CreateObject("Scripting.FileSystemObject")
	Set f=fs.OpenTextFile(sfile, 1)
		
		
	i=1
	do while f.AtEndOfStream = false
		str=f.readline
		if str<>""  then
			arrstr=split(str,vbtab,-1,1)
			if ubound(arrstr)>1 then
				'response.write "<hr>"
				'for each rec in arrstr
				'	if rec<>"" then 
		
						sql="insert into dvlarep(offence,dvla,site,batch) values ('" & arrstr(0) & "','" & arrstr(1) & "','" & arrstr(3) & "'," & lastbatchnumber +1  & ")"
					'	response.write sql
					objconn.execute sql
				'next		
					
					'nd if
			'	next
			end if
		end if
	loop
	f.Close
	
	set f=Nothing
	'fs.DeleteFile(sfile)
	Set fs=Nothing
	if request("manualonly")="yes" then
	response.redirect "admin.asp?cmd=dvlareport&manualonly=yes"
	else
	response.redirect "admin.asp?cmd=dvlareport"
	end if

end sub

sub dvlarepfile2
sfile=request("sfile")
path=configsecuredir & "data\"
sfile=path & sfile
Set fs=Server.CreateObject("Scripting.FileSystemObject")
	Set f=fs.OpenTextFile(sfile, 1)
	
	Do While f.AtEndOfStream=False
f.Readline
numoflines=numoflines+1
Loop
f.Close
	set f=Nothing
		sqlb="select batchnumber from batchnumbers order by batchnumber desc"
				Set rs = Server.CreateObject("ADODB.Recordset")
				rs.Open sqlb, objconn
				lastbatchnumber=rs("batchnumber")
				rs.close 
				sql="insert into  batchnumbers (mydate) values(getdate())"
				objconn.execute sql
		Set fs=Server.CreateObject("Scripting.FileSystemObject")
	Set f=fs.OpenTextFile(sfile, 1)
		
		
	i=1
	do while f.AtEndOfStream = false
		str=f.readline
		if str<>""  then
			sql="insert into dvlarep(dvla,batch) values ('" & str & "'," & lastbatchnumber +1  & ")"
					'	response.write sql
					objconn.execute sql
				
			
			
		end if
	loop
	f.Close
	
	set f=Nothing
	'fs.DeleteFile(sfile)
	Set fs=Nothing
	if request("manualonly")="yes" then
	response.redirect "admin.asp?cmd=dvlareport&manualonly=yes"
	else
	response.redirect "admin.asp?cmd=dvlareport"
	end if
'
end sub
sub zdvlarepauto
'' check for new violators
	sqlm2="select * from tempviolators where registration not in(select dvla from dvlarep)"
	openrs rsm2,sqlm2
if not rsm2.eof and not rsm2.bof then
sqlb="select batchnumber from batchnumbers order by batchnumber desc"
				Set rs = Server.CreateObject("ADODB.Recordset")
				rs.Open sqlb, objconn
				lastbatchnumber=rs("batchnumber")
				rs.close 
				sql="insert into  batchnumbers (mydate) values(getdate())"
				objconn.execute sql

do while not rsm2.eof
	sql="insert into dvlarep(offence,dvla,site,batch,printed) values('" & day(rsm2("date of violation")) &"-" & month(rsm2("date of violation")) & "-" & year(rsm2("date of violation"))  &"','" & rsm2("registration") &"','" &  rsm2("site") & "'," & lastbatchnumber +1 & ",0)"
	objconn.execute sql
rsm2.movenext
loop

end if
end sub
sub dvlasearch
%>

<table>
<form method=post action=admin.asp?cmd=dvlafind>
<tr><td align=center>Enter in search term to search dvla table</td></tr>
<tr><td align=center> <input type=text name=search></td></tr>
<tr><td align=center><input type=submit  value="Search"></td></tr>


</form>
</table>

<%
end sub
sub dvlafind
	searchterm=request("search")
	if searchterm<>"" then i=1
	if request("s")=1 then
		sql=session("sql")
		i=1
		rcount=2
	else
		sqlw="SELECT DVLA.Reg,  DVLA.Model, DVLA.Colour, DVLA.Owner, DVLA.Address1, DVLA.Address2, DVLA.Address3, DVLA.Town, DVLA.Postcode, DVLA.[DVLA Status], DVLA.[DateReceived] FRom dvla where  "
		sqlc="select count(reg) as countc from dvla where "
		sql="DVLA.Reg Like '%" & searchterm & "%' OR DVLA.Make Like '%" & searchterm & "%' OR DVLA.Model Like '%" & searchterm & "%' OR DVLA.Colour Like '%" & searchterm & "%' OR DVLA.Owner Like '%" & searchterm & "%' OR DVLA.Address1 Like '%" & searchterm & "%' OR DVLA.Address2 Like '%" & searchterm & "%' OR DVLA.Address3 Like '%" & searchterm & "%' OR DVLA.Town Like '%" & searchterm & "%' OR DVLA.Postcode like '%" & searchterm & "%' OR  DVLA.[DVLA Status] Like '%" & searchterm & "%'"
		
		
		' OR DVLA.id=" & searchterm 
		sqlc=sqlc& sql
		sql=sqlw & sql
		'response.write sql
		openrs rsc,sqlc
		rcount=rsc("countc")
		closers rsc 
		end if
	
	if i=0 then 
		response.write "You must fill in a search field"
		call dvlasearch
	else
		
		strpagename="admin.asp?cmd=dvlafind"
			
		if rcount=0 then 
			response.write "no records exist with this criteria"
		'elseif rcount=1 then call viewrecord
		else
				createdvlaSortableList objConn,sql,"reg",25,"class=maintable2w",strpagename
		end if
	end if

end sub

Sub createdvlaSortableList(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
		
    

	'response.write "strsort:" & strsort	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
    			
    		Set RS = server.CreateObject("adodb.recordset")
    			
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  
				Response.Write "<TD align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</TD>"	& vbcrlf
   
	
	Next
					
    		Response.Write "<td>&nbsp;</td></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			Response.Write "<TR>" & vbcrlf
    			For Each field In RS.Fields 'for each field in the recordset
				
					Response.Write "<TD align=center>" & vbcrlf
    				 Response.Write field.value
						 	
    				Response.Write "</TD>" & vbcrlf
				
    			Next
			
					
				response.write "<td><a target=_new href=admin.asp?cmd=search&plate=" & rs("reg") & " class=button>PCN</a></td>"
				end if
				Response.Write "</TR>" & vbcrlf
    if not rs.eof then	RS.MoveNext
    			'End if
    		Next
    				
    		Response.Write "<TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    '		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    '		End if
    		Set RS = nothing			
 	End Sub
	
	Sub createdvlaSortableListe(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
		
    

	'response.write "strsort:" & strsort	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
    			
    		Set RS = server.CreateObject("adodb.recordset")
    			
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  
				Response.Write "<Th align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</Th>"	& vbcrlf
   
	
	Next
					
    		Response.Write "<td>&nbsp;</td></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
    			For Each field In RS.Fields 'for each field in the recordset
				
					Response.Write "<TD align=center>" & vbcrlf
    				 Response.Write field.value
						 	
    				Response.Write "</TD>" & vbcrlf
				
    			Next
			
					
				response.write "<td><a target=_new href=admin.asp?cmd=search&p=1&plate=" & rs("reg") & " class=button>PCN</a></td>"
					response.write "<td><a target=_new href=admin.asp?cmd=editdvla&p=1&id=" & rs("id") & " class=button>Edit DVLA</a></td>"
				
				end if
				Response.Write "</TR>" & vbcrlf
    if not rs.eof then	RS.MoveNext
    			'End if
    		Next
    				
    		Response.Write "<TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    '		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    '		End if
    		Set RS = nothing			
 	End Sub
sub dvlarepreprint
if request.form("cmd2")="" then
%>

<form method=post action=admin.asp?cmd=dvlarepreprint><input type=hidden name=cmd2 value="print">
Reprint Dvla report printed on 
<input type=text name=printeddate>
<a href="javascript:NewCal('printeddate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>
<input type=submit value=submit name=sumbit>
</form>

<%
else
	
		sql="select * from batchnumbers where " 
		sqlwhere=sqlwhere & "mydate >="
	datefrom=request.form("printeddate")
	arrdatefrom=split(datefrom,"/")
	myday=arrdatefrom(0)
	mymonth=arrdatefrom(1)
	myear=arrdatefrom(2)
'	sqlwhere=sqlwhere & "dateserial(" & myear & "," & mymonth & "," & myday & ")"
'sqlwhere=sqlwhere & "dateadd(year, " & myear & " - 1900, dateadd(month, " & mymonth & ", dateadd(day, " & myday & ", -32)))"
sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & "' as datetime)"
	sqlwhere=sqlwhere & " and mydate<="
	dateto=request("printeddate")
	arrdateto=split(dateto,"/")
	myday=arrdateto(0)
	mymonth=arrdateto(1)
	myear=arrdateto(2)
	
	sqlwhere=sqlwhere  & "cast('" & myear & mymonth & myday & " 23:59' as datetime)"
'	response.write "<hr>" & sqlwhere & "<hr>"
	
	sql=sql&sqlwhere 
	'sql=sql & " group by dvlarep.batch"
'	response.write sql
	 Set rs = Server.CreateObject("ADODB.Recordset")
   		rs.Open sql, objconn
		if rs.eof and rs.bof then 
			response.write "No report printed on this date"
		else
		response.write "<form method=post action=admin.asp><input type=hidden name=cmd2 value=print>"
		response.write "Please choose the time the report was printed:<br><select name=printedbatch>"
		do while not rs.eof
			response.write "<option value='" & rs("batchnumber") & "'>" & rs("mydate") & "</option>"
		rs.movenext
		loop
		response.write "</select><br><input type=radio name=cmd value=dvlareportnew checked>Automatic File &nbsp;<input type=radio name=cmd value=dvlareport>Manual<br><input type=submit name=submit value=print></form>"
		end if
		rs.close 
end if
end sub

sub dvlarepreprintbatch
if request.form("cmd2")="" then
%>

<form method=post action=admin.asp?cmd=dvlarepreprintbatch><input type=hidden name=cmd2 value="print">
Reprint Dvla report printed on 
<input type=text name=printeddate>
<a href="javascript:NewCal('printeddate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>
<input type=submit value=submit name=sumbit>
</form>

<%
else
	
		sql="select * from batchnumbers where " 
		sqlwhere=sqlwhere & "mydate >="
	datefrom=request.form("printeddate")
	arrdatefrom=split(datefrom,"/")
	myday=arrdatefrom(0)
	mymonth=arrdatefrom(1)
	myear=arrdatefrom(2)
'	sqlwhere=sqlwhere & "dateserial(" & myear & "," & mymonth & "," & myday & ")"
'sqlwhere=sqlwhere & "dateadd(year, " & myear & " - 1900, dateadd(month, " & mymonth & ", dateadd(day, " & myday & ", -32)))"
sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & "' as datetime)"
	sqlwhere=sqlwhere & " and mydate<="
	dateto=request("printeddate")
	arrdateto=split(dateto,"/")
	myday=arrdateto(0)
	mymonth=arrdateto(1)
	myear=arrdateto(2)
	
	sqlwhere=sqlwhere  & "cast('" & myear & mymonth & myday & " 23:59' as datetime)"
'	response.write "<hr>" & sqlwhere & "<hr>"
	
	sql=sql&sqlwhere 
	'sql=sql & " group by dvlarep.batch"
'	response.write sql
	 Set rs = Server.CreateObject("ADODB.Recordset")
   		rs.Open sql, objconn
		if rs.eof and rs.bof then 
			response.write "No report printed on this date"
		else
		response.write "<form method=post action=admin.asp?cmd=dvlasend><input type=hidden name=cmd2 value=print>"
		response.write "Please choose the time the report was printed:<br><select name=printedbatch>"
		do while not rs.eof
			response.write "<option value='" & rs("batchnumber") & "'>" & rs("mydate") & "</option>"
		rs.movenext
		loop
		response.write "</select><input type=submit name=submit value=print></form>"
		end if
		rs.close 
end if
end sub
sub superadmin
sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='superadmin'"
openrs rsm2,sqlm2
if not rsm2.eof and not rsm2.bof then 

if request("cmd2")="menutable" then
	call menutable
	exit sub
end if


	sqlua="select * from useraccess"
	openrs rsua,sqlua
	'response.write "<table class=maintable2>"
	response.write "<table>"
	do while not rsua.eof
		response.write "<tr><td>"
		response.write "<table class=maintable2borderw><tr><th colspan=2>" & rsua("userid") & "-" & rsua("companyname") & "</th></tr>"
		response.write "<tr><td>Temporary Permit</td><td>" 
		 if rsua("temporarypermit")= true then response.write "True"
		 response.write "</td></tr>"
		 response.write "<tr><td>Permanent Permit</td><td>" 
		 if rsua("Permanentpermit")= true then response.write "True"
		 response.write "</td></tr>"
		  response.write "<tr><td>Site All (in permits)</td><td>" 
		 if rsua("siteall")= true then response.write "True"
		 response.write "</td></tr>"
		 response.write "<tr><td>Access to All sites</td><td>" 
		 if rsua("adminsite")= true then response.write "True"
		 response.write "</td></tr>"
		  response.write "<tr><td>Marks Court</td><td>" 
		 if rsua("markscourt")= true then response.write "True"
		 response.write "</td></tr>"
		response.write "<tr><td>Profile</td><td>"  & rsua("profile")
			 response.write "</td></tr>"
		 response.write "<tr><td>Show all Menus</td><td>" 
		 if rsua("menuall")= true then response.write "True"
		 response.write "</td></tr>"
		 response.write "<tr><td colspan=2><b>User has access to the following sites: </b><br>"
		 sqlsites="select * from usersites where userid=" & rsua("id")
		 openrs rssites,sqlsites
		 i=1
		 do while not rssites.eof
		 if i>1 then response.write ", "
		 response.write rssites("sitecode") 
		 rssites.movenext
		 i=i+1
		 loop
		closers rssites
		response.write "</td></tr>"
		
		 response.write "<tr><td colspan=2><b>User has access to the following menu items: </b><br>"
		 sqlsites="select * from usermenu where userid=" & rsua("id")
		 openrs rssites,sqlsites
		 i=1
		 do while not rssites.eof
		 if i>1 then response.write ", "
		 response.write rssites("menu")
		 rssites.movenext
		 i=i+1
		 loop
		closers rssites
		response.write "</td></tr><tr><td colspan=2 align=center><a href=""javascript:popUp('admin.asp?cmd=changeadmin&id=" & rsua("id") & "')"" class=button>Edit</a>&nbsp;<a href=admin.asp?cmd=deluser&id=" & rsua("id") & " class=button>Delete</a></td></tr>"
		response.write "</table></td></tr>"
		
	rsua.movenext
	loop
	response.write "<tr><td colspan=2 align=center><a href=""javascript:popUp('admin.asp?cmd=adduser')"" class=button>Add User</a></td></tr>"	
	closers rsua
	response.write "</table>"
end if
end sub
sub menutable
if request("save")="" then
	response.write "<form method=post action=admin.asp?cmd=superadmin&cmd2=menutable><input type=hidden name=save value=save>"
		sqlua="select * from useraccess"
	Set rsua = Server.CreateObject("ADODB.Recordset")
	rsua.open sqlua, objconn, 2

	
		Dim ausers
	'	rsua.movefirst
	'	rsua.movelast
		ausers= rsua.GetRows()
		
		
	closers rsua
	
'	response.write ubound(ausers,2)
	response.write "<table class=maintable2border>"
	response.write "<tr><td>&nbsp;</td>"
	For i = 0 to UBound(ausers, 2)
    Response.Write "<td>" & ausers(1, i) & "</td>"
	
  Next 
	response.write "</tr>"
	sqlmenu="select * from menu order by menuname"
	openrs rsmenu,sqlmenu
	do while not rsmenu.eof
		response.write "<tr><td>" & rsmenu("menuname") & "</td>"
		for i=0 to ubound(ausers,2)
			sqlckmenu="select * from usermenu where userid=" & ausers(0,i) & " and menu='" & rsmenu("menuname") & "'"
			'response.write sqlckmenu
			openrs rsckmenu,sqlckmenu
				if not rsckmenu.eof and not rsckmenu.bof then
					checked="checked"
				else
					checked=""
				end if
			closers rsckmenu	
			response.write "<td><input type=checkbox name=menu value='" & rsmenu("menuname") & "!" & ausers(0,i) & "' " & checked & "></td>"
		next
		response.write "</tr>"
	rsmenu.movenext
	loop
response.write "</table>"
	closers rsmenu

response.write "<table class=maintable2border>"
	response.write "<tr><td>&nbsp;</td>"
	For i = 0 to UBound(ausers, 2)
    Response.Write "<td>" & ausers(1, i) & "</td>"
  Next 
	response.write "</tr>"
	sqlmenu="select * from [site information] order by sitecode"
	openrs rsmenu,sqlmenu
	do while not rsmenu.eof
		response.write "<tr><td>" & rsmenu("sitecode") & "</td>"
		for i=0 to ubound(ausers,2)
			sqlckmenu="select * from usersites where userid=" & ausers(0,i) & " and sitecode='" & rsmenu("sitecode") & "'"
			'response.write sqlckmenu
			openrs rsckmenu,sqlckmenu
				if not rsckmenu.eof and not rsckmenu.bof then
					checked="checked"
				else
					checked=""
				end if
			closers rsckmenu	
			response.write "<td><input type=checkbox name=site value='" & rsmenu("sitecode") & "!" & ausers(0,i) & "' " & checked & "></td>"
		next
		response.write "</tr>"
	rsmenu.movenext
	loop
response.write "</table>"
	closers rsmenu
	
response.write "<table class=maintable2border>"
	response.write "<tr><td>&nbsp;</td>"
	For i = 0 to UBound(ausers, 2)
    Response.Write "<td>" & ausers(1, i) & "</td>"
  Next 
	response.write "</tr>"
	response.write "<tr><td>Profile</td>"
	for i=0 to ubound(ausers,2)
		sqlua="select profile from useraccess where id=" & ausers(0,i)
		openrs rsua,sqlua
		profile=rsua("profile")
		closers rsus
				response.write "<td><select name=profile!"  & ausers(0,i) & ">"
					for j=1 to 3
						if j=profile then
							response.write "<option value=" & j & " selected>" & j & "</option>"
						else
							response.write "<option value=" & j & ">" & j & "</option>"
						end if
					next
				response.write "</select></td>"
	next	
	response.write "</table>"	
	response.write "<div align=center><input type=submit name=submit value=save></div>"

else
'process
'response.write "doesn't work yet"
sql="delete from usermenu"
objconn.execute sql
menus=request.form("menu")
arrmenu=split(menus,",")
for each menu in arrmenu
arr=split(menu,"!")
menuname=arr(0)
userid=arr(1)
sql="insert into usermenu(userid,menu) values(" & userid & ",'" & trim(menuname) & "')"
'response.write sql & "<br>"
objconn.execute sql
next


sql="delete from usersites"
objconn.execute sql
menus=request.form("site")
arrmenu=split(menus,",")
for each menu in arrmenu
arr=split(menu,"!")
menuname=arr(0)
userid=arr(1)
sql="insert into usersites(userid,sitecode) values(" & userid & ",'" & trim(menuname) & "')"
'response.write sql & "<br>"
objconn.execute sql
next

sqlua="select * from useraccess"
openrs rsua,sqlua
	do while not rsua.eof
		userid=rsua("id")
		profile=request("profile" & "!" & userid)
		if profile<>rsua("profile") then
			sql="update useraccess set profile=" & profile & " where id=" & userid
		'	response.write sql
			objconn.execute sql
		end if
	rsua.movenext
	loop
closers rsua
response.write "saved"
end if
end sub
sub emailadmin
sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='superadmin'"
openrs rsm2,sqlm2
if not rsm2.eof and not rsm2.bof then 
	body=""
	sqlua="select * from useraccess"
	openrs rsua,sqlua
	sqlua="select * from useraccess"
	openrs rsua,sqlua
		Dim ausers
		ausers= rsua.GetRows()
	closers rsua
	
	
	body=body & "<table class=maintable2border>"
	body=body & "<tr><td>&nbsp;</td>"
	For i = 0 to UBound(ausers, 2)
    body=body & "<td>" & ausers(1, i) & "</td>"
  Next 
	body=body & "</tr>"
	sqlmenu="select * from menu order by menuname"
	openrs rsmenu,sqlmenu
	do while not rsmenu.eof
		body=body & "<tr><td>" & rsmenu("menuname") & "</td>"
		for i=0 to ubound(ausers,2)
			sqlckmenu="select * from usermenu where userid=" & ausers(0,i) & " and menu='" & rsmenu("menuname") & "'"
			'body=body & sqlckmenu
			openrs rsckmenu,sqlckmenu
				if not rsckmenu.eof and not rsckmenu.bof then
					checked="checked"
				else
					checked=""
				end if
			closers rsckmenu	
			body=body & "<td><input type=checkbox name=menu value='" & rsmenu("menuname") & "!" & ausers(0,i) & "' " & checked & "></td>"
		next
		body=body & "</tr>"
	rsmenu.movenext
	loop
body=body & "</table>"
	closers rsmenu

body=body & "<table class=maintable2border>"
	body=body & "<tr><td>&nbsp;</td>"
	For i = 0 to UBound(ausers, 2)
    body=body & "<td>" & ausers(1, i) & "</td>"
  Next 
	body=body & "</tr>"
	sqlmenu="select * from [site information] order by sitecode"
	openrs rsmenu,sqlmenu
	do while not rsmenu.eof
		body=body & "<tr><td>" & rsmenu("sitecode") & "</td>"
		for i=0 to ubound(ausers,2)
			sqlckmenu="select * from usersites where userid=" & ausers(0,i) & " and sitecode='" & rsmenu("sitecode") & "'"
			'body=body & sqlckmenu
			openrs rsckmenu,sqlckmenu
				if not rsckmenu.eof and not rsckmenu.bof then
					checked="checked"
				else
					checked=""
				end if
			closers rsckmenu	
			body=body & "<td><input type=checkbox name=site value='" & rsmenu("sitecode") & "!" & ausers(0,i) & "' " & checked & "></td>"
		next
		body=body & "</tr>"
	rsmenu.movenext
	loop
body=body & "</table>"
	closers rsmenu
	
body=body & "<table class=maintable2border>"
	body=body & "<tr><td>&nbsp;</td>"
	For i = 0 to UBound(ausers, 2)
    body=body & "<td>" & ausers(1, i) & "</td>"
  Next 
	body=body & "</tr>"
	body=body & "<tr><td>Profile</td>"
	for i=0 to ubound(ausers,2)
		sqlua="select profile from useraccess where id=" & ausers(0,i)
		openrs rsua,sqlua
		profile=rsua("profile")
		closers rsus
				body=body & "<td>" & profile & "</td>"
					
	next	
	body=body & "</table>"
		toemail="simon.a@cleartoneholdings.co.uk"
	'toemail="ec@awebforyou.com"
	call SendhtmlEmail ("admin@cleartone-holdings.com", toemail, "Admin report", Body ) 
	
	
end if
end sub
sub emailadmin1(id)
sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='superadmin'"
openrs rsm2,sqlm2
if not rsm2.eof and not rsm2.bof then 
	body=""
	sqlua="select * from useraccess where id=" & id
	openrs rsua,sqlua
	'body=body & "<table class=maintable2>"
		body=body & "The rights were changed for the following user with the summary below<br>"
		body=body & "<table class=maintable2borderw><tr><th colspan=2>" & rsua("userid") & "-" & rsua("companyname") & "</th></tr>"
		body=body & "<tr><td>Temporary Permit</td><td>" 
		 if rsua("temporarypermit")= true then body=body & "True"
		 body=body & "</td></tr>"
		 body=body & "<tr><td>Permanent Permit</td><td>" 
		 if rsua("Permanentpermit")= true then body=body & "True"
		 body=body & "</td></tr>"
		  body=body & "<tr><td>Site All (in permits)</td><td>" 
		 if rsua("siteall")= true then body=body & "True"
		 body=body & "</td></tr>"
		 body=body & "<tr><td>Access to All sites</td><td>" 
		 if rsua("adminsite")= true then body=body & "True"
		 body=body & "</td></tr>"
		body=body & "<tr><td>Profile</td><td>"  & rsua("profile")
			 body=body & "</td></tr>"
		 body=body & "<tr><td>Show all Menus</td><td>" 
		 if rsua("menuall")= true then body=body & "True"
		 body=body & "</td></tr>"
		 body=body & "<tr><td colspan=2><b>User has access to the following sites: </b><br>"
		 sqlsites="select * from usersites where userid=" & rsua("id")
		 openrs rssites,sqlsites
		 i=1
		 do while not rssites.eof
		 if i>1 then body=body & ", "
		 body=body & rssites("sitecode") 
		 rssites.movenext
		 i=i+1
		 loop
		closers rssites
		body=body & "</td></tr>"
		
		 body=body & "<tr><td colspan=2><b>User has access to the following menu items: </b><br>"
		 sqlsites="select * from usermenu where userid=" & rsua("id")
		 openrs rssites,sqlsites
		 i=1
		 do while not rssites.eof
		 if i>1 then body=body & ", "
		 body=body & rssites("menu")
		 rssites.movenext
		 i=i+1
		 loop
		closers rssites
		body=body & "</td></tr>"
		body=body & "</table>"
		
	toemail="simon.a@cleartoneholdings.co.uk"
	'toemail="ec@awebforyou.com"
	call SendhtmlEmail ("admin@cleartone-holdings.com", toemail, "Admin change", Body ) 
	
	
end if
end sub

sub changeadmin
sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='superadmin'"
openrs rsm2,sqlm2
if not rsm2.eof and not rsm2.bof then 
if request("cmd2")<>"save" then
	sqlua="select * from useraccess where id=" & request("id")
	openrs rsua,sqlua
	%>
	<form method=post action=admin.asp?cmd=changeadmin>
	<input type=hidden name=cmd2 value="save">
	<input type=hidden name=id value="<%= request("id") %>">
	<table class=maintable2wsm>
	<tr><th colspan=2><%= rsua("userid") %></th></tr>
	<tr><td>Password</td><td><input type=text name=password value="<%= rsua("password") %>"></td></tr>
	<tr><td>Company Name</td><td><input type=text name=companyname value="<%= rsua("companyname") %>"></td></tr>
	<Tr><Td>Temporary Permit</Td><td><input type=checkbox value=yes name=temporarypermit <% if rsua("temporarypermit")=true then response.write "checked" %>></td></Tr>
	<Tr><Td>Permanent Permit</Td><td><input type=checkbox value=yes name=permanentpermit <% if rsua("permanentpermit")=true then response.write "checked" %>></td></Tr>
	<Tr><Td>Site All (in permits)</Td><td><input type=checkbox value=yes name=siteall <% if rsua("siteall")=true then response.write "checked" %>></td></Tr>
		<Tr><Td>MarksCourt</Td><td><input type=checkbox value=yes name=markscourt <% if rsua("markscourt")=true then response.write "checked" %>></td></Tr>
	<Tr><Td>Access to all sites</Td><td><input type=checkbox value=yes name=adminsite <% if rsua("adminsite")=true then response.write "checked" %>></td></Tr>
	<Tr><Td>All Menus</Td><td><input type=checkbox value=yes name=menuall <% if rsua("menuall")=true then response.write "checked" %>></td></Tr>
	<tr><td>Profile</td><td><input type=radio name=profile value=1 <% if rsua("profile")=1 then response.write "checked" %>>1&nbsp; <input type=radio name=profile value=2 <% if rsua("profile")=2 then response.write "checked" %>>2 <input type=radio name=profile value=3 <% if rsua("profile")=3 then response.write "checked" %>>3</td></tr>
	<tr><td valign=top>User Sites</td><td>
		<%
		sql="select [sitecode] from [site information]"
		openrs rs,sql
		do while not rs.eof
			response.write " &nbsp;<input type=checkbox name=usersites value=" & trim(rs("sitecode")) 
			sqlus="select  * from usersites where sitecode='" & rs("sitecode") & "' and userid=" & rsua("id")
			'response.write sqlus	
			openrs rsus,sqlus
			if not rsus.eof and not rsus.bof then response.write " checked "
			closers rsus
			response.write ">" & rs("sitecode") 
					
		rs.movenext
		loop
		closers rs
	%>
	</td></tr>
	<tr><td valign=top>User Menu</td><td>
		<%
		sql="select menuname from menu"
		openrs rs,sql
		do while not rs.eof
			response.write " &nbsp;<input type=checkbox name=usermenu value=" & trim(rs("menuname")) 
			sqlus="select  * from usermenu where menu='" & rs("menuname") & "' and userid=" & rsua("id")
			'response.write sqlus	
			openrs rsus,sqlus
			if not rsus.eof and not rsus.bof then response.write " checked "
			closers rsus
			response.write ">" & rs("menuname") 
					
		rs.movenext
		loop
		closers rs
	%>
	</td></tr>
	<tr><td colspan=2 align=center><input type=submit name=submit value="save"></td></tr>
	</table>
	
	</form>
	<%
	closers rsus	
else
	id=request("id")
	password=request("password")
	companyname=request("companyname")
	if request("temporarypermit")="yes" then 
		temporarypermit="1"
	else
		temporarypermit="0"
	end if
	if request("permanentpermit")="yes" then 
		permanentpermit="1"
	else
		permanentpermit="0"
	end if
	if request("siteall")="yes" then 
		siteall="1"
	else
		siteall="0"
	end if
	if request("markscourt")="yes" then 
		markscourt="1"
	else
		markscourt="0"
	end if
	if request("adminsite")="yes" then 
		adminsite="1"
	else
		adminsite="0"
	end if
	if request("menuall")="yes" then 
		menuall="1"
	else
		menuall="0"
	end if
	profile=request("profile")
	if profile="" then profile="Null"
	t="text"
	sql="update useraccess set [password]=" & tosql(password,t) & ",companyname=" & tosql(companyname,t) & ",temporarypermit=" &temporarypermit & ",permanentpermit=" & permanentpermit & ",siteall=" & siteall & ",profile=" & profile & ",markscourt=" & markscourt &  ",adminsite=" & adminsite & ",menuall=" & menuall &  " where id=" & id
	response.write sql
	objconn.execute sql
	sql="delete from usersites where userid=" & id
	'response.write sql
	objconn.execute sql
	strusersites=request("usersites")
	arrusersites=split(strusersites,",")
	for each site in arrusersites
		sql="insert into usersites(userid,sitecode) values(" & id & ",'" & trim(site) & "')"
		objconn.execute sql
	next
	
	sql="delete from usermenu where userid=" & id
	'response.write sql
	objconn.execute sql
	strusermenu=request("usermenu")
	arrusermenu=split(strusermenu,",")
	for each menu in arrusermenu
		sql="insert into usermenu(userid,menu) values(" & id & ",'" & trim(menu) & "')"
		objconn.execute sql
	next
	call emailadmin1(id)
	call emailadmin
%>
done
<script language="JavaScript" type="text/javascript">
  <!--
   opener.location.reload(true);
    self.close();
  // -->
</script>
	
<%
end if
end if
end sub
sub adduser
sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='superadmin'"
openrs rsm2,sqlm2
if not rsm2.eof and not rsm2.bof then 
if request("cmd2")<>"save" then
	%>
	<form method=post action=admin.asp?cmd=adduser>
	<input type=hidden name=cmd2 value="save">
	<table class=maintable2wsm>
	<tr><td>Username</td><td><input type=text name=username value=""></td></tr>
	<tr><td>Password</td><td><input type=text name=password value=""></td></tr>
	<tr><td>Company Name</td><td><input type=text name=companyname value=""></td></tr>
	<Tr><Td>Temporary Permit</Td><td><input type=checkbox value=yes name=temporarypermit></td></Tr>
	<Tr><Td>Marks Court</Td><td><input type=checkbox value=yes name=markscourt></td></Tr>
	<Tr><Td>Permanent Permit</Td><td><input type=checkbox value=yes name=permanentpermit ></td></Tr>
	<Tr><Td>Site All (in permits)</Td><td><input type=checkbox value=yes name=siteall></td></Tr>
	<Tr><Td>Access to all sites</Td><td><input type=checkbox value=yes name=adminsite></td></Tr>
	<Tr><Td>All Menus</Td><td><input type=checkbox value=yes name=menuall ></td></Tr>
	<tr><td>Profile</td><td><input type=radio name=profile value=1>1&nbsp; <input type=radio name=profile value=2>2 <input type=radio name=profile value=3>3</td></tr>
	<tr><td valign=top>User Sites</td><td>
		<%
		sql="select [sitecode] from [site information]"
		openrs rs,sql
		do while not rs.eof
			response.write " &nbsp;<input type=checkbox name=usersites value=" & trim(rs("sitecode")) 
			response.write "><label for usersites>" & rs("sitecode") & "</label>"
					
		rs.movenext
		loop
		closers rs
	%>
	</td></tr>
	<tr><td valign=top>User Menu</td><td>
		<%
		sql="select menuname from menu"
		openrs rs,sql
		do while not rs.eof
			response.write " &nbsp;<input type=checkbox name=usermenu value=" & trim(rs("menuname")) 
			response.write ">" & rs("menuname") 
					
		rs.movenext
		loop
		closers rs
	%>
	</td></tr>
	<tr><td colspan=2 align=center><input type=submit name=submit value="save"></td></tr>
	</table>
	
	</form>
	<%
	closers rsus	
else
	username=request("username")
	password=request("password")
if request("username")="" or request("password")="" then
response.write "You must enter in a username and password"
response.end

end if
	companyname=request("companyname")
	if request("temporarypermit")="yes" then 
		temporarypermit="1"
	else
		temporarypermit="0"
	end if
	if request("permanentpermit")="yes" then 
		permanentpermit="1"
	else
		permanentpermit="0"
	end if
	if request("siteall")="yes" then 
		siteall="1"
	else
		siteall="0"
	end if
	if request("markscourt")="yes" then 
		markscourt="1"
	else
		markscourt="0"
	end if
	if request("adminsite")="yes" then 
		adminsite="1"
	else
		adminsite="0"
	end if
	if request("menuall")="yes" then 
		menuall="1"
	else
		menuall="0"
	end if
	profile=request("profile")
	if profile="" then profile="Null"
	t="text"
		sql="SET NOCOUNT ON insert into useraccess (userid,[password],companyname,temporarypermit,permanentpermit,markscourt,siteall,profile,adminsite,menuall) values(" & tosql(username,"text") & "," & tosql(password,"text") & "," & tosql(companyname,"text") & "," & temporarypermit & "," & permanentpermit & "," & markscourt & "," & siteall & "," & profile &"," & adminsite & "," & menuall & ");"&_
        "SELECT @@IDENTITY AS NewID;"
	'response.write sql
	set rs=objconn.execute (sql)
	id=rs("NewID")
	response.write id
	sql="delete from usersites where userid=" & id
	response.write sql
	objconn.execute sql
	strusersites=request("usersites")
	if strusersites<>"" then
	arrusersites=split(strusersites,",")
	for each site in arrusersites
		sql="insert into usersites(userid,sitecode) values(" & id & ",'" & trim(site) & "')"
		objconn.execute sql
	next
	end if
	sql="delete from usermenu where userid=" & id
	'response.write sql
	objconn.execute sql
	strusermenu=request("usermenu")
	if strusermenu<>"" then
	arrusermenu=split(strusermenu,",")
	for each menu in arrusermenu
		sql="insert into usermenu(userid,menu) values(" & id & ",'" & trim(menu) & "')"
		objconn.execute sql
	next
	end if
	call emailadmin1(id)
	call emailadmin
%>
done
<script language="JavaScript" type="text/javascript">
  <!--
   opener.location.reload(true);
    self.close();
  // -->
</script>
	
<%
end if
end if
end sub
sub deluser

sql="delete from usersites where userid=" & request("id")
'response.write sql
objconn.execute sql
sql="delete from usermenu where userid=" & request("id")
objconn.execute sql
sql="delete from useraccess where id=" & request("id")
objconn.execute sql
response.write "User Deleted"
call superadmin
end sub
sub outstandingdvla
cmd2=request("cmd2")
select case cmd2
case ""
	sql="SELECT dvlarep.* FROM dvlarep where dvlarec is null "
	



sqlc1=sqlc & " and  dategenerated < getdate()-7" 
	sql1=sql & " and  dategenerated < getdate()-7"
	sqlc2=sqlc & " and  dategenerated < getdate()-14" 
	sql2=sql & " and  dategenerated < getdate()-14"
	sqlc3=sqlc & " and  dategenerated < getdate()-21" 
	sql3=sql & " and  dategenerated < getdate()-21"
	sqlc4=sqlc & " and  dategenerated < getdate()-28" 
	sql4=sql & " and  dategenerated < getdate()-28"
	


		'response.write sql1
	Response.write "<form method=post action=admin.asp?cmd=outstandingdvla&cmd2=showlist>Older than 1-week&nbsp;&nbsp;"
	response.write "<input type=hidden name=sql value=""" & sql1 & """><input type=submit class=button value=View></form>" & "<br>"
	
Response.write "<form method=post action=admin.asp?cmd=outstandingdvla&cmd2=showlist>Older than 2-weeks&nbsp;&nbsp;"
	response.write "<input type=hidden name=sql value=""" & sql2 & """><input type=submit class=button value=View></form>" & "<br>"
	
Response.write "<form method=post action=admin.asp?cmd=outstandingdvla&cmd2=showlist>Older than 3-weeks&nbsp;&nbsp;"
	response.write "<input type=hidden name=sql value=""" & sql3 & """><input type=submit class=button value=View></form>" & "<br>"
	
Response.write "<form method=post action=admin.asp?cmd=outstandingdvla&cmd2=showlist>Older than 4-weeks&nbsp;&nbsp;"
	response.write "<input type=hidden name=sql value=""" & sql4 & """><input type=submit class=button value=View></form>" & "<br>"
	

case "showlist"
'response.write sql
	sql= request("sql")
	if sql="" then sql=session("sql")
	
	'response.write sql
	norecords=0
	openrs rs,sql
	if rs.eof and rs.bof then norecords=1
	closers rs
	if norecords=0 then
createdvla2SortableList objConn,sql,"dvla",25,"class=maintable2w","admin.asp?cmd=outstandingdvla&cmd2=showlist"
outputexcel(Sql)
else
		response.write "No records"
end if


end select
end sub
Sub createdvla2SortableList(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
	'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
	
	'response.write "strsort:" & strsort	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
  '  response.write "xx" & strsort & "xx"			
    		Set RS = server.CreateObject("adodb.recordset")
strsql= strsql &  " order by " & replace(strSort,"desc"," desc")
' response.write strsql & " order by " & replace(strSort,"desc"," desc")
    		With RS
    			.CursorLocation=3
    			.Open strSQL, objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"ID" and field.name<>"id" then
				Response.Write "<Th align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</TD>"	& vbcrlf
    			end if
	
	Next
					
    		Response.Write "</TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
    			For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"ID" and field.name<>"id" then
					Response.Write "<TD align=center>" & vbcrlf
    				 Response.Write field.value
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
				
					
				'response.write "<td><a target=_new href=admin.asp?cmd=viewrecord&id=" & rs("ID") & " class=button>View</a></td>"
				'end if
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "<TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing			
    	End Sub
	sub siteadmin
	cmd2=request("cmd2")
	select case cmd2
	case ""
	response.write "<form method=post action=admin.asp?cmd=siteadmin><input type=hidden name=cmd2 value=general>Choose site to see the details:"
	sqlsite="select sitecode,[name of site] from [site information]"
	openrs rssite,sqlsite
	response.write buildCombo(rssite,"site","<option value=''>-Select--</option>",0,"1","")	
	closers rssite
	response.write "<br><input type=submit namesubmit value='View Details' class=button></form>"
	case "general"
	site=request("site")
	if site="" then 
		 response.write "you must choose a site"
		 exit sub
end if
response.write "<a href=admin.asp?cmd=siteadmin&cmd2=addsite class=button>Add Site</a>"
	call siteadminmenu(site)
	sql="select * from [site information] where sitecode='" & site & "'"
	openrs rssite,sql
%>	
<form method=post action=admin.asp?cmd=siteadmin>
<input type=hidden name=cmd2 value="generalsave">
<input type=hidden name=id value="<%= rssite("id")%>">
<table>
<tr><td>Name of Site</td><td><input type=text name=nameofsite value="<%= rssite("name of site") %>"></td></tr>
<tr><td>Site Code</td><td><input type=text name=sitecode value="<%= rssite("sitecode") %>"></td></tr>
<tr><td>Site Ref </td><td><input type=text name=siteref value="<%= rssite("siteref") %>"</td></tr>
<tr><td>Address</td><td><input type=text name=address1 value="<%= rssite("address1") %>"></td></tr>
<tr><td>Address 2</td><td><input type=text name=address2 value="<%= rssite("address2") %>"></td></tr>
<tr><td>Address 3</td><td><input type=text name=address3 value="<%= rssite("address3") %>"></td></tr>
<tr><td>Town</td><td><input type=text name=town value="<%= rssite("town") %>"></td></tr>
<tr><td>Postcode</td><td><input type=text name=postcode value="<%= rssite("postcode") %>"></td></tr>
<tr><td>Telephone</td><td><input type=text name=telephone value="<%= rssite("telephone") %>"></td></tr>
<tr><td>Fax</td><td><input type=text name=fax value="<%= rssite("fax") %>"></td></tr>
<tr><td>Email</td><td><input type=text name=email value="<%= rssite("email") %>"></td></tr>
<tr><td>Store Manager</td><td><input type=text  name=storemanager value="<%= rssite("storemanager") %>"></td></tr>
<tr><td>Contact Number</td><td><input type=text name=contactnumber value="<%= rssite("contactnumber") %>"></td></tr>
<tr><td>Contact Email</td><td><input type=text name=contactemail value="<%= rssite("contactemail") %>"></td></tr>
<tr><td colspan=2 align=center><input type=submit name=submit value=save class=button></form>
	<%
	closers rssite
case "generalsave"
		 t="text"
		 sql="update [site information] set sitecode=" & tosql(request.form("sitecode"),t)  & ",[name of site]="  & tosql(request.form("nameofsite"),t) & ",address1=" &tosql(request.form("address1"),t) & ",address2=" & tosql(request.form("address2"),t)  & ",address3=" & tosql(request.form("address3"),t) & ",town=" & tosql(request.form("town"),t) & ",postcode=" & tosql(request.form("postcode"),t) & ",siteref=" & tosql(request.form("siteref"),t) & ",telephone=" & tosql(request.form("telephone"),t) & ",fax=" & tosql(request.form("fax"),t)& ",email=" & tosql(request.form("email"),t) & ",storemanager=" & tosql(request.form("storemanager"),t) & ",contactnumber=" & tosql(request.form("contactnumber"),t) & ",contactemail=" & tosql(request.form("contactemail"),t) & " where id=" & request("id") 
		 objconn.execute sql
		 response.redirect "admin.asp?cmd=siteadmin&cmd2=general&site=" & request.form("sitecode") 
case "adsl"
	site=request("site")
	call siteadminmenu(site)
	sql="select * from [site information] where sitecode='" & site & "'"
	openrs rssite,sql
%>	
<form method=post action=admin.asp?cmd=siteadmin>
<input type=hidden name=cmd2 value="adslsave">
<input type=hidden name=id value="<%= rssite("id")%>">
<input type=hidden name=site value=<%= site %>>
<table>
<tr><td>ADSL Telephone</td><td><input type=text name=adsltelephone value="<%= rssite("adsltelephone") %>"></td></tr>
<tr><td>ADSL IP Address</td><td><input type=text name=adslip value="<%= rssite("adslip") %>"></td></tr>
<tr><td>ADSL Username</td><td><input type=text name=adslusername value="<%= rssite("adslusername") %>"></td></tr>
<tr><td>ADSL Password</td><td><input type=text name=adslpassword value="<%= rssite("adslpassword") %>"></td></tr>
<tr><td>Line Provided by Client </td><td><input type=checkbox name=adsllinebyclient value="1" <%if rssite("adsllinebyclient")=true then response.write "checked"%>></td></tr>
<tr><td colspan=2 align=center><input type=submit name=submit value=save class=button></form>
	<%
	closers rssite
case "adslsave"
		 t="text"
		 sql="update [site information] set adsltelephone=" & tosql(request.form("adsltelephone"),t) & ",[adslip]="  & tosql(request.form("adslip"),t) & ",adslusername=" &tosql(request.form("adslusername"),t) & ",adslpassword=" & tosql(request.form("adslpassword"),t)  & ",adsllinebyclient=" & tosql(request.form("adsllinebyclient"),"Number")  & " where id=" & request("id") 
		 objconn.execute sql
		 
		 response.redirect "admin.asp?cmd=siteadmin&cmd2=adsl&site=" & request.form("site")         
case "engineers"
call siteadminmenu(request("site"))
		 call engineers
case "tariff"
	site=request("site")
	call siteadminmenu(site)
	sql="select * from tariff where site='" & site & "'"
	openrs rssite,sql
%>	
<form method=post action=admin.asp?cmd=siteadmin>
<input type=hidden name=cmd2 value="tariffsave">
<input type=hidden name=site value=<%= site %>>
<table>
<tr><td>Ticket Price</td><td><input type=text name=ticketprice value="<%= rssite("ticketprice") %>"></td></tr>
<tr><td>Reduced Amount</td><td><input type=text name=reducedamount value="<%= rssite("reducedamount") %>"></td></tr>
<tr><td>Reduced Maximum Days</td><td><input type=text name=reducedmaximumdays value="<%= rssite("reduced maximum days") %>"></td></tr>

<tr><td colspan=2 align=center><input type=submit name=submit value=save class=button></form>
	<%
	closers rssite
case "tariffsave"
		 t="text"
		 sql="update tariff set ticketprice=" & request.form("ticketprice") & ",reducedamount="  & request.form("reducedamount") & ",[reduced maximum days]=" &request.form("reducedmaximumdays") & " where site=" & tosql(request("site"),t) 
		' response.write sql
		 objconn.execute sql
		 
		 response.redirect "admin.asp?cmd=siteadmin&cmd2=tariff&site=" & request.form("site") 
case "appealpara"
	site=request("site")
	call siteadminmenu(site)
	sql="select * from siteappealpara where site='" & site & "'"
	openrs rssite,sql
				 if rssite.eof and rssite.bof then
				 closers rssite
				 sql="select * from appealconfig"
				 openrs rssite,sql
				 end if
%>	
<form method=post action=admin.asp?cmd=siteadmin>
<input type=hidden name=cmd2 value="appealparasave">
<input type=hidden name=site value=<%= site %>>
<table>
<tr><td>Initial Paragraph</td><td><textarea name=initpara cols=50 rows=10><%= rssite("initpara") %></textarea></td></tr>
<tr><td>Final Paragraph</td><td><textarea name=finalpara cols=50 rows=10><%= rssite("finalpara") %></textarea></td></tr>
<tr><td>Final Paragraph if paid</td><td><textarea name=finalparapaid cols=50 rows=10><%= rssite("finalparapaid") %></textarea></td></tr>

<tr><td colspan=2 align=center><input type=submit name=submit value=save class=button></form>
	<%
	closers rssite
case "appealparasave"
		 t="text"
		sql="exec appealparasave @site='" & request("site") & "',@initpara=" & tosql(request("initpara"),"text") & ",@finalpara=" & tosql(request("finalpara"),"text") & ",@finalparapaid=" & tosql(request("finalparapaid"),"text")
		'response.write sql
		 objconn.execute sql
		 
		 response.redirect "admin.asp?cmd=siteadmin&cmd2=appealpara&site=" & request.form("site") 

case "addsite"
		 		if request.form("save")<>"yes" then
				%>
				<form method=post action=admin.asp?cmd=siteadmin>
<input type=hidden name=cmd2 value="addsite">
<input type=hidden name=save value="yes">
<table>
<tr><td>Name of Site</td><td><input type=text name=nameofsite value=""></td></tr>
<tr><td>Site Code</td><td><input type=text name=sitecode value=""></td></tr>
<tr><td>Site Ref </td><td><input type=text name=siteref value=""</td></tr>
<tr><td>Address</td><td><input type=text name=address1 value=""></td></tr>
<tr><td>Address 2</td><td><input type=text name=address2 value=""></td></tr>
<tr><td>Address 3</td><td><input type=text name=address3 value=""></td></tr>
<tr><td>Town</td><td><input type=text name=town value=""></td></tr>
<tr><td>Postcode</td><td><input type=text name=postcode value=""></td></tr>
<tr><td>Telephone</td><td><input type=text name=telephone value=""></td></tr>
<tr><td>Fax</td><td><input type=text name=fax value=""></td></tr>
<tr><td>Email</td><td><input type=text name=email value=""></td></tr>
<tr><td>Store Manager</td><td><input type=text  name=storemanager value=""></td></tr>
<tr><td>Contact Number</td><td><input type=text name=contactnumber value=""></td></tr>
<tr><td>Contact Email</td><td><input type=text name=contactemail value=""></td></tr>
<tr><td colspan=2 align=center><input type=submit name=submit value=save class=button></form>
				
				
				<%
				else
				t="text"
				siteref=request("siteref")
				if siteref="" then siteref=request("sitecode")
sql="insert into [site information] ([name of site],sitecode,siteref,address1,address2,address3,town,postcode,telephone,fax,email,storemanager,contactnumber,contactemail)"
sql=sql & " values(" & tosql(request("nameofsite"),t) & "," & tosql(request("sitecode"),t) & "," & tosql(siteref,t) & "," & tosql(request("address1"),t) & "," & tosql(request("address2"),t) & "," & tosql(request("address3"),t) & "," & tosql(request("town"),t) & "," & tosql(request("postcode"),t) & "," & tosql(request("telephone"),t) & "," & tosql(request("fax"),t) & "," & tosql(request("email"),t) & "," & tosql(request("storemanager"),t) & "," & tosql(request("contactnumber"),t) & "," & tosql(request("conactemail"),t) & ")"

objconn.execute sql
response.write "saved"
	end if
case "violators"
call siteadminmenu(request("site"))
	call(violatorscalendar(request("site")))
case "revenue"
call siteadminmenu(request("site"))
		 call(revenuecalendar(request("site")))
	end  select	
	end sub
	
sub siteadminmenu(site)
if site<>"" then response.write "Current Site:" & site

%>

<table align=center>
<tr>
<td>
<div id="container"> 
        <div id="menu"> 
          <div class="box"> <a href="admin.asp?cmd=siteadmin&cmd2=general&site=<%=site%>"> 
            General <span class="topleft"></span> <span class="topright"></span> 
            </a> </div>
          <div class="box"> <a href="admin.asp?cmd=siteadmin&cmd2=engineers&site=<%=site%>"> 
            Engineers <span class="topleft"></span> <span class="topright"></span> 
            </a> </div>
          <div class="box"> <a href="admin.asp?cmd=siteadmin&cmd2=adsl&site=<%=site%>"> 
            ADSL <span class="topleft"></span> <span class="topright"></span> 
            </a> </div>
          <div class="box"> <a href="admin.asp?cmd=siteadmin&cmd2=tariff&site=<%=site%>"> 
            Tariff <span class="topleft"></span> <span class="topright"></span> 
            </a> </div>
          <div class="box"> <a href="admin.asp?cmd=siteadmin&cmd2=violators&site=<%=site%>"> 
            Violators <span class="topleft"></span> <span class="topright"></span> 
            </a> </div>
          <div class="box"> <a href="admin.asp?cmd=siteadmin&cmd2=revenue&site=<%=site%>"> 
            Revenue <span class="topleft"></span> <span class="topright"></span> 
            </a> </div>
          <div class="box"> <a href="admin.asp?cmd=siteadmin&cmd2=appealpara&site=<%=site%>"> 
            Appeal <span class="topleft"></span> <span class="topright"></span> 
            </a> </div>
        </div>
      </div>

</td>
</tr>
</table>





	<%

end sub	
sub engineers
arrform=array("companyname","contactname","telephone","mobile","email","companyname2","contactname2","telephone2","mobile2","email2")
arrformcaption=array("companyname","contactname","telephone","mobile","email","companyname2","contactname2","telephone2","mobile2","email2")
arrformtype=array("t","t","t","t","t","t","t","t","t","t")

if request("cmd3")="add" then
	response.write "<form method=post action=admin.asp?cmd=siteadmin&cmd2=engineers&cmd3=savenew><table class=maintable2>"
	for i= lbound(arrform) to ubound(arrform)
	response.write "<tr><td>" & arrformcaption(i) & "</td><td>" & createinput(arrform(i),arrformtype(i)) & "</td></tr>"
	next
	response.write "<input type=hidden name=site value=" & request("site") & ">"
	response.write "<tr><td colspan=2 align=center><input type=submit value=Save></td></tr></table></form>"
end if
if request("cmd3")="savenew" then
	sql="insert into engineers (sitecode," & join(arrform,",") & ") values (" 
	sql=sql &  tosql(request("site"),"text") & ","
	for i=lbound(arrform) to ubound(arrform)
	sql=sql & tosqla(request.form(arrform(i)),arrformtype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & ")"
'response.write sql
	objconn.execute sql
	response.write "Added Successfully"
	response.redirect "admin.asp?cmd=siteadmin&cmd2=engineers&site=" & request("site")
end if
if request("cmd3")="edit" then
		response.write "<form method=post action=admin.asp?cmd=siteadmin&cmd2=engineers&cmd3=saveedit><table class=maintable2>"
		response.write "<input type=hidden name=id value=" & request("id") & ">"
			response.write "<input type=hidden name=site value=" & request("site") & ">"
		sqle="select * from engineers where id=" & request("id")
		openrs rse,sqle
		for i= lbound(arrform) to ubound(arrform)
		response.write "<tr><td>" & arrformcaption(i) & "</td><td>" & createeditinput(arrform(i),arrformtype(i),rse(arrform(i))) & "</td></tr>"
		next
		closers rse
		response.write "<tr><td colspan=2 align=center><input type=submit value=Save></td></tr></table></form>"
end if
if request("cmd3")="saveedit" then
	sql="update engineers set "
	for i=lbound(arrform) to ubound(arrform)
	sql=sql & arrform(i) & "=" & tosqla(request.form(arrform(i)),arrformtype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & " where id=" & request("id")
'	response.write sql
	objconn.execute sql
	response.write "Edited Successfully"
end if
if request("cmd3")="delete" then
		sqld="delete from engineers where id=" & request("id")
		objconn.execute sqld
		response.write "Deleted Successfully"
	end if
	
if request("cmd3")="" then
	editlink="admin.asp?cmd=siteadmin&cmd2=engineers&cmd3=edit&site=" & request("site")
	deletelink="admin.asp?cmd=siteadmin&cmd2=engineers&cmd3=delete&site=" & request("site")
	strpagename="admin.asp?cmd=siteadmin&cmd2=engineers&s=1&site=" & request("site")
	if request("s")=1 then
		strsql=session("sql")
	else
	site=request("site")
	strsql="select * from engineers where sitecode=" & tosql(site,"text")
	end if
	'response.write strsql
	openrs rst,strsql
	if rst.eof and rst.bof then 
		 response.write "No records<br><br>"
	else
		createmySortableList objConn,strSQL,"ID",15,"border=1 bgcolor='#cccccc'",editlink,deletelink,strpagename	
	end if
	closers rst
	
	response.write "<br><a href=admin.asp?cmd=siteadmin&cmd2=engineers&cmd3=add&site=" & request("site") & " class=button>Add Engineer</a>"


end if	
end sub


sub createmySortableList(objConn, strSQL, strDefaultSort, _ 
  intPageSize,  strTableAttributes,editlink,deletelink,strpagename)
session("sql")=strsql
  dim RS
  dim strSort
  dim intCurrentPage

  dim strTemp
  dim field
  dim strMoveFirst
  dim strMoveNext
  dim strMovePrevious
  dim strMoveLast
  dim i
  dim intTotalPages
  dim intCurrentRecord
  dim intTotalRecords 

  i = 0
 
  strSort = request("sort")
  intCurrentPage = request("page")
 
 
  if strSort = "" then
    strSort = strDefaultSort
  end if
 
  if intCurrentPage = "" then
    intCurrentPage = 1
  end if
 
  set RS = server.CreateObject("adodb.recordset")
 
  with RS
    .CursorLocation=3
  .Open strSQL & " order by " & replace(strSort,"desc"," desc"), _ 
       objConn,3 '3 is adOpenStatic
    .PageSize = cint(intPageSize)
    intTotalPages = .PageCount
    intCurrentRecord = .AbsolutePosition 
    .AbsolutePage = intCurrentPage
    intTotalRecords = .RecordCount
  end with

  Response.Write "<table " & strTableAttributes & " >" & vbcrlf

  'table head
  Response.Write "<tr>" & vbcrlf

  'loop through the fields in the recordset
  for each field in RS.Fields 
    Response.Write "<td align=center>" & vbcrlf
    'check the sort order, if its currently ascending, make the link descending
    if instr(strSort, "desc") then 
      Response.Write "<a href=" & strPageName & "&sort="& field.name & _
         "&page=" & intCurrentPage & ">" & field.name & "</a>" & vbcrlf
    else
      Response.Write "<a href=" & strPageName & "&sort="& field.name & _ 
        "desc&page=" & intCurrentPage & ">" & field.name & "</a>"  _
        & vbcrlf
    end if
    Response.Write "<td>" & vbcrlf
  next
  
  Response.Write "<tr>"

  'display from the current record to the pagesize
  for i = intCurrentRecord to RS.PageSize 
    if not RS.eof then 
      Response.Write "<tr>" & vbcrlf

      'for each field in the recordset
      for each field in RS.Fields 
        Response.Write "<td align=center>" & vbcrlf

        'if this field is the "linked field" provide a link
        if lcase(strLinkedColumnName) = lcase(field.name) then 
        Response.Write "<a href=" & strLink & "?sort="& strSort & _ 
            "&page=" & intCurrentPage & "&" & field.name & "=" & _ 
            field.value & " >" & field.value & "</a>" & vbcrlf
        else
          Response.Write field.value
        end if
        Response.Write "<td>" & vbcrlf
      next
	  if editlink<>"" then response.write "<td><a href=" & editlink & "&id=" & rs.fields("id") & " class=button>Edit </a></td>"
	   if deletelink<>"" then response.write "<td><a href=" & deletelink & "&id=" & rs.fields("id") & " class=button>Delete </a></td>"
      Response.Write "</tr>" & vbcrlf
      RS.MoveNext
    end if
  next

  Response.Write "<table>" & vbcrlf
    
  'page navigation
  select case cint(intCurrentPage) 
    'if its the last page give only links to movefirst and move previous
    case cint(intTotalPages) 
      strMoveFirst = "<a href=" & strPageName & "?sort="& strSort & _ 
        "&page=1 >"& "First" &"</a>"
        
      strMoveNext = ""

      strMovePrevious = "<a href=" & strPageName & "?sort="& strSort &"page=" & intCurrentPage - 1 & " >"& "Prev" &"</a>"

      strMoveLast = "" 

    'if its the first page only give links to move next and move last
    case 1 
      strMoveFirst = ""

      strMoveNext = "<a href=" & strPageName & "?sort="& strSort & _ 
        "&page=" & intCurrentPage + 1 & " >"& "Next" &"</a>"

      strMovePrevious = "" 

      strMoveLast = "<a href=" & strPageName & "?sort="& strSort &"page=" & intTotalPages & " >"& "Last" &"</a>"

    case else 
      strMoveFirst = "<a href=" & strPageName & "?sort="& strSort & "&page=1 >"& "First" &"</a>"

      strMoveNext = "<a href=" & strPageName & "?sort="& strSort &     "&page=" & intCurrentPage + 1 & " >"& "Next" &"</a>"

      strMovePrevious = "<a href=" & strPageName & "?sort="& strSort &      "&page=" & intCurrentPage - 1 & " >"& "Prev" &"</a>"

      strMoveLast = "<a href=" & strPageName & "?sort="& strSort &     "&page=" & intTotalPages & " >"& "Last" &"</a>"
  end select 
    
  with Response
    .Write strMoveFirst & " "
    .Write strMovePrevious 
    .Write " " & intCurrentPage & " of " & intTotalPages & " "
    .Write strMoveNext & " "
    .Write strMoveLast
  end with
    
  if RS.State = &H00000001 then 'its open
    RS.Close
  end if
  set RS = nothing
end sub

function createinput(fieldname,fieldtype)
myvalue=request(fieldname)
select case fieldtype
case "t"
	createinput="<input type=text name=" & fieldname & " value='"  & "'>"
case "tas"
	createinput="<textarea  name=" & fieldname & ">" &  "</textarea>"
case "ta"
	createinput="<textarea cols=50 rows=10 name=" & fieldname &  ">" & myvalue & "</textarea>"	
case "n"
	createinput="<input type=text name=" & fieldname & " value='" &  "' onKeyPress=""return checkValue(event)"">"
case "i"
	createinput="<input type=file name=" & fieldname & " value='" &  "'>"
case "b"
	createinput="<input type=checkbox name=" & fieldname & " value=1>"
case "d"
	createinput="<input type=text name=" & fieldname & " value='" &  "'><a href=""javascript:NewCal('dateto','mmddyyyy')""><img src=""cal.gif"" width=""16"" height=""16"" border=""0"" alt=""Pick a date""></a><br>(mm/dd/yyyy)"

end select

end function

function createeditinput(fieldname,fieldtype,fieldvalue)
'response.Write "<hr>" & fieldtype & "<hr>"
select case fieldtype
case "t"
	if fieldvalue<>"" then fieldvalue=replace(fieldvalue,"'","&#146")
	createeditinput="<input type=text name=" & fieldname & " value='" & fieldvalue & "'>"
case "c"
	if fieldvalue<>"" then fieldvalue=decode(fieldvalue)
	createeditinput="<input type=text name=" & fieldname & " value='" & fieldvalue & "' onKeyPress=""return checkValue(event)"">"
case "tas"
	createeditinput="<textarea  name=" & fieldname & ">" & fieldvalue & "</textarea>"
case "ta"
	createeditinput="<textarea cols=50 rows=10 name=" & fieldname & ">" & fieldvalue& "</textarea>"	
case "n"
	createeditinput="<input type=text name=" & fieldname & " value='" & fieldvalue& "' onKeyPress=""return checkValue(event)"">"
case "i"
	createeditinput="<input type=file name=" & fieldname & " value='" & fieldvalue& "'>"
case "b"
	createeditinput="<input type=checkbox name=" & fieldname & " value=1"
	if fieldvalue=true then createeditinput=createeditinput & " checked "
	createeditinput=createeditinput & " >"
case "d"
	createeditinput="<input type=text name=" & fieldname  & " id=" & fieldname & " value='" & fieldvalue & "'><a href=""javascript:NewCal('" & fieldname & "','ddmmyyyy')""><img src=""cal.gif"" width=""16"" height=""16"" border=""0"" alt=""Pick a date""></a><br>(dd/mm/yyyy)"



case "taw"
	createeditinput="<textarea cols=50 rows=10 name=" & fieldname & ">" &fieldvalue & "</textarea>"
createeditinput=createeditinput & "<script language=""javascript1.2"">" & vbcrlf
createeditinput=createeditinput & "var config = new Object();    // create new config object" & vbcrlf
createeditinput=createeditinput & "config.width = ""90%"";" & vbcrlf
createeditinput=createeditinput & "config.height = ""200px"";" & vbcrlf
createeditinput=createeditinput & "config.bodyStyle = 'background-color: white; font-family: ""Verdana""; font-size: x-small;';" & vbcrlf
createeditinput=createeditinput & "config.debug = 0;" & vbcrlf
createeditinput=createeditinput & "config.toolbar = [" & vbcrlf
 createeditinput=createeditinput & "    ['bold','italic','underline','separator']," & vbcrlf
createeditinput=createeditinput & "  ['strikethrough','subscript','superscript','separator']," & vbcrlf
createeditinput=createeditinput & "    ['justifyleft','justifycenter','justifyright','separator']," & vbcrlf
createeditinput=createeditinput &"    ['OrderedList','UnOrderedList','Outdent','Indent','separator']," & vbcrlf
 
createeditinput=createeditinput & "];" & vbcrlf
createeditinput=createeditinput & "editor_generate('" &  fieldname & "',config);" & vbcrlf
   createeditinput=createeditinput & "             </script>" & vbcrlf
end select


if left(fieldtype,1)="s" then
	l=len(fieldtype)-3
	'response.write l
	table=mid(fieldtype,3,l)
	sql="select * from " & table
	openrs rscm,sql
		createeditinput="<select name=" & fieldname  & ">"
	createeditinput=createeditinput & "<option value="""">--select--</option>"
		do while not rscm.eof
		if rscm(0)=fieldvalue then
			createeditinput=createeditinput &  "<option selected value='" & rscm(0) & "'>" & rscm(1) & "</option>"
		else
			createeditinput=createeditinput &  "<option value='" & rscm(0) & "'>" & rscm(1) & "</option>"
		end if
		rscm.movenext
		loop
		createeditinput=createeditinput & "</select>"
	closers rscm
end if
if left(fieldtype,1)="r" then
	l=len(fieldtype)-3
	'response.write l
	table=mid(fieldtype,3,l)
	sql="select * from " & table
	openrs rscm,sql
		createeditinput="<select name=" & fieldname  & ">"
	createeditinput=createeditinput & "<option value="""">--select--</option>"
		do while not rscm.eof
		if rscm(0)=fieldvalue then
			createeditinput=createeditinput &  "<option selected value='" & rscm(0) & "'>" & rscm(0) & "</option>"
		else
			createeditinput=createeditinput &  "<option value='" & rscm(0) & "'>" & rscm(0) & "</option>"
		end if
		rscm.movenext
		loop
		createeditinput=createeditinput & "</select>"
	closers rscm
end if
end function



sub violatorscalendar(site)
res=Request.QueryString("Show")
whichmonth=Request.QueryString("CurMonth")
whichyear=Request.QueryString("CurYear")
whataction=Request.QueryString("Action")
%>
<form name="Cal">
<% 

dim nextmonth
dim acalandardays(42)
nextmonth = 0

dbcurrentdate = date()

if res="Yes" then
	whichdate="01" & "/" & whichmonth & "/" & whichyear
	if whataction="Next" then
		dbcurrentdate=dateadd("m",1,cdate(whichdate))
	elseif whataction="Prev" then
		dbcurrentdate=dateadd("m",-1,cdate(whichdate))
	else
		dbcurrentdate=whichdate
	end if
end if

Dim dbcurrentdate 

dim ifirstweekday
ifirstweekday = datepart("W",dateserial(year(dbcurrentdate),month(dbcurrentdate),1))
dim idaysinmonth 
idaysinmonth = datepart("d",dateserial(year(dbcurrentdate),month(dbcurrentdate)+1,1-1))
dim iloop
for iloop = 1 to idaysinmonth
	acalandardays(iloop+ifirstweekday-1)=iloop
next

dim icolumns,irows
icolumns = 7 
irows = 6-int((42-(ifirstweekday+idaysinmonth))/7)
%>

<table align =center border=1 cellspacing = 1 width=80% height=80%>
<!--<tr>
	<select name="MonthNames">
		<option></option>
		<option>Jan</option>
		<option>Feb</option>
		<option>Mar</option>
		<option>Apr</option>
		<option>May</option>
		<option>Jun</option>
		<option>Jul</option>
		<option>Aug</option>
		<option>Sep</option>
		<option>Oct</option>
		<option>Nov</option>
		<option>Dec</option>
	</select>

	<select name="ListYears">
		<option></option>
	<% for yr=2000 to 2010 %>
		<option><%=yr%></option>
	<% next %>
	</select>

	<input type=button value="Go" onclick="SendVal()" id=button1 name=button1>
</tr>-->
<th colspan=7 bgcolor=lightblue>

<%
curmonth=monthname(month(dbcurrentdate))
Response.Write curmonth
curyear=year(dbcurrentdate)
Response.Write " " & curyear %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="admin.asp?cmd=siteadmin&cmd2=violators&Show=Yes&CurMonth=<%=left(curmonth,3)%>&CurYear=<%=curyear%>&Action=Prev&site=<%= site %>">Prev</a>&nbsp;
<a href="admin.asp?cmd=siteadmin&cmd2=violators&Show=Yes&CurMonth=<%=left(curmonth,3)%>&CurYear=<%=curyear%>&Action=Next&site=<%= site %>">Next</a>

<%
Response.Write	"<tr>"
Response.Write "<td align=center>"
Response.Write "<font color=red><b>"
Response.Write "Sun"
Response.Write "</font>"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Mon"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Tue"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Wed"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Thu"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Fri"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Sat"
Response.Write "</td>"

Response.Write	"</tr>"
%>

</font>

<% 
dim irowsloop ,icolumnsloop
startday=weekday("01" & "/" & monthname(month(dbcurrentdate)) & "/" & year(dbcurrentdate))
ctr=1

for irowsloop = 1 to irows
	Response.Write "<tr>"
	if ctr=1 then
		colm = startday
	else
		colm=1
	end if
	if startday>1 then
		diff=colm - 1
	end if
		
		for icolumnsloop=colm to icolumns
			if acalandardays((irowsloop-1)*7 +icolumnsloop) >= 0 then
				if diff>0 then
					for p=1 to diff
						Response.Write "<td valign= top align=right width=""14%"">"
						Response.Write "</td>"	
					next
					diff=0
				end if
					Response.Write "<td valign= top align=right width=""14%"">"
					if icolumnsloop=1 then
						Response.Write "<font color=blue>"
					end if
					if month(dbcurrentdate)<month(Date()) then
						if acalandardays((irowsloop-1)*7 + icolumnsloop)=day(Date()) and month(dbcurrentdate)=month(Date()) and year(dbcurrentdate)=year(Date()) then
							Response.Write "<font color=blue>"
						end if
						Response.Write acalandardays((irowsloop-1)*7 + icolumnsloop) 			
					elseif month(dbcurrentdate)=month(Date()) and acalandardays((irowsloop-1)*7 + icolumnsloop) < day(Date()) then
						Response.Write acalandardays((irowsloop-1)*7 + icolumnsloop) 			
					elseif month(dbcurrentdate)=month(Date()) and acalandardays((irowsloop-1)*7 + icolumnsloop) >= day(Date())then
						Response.Write  acalandardays((irowsloop-1)*7 + icolumnsloop) 
						
					elseif month(dbcurrentdate)>month(Date()) then
						Response.Write  acalandardays((irowsloop-1)*7 + icolumnsloop) 
					end if
					response.write "<br>"
					cmonth=month(dbcurrentdate)
					cday=acalandardays((irowsloop-1)*7 + icolumnsloop) 
					if cday<>"" then
					if len(cmonth)<2 then cmonth= "0" & cmonth
					if len(cday)<2 then cday="0" & cday
					sqlv="select count(id) as count from violators where site='" & request("site") & "' and [date of violation]>=cast('"& year(dbcurrentdate) & cmonth & cday &"' as datetime) and [date of violation]<=cast('" & year(dbcurrentdate) & cmonth & cday &" 23:59' as datetime)"
					openrs rsv,sqlv
					response.write rsv("count") & " Violators"
					closers rsv
				'	Response.Write "<br>violator info here " & year(dbcurrentdate) & cmonth & cday & "</td>"
					end if
			else
				Response.Write "<td>&nbsp;</td>"
			end if
			ctr=2
		next
		Response.Write "<tr>"
	colm=2
next
Response.Write "<br>"
%>
</table>

</form>

<%
end sub

sub revenuecalendar(site)
res=Request.QueryString("Show")
whichmonth=Request.QueryString("CurMonth")
whichyear=Request.QueryString("CurYear")
whataction=Request.QueryString("Action")
%>
<form name="Cal">
<% 

dim nextmonth
dim acalandardays(42)
nextmonth = 0

dbcurrentdate = date()

if res="Yes" then
	whichdate="01" & "/" & whichmonth & "/" & whichyear
	if whataction="Next" then
		dbcurrentdate=dateadd("m",1,cdate(whichdate))
	elseif whataction="Prev" then
		dbcurrentdate=dateadd("m",-1,cdate(whichdate))
	else
		dbcurrentdate=whichdate
	end if
end if

Dim dbcurrentdate 

dim ifirstweekday
ifirstweekday = datepart("W",dateserial(year(dbcurrentdate),month(dbcurrentdate),1))
dim idaysinmonth 
idaysinmonth = datepart("d",dateserial(year(dbcurrentdate),month(dbcurrentdate)+1,1-1))
dim iloop
for iloop = 1 to idaysinmonth
	acalandardays(iloop+ifirstweekday-1)=iloop
next

dim icolumns,irows
icolumns = 7 
irows = 6-int((42-(ifirstweekday+idaysinmonth))/7)
%>

<table align =center border=1 cellspacing = 1 width=80% height=80%>
<!--<tr>
	<select name="MonthNames">
		<option></option>
		<option>Jan</option>
		<option>Feb</option>
		<option>Mar</option>
		<option>Apr</option>
		<option>May</option>
		<option>Jun</option>
		<option>Jul</option>
		<option>Aug</option>
		<option>Sep</option>
		<option>Oct</option>
		<option>Nov</option>
		<option>Dec</option>
	</select>

	<select name="ListYears">
		<option></option>
	<% for yr=2000 to 2010 %>
		<option><%=yr%></option>
	<% next %>
	</select>

	<input type=button value="Go" onclick="SendVal()" id=button1 name=button1>
</tr>-->
<th colspan=7 bgcolor=lightblue>

<%
curmonth=monthname(month(dbcurrentdate))
Response.Write curmonth
curyear=year(dbcurrentdate)
Response.Write " " & curyear %>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="admin.asp?cmd=siteadmin&cmd2=revenue&Show=Yes&CurMonth=<%=left(curmonth,3)%>&CurYear=<%=curyear%>&Action=Prev&site=<%= site %>">Prev</a>&nbsp;
<a href="admin.asp?cmd=siteadmin&cmd2=revenue&Show=Yes&CurMonth=<%=left(curmonth,3)%>&CurYear=<%=curyear%>&Action=Next&site=<%= site %>">Next</a>

<%
Response.Write	"<tr>"
Response.Write "<td align=center>"
Response.Write "<font color=red><b>"
Response.Write "Sun"
Response.Write "</font>"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Mon"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Tue"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Wed"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Thu"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Fri"
Response.Write "</td>"
Response.Write "<td align=center><b>"
Response.Write "Sat"
Response.Write "</td>"

Response.Write	"</tr>"
%>

</font>

<% 
dim irowsloop ,icolumnsloop
startday=weekday("01" & "/" & monthname(month(dbcurrentdate)) & "/" & year(dbcurrentdate))
ctr=1

for irowsloop = 1 to irows
	Response.Write "<tr>"
	if ctr=1 then
		colm = startday
	else
		colm=1
	end if
	if startday>1 then
		diff=colm - 1
	end if
		
		for icolumnsloop=colm to icolumns
			if acalandardays((irowsloop-1)*7 +icolumnsloop) >= 0 then
				if diff>0 then
					for p=1 to diff
						Response.Write "<td valign= top align=right width=""14%"">"
						Response.Write "</td>"	
					next
					diff=0
				end if
					Response.Write "<td valign= top align=right width=""14%"">"
					if icolumnsloop=1 then
						Response.Write "<font color=blue>"
					end if
					if month(dbcurrentdate)<month(Date()) then
						if acalandardays((irowsloop-1)*7 + icolumnsloop)=day(Date()) and month(dbcurrentdate)=month(Date()) and year(dbcurrentdate)=year(Date()) then
							Response.Write "<font color=blue>"
						end if
						Response.Write acalandardays((irowsloop-1)*7 + icolumnsloop) 			
					elseif month(dbcurrentdate)=month(Date()) and acalandardays((irowsloop-1)*7 + icolumnsloop) < day(Date()) then
						Response.Write acalandardays((irowsloop-1)*7 + icolumnsloop) 			
					elseif month(dbcurrentdate)=month(Date()) and acalandardays((irowsloop-1)*7 + icolumnsloop) >= day(Date())then
						Response.Write  acalandardays((irowsloop-1)*7 + icolumnsloop) 
						
					elseif month(dbcurrentdate)>month(Date()) then
						Response.Write  acalandardays((irowsloop-1)*7 + icolumnsloop) 
					end if
					response.write "<br>"
					cmonth=month(dbcurrentdate)
					cday=acalandardays((irowsloop-1)*7 + icolumnsloop) 
					if cday<>"" then
					if len(cmonth)<2 then cmonth= "0" & cmonth
					if len(cday)<2 then cday="0" & cday
					sqlv="select count(id) as count from payments where  right(pcn,3)='" & request("site") & "' and [received]>=cast('"& year(dbcurrentdate) & cmonth & cday &"' as datetime) and [received]<=cast('" & year(dbcurrentdate) & cmonth & cday &" 23:59' as datetime)"
					openrs rsv,sqlv
					response.write rsv("count") & " Payments Recieved"
					closers rsv
					response.write "<br><br>"
					sqlv="SELECT count(payments.id) as count FROM VIOLATORS RIGHT JOIN PAYMENTS ON (VIOLATORS.PCN+violators.site) = PAYMENTS.PCN where site='001' and [date of violation]>=cast('"& year(dbcurrentdate) & cmonth & cday &"' as datetime) and [date of violation]<=cast('" & year(dbcurrentdate) & cmonth & cday &" 23:59' as datetime)"
					openrs rsv,sqlv
					response.write rsv("count") & " Payments Recieved for violations sent on this date"
					closers rsv
					
					end if
			else
				Response.Write "<td>&nbsp;</td>"
			end if
			ctr=2
		next
		Response.Write "<tr>"
	colm=2
next
Response.Write "<br>"
%>
</table>

</form>

<%
end sub
sub siteoverview
if request("cmd2")="" then
%>
<table class=maintable2 width=500>
<tr>
<td>
<form method=post action=admin.asp?cmd=siteoverview>
	 From </td><td><input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To </td><td><input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)

</td></tr>
<tr><td>
Date</td><td> <select name=datetype><option value="offencedate">of Offence</option>
<option value="ticketissued">Ticket Issued</option></select></td></tr>
<input type=hidden name=cmd2 value="show">
<tr><td colspan=2 align=center>
<input type=submit name=submit value="Submit" class=button>
</form>
</td>
</tr>
</table>
<%
else

	

datetype=request("datetype")
if datetype="offencedate" then
	 fielddate="[date of violation]"
else
		fielddate="pcnsent"
end if
datefrom=request.form("datefrom")
arrdatefrom=split(datefrom,"/")
myday=arrdatefrom(0)
if len(myday)<2 then myday="0" & myday
mymonth=arrdatefrom(1)
if len(mymonth)<2 then mymonth="0" & mymonth
myear=arrdatefrom(2)
sqlfrom=  "cast('" & myear & mymonth& myday &"' as datetime)"
dateto=request("dateto")
arrdateto=split(dateto,"/")
myday=arrdateto(0)
mymonth=arrdateto(1)
myear=arrdateto(2)
if len(myday)<2 then myday="0" & myday
if len(mymonth)<2 then mymonth="0" & mymonth
sqlto= "cast('" & myear & mymonth& myday &" 23:59' as datetime)"
mytable= "<table  class=maintable2border cellpadding=10><tr><tr><td>Site</td><td>Tickets</td><td>% paid before reminder</td><td>% cancelled</td><td>Reminders</td><td>%paid after Reminder</td><td>%Cancelled after Reminder</td> </tr>"
sqlsite="select * from [site information]"
openrs rssite,sqlsite
i=0
do while not rssite.eof		
	mytable=mytable &  "<tr>"
	if i mod 2=0 then 
						mytable=mytable & "<TR class=rowcolor2>" & vbcrlf
					else
							mytable=mytable & "<TR>" & vbcrlf
					end if
	mytable=mytable & "<td>" & rssite("sitecode") & "</td>"
	sql1="select count(id) as mycount from violators where " & fielddate & " >=" & sqlfrom & " and " & fielddate &  "<=" & sqlto & " and site='" & rssite("sitecode") & "'"
'	response.write sql1
	openrs rs1,sql1
	tviolators=rs1("mycount")
	closers rs1
	mytable=mytable & "<td>" & tviolators& "</td>"
	sql2="SELECT count(violators.id)as mycount FROM (Reminders RIGHT JOIN VIOLATORS ON Reminders.PCN = VIOLATORS.PCN) LEFT JOIN vwpayments ON VIOLATORS.PCN = vwpayments.PCN7 WHERE (Reminders.[REMINDER] Is Null AND [vwpayments].AMOUNT Is Null) and 	violators." & fielddate & " >=" & sqlfrom & " and violators." & fielddate &  "<=" & sqlto & " and violators.site='" & rssite("sitecode") & "'"
	'response.write sql2
	openrs rs2,sql2
	count2=rs2("mycount")
	if tviolators>0 then
		 mytable=mytable & "<td>" & 	formatnumber((count2/tviolators)*100,2) & "%</td>"
	 else
	mytable=mytable & "<td>&nbsp;</td>"		
	 end if
	sql3="SELECT count(violators.id) as mycount FROM VIOLATORS LEFT JOIN vwcancelled ON VIOLATORS.PCN = vwcancelled.PCN7 WHERE vwcancelled.cancelled is not null and 	violators." & fielddate & " >=" & sqlfrom & " and violators." & fielddate &  "<=" & sqlto & " and violators.site='" & rssite("sitecode") & "'"
	'response.write sql2
	openrs rs3,sql3
	count3=rs3("mycount")
	if tviolators>0 then
	mytable=mytable & "<td>" & 	formatnumber((count3/tviolators)*100,2) & "%</td>"
	else
	 mytable=mytable & "<td>&nbsp;</td>"		
	 end if
	sql4="SELECT count(violators.id) as mycount FROM VIOLATORS LEFT JOIN Reminders ON VIOLATORS.PCN = Reminders.PCN WHERE Reminders.REMINDER Is Not Null and	violators." & fielddate & " >=" & sqlfrom & " and violators." & fielddate &  "<=" & sqlto & " and violators.site='" & rssite("sitecode") & "'"
	'response.write sql2
	openrs rs4,sql4
	count4=rs4("mycount")
	mytable=mytable & "<td>" & 	count4 & "</td>"
	
	sql5="SELECT count(violators.id) as mycount FROM (VIOLATORS LEFT JOIN Reminders ON VIOLATORS.PCN = Reminders.PCN) LEFT JOIN vwpayments ON VIOLATORS.PCN = vwpayments.PCN7 WHERE (((vwpayments.Received)>[REMINDER])) and 	violators." & fielddate & " >=" & sqlfrom & " and violators." & fielddate &  "<=" & sqlto & " and violators.site='" & rssite("sitecode") & "'"
	'response.write sql5
	openrs rs5,sql5
	count5=rs5("mycount")
	if tviolators>0 then
	mytable=mytable &"<td>" & 	formatnumber((count5/tviolators)*100,2) & "%</td>"
	else
	 mytable=mytable & "<td>&nbsp;</td>"		
	 end if
	
	sql6=" SELECT count(violators.id) as mycount FROM vwcancelled RIGHT JOIN (VIOLATORS LEFT JOIN Reminders ON VIOLATORS.PCN = Reminders.PCN) ON vwcancelled.PCN7 = VIOLATORS.PCN WHERE (((vwcancelled.Cancelled)>[REMINDER])) and 	violators." & fielddate & " >=" & sqlfrom & " and violators." & fielddate &  "<=" & sqlto & " and violators.site='" & rssite("sitecode") & "'"
	'response.write sql5
	openrs rs6,sql6
	count6=rs6("mycount")
	if tviolators>0 then
	mytable=mytable &"<td>" & 	formatnumber((count6/tviolators)*100,2) & "%</td>"
	else
	mytable=mytable &"<td>&nbsp;</td>"		
	 end if
	
	mytable=mytable & "</tr>"
	i=i+1
rssite.movenext
loop		
    response.Write mytable
     
end if
end sub
sub uploadanalysis
if request("cmd2")="" then
response.write "Place file in " & configsecuredir & "db\files\ directory and type filename below"
%>

<form method=post action=admin.asp?cmd=uploadanalysis&cmd2=process>
<input type=text name=sfile>
<input type=submit name=submit value=Process>
</form>
<%
else
'response.write "need to process file " & request("sfile")
uplstrcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & "" & configsecuredir & "db\files\" & request("sfile")
set uplconnection=server.createobject("ADODB.connection")
uplconnection.connectionstring=uplstrcon
uplconnection.open
t="text"

'move umatched
sqlunmatched="select * from unmatched"
set rsunmatched=uplconnection.execute(sqlunmatched)
do while not rsunmatched.eof
sqli="insert into anunmatched(Plate,D,T,Site,Notes,Direction) values (" & tosqlnn(rsunmatched("Plate"),t) & "," & tosqlnn(rsunmatched("D"),t)  & "," & tosqlnn(rsunmatched("T"),t) & "," & tosqlnn(rsunmatched("Site"),t) & "," & tosqlnn(rsunmatched("Notes"),t) & "," & tosqlnn(rsunmatched("Direction"),t) & ")"
'response.write sqli & "<br>"
objconn.execute sqli
rsunmatched.movenext
loop
rsunmatched.close

'move nonviolators
sqlunmatched="select * from [Non Violations]"
set rsunmatched=uplconnection.execute(sqlunmatched)
do while not rsunmatched.eof
sqli="insert into annonviolations(Plate,D,T_In,T_Out,Duration,Site,Notes,Direction) values (" & tosqlnn(rsunmatched("Plate"),t) & "," & tosqlnn(rsunmatched("D"),t)  & "," & tosqlnn(rsunmatched("T_in"),t) & "," & tosqlnn(rsunmatched("T_out"),t) & "," & tosqlnn(rsunmatched("duration"),t)& "," & tosqlnn(rsunmatched("Site"),t) & "," & tosqlnn(rsunmatched("Notes"),t) & "," & tosqlnn(rsunmatched("Direction"),t) & ")"
'response.write sqli & "<br>"
objconn.execute sqli
rsunmatched.movenext
loop
rsunmatched.close

'move possibleviolators
sqlunmatched="select * from [Possible Violators]"
set rsunmatched=uplconnection.execute(sqlunmatched)
do while not rsunmatched.eof
sstatus=rsunmatched("Status")
' chec if permit
p=checkpermit(rsunmatched("plate"))
if p=true then 	 sstatus="100% Permit"
sqli="insert into anpossibleviolators(Plate,D,T_In,T_Out,Duration,Site,Code,Notes,Direction,Status) values (" & tosqlnn(rsunmatched("Plate"),t) & "," & tosqlnn(rsunmatched("D"),t)  & "," & tosqlnn(rsunmatched("T_in"),t) & "," & tosqlnn(rsunmatched("T_out"),t) & "," & tosqlnn(rsunmatched("duration"),t)& "," & tosqlnn(rsunmatched("Site"),t) & "," & tosqlnn(rsunmatched("code"),t) & ","& tosqlnn(rsunmatched("Notes"),t) & "," & tosqlnn(rsunmatched("Direction"),t)& "," & tosqlnn(sstatus,t)  & ")"
'response.write sqli & "<br>"
objconn.execute sqli
rsunmatched.movenext
loop
rsunmatched.close

uplconnection.close
Response.write "done"
end if
end sub
function checkpermit(plate)
sqlp="select * from apermits where registration='" & plate & "'"
openrsp,sqlp
if rsp.eof and rsp.bof then
	 checkpermit=false
else
		checkpermit=true
		response.write plate & " has a permit<br>"
end if	
closers rsp
end function
sub analysisstage1

sqlma="select * from usermenu where userid=" & session("userid") & " and menu='analysisstage1'"
	openrs rsma,sqlma
	if  rsma.eof and rsma.bof then
			exit sub
	end if
	closers rsma
	

if request("cmd2")="" then
	 sqlda="select distinct site,d from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=0 order by d,site"
  sqlda="select distinct site,d,(SELECT COUNT(id) FROM anPossibleViolators aPV2 WHERE ((apv2.status!='100% Permit' and apv2.status!='repeat')  or apv2.status is null) and apv2.stage=0 and aPV2.site=aPV1.site and aPV2.d>=aPV1.d and aPV2.d<=aPV1.d) AS mycount  from anPossibleViolators apv1 where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=0 order by d,site"
sqlda="SELECT		Site, 		convert(varchar, d, 103) as d, 		COUNT(*) AS MyCount FROM		anpossibleviolators WHERE		COALESCE(Status, '') NOT IN ('100% Permit', 'repeat') 		AND Stage = 0 group by convert(varchar, d, 103),site ORDER BY	convert(varchar, d, 103),	Site"

  openrs rsda,sqlda
	if rsda.eof and rsda.bof then
		 response.write "No data to analyse for stage 1"
	else
	totalcountv=0
			response.write "<table class=""sortable"" id=""mytable""><tr><th>Site</th><th>Date</th><th>Violators</th></tr>"
			response.write "<form method=post action=admin.asp?cmd=analysisstage1&cmd2=choose>"
			response.write "Select site & Date to analyze:" '<select name=sitedate>"
				do while not rsda.eof
				' response.write "<option value='" & rsda("site") & "-" & rsda("d") & "'>" & rsda("site") & "-" & rsda("d") & "</option>"
				sqlcountv="select count(id) as mycount from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=0 and site='" & rsda("site") & "' and d>='" & cIsoDate(rsda("d")) & "' and d<='" & cisodate(rsda("d")) & " 23:59'"
				'openrs rscountv,sqlcountv
				countv=rsda("mycount")
				totalcountv=totalcountv+countv
				'closers rscountv
				response.write "<tr><td>" & rsda("site") & "</td><td>" & rsda("d") & "</td><td>" & countv & "</td><td><a class=button href=?cmd=analysisstage1&cmd2=choose&sitedate=" & rsda("site") & "-" & rsda("d") & ">View</a></td><td><a class=button href=admin.asp?cmd=analysisstage1&cmd2=delete&sitedate=" & rsda("site") & "-" & rsda("d") & " onclick=""return confirm('Are you sure')"">Delete</a></td></tr>"
				
				 rsda.movenext
			loop
			'response.write "</select><input type=submit name=submit value=analyze></form>"
			response.write "</table>"
			response.write "Total Violator Count:" & totalcountv
	end if
elseif request("cmd2")="choose" then
	 ids=""
	  sitedate=request("sitedate")
	 asitedate=split(sitedate,"-")
	 site=asitedate(0)
	 mydate=asitedate(1)
  sql="select id,plate,d,convert(varchar,t_in,114) as t_in,convert(varchar,t_out,114) as t_out,convert(varchar,Duration,114) as duration,site,notes,direction from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=0 and site='" & site & "' and d>='" & cIsoDate(mydate) & "' and d<='" & cisodate(mydate) & " 23:59' order by direction,plate"
  openrs rs,sql 
  response.write "<form method=post action=admin.asp?cmd=analysisstage1&cmd2=confirm>"
  response.write "<table border=1><tr><td>p</td><td>Direction</td><td>Plate</td><td>D</td><td>T_In</td><td>T_Out</td><td>Duration</td><td>Site</td></tr>"
 
  do while not rs.eof
  	 response.write "<tr><td><input type=checkbox name=possibleviolator-" & rs("id") & ">PV</td><td>" & rs("Direction") & "</td><td>" & rs("Plate") & "</td><td>" & rs("D") & "</td><td>" & rs("T_In") & "</td><td>" & rs("T_Out") & "</td><td>" & rs("Duration") & "</td><td>" & rs("Site") & "</td></tr>"
 		 if ids<>"" then ids=ids&"," 
		 ids=ids & rs("id")
  rs.movenext
  loop
	response.write "<input type=hidden name=ids value='" & ids & "'><tr><td colspan=5><input type=submit name=submit value=Save></form>"
  response.write "</table>"
  closers rs
elseif request("cmd2")="delete" then
	 ids=""
	  sitedate=request("sitedate")
	 asitedate=split(sitedate,"-")
	 site=asitedate(0)
	 mydate=asitedate(1)
  sql="delete from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=0 and site='" & site & "' and d>='" & cIsoDate(mydate) & "' and d<='" & cisodate(mydate) & " 23:59'"
 'response.write sql
objconn.execute sql
response.write "deleted"
	elseif request("cmd2")="confirm" then
 response.write "<form method=post action=admin.asp?cmd=analysisstage1&cmd2=save>"
	For Each Field In Request.Form
                TheString="<input type=""hidden"" name=""" & Field & """ value="""
                Value=Request.Form(Field)
                Thestring=TheString + cstr(Value) & """>" & vbcrlf
                Response.Write TheString
            Next
arrids=split(request("ids"),",")
ticked=0
For x = lbound(arrids) to ubound(arrids)
					if request.form.item("possibleviolator-" & arrids(x))<>"" then
    				ticked=ticked+1
  				end if
					
		Next
response.write "You have " & ticked & " violators ticked<br>"
response.write "<input type=submit name=submit value=save class=button>&nbsp;<a href=""javascript:history.go(-1)"" class=button>Back</a></form>"
	
elseif request("cmd2")="save" then

		'response.write "<hr>" & request("ids") & "<hr>"
		arrids=split(request("ids"),",")
		For x = lbound(arrids) to ubound(arrids)
		'response.write "<hr>ID:" & arrids(x)
				'response.write request.form.key("possibleviolator-" & arrids(x)) & "<hr>"
					if request.form.item("possibleviolator-" & arrids(x))<>"" then
    				sql="update anpossibleviolators set possibleviolator=1,stage=1,mydatestage1=getdate(),userid=" & session("userid") & ",useridpv=" & session("userid") & " where id=" & arrids(x)
      	'		response.write sql	 & "<br>"
						objconn.execute sql
  				else
						sql="update anpossibleviolators set stage=1,mydatestage1=getdate(),userid=" & session("userid") & " where id=" & arrids(x)
    			'	response.write sql	 & "<br>"
						objconn.execute sql
					end if
					
		Next
response.write "done"
end if

end sub


sub analysisstage2

'call sendemail("webmaster@cleartone.com","ec@awebforyou.com","test","test")

if request("cmd2")="" then
	 sqlda="select distinct site,d from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=1 and possibleviolator=1"
  openrs rsda,sqlda
	if rsda.eof and rsda.bof then
		 response.write "No data to analyse for stage 2"
	else
			response.write "<table class=""sortable"" id=""mytable""><tr><th>Site</th><th>Date</th><th>Violators</th></tr>"
			response.write "<form method=post action=admin.asp?cmd=analysisstage2&cmd2=choose>"
			response.write "Select site & Date to analyze:" '<select name=sitedate>"
				totalcountv=0
				do while not rsda.eof
				sqlcountv="select count(id) as mycount from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=1 and possibleviolator=1 and site='" & rsda("site") & "' and d>='" & cIsoDate(rsda("d")) & "' and d<='" & cisodate(rsda("d")) & " 23:59'"
				openrs rscountv,sqlcountv
				countv=rscountv("mycount")
				totalcountv=totalcountv+countv
				closers rscountv
				' response.write "<option value='" & rsda("site") & "-" & rsda("d") & "'>" & rsda("site") & "-" & rsda("d") & "</option>"
				response.write "<tr><td>" & rsda("site") & "</td><td>" & rsda("d") & "</td><td>" & countv & "</td><td><a class=button href=?cmd=analysisstage2&cmd2=choose&sitedate=" & rsda("site") & "-" & rsda("d") & ">View</a>"
				
				 rsda.movenext
			loop
			'response.write "</select><input type=submit name=submit value=analyze></form>"
			response.write "</table>"
			response.write "Total Violator Count:" & totalcountv
	end if
elseif request("cmd2")="choose" then
	 ids=""
	 sitedate=request("sitedate")
	 asitedate=split(sitedate,"-")
	 site=asitedate(0)
	 mydate=asitedate(1)
  sql="select id,plate,d,convert(varchar,t_in,114) as t_in,convert(varchar,t_out,114) as t_out,convert(varchar,Duration,114) as duration,site,code,notes,direction,status from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=1 and possibleviolator=1 and site='" & site & "' and d>='" & cIsoDate(mydate) & "' and d<='" & cisodate(mydate) & " 23:59'"
	 ' response.write sql
	openrs rs,sql 
  response.write "<form method=post action=admin.asp?cmd=analysisstage2&cmd2=confirm>"
  response.write "<table border=1><tr><td></td><td>Duration</td><td>Plate</td><td>D</td><td>T_In</td><td>T_Out</td><td>Site</td><td>Direction</td>"
  if site="071"  or site="072" then response.Write "<td>Fuzzy Ex Matches</td>"
  response.Write "</tr>"
  do while not rs.eof
  	 response.write "<tr><td><input type=checkbox name=confirmedviolator-" & rs("id") & ">CV</td><td>" & rs("Duration") & "</td><td>" & rs("Plate") & "</td><td>" & rs("D") & "</td><td>" & rs("T_In") & "</td><td>" & rs("T_Out") & "</td><td>" & rs("Site") & "</td><td>" & rs("Direction") & "</td>"
  	 if site="071" or site="072" then
  	 
        response.Write "<td>" & checksitefuzzysex(rs("plate"),mydate,site)  	  & "</td>"
  	 end if
  	 
  	 response.Write "</tr>"
 		 if ids<>"" then ids=ids&"," 
		 ids=ids & rs("id")
  rs.movenext
  loop
	response.write "<input type=hidden name=ids value='" & ids & "'><tr><td><input type=submit name=submit value=Save></form>"
  response.write "</table>"
  closers rs
	elseif request("cmd2")="confirm" then
 response.write "<form method=post action=admin.asp?cmd=analysisstage2&cmd2=save>"
	For Each Field In Request.Form
                TheString="<input type=""hidden"" name=""" & Field & """ value="""
                Value=Request.Form(Field)
                Thestring=TheString + cstr(Value) & """>" & vbcrlf
                Response.Write TheString
            Next
arrids=split(request("ids"),",")
ticked=0
For x = lbound(arrids) to ubound(arrids)
					if request.form.item("confirmedviolator-" & arrids(x))<>"" then
    				ticked=ticked+1
  				end if
					
		Next
response.write "You have " & ticked & " violators ticked<br>"
response.write "<input type=submit name=submit value=save class=button>&nbsp;<a href=""javascript:history.go(-1)"" class=button>Back</a></form>"
	
elseif request("cmd2")="save" then
		'response.write "<hr>" & request("ids") & "<hr>"
		arrids=split(request("ids"),",")
		For x = lbound(arrids) to ubound(arrids)
		'response.write "<hr>ID:" & arrids(x)
				'response.write request.form.key("possibleviolator-" & arrids(x)) & "<hr>"
					if request.form.item("confirmedviolator-" & arrids(x))<>"" then
    				sql="update anpossibleviolators set confirmedviolator=1,stage=2,mydatestage2=getdate(),useridcv=" & session("userid") & " where id=" & arrids(x)
      			'response.write sql	 & "<br>"
						objconn.execute sql
					'	call insertviolator(arrids(x))
  				else
						sql="update anpossibleviolators set stage=2,mydatestage2=getdate() where id=" & arrids(x)
    			'	response.write sql	 & "<br>"
						objconn.execute sql
						
					end if
					
		Next
response.write "done"
end if

end sub
sub confirmanalysis
if request("cmd2")<>"confirm" then
 sqlda="select distinct site,d from anPossibleViolators where confirmedviolator=1 and stage=2 and inserted=0 order by site,d"
  sqlda="SELECT DISTINCT site, d, (SELECT COUNT(id) FROM anPossibleViolators aPV2 WHERE aPV2.confirmedviolator=1 and aPV2.stage=2 and aPV2.inserted=0 and aPV2.site=aPV1.site and aPV2.d>=aPV1.d and aPV2.d<=aPV1.d) AS mycount FROM anPossibleViolators aPV1 WHERE confirmedviolator=1 AND stage=2 AND inserted=0 ORDER BY site,d"
  sqlda="SELECT		Site,d,		COUNT(*) AS MyCount FROM		anPossibleviolators WHERE		Confirmedviolator = 1 		AND Stage = 2 		AND Inserted = 0 GROUP BY	Site, 	d ORDER BY	Site, 		d "
  
  openrs rsda,sqlda
	if rsda.eof and rsda.bof then
		 response.write "No data to confirm"
	else
			response.write "<table class=""sortable"" id=""mytable""><tr><td></td><th>Site</th><th>Date</th><th>Violators</th></tr>"
			response.write "<form method=post action=admin.asp?cmd=confirmanalysis&cmd2=confirm>"
			response.write "Check to confirm" '<select name=sitedate>"
				totalcountv=0
				do while not rsda.eof
			'	sqlcountv="select count(id) as mycount from anPossibleViolators where confirmedviolator=1 and stage=2 and inserted=0 and site='" & rsda("site") & "' and d>='" & cIsoDate(rsda("d")) & "' and d<='" & cisodate(rsda("d")) & " 23:59'"
			'	openrs rscountv,sqlcountv
				countv=rsda("mycount")
				totalcountv=totalcountv+countv
			'	closers rscountv
				' response.write "<option value='" & rsda("site") & "-" & rsda("d") & "'>" & rsda("site") & "-" & rsda("d") & "</option>"
				response.write "<tr><td><input type=checkbox name=confirm value='" & rsda("site") & "!" & rsda("d") & "'></td><td>" & rsda("site") & "</td><td>" & rsda("d") & "</td><td>" & countv & "</td></tr>"
				
				 rsda.movenext
			loop
			response.write "<tr><td colspan=2 align=center><input type=submit name=submit value=Confirm class=button></td></tr></form>"
			response.write "</table>"
end if		
else
confirm=request("confirm")

arrconfirm=split(confirm,",")
'response.write confirm & "<hr>"
for i=lbound(arrconfirm) to ubound(arrconfirm)
		arrid=split(arrconfirm(i),"!")
		'response.write "<font color=red>" & arrid(0) & "</font>" & arrid(1) & "<bR>"
		sql="select * from anPossibleViolators where confirmedviolator=1 and stage=2 and inserted=0 and site='" & trim(arrid(0))& "' and d>='" & cIsoDate(trim(arrid(1))) & "' and d<='" & cisodate(trim(arrid(1))) & " 23:59'"
	
	sql="exec insertviolators @thesite='" & trim(arrid(0))& "',@thedate='" & cIsoDate(trim(arrid(1))) & "',@thedateto='" & cIsoDate(trim(arrid(1))) & " 23:59'"
	'response.write sql
	objconn.execute sql 
	
		'response.write sql
		'openrs rs,sql
			'		 do while not rs.eof
				'	 response.write "insertviolator "& rs("id") & "<br>"
				'	 call insertviolator(rs("id"))
					' rs.movenext
					 'loop
'		closers rs
'	response.write arrconfirm(i) & "<hr>"
	
	
next

response.write "confirmed and inserted successfully"
end if

end sub


sub confirmanalysisall
if request("cmd2")<>"confirm" then

  
  sqlda="SELECT	id,	plate,d,convert(varchar,t_in,108) as t_in,convert(varchar,t_out,108) as t_out,convert(varchar,duration,108) as duration,site,notes from		anPossibleviolators WHERE		Confirmedviolator = 1 		AND Stage = 2 		AND Inserted = 0 order by site,d,plate"
  
  openrs rsda,sqlda
	if rsda.eof and rsda.bof then
		 response.write "No data to confirm"
	else
			response.write "<table class=""sortable"" id=""mytable""><tr><td>Confirm</td><th>Plate</th><th>date</th><th>In</th><th>Out</th><th>Duration</th><th>Site</th><th>Notes</th><td>Delete</td></tr>"
			response.write "<form method=post action=admin.asp?cmd=confirmanalysisall&cmd2=confirm>"
			response.write "Check to confirm" '<select name=sitedate>"
				totalcountv=0
				do while not rsda.eof
			
				response.write "<tr><td><input type=checkbox name=confirm  value='" & rsda("id") & "' ></td></td><td>" & rsda("plate") & "</td><td>" & rsda("d") & "</td><td>" & rsda("t_in") & "</td><td>" & rsda("t_out")& "</td><td>" & rsda("duration")& "</td><td>" & rsda("site")& "</td><td>" & rsda("notes")& "</td><td><input type=checkbox name=delete value='" & rsda("id") & "' id=d" & rsda("id") & " onclick = ""confirmreason(this)""><br><input type=text name=deletedreason" & rsda("id") & "  id = ""deletedreason" & rsda("id") & """  onblur = ""checkreason(this)""></td></tr>"
				
				 rsda.movenext
			loop
			
			response.write "</table>"
			response.write "<input type=submit name=submit value=Confirm class=button></form>"
end if		
else
confirm=request("confirm")
arrconfirm=split(confirm,",")
'response.Write "confirm:" & confirm
deleted=request("delete")
'response.Write "delete:" & deleted
arrdeleted=split(deleted,",")
reasonsfilled=1
for i=lbound(arrdeleted) to ubound(arrdeleted)
if request("deletedreason" & arrdeleted(i) )="" then    reasonsfilled=0
    

next
'if reasonsfilled=0 then 
 '   response.Write "Please hit the back key to go back and fill in all reasons for deleted items."
  '  response.end
'end if


for i=lbound(arrdeleted) to ubound(arrdeleted)

delreason=request("deletedreason" & trim(arrdeleted(i)))

sql="update anpossibleviolators set confirmedviolator=0,deletedreason=" & tosql(delreason ,"text") & ",deletedby=" & tosql(session("userid"),"Number") & " ,datedeleted=getdate() where id=" & arrdeleted(i) 
'response.Write sql & "<br>"
objconn.execute sql
next

for i=lbound(arrconfirm) to ubound(arrconfirm)
		'arrid=split(arrconfirm(i),"!")
		'response.write "<font color=red>" & arrid(0) & "</font>" & arrid(1) & "<bR>"
	'	sql="select * from anPossibleViolators where confirmedviolator=1 and stage=2 and inserted=0 and site='" & trim(arrid(0))& "' and d>='" & cIsoDate(trim(arrid(1))) & "' and d<='" & cisodate(trim(arrid(1))) & " 23:59'"
	
	sql="exec inserttempviolator @id="&  arrconfirm(i)	
'response.Write " <br>need to insert " &  arrconfirm(i)	
'response.Write sql & "<br>"	
objconn.execute sql	
next

response.write "confirmed  successfully"
end if

end sub
sub insertviolator(id)
sqlanp="select plate,convert(varchar,d,21) as d,site,convert(varchar,t_in,21) as t_in,convert(varchar,t_out,21) as t_out,duration from anpossibleviolators where id=" & id
openrs rsanp,sqlanp
'sqlckd="select * from dvla where reg='" & rsanp("plate") & "'"
'openrs rsckd,sqlckd
'if not rsckd.eof and not rsckd.bof then 
'sql="insert into tempviolators(registration,[date of violation],site,[arrival time],[departure time],[duration of stay]) values('" & rsanp("plate") & "','" & rsanp("D") & "','" & rsanp("site") & "','" & rsanp("T_In") & "','" & rsanp("T_Out") & "','" & rsanp("Duration")  & "')"
					'	response.write sql
'						objconn.execute sql
'else
sql="insert into tempviolators(registration,[date of violation],site,[arrival time],[departure time],[duration of stay]) values('" & rsanp("plate") & "','" & rsanp("D") & "','" & rsanp("site") & "','" & rsanp("T_In") & "','" & rsanp("T_Out") & "','" & rsanp("Duration") &" ')"
					'	response.write sql
						objconn.execute sql
						sql="update tempviolators set site=RIGHT('000'+site,3) where len(site)<3"
						objconn.execute sql
sql="update anpossibleviolators set inserted=1 where id=" & id
objconn.execute sql
'end if
'closers rsckd
closers rsanp
end sub
function newpcn
mypcn=""
do while mypcn=""
Randomize ' Initialize random-number generator.
rnumber= Int((9999999 * Rnd) + 1000000) 
sqlck="select * from violators where left(pcn,7)=" & rnumber
openrs rsck,sqlck
if rsck.eof and rsck.bof then mypcn=rnumber
closers rsck
loop
newpcn=mypcn
end function
sub anreportbyemp
''selet date and show how many of each stage each employee did on that day
if request.form("cmd2")="" then
%>

<form method=post action=admin.asp?cmd=anreportbyemp><input type=hidden name=cmd2 value="print">
Report on date 
<input type=text name=printeddate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('printeddate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)

to 

<input type=text name=printeddateto value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('printeddateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)

<br>
<input type=submit value=submit name=sumbit>
</form>

<%
else
		Response.write "Report for date:" & request("printeddate") & "-" & request("printeddateto")
		sql="select count(id) as mycount from anpossibleviolators where " 
		sqlwhere = " mydatestage1 >="
	datefrom=request.form("printeddate")
	arrdatefrom=split(datefrom,"/")
	myday=arrdatefrom(0)
	mymonth=arrdatefrom(1)
	myear=arrdatefrom(2)
		dateto=request.form("printeddateto")
	arrdateto=split(dateto,"/")
	mydayt=arrdateto(0)
	mymontht=arrdateto(1)
	myeart=arrdateto(2)
sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
	sqlwhere=sqlwhere & " and mydatestage1<="  & "cast('" & myeart & mymontht & mydayt & " 23:59' as datetime)"
	sql1=sql & sqlwhere
	
			sqlwhere= " mydatestage2>="
	datefrom=request.form("printeddate")
	arrdatefrom=split(datefrom,"/")
	myday=arrdatefrom(0)
	mymonth=arrdatefrom(1)
	myear=arrdatefrom(2)
		dateto=request.form("printeddateto")
	arrdateto=split(dateto,"/")
	mydayt=arrdateto(0)
	mymontht=arrdateto(1)
	myeart=arrdateto(2)
sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
	sqlwhere=sqlwhere & " and mydatestage2<="  & "cast('" & myeart & mymontht & mydayt & " 23:59' as datetime)"
	sql2=sql & sqlwhere
	response.write  "<table class=maintable2border><tr><td>Employee</td><td>Stage 1 checked</td><td>stage 2 checked</td></tr>"
	sqlemp="select * from useraccess"
	openrs rsemp,sqlemp
	do while not rsemp.eof
		 sqlstage1=sql1 & " and useridpv=" & rsemp("id")
		 response.write "<!--st1" & sqlstage1 & "-->" & vbcrlf
		 openrs rsstage1,sqlstage1
		 stage1count=rsstage1("mycount")
		 closers rsstage1
		 sqlstage2=sql2 & " and useridcv=" & rsemp("id")
		 response.write "<!--st2" & sqlstage2 & "-->" & vbcrlf
		 openrs rsstage2,sqlstage2
		 stage2count=rsstage2("mycount")
		 closers rsstage2
		 response.write "<tr><td>" & rsemp("userid") & "</td><td>" & stage1count & "</td><td>" & stage2count & "</td></tr>"
	rsemp.movenext
	loop	
	response.write "</table>"
	closers rsemp
end if	


end sub

sub analysisreport


if request.form("cmd2")="" then
%>
<table class=maintable2>
<tr>
<td>
<form method=post action=admin.asp?cmd=analysisreport><input type=hidden name=cmd2 value="print">
<b>Date of violation:</b></td>
</tr>
<tr><td>
From: </td><td><input type=text name=fromdate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('fromdate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To:</td><td> <input type=text name=todate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('todate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<Tr><td colspan=2>
		<input type=checkbox name=doc value=1>Check here to see by Date of Action and Not Date of Violation<br> 
	</td></tr>
	<tr><td colspan=2 align=center>
<input type=submit value=submit name=sumbit>
</td>
</tr>
</table>
</form>

<%
else
		Response.write "Report for date :" & request("fromdate") & " to " & request("todate")
		
	datefrom=request.form("fromdate")
	arrdatefrom=split(datefrom,"/")
	myday=arrdatefrom(0)
	mymonth=arrdatefrom(1)
	myear=arrdatefrom(2)
	dateto=request.form("todate")
		arrdatefrom=split(dateto,"/")
	mydayt=arrdatefrom(0)
	mymontht=arrdatefrom(1)
	myeart=arrdatefrom(2)
	if request("doc")="" then
    datebyd="1"
	sqlwhere=sqlwhere & "d >="
	sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
	sqlwhere=sqlwhere & " and d<=cast('" & myeart & mymontht & mydayt & " 23:59' as datetime)"
	sqlwheremd=sqlwhere
	sqlwheremd1=sqlwhere
	sqlwheremd2=sqlwhere
	else
	    datebyd="0"
			sqlwheremd=sqlwheremd & "mydate >="
			sqlwheremd=sqlwheremd & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
			sqlwheremd=sqlwheremd & " and mydate<=cast('" & myeart & mymontht & mydayt & " 23:59' as datetime)"
			
			sqlwheremd1=sqlwheremd1 & "mydatestage1 >="
			sqlwheremd1=sqlwheremd1 & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
			sqlwheremd1=sqlwheremd1 & " and mydatestage1<=cast('" & myeart & mymontht & mydayt & " 23:59' as datetime)"

			sqlwheremd2=sqlwheremd2 & "mydatestage2 >="
			sqlwheremd2=sqlwheremd2 & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
			sqlwheremd2=sqlwheremd2 & " and mydatestage2<=cast('" & myeart & mymontht & mydayt & " 23:59' as datetime)"

end if
response.write "<table class=maintable2border><tr><td>Site</td><td>Violators</td><td>Possible Violation</td><td>Confirmed Violation</td></tr>"
sqlsite="select * from [site information] order by sitecode"
sqlsite="exec spsiteoverview @datebyd=" & datebyd & ",@fromdate='" & myear & mymonth & myday & " 00:00',@todate='" & myeart & mymontht & mydayt & " 23:59'"
'response.Write sqlsite & "<hr>"
i=0
openrs rssite,sqlsite
do while not rssite.eof
sql="select count(id) as mycount from anpossibleviolators where site='" & rssite("sitecode") & "' and "&sqlwheremd
'response.Write sql
'openrs rs,sql
tviolators=rssite("tviolators") 'rs("mycount")
'tviolatorsold=rs("mycount")
'closers rs
'sql="select count(plate) as mycount from annonviolations where site='" & rssite("sitecode") & "' and "&sqlwheremd
'openrs rs,sql
'nonviolators=rs("mycount")
'closers rs
'sql="select count(plate) as mycount from anunmatched where site='" & rssite("sitecode") & "' and "&sqlwheremd
'openrs rs,sql
'unmatchedcount=rs("mycount")
'closers rs
sql="select count(id) as mycount from anpossibleviolators where site='" & rssite("sitecode") & "' and possibleviolator=1 and "&sqlwheremd1
openrs rs,sql
pviolatorscount=rs("mycount")
closers rs
sql="select count(id) as mycount from anpossibleviolators where site='" & rssite("sitecode") & "' and confirmedviolator=1 and "&sqlwheremd2 
response.write "<!--sql:" & sql & "-->" & vbcrlf
openrs rs,sql
cviolatorscount=rs("mycount")
closers rs
if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
response.write "<td>" & rssite("sitecode") & "</td><td>" & tviolators   & "</td><td>" & pviolatorscount & "</td><td>" & cviolatorscount & "</td></tr>" 
i=i+1
rssite.movenext

loop
closers rssite
response.write "</table>"
end if
end sub
sub ansitenodata
if request("cmd2")="" then
response.write "<form method=post action=admin.asp?cmd=ansitenodata><input type=hidden name=cmd2 value=show>"
response.write "Select a Week: <select name=datetoday>"
		dWkStartDate = date()
		j=1
		do while j<53
		
		
		Do until weekday(dWkStartDate) = vbsunday
 		dWkStartDate = DateAdd("d",-1,dWkStartDate)
		Loop
		dWkEndDate = DateAdd("d",6,dWkStartDate)
		response.write "<option value=" & dwkstartdate 
		if dtoday=date then response.write " selected "
		response.write  ">" 
		Response.Write dwkstartdate & "-" & dwkenddate
		response.write "</option>" & vbcrlf
	'	response.write "<!--today:" & date() & "-" & dtoday & "-->" 
		dWkStartDate=dwkstartdate-7
		j=j+1
		loop
		response.write "</select>"
		
		
		response.write "<input type=submit name=submit value='Go'>"

else
		datestartweek=request("datetoday")
		dateendweek=dateadd("d","6",datestartweek)
		'sql="select * from anpossibleviolators where "
		response.write "Sites with no data on date of violation"
response.write "<table class=maintable2border>"
response.write "<tr>"
dt=datestartweek
while datediff("D", dt, dateendweek)>=0
  response.write "<td><b>" &  weekdayname(weekday(dt)) & "</b><br>" & dt & "</td>"
  dt = dateadd("D", 1, dt)
wend
response.write "</tr>"

	  dt=datestartweek
  while datediff("D", dt, dateendweek)>=0
   		sqlwhere= "d >="
    	datefrom=dt
    	arrdatefrom=split(datefrom,"/")
    	myday=arrdatefrom(0)
    	mymonth=arrdatefrom(1)
    	myear=arrdatefrom(2)
    sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
     sqlwhere=sqlwhere & " and d<=cast('" & myear & mymonth & myday & " 23:59' as datetime)"
    sql="select sitecode from [site information] where sitecode not in(select site from anpossibleviolators where " & sqlwhere & ")"
    openrs rs,sql
		response.write "<td>"
    do while not rs.eof
    
    response.write "<div class=flt>" & rs("sitecode") & "</div>"
    rs.movenext
    loop
    'response.write "</td></tr></table>"
    closers rs	
		response.write "</td>"
		
    dt = dateadd("D", 1, dt)
  wend
  response.write "</tr>"

end if
end sub
sub anprintstage1

sql="select distinct site,d from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=0"
openrs rsda,sql
if not rsda.eof and not rsda.bof then
	 response.write "<table class=maintable2border><tr><th colspan=2>Sites waiting for analysis stage 1</th></tR><tr><td colspan=2>"
totalcountv=0
			response.write "<table class=""sortable"" id=""mytable""><tr><th>Site</th><th>Date</th><th>Violators</th></tr>"
			
				do while not rsda.eof
				' response.write "<option value='" & rsda("site") & "-" & rsda("d") & "'>" & rsda("site") & "-" & rsda("d") & "</option>"
				sqlcountv="select count(id) as mycount from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=0 and site='" & rsda("site") & "' and d>='" & cIsoDate(rsda("d")) & "' and d<='" & cisodate(rsda("d")) & " 23:59'"
				openrs rscountv,sqlcountv
				countv=rscountv("mycount")
				totalcountv=totalcountv+countv
				closers rscountv
				response.write "<tr><td>" & rsda("site") & "</td><td>" & rsda("d") & "</td><td>" & countv & "</td>"
				
				 rsda.movenext
			loop
			'response.write "</select><input type=submit name=submit value=analyze></form>"
			response.write "</table>"
			response.write "Total Violator Count:" & totalcountv
response.write "</td></tr></table>"
else
response.write "no records waiting for stage 1"
end if
closers rs

end sub
sub anprintstage2
sql="select distinct site,d from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=1 and possibleviolator=1"
openrs rsda,sql
if not rsda.eof and not rsda.bof then
	 response.write "<table class=maintable2border><tr><th colspan=2>Sites waiting for analysis stage 2</th></tR><tr><td colspan=2>"
totalcountv=0
			response.write "<table class=""sortable"" id=""mytable""><tr><th>Site</th><th>Date</th><th>Violators</th></tr>"
			
				do while not rsda.eof
				' response.write "<option value='" & rsda("site") & "-" & rsda("d") & "'>" & rsda("site") & "-" & rsda("d") & "</option>"
				sqlcountv="select count(id) as mycount from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=1 and possibleviolator=1 and site='" & rsda("site") & "' and d>='" & cIsoDate(rsda("d")) & "' and d<='" & cisodate(rsda("d")) & " 23:59'"
				openrs rscountv,sqlcountv
				countv=rscountv("mycount")
				totalcountv=totalcountv+countv
				closers rscountv
				response.write "<tr><td>" & rsda("site") & "</td><td>" & rsda("d") & "</td><td>" & countv & "</td>"
				
				 rsda.movenext
			loop
			'response.write "</select><input type=submit name=submit value=analyze></form>"
			response.write "</table>"
			response.write "Total Violator Count:" & totalcountv
response.write "</td></tr></table>"
else
response.write "no records waiting for stage 2"
end if
closers rs

end sub
sub zanprintstage1
response.write "<table class=maintable2border><tr><th colspan=5>Outstanding Stage 1</th></tr><tr><td>Direction</td><td>Plate</td><td>D</td><td>T_In</td><td>T_Out</td><td>Duration</td><td>Site</td><td>Code</td><td>Notes</td><td>Status</td></tr>"
sql="select id,plate,d,convert(varchar,t_in,114) as t_in,convert(varchar,t_out,114) as t_out,convert(varchar,Duration,114) as duration,site,notes,direction from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=0 order by direction,plate"
openrs rs,sql
do while not rs.eof
response.write "<tr><td>" & rs("Direction") & "</td><td>" & rs("Plate") & "</td><td>" & rs("D") & "</td><td>" & rs("T_In") & "</td><td>" & rs("T_Out") & "</td><td>" & rs("Duration") & "</td><td>" & rs("Site") & "</td><td>" & rs("Notes") & "</td></tr>"
rs.movenext
loop
closers rs

end sub

sub zanprintstage2
response.write "<table class=maintable2border><tr><th colspan=5>Outstanding Stage 2</th></tr><tr><td>Direction</td><td>Plate</td><td>D</td><td>T_In</td><td>T_Out</td><td>Duration</td><td>Site</td><td>Code</td><td>Notes</td><td>Status</td></tr>"
sql="select id,plate,d,convert(varchar,t_in,114) as t_in,convert(varchar,t_out,114) as t_out,convert(varchar,Duration,114) as duration,site,code,notes,direction,status from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=1 and possibleviolator=1"
openrs rs,sql
do while not rs.eof
response.write "<tr><td>" & rs("Direction") & "</td><td>" & rs("Plate") & "</td><td>" & rs("D") & "</td><td>" & rs("T_In") & "</td><td>" & rs("T_Out") & "</td><td>" & rs("Duration") & "</td><td>" & rs("Site") & "</td><td>" & rs("Notes") & "</td></tr>"
rs.movenext
loop
closers rs

end sub
sub analerts
response.write "<table class=maintable2border>"
'	For any site that has not had data uploaded within 4 days.  If not data in analysis within 4 days
sql="select sitecode from [site information] where sitecode not in(select site from anpossibleviolators where mydate>getdate()-4)"
response.write "<tr><th>Sites that data has not been uploaded within 4 days</th></tr><tr><td>"
openrs rs,sql
do while not rs.eof
	 response.write rs("sitecode") &"<br>"
	 rs.movenext
loop
closers rs
response.write "</td></tr>"
'	If 1st stage is not completed within 2-days of the data being uploaded.
 response.write "<tr><th>Sites where 1st stage not completed within 2 days of being uploaded</th></tr><tr><td>"
sql="select distinct site,d from anpossibleviolators where datediff(day, mydate, mydatestage1) > 2"
'response.write sql
openrs rs,sql
do while not rs.eof
	 response.write rs("site") & " - " & rs("d")  &"<br>"
	 rs.movenext
loop
closers rs
response.write "</td></tr>"
'	If second stage is not completed with 1-day of 1st stage.
  response.write "<tr><th>Sites where second stage not completed within 1 day of first stage</th></tr><tr><td>"
sql="select distinct site,d from anpossibleviolators where datediff(day, mydatestage1, mydatestage2) > 1"
'response.write sql
openrs rs,sql
do while not rs.eof
	 response.write rs("sitecode") &"<br>"
	 rs.movenext
loop
closers rs
response.write "</td></tr>"
'	If DVLA request is not submitted within 2-days of second stage.
   response.write "<tr><th>Amount of records where dvla request was not submitted <br>and more then  2 days have passed since second stage</th></tr><tr><td>"
sql="select count(id) as mycount from anpossibleviolators where mydatestage2> getdate()+2 and plate not in(select dvla from dvlarep)"
openrs rs,sql

	 response.write rs("mycount") &"<br>"

closers rs
response.write "</td></tr>"
	'IF DVLA results is not received within 2-days of the request.
 
 response.write "<tr><th>Amount of records where dvla request was not received <br>and  2 days have passed since the request/th></tr><tr><td>"
sql="select count(id) as mycount from dvlarep where dategenerated>getdate()+2 and dvla not in (select reg from dvla)"
openrs rs,sql

	 response.write rs("mycount") &"<br>"

closers rs
end sub
sub anweekly
if request("cmd2")="" then
response.write "<form method=post action=admin.asp?cmd=anweekly><input type=hidden name=cmd2 value=show>"
response.write "Select a Week: <select name=datetoday>"
		dWkStartDate = date()
		j=1
		do while j<53
		
		
		Do until weekday(dWkStartDate) = vbsunday
 		dWkStartDate = DateAdd("d",-1,dWkStartDate)
		Loop
		dWkEndDate = DateAdd("d",6,dWkStartDate)
		response.write "<option value=" & dwkstartdate 
		if dtoday=date then response.write " selected "
		response.write  ">" 
		Response.Write dwkstartdate & "-" & dwkenddate
		response.write "</option>" & vbcrlf
	'	response.write "<!--today:" & date() & "-" & dtoday & "-->" 
		dWkStartDate=dwkstartdate-7
		j=j+1
		loop
		response.write "</select>" %>
		<br>
		<input type=checkbox name=doc value=1>Check here to see by Date of Action and Not Date of Violation<br> 
		<%
		response.write "<input type=submit name=submit value='Go'>"

else
		datestartweek=request("datetoday")
		dateendweek=dateadd("d","6",datestartweek)
		'sql="select * from anpossibleviolators where "
		response.write "Weekly report of Possible Violators"
response.write "<table class=maintable2border>"
response.write "<tr><td></td>"
dt=datestartweek
while datediff("D", dt, dateendweek)>=0
  response.write "<td><b>" &  weekdayname(weekday(dt)) & "</b><br>" & dt & "</td>"
  dt = dateadd("D", 1, dt)
wend
response.write "</tr>"
sqlsite="select * from [site information] order by sitecode"
openrs rssite,sqlsite
do while not rssite.eof
	 response.write "<tr><td><b>" & rssite("sitecode") & "</b></td>"
  dt=datestartweek
  while datediff("D", dt, dateendweek)>=0
   
		
		arrdatefrom=split(dt,"/")
	myday=arrdatefrom(0)
	mymonth=arrdatefrom(1)
	myear=arrdatefrom(2)
	
	sqlwhere= "site=" & rssite("sitecode") & " and d >="
	sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
	sqlwhere=sqlwhere & " and d<=cast('" & myear & mymonth & myday & " 23:59' as datetime)"
		'data extracted, no data, first stage, second stage and perfect or the back log, which would mean the amount of records extracted that has not been confirmed after 2nd stage.
		if request("doc")=1 then
			 sqlwhere= "site=" & rssite("sitecode") & " and mydate >="
			 sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
			 sqlwhere=sqlwhere & " and mydate<=cast('" & myear & mymonth & myday & " 23:59' as datetime)"
		end if
		sqlc="select count(id) as mycount from anpossibleviolators where " & sqlwhere
	'	response.write sqlc
		openrs rsc,sqlc
		countpv=rsc("mycount")
														if request("doc")=1 then
			 sqlwhere= "site=" & rssite("sitecode") & " and mydatestage1 >="
			 sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
			 sqlwhere=sqlwhere & " and mydatestage1<=cast('" & myear & mymonth & myday & " 23:59' as datetime)"
		end if
				sqlc="select count(id) as mycount from anpossibleviolators where possibleviolator=1 and " & sqlwhere
	'	response.write sqlc
		openrs rsc,sqlc
		countstage1=rsc("mycount")
			if request("doc")=1 then
			 sqlwhere= "site=" & rssite("sitecode") & " and mydatestage2 >="
			 sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
			 sqlwhere=sqlwhere & " and mydatestage2<=cast('" & myear & mymonth & myday & " 23:59' as datetime)"
		end if
		sqlc="select count(id) as mycount from anpossibleviolators where confirmedviolator=1 and " & sqlwhere
	'	response.write sqlc
		openrs rsc,sqlc
		countstage2=rsc("mycount")
		
		sqlc="select count(id) as mycount from anpossibleviolators where (confirmedviolator!=1 or confirmedviolator is null) and " & sqlwhere
	'	response.write sqlc
		openrs rsc,sqlc
		countbacklog=rsc("mycount")
		sqlfin="select count(id) as mycount from anpossibleviolators where mydatestage1 is not null and mydatestage2 is not null and " & sqlwhere
		openrs rsfin,sqlfin
		countfinished=rsfin("mycount")
		closers rs
		if countfinished=0 then
		 response.write "<td>" 
		else
				response.write "<td class=bgyellow>"
		end if
		response.write "extracted:" & countpv & "<br><br>"
			response.write "stage1 checked:" & countstage1 & "<br><br>"
			response.write "stage2 checked:" & countstage2 & "<br><br>"
		'response.write "backlog:" & countpv & "<br>"
		response.write  "</td>"
		
    dt = dateadd("D", 1, dt)
  wend
  response.write "</tr>"

rssite.movenext
loop
closers rssite
end if
end sub
sub anpdf
		
	 sqlda="select distinct site,d from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=0 order by d,site"
  openrs rsda,sqlda
	if rsda.eof and rsda.bof then
		 response.write "No data to analyse for stage 1"
	else
			response.write "<table class=maintable2><tr><th>Site</th><th>Date</th></tr>"
			'response.write "<form method=post action=admin.asp?cmd=analysisstage1&cmd2=choose>"
			response.write "Select site & Date to analyze:" '<select name=sitedate>"
				do while not rsda.eof
				' response.write "<option value='" & rsda("site") & "-" & rsda("d") & "'>" & rsda("site") & "-" & rsda("d") & "</option>"
				response.write "<tr><td>" & rsda("site") & "</td><td>" & rsda("d") & "</td><td><a class=button href=analysispdf.asp?sitedate=" & rsda("site") & "-" & rsda("d") & ">Create PDF</a>"
				
				 rsda.movenext
			loop
			response.write "<tr><td colspan=2 align=center><a href=analysispdf.asp?sitedate=all class=button>All</a></td></tr>"
			'response.write "</select><input type=submit name=submit value=analyze></form>"
			response.write "</table>"
	end if


end sub
sub anpdf2
		
	 sqlda="select distinct site,d from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=1 and possibleviolator=1"
  openrs rsda,sqlda
	if rsda.eof and rsda.bof then
		 response.write "No data to analyse for stage 2"
	else
			response.write "<table class=maintable2><tr><th>Site</th><th>Date</th></tr>"
			'response.write "<form method=post action=admin.asp?cmd=analysisstage1&cmd2=choose>"
			response.write "Select site & Date to analyze:" '<select name=sitedate>"
				do while not rsda.eof
				' response.write "<option value='" & rsda("site") & "-" & rsda("d") & "'>" & rsda("site") & "-" & rsda("d") & "</option>"
				response.write "<tr><td>" & rsda("site") & "</td><td>" & rsda("d") & "</td><td><a class=button href=analysispdf.asp?sitedate=" & rsda("site") & "-" & rsda("d") & ">Create PDF</a>"
				
				 rsda.movenext
			loop
			response.write "<tr><td colspan=2 align=center><a href=analysispdf2.asp?sitedate=all class=button>All</a></td></tr>"
			'response.write "</select><input type=submit name=submit value=analyze></form>"
			response.write "</table>"
	end if


end sub
sub anexcel
		
	 sqlda="select distinct site,d from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=0 order by d,site"
  sqlda="SELECT		Site, 		convert(varchar, d, 103) as d, 		COUNT(*) AS MyCount FROM		anpossibleviolators WHERE		COALESCE(Status, '') NOT IN ('100% Permit', 'repeat') 		AND Stage = 0 group by convert(varchar, d, 103),site ORDER BY	convert(varchar, d, 103),	Site"

  openrs rsda,sqlda
	if rsda.eof and rsda.bof then
		 response.write "No data to analyse for stage 1"
	else
			%>
				<table class=maintable2><tr><td colspan=3>
			<%
			response.write "<table class=""sortable"" id=""mytable""><tr><td></td><th>Site</th><th>Date</th><th>violators</tr>"
			'response.write "<form method=post action=admin.asp?cmd=analysisstage1&cmd2=choose>"
			response.write "Select site & Date to analyze:"
			response.write "<form name=myform method=post action=analysisexcel.asp>" '<select name=sitedate>"
				do while not rsda.eof
				sqlcountv="select count(id) as mycount from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=0  and site='" & rsda("site") & "' and d>='" & cIsoDate(rsda("d")) & "' and d<='" & cisodate(rsda("d")) & " 23:59'"
				openrs rscountv,sqlcountv
				countv=rscountv("mycount")
				closers rscountv
				' response.write "<option value='" & rsda("site") & "-" & rsda("d") & "'>" & rsda("site") & "-" & rsda("d") & "</option>"
				response.write "<tr><td><input type=checkbox name=sitedate value='" & rsda("site") & "-" & rsda("d") &"'></td><td>" & rsda("site") & "</td><td>" & rsda("d") & "</td><td>" & countv & "</td></tr>"
				
				 rsda.movenext
			loop
				response.write "</table></td></tr>"
			%>
					<tr><td colspan=3 align=center>
			<input type="button" name="CheckAll" value="Check All"
onClick="checkAll(document.myform.sitedate)" class=button>
<input type="button" name="UnCheckAll" value="Uncheck All"
onClick="uncheckAll(document.myform.sitedate)" class=button></td></tr>
			
			<%
			response.write "<tr><td colspan=2 align=center><input class=button type=submit name=submit value='Output Excel'></form></td></tr>"
			'response.write "</select><input type=submit name=submit value=analyze></form>"
		
	end if


end sub
sub anexcel2
		
	 sqlda="select distinct site,d from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=1 and possibleviolator=1"
  openrs rsda,sqlda
	if rsda.eof and rsda.bof then
		 response.write "No data to analyse for stage 2"
	else
	%>
				<table class=maintable2><tr><td colspan=3>
			<%
		response.write "<form name=myform method=post action=analysisexcel2.asp>" '<select name=sitedate>"
			response.write "<table class=""sortable"" id=""mytable""><tr><th></th><th>Site</th><th>Date</th><th>Violators</th></tr>"
			'response.write "<form method=post action=admin.asp?cmd=analysisstage1&cmd2=choose>"
			response.write "Select site & Date to analyze:" '<select name=sitedate>"
				do while not rsda.eof
				sqlcountv="select count(id) as mycount from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and stage=1 and possibleviolator=1 and site='" & rsda("site") & "' and d>='" & cIsoDate(rsda("d")) & "' and d<='" & cisodate(rsda("d")) & " 23:59'"
				openrs rscountv,sqlcountv
				countv=rscountv("mycount")
				closers rscountv
				' response.write "<option value='" & rsda("site") & "-" & rsda("d") & "'>" & rsda("site") & "-" & rsda("d") & "</option>"
				response.write "<tr><td><input type=checkbox name=sitedate value='" & rsda("site") & "-" & rsda("d") &"'></td><td>" & rsda("site") & "</td><td>" & rsda("d") & "</td><td>" & countv & "</td></tr>"
				
				 rsda.movenext
			loop
			%></table></td></tr>
			<tr><td colspan=3 align=center>
			<input type="button" name="CheckAll" value="Check All"
onClick="checkAll(document.myform.sitedate)" class=button>
<input type="button" name="UnCheckAll" value="Uncheck All"
onClick="uncheckAll(document.myform.sitedate)" class=button></td></tr>
			
			<%
			response.write "<tr><td colspan=2 align=center><input class=button type=submit name=submit value='Output Excel'></form></td></tr>"
			'response.write "<tr><td colspan=2 align=center><a href=analysisexcel2.asp?sitedate=all class=button>All</a></td></tr>"
			'response.write "</select><input type=submit name=submit value=analyze></form>"
			response.write "</table>"
	end if


end sub

sub analysistobecopied

i=0
Dim objFSO
 Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
Dim objFolder 
Dim strRootFolder
 strRootFolder = "" & configsecuredir & "db\analysisfiles\"
 
' strRootFolder = "" & configsecuredir & "db\"testanalysis\"
 Set objFolder = objFSO.GetFolder(strRootFolder)
Dim objFile 
For Each objFile in objFolder.Files
		i=i+1
		
	if right(objfile.name,4)=".mdb"	then		
		uplstrcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strrootfolder& objfile.name
set uplconnection=server.createobject("ADODB.connection")
uplconnection.connectionstring=uplstrcon
uplconnection.open
		'check first 5 possibleviolators
sqlunmatched="select * from [Possible Violators]"
set rsunmatched=uplconnection.execute(sqlunmatched)
c=1
t="text"
dupe=0
do while not rsunmatched.eof and c<6
sqlck="select * from anpossibleviolators where plate=" & "'" & rsunmatched("plate") & "' and D='" & formatdint(rsunmatched("d")) & "' and T_In='" & rsunmatched("t_in") & "' and T_Out='" & rsunmatched("t_out") & "' and Duration='" & rsunmatched("duration") & "' and Site='" & rsunmatched("site")  & "'"
'response.write sqlck
openrs rsck,sqlck
'response.write "opened"
if not rsck.eof and not rsck.bof then dupe=1
closers rsck 

c=c+1
rsunmatched.movenext
loop
'response.write dupe
'dupe=1
if dupe=1  then
	 response.write "<br>"
	 response.write objfile.name & " is a duplicate <a href=delanfile.asp?filename=" & server.urlencode(objfile.name) & ">Delete</a><br>"
end if
end if
Next
response.write "you have " & i & " files to be copied"
response.write "<br><a href=analysis.asp class=button>copy</a>"
end sub

sub analysisfile(filename)

'response.write "need to process file " & request("sfile")
uplstrcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & "" & configsecuredir & "db\analysisfiles\" & filename
set uplconnection=server.createobject("ADODB.connection")
uplconnection.connectionstring=uplstrcon
uplconnection.open
t="text"

'check first 5 possibleviolators
sqlunmatched="select * from [Possible Violators]"
set rsunmatched=uplconnection.execute(sqlunmatched)
i=1
t="text"
dupe=0
do while not rsunmatched.eof and i<6
sqlck="select * from anpossibleviolators where plate=" & "'" & rsunmatched("plate") & "' and D='" & formatdint(rsunmatched("d")) & "' and T_In='" & rsunmatched("t_in") & "' and T_Out='" & rsunmatched("t_out") & "' and Duration='" & rsunmatched("duration") & "' and Site='" & rsunmatched("site")  & "'"
'response.write sqlck
openrs rsck,sqlck
'response.write "opened"
if not rsck.eof and not rsck.bof then dupe=1
closers rsck 

i=i+1
rsunmatched.movenext
loop
dupe=0
if dupe=1  then
'	 response.write filename & " is a duplicate <a href="h"></a>"
else
'response.write "moving"
'move possibleviolators
sqlunmatched="select * from [Possible Violators]"
set rsunmatched=uplconnection.execute(sqlunmatched)
do while not rsunmatched.eof
mystatus=rsunmatched("Status")
'check for violator within 14 days




sqli="insert into anpossibleviolators(Plate,D,T_In,T_Out,Duration,Site,Code,Notes,Direction,Status) values (" & tosql(rsunmatched("Plate"),t) & ",'" & formatdint(rsunmatched("D"))  & "'," &tosql(rsunmatched("T_in"),t) & "," & tosql(rsunmatched("T_out"),t) & "," & tosql(rsunmatched("duration"),t)& "," & tosql(rsunmatched("Site"),t) & "," & tosql(rsunmatched("code"),t) & ","& tosql(rsunmatched("Notes"),t) & "," & tosql(rsunmatched("Direction"),t)& "," & tosql(mystatus,t)  & ")"
'response.write sqli & "<br>"
objconn.execute sqli
rsunmatched.movenext
loop



'move umatched
sqlunmatched="select * from unmatched"
set rsunmatched=uplconnection.execute(sqlunmatched)
do while not rsunmatched.eof
sqli="insert into anunmatched(Plate,D,T,Site,Notes,Direction) values (" & tosql(rsunmatched("Plate"),t) & ",'" & formatdint(rsunmatched("D"))  & "'," & tosql(rsunmatched("T"),t) & "," & tosql(rsunmatched("Site"),t) & "," & tosql(rsunmatched("Notes"),t) & "," & tosql(rsunmatched("Direction"),t) & ")"
'response.write sqli & "<br>"
objconn.execute sqli
rsunmatched.movenext
loop
rsunmatched.close

'move nonviolators
sqlunmatched="select * from [Non Violations]"
set rsunmatched=uplconnection.execute(sqlunmatched)
do while not rsunmatched.eof
sqli="insert into annonviolations(Plate,D,T_In,T_Out,Duration,Site,Notes,Direction) values (" & tosql(rsunmatched("Plate"),t) & ",'" & formatdint(rsunmatched("D"))& "'," & tosql(rsunmatched("T_in"),t) & "," & tosql(rsunmatched("T_out"),t) & "," & tosql(rsunmatched("duration"),t)& "," & tosql(rsunmatched("Site"),t) & "," & tosql(rsunmatched("Notes"),t) & "," & tosql(rsunmatched("Direction"),t) & ")"
'response.write sqli & "<br>"
objconn.execute sqli
rsunmatched.movenext
loop
rsunmatched.close


end if

uplconnection.close
filename="" & configsecuredir & "db\analysisfiles\" & filename
set fs = CreateObject("Scripting.FileSystemObject")
   fs.DeleteFile(filename) 
end sub


sub printpcninti

sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='printpcn'"
	openrs rsm2,sqlm2
	if rsm2.eof and  rsm2.bof then 
		exit sub
	End If 
	closers rsm2

end sub
sub dvlafiletemp
'on error goto 0
'response.write "This is temporarily not available" & "<br><br>"
sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='dvlarep'"
	openrs rsm2,sqlm2
	if rsm2.eof and  rsm2.bof then 
		exit sub
	End If 
	closers rsm2
'sqldv="select * from tempviolators where registration not in(select reg from dvla) and registration not in(select dvla from dvlarep)"
'sqldv="select *  from tempviolators t where Not Exists  	(select 1 from dvlarep r Where r.Dvla = t.registration) And  [date of violation] = 	(Select Max([date of violation]) From Tempviolators t1 Where t.registration = t1.registration) And dvlasent=0 and t.registration not in(select distinct reg from dvla where reg is not null)"
''loop through and dleete blackout days

deletedbo=0

sql="select * from tempviolators"
openrs rst,sql
do while not rst.eof 
if checkifblackout(rst("site"),rst("date of violation"))= true then 
    sql= "exec delviolator @id=" & rst("id") & ",@reasondeleted='blackoutdate',@stage='dvla',@username='system'"
    'response.write sql & "<br>"
    objconn.execute sql
    deletedbo=deletedbo+1
end if

rst.movenext
loop
closers rst

if deletedbo>0 then
''send simon an emails that blackout records were deleted
ssubject="Deleted Violator records due to blackout dates"
sbody="Violator records were deleted due to blackout dates"
call sendemail("web@creativecarpark.co.uk", "simon.a@cleartoneholdings.co.uk", sSubject, sBody)
call sendemail("web@creativecarpark.co.uk", "esther@creativecarpark.co.uk", sSubject, sBody)


end if


sqlb="select batchnumber from batchnumbers order by batchnumber desc"
				Set rs = Server.CreateObject("ADODB.Recordset")
				rs.Open sqlb, objconn
				lastbatchnumber=rs("batchnumber")
				numbersent=0
				rs.close 
				sql="insert into  batchnumbers (mydate,type) values(getdate(),'" & request("type") & "')"
				objconn.execute sql
	sql="select *  from tempviolators where registration in(select registration from apermits)"
openrs rsd,sql
do while not rsd.eof
sql= "exec delviolator @id=" & rsd("id") & ",@reasondeleted='has permit',@stage='dvla',@username='system'"
'response.write sql & "<br>"
objconn.execute sql
rsd.movenext
loop
closers rsd

sql="select *  from tempviolators t left join exemptions e on t.site=e.site and t.[date of violation]>=DATEADD(dd, DATEDIFF(dd,0,e.startdate), 0) and t.[date of violation]<=e.enddate and t.registration=e.registration where e.registration is not null"
openrs rsd,sql
do while not rsd.eof
sql= "exec delviolator @id=" & rsd("id") & ",@reasondeleted='has exemption',@stage='dvla',@username='system'"
'response.write sql & "<br>"
objconn.execute sql
rsd.movenext
loop
closers rsd



fptable="Fuzzy Permits<bR><table><tr><th>PLATE</th><th>SITE</th><th>Violation Date</th><th>% </th><th>Fuzzy Match</th><th>Start</th><th>End</th></tr>"
 fetable="Fuzzy Exemptions <table><tr><th>PLATE</th><th>SITE</th><th>Violation Date</th><th>% </th><th>Fuzzy Match</th><th>Start</th><th>End</th></tr>"
 
			sqldv="select * from tempviolators where dvlasent=0 and dvlaid is null order by [date of violation]"	
openrs rsm2,sqldv
	
	if not rsm2.eof and not rsm2.bof then
 do while not rsm2.eof
 'response.write "<hr>" & rsm2("id") & "<br>"
 		if checkdvlareprec(rsm2("registration"))=true then
		'skip it -- it's waiting
		'check if plate is wiatin in dvla
			'delete teh tempviolator record
				sql="exec delviolator @id=" & rsm2("id") & ",@reasondeleted='duplicate waiting for dvla returned',@stage='dvla',@username='system'"
		'	response.write sql & "<br>"
				objconn.execute	(sql)		
			' response.write "<br>checked alrady if waiting dvla<br>"		 
		else
		'dvla was rec
					if checkifprinted14(rsm2("registration"),rsm2("date of violation"))=true then
						 sql="exec delviolator @id=" & rsm2("id") & ",@reasondeleted='printed within 14 days',@stage='dvla',@username='system'"
			'			' response.write sql & "<br>"
						 	objconn.execute	(sql)	
					'	 response.write "<br>checked alrady if printed<br>"
					elseif checkifsent14(rsm2("registration"),rsm2("date of violation"))=true then
						 sql="exec delviolator @id=" & rsm2("id") & ",@reasondeleted='sent to dvla within 14 days',@stage='dvla',@username='system'"
			'			 response.write sql & "<br>"
						 	objconn.execute	(sql)	
					'	 response.write "<br>checked alrady if printed<br>"
					
					elseif checkiftempviolator(rsm2("registration"))=true then	
							  sql="exec delviolator @id=" & rsm2("id") & ",@reasondeleted='repeat violator in temp',@stage='dvla',@username='system'"
						' response.write sql & "<br>"
						 	objconn.execute	(sql)
					else
					    
							sql="insert into dvlarep(offence,dvla,site,batch,printed,type) values('" & day(rsm2("date of violation")) &"-" & month(rsm2("date of violation")) & "-" & year(rsm2("date of violation"))  &"','" & rsm2("registration") &"','" &  rsm2("site") & "'," & lastbatchnumber +1 & ",1,'" & request("type") & "')"
  						' response.write "<font color=red>" & sql & "</font><br>"
							objconn.execute sql
							numbersent=numbersent+1
							'fptable=fptable & "<tr><th>" & rsm2("registration") & "</td></tr>"
							fptable=fptable & getfuzzypermit(rsm2("registration"),rsm2("site"),rsm2("date of violation"))
						'	fetable=fetable & getfuzzyexemption(rsm2("registration"),rsm2("site"),rsm2("date of violation"))
							
							sql="update tempviolators set dvlasent=1 where id=" & rsm2("id")
							'response.write sql & "<hr>"
								objconn.execute sql
					end if	 				
		end if
  
	
	
		
  rsm2.movenext
  loop
  
            

 
'if numbersent>0 then
''get 14 day critical

sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from dvlarep)"
openrs rssites,sqlsites
numsiteswaiting=0
siteswaiting=""
do while not  rssites.eof
numsiteswaiting=numsiteswaiting+1
siteswaiting=siteswaiting & rssites("sitecode") & ", "
rssites.movenext
loop
closers rssites
asiteswaiting=split(siteswaiting,",")
for i=lbound(asiteswaiting) to ubound(asiteswaiting)
asiteswaiting(i)=trim(asiteswaiting(i))
next


sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from dvlarep where  CONVERT(DATETIME,dvlarep.offence,105)>getdate()-14 group by site)"
openrs rssites,sqlsites
numsites=0
dim asites(200)
do while not  rssites.eof
if not elementinarray(asiteswaiting,rssites("sitecode"),0) then 
numsites=numsites+1
asites(i)=rssites("sitecode")
i=i+1
end if
rssites.movenext
loop
closers rssites
dvlamessage=numbersent & " " & numsites & " "
'get 6 day
sqlsites="select sitecode from [site information] where disabled=0 and sitecode not in(select site from dvlarep where  CONVERT(DATETIME,dvlarep.offence,105)>getdate()-6 group by site)"
openrs rssites,sqlsites
numsites=0
criticalsites=""
do while not  rssites.eof
'chec array asites
if not elementinarray(asites,rssites("sitecode"),0) and not elementinarray(asiteswaiting,rssites("sitecode"),0) then 
numsites=numsites+1
   
end if
rssites.movenext
loop
closers rssites
dvlamessage=dvlamessage & numsites & " "  & numsiteswaiting


  call smsalert("DVLA file created with " & dvlamessage & "records.",ssimonsmobile,"Creative")
  call smsalert("DVLA file created with " & dvlamessage & "records.","447813830046","Creative")
	
'end if

fptable=fptable & "</table>"
fetable=fetable & "</table>"
'response.Write "here 1"
'response.Write fptable
'adminemail="esther@creativecarpark.co.uk"
call sendhtmlemail("emails@cleartoneholdings.co.uk",adminemail,"Dvla fuzy permits",fptable & fetable)

call sendemail("simon.a@cleartoneholdings.co.uk",adminemail,"New Dvlarep file created","Checkout the deleted violators at " & configweburl & "/admin.asp?cmd=viewdelviolators&mydate=" & date())

adminemail="esther@creativecarpark.co.uk"
call sendhtmlemail("emails@cleartoneholdings.co.uk",adminemail,"Dvla fuzy permits",fptable & fetable)
adminemail="ashley.c@creativecarpark.co.uk"
call sendhtmlemail("emails@cleartoneholdings.co.uk",adminemail,"Dvla fuzy permits",fptable & fetable)
adminemail="keiu.k@creativecarpark.co.uk"
call sendhtmlemail("emails@cleartoneholdings.co.uk",adminemail,"Dvla fuzy permits",fptable & fetable)

'response.Write "here here"
'call repritn batach with batch number lastbatchnumber +1  and request("type") and update printed to 1
	'sql="update dvlarep set printed=1 where printed=0"
'	objconn.execute sql

	if request("type")="manual" then
		 response.redirect "admin.asp?cmd=dvlareport&cmd2=print&printedbatch=" & lastbatchnumber+1 
	else
			 response.redirect "admin.asp?cmd=dvlareportnew&cmd2=print&printedbatch=" & lastbatchnumber+1 
	end if	 
'	call dvlareportnew
	'response.write "need to create file here"
end if
closers rsm2	
end sub
function checkdvlareprec(registration)
sql="select dvla from dvlarep where  dvlarec is null and dvla='" & registration & "'"
openrs rsd,sql
if rsd.eof and rsd.bof then
	 checkdvlarep=false
else
		checkdvlarep=true
end if		

closers rsd
end function

function checkiftempviolator(registration)
sql="select registration from tempviolators where dvlasent=1 and registration='" & registration & "'"
openrs rsd,sql
if rsd.eof and rsd.bof then
	 checkiftempviolator=false
else
		checkiftempviolator=true
end if		

closers rsd
end function


function checkifprinted14(registration,dateofviolation)
sqlr="select * from violators where registration='" & registration & "' and datediff(day,pcnsent,'" & cisodate(dateofviolation) & "')<14"
openrs rsd,sqlr
if rsd.eof and rsd.bof then
	 checkifprinted14=false
else
		checkifprinted14=true
end if		

closers rsd
end function

function checkifsent14(registration,dateofviolation)
sqlr="select * from dvlarep where dvla='" & registration & "' and datediff(day,dategenerated,'" & cisodate(dateofviolation) & "')<14"
openrs rsd,sqlr
if rsd.eof and rsd.bof then
	 checkifsent14=false
else
		checkifsent14=true
end if		

closers rsd
end function
sub zdvlafiletemp

sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='dvlarep'"
	openrs rsm2,sqlm2
	if rsm2.eof and  rsm2.bof then 
		exit sub
	End If 
	closers rsm2
'sqldv="select * from tempviolators where registration not in(select reg from dvla) and registration not in(select dvla from dvlarep)"
sqldv="select *  from tempviolators t where Not Exists  	(select 1 from dvlarep r Where r.Dvla = t.registration) And  [date of violation] = 	(Select Max([date of violation]) From Tempviolators t1 Where t.registration = t1.registration) And dvlasent=0 and t.registration not in(select distinct reg from dvla where reg is not null)"
openrs rsm2,sqldv
	
	if not rsm2.eof and not rsm2.bof then
sqlb="select batchnumber from batchnumbers order by batchnumber desc"
				Set rs = Server.CreateObject("ADODB.Recordset")
				rs.Open sqlb, objconn
				lastbatchnumber=rs("batchnumber")
				rs.close 
				sql="insert into  batchnumbers (mydate) values(getdate())"
				objconn.execute sql

  do while not rsm2.eof
  	sql="insert into dvlarep(offence,dvla,site,batch,printed) values('" & day(rsm2("date of violation")) &"-" & month(rsm2("date of violation")) & "-" & year(rsm2("date of violation"))  &"','" & rsm2("registration") &"','" &  rsm2("site") & "'," & lastbatchnumber +1 & ",0)"
  	objconn.execute sql
		sql="update tempviolators set dvlasent=1 where id=" & rsm2("id")
		objconn.execute sql
  rsm2.movenext
  loop
	call dvlasend
end if
closers rsm2	
end sub
sub dvlawaiting

response.write "Excel of records not yet sent to dvla"
sqldv="select  registration,convert(varchar,[date of violation],103) as dateofviolation,convert(varchar,[arrival time],108) as arrivaltime,convert(varchar,[departure time],108) as departuretime,convert(varchar,[duration of stay],108) as duration,site from tempviolators t where Not Exists  	(select 1 from dvlarep r Where r.Dvla = t.registration) And  [date of violation] = 	(Select Max([date of violation]) From Tempviolators t1 Where t.registration = t1.registration) And dvlasent=0"
call outputexcel(sqldv)

end sub

sub sscr
'sqllt="select top 1 dategenerated from dvlarep order by dategenerated desc"
'openrs rslt,wqllt
'lasttime=rslt("dategenerated")
'closers rslt
sql="select d,site,count(id) as mycount from anpossibleviolators where mydatestage2>(select top 1 dategenerated from dvlarep order by dategenerated desc) and confirmedviolator=1 group by d,site"
openrs rs,sql
response.write "<table class=""sortable"" id=""mytable""><tr><th>Site</th><th>Date</th><th>Violators</th></tr>"
do while not rs.eof
	 response.write "<tr><td>" & rs("site") & "</td><td>" & rs("d") & "</td><td>" & rs("mycount") & "</td></tr>"

rs.movenext
loop
response.write "</table>"
closers rs
'' groupp by date site violartor  ?date of violation  what is violator
sql="select * from anpossibleviolators where mydatestage2>(select top 1 dategenerated from dvlarep order by dategenerated desc) and confirmedviolator=1"
call outputexcel(sql)
end sub
sub printpcnsummary
%>
<!--#include file=openconntriangle.asp-->


<%

sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='printpcn'"
 set  rsm2=objconn.execute(sqlm2)
	if  rsm2.eof and  rsm2.bof then
			response.write "no access"
			response.end
		end if
rsm2.close
set rsm2=nothing 
	
	deletedbo=0
	''loop through and dleete blackout days
sql="select * from tempviolators"
openrs rst,sql
do while not rst.eof 
if checkifblackout(rst("site"),rst("date of violation"))= true then 
    sql= "exec delviolator @id=" & rst("id") & ",@reasondeleted='blackoutdate',@stage='print',@username='system'"
    'response.write sql & "<br>"
    deletedbo=deletedbo+1
    objconn.execute sql
end if

rst.movenext
loop
closers rst
if deletedbo>0 then
''send simon an emails that blackout records were deleted
ssubject="Deleted Violator records due to blackout dates"
sbody="Violator records were deleted due to blackout dates"
call sendemail("web@creativecarpark.co.uk", "simon.a@cleartoneholdings.co.uk", sSubject, sBody)
call sendemail("web@creativecarpark.co.uk", "esther@creativecarpark.co.uk", sSubject, sBody)
b extract

end if

'if request("cmd2")="delpcnpermits" then
sql="select *  from tempviolators where registration in(select registration from apermits)"
openrs rsd,sql
do while not rsd.eof
sql= "exec delviolator @id=" & rsd("id") & ",@reasondeleted='has permit(deleted in stage print)',@stage='print',@username='system'"
'response.write sql & "<br>"
objconn.execute sql
rsd.movenext
loop
closers rsd



sql="select tempviolators.id from tempviolators left join dvla on dvla.id=tempviolators.dvlaid where dvlaid is not null and owner like '%044441%'"
openrs rsd,sql
do while not rsd.eof
sql= "exec delviolator @id=" & rsd("id") & ",@reasondeleted='04441 deleted',@stage='print',@username='system'"
'response.write sql & "<br>"
objconn.execute sql
rsd.movenext
loop
closers rsd


sql="select t.id  from tempviolators t left join exemptions e on t.site=e.site and t.[date of violation]>=DATEADD(dd, DATEDIFF(dd,0,e.startdate), 0) and t.[date of violation]<=e.enddate and t.registration=e.registration where e.registration is not null"
openrs rsd,sql
do while not rsd.eof
sql= "exec delviolator @id=" & rsd("id") & ",@reasondeleted='has exemption(deleted in stage print)',@stage='print',@username='system'"
'response.write sql & "<br>"
objconn.execute sql
rsd.movenext
loop
closers rsd

'response.write "violators with permits deleted<br>"
'end if	

if request("cmd2")="delpcndisabled" then
sql="select *  from qrytempviolatorsdvla where ([dvla status]='disabled') and (registration in(select distinct reg from dvla where reg is not null))"
'response.write sql
openrs rsd,sql
do while not rsd.eof
sql= "exec delviolator @id=" & rsd("id")& ",@reasondeleted='deleted as disabled(deleted in stage print)',@stage='print',@username='system'"
'response.write sql & "<br>"
objconn.execute sql
rsd.movenext
loop
closers rsd
'objconn.execute sql
response.write "violators with disabled deleted<br>"
end if	

if request("cmd2")="delpcnownerb" then
sqld="select *  from  qrytempviolatorsdvla where  (owner='' or owner is null or len(owner)<3) and (registration in(select distinct reg from dvla where reg is not null))"
'response.write sql
openrs rsd,sqld
do while not rsd.eof
sql= "exec delviolator @id=" & rsd("id")& ",@reasondeleted='deleted owner blank(deleted in stage print)',@stage='print',@us"
response.write sql & "<br>"
objconn.execute sql
rsd.movenext
loop
closers rsd
response.write "violators with no owner were deleted<br>"
end if	





if request("delviolator")<>"" then
i=0 
delviolators=request("delviolator")
'response.write delviolators &"<hr>"
arrdelviolators=split(delviolators,",")
for m=lbound(arrdelviolators) to ubound(arrdelviolators)
if request("delreason" & trim(arrdelviolators(m)))="" then
    response.write trim(arrdelviolators(m)) & " was not deleted because you failed to enter a reason deleted<br>"
else
    sql= "exec delviolator @id=" & trim(arrdelviolators(m)) & ",@reasondeleted='" & session("username") & " deleted(deleted in stage print) " & request("delreason" & trim(arrdelviolators(m))) & "',@stage='print',@username=" & tosql(session("username"),"text")
    'response.write sql & "<br>"
    objconn.execute sql
    i=i+1
end if    
'redirect to printpcn
next
response.write i & " records deleted<br><br>"
end if
if request("submit")="Delete Checked tickets or Mark Hold Off" then
sql="update tempviolators set holdoff=0"
objconn.execute sql

end if
if request("holdoff")<>"" then
i=0 

holdoff=request("holdoff")
'response.write delviolators &"<hr>"
arrholdoff=split(holdoff,",")
for m=lbound(arrholdoff) to ubound(arrholdoff)
sql= "update tempviolators set holdoff=1 where id=" & arrholdoff(m)
'response.write sql & "<br>"
objconn.execute sql
i=i+1
next

end if
sql="update tempviolators set holdoffwaiting=0,holdoffwaitingreason=null"
objconn.execute sql

'''markes holdoff on those with more then 3 day
'response.Write "here"
sql="update tempviolators set holdoffdate=0"
objconn.execute sql
lastsite=""
lastdate=""
i=1
''check and only allow to print for the first 3 days.
    sqlpn="select * from tempviolators where dvlaid is not null and holdoff=0 order by site,[date of violation]" '
    openrs rs,sqlpn
    do while not rs.eof 
    site=rs("site")
    mydate=rs("date of violation")
    if site="071" then ' or site="072" then 
    
        '''071  072Do not allow tickets to be sent out unless the exemptions have been uploaded for that particular date.
         '  sqlce="select * from exemptions where site='" & site & "' and DATEADD(dd, DATEDIFF(dd,0,datetime), 0)=" & tosql(cisodate(mydate),"Text")
         sqlce="select * from exemptions where site='" & site & "' and addedfrom='extracted' and DATEADD(dd, DATEDIFF(dd,0,startdate), 0)<=" & tosql(cisodate(mydate),"Text") & " and DATEADD(dd, DATEDIFF(dd,0,enddate), 0)>=" & tosql(cisodate(mydate),"Text") 
          '  response.Write sqlce & "<br>"
            openrs rsce,sqlce
            if rsce.eof and rsce.bof then
                sql="update tempviolators set holdoffwaiting=1,holdoffwaitingreason='Waiting Exemptions' where site='" & site & "' and  DATEADD(dd, DATEDIFF(dd,0,[date of violation]), 0)='" & cisodate(mydate) & "'"
         
                 objconn.execute sql  
            
            end if
            closers rsce
    
    end if
    
    
  '  response.Write "<bR>" &  site & "-" & mydate 
    if site<>lastsite then i=1
    if mydate>lastdate then i=i+1
   ' response.Write "<font color=blue>" & i & "</font>"
    if i>3 then
        ' response.write "from this date should be holding" & mydate
          sql="update tempviolators set holdoffdate=1 where site='" & site & "' and [date of violation]>='" & cisodate(mydate) & "'"
          objconn.execute sql  
    end if
    lastsite=site
    lastdate=mydate
    
    rs.movenext
    loop

i=1
''check and only allow to print those not waiting for transcription 
    sqlpn="select distinct(DATEADD(dd, DATEDIFF(dd,0,datetime), 0)) as datetime from trianglepayment where vehiclereg is null and sitecode='3526'" '
   set rs=objconnt.execute(sqlpn)
    do while not rs.eof 
    
          sql="update tempviolators set holdoffwaiting=1,holdoffwaitingreason='Waiting Transcription' where site='087' and  DATEADD(dd, DATEDIFF(dd,0,[date of violation]), 0)='" & cisodate(rs("datetime")) & "'"
         ' response.Write sql 
          objconn.execute sql  
  
    
    rs.movenext
    loop
    sqlpn="select distinct(DATEADD(dd, DATEDIFF(dd,0,datetime), 0)) as datetime from trianglepayment where vehiclereg is null and sitecode='4380'" '
   set rs=objconnt.execute(sqlpn)
    do while not rs.eof 
    
          sql="update tempviolators set holdoffwaiting=1,holdoffwaitingreason='Waiting Transcription' where site='266' and  DATEADD(dd, DATEDIFF(dd,0,[date of violation]), 0)='" & cisodate(rs("datetime")) & "'"
         ' response.Write sql 
          objconn.execute sql  
  
    
    rs.movenext
    loop

sqlpn="select distinct(DATEADD(dd, DATEDIFF(dd,0,datetime), 0)) as datetime from trianglepayment where vehiclereg is null and sitecode='4586'" '
   set rs=objconnt.execute(sqlpn)
    do while not rs.eof 
    
          sql="update tempviolators set holdoffwaiting=1,holdoffwaitingreason='Waiting Transcription' where site='271' and  DATEADD(dd, DATEDIFF(dd,0,[date of violation]), 0)='" & cisodate(rs("datetime")) & "'"
         ' response.Write sql 
          objconn.execute sql  
  
    
    rs.movenext
    loop
sql="exec cleanforprinting"
objconn.execute sql
	if 1=0 then

'sqlpn="select * from tempviolators where registration in(select distinct reg from dvla where reg is not null)"' and registration not in(select distinct registration from apermits)" ' and id>29034"
sqlpn="select * from tempviolators where  dvlaid is not null and registration not in(select distinct registration from apermits)"



'response.write sqlpn
set  rspn=objconn.execute(sqlpn)
k=0

do while not rspn.eof
sqlr="select * from violators where registration='" & rspn("registration") & "' and datediff(day,pcnsent,'" & cIsoDate(rspn("date of violation")) & "')<14"
	'response.write sqlr

	set  rsr=objconn.execute(sqlr)')<14" 
			if not rsr.eof and not rsr.bof then
 				 sql="exec delviolator @id=" & rspn("id") & ",@reasondeleted='deleted <14 days at stage printing',@stage='print'"
		'		 		response.write "delete"
					 objconn.execute sql
					k=k+1
					 'don't print901
					 else 
					 ' check if more then one occurence and del all but first
				 sqld="select count(registration) as countd from tempviolators where registration=" &tosql(rspn("registration"),"text")
					 openrs rsd,sqld
					 countd=rsd("countd")
			'	response.write countd
				 closers rsd
					 			 if countd>1 then
									 	'delete all but first	
										sqldd="select registration, [date of violation],id from tempviolators where registration = "& tosql(rspn("registration"),"text") & " and [date of violation]<(select max([date of violation]) from tempviolators where registration ="&tosql(rspn("registration"),"text")  & ")"
								'		'select max([date of violation]),max([arrival time]) from tempviolators where registration ='Y642UCF'

									'	response.write sqldd
										openrs rsdd,sqldd
										do while not rsdd.eof
									
											sql="exec delviolator @id=" & rsdd("id")& ",@reasondeleted='duplicate while printing',@stage='print'"
											'response.write "deleted " & rsdd("registration") & "<br>"
											objconn.execute sql
										k=k+1
									'	response.write k & "*"
										rsdd.movenext
										loop
										closers rsdd
										end if
										
										
										sqld="select count(registration) as countd from tempviolators where registration=" &tosql(rspn("registration"),"text")
					 openrs rsd,sqld
					 countd=rsd("countd")
			'	response.write countd
					 closers rsd
					 				 if countd>1 then
						'			 	'delete all but first	
										sqldd="select registration, [date of violation],id from tempviolators where registration = "& tosql(rspn("registration"),"text") & " and id<(select max([id]) from tempviolators where registration ="&tosql(rspn("registration"),"text")  & ")"
								'						'	response.write sqldd
										openrs rsdd,sqldd
										do while not rsdd.eof
									
											sql="exec delviolator @id=" & rsdd("id")& ",@reasondeleted='duplicate deleted(stage printing)'"
										'	'response.write sql
										'	response.write "deleted " & rsdd("registration") & "<br>"
											objconn.execute sql
										k=k+1
									'	response.write k & "*"
										rsdd.movenext
										loop
										closers rsdd
										end if
										'' need to check here for duplicates with teh same date
										
					end if
					closers rsr
				rspn.movenext	
		loop
		
end if '1=0			
	'sqlca="select count(id) as mycount from tempviolators where registration in(select registration from apermits) and dvlaid is not null"
'openrs rsca,sqlca
'response.write rsca("mycount") & " records have permits <a href=admin.asp?cmd=printpcnsummary&cmd2=delpcnpermits class=button>Delete</a><br>"
'closers rsca 

sqlca="select count(id) as mycount from qrytempviolatorsdvla where  (owner='' or owner is null or len(owner)<3) and dvlaid is not null"
'response.write sqlca
openrs rsca,sqlca

response.write rsca("mycount") & " records have no owner <a href=admin.asp?cmd=printpcnsummary&cmd2=delpcnownerb class=button>Delete</a><br>"
closers rsca 

sqlca="select count(id) as mycount from qrytempviolatorsdvla where  ([dvla status]='disabled') and dvlaid is not null"
'response.write sqlca
openrs rsca,sqlca
response.write rsca("mycount") & " records have a status of diabled <a href=admin.asp?cmd=printpcnsummary&cmd2=delpcndisabled class=button>Delete</a><br>"
closers rsca 
								 
sqlpn="select * from tempviolators where  dvlaid is not null"' and registration not in(select distinct registration from apermits)" ' and id>29034"

'[date of violation]<getdate()-28 and
set  rspn=objconn.execute(sqlpn)
i=0
response.flush
response.write "<table class=""sortable"" id=""mytable""><tr><th>del</th><th>Registration</th><th>Site<th>Date of violation</th><th>Owners Name</th><th>Owners Address</th><th>Make</th><th>Dvla Status</th><th>Hold Off</th></tr>" & vbcrlf
			response.write "<form name=myform method=post action=admin.asp?cmd=printpcnsummary&cmd2=go>"
			response.write "Check Violators to Delete:" 
			

do while not rspn.eof
                            disreason=""

			 					 			sqlqv="SELECT * from qrytempviolatorsdvla where ID =" & rspn("id")
    					 	 openrs rsqv,sqlqv
    					 'response.write sqlqv
    					 'response.write rsqv("registration")
    					 hchecked=""
    					if rspn("holdoff")= true then
    					     hchecked=" checked"
    					     
    					  end if   
    					 if rspn("holdoffdate")= true then 
    					    hchecked=" checked disabled"
    					    disreason="More then 3 dates"
    					  end if  
    					 if rspn("holdoffwaiting")= true then 
    					    hchecked=" checked disabled"
    					     disreason=rspn("holdoffwaitingreason")
    					 end if
    					  
    					 response.write "<tr><td><input type=checkbox name=delviolator value=" & rspn("id") & " onClick=""document.getElementById('delreason" & rspn("id") & "').style.display = '';""><input type=text name=delreason" & rspn("id") & " id=delreason" & rspn("id") & " style=""display:none""></td><td>" & rsqv("registration") & "</td><td>" & rsqv("site") & "</td><td>" & rsqv("date of violation") & "</td><td>" & rsqv("owner") & "</td><td>" & rsqv("address1") & "</td><td>" & rsqv("make") & "</td><td>" & rsqv("dvla status") & "</td><td><input type=checkbox name=holdoff value=" & rspn("id") & hchecked & ">" & disreason & "</td></tr>" & vbcrlf
    					 closers rsqv
    					 		 i=i+1
					 				 
									
	response.flush				

				'rsr.close	 
rspn.movenext
loop
sqlct="select site from tempviolators where dvlaid is not null and site not in(select site from tariff) group by site"
openrs rsct,sqlct
sptb=1
if not rsct.eof and not rsct.bof then
sptb=0
sptb=1
response.write "<br><br>You must add the following sites Tariff's before you can print tickets<br>"
do while not rsct.eof
 response.write rsct("site") & "<br>"
rsct.movenext
loop
response.write "<br>"
end if
closers rsct
 response.write "</table><table><tr><td colspan=2> If you marked new records to hold off you must click here before going to print<br /></td></tR><tr><td> <input type=submit name=submit value='Delete Checked tickets or Mark Hold Off' class=button></td>"
if sptb=1 then
 response.write "<td><a class=button href=confirmimages.asp>Confirm Tickets</a></td>"
' response.write "<td><a class=button href=printnewpcn.asp>Print Tickets</a></td>"
 'response.write "<td><a class=button href=printnewpcn.asp>Print Tickets</a></td>"
end if
response.write "</tr></table></form><br>"
 
'response.write k & " records were deleted as they were sent within 14 days or duplicate registrations<br>"


sqlc="select count(registration) as mycount from tempviolators where dvlaid is not null and holdoff=0" ' and registration not in(select distinct registration from apermits)"
openrs rsc,sqlc
i=rsc("mycount")
closers rsc
response.write i & " records to be printed"
rspn.close


'else
'delete adn redirect to printnewpcn

'end if

end sub
sub delreissue(reissueid,reason)
sql="exec delreissue @reissueid=" & reissueid & ",@reasondeleted='" & reason & "'"
objconn.execute sql


end sub
sub delreissue(reissueid,reason)
sql="exec delreissue @reissueid=" & reissueid & ",@reasondeleted='" & reason & "'"
objconn.execute sql


end sub

sub printreissuesummary




if request("delreissues")<>"" then
i=0 
delreissues=request("delreissues")
'response.write delviolators &"<hr>"
arrdelreissues=split(delreissues,",")
for m=lbound(arrdelreissues) to ubound(arrdelreissues)
sql= "exec delreissue @reissueid=" & trim(arrdelreissues(m)) & ",@reasondeleted='" & session("userid") & "deleted'"
'response.write sql & "<br>"
objconn.execute sql
i=i+1
'redirect to printpcn
next
response.write i & " records deleted<br><br>"
end if


''old code redone with sp

if 1=1 then
''del those that are paid , cancelled or permits
sql="select reissues.id as reissueid,violatorid,violators.pcn+violators.site as pcn,violators.registration from reissues left join violators on reissues.violatorid=violators.id where reissuedate is null"
openrs rs,sql
'delreissue(reissueid,reason)
do while not rs.eof
' check for permits
sqlck="select * from apermits where registration='" & rs("registration") & "'"
openrs rsck,sqlck
if not rsck.eof and not rsck.bof then call delreissue(rs("reissueid"),"on permit list")

closers rsck
sqlck="select * from cancelled where pcn='" & rs("pcn") & "'"
openrs rsck,sqlck
if not rsck.eof and not rsck.bof then call delreissue(rs("reissueid"),"pcn cancelled")

closers rsck
sqlck="select * from payments where pcn='" & rs("pcn") & "'"
openrs rsck,sqlck
if not rsck.eof and not rsck.bof then call delreissue(rs("reissueid"),"pcn paid")

closers rsck
rs.movenext
loop

closers rs

sqlpn="select reissues.id as reissueid,violatorid,violators.pcn+violators.site as pcn,violators.registration from reissues left join violators on reissues.violatorid=violators.id where reissuedate is null"
'sqlpn="select reissues.id as reissueid,violatorid,violators.pcn+violators.site as pcn,violators.registration from reissues left join violators on reissues.violatorid=violators.id where reissuedate is null"
end if
sqlpn="exec reissuesummary"
set  rspn=objconn.execute(sqlpn)
i=0




response.write "<table class=""sortable"" id=""mytable""><tr><th>del</th><th>Registration</th><th>Site<th>Date of violation</th><th>Owners Name</th><th>Owners Address</th><th>Make</th><th>Dvla Status</th></tr>" & vbcrlf
			response.write "<form method=post action=admin.asp?cmd=printreissuesummary&cmd2=go>"
			response.write "Check Reissues to Delete:" 
			

do while not rspn.eof
	 			' response.write rspn("reissueid")
			 				'	 			sqlqv="select violators.*,dvla.*,reissues.* from reissues left join violators on reissues.violatorid=violators.id left join dvla on reissues.dvlaid=dvla.id where reissues.id=" & rspn("reissueid")
    		'		response.write sqlqv
						'	 	 openrs rsqv,sqlqv
    					' response.write rsqv("registration")
    					 'response.Write rspn("registration")
    					 response.write "<tr><td><input type=checkbox name=delreissues value=" & rspn("reissueid") & "></td><td>" & rspn("registration") & "</td><td>" & rspn("site") & "</td><td>" & rspn("date of violation") & "</td><td>" & rspn("owner") & "</td><td>" & rspn("address1") & "</td><td>" & rspn("make") & "</td><td>" & rspn("dvla status") & "</td></tr>" & vbcrlf
    					' closers rsqv
    					 		 i=i+1
					 				 
									
					

				'rsr.close	 
rspn.movenext
loop
response.Write "here"

 response.write "</table><table><tr><td><input type=submit name=submit value='Delete Checked tickets' class=button></td>"

 response.write "<td><a class=button href=printreissue.asp>Print Tickets</a></td>"

response.write "</tr></table></form><br>"
 
'response.write k & " records were deleted as they were sent within 14 days or duplicate registrations<br>"


sqlc="select count(id) as mycount from reissues where reissuedate is  null"

openrs rsc,sqlc
i=rsc("mycount")
closers rsc
response.write i & " records to be printed"
rspn.close


'else
'delete adn redirect to printnewpcn

'end if

end sub
sub printtempviolators
sql="select registration,convert(varchar,[date of violation],103),convert(varchar,[arrival time],108),convert(varchar,[departure time],108),convert(varchar,[duration of stay],108),site from tempviolators"
call outputexcel(sql)

end sub

sub violatorupload
'on error resume next
filename=configsecuredir & "data\" & request("sfile")
'filename="" & configsecuredir & "db\"sampleviolators2.csv"
Const ForReading = 1, ForWriting = 2, ForAppending = 3
Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

' Create a filesystem object
Dim FSO
set FSO = server.createObject("Scripting.FileSystemObject")

' Map the logical path to the physical system path


if FSO.FileExists(Filename) Then

	' Get a handle to the file
	Dim file	
	set file = FSO.GetFile(Filename)

	' Get some info about the file
	Dim FileSize
	FileSize = file.Size

	
	' Open the file
	Dim TextStream
	Set TextStream = file.OpenAsTextStream(ForReading, _
                                               TristateUseDefault)

	' Read the file line by line
	Do While Not TextStream.AtEndOfStream
		Dim Line
		Line = TextStream.readline
'	on error resume next
		' Do something with "Line"
		arr=split(line,",")
		sql="insert into violators(registration,[date of violation],site,[arrival time],[departure time],[duration of stay],pcn,pcnsent,ticketprice,reducedamount) values('" & arr(0) & "','" & cisodate(arr(1)) & "','" & arr(2) & "','" & arr(3) & "','" & arr(4) & "','" & arr(5) & "','" & arr(6) & "','" & cisodate(arr(7)) & "'," & arr(8) & "," & arr(9) & ")"
		sql="exec insertnewviolator @registration='" & arr(0) & "',@dateofviolation='" & cisodate(arr(1)) & "',@site='" & arr(2) & "',@arrivaltime='" & arr(3) & "',@departuretime='" & arr(4) & "',@durationofstay='" & arr(5) & "',@pcn='" & arr(6) & "',@pcnsent='" & cisodate(arr(7)) & "',@ticketprice=" & arr(8) & ",@reducedamount=" & arr(9)  
		
		'sql="insert into reminders(registration,[date of violation],site,[arrival time],[departure time],[duration of stay],pcn,pcnsent,reminder) values('" & arr(0) & "','" & cisodate(arr(1)) & "','" & arr(2) & "','" & arr(3) & "','" & arr(4) & "','" & arr(5) & "','" & arr(6) & "','" & cisodate(arr(7)) & "','" & cisodate(arr(8)) & "'" & ")"
		
		
		'Line = Line & vbCRLF
	
		Response.write sql & "<br><br>"
		objconn.execute sql 
	Loop
response.write "done"

	
	Set TextStream = nothing
	
Else

	Response.Write "<h3><i><font color=red> File " & Filename &_
                       " does not exist</font></i></h3>"

End If


end sub
sub tempviolatorupload
'on error resume next
filename=configsecuredir & "db\tempviolators.csv"
'filename= configsecuredir & "db\sampleviolators2.csv"
Const ForReading = 1, ForWriting = 2, ForAppending = 3
Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

' Create a filesystem object
Dim FSO
set FSO = server.createObject("Scripting.FileSystemObject")

' Map the logical path to the physical system path


if FSO.FileExists(Filename) Then

	' Get a handle to the file
	Dim file	
	set file = FSO.GetFile(Filename)

	' Get some info about the file
	Dim FileSize
	FileSize = file.Size

	
	' Open the file
	Dim TextStream
	Set TextStream = file.OpenAsTextStream(ForReading, _
                                               TristateUseDefault)

	' Read the file line by line
	Do While Not TextStream.AtEndOfStream
		Dim Line
		Line = TextStream.readline
'	on error resume next
		' Do something with "Line"
		arr=split(line,",")
		sql="insert into tempviolators(registration,[date of violation],site,[arrival time],[departure time],[duration of stay]) values('" & arr(0) & "','" & cisodate(arr(1)) & "','" & arr(5) & "','" & arr(2) & "','" & arr(3) & "','" &arr(4) & "')"
		'sql="exec insertnewviolator @registration='" & arr(0) & "',@dateofviolation='" & cisodate(arr(1)) & "',@site='" & arr(2) & "',@arrivaltime='" & arr(3) & "',@departuretime='" & arr(4) & "',@durationofstay='" & arr(5) & "',@pcn='" & arr(6) & "',@pcnsent='" & cisodate(arr(7)) & "',@ticketprice=" & arr(8) & ",@reducedamount=" & arr(9)  
		
		'sql="insert into reminders(registration,[date of violation],site,[arrival time],[departure time],[duration of stay],pcn,pcnsent,reminder) values('" & arr(0) & "','" & cisodate(arr(1)) & "','" & arr(2) & "','" & arr(3) & "','" & arr(4) & "','" & arr(5) & "','" & arr(6) & "','" & cisodate(arr(7)) & "','" & cisodate(arr(8)) & "'" & ")"
		
		
		'Line = Line & vbCRLF
	
		Response.write sql & "<br><br>"
	objconn.execute sql 
	Loop
response.write "done"

	
	Set TextStream = nothing
	
Else

	Response.Write "<h3><i><font color=red> File " & Filename &_
                       " does not exist</font></i></h3>"

End If


end sub
sub reissueupload
'on error resume next
filename="" & configsecuredir & "db\reissues.csv"
'filename="" & configsecuredir & "db\sampleviolators2.csv"
Const ForReading = 1, ForWriting = 2, ForAppending = 3
Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

' Create a filesystem object
Dim FSO
set FSO = server.createObject("Scripting.FileSystemObject")

' Map the logical path to the physical system path


if FSO.FileExists(Filename) Then

	' Get a handle to the file
	Dim file	
	set file = FSO.GetFile(Filename)

	' Get some info about the file
	Dim FileSize
	FileSize = file.Size

	
	' Open the file
	Dim TextStream
	Set TextStream = file.OpenAsTextStream(ForReading, _
                                               TristateUseDefault)

	' Read the file line by line
	Do While Not TextStream.AtEndOfStream
		Dim Line
		Line = TextStream.readline
'	on error resume next
		' Do something with "Line"
		arr=split(line,",")
		sql="insert into re_issue_data(reg,owner,address1,town,postcode,[dvla status],re_issued,date_re_issued,pcn,pcn_sent,reminder_sent,cancelled,paid,date_received) values('" & arr(0) & "'," & tosql(arr(1),"text") & "," & tosql(arr(2),"text") & "," & tosql(arr(3),"text") & ",'" & arr(4)& "','" & arr(5)  &  "','"& cisodate(arr(6))&  "','"& cisodate(arr(7))& "','"  & arr(8) & "','" & cisodate(arr(9)) &  "','"& arr(10) &  "','"& cisodate(arr(11))&  "','"& cisodate(arr(12))&  "','"& cisodate(arr(13)) & "')"
		'sql="exec insertnewviolator @registration='" & arr(0) & "',@dateofviolation='" & cisodate(arr(1)) & "',@site='" & arr(2) & "',@arrivaltime='" & arr(3) & "',@departuretime='" & arr(4) & "',@durationofstay='" & arr(5) & "',@pcn='" & arr(6) & "',@pcnsent='" & cisodate(arr(7)) & "',@ticketprice=" & arr(8) & ",@reducedamount=" & arr(9)  
		
		'sql="insert into reminders(registration,[date of violation],site,[arrival time],[departure time],[duration of stay],pcn,pcnsent,reminder) values('" & arr(0) & "','" & cisodate(arr(1)) & "','" & arr(2) & "','" & arr(3) & "','" & arr(4) & "','" & arr(5) & "','" & arr(6) & "','" & cisodate(arr(7)) & "','" & cisodate(arr(8)) & "'" & ")"
		
		
		'Line = Line & vbCRLF
	
		Response.write sql & "<br><br>"
	objconn.execute sql 
	Loop
response.write "done"

	
	Set TextStream = nothing
	
Else

	Response.Write "<h3><i><font color=red> File " & Filename &_
                       " does not exist</font></i></h3>"

End If


end sub
sub remindersupload
'on error resume next
filename="" & configsecuredir & "db\reminders.csv"
'filename="" & configsecuredir & "db\sampleviolators2.csv"
Const ForReading = 1, ForWriting = 2, ForAppending = 3
Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

' Create a filesystem object
Dim FSO
set FSO = server.createObject("Scripting.FileSystemObject")

' Map the logical path to the physical system path


if FSO.FileExists(Filename) Then

	' Get a handle to the file
	Dim file	
	set file = FSO.GetFile(Filename)

	' Get some info about the file
	Dim FileSize
	FileSize = file.Size

	
	' Open the file
	Dim TextStream
	Set TextStream = file.OpenAsTextStream(ForReading, _
                                               TristateUseDefault)

	' Read the file line by line
	Do While Not TextStream.AtEndOfStream
		Dim Line
		Line = TextStream.readline
'	on error resume next
		' Do something with "Line"
		arr=split(line,",")
		sql="insert into reminders(Registration,[Date of Violation],Site,[Arrival Time],[Departure Time],	[Duration of Stay],PCN,	PCNSENT,REMINDER,[Written-off],Solictors ) values('" & arr(0) & "'," & tosql(arr(1),"date") & "," & tosql(arr(2),"text") & "," & tosql(arr(3),"text") & ",'" & arr(4)& "','" & arr(5)  &  "','"& arr(6)&  "','"& cisodate(arr(7))& "','"  & cisodate(arr(8)) & "','" &arr(9)& "','" &arr(10) & "')"
		'sql="exec insertnewviolator @registration='" & arr(0) & "',@dateofviolation='" & cisodate(arr(1)) & "',@site='" & arr(2) & "',@arrivaltime='" & arr(3) & "',@departuretime='" & arr(4) & "',@durationofstay='" & arr(5) & "',@pcn='" & arr(6) & "',@pcnsent='" & cisodate(arr(7)) & "',@ticketprice=" & arr(8) & ",@reducedamount=" & arr(9)  
		
		'sql="insert into reminders(registration,[date of violation],site,[arrival time],[departure time],[duration of stay],pcn,pcnsent,reminder) values('" & arr(0) & "','" & cisodate(arr(1)) & "','" & arr(2) & "','" & arr(3) & "','" & arr(4) & "','" & arr(5) & "','" & arr(6) & "','" & cisodate(arr(7)) & "','" & cisodate(arr(8)) & "'" & ")"
		
		
		'Line = Line & vbCRLF
	
		Response.write sql & "<br><br>"
	objconn.execute sql 
	Loop
response.write "done"

	
	Set TextStream = nothing
	
Else

	Response.Write "<h3><i><font color=red> File " & Filename &_
                       " does not exist</font></i></h3>"

End If


end sub
sub permitsynch
on error resume next
uplstrcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & "" & configsecuredir & "db\permitdb\permits.mdb"
set uplconnection=server.createobject("ADODB.connection")
uplconnection.connectionstring=uplstrcon
uplconnection.open
t="text"
sql="select * from permits"
set rs=uplconnection.execute (sql)
do while not rs.eof
if rs("temp")=true then
	 temp=1
else
		temp=0
end if
sqlu="exec spsynchpermit @registration=" & tosql(rs("registration"),t)& ",@valid_date=" & tosql(cisodate(rs("valid_date")),t) & ",@temp=" & temp & ",@expiry_date=" & tosql(cisodate(rs("expiry_date")),t) & ",@site=" & tosql(rs("site"),t) & ",@other_info=" & tosql(rs("other_info"),t) & ",@file_page=" & tosql(rs("file_page"),t) & ",@created=" & tosql(cisodate(rs("created")),t)
response.write sqlu
objconn.execute sqlu
rs.movenext
loop
response.write "Permits synchronized correctly"
end sub

sub anmonthlyex
if request("cmd2")="" then
Response.Write "<table class=maintable2><tr><td><form method=post action=admin.asp?cmd=anmonthlyex><input type=hidden name=cmd2 value=show>"
Response.Write "Select a Month: </td><td><select name=month>"
		for i=1 to 12
		Response.Write "<option value=" & i & ">" & monthname(i) & "</option>"
		next
		Response.Write "</select>" %>
		</td></tr>
		<tr><td>
		
		<%
		Response.Write "Select a Year: </td><td><select name=year>"
		ly=year(date)-2
	
		for n=ly to year(date) 
		Response.Write  "<option value=" & n & ">" & n & "</option>"
		next
		Response.Write "</select></td></tr>" 
		
		Response.Write  "<tr><td colspan=2 align=center><input type=submit name=submit value='Go' class=button></td></tr></table></form>"
%>
<hr>
OR
<table class=maintable2>
<tr>
<td>


<form method=post action=admin.asp?cmd=anmonthlyex2><input type=hidden name=cmd2 value="print">
<b>Date</b></td>
</tr>
<tr><td>
From: </td><td><input type=text name=fromdate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('fromdate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To:</td><td> <input type=text name=todate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('todate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>

	<tr><td colspan=2 align=center>
<input type=submit value=submit name=sumbit class=button>
</td>
</tr>
</table>
</form>

<%
else
		
		totale=0
		total1=0
		total2=0
		totalred=0
		totalcrossed=0
		waitingdata=0
		count2=0
		count1=0
		countr=0
		counte=0
		mymonth=request("month")
		myear=request("year")
		daysinm=getdaysinmonth(mymonth,myear)
	'	daysinm=2
		if len(mymonth)<2 then mymonth="0" & mymonth
		mytable= "<b>Data Analysis Overview " & monthname(mymonth) & " " &  myear &  "</b>"
		response.write "<br>Check to cancel site and date<br>"
mytable=mytable & "<table border=0 cellpadding=0 cellspacing=0>"
mytable=mytable & "<tr><td>site <br>code</td><!--<td>Name of Site</td>--><td>Notes</td>"
for i=1 to daysinm
mytable=mytable & "<td><b>" & i & "</b></td>"
next

mytable=mytable & "</tr>"
response.write mytable

response.write "<form method=post action=admin.asp?cmd=cancelanal>"
sqlsite="select * from siteinformationanal order by sitecode"
openrs rssite,sqlsite
do while not rssite.eof
snotes=rssite("notes")
'response.write "<hr>" & snotes

	 sqlcc="select * from cancelanal where alldays=1 and site=" & rssite("sitecode")
	 openrs rscc,sqlcc
	 noshowrow=0
	 if not rscc.eof and not rscc.bof then noshowrow=1
	 closers rscc
	 if noshowrow=0 then
	 mytable=mytable & "<tr><td><b>'" & rssite("sitecode") & "</b></td><!--<Td>" & rssite("name of site") & "</td>--><td>" &snotes & "</td>"
	 response.write "<tr><td><b>'" & rssite("sitecode") & "<input type=checkbox name=site value=" & rssite("sitecode") & "></b></td><!--<td>" & rssite("name of site") & "</td>-->"
	 
	  response.write "<td><textarea name=notes-" & rssite("sitecode") & ">" & snotes  & "</textarea></td>"
  for myday=1 to daysinm
	if len(myday)<2 then myday="0" & myday
	sqlwhere= "site='" & rssite("sitecode") & "' and d >="
	sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
	sqlwhere=sqlwhere & " and d<=cast('" & myear & mymonth & myday & " 23:59' as datetime)"
		sites="'" & rssite("sitecode") & "'"
		df="'" & myear & mymonth & myday & " 00:00'"
		dt="'" & myear & mymonth & myday & " 23:59'"
		
	
			sqlcc="select * from cancelanal where date='" & myear & mymonth & myday & "' and site='" & rssite("sitecode") & "'"
		'	response.write sqlcc
			openrs rscc,sqlcc
			if  rscc.eof and rscc.bof then
				acan=0
			else
				acan=1
		end if 
			closers rscc
			
			sqlc="exec spanal @site=" & sites & ",@df=" & df & ",@dt=" & dt
		
		'	response.Write sqlc
		
	
		
			
			
		openrs rsc,sqlc
		countpv=rsc("countpv")
		countred=rsc("countred")
		countstage1=rsc("countstage1")
		countstage2=rsc("countstage2")
		closers rsc
			
		if acan=1 then 
			 mytable=mytable & "<td bgcolor=gray>&nbsp;</td>"
			 totalcrossed=totalcrossed+1
		else
    		if countstage2>0 then
    			  mytable=mytable & "<td bgcolor=#0000ff><font color=#ffffff>" & countstage2 & "</font></td>"
						ps1="<td bgcolor=#0000ff><font color=#ffffff>" & countstage2 
						ps2= "</font></td>"
    				total2=total2+ countstage2
						count2=count2+1
    		elseif countstage1>0 then 
    					 mytable=mytable & "<td bgcolor=#339966><font color=#ffffff>" & countstage1 & "</font></td>"
    					 ps1="<td bgcolor=#339966><font color=#ffffff>" & countstage1
							
							 total1=total1+ countstage1
							 count1=count1+1
				elseif countred>0 then 
    					 mytable=mytable & "<td bgcolor=red><font color=#ffffff>" & countred & "</font></td>"
    					 ps1="<td bgcolor=red><font color=#ffffff>" & countred
							
							 totalred=totalred+ countred
							 countr=countr+1
    		else
    				if countpv>0 then
    				mytable=mytable & "<td bgcolor=#ffff00>" & countpv & "</td>"
    				totale=totale+ countpv
						counte=counte+1
						ps1="<td bgcolor=#ffff00>" & countpv 
    				else
    						mytable=mytable & "<td>&nbsp;</td>"
							
							mydate=dateserial(myear,mymonth,myday)
							if cdate(mydate)<date() then
								  waitingdata=waitingdata+1
								'	else
									'		response.write "<hr>didn't put " & mydate & "<hr>"
											 
									end if
								ps1="<td>"
    				end if
    		end if 	
		end if
	response.write ps1 & "<br><input type=checkbox value=" & rssite("sitecode") & "_" & myear & mymonth & myday & " name=cancelled"
	if acan=1 then response.write " checked"
	response.write "></font></td>"
		 
	'	else
		'		mytable=mytable & "<td bgcolor=#0000ff>"
		'end if
	'	if countpv>0 then
			'  mytable=mytable & "<p bgcolor=#ffff00>" & countpv & "</p><br>"
			'	totale=totale+countpv
		'	end if
		'	if countstage1>0 then
		'	mytable=mytable & "<span bgcolor=#339966>" & countstage1 & "</span><br>"
		'	total1=total1+countstage1
		'	end if
'			mytable=mytable & "stage2 checked:" & countstage2 & "<br><br>"
		'mytable=mytable & "backlog:" & countpv & "<br>"
		'mytable=mytable &  "</td>"
		
  '  dt = dateadd("D", 1, dt)
  next
  mytable=mytable & "</tr>"
	response.write "</tr>"
response.flush
end if
rssite.movenext

loop
closers rssite

mytable=mytable & "<tr bgcolor=#ffff00><td colspan=2>Extracted</td><td> " & totale & "</td><td>" & counte & "</td></tr>"
mytable=mytable & "<tr bgcolor=red><td colspan=2 ><font color=#ffffff>Extracted and passed through 1st stage with no pv</td><td><font color=#ffffff> " & totalred & "</font></td><td><font color=#ffffff> " & countr & "</font></td></tr>"
mytable=mytable & "<tr bgcolor=#339966><td colspan=2 ><font color=#ffffff>1st stage completed</font></td><td><font color=#ffffff> " & total1 & "</font></td><td><font color=#ffffff> " & count1 & "</font></td></tr>"
mytable=mytable & "<tr bgcolor=#0000ff><td colspan=2 ><font color=#ffffff>2nd stage completed </td><td><font color=#ffffff>" & total2 & "</font></td><td><font color=#ffffff> " & count2 & "</font></td></tr>"
mytable=mytable & "<tr bgcolor=#000000><td colspan=2 ><font color=#ffffff>Total Crossed off </font></td><td><font color=#ffffff>" & totalcrossed & "</font></td></tr>"
'if totalcrossed>0 then
'newtotal=totale+totalred+total1+total2
'mytable=mytable & "<tr><td colspan=4 bgcolor=#000000><font color=#ffffff>Percentage Crossed off: " & totalcrossed/newtotal & "</font></td></tr>"
'end if
mytable=mytable & "<tr bgcolor=#000000><td colspan=2 ><font color=#ffffff>Waiting Data: </font></td><td><font color=#ffffff>" & waitingdata & "</font></td></tr>"
mytable=mytable & "</table>"
response.write "</table>"
call writetoexcel(mytable)
response.write "<br><br><input type=submit name=submit value='Add Notes Cacel Analysis for checked sites/dates'></form>"

end if
end sub
sub anmonthlyex2
	
		totale=0
		total1=0
		total2=0
		totalred=0
		totalcrossed=0
		waitingdata=0
		count2=0
		count1=0
		countr=0
		counte=0
		'mymonth=request("month")
		'myear=request("year")
		fromdate=cdate(request("fromdate"))
		todate=cdate(request("todate"))
		'daysinm=getdaysinmonth(mymonth,myear)
	'	daysinm=2
		'if len(mymonth)<2 then mymonth="0" & mymonth
		mytable= "<b>Data Analysis Overview From:" & fromdate & " To: " &  todate &  "</b>"
		response.write "<br>Check to cancel site and date<br>"
mytable=mytable & "<table border=1 cellpadding=0 cellspacing=0>"
mytable=mytable & "<tr><td>site <br>code</td><!--<td>Name of Site</td>--><td>Notes</td>"
dfrom=fromdate
do while dfrom<=todate
mytable=mytable & "<td><b>" & dfrom & "</b></td>"
dfrom=dateadd("d",1,dfrom)
loop

mytable=mytable & "</tr>"
response.write mytable

response.write "<form method=post action=admin.asp?cmd=cancelanal>"
sqlsite="select * from siteinformationanal order by sitecode"
openrs rssite,sqlsite
do while not rssite.eof
snotes=rssite("notes")
'response.write "<hr>" & snotes
	 sqlcc="select * from cancelanal where alldays=1 and site=" & rssite("sitecode")
	 openrs rscc,sqlcc
	 noshowrow=0
	 if not rscc.eof and not rscc.bof then noshowrow=1
	 closers rscc
	 if noshowrow=0 then
	 mytable=mytable & "<tr><td><b>'" & rssite("sitecode") & "</b></td><!--<Td>" & rssite("name of site") & "</td>--><td>" &snotes & "</td>"
	 response.write "<tr><td><b>'" & rssite("sitecode") & "<input type=checkbox name=site value=" & rssite("sitecode") & "></b></td><!--<td>" & rssite("name of site") & "</td>-->"
	 
	  response.write "<td><textarea name=notes-" & rssite("sitecode") & ">" & snotes  & "</textarea></td>"

dfrom=fromdate
do while dfrom<=todate
	 			 myear=year(dfrom)
				 mymonth=month(dfrom)
				 myday=day(dfrom)

	if len(myday)<2 then myday="0" & myday
	if len(mymonth)<2 then mymonth="0" & mymonth
	sqlwhere= "site='" & rssite("sitecode") & "' and d >="
	sqlwhere=sqlwhere & "cast('" & myear & mymonth & myday & " 00:00' as datetime)"
	sqlwhere=sqlwhere & " and d<=cast('" & myear & mymonth & myday & " 23:59' as datetime)"
		sites="'" & rssite("sitecode") & "'"
		df="'" & myear & mymonth & myday & " 00:00'"
		dt="'" & myear & mymonth & myday & " 23:59'"
		
	
			sqlcc="select * from cancelanal where date='" & myear & mymonth & myday & "' and site='" & rssite("sitecode") & "'"
			'response.write sqlcc
			openrs rscc,sqlcc
			if  rscc.eof and rscc.bof then
				acan=0
			else
				acan=1
		end if 
			closers rscc
			
			sqlc="exec spanal @site=" & sites & ",@df=" & df & ",@dt=" & dt
			
			'	sqlc="select count(id) as mycount from anpossibleviolators where " & sqlwhere
	'response.write sqlc
	'	mytable=mytable & sqlc
'	response.write sqlc
		openrs rsc,sqlc
		countpv=rsc("countpv")
		countred=rsc("countred")
		countstage1=rsc("countstage1")
		countstage2=rsc("countstage2")
		closers rsc
			'			sqlc="select count(id) as mycount from anpossibleviolators where possibleviolator=0 and stage=1 and " & sqlwhere
	'response.write sqlc
	'	mytable=mytable & sqlc
		'openrs rsc,sqlc
	'countred=rsc("mycount")
	'closers rsc	
	'countred=0
	
				'sqlc="select count(id) as mycount from anpossibleviolators where possibleviolator=1 and " & sqlwhere
	'	mytable=mytable & sqlc
		'openrs rsc,sqlc
	'countstage1=rsc("mycount")
	'closers rsc	
	'	sqlc="select count(id) as mycount from anpossibleviolators where confirmedviolator=1 and " & sqlwhere
		'mytable=mytable & sqlc
	'	openrs rsc,sqlc
	'	countstage2=rsc("mycount")
	'	closers rsc
		if acan=1 then 
			 mytable=mytable & "<td bgcolor=gray>&nbsp;</td>"
			 totalcrossed=totalcrossed+1
		else
    		if countstage2>0 then
    			  mytable=mytable & "<td bgcolor=#0000ff><font color=#ffffff>" & countstage2 & "</font></td>"
						ps1="<td bgcolor=#0000ff><font color=#ffffff>" & countstage2 
						ps2= "</font></td>"
    				total2=total2+ countstage2
						count2=count2+1
    		elseif countstage1>0 then 
    					 mytable=mytable & "<td bgcolor=#339966><font color=#ffffff>" & countstage1 & "</font></td>"
    					 ps1="<td bgcolor=#339966><font color=#ffffff>" & countstage1
							
							 total1=total1+ countstage1
							 count1=count1+1
				elseif countred>0 then 
    					 mytable=mytable & "<td bgcolor=red><font color=#ffffff>" & countred & "</font></td>"
    					 ps1="<td bgcolor=red><font color=#ffffff>" & countred
							
							 totalred=totalred+ countred
							 countr=countr+1
    		else
    				if countpv>0 then
    				mytable=mytable & "<td bgcolor=#ffff00>" & countpv & "</td>"
    				totale=totale+ countpv
						counte=counte+1
						ps1="<td bgcolor=#ffff00>" & countpv 
    				else
    						mytable=mytable & "<td>&nbsp;</td>"
							
							mydate=dateserial(myear,mymonth,myday)
							if cdate(mydate)<date() then
								  waitingdata=waitingdata+1
								'	else
									'		response.write "<hr>didn't put " & mydate & "<hr>"
											 
									end if
								ps1="<td>"
    				end if
    		end if 	
		end if
	response.write ps1 & "<br><input type=checkbox value=" & rssite("sitecode") & "_" & myear & mymonth & myday & " name=cancelled"
	if acan=1 then response.write " checked"
	response.write "></font></td>"
		 
	'	else
		'		mytable=mytable & "<td bgcolor=#0000ff>"
		'end if
	'	if countpv>0 then
			'  mytable=mytable & "<p bgcolor=#ffff00>" & countpv & "</p><br>"
			'	totale=totale+countpv
		'	end if
		'	if countstage1>0 then
		'	mytable=mytable & "<span bgcolor=#339966>" & countstage1 & "</span><br>"
		'	total1=total1+countstage1
		'	end if
'			mytable=mytable & "stage2 checked:" & countstage2 & "<br><br>"
		'mytable=mytable & "backlog:" & countpv & "<br>"
		'mytable=mytable &  "</td>"
		
  '  dt = dateadd("D", 1, dt)
dfrom=dateadd("d",1,dfrom)
loop
  mytable=mytable & "</tr>"
	response.write "</tr>"
end if
response.flush
rssite.movenext

loop
closers rssite

mytable=mytable & "<tr bgcolor=#ffff00><td colspan=2>Extracted</td><td> " & totale & "</td><td>" & counte & "</td></tr>"
mytable=mytable & "<tr bgcolor=red><td colspan=2 ><font color=#ffffff>Extracted and passed through 1st stage with no pv</td><td><font color=#ffffff> " & totalred & "</font></td><td><font color=#ffffff> " & countr & "</font></td></tr>"
mytable=mytable & "<tr bgcolor=#339966><td colspan=2 ><font color=#ffffff>1st stage completed</font></td><td><font color=#ffffff> " & total1 & "</font></td><td><font color=#ffffff> " & count1 & "</font></td></tr>"
mytable=mytable & "<tr bgcolor=#0000ff><td colspan=2 ><font color=#ffffff>2nd stage completed </td><td><font color=#ffffff>" & total2 & "</font></td><td><font color=#ffffff> " & count2 & "</font></td></tr>"
mytable=mytable & "<tr bgcolor=#000000><td colspan=2 ><font color=#ffffff>Total Crossed off </font></td><td><font color=#ffffff>" & totalcrossed & "</font></td></tr>"
'if totalcrossed>0 then
'newtotal=totale+totalred+total1+total2
'mytable=mytable & "<tr><td colspan=4 bgcolor=#000000><font color=#ffffff>Percentage Crossed off: " & totalcrossed/newtotal & "</font></td></tr>"
'end if
mytable=mytable & "<tr bgcolor=#000000><td colspan=2 ><font color=#ffffff>Waiting Data: </font></td><td><font color=#ffffff>" & waitingdata & "</font></td></tr>"
mytable=mytable & "</table>"
response.write "</table>"
call writetoexcel(mytable)
response.write "<br><br><input type=submit name=submit value='Add Notes Cacel Analysis for checked sites/dates'></form>"



end sub
sub cancelanal
cancelled=request("cancelled")
acancelled=split(cancelled,",")
for i=lbound(acancelled) to ubound(acancelled)
'response.write acancelled(i) & "<br>"
a2=split(acancelled(i),"_")
sql="insert into cancelanal(site,date) values('" & trim(a2(0)) & "','" & trim(a2(1)) & "')"
objconn.execute sql
next
asite=split(request("site"),",")
for i=lbound(asite) to ubound(asite)
sql="insert into cancelanal(site,alldays) values('" & trim(asite(i)) & "',1)" 
objconn.execute sql
next
'check for notes  notes-sitename
			 For x = 1 to Request.Form.Count
			 fieldname=Request.Form.Key(x)
    if left(fieldname,6)="notes-" then
			 
		    'Response.Write Request.Form.Item(x) & "<br>"
			if request(fieldname)<>"" then 
			sql="exec spnotesanal @site='" & right(fieldname,len(fieldname)-6) & "',@notes=" & tosql(request(fieldname),"text")
			objconn.execute sql
			end if	
		end if		
Next 
response.write "cancelled and notes submitted successfully"
end sub
Function GetDaysInMonth(strMonth, strYear)
    Dim strDays
    Select Case cint(strMonth)
        Case 1, 3, 5, 7, 8, 10, 12:
            strDays = 31
        Case 4, 6, 9, 11:
            strDays = 30
        Case 2:
            if ((cint(strYear) mod 4 = 0 and cint(strYear) mod 100 <> 0) or ( cint(strYear) mod 400 = 0) ) then
                strDays = 29
            else
                strDays = 28
            end if
    End Select
    GetDaysInMonth = strDays
End Function
sub writetoexcel(mytable)
randomize
     nRandom = (100-1)*Rnd+1
     fileExcel =monthname(month(date)) &  cstr(year(date)) & "_" & CStr(nRandom) & ".xls"
     filePath= Server.mapPath("admin.asp")
	 filepath=mid(filepath,1,len(filepath)-9)
	 'response.write "filepath:" & filepath & "<br>"
	  filename=filePath & "files\" & fileExcel
     'response.write filename
	 Set fs = Server.CreateObject("Scripting.FileSystemObject")
     Set MyFile = fs.CreateTextFile(filename, True)

      

     myfile.writeline(mytable)
    MyFile.Close
  Set MyFile=Nothing
  Set fs=Nothing

  link="<A HREF=files/" & fileExcel & " class=adminmenu target=_new>Click Here to Open Excel</a>"
   Response.write "Right Click save target as with this link: " &  link



end sub
sub adddvla
if request("cmd2")="save" then

'sql="select * from dvlarep where dvla='" & request("reg") & "'"
'openrs rs,sql
'if rs.eof and rs.bof then 
	' response.write "invalid registration - please go back and reenter"
	 'exit sub
'end if
'closers rs
t="text"
userid=session("userid")
ip=Request.ServerVariables("remote_addr")
datereceived=cisodate(request("datereceived" & i))
'sql="insert into dvla(reg,owner,address1,address2,address3,town,postcode,[dvla status],datereceived,make,model,colour) values("
'sql=sql & tosql(request("reg"),t) & "," & tosql(request("owner"),t)& "," & tosql(request("address1"),t)& "," & tosql(request("address2"),t)& "," & tosql(request("address3"),t)& "," & tosql(request("town"),t)& "," & tosql(request("postcode"),t)& "," & tosql(request("dvlastatus"),t) & ",'" & datereceived & "'," & tosql(request("make"),t)& "," & tosql(request("model"),t)& "," & tosql(request("colour"),t)& ")"

sql="exec spinsertdvlarecord @reg=" & tosql(request("reg"),t) & ",@owner=" & tosql(request("owner"),t)& ",@address1=" & tosql(request("address1"),t)& ",@address2=" & tosql(request("address2"),t)& ",@address3=" & tosql(request("address3"),t)& ",@town=" & tosql(request("town"),t)& ",@postcode=" & tosql(request("postcode"),t)& ",@dvlastatus=" & tosql(request("dvlastatus"),t) & ",@datereceived='" & datereceived & "',@make=" & tosql(request("make"),t)& ",@model=" & tosql(request("model"),t)& ",@colour=" & tosql(request("colour"),t) & ",@userid=" & userid & ",@ip=" & tosql(ip,t)
response.write "<!--" & sql & "-->"
set rs=objconn.execute(sql)
'if rs("existsdvla")>0 then 
	' response.write "This record already exists in the dvla db"
'end if
if rs("existsdvlarep")=0 then 
	 response.write "This record was not sent to the dvla"
end if
if rs("myidentity")<>"0" then
response.write "record saved<br>"
'sql="select top 1(id) from dvla order by id desc"
'openrs rs,sql
response.write "Record ID:" & rs("myidentity")
end if
'closers rs
end if
%>
<form method=post action=admin.asp?cmd=adddvla&cmd2=save>
<table class=maintable2>
<tr>
<td colspan=2><b>Add Dvla</td>
</tr>
<tr><th>Vehicle Registration</th><td><input type=text name=reg></td></tr>
<tr><th>Owner</th><td><input type=text name=owner></td></tr>
<tr><th>Address </th><td><input type=text name=address1><br><input type=text name=address2><br><input type=text name=address3></td></tr>
<tr><th>Town</th><td><input type=text name=town></td></tr>
<tr><th>Post Code</th><td><input type=text name=postcode></td></tr>
<tr><th>Dvla Status</th><td><input type=text name=dvlastatus></td></tr>
<tr><th>Date Received</th><td><input type=text name=datereceived value="<%=date()%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)</td></tr>
<tr><th>Make</th><td><input type=text name=make></td></tr>
<tr><th>Model</th><td><input type=text name=model></td></tr>
<tr><th>Colour</th><td><input type=text name=colour></td></tr>
<tr><td colpsan=2 align=center><input type=submit name=submit value=Save class=button></td></tr>
</table>
</form>



<%
'end if
end sub
sub delpcnpermits

'if request("cmd2")="delpcnpermits" then
sql="delete from tempviolators where registration in(select registration from apermits)"
'response.write sql
objconn.execute sql
response.write "violators with permits deleted<br>"
'end if
end sub
sub dvlasearchbyid
	  sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='dvlaedit'"
	openrs rsm2,sqlm2
	if  rsm2.eof and  rsm2.bof then
			 exit sub
			response.write "no access"
	end if
	closers rsm2
if request.form("cmd2")="save" then
sql="select * from dvla where id>=" & request("startingid") & " and id<=" & request("endingid")
call write_html(sql)
call outputexcel(sql)
end if
%>
<form method=post action=admin.asp?cmd=dvlasearchbyid><input type=hidden name=cmd2 value=save>
<table class=maintable2>
<tr>
<td colspan=2><b>Search by ID</b></td>
</tr>

<tr><td>Starting ID</td><td><input type=text name=startingid></td></tr>
<tr><td>Ending ID</td><td><input type=text name=endingid></td></tr>
<tr><td colspan=2 align=center><input type=submit name=submit value=Search></td></tr>
</table>
</form>


<%

end sub
sub dvlasearchbyletter
sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='dvlaedit'"
	openrs rsm2,sqlm2
	if  rsm2.eof and  rsm2.bof then
			 exit sub
			response.write "no access"
	end if
	closers rsm2
if request.form("cmd2")="save" then
sql="select * from dvla where reg like '" & request("startingid") & "%' "
call write_html(sql)
call outputexcel(sql)
end if
%>
<form method=post action=admin.asp?cmd=dvlasearchbyletter><input type=hidden name=cmd2 value=save>
<table class=maintable2>
<tr>
<td colspan=2><b>Search by dvla starting</b></td>
</tr>

<tr><td><input type=text name=startingid></td></tr>

<tr><td  align=center><input type=submit name=submit value=Search></td></tr>
</table>
</form>


<%

end sub

sub editdvla
sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='dvlaedit'"
	openrs rsm2,sqlm2
	if  rsm2.eof and  rsm2.bof then
			 exit sub
			response.write "no access"
	end if
	closers rsm2

if request("cmd2")<>"save" then
	
	sql="select * from dvla where id=" & request("id")
	openrs rs,sql
	%>
	<table class=maintable2>
	<form method=post action=admin.asp?cmd=editdvla&cmd2=save>
	<input type=hidden name=id value='<%=rs("id")%>'>
	
	<%
	stringf="Reg,Model,Colour,Owner,Address1,Address2,Address3,Town,Postcode"
	
	arrf=split(stringf,",")
	for i=lbound(arrf) to ubound(arrf)
	
	response.write "<tr>"
	  response.write "<th>" & arrf(i) & "</th>"
		
				response.write "<td><input type=text name=" & arrf(i) & " value='" & rs(arrf(i)) & "'></td>"
		
		response.write "</tr>"
	next
	response.write "<tr><td><input type=submit name=submit value=save class=button>"
	
	%>
	</form>
	</table>
	<%
	closers rs
else

	sql="update dvla set "
	stringf="Reg,Model,Colour,Owner,Address1,Address2,Address3,Town,Postcode"
	
	arrf=split(stringf,",")
	for i=lbound(arrf) to ubound(arrf)
	sql=sql & arrf(i) & "=" & tosql(request(arrf(i)),"text") & ","
		next
	sql=left(sql,len(sql)-1)
	sql=sql &  " where id=" & tosql(request("id"),"Number")
		'response.write sql
	objconn.execute sql
	
	
	response.write "updated"
	

end if
end sub

sub dvlasearch2
sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='dvlaedit'"
	openrs rsm2,sqlm2
	if  rsm2.eof and  rsm2.bof then
			 exit sub
			response.write "no access"
	end if
	closers rsm2
%>

<table>
<form method=post action=admin.asp?cmd=dvlafind2>
<tr><td align=center>Enter in search term to search dvla table</td></tr>
<tr><td align=center> <input type=text name=search></td></tr>
<tr><td align=center><input type=submit  value="Search"></td></tr>
</form>
<tr><td colspan=2><hr></td></tr>
<tr><td colspan=2 align=center>OR Enter ID below</td></tr>
<form method=post action=admin.asp?cmd=dvlafind2><input type=hidden name=cmd2 value="id">
<tr><td align=center> <input type=text name=search></td></tr>
<tr><td align=center><input type=submit  value="Search"></td></tr>
</form>
</table>

<%
end sub
sub dvlafind2
sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='dvlaedit'"
	openrs rsm2,sqlm2
	if  rsm2.eof and  rsm2.bof then
			 exit sub
			response.write "no access"
	end if
	closers rsm2
	searchterm=request("search")
	if searchterm<>"" then i=1
	if request("s")=1 then
		sql=session("sql")
		i=1
		rcount=2
	else
		sqlw="SELECT dvla.id,DVLA.Reg,  DVLA.Model, DVLA.Colour, DVLA.Owner, DVLA.Address1, DVLA.Address2, DVLA.Address3, DVLA.Town, DVLA.Postcode, DVLA.[DVLA Status], DVLA.[DateReceived] FRom dvla where  "
		sqlc="select count(reg) as countc from dvla where "
		if request("cmd2")<>"id" then
		sql="DVLA.Reg Like '%" & searchterm & "%' OR DVLA.Make Like '%" & searchterm & "%' OR DVLA.Model Like '%" & searchterm & "%' OR DVLA.Colour Like '%" & searchterm & "%' OR DVLA.Owner Like '%" & searchterm & "%' OR DVLA.Address1 Like '%" & searchterm & "%' OR DVLA.Address2 Like '%" & searchterm & "%' OR DVLA.Address3 Like '%" & searchterm & "%' OR DVLA.Town Like '%" & searchterm & "%' OR DVLA.Postcode like '%" & searchterm & "%' OR  DVLA.[DVLA Status] Like '%" & searchterm & "%' OR DVLA.[DateReceived] Like '%" & searchterm & "%'" 
		else
		
		sql="id=" & searchterm
		end if
		sqlc=sqlc& sql
		sql=sqlw & sql
	'	response.write sql
		openrs rsc,sqlc
		rcount=rsc("countc")
		closers rsc 
		end if
	
	if i=0 then 
		response.write "You must fill in a search field"
		call dvlasearch
	else
		
		strpagename="admin.asp?cmd=dvlafind"
			
		if rcount=0 then 
			response.write "no records exist with this criteria"
		'elseif rcount=1 then call viewrecord
		else
				createdvlaSortableListe objConn,sql,"reg",25,"class=maintable2w",strpagename
		end if
	end if

end sub

sub dvlaexcel
 sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='superadmin'"
	openrs rsm2,sqlm2
	if  rsm2.eof and  rsm2.bof then exit sub 
	
sql="select reg,make,model,colour,owner,address1,address2,address3,town,postcode,[dvla status],convert(varchar,datereceived,108),transfered,auto1,auto2,auto3,userid,ip,dateadded from dvla "
call outputexcel(sql)

end sub

sub tarrifexcel
 sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='superadmin'"
	openrs rsm2,sqlm2
	if  rsm2.eof and  rsm2.bof then exit sub 
	
sql="select * from tariff"
call outputexcel(sql)

end sub

sub sitedetailexcel
 sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='superadmin'"
	openrs rsm2,sqlm2
	if  rsm2.eof and  rsm2.bof then exit sub 
	
sql="select * from [site information]"
call outputexcel(sql)

end sub
objconn.close
set objconn=nothing
sub weeklyoverview
sqlm="select * from usermenu where userid=" & session("userid") & " and menu='reports'"
	openrs rsm,sqlm
	if rsm.eof and rsm.bof then 
		response.write "no access"
		response.end
	end if
	closers rsm
	if request.form("cmd2")="" then
	%>
	<table class=maintable2 width="500">
	
<tr>
<td>
	<form method=post action=admin.asp?cmd=weeklyoverview>
	
	From</td><td> <input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To </td><td><input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td colspan=2 align=center>
<input type=hidden name=cmd2 value=showreport>
<input type=submit name=submit class=button value="Show Report">
	</td>
</tr>
</table>
	</form>
	<%
	end if
if request.form("cmd2")="z" then
	 %>
	 <form method=post action=admin.asp?cmd=weeklyoverview>Year 
	 <select name=year>
	 <%
	 cyear=year(now)
	 for i=cyear-5 to cyear-1
	 response.write "<option value=" & i & ">" & i& "</option>" 
	 next
	  response.write "<option selected value=" & cyear & ">" & cyear& "</option>" 
	 %>
	 </select> <input type=submit name=submit value="Change Year" class=submit></form>
<br><br>
<%
response.write "<br><br><table class=maintable1><tr><td><form method=post action=admin.asp?cmd=weeklyoverview><input type=hidden name=cmd2 value=showreport>"
			response.write "Pick a Week:<br>"
		response.write "<select name=datetoday>"
		dWkStartDate = date()
		j=1
		do while j<53
			
		Do until weekday(dWkStartDate) = vbsunday
 		dWkStartDate = DateAdd("d",-1,dWkStartDate)
		Loop
		dWkEndDate = DateAdd("d",6,dWkStartDate)
		response.write "<option value=" & dwkstartdate 
		if dtoday=date then response.write " selected "
		response.write  ">" 
		Response.Write dwkstartdate & "-" & dwkenddate
		response.write "</option>" & vbcrlf
	'	response.write "<!--today:" & date() & "-" & dtoday & "-->" 
		dWkStartDate=dwkstartdate-7
		j=j+1
		loop
		response.write "</select>"
		
		response.write "<input type=submit name=submit class=submit value='View Report'>"
		response.write "</form></td></tr></table>"
 

end if	
if request("cmd2")="showreport" then
	 
  	datefrom=request("datefrom")
		dateto=request("dateto")
		dwkstartdate=datefrom
		
  		Do until weekday(dWkStartDate) = vbsunday
   		dWkStartDate = DateAdd("d",-1,dWkStartDate)
  	Loop
  		dWkEndDate = DateAdd("d",6,dWkStartDate)
		response.write dwkstartdate & "-" & dwkenddate
	sqlpm="select * from paymentmethods"
	openrs rspm,sqlpm
	
	i=0
	do while not rspm.eof
	if i>0 then strmethods=strmethods & ","
		 strmethods=strmethods & rspm("method")
		 i=i+1	
		 rspm.movenext
		 
	loop	
	
	amethods=split(strmethods,",")
	nmethods=ubound(amethods)
	dim	asmethods(100)
	'redim asmethods(nmethods)
	closers rspm
	mytable= "<table class=maintable2border><Td></td>"
	for i=lbound(amethods) to ubound(amethods)
			mytable=mytable &  "<td>" & amethods(i) & "</td>"
	next
	
		mytable=mytable & "<td>Total</td></tr>"
		
			mytable=mytable & "<tr><Td>" & "Commencing " & dwkstartdate & "</td>"
			tamount=0
			for i=lbound(amethods) to ubound(amethods)
			sql="select sum(amount) as samount from payments where method='" & amethods(i) & "' and received>='" & cisodate(dwkstartdate) & "' and received<='" & cisodate(dwkenddate) & " 23:59'"
			'response.write sql
			openrs rs,sql
						 samount=rs("samount")
						 asmethods(i)=asmethods(i)+samount
						 if samount="" or isnull(samount) then samount=0
						tamount=samount+tamount
					'	 response.write "<hr>" & tamount & "<hr>"
			closers rs
			mytable=mytable & "<td align=right>" &formatnumber(samount,0) & "</td>"
	next
			mytable=mytable &"<td align=right>" & formatnumber(tamount,0) & "</td>"
			mytable=mytable & "</tr>"
			
			DO WHILE dwkenddate<cdate(dateto) 
				 dwkstartdate=dateadd("d",1,dwkenddate)
  			 Do until weekday(dWkStartDate) = vbsunday
   			 dWkStartDate = DateAdd("d",-1,dWkStartDate)
  			 Loop
  			 dWkEndDate = DateAdd("d",6,dWkStartDate)
				mytable=mytable & "<tr><Td>" & "Commencing " & dwkstartdate & "</td>"
				 tamount=0
				 for i=lbound(amethods) to ubound(amethods)
			sql="select sum(amount) as samount from payments where method='" & amethods(i) & "' and received>='" & cisodate(dwkstartdate) & "' and received<='" & cisodate(dwkenddate) & " 23:59'"
			'response.write sql
			openrs rs,sql
						 samount=rs("samount")
					 if samount="" or isnull(samount) then samount=0
					  asmethods(i)=asmethods(i)+samount
						 tamount=samount+tamount
			closers rs
			mytable=mytable & "<td align=right>" &formatnumber(samount,0) & "</td>"
	next
			mytable=mytable & "<td align=right>" & formatnumber(tamount,0) & "</td>"
			mytable=mytable & "</tr>" & vbcrlf
			loop	 									 
			mytable=mytable &"<tr><td>Totals</td>" 
			totalamount=0
			for i=lbound(amethods) to ubound(amethods)
			if asmethods(i)<>"" then
				 ttamount=asmethods(i)
			else
					ttamount=0
			end if
			totalamount=totalamount+cdbl(ttamount)
					mytable=mytable & "<td align=right>" & formatnumber(ttamount,0) & "</td>"
			next
					mytable=mytable & "<td align=right>" & formatnumber(totalamount,0) & "</td>"
			mytable=mytable & "</tr>"
	mytable=mytable & "</table>"
	response.write mytable
	 writetoexcel(mytable)
	end if
end sub

sub zplateanalysis

		i=0
Dim objFSO
 Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
Dim objFolder 
Dim strRootFolder
 strRootFolder = "" & configsecuredir & "db\plates\" 
 Set objFolder = objFSO.GetFolder(strRootFolder)
Dim objFile 
For Each objFile in objFolder.Files
		sfile=objfile
		if instr(sfile,".csv")>0 then
		i=i+1
	
'read each excle and process
	
		response.write sfile
		Set fs=Server.CreateObject("Scripting.FileSystemObject")
	Set f=fs.OpenTextFile(sfile, 1)
		
		
	i=1
	do while f.AtEndOfStream = false
		str=f.readline
		if str<>""  then
	'	response.write str & "%%%"
			arrstr=split(str,",",-1,1)
			if ubound(arrstr)>1 then
				 imagename=arrstr(5)
				 imagename2=arrstr(6)
				 imagename3=arrstr(7)
				'response.write "<font color=blue>" & imagename & "</font>"
				 nu=instrrev(imagename,"\",-1,1)
				
				 imagename=right(imagename,len(imagename)-nu)
				 'response.write "<font color=red>" & nu & "</font>"
				' response.write "<hr>" &imagename & arrstr(0) & "<br>"
						if fs.FileExists("" & configsecuredir & "db\data stream\images\" & imagename)=true then
							 				mydate=arrstr(1)
										myyear=year(mydate)
										mymonth=month(mydate)
										myday=day(mydate)
										myhour=hour(arrstr(2))
									'	response.write arrstr(2) & "-"
										imagedir=configwebdir & "traffica\plateimages\server1\"
									newimagepath=imagedir & myyear & "\" & mymonth & "\" & myday & "\" & myhour & "\"
								'	response.write newimagepath
										CheckDirectory newimagepath
									fs.MoveFile "" & configsecuredir & "db\data stream\images\" & imagename,newimagepath 
										
										
										'move images
										imagesexist=1
						else
								imagesexist=0
						end if
						'imagename=replace(imagename,"P.jpg","F.jpg")
						if fs.FileExists("" & configsecuredir & "db\data stream\images\" & imagename2)=true then
							 				mydate=arrstr(1)
										myyear=year(mydate)
										mymonth=month(mydate)
										myday=day(mydate)
										myhour=hour(arrstr(2))
										imagedir="" & configwebdir & "traffica\plateimages\server1\"
								newimagepath=imagedir & myyear & "\" & mymonth & "\" & myday & "\" & myhour & "\"
										CheckDirectory newimagepath
									fs.MoveFile "" & configsecuredir & "db\data stream\images\" & imagename2,newimagepath 
										
										
						end if
						
						if fs.FileExists("" & configsecuredir & "db\data stream\images\" & imagename3)=true then
							 				mydate=arrstr(1)
										myyear=year(mydate)
										mymonth=month(mydate)
										myday=day(mydate)
										myhour=hour(arrstr(2))
										imagedir=configwebdir & "traffica\plateimages\server1\"
										newimagepath=imagedir & myyear & "\" & mymonth & "\" & myday & "\" & myhour & "\"
										CheckDirectory newimagepath
									fs.MoveFile "" & configsecuredir & "db\data stream\images\" & imagename3,newimagepath 
										
										
										'move images
						'				imagesexist=1
						
						end if
						'EK03VBN	29/04/2007	00:00:03	63	99	.\20070429\EK03VBN 00102B9 P.jpg	.\20070429\EK03VBN 00102B9 F.jpg
	
						sql="exec spinsertplate @plate='" & arrstr(0) & "',@date='" &cisodate(arrstr(1)) & " " & arrstr(2) & "',@lane='" & arrstr(3) & "',@accuracy=" & arrstr(4) &  ",@imagesexist=" & imagesexist & ",@picture='" & imagename & "',@picture2='" & imagename2 & "',@picture3='" & imagename3 & "'" 
					'	response.write sql & "<br>"
						objconn.execute sql
						'response.write sql & "-" & imagename & "<br>"
			end if
		end if
		
		
	loop
	f.Close
	
	set f=Nothing
	'fs.DeleteFile(sfile)
	
	'check all recores with imagesexist=0 adn check for images
	sqlck="select * from plateimages where imagesexist=0"
	openrs rsck,sqlck
	do while not rsck.eof
		 			 		 imagename=rsck("picture")
							 imagesexist=0
		 			 		 if fs.FileExists("" & configsecuredir & "db\data stream\images\" & imagename)=true then
							 				mydate=rsck("date")
										myyear=year(mydate)
										mymonth=month(mydate)
										myday=day(mydate)
										myhour=hour(arrstr(2))
										imagedir="" & configwebdir & "traffica\plateimages\server1\"
									newimagepath=imagedir
									checkdirectory newimagepath
										fs.MoveFile "" & configsecuredir & "db\data stream\images\" & imagename,newimagepath 
										
										
										'move images
										imagesexist=1
						else
								imagesexist=0
						end if
						imagename=replace(imagename,"P.jpg","F.jpg")
						if fs.FileExists("" & configsecuredir & "db\data stream\images\" & imagename)=true then
							 				mydate=arrstr(1)
										myyear=year(mydate)
										mymonth=month(mydate)
										myday=day(mydate)
										myhour=hour(arrstr(2))
										imagedir=configsecuredir & "traffica\plateimages\server1\"
									'newimagepath=imagedir
										newimagepath=imagedir & myyear & "\" & mymonth & "\" & myday & "\" & myhour & "\"
											CheckDirectory newimagepath
										fs.MoveFile "" & configsecuredir & "db\data stream\images\" & imagename,newimagepath 
										
										
						end if
						imagename=replace(imagename,"F.jpg","A.jpg")
						if fs.FileExists("" & configsecuredir & "db\data stream\images\" & imagename)=true then
							 				mydate=arrstr(1)
										myyear=year(mydate)
										mymonth=month(mydate)
										myday=day(mydate)
										myhour=hour(arrstr(2))
								imagedir=configwebdir & "traffica\plateimages\server1\"
									newimagepath=imagedir
										newimagepath=imagedir & myyear & "\" & mymonth & "\" & myday & "\" & myhour & "\"
											CheckDirectory newimagepath
								
										fs.MoveFile "" & configsecuredir & "db\data stream\images\" & imagename,newimagepath 
										
										
										'move images
						'				imagesexist=1
						
						end if
						if imagesexist=1 then 
							 sqlu="update plateimages set imagesexist=1 where id=" & rsck("id")
						end if
	rsck.movenext
	loop
	closers rsck
	Set fs=Nothing
	response.write "done"
	end if 
Next
end sub
sub plateanalysis

		i=0
Dim objFSO
 Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
Dim objFolder 
Dim strRootFolder
 strRootFolder = plateanaldir
 Set objFolder = objFSO.GetFolder(strRootFolder)
Dim objFile 
For Each objFile in objFolder.Files
		sfile=objfile
		'if instr(sfile,".jpg")>0 then
		'response.Write sfile & "<br>"
		'file=mid(sfile,n+1)
		'sql="select * from plateimages where picture='" &sfile & "' or picture3='" &sfile & "' or picture2='" &sfile & "'"
		'response.Write sql
		'openrs rs,sql
		'if not rs.eof and not rs.bof then response.Write sfile & "file exits in plateimages"
		'closers rs
		'response.Write "<hr>"
		'response.flush
		'end if
		if instr(sfile,".txt")>0 then
		
		i=i+1
	
'read each excle and process
	
		'response.write sfile
		Set fs=Server.CreateObject("Scripting.FileSystemObject")
	Set f=fs.OpenTextFile(sfile, 1)
		
		
	i=1
	do while f.AtEndOfStream = false
		str=f.readline
		if str<>""  then
		duplicate=0
	     '   response.write str
			arrstr=split(str,",",-1,1)
			if ubound(arrstr)>1 then
				 imagename=arrstr(6)
				 imagename2=arrstr(7)
				 imagename3=arrstr(8)
				
				 mydate=arrstr(2)
				 plate=arrstr(1)
				 site=arrstr(0)
				 mytime=arrstr(3)
			'	 response.Write mytime
			'	 response.end
				 lane=arrstr(4)
				 accuracy=arrstr(5)
				 myyear=year(mydate)
				 mymonth=month(mydate)
				myday=day(mydate)
				myhour=hour(mytime)
				
               i=instrrev(imagename,"\")
             '  response.Write imagename & ":" & i
               imagename=mid(imagename,i+1)
               imagename=ltrim(imagename)
            '   response.Write "<br>a" & imagename & "<br>"
              ' response.end
               i=instrrev(imagename2,"\")
               imagename2=mid(imagename2,i+1)
               imagename2=ltrim(imagename2)
               i=instrrev(imagename3,"\")
               imagename3=mid(imagename3,i+1)
               imagename3=ltrim(imagename3)
				'response.write "<font color=blue>" & imagename & "</font>"
				' nu=instrrev(imagename,"\",-1,1)
				
				 imagename=right(imagename,len(imagename)-nu)
				 'response.write "<font color=red>" & nu & "</font>"
				' response.write "<hr>" &imagename & arrstr(0) & "<br>"
				'check for duplicates  Site, date, time, plate
					sqlcp="select * from plateimages where site='" & site & "' and date='" &cisodate(mydate) & " " & mytime & "' and plate='" & plate & "'"
					'response.write sqlcp
					openrs rscp,sqlcp
					if not rscp.eof and not rscp.bof then
					    duplicate=1
					   
					 end if
					 closers rscp
	'response.end
				if duplicate=1 then	
				'    fs.DeleteFile(sfile)
				else
						if fs.FileExists(plateanaldir& imagename)=true then
							 				
										
									'	response.write arrstr(2) & "-"
										'imagedir=configwebdir & "traffica\plateimages\server1\"
										imagedir=configsecuredir & "db\plateimages\server1\"
									newimagepath=imagedir & myyear & "\" & mymonth & "\" & myday & "\" & myhour & "\"
								'	response.write newimagepath
										CheckDirectory newimagepath
									fs.MoveFile plateanaldir & imagename,newimagepath 
										
										
										'move images
										imagesexist=1
						else
								imagesexist=0
						end if
						'imagename=replace(imagename,"P.jpg","F.jpg")
						if fs.FileExists(plateanaldir &  imagename2)=true then
							 				
									'	imagedir="" & configwebdir & "traffica\plateimages\server1\"
									
									imagedir=configsecuredir & "db\plateimages\server1\"
								newimagepath=imagedir & myyear & "\" & mymonth & "\" & myday & "\" & myhour & "\"
										CheckDirectory newimagepath
									fs.MoveFile plateanaldir & imagename2,newimagepath 
										
										
						end if
						
						if fs.FileExists(plateanaldir & imagename3)=true then
							 				
										'imagedir=configwebdir & "traffica\plateimages\server1\"
										
										imagedir=configsecuredir & "db\plateimages\server1\"
										newimagepath=imagedir & myyear & "\" & mymonth & "\" & myday & "\" & myhour & "\"
										CheckDirectory newimagepath
									fs.MoveFile plateanaldir& imagename3,newimagepath 
										
										
										'move images
						'				imagesexist=1
						
						end if
						'EK03VBN	29/04/2007	00:00:03	63	99	.\20070429\EK03VBN 00102B9 P.jpg	.\20070429\EK03VBN 00102B9 F.jpg
	
						sql="exec spinsertplate @plate='" & plate & "',@date='" &cisodate(mydate) & " " & mytime & "',@lane='" & lane& "',@accuracy=" & accuracy &  ",@imagesexist=" & imagesexist & ",@picture='" & imagename & "',@picture2='" & imagename2 & "',@picture3='" & imagename3 & "',@site='" & site & "'"
						
						'response.write sql & "<br>"
						objconn.execute sql
						'response.write sql & "-" & imagename & "<br>"
						  
			end if
		end if
		end if
		'fs.MoveFile  sfile,configsecuredir & "db\plates\oldfiles"
	loop
	
	
									
	f.Close
	
	set f=Nothing
	fs.deleteFile(sfile)
		
	end if 
	
Next

Set fs=Server.CreateObject("Scripting.FileSystemObject")
	'response.Write "here chekcing images"
	'check all recores with imagesexist=0 adn check for images
	sqlck="select * from plateimages where imagesexist=0"
	openrs rsck,sqlck
	do while not rsck.eof
		 			 		 imagename=rsck("picture")
							 imagesexist=0
						'	 response.Write imagename
		 			 		 if fs.FileExists(plateanaldir & imagename)=true then
		 			 		' response.Write "found file" & imagename
							 				mydate=rsck("date")
										myyear=year(mydate)
										mymonth=month(mydate)
										myday=day(mydate)
										myhour=hour(mydate)
										'imagedir="" & configwebdir & "traffica\plateimages\server1\"
										imagedir=configsecuredir & "db\plateimages\server1\"
									newimagepath=imagedir
									checkdirectory newimagepath
										fs.MoveFile plateanaldir & imagename,newimagepath 
										
										
										'move images
										imagesexist=1
						else
								imagesexist=0
						end if
						imagename=replace(imagename,"P.jpg","F.jpg")
						if fs.FileExists(plateanaldir & imagename)=true then
							 				mydate=rsck("date")
										myyear=year(mydate)
										mymonth=month(mydate)
										myday=day(mydate)
										myhour=hour(mydate)
										'imagedir=configsecuredir & "traffica\plateimages\server1\"
										imagedir=configsecuredir & "db\plateimages\server1\"
									'newimagepath=imagedir
										newimagepath=imagedir & myyear & "\" & mymonth & "\" & myday & "\" & myhour & "\"
											CheckDirectory newimagepath
										fs.MoveFile plateanaldir & imagename,newimagepath 
										
										
						end if
						imagename=replace(imagename,"F.jpg","A.jpg")
						if fs.FileExists(plateanaldir& imagename)=true then
							 				mydate=rsck("date")
										myyear=year(mydate)
										mymonth=month(mydate)
										myday=day(mydate)
										myhour=hour(mydate)
							'	imagedir=configwebdir & "traffica\plateimages\server1\"
							imagedir=configsecuredir & "db\plateimages\server1\"
									newimagepath=imagedir
										newimagepath=imagedir & myyear & "\" & mymonth & "\" & myday & "\" & myhour & "\"
											CheckDirectory newimagepath
								
										fs.MoveFile plateanaldir & imagename,newimagepath 
										
										
										'move images
						'				imagesexist=1
						
						end if
						if imagesexist=1 then 
							 sqlu="update plateimages set imagesexist=1 where id=" & rsck("id")
							 objconn.execute sqlu
						end if
	rsck.movenext
	loop
	closers rsck
	Set fs=Nothing

response.write "done"
end sub
'Function CheckDirectory(dirPath) 
Function CheckDirectory( path )

      Checkdirectory = False    '// Default return value

      folders = Split(path, "\")

      '// Check if the drive is in the path, assume C: if it's not
      If InStr(folders(0), ":") Then
          currentPath = folders(0) & "\"
          startIdx = 1
      Else
          currentPath = "C:\"
          startIdx = 0
      End If

      '// Create the file system object
      Set fso = Server.CreateObject("Scripting.FileSystemObject")

      On Error Resume Next

      For i = startIdx To UBound(folders)

         currentPath = currentPath & folders(i) & "\"
         If Not fso.FolderExists(currentPath) Then
             fso.CreateFolder(currentPath)
         End If

         If Err.number <> 0 Then
            '// An error occured - probably permissions - so exit
            Exit For
         End If

      Next

   '   On Error Goto 0

      '// Final check that the path was created
      If fso.FolderExists(currentPath) Then
          path = currentPath      '// Return our path in case the drive has changed
          CheckDirectory = True
      End If      

   End Function
sub explateanalysis
if request("cmd2")="" then

%>
<table class=maintable2>

<form method=post action=admin.asp?cmd=explateanalysis><input type=hidden name=cmd2 value="add">

<tr><td>
Date exemption starts: </td><td><input type=text name=fromdate id=fromdate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('fromdate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
Date exemption ends:</td><td> <input type=text name=todate id=todate value="<%=FormatDateTime(dateadd("d",21,date()),2)%>">
<a href="javascript:NewCal('todate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>

	<tr><td colspan=2 align=center>
<input type=submit value=submit name=sumbit>
</td>
</tr>
</table>
</form>

<%

else

fromdate=request("fromdate")
todate=request("todate")
		    i=0
    Dim objFSO
     Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
    Dim objFolder 
    Dim strRootFolder
     strRootFolder = configsecuredir & "db\exemptionplates\"
     Set objFolder = objFSO.GetFolder(strRootFolder)
    Dim objFile 
    For Each objFile in objFolder.Files
		    sfile=objfile
    		
		    if instr(sfile,".txt")>0 then
    		
		    i=i+1
    	
    'read each excle and process
    	
		    'response.write sfile
		    Set fs=Server.CreateObject("Scripting.FileSystemObject")
	    Set f=fs.OpenTextFile(sfile, 1)
    		
    		
	    i=1
	    do while f.AtEndOfStream = false
		    str=f.readline
		    'response.Write "<hr class=blue>" & str & "<br>"
		    if str<>""  then
		    astr=split(str,",")
		    registration=astr(0)
		   ' if ubound(astr)>0 then
		    '    datetime=astr(1)
		    'else 
		        datetime=fromdate
		        enddate= todate 'dateadd("d",21,datetime)
		        
		    'end if
	    on error resume next
	    sql="insert into exemptions(registration,datetime,dateadded,site,startdate,enddate,addedfrom) values(" & tosql(registration,"text") & "," & tosql(datetime,"date") & ",getdate(),'071'" & "," & tosql(datetime,"date") & "," & tosql(enddate,"date") & ",'extracted')"
	   ' response.Write sql
	    objconn.execute sql
	    if err.number>0 then
    	
	    response.Write "error:" & sql
	    end if
    '	on error goto 0
	    end if	
	    loop
    	
    									
	    f.Close
		    set f=Nothing
	    fs.deleteFile(sfile)
    		
	    end if 
    	
    Next

    response.write "done"
end if
end sub

sub explateanalysis072
if request("cmd2")="" then

%>
<table class=maintable2>

<form method=post action=admin.asp?cmd=explateanalysis072><input type=hidden name=cmd2 value="add">

<tr><td>
Date exemption starts: </td><td><input type=text name=fromdate id=Text2 value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('fromdate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
Date exemption ends:</td><td> <input type=text name=todate id=Text3 value="<%=FormatDateTime(dateadd("d",21,date()),2)%>">
<a href="javascript:NewCal('todate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>

	<tr><td colspan=2 align=center>
<input type=submit value=submit name=sumbit>
</td>
</tr>
</table>
</form>

<%

else

fromdate=request("fromdate")
todate=request("todate")
		    i=0
    Dim objFSO
     Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
    Dim objFolder 
    Dim strRootFolder
     strRootFolder = configsecuredir & "db\exemptionplates\"
     Set objFolder = objFSO.GetFolder(strRootFolder)
    Dim objFile 
    For Each objFile in objFolder.Files
		    sfile=objfile
    		
		    if instr(sfile,".txt")>0 then
    		
		    i=i+1
    	
    'read each excle and process
    	
		    'response.write sfile
		    Set fs=Server.CreateObject("Scripting.FileSystemObject")
	    Set f=fs.OpenTextFile(sfile, 1)
    		
    		
	    i=1
	    do while f.AtEndOfStream = false
		    str=f.readline
		    'response.Write "<hr class=blue>" & str & "<br>"
		    if str<>""  then
		    astr=split(str,",")
		    registration=astr(0)
		   ' if ubound(astr)>0 then
		    '    datetime=astr(1)
		    'else 
		        datetime=fromdate
		        enddate= todate 'dateadd("d",21,datetime)
		        
		    'end if
	    on error resume next
	    sql="insert into exemptions(registration,datetime,dateadded,site,startdate,enddate) values(" & tosql(registration,"text") & "," & tosql(datetime,"date") & ",getdate(),'071'" & "," & tosql(datetime,"date") & "," & tosql(enddate,"date") & ")"
	   ' response.Write sql
	    objconn.execute sql
	    if err.number>0 then
    	
	    response.Write "error:" & sql
	    end if
    '	on error goto 0
	    end if	
	    loop
    	
    									
	    f.Close
		    set f=Nothing
	    fs.deleteFile(sfile)
    		
	    end if 
    	
    Next

    response.write "done"
end if
end sub
sub platereport
fromdate=request("datefrom")
todate=request("dateto")
if fromdate="" and todate="" then show7average=1
if fromdate="" then fromdate=date
if todate="" then todate=date

 noexcel=0
sql="select plate,convert(varchar,[date],103) as mydate,convert(varchar,[date],108) as mytime,lane,permit,violation14 from plateimages where date>='" & cisodate(fromdate) & "' and date<='" & cisodate(todate) & " 23:59'"
'response.write sql
openrs rs,sql
if rs.eof and rs.bof then 
	 response.write "no records"
	
if rs.eof and rs.bof then noexcel=1
else
		mytable="<table class=maintable2><tr><td><b>Plate</b></td><td><b>Date</b></td><td><b>	Time</b></td><td><b>Lane</b></td><td><b>Permit</b></td><td><b>Violator</b></td></tR>"

end if
do while not rs.eof
permit=""
violation14=""
if rs("permit")=true then permit="p"
if rs("violation14")=true then violation14="v"
mytable=mytable & "<tr><td>" & rs("plate") & "</td><td>" & rs("mydate") & "</td><td>" & rs("mytime") & "</td><td>" & rs("lane") & "</td><Td>" & permit & "</td><td>" & violation14 & "</td></tr>" 
rs.movenext
loop

closers rs
mytable=mytable & "</table>"
response.write mytable
mytable1=mytable


sql="exec spplatereport @startdate='" & cisodate(fromdate) & "',@enddate='" & cisodate(todate) & " 23:59'"
openrs rs,sql
if not rs.eof and not rs.bof then 
	 mytable="<table class=maintable2><tr><td>Lane</td><td>Start Time</td><td>First Plate</td><td>End Time</td><td>Last Plate</td><td>Records</td>"
	 if show7average=1 then mytable=mytable &  "<td>7 day average</td>"
	 mytable=mytable & "</tr>"

end if

do while not rs.eof
mytable=mytable &  "<tr><Td>"& rs("lane") & "</td><td>" & rs("starttime") & "</td><td>" & rs("firstplate") & "</td><td>" & rs("endtime") & "</td><td>" & rs("lastplate") & "</td><td>" & rs("total records") & "</td>"
if show7average=1 then mytable=mytable &  "<td>"& rs("countplatesforprevious7days")/7 & "</td>"
 mytable=mytable &  "</tr>"
rs.movenext
loop
closers rs
mytable=mytable & "</table>"
		response.write mytable& "<br>"		
if noexcel=0 then call writetoexcel(mytable1 & mytable)
response.write "<br><br>Select another date:<bR>"
%>
<form method=post action=admin.asp?cmd=platereport>
	
	From <input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>
To <input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>
<input type=submit name=submit class=button value="Show Report">
	
	</form>

<%
end sub


sub dvlatrans
fromdate=request("datefrom")
todate=request("dateto")

if fromdate<>"" and todate<>"" then
 noexcel=0
%>
<table class=maintable2border>

	<tr>
		<th>Date</th>
		<th>Count Automatic Sent</th>
		<th>Count Automatic Rec</th>
		<th>Count Manual Sent</th>
		<th>Count Manual Rec</th>
	</tr>
<%
	if request("DateFrom")<>"" then
		DateFrom = CDate(request("datefrom"))
	'else
		'DateFrom = FormatDateTime(Now()-30,vbShortDate)
	end if
	
	if request("DateTo")<>"" then
		DateTo = CDate(request("dateto"))
	'else
		'DateTo = FormatDateTime(Now()-30,vbShortDate)
	End if
	
	ADate = DateFrom
	do while ADate < DateTo
		sql = "SELECT SUM(CASE WHEN type='auto' THEN 1 ELSE 0 END) AS cas, SUM(CASE WHEN type='manual' THEN 1 ELSE 0 END) AS cms FROM dvlarep WHERE dategenerated > '" & cisodate(ADate) & "' and dategenerated<='" & cisodate(adate) & " 23:59' "
	'	response.write sql
		openrs rs,sql
		CAS = rs("cas")  'auto sent
		CMS = rs("cms")  'manual sent
		closers rs
		
		sql = "SELECT SUM(CASE WHEN type='auto' THEN 1 ELSE 0 END) AS car, SUM(CASE WHEN type='manual' THEN 1 ELSE 0 END) AS cmr FROM dvla WHERE datereceived >= '" & cisodate(ADate) & "'" & " and datereceived<='" & cisodate(adate) & " 23:59' "
		'response.write sql
		openrs rs,sql
		CAR = rs("car") 'auto rec
		CMR = rs("cmr") 'manual rec
		closers rs
		if cas="" or isnull(cas) then cas="0"
		if cms="" or isnull(cms) then cms="0"
		if car="" or isnull(car) then car="0"
		if cmr="" or isnull(cmr) then cmr="0"
%>	<tr>
<td><%=adate%></td>
		<td><%=CAS%></td>
		<td><%=CAR%></td>
		<td><%=CMS%></td>
		<td><%=CMR%></td>
	</tr>
<%
		ADate = DateAdd("d",1,ADate)
	loop
%>
</table>

<%
end if
'if noexcel=0 then call writetoexcel(mytable1 & mytable)
response.write "<br><br>Select another date:<bR>"
%>
<form method=post action=admin.asp?cmd=dvlatrans>
	
	From <input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>
To <input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>
<input type=submit name=submit class=button value="Show Report">
	
	</form>

<%
end sub
sub backupdb

sql="exec backuptraffic"
objconn.execute sql
response.write "DB backed up"
response.write "<a href=" & configweburl & "/sqlbu/traffic.dat>Right Click here to save</a>"

end sub
sub paymentstoexcel

 sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='superadmin'"
	openrs rsm2,sqlm2
	if rsm2.eof and  rsm2.bof then
		 response.write "no access"
		 exit sub
	end if
sql="select * from payments"
outputexcel sql
end sub

sub cancellationtoexcel

 sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='superadmin'"
	openrs rsm2,sqlm2
	if rsm2.eof and  rsm2.bof then
		 response.write "no access"
		 exit sub
	end if
sql="select * from cancelled"
 outputexcel sql
end sub
sub pcnnotesexcel

 sqlm2="select * from usermenu where userid=" & session("userid") & " and menu='superadmin'"
	openrs rsm2,sqlm2
	if rsm2.eof and  rsm2.bof then
		 response.write "no access"
		 exit sub
	end if
sql="select * from pcnnotes"
 outputexcel sql
end sub
sub showfuzzy
mydate=request("mydate")
myplate=request("myplate")
sql="select id,date,convert(varchar,[date],103) as rdate,convert(varchar,[date],108) as rtime,lane,accuracy,plate,picture,permit,violation14 from plateimages  where date>='" & cisodate(mydate) & "' and date<='" & cisodate(mydate) & " 23:59'"
fuzzies=checkfuzzys(myplate,sql)
response.write fuzzies
end sub
function checkfuzzys(plate,sqlp)
'show 10 fuzzy plates from the same date
	'response.write "checkign fuzzy"
	'sqlp="select * from apermits where site='" & sitecode & "' or site='all'"
	'response.write sqlp
'response.write "<font class=red>Checking fuzzy</font>"
'response.write sqlp
	openrs rsp,sqlp
	fuzzyp=0
	matchingfuzzies=""
	dim afuzzies(500)
	do while not rsp.eof
	ic=0
		permitplate=rsp("registration")
		lenplate=len(plate)
		lenpermitplate=len(permitplate)
		if lenplate<lenpermitplate then
			j=lenplate
		else
			j=lenpermitplate
		end if	
		
		result=0
	For i = 1 To j 
		if mid(plate,i,1)=mid(permitplate,i,1) then result=result+1
	
	next
		resultb=0
		For i = j To 1 STEP -1
			if mid(plate,i,1)=mid(permitplate,i,1) then resultb=resultb+1
			next
	
	if resultb>result then result=resultb
	ifuzzyp=result*100
	ifuzzyp=ifuzzyp/j
	if fuzzyp<ifuzzyp then 
		fuzzyp=ifuzzyp
		fpermitplate=permitplate
	'	fpermitid=rsp("id")
	end if
	if fuzzyp>60 and fuzzyp<100 then
		'mystatus="matchedfuzzy" & fuzzyp & "-%"
		'sql="insert into fuzzy" & tablename & " (plate,permitplate,permitid,mystatus) values('" & plate & "','" &  fpermitplate & "'," & fpermitid & ",'" & mystatus & "')"
		'	response.write sql
		'	objconntemp.execute sql
	'check if in array and only show if not iin array already
			if not elementinarray(afuzzies,fpermitplate,0) then 
				 matchingfuzzies=matchingfuzzies & "<tr><td>" & rsp("rdate") & "</td><td>" & rsp("rtime") & "</td><td>" & rsp("lane") & "</td><td>" & rsp("accuracy") & "</td><td>" &  fpermitplate & "</td><td>" & formatnumber(fuzzyp,2) & "%</td></tr>"
				 afuzzies(ic)=fpermitplate
end if
'else	
	'	mystatus="matchedviolator"
	end if
	
	
		rsp.movenext
		ic=ic+1
	loop
	closers rsp
'response.write "<font class=red>fuzzy" & fuzzyp & "</font>"
	
if matchingfuzzies="" then 
	 matchingfuzzies="no matching fuzzies"
else
		matchingfuzzies="<table class=maintable2><tr><td colspan=6 align=center>Plate:" & plate & "</td></tr><tr><th>Date</th><th>Time</th><th>Lane</th><th>Accuracy</th><th>Plate</th><th>% match</th></tr>" & matchingfuzzies & "</table>"

end if
checkfuzzys=matchingfuzzies
end function





sub markspermitexcel

call outputexcelmp("select * from mpermits")
response.write "<br>"
call outputexcelmp("select * from mnumbers")
end sub
sub outputexcelmp(sql)
	'on error resume next
	'response.write sql
	randomize
     nRandom = (100-1)*Rnd+1
     fileExcel =monthname(month(date)) &  cstr(year(date)) & "_" & CStr(nRandom) & ".xls"
     filePath= Server.mapPath("admin.asp")
	 filepath=mid(filepath,1,len(filepath)-9)
	 'response.write "filepath:" & filepath & "<br>"
	  filename=filePath & "excelfiles\" & fileExcel
     'response.write filename
	 Set fs = Server.CreateObject("Scripting.FileSystemObject")
     Set MyFile = fs.CreateTextFile(filename, True)
set objConnmp=server.createobject("ADODB.connection")
objConnmp.Open "markspermits"
      Set rs = objConnmp.Execute(sql)

     strLine="<table><tr>" 'Initialize the variable for storing the filednames

     For each x in rs.fields
             strLine= strLine & "<td>" & x.name & "</td>"
     Next
	 	strline=strline&"</tr>"
       MyFile.writeline strLine

        Do while Not rs.EOF
     strLine="<tr>"
     for each x in rs.Fields
	  	cell=x.value
		strLine= strLine & "<td>" & cell & "</td>"
     next
	 	strline=strline & "</tr>"
		'response.write strline & vbcrlf
     MyFile.writeline strLine
	 
	 err.clear
     rs.MoveNext
  Loop
	myfile.writeline "</table>"
    MyFile.Close
  Set MyFile=Nothing
  Set fs=Nothing

  link="<A HREF=excelfiles/" & fileExcel & " class=adminmenu target=_new>Click Here to Open Excel</a>"
   Response.write "<br>Excel File Created<br><br>" &  link
objconnmp.close

end sub


sub editpayments
sqlm="select * from usermenu where userid=" & session("userid") & " and menu='payments'"
	openrs rsm,sqlm
	if rsm.eof and rsm.bof then 
		response.write "no access"
		response.end
	end if
	closers rsm
select case request("cmd2")

case ""
	%>
	
	<form method=post action=admin.asp?cmd=editpayments&cmd2=search>
	PCN 
	<input type=text name=pcn>
	<input type=submit name=submit value=submit>
	</form>
	
<%
case "search"
		sql="select * from payments where pcn like '" & request("pcn") & "'"
		'response.write sql
		
		openrs rs,sql
		if not rs.eof and not rs.bof then
		do while not rs.eof 
			response.write "Payment for pcn:"  & rs("pcn") & " was received on " & rs("received") & " amount of:" & rs("amount")
			if rs("batchid")>0 then
				response.write "&nbsp;<a class=button href=admin.asp?cmd=editpayments&cmd2=edit&batchid=" & rs("batchid") & ">Edit Batch</a><br>"
			else
				response.write "&nbsp;<a class=button href=admin.asp?cmd=editpayments&cmd2=edit&date=" & rs("received") & ">Edit Batch</a><br>"
			end if
			rs.movenext
			loop
			
			
		else
			response.write "PCN not found"
		end if
		closers rs
case "edit" 
response.write "Edit Payments"
response.write "<form method=post name=editpayments action=admin.asp?cmd=editpayments>"
%>
<table class=maintable2>

<tr><td>ID</td><td>PCN</td><td>PLATE</td><td>Amount</td><td>Date</td><td>Type</td><td>Book Reference</td></tr>
<%
tamount=0
i=0
batchid=request("batchid")
mydate=request("date")
sql="select * from payments where ip<>'80.82.138.201' and "
if batchid<>"" then
i=i+1
sql=sql & " batchid=" & batchid
end if
if mydate<>"" then
i=i+1
sql=sql & " received='" &  cisodate(mydate) & "'"
end if
if i=0 then 
	response.write "you must fill in a search term"
	response.end
end if
'response.write sql
openrs rs,sql
ids=""
do while not rs.eof
	myerrort=""
	id= rs("id")
	response.write "<tr><td>" & id & "</td>"
	

	response.write "<td><input disabled type=text maxLength=10 size=10 name=pcn-" & id & " value='" &  rs("pcn")  & "'></td>"
	
	response.write "<input type=hidden name=pcn-" & id & " value='" &  rs("pcn")  & "'>"
	
	response.write "<td><input type=text size=10 name=plate-" & id & " value='" & rs("plate") &"'></td>"
	response.write "<td><input type=text size=4 name=amount-" & id & " value='" &rs("amount") & "' onKeyPress=""return checkValue(event)""></td>"
	response.write "<td><input type=text size=10 name=date-" & id & "  value='" & rs("received") & "'>"
		response.write "<td><select name=type-" & id & "><option selected value='" & rs("method") & "'>" & rs("method") & "</option><option value='Cheque'>Cheque</option><option value='Postal Order'>Postal Order</option><option value='Credit Card'>Credit Card</option><option value='Credit Card Manual'>Credit Card Manual</option><option value='Newlyn'>Newlyn</option></select></td>"
	'response.write "<td><input type=button class=button name=deleteme value=delete onClick=PaymentsDelete("& i & ");></td>"
	response.write "<td><input type=text size=10 name=bookreference-" & id & "  value='" & rs("bookreference") & "'>"
	response.write "</tr>"
	if ids<>"" then ids=ids & ","
	ids=ids& id
rs.movenext
loop	
	
	

	%>
	<tr><td colspan=6 align=center><input type=submit name=cmd2 value="Refresh" class=button></td></tr>

<input type=hidden name=ids value="<%= ids %>">

</table>
</form>
<%
case "Refresh" 
response.write "<form method=post name=editpayments action=admin.asp?cmd=editpayments>"
response.write "Please confirm the following:<table class=maintable2>"
ids=request("ids")
'response.write ids
aids=split(ids,",")
myerrort=""
for i=lbound(aids) to ubound(aids)
		myerrort=""
		id=aids(i)
  		spcn=request("pcn-" & aids(i))
  		plate=request("plate-" & aids(i))
  		amount=request("amount-"  & aids(i))
  		method=request("type-" & aids(i))
  		received=request("date-" & aids(i))
  		bookreference=request("bookreference-" & aids(i))
  '		response.write "amount:" & amount
			
			if spcn="" then spcn=lookuppcn(plate,myerrort)
  		site=lookupsite(spcn,myerrort)
  		if right(spcn,3)<>site then myerrort="invalid pcn site"
  		if plate="" then plate=lookupplate(spcn,myerrort)
  		sqlckp="select * from vwviolators where pcn=" & tosql(spcn,"text") & " and registration=" & tosql(plate,"text")
  	openrs rsckp,sqlckp
  	if myerrort="" then
  	if  rsckp.eof and  rsckp.bof  then myerrort="PCN does not match plate"
  	end if
  		closers rsckp
  		if amount="" or amount="0"   then myerrort="Amount cannot be blank"
  		response.write "<tr><td>" & id & "</td>"
  	
  
  	response.write "<td><input type=text disabled maxLength=10 size=10 name=pcn-" & id & " value='" &  spcn  & "'></td>"
  	
  		response.write "<input type=hidden name=pcn-" & id & " value='" &  spcn  & "'>"
  	
  	response.write "<td><input type=text size=10 name=plate-" & id & " value='" &plate &"'></td>"
  	response.write "<td><input type=text size=4 name=amount-" & id & " value='" &amount & "' onKeyPress=""return checkValue(event)""></td>"
  	response.write "<td><input type=text size=10 name=date-" & id & "  value='" & received & "'>"
  		response.write "<td><select name=type-" & id & "><option selected value='" & method & "'>" & method & "</option><option value='Cheque'>Cheque</option><option value='Postal Order'>Postal Order</option><option value='Credit Card'>Credit Card</option><option value='Credit Card Manual'>Credit Card Manual</option><option value='Newlyn'>Newlyn</option></select></td>"
  	'response.write "<td><input type=button class=button name=deleteme value=delete onClick=PaymentsDelete("& i & ");></td>"
  	response.write "<td><input type=text size=10 name=bookreference-" & id & "  value='" & bookreference & "'>"
  	response.write "</tr>"
	
		if myerrort<>"" then 
		response.write "<tr><td colspan=6>" & myerrort & "</td></tr>"
		berror=1
	end if	
	'end if


next
response.write "<input type=hidden name=ids value='" & request("ids") & "'>"
if berror=0 then 
	%>
	<tr><td colspan=6 align=center><input type=submit name=cmd2 value="Refresh" class=button>&nbsp;<input type=submit name=cmd2 value="Save" class=button></td></tr>
<%
	
else
%>
<tr><td colspan=6 align=center><input type=submit name=cmd2 value="Refresh" class=button></td></tr>

<%
end if
	response.write "</table></form>"
case "Save" 
ids=request("ids")
aids=split(ids,",")
err1=0
for i=lbound(aids) to ubound(aids)
		spcn=request("pcn-" & aids(i))
		plate=request("plate-" & aids(i))
		amount=request("amount-"  & aids(i))
		method=request("type-" & aids(i))
		received=cisodate(request("date-" & aids(i)))
		bookreference=request("bookreference-" & aids(i))
		n="Number"
		t="text"
		if spcn<>"" then
		sql="update payments set pcn=" & tosql(spcn,t) & ",plate=" & tosql(plate,t) & ",amount=" & tosql(amount,n) & ",method=" & tosql(method,t) & ",received=" & tosql(received,t) & ",bookreference=" & tosql(bookreference,t) & " where id=" & aids(i)
	'response.write sql
		objconn.execute sql
		else
				err1=1
				response.write "pcn cannot be blank"
		end if
next
		if err1=0 then 	response.write "updated successfully"
end select
end sub
sub showplateimages
myfilename=request("filename")

	filename=myfilename
	'response.write filename
						filename= year(request("rdate")) & "/" & month(request("rdate")) & "/" & day(request("rdate")) & "/" & hour(request("rtime")) & "/" & filename
						itemimageurl= "http://cleartonecommunications.com/plateimages/server1/" & filename
						itemimage=configwebdir & "traffica\plateimages\server1\" & filename
						response.write "<img src=""" & itemimageurl & """ " &ImageResize(itemimage,200, 68) & " border=""0"" align=middle valign=middle><br><br>"


						filename=replace(myfilename,"A.jpg","P.jpg")
						filename= year(request("rdate")) & "/" & month(request("rdate")) & "/" & day(request("rdate")) & "/" & hour(request("rtime")) & "/" & filename
						itemimageurl= "http://cleartonecommunications.com/plateimages/server1/" & filename
						itemimage=configwebdir & "traffica\plateimages\server1\" & filename
						response.write "<img src=""" & itemimageurl & """ " &ImageResize(itemimage,200, 68) & " border=""0"" align=middle valign=middle><br><br>"

					filename=replace(myfilename,"A.jpg","F.jpg")
						filename= year(request("rdate")) & "/" & month(request("rdate")) & "/" & day(request("rdate")) & "/" & hour(request("rtime")) & "/" & filename
						itemimageurl= "http://cleartonecommunications.com/plateimages/server1/" & filename
						itemimage=configweburl & "traffica\site1\plateimages\server1\" & filename
						response.write "<img src=""" & itemimageurl & """ " &ImageResize(itemimage,200, 68) & " border=""0"" align=middle valign=middle><br><br>"
	
						
end sub
sub appealpara
arrform=array("ref","shortparagraph","paragraph")
arrformcaption=array("Reference #","Short Paragraph","Paragraph")
arrformtype=array("t","tas","ta")
cmdname="appealpara"
tablename="appealpara"
idfield="id"
mainfieldname="Appeal Paragraph"

orderby="shortparagraph"
'response.write "x" & request("cmd2") & "x"
if request("cmd2")="add" then
	response.write "<form method=post action=admin.asp?cmd=" & cmdname & "&cmd2=savenew><table class=maintable2>"
	for i= lbound(arrform) to ubound(arrform)
	response.write "<tr><td>" & arrformcaption(i) & "</td><td>" & createeditinput(arrform(i),arrformtype(i),"") & "</td></tr>"
	next
	response.write "<tr><td colspan=2 align=center><input type=submit value=Save></td></tr></table></form>"
end if
if request("cmd2")="savenew" then
	sql="insert into " & tablename & " (" & join(arrform,",") & ") values (" 
	for i=lbound(arrform) to ubound(arrform)
	sql=sql & tosqla(request.form(arrform(i)),arrformtype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & ")"
	'response.write sql
	objconn.execute sql
	response.write jsalert("Added Successfully")
	response.redirect("admin.asp?cmd=" & cmdname & "&cmd2=list")
end if
if request("cmd2")="edit" then
	if request("id")="" then
		response.write "<form method=post action=admin.asp?cmd=" & cmdname & "&cmd2=edit>"
		response.write "<table class=maintable2><tr><td>Choose " & mainfieldname & " to Edit</td></tr>"
		response.write "<tr><td><select name=id>"
		sql="select * from " & tablename & " order by " & orderby
		openrs rs,sql
		do while not rs.eof
		response.write "<option value='" & rs("id") & "'>" & rs(orderby) & "</option>"
		rs.movenext
		loop
		closers rs
		response.write "</select><input type=submit name=submit value=Edit></td></tr></table></form>"
	else
		
		
		response.write "<form method=post action=admin.asp?cmd=" & cmdname & "><table class=maintable2>"
		response.write "<input type=hidden name=id value=" & request("id") & "><input type=hidden name=cmd2 value=saveedit>"
		sqle="select * from " & tablename & " where " & idfield & "=" & request("id")
		openrs rse,sqle
		for i= lbound(arrform) to ubound(arrform)
		response.write "<tr><td>" & arrformcaption(i) & "</td><td>" & createeditinput(arrform(i),arrformtype(i),rse(arrform(i))) & "</td></tr>"
		next
		closers rse
		response.write "<tr><td colspan=2 align=center><input type=submit value=Save></td></tr></table></form>"
	end if
	end if
	if request("cmd2")="saveedit" then
	sql="update " & tablename & " set "
	for i=lbound(arrform) to ubound(arrform)
	sql=sql & arrform(i) & "=" & tosqla(request.form(arrform(i)),arrformtype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & " where " & idfield & "=" & request("id")
	'response.write sql
	objconn.execute sql
	response.write jsalert("Edited Successfully")
	response.redirect("admin.asp?cmd=" & cmdname & "&cmd2=list")
end if
if request("cmd2")="delete" then
	if request("id")="" then
		response.write "<form method=post action=admin.asp?cmd=" & cmdname & "&cmd2=delete onsubmit=""return confirm('Are you sure you want to delete?')"">"
		response.write "<table class=maintable2><tr><td>Choose " & mainfieldname &  " to Delete</td></tr>"
		response.write "<tr><td><select name=id>"
		sql="select * from " & tablename & " order by " & orderby
		openrs rs,sql
		do while not rs.eof
		response.write "<option value='" & rs("id") & "'>" & rs(orderby) & "</option>"
		rs.movenext
		loop
		closers rs
		response.write "</select><input type=submit name=submit value=Delete></td></tr></table></form>"
	else
		sqld="delete from " & tablename & " where id=" & request("id")
		objconn.execute sqld
		response.write jsalert("Deleted Successfully")
		response.redirect("admin.asp?cmd=" & cmdname & "&cmd2=list")
	end if
	end if

if request("cmd2")="list" then
response.write "<table class=maintable2><tr><th>" & mainfieldname & "</th><th>Actions</th></tr>" & vbcrlf
sql="select * from " & tablename
openrs rs,sql
do while not rs.eof
response.write "<tr><td>" & rs(orderby) & "</td><td align=center><a href=admin.asp?cmd=" & cmdname & "&cmd2=edit&id=" & rs(idfield) & ">Edit</a>&nbsp;<a href=admin.asp?cmd=" & cmdname & "&cmd2=delete&id=" & rs(idfield) & " onclick=""return confirm('Are you sure you want to Delete')"">Delete</a></td></tr>" & vbcrlf
rs.movenext
loop
closers rs
response.write "</table>"
end if
end sub
sub appealletters

if request("cmd2")="" then
%>
<table class=maintable2>

<form method=post action=admin.asp?cmd=appealletters&cmd2=new>
<tr><td colspan=2>Enter in a pcn or plate </td></tr>
<tr><td>pcn:</td><td> <input type=text name=pcn></td></tr>
<tr><td>plate:</tD><tD> <input type=text name=plate></td></tr>
<tr><td colspan=2 align=center><input type=submit  value="Submit"></td></tr>
</form>
</table>

<%
end if

if request("cmd2")="new" then
	spcn=request("pcn")
splate=request("plate")

if request("s")=1 then
		sql=session("sql")
		i=1
		rcount=2
	else
		sqlw="SELECT violators.ID, violators.Registration, violators.[Date of Violation], violators.Site,  violators.PCN+violators.site as pcn FROM violators where  "
		sqlc="select count(ID) as countc from violators where "
		i=0
		if len(spcn)=10 then
		'check that valid site
		sqlcs="select sitecode from [site information] where sitecode='" & right(spcn,3) & "'"
		
		openrs rscs,sqlcs
		if  rscs.eof and  rscs.bof then
			 response.write "no site information available"
			 exit sub
		closers rscs
		end if
		end if
		if spcn<>"" then
			sql= sql & "(pcn+site like '%" &spcn & "%'"
			'if len(spcn)>7 then sql=sql & " and site like '%" & right(spcn,3) & "%'"
			sql=sql &  ")"
			i=i+1
		end if 
		if splate<>"" then
			if i>0 then sql=sql & " OR "
			sql=sql & "registration like '%" & splate & "%'"
			i=i+1
		end if 
				if i>0 then
			if session("adminsite")=true then
				sqlsites="select * from [site information]"
			else	
			sqlsites="select * from usersites where userid=" & session("userid")
			end if
openrs rssites,sqlsites  ' check the sites he can see 
if rssites.eof and rssites.bof then
	 response.write "YOu do not have access to any site information"
	 response.End
end if

i=1
sql=sql & " and ("
do while not rssites.eof
	if i>1 then sql=sql & " OR "
 	sql=sql & "(site='" &  rssites("sitecode") & "')" 
	i=i+1
	rssites.movenext
loop 
closers rssites
	
		sql=sql & ")"		
				
		sqlc=sqlc& sql
		sql=sqlw & sql
		response.write "<!--" &  sql & "-->"
		openrs rsc,sqlc
		rcount=rsc("countc")
		closers rsc 
		end if
	end if
	if i=0 then 
		response.write "You must fill in a search field"
		call pcn
	else
		
		strpagename="admin.asp?cmd=appealletters&cmd2=new"
			
		if rcount=0 then 
			response.write "no records exist with this criteria"
		'elseif rcount=1 then call viewrecord
		'else
			'if rcount=1 then
			'openrs rsa,sql
			'	call viewrecord(rsa("id"))
			'closers rsa
		else
			createSortableLista objConn,sql,"ID",25,"class=maintable2w",strpagename
		end if
		end if
		
	end if
'end if
if request("cmd2")="addletter" then 
	 		call addappealletter(request("id"))
end if
if request("cmd2")="edit" then
 if request("ref")<>"" then response.write "Reference Number:" & request("ref")
appealid=request("appealid")

sql="select * from appealletters where id=" & appealid

'response.write sql
openrs rs,sql

			
response.write getpcndetails(rs("violatorid"))
response.write "<form method=post action=admin.asp?cmd=appealletters&cmd2=save&p=1>"
response.write "<input type=hidden name=appealid value=" & appealid & ">"
response.write "<input type=hidden name=letterid value=" & request("letterid") & ">"
response.write "<textarea name=appealletter id=appealletter>" 

response.write replace(rs("letter"),vbcrlf,"<br>") & "</textarea>"
if 1=0 then 
%>
<script language="javascript1.2">
var config = new Object();    // create new config object

config.width = "70%";
config.height = "200px";
config.bodyStyle = 'background-color: white; font-family: "Verdana"; font-size: x-small;';
config.debug = 0;

// NOTE:  You can remove any of these blocks and use the default config!

config.toolbar = [
    //['fontname'],
    //['fontsize'],
    //['fontstyle'],
   // ['linebreak'],
    ['bold','italic','underline','separator'],
 // ['strikethrough','subscript','superscript','separator'],
    //['justifyleft','justifycenter','justifyright','separator'],
   // ['OrderedList','UnOrderedList','Outdent','Indent','separator'],
  //  ['forecolor','backcolor','separator'],
 //   ['HorizontalRule','Createlink','InsertImage','htmlmode','separator'],
  //  ['about','help','popupeditor'],
];



editor_generate('appealletter',config);
                </script>	
								<input type=hidden name=ref value="<%=request("ref")%>">
<%
end if
response.Write "<input type=hidden name=ref value=" & request("ref") & ">"
response.write "<br><input type=submit name=submit class=button value=Print Letter></form>"
closers rs

end if
if request("cmd2")="save" then
sql="update appealletters set letter=" & tosql(request("appealletter"),"text") & " where id=" & request("appealid")
objconn.execute sql
'response.write "Appeal Saved"
'if request("letterid")=0 then
	' response.write "Letter Saved"
	' call pcnnotes
'else
refnumber=request("ref")
response.redirect "printappeal.asp?id=" & request("appealid")  & "&ref=" & refnumber
'end if
end if
end sub
sub addappealletter(id)
'response.write "add letter for id" & id
if request("cmd3")<>"save" then
%>
<form method=post name=appeal action=admin.asp?cmd=appealletters&cmd2=addletter&cmd3=save>
<input type=hidden name=id value="<%=id%>">
<input type=hidden name=letterid value=<%=request("letter") %>>
<table class=maintable2 width=550>
<tr><td colspan=2>
<%= getpcndetails(id) %>
</td></tr>
<tr>
<td><b>Letter Received Date:</b> <input type=text name=letterrecieveddate><a href="javascript:NewCal('letterrecieveddate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)</td>

</tr><tr>
<td><b>Address:</b><br><textarea name=address cols=40 rows=10><%=getaddress(id)%></textarea></td>
</tr>
<tr><td><b>Choose Appeal Paragraphs below:</b><br>
<%

sqla="select * from appealpara where id<>31 and id<>32 and id<>33 order by shortparagraph "
openrs rsa,sqla
do while not rsa.eof
response.write "<input type=checkbox name=appealpara value=" & rsa("id") & ">" & rsa("shortparagraph") & "<br>"
rsa.movenext
loop
disabledletternum=getdisabledletternum(id)
response.write "<input type=checkbox name=appealpara value=3" & disabledletternum & ">DISABLED<br>"

closers rsa
%>
</td></tr>
<tr><td align=center><input type=submit name=submit value="Create Letter" class=button></td></tr>
</table></form>
<script language="JavaScript" type="text/javascript">
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("appeal");
    frmvalidator.addValidation("letterrecieveddate","req","Please enter letter received date");
  
</script>

<%
else
'response.write request("letterrecieveddate")
'response.write "<hr>today" & date()
lrd=request("letterrecieveddate")
lrdd=dateserial(year(lrd),month(lrd),day(lrd))
if lrdd>date() then
response.write "Letter Recieved date must be in the past -Please go back and fill in"
response.end
end if
site=getsite(id)

sqlac="select * from siteappealpara where site='" & site & "'"
openrs rsac,sqlac
			 if rsac.bof and rsac.eof then
			 		rsac.close
          sqlac="select * from appealconfig where id=1"
          openrs rsac,sqlac
				end if
myletter="<br>"
			
myletter=myletter & "<p>" & request("address") & "<br></p>"
myletter=myletter &   "<P><img src='http://traffica.cleartoneholdings.co.uk/traffica/images/spacer.gif' width='590' height='10'>" & date()& "</p>"                 
myletter=myletter & "<p>" & getpcndetails(id)  & "<br></p>"	
myletter=myletter & "<p align=center><b>RESPONSE TO REPRESENTATION</b></p>" 
myletter=myletter & "<p align=justify>We are in receipt of your letter dated " & request("letterrecieveddate")  & ".  " 
myletter=myletter &  "" & rsac("initpara") & "</p>"
aappealpara=split(request("appealpara"),",")
for i=lbound(aappealpara) to ubound(aappealpara)
myletter=myletter & "<p align=justify>" & getappealpara(aappealpara(i)) & "</p>" 

next
paid=checkbyidifpaid(id)
'response.write "paid:" & paid
if paid=true then
	 myletter=myletter & "<p align=justify>" & rsac("finalparapaid") & "</p>"
else
myletter=myletter & "<p align=justify>" &  rsac("finalpara") & "</p>"
end if
myletter=replace(myletter,"&nbsp","&nbsp;")
'response.write myletter
closers rsac
sql="set nocount on insert into appealletters(violatorid,letter,userid,ip,letterreceiveddate) values(" & id & "," & tosql(myletter,"text") & "," & session("userid") & ",'" & ip & "'," & tosql(request("letterrecieveddate"),"date") & ");"&_
      "SELECT @@IDENTITY AS NewID;"

'response.write "<hr>" & sql
 set rsi=objconn.execute (sql )
 appealid=rsi(0)
if request("letterid")<>"" then
sql="insert into appealresponses(appealid,letterid) values(" & appealid & "," & request("letterid") & ")"
sql="exec insertappealresponse @appealid=" & appealid & ",@letterid=" & request("letterid")
openrs rsa,sql
refnumber="a" & rsa("id")

closers rsa

''update letter
sql="update incomingletters set updatetoclients=1,statusid=2,datestatuschanged=getdate() where id=" & request("letterid")
'response.write sql
objconn.execute sql


end if

response.redirect("admin.asp?cmd=appealletters&p=1&cmd2=edit&appealid=" & appealid& "&ref=" & refnumber & "&letterid=" & letterid)
end if
end sub
sub cancelledletters

if request("cmd2")="" then
%>
<table class=maintable2>

<form method=post action=admin.asp?cmd=cancelledletters&cmd2=new>
<tr><td colspan=2>Enter in a pcn or plate </td></tr>
<tr><td>pcn:</td><td> <input type=text name=pcn></td></tr>
<tr><td>plate:</tD><tD> <input type=text name=plate></td></tr>
<tr><td colspan=2 align=center><input type=submit  value="Submit"></td></tr>
</form>
</table>

<%
end if

if request("cmd2")="new" then
	spcn=request("pcn")
splate=request("plate")

if request("s")=1 then
		sql=session("sql")
		i=1
		rcount=2
	else
		sqlw="SELECT violators.ID, violators.Registration, violators.[Date of Violation], violators.Site,  violators.PCN+violators.site as pcn FROM violators where  "
		sqlc="select count(ID) as countc from violators where "
		i=0
		if len(spcn)=10 then
		'check that valid site
		sqlcs="select sitecode from [site information] where sitecode='" & right(spcn,3) & "'"
		
		openrs rscs,sqlcs
		if  rscs.eof and  rscs.bof then
			 response.write "no site information available"
			 exit sub
		closers rscs
		end if
		end if
		if spcn<>"" then
			sql= sql & "(pcn+site like '%" &spcn & "%'"
			'if len(spcn)>7 then sql=sql & " and site like '%" & right(spcn,3) & "%'"
			sql=sql &  ")"
			i=i+1
		end if 
		if splate<>"" then
			if i>0 then sql=sql & " OR "
			sql=sql & "registration like '%" & splate & "%'"
			i=i+1
		end if 
				if i>0 then
			if session("adminsite")=true then
				sqlsites="select * from [site information]"
			else	
			sqlsites="select * from usersites where userid=" & session("userid")
			end if
openrs rssites,sqlsites  ' check the sites he can see 
if rssites.eof and rssites.bof then
	 response.write "YOu do not have access to any site information"
	 response.End
end if

i=1
sql=sql & " and ("
do while not rssites.eof
	if i>1 then sql=sql & " OR "
 	sql=sql & "(site='" &  rssites("sitecode") & "')" 
	i=i+1
	rssites.movenext
loop 
closers rssites
	
		sql=sql & ")"		
				
		sqlc=sqlc& sql
		sql=sqlw & sql
		response.write "<!--" &  sql & "-->"
		openrs rsc,sqlc
		rcount=rsc("countc")
		closers rsc 
		end if
	end if
	if i=0 then 
		response.write "You must fill in a search field"
		call pcn
	else
		
		strpagename="admin.asp?cmd=cancelledletters&cmd2=new"
			
		if rcount=0 then 
			response.write "no records exist with this criteria"
		'elseif rcount=1 then call viewrecord
		'else
			'if rcount=1 then
			'openrs rsa,sql
			'	call viewrecord(rsa("id"))
			'closers rsa
		else
			createSortableLista objConn,sql,"ID",25,"class=maintable2w",strpagename
		end if
		end if
		
	end if
'end if
if request("cmd2")="addletter" then 
	 		call addcancelledletter(request("id"))
end if
if request("cmd2")="edit" then
 if request("ref")<>"" then response.write "Reference Number:" & request("ref")
appealid=request("appealid")

sql="select * from cancelledletters where id=" & appealid
openrs rs,sql

			
response.write getpcndetails(rs("violatorid"))
response.write "<form method=post action=admin.asp?cmd=cancelledletters&cmd2=save&p=1>"
response.write "<input type=hidden name=appealid value=" & appealid & ">"

response.write "<textarea name=appealletter>" & replace(rs("letter"),vbcrlf,"<br>") & "</textarea>"
%>
	
								<input type=hidden name=ref value="<%=request("ref")%>">
								<input type=hidden name=cancelid value="<%=request("cancelid") %>" />
<%
response.write "<br><input type=submit name=submit class=button value=Print Letter></form>"
closers rs

end if
if request("cmd2")="save" then
sql="update cancelledletters set letter=" & tosql(request("appealletter"),"text") & " where id=" & request("appealid")
objconn.execute sql
'response.write "Appeal Saved"

response.redirect "printcancel.asp?id=" & request("appealid") & "&c=1&ref=" & request("ref") 

end if
end sub
sub addcancelledletter(id)

'response.write "add letter for id" & id
if request("cmd3")<>"save" then
%>
<form method=post name=appeal action=admin.asp?cmd=cancelledletters&cmd2=addletter&cmd3=save>
<input type=hidden name=id value="<%=id%>">
<input type=hidden name=nocharge value="<%=request("nocharge") %>" />
<input type=hidden name=letterid value=<%=request("letter") %>>
<input type=hidden name=cancelid value="<%=request("cancelid") %>" />
<table class=maintable2 width=550>
<tr><td colspan=2>
<%= getpcndetails(id) %>
</td></tr>
<tr>
<td><b>Letter Received Date:</b> <input type=text name=letterrecieveddate><a href="javascript:NewCal('letterrecieveddate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)</td>

</tr><tr>
<td><b>Address:</b><br><textarea name=address cols=40 rows=10><%=getaddress(id)%></textarea></td>
</tr>

<tr><td align=center><input type=submit name=submit value="Create Letter" class=button></td></tr>
</table></form>
<script language="JavaScript" type="text/javascript">
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("appeal");
    frmvalidator.addValidation("letterrecieveddate","req","Please enter letter received date");
  
</script>

<%
else
'response.Write " no charge:" & request("nocharge")
'response.write request("letterrecieveddate")
'response.write "<hr>today" & date()
lrd=request("letterrecieveddate")
lrdd=dateserial(year(lrd),month(lrd),day(lrd))
if lrdd>date() then
response.write "Letter Recieved date must be in the past -Please go back and fill in"
response.end
end if
if request("nocharge")="1" then
    myletter="<br><br><br>"
    			
    myletter=myletter & "<p>" & request("address") & "</p><br>"
    myletter=myletter &   "<P><img src='http://traffica.cleartoneholdings.co.uk/traffica/images/spacer.gif' width='590' height='10'>"  & date()& "</p>"                 
    myletter=myletter & "<p>" & getpcndetails(id)  & "<br>"	

    myletter=myletter & "<p align=center><b>CONFIRMATION OF CANCELLATION</b></p>" 

    myletter=myletter & "<p align=justify>We refer to recent communications with this office in respect of the Parking Contravention Enforcement Notice.<br><br>We now confirm that this Notice has been cancelled.<br><br>Yours faithfully,<br><br><br><br><b><u>Representations Team</u></b>"

else
    myletter="<br><br><br>"
			
    myletter=myletter & "<p>" & request("address") & "</p><br>"
    myletter=myletter &   "<p align=right>"  & date()& "</p>"                 
    myletter=myletter & "<p>" & getpcndetails(id)  & "<br>"	

    myletter=myletter & "<p align=center><b>CONFIRMATION OF CANCELLATION</b></p>" 

    myletter=myletter & "<p align=justify>We refer to recent communications with this office in respect of the Parking Contravention Enforcement Notice.<br><br>We now confirm that this Notice has been cancelled. You must pay a fee of &pound15<br><br>Yours faithfully,<br><br><br><br><b><u>Representations Team</u></b>"


end if
myletter=replace(myletter,"<p>&nbsp</p>","<p>&nbsp;</p>")
'response.Write myletter 
'response.End 
'paid=checkbyidifpaid(id)
'response.write "paid:" & paid
'if paid=true then
	' myletter=myletter & "<p align=justify>" & rsac("finalparapaid") & "</p>"
'else
'myletter=myletter & "<p align=justify>" &  rsac("finalpara") & "</p>"
'end if
'response.write myletter
'closers rsac
sql="set nocount on insert into cancelledletters(violatorid,letter,userid,ip,letterreceiveddate) values(" & id & "," & tosql(myletter,"text") & "," & session("userid") & ",'" & ip & "'," & tosql(request("letterrecieveddate"),"date") & ");"&_
      "SELECT @@IDENTITY AS NewID;"

'response.write "<hr>" & sql
 set rsi=objconn.execute (sql )
 appealid=rsi(0)
if request("letterid")<>"" then

    sql="exec insertcancelresponse @appealid=" & appealid & ",@letterid=" & request("letterid")
    openrs rsa,sql
    'refnumber="c" & rsa("id")

    closers rsa
    sql="update incomingletters set updatetoclients=1,statusid=3,datestatuschanged=getdate() where id=" & request("letterid")
    'response.write sql
    objconn.execute sql
    ip=Request.ServerVariables("remote_addr")
    if request("cancelid")="" then
        sql="exec cancelbyid @id=" & id & ",@ip='" & ip& "',@userid=" & session("userid") 
        response.write sql
        openrs rsc,sql
        refnumber=rsc("ref")
        closers rsc
    'return id and make it the cancelled letter
    else
        refnumber=request("cancelid")
    end if
end if
sql="update cancelledresponses set ref='" & refnumber & "' where appealid=" & appealid & " and letterid=" & request("letterid")
response.write sql
objconn.execute sql
response.redirect ("admin.asp?cmd=cancelledletters&p=1&cmd2=edit&appealid=" & appealid& "&ref=" & refnumber)
end if
end sub
function getsite(violatorid)
sqls="select site from violators where id=" & violatorid
openrs rss,sqls
getsite=rss("site")
closers rss

end function
function getpcndetails(violatorid)
sql="SELECT    VIOLATORS.*, DVLA.Reg, DVLA.Owner, DVLA.Address1, DVLA.Address2, DVLA.Address3, DVLA.Town, DVLA.Postcode FROM  VIOLATORS LEFT OUTER JOIN DVLA ON VIOLATORS.dvlaid = DVLA.id where violators.id=" & violatorid
'response.write sql
openrs rs,sql
getpcndetails="<table>"
getpcndetails=getpcndetails & "<tr><td><b>Vehicle Registration</b></td><td>" & rs("registration") & "</td></tr>"
getpcndetails=getpcndetails &"<tr><td><b>PCN Reference</b></td><td>" & rs("pcn") & rs("site") &  "</td></tr>"
getpcndetails=getpcndetails &"<tr><td><b>Date of Incident</b></td><td>" & rs("date of violation") & "</td></tr>"
getpcndetails=getpcndetails &"<tr><td><b>Site Details</b></td><td>" & getsitename(rs("site")) & "</td></tr>"
getpcndetails=getpcndetails &"</table>"

closers rs
end function
function checkbyidifpaid(violatorid)
sqls="exec checkbyidifpaid @id=" & violatorid
openrs rss,sqls
checkbyidifpaid=rss("paid")
closers rss

end function
function getaddress(violatorid)
sql="SELECT    VIOLATORS.*, DVLA.Reg, DVLA.Owner, DVLA.Address1, DVLA.Address2, DVLA.Address3, DVLA.Town, DVLA.Postcode FROM  VIOLATORS LEFT OUTER JOIN DVLA ON VIOLATORS.dvlaid = DVLA.id where violators.id=" & violatorid
'response.write sql
openrs rs,sql

getaddress=rs("owner") & vbcrlf & rs("address1") & vbcrlf 
if rs("address2")<>"" then getaddress=getaddress & rs("address2") & vbcrlf 
if rs("address3")<>"" then getaddress=getaddress & rs("address3") & vbcrlf 
if rs("town")<>"" then getaddress=getaddress & rs("town") & vbcrlf 
if rs("postcode")<>"" then getaddress=getaddress & rs("postcode") & vbcrlf 

closers rs
end function
function getdisabledletternum(violatorid)
sql="select site,disabledletter from violators v left join [site information] s on v.site=s.sitecode where v.id=" & violatorid
'response.write sql
openrs rs,sql
getdisabledletternum=rs("disabledletter")
closers rs
end function
function getsitename(site)
sql="select [name of site] from [site information] where sitecode=" & site
openrs rs,sql
if not rs.eof and not rs.bof then
getsitename=rs("name of site")
else
getsitename=site
end if
end function
function getappealpara(id)
sqla="select * from appealpara where id=" & id
'response.write sqla
openrs rsa,sqla
if not rsa.bof and not rsa.eof then getappealpara=replace(rsa("paragraph"),vbcrlf,"<br>")
'if right(getappealpara,4)="<br>" then
'getappealpara=left(getappealpara,len(getappealpara)-4)
'end if
closers rsa
end function


sub appealconfig
arrform=array("initpara","finalpara","finalparapaid")
arrformcaption=array("Initial Paragraph","Final Paragraph","Final Paragraph Paid")
arrformtype=array("ta","ta","ta")
cmdname="appealconfig"
tablename="appealconfig"
idfield="id"
mainfieldname="Paragraph"
orderby="id"
'response.write "x" & request("cmd2") & "x"
if request("cmd2")="add" then
	response.write "<form method=post action=admin.asp?cmd=" & cmdname & "&cmd2=savenew><table class=maintable2>"
	for i= lbound(arrform) to ubound(arrform)
	response.write "<tr><td>" & arrformcaption(i) & "</td><td>" & createeditinput(arrform(i),arrformtype(i),"") & "</td></tr>"
	next
	response.write "<tr><td colspan=2 align=center><input type=submit value=Save></td></tr></table></form>"
end if
if request("cmd2")="savenew" then
	sql="insert into " & tablename & " (" & join(arrform,",") & ") values (" 
	for i=lbound(arrform) to ubound(arrform)
	sql=sql & tosqla(request.form(arrform(i)),arrformtype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & ")"
	'response.write sql
	objconn.execute sql
	response.write jsalert("Added Successfully")
	response.redirect("admin.asp?cmd=" & cmdname & "&cmd2=list")
end if
if request("cmd2")="edit" then
	if request("id")="" then
		response.write "<form method=post action=admin.asp?cmd=" & cmdname & "&cmd2=edit>"
		response.write "<table class=maintable2><tr><td>Choose " & mainfieldname & " to Edit</td></tr>"
		response.write "<tr><td><select name=id>"
		sql="select * from " & tablename & " order by " & orderby
		openrs rs,sql
		do while not rs.eof
		response.write "<option value='" & rs("id") & "'>" & rs(orderby) & "</option>"
		rs.movenext
		loop
		closers rs
		response.write "</select><input type=submit name=submit value=Edit></td></tr></table></form>"
	else
		
		
		response.write "<form method=post action=admin.asp?cmd=" & cmdname & "><table class=maintable2>"
		response.write "<input type=hidden name=id value=" & request("id") & "><input type=hidden name=cmd2 value=saveedit>"
		sqle="select * from " & tablename & " where " & idfield & "=" & request("id")
		openrs rse,sqle
		for i= lbound(arrform) to ubound(arrform)
		response.write "<tr><td>" & arrformcaption(i) & "</td><td>" & createeditinput(arrform(i),arrformtype(i),rse(arrform(i))) & "</td></tr>"
		next
		closers rse
		response.write "<tr><td colspan=2 align=center><input type=submit value=Save></td></tr></table></form>"
	end if
	end if
	if request("cmd2")="saveedit" then
	sql="update " & tablename & " set "
	for i=lbound(arrform) to ubound(arrform)
	sql=sql & arrform(i) & "=" & tosqla(request.form(arrform(i)),arrformtype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & " where " & idfield & "=" & request("id")
	'response.write sql
	objconn.execute sql
	response.write "Paragraphs edited successfully"
	'response.write jsalert("Edited Successfully")
	'response.redirect("admin.asp?cmd=" & cmdname & "&cmd2=list")
end if
if request("cmd2")="delete" then
	if request("id")="" then
		response.write "<form method=post action=admin.asp?cmd=" & cmdname & "&cmd2=delete onsubmit=""return confirm('Are you sure you want to delete?')"">"
		response.write "<table class=maintable2><tr><td>Choose " & mainfieldname &  " to Delete</td></tr>"
		response.write "<tr><td><select name=id>"
		sql="select * from " & tablename & " order by " & orderby
		openrs rs,sql
		do while not rs.eof
		response.write "<option value='" & rs("id") & "'>" & rs(orderby) & "</option>"
		rs.movenext
		loop
		closers rs
		response.write "</select><input type=submit name=submit value=Delete></td></tr></table></form>"
	else
		sqld="delete from " & tablename & " where id=" & request("id")
		objconn.execute sqld
		response.write jsalert("Deleted Successfully")
		response.redirect("admin.asp?cmd=" & cmdname & "&cmd2=list")
	end if
	end if

if request("cmd2")="list" then
response.write "<table class=maintable2><tr><th>" & mainfieldname & "</th><th>Actions</th></tr>" & vbcrlf
sql="select * from " & tablename
openrs rs,sql
do while not rs.eof
response.write "<tr><td>" & rs(orderby) & "</td><td align=center><a href=admin.asp?cmd=" & cmdname & "&cmd2=edit&id=" & rs(idfield) & ">Edit</a>&nbsp;<a href=admin.asp?cmd=" & cmdname & "&cmd2=delete&id=" & rs(idfield) & " onclick=""return confirm('Are you sure you want to Delete')"">Delete</a></td></tr>" & vbcrlf
rs.movenext
loop
closers rs
response.write "</table>"
end if
end sub
sub viewappealletter

id=request("id")
'sqlap="select appealletters.id as id,appealresponses.id as ref from appealletters inner join appealresponses on appealletters.id=appealresponses.appealid where appealletters.id=" & id
sqlap="select * from appealletters where id=" & id
'response.write sqlap
openrs rsap,sqlap
ref=getappealref(id)
mydate=rsap("date")
'response.redirect "printappeal.asp?id=" & rsap("id") & "&ref=" & ref 
'response.write "<div align=left>" & replace (rsap("letter"),vbcrlf,"<br>") & "</div>"
'response.write "<a href=printappeal.asp?id=" & rsap("id") & " class=button>Print Appeal</a>"


set fs=Server.CreateObject("Scripting.FileSystemObject")
'if fs.FileExists(configwebdir & "\traffica\incomingletters\appeals\" & year(mydate) & "\" & month(mydate) & "\" &  id & ".pdf")=true then
if 1=0 then 
response.Redirect configweburl & "\incomingletters\appeals\"& year(mydate) & "\" & month(mydate) & "\" &  id & ".pdf"
else
   ' openrs rsap,sqlap
    
    response.redirect "printappeal.asp?id=" & rsap("id") & "&ref=" & ref 
    'closers rsap
end if
set fs=nothing

end sub
function getappealref(id)
sql="select * from appealresponses where appealid=" & id
openrs rs,sql
if not rs.eof and not rs.bof then 
	 getappealref=rs("id")
else
		getappealref=""
end if 

closers rs
end function

sub viewcancelledletter

id=request("id")
sqlap="select * from cancelledletters inner join cancelledresponses on cancelledletters.id=cancelledresponses.appealid where cancelledletters.id=" & id
response.Write sqlap
openrs rsap,sqlap
mydate=rsap("date")
'response.redirect "printappeal.asp?id=" & rsap("id") & "&c=1&ref=" & rsap("ref") 
set fs=Server.CreateObject("Scripting.FileSystemObject")
if  fs.FileExists(configwebdir & "traffica\incomingletters\cancelled\" & year(mydate) & "\" & month(mydate) & "\" & rsap("appealid") & ".pdf")=true then
'response.Write "exists"

response.redirect configweburl & "\incomingletters\cancelled\"& year(mydate) & "\" & month(mydate) & "\" & rsap("appealid") & ".pdf"
else
'    openrs rsap,sqlap
 
    response.redirect "printappeal.asp?id=" & rsap("appealid") & "&c=1&ref=" & rsap("ref")  
 '   closers rsap
end if
closers rsap
set fs=nothing
end sub
sub viewletter

id=request("id")
sqlap="select * from generalletters where id=" & id
 openrs rsap,sqlap
'response.write "<div align=left>" & replace (rsap("letter"),vbcrlf,"<br>") & "</div>"
'response.write "<a href=printappeal.asp?id=" & rsap("id") & " class=button>Print Appeal</a>"

set fs=Server.CreateObject("Scripting.FileSystemObject")
if  fs.FileExists(configwebdir & "\incomingletters\letter\" & year(rsap("date")) & "\" & month(rsap("date")) & "\" &  id & ".pdf")=true then
response.Redirect configweburl & "\incomingletters\letter\" & year(rsap("date")) & "\" & month(rsap("date")) & "\" &  id & ".pdf"
else
   
    ref=rsap("ref")
    response.redirect "printletter.asp?id=" & rsap("id")  & "&ref=" & ref

end if
    closers rsap
set fs=nothing

end sub
sub viewviolatorticket
id=request("id")
set fs=Server.CreateObject("Scripting.FileSystemObject")
'if fs.FileExists(configwebdir & "\traffica\violatortickets\" & id & ".pdf")=true then
if 1=0 then
response.Redirect configweburl & "\violatortickets\" & id & ".pdf"
else
    
   ' response.redirect "printbatchpcn.asp?id=" & id
   call printticket(id)
   response.Redirect configweburl & "\violatortickets\" & id & ".pdf"
   
end if
set fs=nothing



end sub
Sub createSortableLista(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
	'	if strpagename="admin.asp?cmd=paymentssort" then
    'if strsort="PCNdesc" then strsort="PCNdesc"
	' if strsort="iddesc" then strsort="iddesc"
	'  if strsort="PCN" then strsort="payments.pcn"
	' if strsort="ID" then strsort="violators.id"
  ' response.write "aa" & strsort & "aa"
	'if strsort="Date" then response.write "zadafsdfasdkfads"	
	  if strsort="Date" then strsort="[Date of Violation]"
	    if strsort="violators.[Date" then strsort="[Date of Violation]"
		  if strsort="violators.[Datedesc" then strsort="[Date of Violation]desc"
	    if strsort="Datedesc" then strsort="[Date of Violation]desc"
	'	 if strsort="Received" then strsort="payments.[Received]"
	 '   if strsort="Received" then strsort="payments.[Received]desc"	
    
	'end if
	'response.write "strsort:" & strsort	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
  '  response.write "xx" & strsort & "xx"			
    		Set RS = server.CreateObject("adodb.recordset")
   ' response.write strsql & " order by " & replace(strSort,"desc"," desc")
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"ID" and field.name<>"id" then
				Response.Write "<TD align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</TD>"	& vbcrlf
    			end if
	
	Next
					
    		Response.Write "<td>&nbsp;</td></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			Response.Write "<TR>" & vbcrlf
    			For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"ID" and field.name<>"id" then
					Response.Write "<TD align=center>" & vbcrlf
    				 Response.Write field.value
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
				if rs("ID")<>"" then
			
					
			response.write "<td><a  href=admin.asp?cmd=appealletters&p=1&cmd2=addletter&id=" & rs("ID") & " class=button target=_new>Create Appeal</a></td>"
										 sqlap="select * from appealletters where violatorid=" & rs("id")
										 openrs rsap,sqlap
										 	if not rsap.eof and not rsap.bof then 
											response.write "<td>"			
														do while not rsap.eof
															 response.write "<a href=admin.asp?cmd=viewappealletter&p=1&id=" & rsap("id") & " target=_new>View Apeal dated " & rsap("date") & "</a><Br>"
														rsap.movenext
														loop
										 response.write "</td>"
										 end if			
										closers rsap				
				end if
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "<TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing			
    	End Sub
			
			sub fintodays
			%>
			<table class=maintable2>
<tr>
<td colspan=2><b>Today's Overview </b> </td>
</tr>
<%
sql="exec sptodaysoverview"
openrs rs,sql
%>
<tr><td colspan=2><b>Analysis</b></td></tr>  
<!--<tr><td>Completed 1st Stage</td><td> <%=rs("anal1")%></td></tr> -->
<tr><td>Completed 2nd Stage</td><td> <%=rs("anal2")%></td></tr> 
<tr><td>Waiting (1st + 2nd)</td><td> <%=rs("analwaiting")%></td></tr> 
<tr><td colspan=2><b>Financial</b></td></tr><%
if rs("paymentscheque")="" or isnull(rs("paymentscheque")) then
	 	 paymentscheque=0
else
		paymentscheque=formatnumber(rs("paymentscheque"),2)
end if
if isnull(rs("paymentscc")) then
	 	 paymentsvv=0
else
		paymentscc=formatnumber(rs("paymentscc"),2)
end if

if isnull(rs("paymentsother")) then
	 	 paymentsother=0
else
		paymentsother=formatnumber(rs("paymentsother"),2)
end if
messagesover3=rs("messagesover3")
messagestoday=rs("messagestoday")

%>  
<tr><td>Cheque</td><td> £<%=paymentscheque%></td></tr> 
<tr><td>Credit Card</td><td> £<%=paymentscc%></td></tr> 
<tr><td>Other </td><td>£<%=paymentsother%></td></tr> 
<tr><td>Dvla Outstanding over 7 days</td><tD><%=rs("dvlaoutstanding")%></td></tr>

<tr><td>Messages  over 3hours (not responded)</td><tD><%=rs("messagesover3")%></td></tr>
<tr><td>Messages today</td><tD><%=rs("messagestoday")%></td></tr>

 <tr><td colspan=2><b>Performance</td></tr>
 
   
 
 <%sql2="exec spincoming @userid=" & session("userid")
openrs rs2,sql2
%>
 
<tr><td>Outgoing Letters </td><td><%=rs2("appealstoday")%></td></tr> 
 <tr><td>Re-Issues Waiting (+7 days)</td><td><%=rs2("totalreissuetbc")%></td></tr>
 
<!--<tr><td>Cancellations</td><td> <%= rs("canctoday")%></td></tr>--> 
<tr><td>Notes</td><td> <%=rs2("notestoday") %></td></tr> 


<tr><Td>Scanned Post  </td><td><%=rs2("totaltoday")%></tD></tr>

<tr><Td>Outstanding Post     </td><td><%=rs2("outstanding")%></tD></tr>
<tr><Td>Outstanding Post (+3 Days)    </td><td><%=rs2("outstanding3")%></tD></tr>

 
 
 
</table>
<%closers rs
closers rs2 %>
<%
			
			
			end sub
			
			sub reprinttickets
			if request("batchid")="" then
			sql="select * from pcnbatch order by batchid desc"
			response.write "<form method=post action=printbatchpcn.asp target=_new>Choose Batch"
			response.write "<select name=batchid>"
			openrs rs,sql
			do while not rs.eof
				 response.write "<option value=" & rs("batchid") & ">" & rs("date") & "</option>"
				 			
			rs.movenext
			loop
			response.write "</select><input type=submit name=submit class=button value=Reprint></form>"
			closers rs
			else
			
			
			
			
			end if
			end sub
			
			sub reprintreissues
		
			sql="select * from reprintbatches order by batchid desc"
			response.write "<form method=post action=reprintbatchreissues.asp target=_new>Choose Batch"
			response.write "<select name=batchid>"
			openrs rs,sql
			do while not rs.eof
				 response.write "<option value=" & rs("batchid") & ">" & rs("datetime") & " BatchID:" & rs("batchid") & "</option>"
				 			
			rs.movenext
			loop
			response.write "</select><input type=submit name=submit class=button value=Reprint></form>"
			closers rs
			
			
			
			
			
			end sub
			
			sub reprintreminders
		
			sql="select * from remindersbatches order by batchid desc"
			response.write "<form method=post action=printreminders.asp target=_new>Choose Batch"
			response.write "<select name=batchid>"
			openrs rs,sql
			do while not rs.eof
				 response.write "<option value=" & rs("batchid") & ">" & rs("datetime") & " BatchID:" & rs("batchid") & "</option>"
				 			
			rs.movenext
			loop
			response.write "</select><input type=submit name=submit class=button value=Reprint></form>"
			closers rs
			
			
			
			
			
			end sub
			
			sub reprintreminderstoexcel
		
		if request("cmd2")="" then
			sql="select * from remindersbatches order by batchid desc"
			response.write "<form method=post action=admin.asp?cmd=reprintreminderstoexcel&cmd2=go target=_new>Choose Batch"
			response.write "<select name=batchid>"
			openrs rs,sql
			do while not rs.eof
				 response.write "<option value=" & rs("batchid") & ">" & rs("datetime") & " BatchID:" & rs("batchid") & "</option>"
				 			
			rs.movenext
			loop
			response.write "</select><input type=submit name=submit class=button value=Reprint></form>"
			closers rs
			
			else
			sqlpn="select reminders.pcn,violators.id from reminders left join violators on violators.pcn=reminders.pcn where reminders.batchid=" & batchid & " order by violators.pcnsent,violators.site,violators.[date of violation],violators.registration"
sql="SELECT dbo.violators.ID, dbo.violators.Registration, convert(varchar,[date of violation],103) as [date of violation],convert(varchar,[arrival time],108) as [arrival time],convert(varchar,[departure time],108) as [departure time],convert(varchar,[duration of stay],108) as [duration of stay], violators.pcnsent, dbo.DVLA.Reg, dbo.DVLA.Make, dbo.DVLA.Model, dbo.DVLA.Colour, dbo.DVLA.Owner, dbo.DVLA.Address1 AS dvlaaddress1,dbo.DVLA.Address2 AS dvlaaddress2, dbo.DVLA.Address3 AS dvlaaddress3, dbo.DVLA.Town AS dvlatown, dbo.DVLA.Postcode AS dvlapostcode, dbo.DVLA.[DVLA Status], dbo.DVLA.DateReceived, dbo.DVLA.Transfered, dbo.DVLA.Auto2, dbo.DVLA.Auto1, dbo.DVLA.Auto3, dbo.[Site Information].SiteCode, dbo.[Site Information].[Name of Site], dbo.[Site Information].Borough, dbo.[Site Information].Address3, dbo.[Site Information].Address1, dbo.[Site Information].Address2, dbo.[Site Information].Town, dbo.[Site Information].Postcode, dbo.[Site Information].siteref,violators.reducedamount as reducedamount,violators.ticketprice as ticketprice,violators.pcn+violators.site as pcn FROM violators  LEFT OUTER JOIN dbo.[Site Information] ON dbo.violators.Site = dbo.[Site Information].SiteCode LEFT OUTER JOIN dbo.DVLA ON dbo.violators.dvlaid = dbo.DVLA.id where violators.id in(select violators.id from reminders left join violators on violators.pcn=reminders.pcn where reminders.batchid=" & request("batchid") & ")  order by pcnsent,site,[date of violation],registration"
'response.write sql
call outputexcel(sql)
			
			end if
			end sub
			sub reprintticketsexcel
			if request("batchid")="" then
			sql="select * from pcnbatch order by batchid desc"
			response.write "<form method=post action=admin.asp?cmd=violatortoexcel target=_new>Choose Batch"
			response.write "<select name=batchid>"
			openrs rs,sql
			do while not rs.eof
				 response.write "<option value=" & rs("batchid") & ">" & rs("date") & "</option>"
				 			
			rs.movenext
			loop
			response.write "</select><input type=submit name=submit class=button value=Reprint></form>"
			closers rs
			else
			
			
			
			
			end if
			end sub
			sub reissues

if request("cmd2")="" then
%>
<table class=maintable2>

<form method=post action=admin.asp?cmd=reissues&cmd2=new>
<tr><td colspan=2>Enter in a pcn or plate </td></tr>
<tr><td>pcn:</td><td> <input type=text name=pcn></td></tr>
<tr><td>plate:</tD><tD> <input type=text name=plate></td></tr>
<tr><td colspan=2 align=center><input type=submit  value="Submit" class=button></td></tr>
</form>
</table>

<%
end if

if request("cmd2")="new" then
	spcn=request("pcn")
splate=request("plate")

if request("s")=1 then
		sql=session("sql")
		i=1
		rcount=2
	else
		sqlw="SELECT violators.ID, violators.Registration, violators.[Date of Violation], violators.Site,  violators.PCN+violators.site as pcn FROM violators where  "
		sqlc="select count(ID) as countc from violators where "
		i=0
		if len(spcn)=10 then
		'check that valid site
		sqlcs="select sitecode from [site information] where sitecode='" & right(spcn,3) & "'"
		
		openrs rscs,sqlcs
		if  rscs.eof and  rscs.bof then
			 response.write "no site information available"
			 exit sub
		closers rscs
		end if
		end if
		if spcn<>"" then
			sql= sql & "(pcn+site like '%" &spcn & "%'"
			'if len(spcn)>7 then sql=sql & " and site like '%" & right(spcn,3) & "%'"
			sql=sql &  ")"
			i=i+1
		end if 
		if splate<>"" then
			if i>0 then sql=sql & " OR "
			sql=sql & "registration like '%" & splate & "%'"
			i=i+1
		end if 
				if i>0 then
			if session("adminsite")=true then
				sqlsites="select * from [site information]"
			else	
			sqlsites="select * from usersites where userid=" & session("userid")
			end if
openrs rssites,sqlsites  ' check the sites he can see 
if rssites.eof and rssites.bof then
	 response.write "YOu do not have access to any site information"
	 response.End
end if

i=1
sql=sql & " and ("
do while not rssites.eof
	if i>1 then sql=sql & " OR "
 	sql=sql & "(site='" &  rssites("sitecode") & "')" 
	i=i+1
	rssites.movenext
loop 
closers rssites
	
		sql=sql & ")"		
				sql=sql & " and violators.pcn+violators.site in(select pcn from incomingletters where statusid=5)"
		sqlc=sqlc& sql
		sql=sqlw & sql
	'	response.write sql
		response.write "<!--" &  sql & "-->"
		openrs rsc,sqlc
		rcount=rsc("countc")
		closers rsc 
		end if
	end if
	if i=0 then 
		response.write "You must fill in a search field"
		call pcn
	else
		
		strpagename="admin.asp?cmd=reissues&cmd2=new"
			
		if rcount=0 then 
			response.write "no records with autorized reissue"
		'elseif rcount=1 then call viewrecord
		'else
			'if rcount=1 then
			'openrs rsa,sql
			'	call viewrecord(rsa("id"))
			'closers rsa
		else
			createSortableListr objConn,sql,"ID",25,"class=maintable2w",strpagename
		end if
		end if
		
	end if
'end if
if request("cmd2")="addreissue" then 
	 		call addreissues(request("id"))
end if

end sub

Sub createSortableListr(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
	'	if strpagename="admin.asp?cmd=paymentssort" then
    'if strsort="PCNdesc" then strsort="PCNdesc"
	' if strsort="iddesc" then strsort="iddesc"
	'  if strsort="PCN" then strsort="payments.pcn"
	' if strsort="ID" then strsort="violators.id"
  ' response.write "aa" & strsort & "aa"
	'if strsort="Date" then response.write "zadafsdfasdkfads"	
	  if strsort="Date" then strsort="[Date of Violation]"
	    if strsort="violators.[Date" then strsort="[Date of Violation]"
		  if strsort="violators.[Datedesc" then strsort="[Date of Violation]desc"
	    if strsort="Datedesc" then strsort="[Date of Violation]desc"
	'	 if strsort="Received" then strsort="payments.[Received]"
	 '   if strsort="Received" then strsort="payments.[Received]desc"	
    
	'end if
	'response.write "strsort:" & strsort	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
  '  response.write "xx" & strsort & "xx"			
    		Set RS = server.CreateObject("adodb.recordset")
   ' response.write strsql & " order by " & replace(strSort,"desc"," desc")
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"ID" and field.name<>"id" then
				Response.Write "<TD align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</TD>"	& vbcrlf
    			end if
	
	Next
					
    		Response.Write "<td>&nbsp;</td></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			Response.Write "<TR>" & vbcrlf
    			For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"ID" and field.name<>"id" then
					Response.Write "<TD align=center>" & vbcrlf
    				 Response.Write field.value
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
				response.write "<td><a href=admin.asp?cmd=reissues&cmd2=addreissue&id=" & rs("id") & " class=button>Add Reissue</a></td>"
												
			'	end if
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "<TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing			
    	End Sub
			
			sub addreissues(id)
if request("cmd3")="save" then


t="text"
userid=session("userid")
ip=Request.ServerVariables("remote_addr")
datereceived=cisodate(request("datereceived" & i))
refnumber=tosql(request("refnumber"),t)
temporary=request("temporary")
if temporary="" then temporary=0
'sql="insert into dvla(reg,owner,address1,address2,address3,town,postcode,[dvla status],datereceived,make,model,colour) values("
'sql=sql & tosql(request("reg"),t) & "," & tosql(request("owner"),t)& "," & tosql(request("address1"),t)& "," & tosql(request("address2"),t)& "," & tosql(request("address3"),t)& "," & tosql(request("town"),t)& "," & tosql(request("postcode"),t)& "," & tosql(request("dvlastatus"),t) & ",'" & datereceived & "'," & tosql(request("make"),t)& "," & tosql(request("model"),t)& "," & tosql(request("colour"),t)& ")"

sql="exec spinsertdvlareissue @reg=" & tosql(request("reg"),t) & ",@owner=" & tosql(request("owner"),t)& ",@address1=" & tosql(request("address1"),t)& ",@address2=" & tosql(request("address2"),t)& ",@address3=" & tosql(request("address3"),t)& ",@town=" & tosql(request("town"),t)& ",@postcode=" & tosql(request("postcode"),t)& ",@dvlastatus=" & tosql(request("dvlastatus"),t) & ",@datereceived='" & datereceived  & "',@userid=" & userid & ",@ip=" & tosql(ip,t) & ",@refnumber=" & refnumber & ",@violatorid=" & id & ",@temporary=" & temporary
'response.write  sql 
objconn.execute sql
sqlv="select pcn+site as pcn from violators where id=" & id
openrs rsv,sqlv
spcn=rsv("pcn")
closers rsv
sqlu="update incomingletters set updatetoclients=1,statusid=4,datestatuschanged=getdate() where statusid=5 and pcn='" & spcn & "'"
'response.write sqlu,
objconn.execute sqlu

response.write "record saved<br>"

'closers rs
else
if id="" then serror="A reissue can only be done on a pcn with status of reissue to be considered"
''check that record is reissue to be considered
sqlck="select * from incomingletters left join violators on incomingletters.pcn=violators.pcn+violators.site where statusid=5 and violators.id=" & id
openrs rsck,sqlck
if rsck.eof and rsck.bof then serror="A reissue can only be done on a pcn with status of reissue to be considered"
closers rsck
if serror<>"" then
response.write serror
exit sub
end if
sqlv="select registration from violators where id=" & id
openrs rsv,sqlv
reg=rsv("registration")
closers rsv
%>
<form method=post action=admin.asp?cmd=reissues&cmd2=addreissue&cmd3=save>
<input type=hidden name=id value="<%=id%>">
<table class=maintable2>
<tr>
<td colspan=2><b>Add Reissue</td>
</tr>
<tr><th>Reference Number</th><td><input type=text name=refnumber></td></tr>
<tr><th>Temporary DVLA</td><td><input type=checkbox name=temporary value=1></td></tr>
<input type=hidden name=reg value=<%=reg%>>
<tr><th>Owner</th><td><input type=text name=owner></td></tr>
<tr><th>Address </th><td><input type=text name=address1><br><input type=text name=address2><br><input type=text name=address3></td></tr>
<tr><th>Town</th><td><input type=text name=town></td></tr>
<tr><th>Post Code</th><td><input type=text name=postcode></td></tr>
<tr><th>Dvla Status</th><td><input type=text name=dvlastatus></td></tr>
<tr><th>Date Received</th><td><input type=text name=datereceived value="<%=date()%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)</td></tr>

<tr><td colpsan=2 align=center><input type=submit name=submit value=Save class=button></td></tr>
</table>
</form>



<%
end if
end sub
sub incomingletters


Dim objFSO
 Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
Dim objFolder 
Dim strRootFolder
 strRootFolder = "" & configsecuredir & "db\incomingletters\" 
 'response.Write strrootfolder
 Set objFolder = objFSO.GetFolder(strRootFolder)
Dim objFile 

For Each objFile in objFolder.Files
		filename=objfile.name
		'response.write filename
		'first check pcn
		pcnname=left(filename,len(filename)-4)
		sql="select * from violators where pcn+site='" & pcnname & "'"
		openrs rs,sql
		if rs.eof and rs.bof then
			  response.write "<br>" & pcnname & " is not a valid pcn"
		else
				movefile(filename)
		end if
		
		closers rs
Next
set objfso=nothing


end sub
	sub movefile(filename)
	pcnname=left(filename,len(filename)-4)
dim fs,f
set fs=Server.CreateObject("Scripting.FileSystemObject")
Set f=fs.GetFile("" & configsecuredir & "db\incomingletters\" & filename)
randomize
RandNumb = (9 * Rnd) + 1
newfilename=randnumb & filename
mydate=date()
mynewpath=configwebdir & "traffica\incomingletters\" & year(mydate) & "\" & month(mydate) & "\"
checkdirectory  mynewpath
f.Move(mynewpath & newfilename)
'response.write filename & " moved" 
set f=nothing
set fs=nothing
ip=Request.ServerVariables("REMOTE_ADDR")
sql="insert into incomingletters(pcn,filename,userid,ip,datefilecreated) values (" & tosql(pcnname,"text") & "," & tosql(newfilename,"text") & "," & session("userid") & "," & tosql(ip,"text") & ",getdate())"
objconn.execute sql
end sub
sub searchincomingletters
if request("fromdate")="" and request("todate")="" then
%>
<table class=maintable2>
<tr>
<td>
<form method=post action=admin.asp?cmd=searchincomingletters><input type=hidden name=cmd2 value="print">
<b>Date</b></td>
</tr>
<tr><td>
From: </td><td><input type=text name=fromdate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('fromdate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To:</td><td> <input type=text name=todate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('todate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>

	<tr><td colspan=2 align=center>
<input type=submit value=submit name=sumbit>
</td>
</tr>
</table>
</form>

<%
else
fromdate=request("fromdate")
todate=request("todate")
if fromdate="" or todate="" then
	 response.write "Please go back and enter in a fromdate and todate"
	 response.end
end if
sql="select incomingletters.id, incomingletters.pcn,VIOLATORS.Registration, incomingletters.filename, incomingletters.datefilecreated as Date,letterstatus.status  from VIOLATORS RIGHT OUTER JOIN incomingletters ON VIOLATORS.PCN+violators.site = incomingletters.pcn left join letterstatus on letterstatus.statusid=incomingletters.statusid  where datefilecreated>'" & cisodate(fromdate) & "' and datefilecreated<='" & cisodate(todate) & " 23:59'"
'response.write sql
openrs rs,sql
'response.write "<table class=maintable2><tr><td>PCN</td><td>Vehicle</td><td>Date</td><td>Status</td></tr>"
if rs.eof and rs.bof then 

response.write "no records"
else
'do while not rs.eof
'response.write "<tr><td>" & rs("pcn") & "</td><td>" & rs("registration") & "</td><td>" & rs("datefilecreated") & "</td><td>" & rs("status") & "</td><td><a href=incomingletters/" & rs("filename") & " target=_new class=button>View Letter</a></td></tr>"

'rs.movenext
'loop
'response.write "</table>"
strpagename="admin.asp?cmd=searchincomingletters&fromdate=" & request("fromdate") & "&todate=" & request("todate")
createSortableListincoming objConn,sql,"ID",25,"class=maintable2",strpagename

end if
closers rs
end if



end sub
sub viewdelviolators
mydate=request("mydate")
if mydate="" then
%>
    <table class=maintable2>
<tr>
<td>
<form method=post action=admin.asp?cmd=viewdelviolators>
<b>Date</b></td>
</tr>
<tr><td>
<td><input type=text name=mydate id=mydate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('fromdate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td align=center><input type=submit name=submit value=Submit class="button"/></table></form>
<%
else
    sql="select registration,convert(varchar,[date of violation],103) as dateofviolation,site,convert(varchar,[Arrival Time],108) as arrivaltime,convert(varchar,[departure Time],108) as departuretime,convert(varchar,[Duration of Stay],108) as duration,dateadded,reasondeleted from delviolators where dateadded>'" & cisodate(mydate) & "' and dateadded<='" & cisodate(mydate) & " 23:59'"
    openrs rs,sql
    if not rs.eof and not rs.bof then

    strpagename="admin.asp?cmd=viewdelviolators&mydate=" & request("mydate")
    createSortableListmain objConn,sql,"registration",50,"class=maintable2",strpagename
    call outputexcel(sql)
    else
    response.write "no records"
    end if

    closers rs
end if
end sub
function checkifletteroutstanding(pcn)
checkifletteroutstanding=0
sql="select *  from incomingletters  where pcn='" & pcn & "' and (statusid=5 or statusid=6)"
openrs rscl,sql
if not rscl.eof and not rscl.bof then checkifletteroutstanding=1
closers rscl

end function
sub outstandingincomingletters
response.write "Filter by Status:"
sqlstatus="select statusid,status from letterstatus where statusid=5 or statusid=6 or statusid=18"
'response.write sqlstatus
openrs rsstatus,sqlstatus
response.write "<form method=post action=admin.asp?cmd=outstandingincomingletters><select name=status>"
do while not rsstatus.eof
response.write "<option value='" & rsstatus("statusid") & "'>" & rsstatus("status") & "</option>"
rsstatus.movenext
loop
response.write "</select><input type=submit name=submit value=Filter class=button></form>"
closers rsstatus
if request("status")="" then
mywhere="(incomingletters.statusid=5 or incomingletters.statusid=6 or incomingletters.statusid=18)" 
else
mywhere="incomingletters.statusid =" & request("status") 
end if
sql="select incomingletters.id, incomingletters .pcn,VIOLATORS.Registration, incomingletters.filename, incomingletters.datefilecreated as Date,letterstatus.status  from VIOLATORS RIGHT OUTER JOIN incomingletters ON VIOLATORS.PCN+violators.site = incomingletters.pcn left join letterstatus on letterstatus.statusid=incomingletters.statusid  where " 
sql=sql & mywhere
'esponse.write sql
openrs rs,sql
'response.write "<table class=maintable2><tr><td>PCN</td><td>Vehicle</td><td>Date</td><td>Status</td></tr>"
if rs.eof and rs.bof then 

response.write "no records"
else


'do while not rs.eof
'response.write "<tr><td>" & rs("pcn") & "</td><td>" & rs("registration") & "</td><td>" & rs("datefilecreated") & "</td><td>" & rs("status") & "</td><td><a href=incomingletters/" & year(rs("date") & "/" & month(rs("date")) & "/"  & rs("filename") & " target=_new class=button>View Letter</a></td></tr>"

'rs.movenext
'loop
'response.write "</table>"
strpagename="admin.asp?cmd=outstandingincomingletters&status=" & request("status")
createSortableListincoming objConn,sql,"ID",25,"class=maintable2",strpagename

end if

end sub

sub outstandingappeals
if request("cmd2")="cancelaction" then

'response.Write "here"
sql="update appeals set action=0 where id=" & request("id")
objconn.execute sql

end if 
sql="select id,mydatetime,comments from appeals where action=1"
openrs rs,sql
if rs.eof and rs.bof then 
    response.Write "no records"
else    
    response.Write "<table class=sortable id=mytable><tr><th>Date</th><th>Comments</th></tr>"
do while not rs.eof 
    response.Write "<tr><td>" & rs("mydatetime") & "</td><td>" & rs("comments") & "</td><td><a href=admin.asp?cmd=outstandingappeals&cmd2=cancelaction&id=" & rs("id") & ">Mark as Done</a></td></tr>"
    
rs.movenext
loop
end if
closers rs
end sub


function checkifappeal(letterid)
checkifappeal="No"
sqla="select * from appealresponses where letterid=" & letterid
'response.write sqla
openrs rsa,sqla
if not rsa.eof and not rsa.bof then checkifappeal="Yes"
closers rsa

end function
sub marknoresponse
violatorid=request("violatorid")
letterid=request("letterid")
sql="update incomingletters set updatetoclients=1, statusid=1,datestatuschanged=getdate() where id=" & letterid
'response.write sql
objconn.execute sql
sql="exec insertnoaction @letterid=" & letterid
openrs rs,sql
refnumber=rs("noactionid")
closers rs
'if violatorid <>"" then 
'call viewrecord(violatorid)
'else
response.write "marked as no resonse successfully<br>Reference Number is: n" & refnumber
call pcnnotes
'end if

end sub
sub markdatarequired
violatorid=request("violatorid")
letterid=request("letterid")
sql="update incomingletters set updatetoclients=1, statusid=18,datestatuschanged=getdate() where id=" & letterid
'response.write sql
objconn.execute sql
sql="exec insertnoaction @letterid=" & letterid
openrs rs,sql
refnumber=rs("noactionid")
closers rs
'if violatorid <>"" then 
'call viewrecord(violatorid)
'else
response.write "marked as Data required successfully<br>Reference Number is: n" & refnumber
call pcnnotes
'end if

end sub
Sub createSortableListincoming(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
response.write  "<!--" & strsql & "-->"
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
  '  response.write "xx" & strsort & "xx"			
    		Set RS = server.CreateObject("adodb.recordset")
   ' response.write strsql & " order by " & replace(strSort,"desc"," desc")
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"ID" and field.name<>"id" and field.name<>"filename" then
				Response.Write "<TD align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</TD>"	& vbcrlf
    			end if
	
	Next
					
    		Response.Write "<td align=center>Days</td><td>&nbsp;</td></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			Response.Write "<TR>" & vbcrlf
    			For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"ID" and field.name<>"id" and field.name<>"filename" then
					Response.Write "<TD align=center>" & vbcrlf
    				 if field.name<>"pcn" then
						 Response.Write field.value
						 elseif field.name="Date" then
						 				response.write formatdatetime(field.value,2)
						 
						 else
						 Response.Write "<a href=admin.asp?cmd=search&p=1&pcn=" & field.value & " target=_new>" &  field.value & "</a>"
						 end if
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
					days=datediff("d",rs("Date"),date)
					response.write "<td align=center>" & days & "</td>"
				response.write "<td><a href=incomingletters/" & year(rs("date")) & "/" & month(rs("date")) & "/"  & rs("filename") & " target=_new class=button>View Letter</a>"
				if rs("status")<>"replied"  then
				response.write "&nbsp;<a href=admin.asp?cmd=changeletterstatus&letterid=" & rs("id") & "&p=1&pcn=" & rs("pcn") & " class=button target=_new>Change Status</a>"
			end if
				
				response.write "</td>"
			
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "</TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing
		response.write "<div align=center><a href=incomingpopup.asp?sort="& strsort & "  onclick=""return popitup('incomingpopup.asp?sort="& strsort &"')"" class=button>Print</a></div>"			
    	End Sub
			
			
			Sub createSortableListmain(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
response.write  "<!--" & strsql & "-->"
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
  '  response.write "xx" & strsort & "xx"			
    		Set RS = server.CreateObject("adodb.recordset")
   ' response.write strsql & " order by " & replace(strSort,"desc"," desc")
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"ID" and field.name<>"id" and field.name<>"filename" then
				Response.Write "<TD align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</TD>"	& vbcrlf
    			end if
	
	Next
					
    		Response.Write "</TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			Response.Write "<TR>" & vbcrlf
    			For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"ID" and field.name<>"id" and field.name<>"filename" then
					Response.Write "<TD align=center>" & vbcrlf
    				 
						 Response.Write field.value
						 
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
				
				
				response.write "</td>"
			
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "</TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing
'		response.write "<div align=center><a href=incomingpopup.asp?sort="& strsort & "  onclick=""return popitup('incomingpopup.asp?sort="& strsort &"')"" class=button>Print</a></div>"			
    	End Sub
			
			function getmystatus(letterid)
			sql="select statusid from incomingletters where id=" & letterid
			openrs rs,sql
			getmystatus=rs("statusid")
			
			closers rs
			end function
			sub changeletterstatus
			if request("status")="" then
			
			
	
letterid=request("letterid")
spcn=request("pcn")
violatorid=getviolatorid(spcn)
plate=getplate(spcn)
currentstatus=getmystatus(letterid)
%>

			<table class=maintable2>
		
<tr>

<td colspan=2><b>Change Letter Status</b></td></tr>

<tr><td>PCN</td><td><%=spcn%></td></tr>
<tr><td>Plate</td><td><%=plate%></td></tr>	
<form method=post action=admin.asp?cmd=changeletterstatus onsubmit="return confirm('Are you sure')">
<input type=hidden name=pcn value="<%=spcn%>">
<tr><td><input type=radio name=status value="admin.asp?cmd=appealletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>">Reply</td></tr>

<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=marknoresponse&letterid=<%=letterid%>">No Response</td></tr>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=markdatarequired&letterid=<%=letterid%>">Data Required</td></tr>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=showcancelform&id=<%=violatorid%>&letter=<%=letterid%>">Cancelled</td></tr>
<!--<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=cancelledletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>">Cancelled</td></tr>-->
<%'if currentstatus<>5 then %>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=markreissuetobeconsidered&letterid=<%=letterid%>">Mark Reissue to be considered</td></tr>
<%'else
' <tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=markreissue&letterid=<l etterid >">Mark Reissue</td></tr>
' end if %>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>&lettertype=1">Invalid Completed Cheque </td></tr>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>&lettertype=2">Missing Payment</td></tr>
<%
'only send if pcn has a payment
sqlckp="select * from payments where pcn='" & spcn & "'"
openrs rsckp,sqlckp
if not rsckp.eof and not rsckp.eof then

%>

<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>&lettertype=3">Receipt </td></tr>
<%
closers rsckp
end if

%>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>&lettertype=4">Sending Photographic Evidence </td></tr>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>&lettertype=5">Bounced Cheque </td></tr>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>&lettertype=7">Refund </td></tr>

<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>&lettertype=8">Exhaustion Letter </td></tr>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>&lettertype=9">Cancelled Credit Card Letter</td></tr>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>&lettertype=10">Special Letter </td></tr>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>&lettertype=12">Special Letter 2 </td></tr>
<tr><td colspan=2><input type=radio name=status value="admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=<%=letterid%>&lettertype=14">Registered Keeper Letter  </td></tr>
<tr><td colspan=2 align=center><input type=submit name=submit value="Submit" class=button>
<form>
</table>
			
			<%
			else
			response.redirect request("status") & "&pcn=" & request("pcn")
			
			end if
			end sub
			sub createletter
			violatorid=request("id")
			
			%>
<table class=maintable2>
<tr><td>
			Click on the letter below to create </tD></tr>
			<tr><td><a href=admin.asp?cmd=appealletters&cmd2=addletter&id=<%=violatorid%>&letter=0>Appeal Letter</a></td></tr>
<!--<tr><td colspan=2><a href=admin.asp?cmd=cancelledletters&cmd2=addletter&id=<%=violatorid%>&letter=0>Cancelled</a></td></tr>-->
<tr><td colspan=2><a href=admin.asp?cmd=showcancelform&id=<%=violatorid%>&letter=0>Cancelled</td></tr>
<% 
sqllt="select * from lettertypes"
openrs rslt,sqllt
do while not rslt.eof
if rslt("id")<>3 then 
    %>
    <tr><td colspan=2><a href=admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=0&lettertype=<%=rslt("id") %>><%=rslt("type") %> </a></td></tr>

    <%
else
    'check if paid
  
    if checkbyidifpaid(violatorid) then
        %>
        <tr><td colspan=2><a href=admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=0&lettertype=<%=rslt("id") %>><%=rslt("type") %> </a></td></tr>

        <%
    end if

end if    
rslt.movenext
loop
closers rslt
%>
<!--
<tr><td colspan=2><a href=admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=0&lettertype=2>Missing Payment</a></td></tr>
<tr><td colspan=2><a href=admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=0&lettertype=3>Receipt </a></td></tr>
<tr><td colspan=2><a href=admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=0&lettertype=4>Sending Photographic Evidence </a></td></tr>
<tr><td colspan=2><a href=admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=0&lettertype=5>Bounced Check </a></td></tr>
<tr><td colspan=2><a href=admin.asp?cmd=generalletters&cmd2=addletter&id=<%=violatorid%>&letter=0&lettertype=7>Refund </a></td></tr>
-->
<%			
			end sub
			sub markreissue
			sql="update incomingletters set updatetoclients=1,statusid=4 where id=" & request("letterid")
			objconn.execute sql
			response.write "Status Changed Successfully to Reissue"		
			
			end sub
			sub markreissuetobeconsidered
			sql="update incomingletters set updatetoclients=1,statusid=5 where id=" & request("letterid")
			objconn.execute sql
			response.write "Status Changed Successfully to Reissue to be considered"		
			
			end sub
			sub markcancelled
			sql="update incomingletters set updatetoclients=1,statusid=3 where id=" & request("letterid")
			objconn.execute sql
			response.redirect  "admin.asp?cmd=cancelform&cmd2=cform&pcn=" & request("pcn")		
			
			end sub
	function getviolatorid(spcn)
	sqlv="select * from violators where pcn+site='" & spcn & "'"
	openrs rsv,sqlv
	getviolatorid=rsv("id")
	closers rsv 
	
	end function		
		function getplate(spcn)
	sqlv="select * from violators where pcn+site='" & spcn & "'"
	openrs rsv,sqlv
	getplate=rsv("registration")
	closers rsv 
	
	end function	
	function getletterstatus(statusid)
	
	sql="select * from letterstatus where statusid=" & statusid
	openrs rs,sql
if not rs.eof and not rs.bof then	
	 getletterstatus=rs("status")
	else
		getletterstatus=""
	end if
	closers rs
	
	end function
	
	
	
	
	sub generalletters

if request("cmd2")="" then
%>
<table class=maintable2>

<form method=post action=admin.asp?cmd=genealletters&cmd2=new>
<tr><td colspan=2>Enter in a pcn or plate </td></tr>
<tr><td>pcn:</td><td> <input type=text name=pcn></td></tr>
<tr><td>plate:</tD><tD> <input type=text name=plate></td></tr>
<tr><td colspan=2 align=center><input type=submit  value="Submit"></td></tr>
</form>
</table>

<%
end if

if request("cmd2")="new" then
	spcn=request("pcn")
splate=request("plate")

if request("s")=1 then
		sql=session("sql")
		i=1
		rcount=2
	else
		sqlw="SELECT violators.ID, violators.Registration, violators.[Date of Violation], violators.Site,  violators.PCN+violators.site as pcn FROM violators where  "
		sqlc="select count(ID) as countc from violators where "
		i=0
		if len(spcn)=10 then
		'check that valid site
		sqlcs="select sitecode from [site information] where sitecode='" & right(spcn,3) & "'"
		
		openrs rscs,sqlcs
		if  rscs.eof and  rscs.bof then
			 response.write "no site information available"
			 exit sub
		closers rscs
		end if
		end if
		if spcn<>"" then
			sql= sql & "(pcn+site like '%" &spcn & "%'"
			'if len(spcn)>7 then sql=sql & " and site like '%" & right(spcn,3) & "%'"
			sql=sql &  ")"
			i=i+1
		end if 
		if splate<>"" then
			if i>0 then sql=sql & " OR "
			sql=sql & "registration like '%" & splate & "%'"
			i=i+1
		end if 
				if i>0 then
			if session("adminsite")=true then
				sqlsites="select * from [site information]"
			else	
			sqlsites="select * from usersites where userid=" & session("userid")
			end if
openrs rssites,sqlsites  ' check the sites he can see 
if rssites.eof and rssites.bof then
	 response.write "YOu do not have access to any site information"
	 response.End
end if

i=1
sql=sql & " and ("
do while not rssites.eof
	if i>1 then sql=sql & " OR "
 	sql=sql & "(site='" &  rssites("sitecode") & "')" 
	i=i+1
	rssites.movenext
loop 
closers rssites
	
		sql=sql & ")"		
				
		sqlc=sqlc& sql
		sql=sqlw & sql
		response.write "<!--" &  sql & "-->"
		openrs rsc,sqlc
		rcount=rsc("countc")
		closers rsc 
		end if
	end if
	if i=0 then 
		response.write "You must fill in a search field"
		call pcn
	else
		
		strpagename="admin.asp?cmd=generalletters&cmd2=new"
			
		if rcount=0 then 
			response.write "no records exist with this criteria"
		'elseif rcount=1 then call viewrecord
		'else
			'if rcount=1 then
			'openrs rsa,sql
			'	call viewrecord(rsa("id"))
			'closers rsa
		else
			createSortableLista objConn,sql,"ID",25,"class=maintable2w",strpagename
		end if
		end if
		
	end if
'end if
if request("cmd2")="addletter" then 
	 		call addgeneralletter(request("id"))
end if
if request("cmd2")="edit" then
 if request("ref")<>"" then response.write "Reference Number:" & request("ref")
appealid=request("appealid")

sql="select * from generalletters where id=" & appealid

'response.write sql
openrs rs,sql

			
response.write getpcndetails(rs("violatorid"))
response.write "<form method=post action=admin.asp?cmd=generalletters&cmd2=save&p=1>"
response.write "<input type=hidden name=appealid value=" & appealid & ">"
response.write "<textarea name=appealletter id=appealletter>" 

response.write replace(rs("letter"),vbcrlf,"<br>") & "</textarea>"
%>
    
                
	
         
<%
response.write "<input type=hidden name=ref value=" & request("ref")& "><br><input type=submit name=submit class=button value=Print Letter></form>"
closers rs

end if
if request("cmd2")="save" then
sql="update generalletters set letter=" & tosql(request("appealletter"),"text") & " where id=" & request("appealid")
objconn.execute sql
'response.write "Appeal Saved"
response.redirect "printletter.asp?id=" & request("appealid") & "&ref=" & request("ref")

end if
end sub
sub addgeneralletter(id)
'response.write "add letter for id" & id
lettertype=request("lettertype")
if request("cmd3")<>"save" then
%>
<form method=post name=appeal action=admin.asp?cmd=generalletters&cmd2=addletter&cmd3=save>
<input type=hidden name=id value="<%=id%>">
<input type=hidden name=letterid value=<%=request("letter") %>>
<table class=maintable2 width=550>
<tr><td colspan=2>
<%= getpcndetails(id) %>
</td></tr>
<tr>
<td><b>Letter Received Date:</b> <input type=text name=letterrecieveddate><a href="javascript:NewCal('letterrecieveddate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)</td>

</tr><tr>
<td><b>Address:</b><br><textarea name=address cols=40 rows=10><%=getaddress(id)%></textarea></td>
</tr>

<input type=hidden name=lettertype value="<%=lettertype%>">
<%if lettertype=14 then %>
<tr><td><table>
<tr><td>Drivers Name</td><td><input type=text name=driversname value="" /></td></tr>
<tr><td>Drivers Address</td><td><input type=text name=driversaddress value="" /></td></tr>
<tr><td>Drivers Address 2</td><td><input type=text name=driversaddress2 value="" /></td></tr>
<tr><td>Drivers Town</td><td><input type=text name=driverstown /></td></tr>
<tr><td>Drivers Post-Code</td><td><input type=text name=driverspostcode /></td></tr>
</table></td></tr>
<%end if%> 
<tr><td align=center><input type=submit name=submit value="Create Letter" class=button></td></tr>
</table></form>
<script language="JavaScript" type="text/javascript">
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("appeal");
    frmvalidator.addValidation("letterrecieveddate","req","Please enter letter received date");
  <%if lettertype=14 then %>
  frmvalidator.addValidation("driversname","req","Please enter letter Drivers Name");
    frmvalidator.addValidation("driversaddress","req","Please enter letter Drivers Address");
      frmvalidator.addValidation("driverstown","req","Please enter letter Drivers Town");
        frmvalidator.addValidation("driverspostcode","req","Please enter letter Drivers Post code");
  <%end if %>
</script>

<%
else
'response.write request("letterrecieveddate")
'response.write "<hr>today" & date()
lrd=request("letterrecieveddate")
lrdd=dateserial(year(lrd),month(lrd),day(lrd))
lettertype=request("lettertype")
if lrdd>date() then
response.write "Letter Recieved date must be in the past -Please go back and fill in"
response.end
end if
site=getsite(id)
if lettertype<>"" then
sqllt="select * from lettertypes where id=" & lettertype
'response.write sqlac
openrs rslt,sqllt
			 if not rslt.bof and not rslt.eof then
			 		stringletter=rslt("letter")
					showpaidpara=rslt("showpaidpara")
					letterref=rslt("letterref")
					newstatus=rslt("statusid")
					
				end if	
closers rslt
end if
if lettertype=14 then
sqlu="update violators set driversname=" & tosql(request("driversname"),"text") & ",driversaddress=" & tosql(request("driversaddress"),"text")& ",driversaddress2=" & tosql(request("driversaddress2"),"text")  & ",driverstown=" & tosql(request("driverstown"),"text") & ",driverspostcode=" & tosql(request("driverspostcode"),"text") & " where id=" & id

objconn.execute sqlu

end if
myletter="<br><br><br><br>"
			
myletter=myletter & "<p>" & request("address") & "</p><br>"
myletter=myletter &   "<P><img src='http://traffica.cleartoneholdings.co.uk/traffica/images/spacer.gif' width='590' height='10'>" & date()& "</p>"                 
myletter=myletter & "<p>" & getpcndetails(id)  & "<br>"	
if lettertype=3 then
myletter=myletter & "<strong>RECEIPT </strong>   <p>We confirm receipt of your total payment of &pound;" & gettotalpaid(id) & ".</p>  <p>Yours faithfully,</p>  <p><br> <b><u>   Civil Enforcement Ltd</u></b> <br>  </p>"

else
    if lettertype=13 then
        myletter=myletter & filldp(stringletter,id)
    else    
        myletter=myletter & stringletter
    end if    
end if
if showpaidpara=1 then

paid=checkbyidifpaid(id)
'response.write "paid:" & paid

sqlac="select * from siteappealpara where site='" & site & "'"
response.write sql
openrs rsac,sqlac
			 if rsac.bof and rsac.eof then
			 		rsac.close
          sqlac="select * from appealconfig where id=1"
					response.write sql
					response.write sqlac
          openrs rsac,sqlac
				end if	

if lettertype=8 then
    if paid=true then				
	     myletter=myletter & "<p align=justify>We refer to your recent letter.<br><br>We have already expended considerable time responding to your representations. We have already clearly stated our case and cannot continue corresponding with you.<br><br>The decision that has been sent is final, we are therefore unable to refund the payment that has been made in this instance.<br><br>Yours faithfully,<br><br><b><u>Representations Team</u></b></p>"
    else
		    myletter=myletter & "<p align=justify>We refer to your recent letter.<Br><br>We have already expended considerable time responding to your representations. We have already clearly stated our case and cannot continue corresponding with you.<br><br>The decision that has been sent is final, and your continued failure to make payment will result in this matter being forwarded to a collection agency for recovery. <br><br>Yours faithfully,<br><br><b><u>Representations Team</u></b></p>"
    end if
else
    if paid=true then				
	 myletter=myletter & "<p align=justify>" & rsac("finalparapaid") & "</p>"
    else
	myletter=myletter & "<p align=justify>" & rsac("finalpara") & "</p>"
    end if


end if
closers rsac
end if
if lettertype=14 then
'myletter=myletter & stringletter
'myletter=replace(myletter,"**dearname**",getownersname(id))
myletter=replace(myletter,"**dov**",getdov(id))
myletter=myletter & "<p></p><p>CC:<br>" & request("driversname") & "<br>" & request("driversaddress")
if request("driversaddress2")<>"" then myletter=myletter & "<br>" & request("driversaddress2")
myletter=myletter & "<br>" & request("driverstown") & "   " & request("driverspostcode")



myletter=myletter & "<br><br><br>NEW PAGE WILL START HERE IN PRINTED VERSION"
myletter=myletter & "<br><br><br><br><br><br>"
			
myletter=myletter & "<p>" &  request("driversname") & "<br>" & request("driversaddress")
if request("driversaddress2")<>"" then myletter=myletter & "<br>" & request("driversaddress2")
myletter=myletter & "<br>" & request("driverstown") & "   " & request("driverspostcode") & "</p><br>"
myletter=myletter &   "<P><img src='http://traffica.cleartoneholdings.co.uk/traffica/images/spacer.gif' width='590' height='10'>" & date()& "</p>"                 
myletter=myletter & "<p>" & getpcndetails(id)  & "<br>"	
myletter=myletter & "<p>Please find attached a copy of the letter dated  " & date() & " sent to the registered keeper."
myletter=myletter & "</p><p> Yours   faithfully,</p><p></p><P>  <BR><B><U>Civil Enforcement Ltd</U></B></p>"
myletter=myletter & "<br><br><br>NEW PAGE WILL START HERE IN PRINTED VERSION"
myletter=myletter & "<br><br><br><br><br><br>"
			
myletter=myletter & "<p>" & request("address") & "</p><br>"
myletter=myletter &   "<P><img src='http://traffica.cleartoneholdings.co.uk/traffica/images/spacer.gif' width='590' height='10'>" & date()& "</p>"                 
myletter=myletter & "<p>" & getpcndetails(id)  & "<br>"	
myletter=myletter & stringletter
myletter=replace(myletter,"**dov**",getdov(id))
myletter=myletter & "<p></p><p>CC:<br>" & request("driversname") & "<br>" & request("driversaddress")
if request("driversaddress2")<>"" then myletter=myletter & "<br>" & request("driversaddress2")
myletter=myletter & "<br>" & request("driverstown") & "   " & request("driverspostcode")
end if 
myletter=replace(myletter,"<p>&nbsp</p>","<p>&nbsp;</p>")
'response.write myletter
'response.End
sql="set nocount on insert into generalletters(violatorid,letter,userid,ip,letterreceiveddate,lettertypeid) values(" & id & "," & tosql(myletter,"text") & "," & session("userid") & ",'" & ip & "'," & tosql(request("letterrecieveddate"),"date")& "," & lettertype & ");"&_
      "SELECT @@IDENTITY AS NewID;"

'response.write "<hr>" & sql
 set rsi=objconn.execute (sql )
 appealid=rsi(0)
if request("letterid")<>"" then
sql="exec insertletterresponses @appealid=" & appealid & ",@letterid=" & request("letterid") & ",@lettertype=" & lettertype
'response.write sql
openrs rsa,sql

refnumber=letterref & rsa("id")

closers rsa
sql="update generalletters set ref='" & refnumber & "' where id=" & appealid
objconn.execute sql 
''update letter
'if lettertype>7 then lettertype=lettertype=1 
newstatusid=newstatus'lettertype+6

sql="update incomingletters set updatetoclients=1,statusid=" & newstatusid & " where id=" & request("letterid")
response.write sql
objconn.execute sql


end if
'response.write myletter
'response.end

response.redirect("admin.asp?cmd=generalletters&p=1&cmd2=edit&appealid=" & appealid& "&ref=" & refnumber)
end if
end sub
function gettotalpaid(violatorid)
sqls="exec checkbyidtotalpaid @id=" & violatorid
openrs rss,sqls
gettotalpaid=rss("paid")
closers rss

end function
sub violatortoexcel

'response.write "need to output batch:" & request("batchid")  & " to excel"
sql="SELECT     dbo.violators.ID, dbo.violators.Registration, convert(varchar,[date of violation],103) as [date of violation],convert(varchar,[arrival time],108) as [arrival time],convert(varchar,[departure time],108) as [departure time],convert(varchar,[duration of stay],108) as [duration of stay],  dbo.DVLA.Reg, dbo.DVLA.Make, dbo.DVLA.Model, dbo.DVLA.Colour, dbo.DVLA.Owner, dbo.DVLA.Address1 AS dvlaaddress1,dbo.DVLA.Address2 AS dvlaaddress2, dbo.DVLA.Address3 AS dvlaaddress3, dbo.DVLA.Town AS dvlatown, dbo.DVLA.Postcode AS dvlapostcode, dbo.DVLA.[DVLA Status], dbo.DVLA.DateReceived, dbo.DVLA.Transfered, dbo.DVLA.Auto2, dbo.DVLA.Auto1, dbo.DVLA.Auto3,  dbo.[Site Information].SiteCode, dbo.[Site Information].[Name of Site], dbo.[Site Information].Borough, dbo.[Site Information].Address3, dbo.[Site Information].Address1, dbo.[Site Information].Address2, dbo.[Site Information].Town, dbo.[Site Information].Postcode, "
sql=sql & "  dbo.[Site Information].siteref,violators.reducedamount as reducedamount,violators.ticketprice as ticketprice,violators.pcn+violators.site as pcn"
sql=sql & " FROM         dbo.violators LEFT OUTER JOIN  dbo.[Site Information] ON dbo.violators.Site = dbo.[Site Information].SiteCode LEFT OUTER JOIN         dbo.DVLA ON dbo.violators.dvlaid = dbo.DVLA.id "
sql=sql & "where violators.batchid=" & request("batchid")  & " order by site,[date of violation],registration"
'response.write sql
call outputexcel(sql)
end sub



sub declinereport
if request("fromdate")="" and request("todate")="" then
%>
<table class=maintable2>
<tr>
<td>
<form method=post action=admin.asp?cmd=declinereport><input type=hidden name=cmd2 value="print">
<b>Date</b></td>
</tr>
<tr><td>
From: </td><td><input type=text name=fromdate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('fromdate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To:</td><td> <input type=text name=todate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('todate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>Reason</td><td>
<select name=reason>
<option value=''>--Select--</option>
<% sqlr="select reason from declinedpayments group by reason"
openrs rsr,sqlr
if not rsr.eof and  not rsr.bof then
do while not rsr.eof
response.write "<option value='" & rsr("reason") & "'>" & rsr("reason") & "</option>"
rsr.movenext
loop

end if

closers rsr
%>
</select>
</td></tr>
	<tr><td colspan=2 align=center>
<input type=submit value=submit name=sumbit class=button>
</td>
</tr>
</table>
</form>

<%
else
fromdate=request("fromdate")
todate=request("todate")
if fromdate="" or todate=""  then
	 response.write "Please go back and enter in a fromdate and todate"
	 response.end
end if
reason=request("reason")
sql="select mydate,pcn,amount,reason,phone,ip from declinedpayments where mydate>'" & cisodate(fromdate) & "' and mydate<='" & cisodate(todate) & " 23:59'"
if reason<>"" then

sql=sql & " and reason='" & reason & "'"
end if
'response.write sql
openrs rs,sql
'response.write "<table class=maintable2><tr><td>PCN</td><td>Vehicle</td><td>Date</td><td>Status</td></tr>"
if rs.eof and rs.bof then 

response.write "no records"
else
'do while not rs.eof
'response.write "<tr><td>" & rs("pcn") & "</td><td>" & rs("registration") & "</td><td>" & rs("datefilecreated") & "</td><td>" & rs("status") & "</td><td><a href=incomingletters/" & rs("filename") & " target=_new class=button>View Letter</a></td></tr>"

'rs.movenext
'loop
'response.write "</table>"
strpagename="admin.asp?cmd=declinereport&fromdate=" & request("fromdate") & "&todate=" & request("todate") & "&reason=" & reason
createSortableListgeneric objConn,sql,"ID",25,"class=maintable2",strpagename
call outputexcel(sql)
end if
closers rs
end if



end sub
sub dvlaunknown
sql="select count(id) as count from dvlaunknown"
openrs rs,sql
count=rs("count")
Response.write "Total records with dvla unknown are:" & count
closers rs
if count>0 then
sql="select * from dvlaunknown"
strpagename="admin.asp?cmd=dvlaunknown"
createSortableListgeneric objConn,sql,"ID",25,"class=maintable2",strpagename
call outputexcel(sql)
end if
end sub
Sub createSortableListgeneric(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
	
	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
	
    		Set RS = server.CreateObject("adodb.recordset")
   ' response.write strsql & " order by " & replace(strSort,"desc"," desc")
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"ID" and field.name<>"id" then
				Response.Write "<Th align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</Th>"	& vbcrlf
    			end if
	
	Next
					
    	'	Response.Write "<td>Notes</td><td>&nbsp;</td></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			
					if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
					
    			For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"ID" and field.name<>"id" then
					Response.Write "<TD align=center>" & vbcrlf
    				 Response.Write field.value
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
				
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "<TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing			
    	End Sub
			sub searchdvla
			if request("cmd2")="" then
			%>
			<form method=post action=admin.asp?cmd=searchdvla>
			<table class=maintable2>
<tr><th colspan=2>Enter in Plate to search</th></tr>
<tr>

<td>Plate</td><td><input type=text name=plate>
<input type=hidden name=cmd2 value=search></td>
</tr>
<tr><td colspan=2 align=center><input type=submit name=Search value=Search class=button></td></tr>
</table>
			
			</form>
			<%
			else
			sql="select dvla,convert(datetime,offence,104) as offencedate,dategenerated as datesent,dvlarec as datereceived from dvlarep where dvla='" & request("plate") & "'"
			openrs rs,sql
			if rs.eof and rs.bof then
			
			response.write "This plate was not sent to the dvla"
			else
			
	
strpagename="admin.asp?cmd=searchdvla&cmd2=search"
createSortableListdvlarep objConn,sql,"ID",25,"class=maintable2",strpagename
			
			end if
			closers rs
			end if
			end sub
			Sub createSortableListdvlarep(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
	
	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
	
    		Set RS = server.CreateObject("adodb.recordset")
   ' response.write strsql & " order by " & replace(strSort,"desc"," desc")
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"ID" and field.name<>"id" then
				Response.Write "<Th align=center>" & vbcrlf
    fieldname=field.name
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</Th>"	& vbcrlf
    			end if
				
	
	Next
					
    		Response.Write "<td>Status</td><td>&nbsp;</td></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			
					if i mod 2=0 then 
						 Response.Write "<TR class=rowcolor2>" & vbcrlf
					else
							Response.Write "<TR>" & vbcrlf
					end if
					
    			For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"ID" and field.name<>"id" then
					Response.Write "<TD align=center>" & vbcrlf
    				 Response.Write field.value
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
				
				''get status here
				status=0
				sqlcv="select * from violators where [date of violation]='" & cisodate(rs("offencedate")) & "' and registration='" & rs("dvla") & "'"
				'response.write sqlcv
				openrs rscv,sqlcv
				if not rscv.eof and not rscv.bof then
					 response.write "<td>Violator</td><td><a href=admin.asp?cmd=viewrecord&p=1&id=" & rscv("id") & " target=_new class=button>View Violator Record</a></td>"
					 status=1
				end if
				closers rscv
				if status=0 then
				sqlcv="select * from tempviolators where [date of violation]='" & cisodate(rs("offencedate")) & "' and registration='" & rs("dvla") & "'"
				'response.write sqlcv
				openrs rscv,sqlcv
				if not rscv.eof and not rscv.bof then
					 response.write "<td>Temp Violator</td><td><a href=admin.asp?cmd=viewtempviolator&p=1&id=" & rscv("id") & " target=_new class=button>View Temp Violator Record</a></td>"
					 status=1
				end if
				closers rscv
				end if
					if status=0 then
				sqlcv="select * from delviolators where [date of violation]='" & cisodate(rs("offencedate")) & "' and registration='" & rs("dvla") & "'"
				'response.write sqlcv
				openrs rscv,sqlcv
				if not rscv.eof and not rscv.bof then
					 response.write "<td>Deleted " & rscv("reasondeleted") & "</td><td><a href=admin.asp?cmd=viewdelviolator&p=1&id=" & rscv("id") & " target=_new class=button>View Del Violator Record</a></td>"
				end if
				closers rscv
				end if
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "<TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing			
    	End Sub
			
		sub zshowplateimagesnew
	sql = "SELECT plate, date, picture FROM plateimages"
	sql="select top 60  id,date,convert(varchar,[date],103) as rdate,convert(varchar,[date],108) as rtime,lane,accuracy,plate,picture,permit,violation14 from plateimages where imagesexist=1"

	openrs rs,sql
	if not rs.EOF then
		i=0
%>
	<div id="plates" style="width: 100%">
		<div id="platetop" style="width: 100%" align="center">
			Date: <input type="text" name="dateselect" id="dateselect"><br>
			Search: <input type="text" name="searchbox" id="searchbox" onKeyUp="searchPlates()"><br>
			<input type="button" name="Search" value="Search" onclick="searchPlates()" ID="Button1">
		</div>
		<div id="platelist" align="center" style="height: 300px; overflow-y: scroll; float: left; width: 300px;">
		</div>
		<div id="plateimages" align="center" style="height: 300px; width: 300px;">
		</div>
	</div>
	<script type="text/javascript" language="JavaScript">
	<!--
	bigarray = new Array();
	
	function preLoad()
	{
		imgObjects = new Array();
<%
		do while not rs.EOF
			filename=year(rs("date")) & "/" & month(rs("date")) & "/" & day(rs("date")) & "/" & hour(rs("rtime")) & "/" & rs("picture")
			filename2 =filename
			filename = replace(filename,"A.jpg","P.jpg")
			filename3 = replace(filename,"P.jpg","F.jpg")
			itemimageurl= "http://cleartonecommunications.com/plateimages/server1/"
			itemimage="" & configwebdir & "traffica\plateimages\server1\"
			itemimagesrc="<img src='" & itemimageurl & filename & "' " & Replace(ImageResize(itemimage & filename,200, 68),"""","'") & " border='0' align='middle' valign='middle'><br><br>"
			itemimagesrc2="<img src='" & itemimageurl & filename2 & "' " & Replace(ImageResize(itemimage & filename2,200, 68),"""","'") & " border='0' align='middle' valign='middle'><br><br>"
			itemimagesrc3="<img src='" & itemimageurl & filename3 & "' " & Replace(ImageResize(itemimage & filename3,200, 68),"""","'") & " border='0' align='middle' valign='middle'><br><br>"
			
%>
	imgObjects[<%=(3*i)%>] = "<%=itemimageurl & filename%>";
		imgObjects[<%=(3*i+1)%>] = "<%=itemimageurl & filename2%>";
		imgObjects[<%=(3*i+2)%>] = "<%=itemimageurl & filename3%>";
		bigarray[<%=i%>]= new Array("<%=rs("plate")%>","<%=FormatDateTime(rs("date"),2)%>","<%=itemimagesrc%>","<%=itemimagesrc2%>","<%=itemimagesrc3%>");
<%			rs.MoveNext
			i=i+1
		loop
%>		document.getElementById("platelist").innerHTML = "Total Plates Pulled From Database: " + <%=i%>;

		preImages = new Array();
		for(i=0; i < imgObjects.length; i++)
		{
			preImages[i] = new Image();
			preImages[i].src = imgObjects[i];
		}
	}
	function searchPlates()
	{
		innerHTML = "";
		searchvalue = document.getElementById("searchbox").value;
		searchdate = document.getElementById("dateselect").value;
		count = 0;
		for(i=0; i<bigarray.length;i++)
		{
			display= true;
			if(searchdate != '' && bigarray[i][1] != searchdate)
			{
				display = false;
			}
			if(searchvalue != '' && bigarray[i][0].indexOf(searchvalue) < 0)
			{
				display = false;
			}
			if(display)
			{
				count++;
				//innerHTML += "<a onclick='displayPlates("+i+")'>"+bigarray[i][2]+"<br>"+bigarray[i][0] + "</a><br>";
				innerHTML += "<a onclick='displayPlates("+i+")'>"+bigarray[i][2]+"<br>"+bigarray[i][0] + "<br>" + bigarray[i][1] + "</a><br>";
			}
		}
		document.getElementById("platelist").innerHTML = "Total Plates: " + count + "<br><br>" + innerHTML;
	}

	function displayPlates(id)
	{
		if(id < bigarray.length)
		{
			document.getElementById("plateimages").innerHTML = bigarray[id][2] + bigarray[id][3] + bigarray[id][4];
		}
	}
	
	preLoad();
	searchPlates();
	
	//-->
	</script>
<%
	end if
end sub
sub viewtempviolator
id=request("id")
sql="select * from tempviolators where id=" & id
openrs rs,sql
%>
<table class=maintable2>
<tr><th>ID</th><td><%= rs("id")%></td></tr>
<tr><th>Registration</th><td><%= rs("registration")%></td></tr>
<tr><th>Date of Violation</th><td><%= rs("date of violation")%></td></tr>
<tr><th>Site</th><td><%= rs("site")%></td></tr>
<tr><th>Arrival Time</th><td><%= rs("arrival time")%></td></tr>
<tr><th>Departure Time</th><td><%= rs("departure time")%></td></tr>
<tr><th>Duration of Stay</th><td><%= rs("duration of stay")%></td></tr>

</table>


<%
closers rs
end sub
sub viewdelviolator
id=request("id")
sql="select * from delviolators where id=" & id
openrs rs,sql
%>
<table class=maintable2>
<tr><th>ID</th><td><%= rs("id")%></td></tr>
<tr><th>Registration</th><td><%= rs("registration")%></td></tr>
<tr><th>Date of Violation</th><td><%= rs("date of violation")%></td></tr>
<tr><th>Site</th><td><%= rs("site")%></td></tr>
<tr><th>Arrival Time</th><td><%= rs("arrival time")%></td></tr>
<tr><th>Departure Time</th><td><%= rs("departure time")%></td></tr>
<tr><th>Duration of Stay</th><td><%= rs("duration of stay")%></td></tr>
<tr><th>Reason Deleted</th><td><%= rs("reasondeleted")%></td></tr>
</table>


<%
closers rs
end sub

sub showplateimagesnew
 limit = 60
 if(request("limit") <> "" and isNumeric(request("limit"))) then
	limit = request("limit")
 end if
 sql = "SELECT top " & limit & " plate, date, convert(varchar,[date],108) as rtime, picture,  picture2, picture3,  lane, accuracy FROM plateimages"' LIMIT " & limit
 'response.write sql
 'sql="select top 60 id,date,convert(varchar,[date],103) as
'rdate,convert(varchar,[date],108) as
'rtime,lane,accuracy,plate,picture,permit,violation14 from plateimages where imagesexist=1"
  openrs rs,sql
 if not rs.EOF then
  i=0
%>
<div id="Div1" style="width: 900px; position: relative; background: black; height: 560px;"> 
  <div id="plateleft" style="height: 550px; width: 420px; position: absolute; left:5px; top:5px; background: black; color: white;" align="left"> 
    <div align=left style="margin-bottom: 1em; width:230px;" id="calendar-container"></div>
    <input type="hidden" name="dateselect" id="Hidden1">
    Search: 
    <input type="text" name="searchbox" id="Text1" onKeyUp="searchPlates()">
    <input type="button" name="Search" value="Search" onclick="searchPlates()" ID="Button2">
    <br>
    Number to Pull: 
    <input type="text" name="limit" id="limit" value="<%=limit%>">
    <input type="button" name="limitsubmit" id="limitsubmit" value="Load" onClick="location.href=location.href+'&limit='+document.getElementById('limit').value">
    <br>
    <span id="resultspan"></span><br>
    <div id="Div2" align=left style="height: 300px; overflow-y: scroll; width: 420px; position: absolute; bottom: 0; right: 0;"> 
    </div>
  </div>
  <div id="Div3" align="center" style="height: 550px; width: 400px; position: absolute; top:5px; right: 5px; background: black; color: white;"> 
  </div>
</div>
 <script type="text/javascript">
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth()+1;     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      if(m<10) m='0'+m;
      document.getElementById("dateselect").value=d+"/"+m+"/"+y;
      searchPlates();
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged           // our callback function
    }
  );
</script>
 <script type="text/javascript" language="JavaScript">
 <!--
 bigarray = new Array();
 
 function preLoad()
 {
  imgObjects = new Array();
<%
  do while not rs.EOF
   filename = year(rs("date")) & "/" & month(rs("date")) & "/" & day(rs("date")) & "/" & hour(rs("rtime")) & "/" & rs("picture")
   filename2 = year(rs("date")) & "/" & month(rs("date")) & "/" & day(rs("date")) & "/" & hour(rs("rtime")) & "/" & rs("picture2")
   filename3 = year(rs("date")) & "/" & month(rs("date")) & "/" & day(rs("date")) & "/" & hour(rs("rtime")) & "/" & rs("picture3")
   itemimageurl= "http://cleartonecommunications.com/plateimages/server1/"
   itemimage="" & configwebdir & "traffica\plateimages\server1\"
   itemimagesrc="<img src='" & itemimageurl & filename & "' " & Replace(ImageResize(itemimage & filename,200, 68),"""","'") & " border='0' align='middle' valign='middle'><br><br>"
   itemimagesrc2="<img src='" & itemimageurl & filename2 & "' " & Replace(ImageResize(itemimage & filename2,500, 250),"""","'") & " border='0' align='middle' valign='middle'><br><br>"
   itemimagesrc3="<img src='" & itemimageurl & filename3 & "' " & Replace(ImageResize(itemimage & filename3,500, 250),"""","'") & " border='0' align='middle' valign='middle'><br><br>"
   
%>
 imgObjects[<%=(3*i)%>] = "<%=itemimageurl & filename%>";
  imgObjects[<%=(3*i+1)%>] = "<%=itemimageurl & filename2%>";
  imgObjects[<%=(3*i+2)%>] = "<%=itemimageurl & filename3%>";
  bigarray[<%=i%>]= new Array("<%=rs("plate")%>","<%=FormatDateTime(rs("date"),2)%>","<%=itemimagesrc%>","<%=itemimagesrc2%>","<%=itemimagesrc3%>","<%=rs("rtime")%>","<%=rs("accuracy")%>","<%=rs("lane")%>");
<%   rs.MoveNext
   i=i+1
  loop
%>
 
  preImages = new Array();
  for(i=0; i < imgObjects.length; i++)
  {
   preImages[i] = new Image();
   preImages[i].src = imgObjects[i];
  }
 }
 
 function searchPlates()
 {
  innerHTML = "<table class='sortable' width=400 style='color: white;'><TR><TH class='sorttable_alpha'>Plate</TH><TH>Date</TH><TH>Time</TH><TH>Accuracy</TH><TH>Lane ID</TH></TR>";
  searchvalue = document.getElementById("searchbox").value;
  searchdate = document.getElementById("dateselect").value;
  count = 0;
  for(i=0; i<bigarray.length;i++)
  {
   display= true;
   if(searchdate != '' && bigarray[i][1] != searchdate)
   {
    display = false;
   }
   if(searchvalue != '' && bigarray[i][0].toLowerCase().indexOf(searchvalue) < 0)
   {
    display = false;
   }
   if(display)
   {
    count++;
    innerHTML += "<tr onclick='displayPlates("+i+");alternateRowColors();this.style.background=\"#606060\"'><td align='left'>"+bigarray[i][0] + "</td>";
    innerHTML += "<td align='center'>"+bigarray[i][1] + "</td>";
    innerHTML += "<td align='center'>"+bigarray[i][5] + "</td>";
    innerHTML += "<td align='center'>"+bigarray[i][6] + "</td>";
    innerHTML += "<td align='center'>"+bigarray[i][7] + "</td></tr>";
   }
  }
  document.getElementById("resultspan").innerHTML = "Results: " + count;
  document.getElementById("platelist").innerHTML = innerHTML+"</table>";
  alternateRowColors();
 }
 
 function displayPlates(id)
 {
  if(id < bigarray.length)
  {
   document.getElementById("plateimages").innerHTML = bigarray[id][2] + bigarray[id][4] + bigarray[id][3];
  }
 }
 
 preLoad();
 searchPlates();
 
 //-->
 </script>
<%
 end if
end sub
sub printreminders
'insert into reminders table
' create batch 
''redirect to batch reminder reprint
reminders=request("reminders")
arrreminders=split(reminders,",")

sql="select max(batchid) as batchid from remindersbatches"
	openrs rs,sql
	newbatchid=rs("batchid")+1
	closers rs
	sql="insert into remindersbatches(batchid,datetime) values(" & newbatchid & ",getdate())"
	objconn.execute sql
	
for i=lbound(arrreminders) to ubound(arrreminders)
		spcn=trim(arrreminders(i))
		sql="exec insertreminder @pcn='" & left(spcn,7)& "',@site='" & right(spcn,3)  & "',@batchid=" & newbatchid & ",@userid=" & session("userid")
	'response.write sql
	objconn.execute sql
	
next
response.redirect "printreminders.asp?batchid=" & newbatchid

end sub

sub printremindersummary



sqlpn="select registration,site,convert(varchar,[date of violation],103) as [date of violation],convert(varchar,[arrival time],108) as [arrival time],convert(varchar,[departure time],108) as [departure time],pcn,pcnsent from vwviolators where pcn not in (select pcn from payments where pcn is not null union all select pcn from cancelled where pcn is not null union all select pcn+site as pcn from reminders where pcn is not null) and vwviolators.Pcnsent<getDate()-17"
sqlpn="exec printremindersummary"
'response.write sqlpn
set  rspn=objconn.execute(sqlpn)
i=1
response.write "<table class=""sortable"" id=""mytable""><tr><th>Print</th><th>Registration</th><th>Site<th>Date of violation</th><th>Arrival Time</th><td>Departure Time</th><th>PCN </th><th>Pcn Sent</th><th>Incoming Letter Status</th></tr>" & vbcrlf
			response.write "<form method=post action=admin.asp?cmd=printreminders>"
			response.write "Check Reminders to Print:" 
			

do while not rspn.eof
	 			
				
										
										if trim(rspn("strchecked"))="disabled" then 
											 							 strchecked=" disabled"
													else
													strchecked=""
												end if	
    					 response.write "<tr><td>" & i & "<input type=checkbox name=reminders value=" & rspn("pcn")& strchecked & "></td><td>" & rspn("registration") & "</td><td>" & rspn("site") & "</td><td>" & rspn("date of violation") & "</td><td>" & rspn("arrival time") & "</td><td>" & rspn("departure time") & "</td><td>" & rspn("pcn") & "</td><td>" & rspn("pcnsent") & "</td><td>" & rspn("reasondisabled") & "</td></tr>" & vbcrlf
    					 closers rsqv
    					 		 i=i+1
					 				 
									
					

				'rsr.close	 
rspn.movenext
loop


 response.write "</table><table><tr><td><input type=submit name=submit value='Print Reminders for checked Violators' class=button></td>"

'response.write "<td><a class=button href=printreissue.asp>Print Tickets</a></td>"

response.write "</tr></table></form><br>"
 
'response.write k & " records were deleted as they were sent within 14 days or duplicate registrations<br>"



'else
'delete adn redirect to printnewpcn

'end if

end sub
sub printreminderssummary



sqlpn="select registration,site,convert(varchar,[date of violation],103) as [date of violation],convert(varchar,[arrival time],108) as [arrival time],convert(varchar,[departure time],108) as [departure time],pcn,pcnsent from vwviolators where pcn not in (select pcn from payments where pcn is not null union all select pcn from cancelled where pcn is not null union all select pcn+site as pcn from reminders where pcn is not null) and vwviolators.Pcnsent<getDate()-17"
'response.write sqlpn
set  rspn=objconn.execute(sqlpn)
i=1
response.write "<table class=""sortable"" id=""mytable""><tr><th>Print</th><th>Registration</th><th>Site<th>Date of violation</th><th>Arrival Time</th><td>Departure Time</th><th>PCN </th><th>Pcn Sent</th><th>Incoming Letter Status</th></tr>" & vbcrlf
			response.write "<form method=post action=admin.asp?cmd=printreminders>"
			response.write "Check Reminders to Print:" 
			

do while not rspn.eof
	 			
				
				 		 				 'check if should be checked or not
										 strchecked=" checked"
										' strchecked=""
										 incomingletterstatus=""
										 if checkifletteroutstanding(rspn("pcn"))=1 then 
										 		incomingletterstatus="outstanding"
												strchecked=""
										end if
										if DateDiff("m",rspn("pcnsent"),date)> 3 then
											  strchecked=""
												incomingletterstatus="more then 3 months"
										end if	
										 if checkifpermit(rspn("registration"))=1 then 
										 		incomingletterstatus="Permit"
												strchecked=""
										end if
										
										
										if strchecked="" then 
											 							 strchecked=" disabled"
													else
													strchecked=""
												end if	
    					 response.write "<tr><td>" & i & "<input type=checkbox name=reminders value=" & rspn("pcn")& strchecked & "></td><td>" & rspn("registration") & "</td><td>" & rspn("site") & "</td><td>" & rspn("date of violation") & "</td><td>" & rspn("arrival time") & "</td><td>" & rspn("departure time") & "</td><td>" & rspn("pcn") & "</td><td>" & rspn("pcnsent") & "</td><td>" & incomingletterstatus & "</td></tr>" & vbcrlf
    					 closers rsqv
    					 		 i=i+1
					 				 
									
					

				'rsr.close	 
rspn.movenext
loop


 response.write "</table><table><tr><td><input type=submit name=submit value='Print Reminders for checked Violators' class=button></td>"

'response.write "<td><a class=button href=printreissue.asp>Print Tickets</a></td>"

response.write "</tr></table></form><br>"
 
'response.write k & " records were deleted as they were sent within 14 days or duplicate registrations<br>"



'else
'delete adn redirect to printnewpcn

'end if

end sub
function checkifpermit(registration)
sqlp="select * from apermits where registration='" & registration & "'"
openrs rsp,sqlp
if rsp.eof and rsp.bof then
	 checkifpermit=0
else
checkifpermit=1
end if


closers rsp

end function
sub deletepcn
if request("cmd2")=""  then
%>
<form method=post action=admin.asp?cmd=deletepcn>
<input type=hidden name=cmd2 value=search>
<table class=maintable2>
<tr>
<td>PCN</td>
<td><input type=text name=pcn>
</tr>
<tr><td colspan=2 align=center><input type=submit value="Delete" class=button>
</table>
</form>


<%
elseif request("cmd2")="search" then
spcn=request("pcn")
sql="select * from violators where pcn='" & left(spcn,7) & "' and site='" & right(spcn,3) & "'"
openrs rs,sql
if rs.eof and rs.bof then 
	 response.write "PCN does not exist"
	 exit sub
end if
''check if paid
sqlc="select * from payments where pcn='"& spcn & "'"
openrs rsc,sqlc
if not rsc.eof and not rsc.bof then
	 response.write "Violator has a Payment. You cannot delete"
	 exit sub
end if
closers rsc
' chec if cancelled
sqlc="select * from cancelled where pcn='"& spcn & "'"
openrs rsc,sqlc
if not rsc.eof and not rsc.bof then
	 response.write "Violator is Cancelled. You cannot delete"
	 exit sub
end if
closers rsc
' check if incoming letters
sqlc="select * from vwcommunications where pcn='"& spcn & "'"
openrs rsc,sqlc
if not rsc.eof and not rsc.bof then
	 response.write "Violator has a  letter. You cannot delete"
	 exit sub
end if
closers rsc
%>
<form method=post action=admin.asp?cmd=deletepcn&cmd2=delete>
<table class=maintable2>
<tr>
<td align=center>Are you sure you want to Delete? </td>
</tr>
<tr><td>Note: <textarea name=notes></textarea></td></tr>
<tr><td align=center>
<input type=hidden name=pcn value="<%=request("pcn")%>">
<input type=hidden name=id value="<%= rs("id") %>">
<input type=submit name=submit class=button value="Delete">
</tD></tr>
</table>


<%
closers rs 
elseif request("cmd2")="delete" then

sql= "exec delviolatorpcn @id=" & request("id") & ",@reasondeleted='" & request("notes") & " Deleted by " & session("userid") & "'"
'response.write sql & "<br>"
objconn.execute sql
response.write "deleted successfully"

end if

end sub

sub printdebtrecoverysummary



'sqlpn="select registration,violators.site,convert(varchar,[date of violation],103) as [date of violation],convert(varchar,[arrival time],108) as [arrival time],convert(varchar,[departure time],108) as [departure time],violators.pcn+violators.site as pcn,pcnsent,r.reminder from violators left join reminders r on violators.pcn=r.pcn  where violators.pcn+violators.site not in (select pcn from payments where pcn is not null union all select pcn from cancelled where pcn is not null union all select pcn from debtrecovery where pcn is not null) and r.reminder<getDate()-17"
sqlpn="exec printdebtrecoverysummary"
'response.write sqlpn
set  rspn=objconn.execute(sqlpn)
i=1
response.write "<table class=""sortable"" id=""mytable""><tr><th>Print</th><th>Registration</th><th>Site<th>Date of violation</th><th>Arrival Time</th><td>Departure Time</th><th>PCN </th><th>Pcn Sent</th><th>Incoming Letter Status</th></tr>" & vbcrlf
			response.write "<form method=post action=admin.asp?cmd=printdebtrecovery>"
			response.write "Check Debt Recovery to Print:" 
			

do while not rspn.eof
	 			
				
				 		 				
										
											
										if trim(rspn("strchecked"))="disabled" then 
											 							 strchecked=" disabled"
													else
													strchecked=""
												end if	
    					 response.write "<tr><td>" & i & "<input type=checkbox name=reminders value=" & rspn("pcn")& strchecked & "></td><td>" & rspn("registration") & "</td><td>" & rspn("site") & "</td><td>" & rspn("date of violation") & "</td><td>" & rspn("arrival time") & "</td><td>" & rspn("departure time") & "</td><td>" & rspn("pcn") & "</td><td>" & rspn("pcnsent") & "</td><td>" & rspn("reasondisabled")& "</td></tr>" & vbcrlf
    					 closers rsqv
    					 		 i=i+1
					 				 
									
					

				'rsr.close	 
rspn.movenext
loop


 response.write "</table><table><tr><td><input type=submit name=submit value='Print Debt Recovery for checked Violators' class=button></td>"

'response.write "<td><a class=button href=printreissue.asp>Print Tickets</a></td>"

response.write "</tr></table></form><br>"
 
'response.write k & " records were deleted as they were sent within 14 days or duplicate registrations<br>"



'else
'delete adn redirect to printnewpcn

'end if

end sub

sub printdebtrecovery
'insert into reminders table
' create batch 
''redirect to batch reminder reprint
reminders=request("reminders")
arrreminders=split(reminders,",")

sql="select max(batchid) as batchid from debtrecoverybatches"
	openrs rs,sql
	newbatchid=rs("batchid")+1
	closers rs
	sql="insert into debtrecoverybatches(batchid,datetime) values(" & newbatchid & ",getdate())"
	objconn.execute sql
	
for i=lbound(arrreminders) to ubound(arrreminders)
		spcn=trim(arrreminders(i))
		sql="exec insertdebtrecovery @pcn='" & spcn  & "',@batchid=" & newbatchid & ",@userid=" & session("userid")
	'response.write sql
	objconn.execute sql
	
next
response.redirect "printdebtrecovery.asp?batchid=" & newbatchid

end sub

sub reprintdebtrecovery
		
			sql="select * from debtrecoverybatches order by batchid desc"
			response.write "<form method=post action=printdebtrecovery.asp target=_new>Choose Batch"
			response.write "<select name=batchid>"
			openrs rs,sql
			do while not rs.eof
				 response.write "<option value=" & rs("batchid") & ">" & rs("datetime") & " BatchID:" & rs("batchid") & "</option>"
				 			
			rs.movenext
			loop
			response.write "</select><input type=submit name=submit class=button value=Reprint></form>"
			closers rs
			
			
			
			
			
			end sub
			
			sub reprintdebtrecoverytoexcel
		
		if request("cmd2")="" then
			sql="select * from debtrecoverybatches order by batchid desc"
			response.write "<form method=post action=admin.asp?cmd=reprintdebtrecoverytoexcel&cmd2=go target=_new>Choose Batch"
			response.write "<select name=batchid>"
			openrs rs,sql
			do while not rs.eof
				 response.write "<option value=" & rs("batchid") & ">" & rs("datetime") & " BatchID:" & rs("batchid") & "</option>"
				 			
			rs.movenext
			loop
			response.write "</select><input type=submit name=submit class=button value=Reprint></form>"
			closers rs
			
			else
	sql="SELECT violators.Registration,violators.site,violators.pcn+violators.site as pcn,violators.pcnsent,violators.pcnsent as reminder,convert(varchar,[date of violation],103) as [date of violation],convert(varchar,[arrival time],108) as [arrival time],convert(varchar,[departure time],108) as [departure time],convert(varchar,[duration of stay],108) as [duration of stay],ticketprice,reducedamount,dvla.Make,dvla.Colour,dvla.Owner,dvla.Address1,dvla.Address2,dvla.Address3,dvla.Town,dvla.Postcode,[Site Information].[Name of Site] FROM violators  LEFT OUTER JOIN dbo.[Site Information] ON dbo.violators.Site = dbo.[Site Information].SiteCode LEFT OUTER JOIN dbo.DVLA ON dbo.violators.dvlaid = dbo.DVLA.id where violators.id in(select violators.id from debtrecovery left join violators on violators.pcn+violators.site=debtrecovery.pcn where debtrecovery.batchid=" & request("batchid") & ")  order by pcnsent,site,[date of violation],registration"
'response.write sql
call outputexcel(sql)
			
			end if
			end sub
			
			
	sub dvlarepsearch
			if request("cmd2")<>"search" then
			%>
			<form method=post action=admin.asp?cmd=dvlarepsearch>
	<input type=hidden name=cmd2 value="search" />
	From <input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>
To <input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>
Site
<select name=sites><option value="">All Sites</option>
<% 


'if session("adminsite")=true then
	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites
'end if
for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
<input type=submit name=submit class=button value="Show Report">
	
	</form>

			
			<%else
		datefrom=request("datefrom")
		dateto=request("dateto")
		site=request("sites")
		if site="" then 
		    site="Null"
		else
		    site=tosql(site,"Text")
		    
		  end if
		sql="exec dvlarepsearch @datefrom=" & tosql(datefrom,"date") & ",@dateto='" & cisodate(dateto) & " 23:59',@site=" & site 
		'response.Write sql & "<br>"
		openrs rs,sql
		response.Write "<table class=maintable2><tr><th>Total amount requested</th><td>" & rs("totalreq") & "</td></tr>"
		response.Write "<tr><th>Total amount received back</th><td>" & rs("totalrecback") & "</td></tr>"
		response.Write "<tr><th>Total The amount of PCN printed</th><td>" & rs("amountpcnprinted") & "</td></tr>"
 
		
		closers rs
		sql2="exec spdvlarepsearchdetails @datefrom=" & tosql(datefrom,"date") & ",@dateto='" & cisodate(dateto) & " 23:59',@site='" & site & "'"
	'response.Write sql2
	response.write "<tr><td colspan=2>"
		call outputexcel(sql2)
		response.Write "</td></tr></table>"
			end if
			end sub
 
 
 
 sub exemptionsnoplates
			if request("cmd2")<>"search" then
			%>
			<form method=post action=admin.asp?cmd=exemptionsnoplates>
	<input type=hidden name=cmd2 value="search" />
	Date <input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>


<input type=submit name=submit class=button value="Show Report">
	
	</form>

			
			<%else
		datefrom=request("datefrom")
	
		sql="exec spexemptionsnoplates  @datefrom=" & tosql(datefrom,"date")
		'response.Write sql
		call Write_HTML(sql)
		call outputexcel(sql)
			end if
			end sub
			
			sub plateimagesfuzzy
			if request("cmd2")<>"search" then
			%>
			<form method=post action=admin.asp?cmd=plateimagesfuzzy>
	<input type=hidden name=cmd2 value="search" />
	Date <input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>
Site
    <select name=sites>
<% 


'if session("adminsite")=true then
	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites
'end if
for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
    <br />
    Accuracy <= <input type=text name=accuracy />
<br />
<input type=submit name=submit class=button value="Show Report">
	
	</form>

			
			<%else
		datefrom=request("datefrom")
	    site=request("sites")
	    if datefrom="" or site="" then
	    
	    response.Write "You must fill in a Date and Site to Search"
	    end if
	    accuracy=request("accuracy")
	    if accuracy="" then accuracy=100
	    sortby=request("sortby")
	    if sortby="" then sortby="plate"
	   sql="exec spplateimages @datesearch=" & tosql(datefrom,"date")& ",@site='" & site & "',@accuracy=" & accuracy & ",@sortby='" &  sortby & "'"
	 ' response.Write sql
	    openrs rs,sql
	 if not rs.eof and not rs.bof then 
	 qs="&datefrom=" & datefrom & "&sites=" & site & "&accuracy=" & accuracy
	  response.Write "<table class=maintable2><tr><td><a href=admin.asp?cmd=plateimagesfuzzy&cmd2=search&sortby=plate" & qs & ">Plate</a></td><td>Image</td><td><a href=admin.asp?cmd=plateimagesfuzzy&cmd2=search&sortby=accuracy" & qs & ">Accuracy</td><td>Fuzzie matches</td></tr>"
	 
	    itemimageurl=configweburl & "plateimages/server1/" 
	   do while not rs.eof
	  fuzzystr=checkfuzzyimages(rs("plate"),rs("site"),rs("date"))
	  if fuzzystr<>"0" then
     filename = year(rs("date")) & "/" & month(rs("date")) & "/" & day(rs("date")) & "/" & hour(rs("date")) & "/" & rs("picture")
     
   itemimagesrc="<img src='" & itemimageurl & filename & "' " & Replace(ImageResize(itemimage & filename,400, 120),"""","'") & " border='0' align='middle' valign='middle'>"
   
	        response.Write "<tr><td>" & rs("plate") & "</td><td>"&itemimagesrc & "</td><td>" & rs("accuracy") & "</td><td>" & fuzzystr & "</td></tr>"
	           response.flush
	           end if
	   rs.movenext
	   
	   loop
	   response.Write "</table>"
	   else
	    response.Write "No records matched search"
	  end if
	  	closers rs
			end if
			
			end sub
		sub plateimagesfuzzyex
			if request("cmd2")<>"search" then
			%>
			<form method=post action=admin.asp?cmd=plateimagesfuzzyex>
	<input type=hidden name=cmd2 value="search" />
	Date <input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)

<input type=submit name=submit class=button value="Show Report">
	
	</form>

			
			<%else
		datefrom=request("datefrom")
	    site="036"
	    'request("sites")
	    if datefrom="" or site="" then
	    
	    response.Write "You must fill in a Date  to Search"
	    end if
	    accuracy=request("accuracy")
	    if accuracy="" then accuracy=100
	    sortby=request("sortby")
	    if sortby="" then sortby="plate"
	   sql="exec spplateimages @datesearch=" & tosql(datefrom,"date")& ",@site='" & site & "',@accuracy=" & accuracy & ",@sortby='" &  sortby & "'"
	 ' response.Write sql
	    openrs rs,sql
	 if not rs.eof and not rs.bof then 
	 qs="&datefrom=" & datefrom & "&sites=" & site & "&accuracy=" & accuracy
	  response.Write "<table class=maintable2><tr><td><a href=admin.asp?cmd=plateimagesfuzzy&cmd2=search&sortby=plate" & qs & ">Plate</a></td><td>Image</td><td><a href=admin.asp?cmd=plateimagesfuzzy&cmd2=search&sortby=accuracy" & qs & ">Accuracy</td><td>Fuzzie matches</td></tr>"
	 
	    itemimageurl=configweburl & "plateimages/server1/" 
	   do while not rs.eof
	
	if checkdistinctplate(rs("plate"),site,datefrom) = true then
	  fuzzystr=checkfuzzyimagesex(rs("plate"),rs("site"),rs("date"))
	  if fuzzystr<>"0" then
     filename = year(rs("date")) & "/" & month(rs("date")) & "/" & day(rs("date")) & "/" & hour(rs("date")) & "/" & rs("picture")
     
   itemimagesrc="<img src='" & itemimageurl & filename & "' " & Replace(ImageResize(itemimage & filename,400, 120),"""","'") & " border='0' align='middle' valign='middle'>"
   
	        response.Write "<tr><td>" & rs("plate") & "</td><td>"&itemimagesrc & "</td><td>" & rs("accuracy") & "</td><td>" & fuzzystr & "</td></tr>"
	           response.flush
	         end if  
	           end if
	   rs.movenext
	   
	   loop
	   response.Write "</table>"
	   else
	    response.Write "No records matched search"
	  end if
	  	closers rs
			end if
			
			end sub	
function checkfuzzyimages(plate,site,mydate)

	sqlp="select * from plateimages where site='" & site & "' and date>='" & cisodate(mydate) & "' and date<='" &  cisodate(mydate) & " 23:59'"
	'response.Write sqlp
	openrs rsp,sqlp
	fuzzyp=0
	matchingfuzzies=""
	dim afuzzies(500)
	do while not rsp.eof
	ic=0
		permitplate=rsp("plate")
		lenplate=len(plate)
		lenpermitplate=len(permitplate)
		if lenplate<lenpermitplate then
			j=lenplate
		else
			j=lenpermitplate
		end if	
		
		result=0
	For i = 1 To j 
		if mid(plate,i,1)=mid(permitplate,i,1) then result=result+1
	
	next
		resultb=0
		For i = j To 1 STEP -1
			if mid(plate,i,1)=mid(permitplate,i,1) then resultb=resultb+1
			next
	
	if resultb>result then result=resultb
	ifuzzyp=result*100
	ifuzzyp=ifuzzyp/j
	if fuzzyp<ifuzzyp then 
		fuzzyp=ifuzzyp
		fpermitplate=permitplate
	'	fpermitid=rsp("id")
	end if
	if fuzzyp>60 and fuzzyp<100 then
			'check if in array and only show if not iin array already
			if not elementinarray(afuzzies,fpermitplate,0) then 
			    itemimageurl=configweburl & "plateimages/server1/" 
	            ffilename = year(rsp("date")) & "/" & month(rsp("date")) & "/" & day(rsp("date")) & "/" & hour(rsp("date")) & "/" & rsp("picture")
     fitemimagesrc= ffilename
   fitemimagesrc=fitemimagesrc & "<img src='" & itemimageurl & ffilename & "' " & Replace(ImageResize(itemimage & ffilename,400, 120),"""","'") & " border='0' align='middle' valign='middle'>"
   
				 matchingfuzzies=matchingfuzzies &  rsp("date") & " " &  fpermitplate &"-" &  formatnumber(fuzzyp,2) & "%" & fitemimagesrc & "<br>"
				 afuzzies(ic)=fpermitplate
end if

	end if
	
	
		rsp.movenext
		ic=ic+1
	loop
	closers rsp

	
if matchingfuzzies="" then 
	 matchingfuzzies="0"

end if
checkfuzzyimages=matchingfuzzies
end function

function checkfuzzyimagesex(plate,site,mydate)

	sqlp="select * from exemptions where site='" & site & "' and datetime>='" & cisodate(mydate-21) & "' and datetime<='" &  cisodate(mydate) & " 23:59'"
	'response.Write sqlp
	openrs rsp,sqlp
	fuzzyp=0
	matchingfuzzies=""
	dim afuzzies(500)
	do while not rsp.eof
	ic=0
		permitplate=rsp("registration")
		lenplate=len(plate)
		lenpermitplate=len(permitplate)
		if lenplate<lenpermitplate then
			j=lenplate
		else
			j=lenpermitplate
		end if	
		
		result=0
	For i = 1 To j 
		if mid(plate,i,1)=mid(permitplate,i,1) then result=result+1
	
	next
		resultb=0
		For i = j To 1 STEP -1
			if mid(plate,i,1)=mid(permitplate,i,1) then resultb=resultb+1
			next
	
	if resultb>result then result=resultb
	ifuzzyp=result*100
	ifuzzyp=ifuzzyp/j
	if fuzzyp<ifuzzyp then 
		fuzzyp=ifuzzyp
		fpermitplate=permitplate
	'	fpermitid=rsp("id")
	end if
	if fuzzyp>60 and fuzzyp<100 then
			'check if in array and only show if not iin array already
			if not elementinarray(afuzzies,fpermitplate,0) then 
			    itemimageurl=configweburl & "exemptions/server1/" 
	            ffilename = year(rsp("datetime")) & "/" & month(rsp("datetime")) & "/" & day(rsp("datetime")) & "/" & hour(rsp("datetime")) & "/" & rsp("picture")
   '  fitemimagesrc= ffilename
   fitemimagesrc=fitemimagesrc & "<img src='" & itemimageurl & ffilename & "' " & Replace(ImageResize(itemimage & ffilename,400, 120),"""","'") & " border='0' align='middle' valign='middle'>"
   
				 matchingfuzzies=matchingfuzzies &  rsp("datetime") & " " &  fpermitplate &"-" &  formatnumber(fuzzyp,2) & "%" & fitemimagesrc & "<br>"
				 afuzzies(ic)=fpermitplate
end if

	end if
	
	
		rsp.movenext
		ic=ic+1
	loop
	closers rsp

	
if matchingfuzzies="" then 
	 matchingfuzzies="0"

end if
checkfuzzyimagesex=matchingfuzzies
end function

sub s036exemptionssingle
			if request("cmd2")<>"search" then
			%>
			<form method=post action=admin.asp?cmd=s036exemptionssingle>
	<input type=hidden name=cmd2 value="search" />
	Date <input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>

   
<input type=submit name=submit class=button value="Show Report">
	
	</form>

			
			<%else
		datefrom=request("datefrom")
	  
	    if datefrom=""  then
	    
	    response.Write "You must fill in a Date to Search"
	    end if
	 
	    sortby=request("sortby")
	    if sortby="" then sortby="registration"
	    response.Write "<h2>036 exemptions that don't have matching plate images</h2>"
	   sql="exec sp036exemptionssingle @datesearch=" & tosql(datefrom,"date")&  ",@sortby='" &  sortby & "'"
	 ' response.Write sql
	    openrs rs,sql
	 if not rs.eof and not rs.bof then 
	 
	  response.Write "<table class=maintable2><tr><td>Plate</td><td>Image</td></tr>"
	 
	    itemimageurl=configweburl & "exemptions/server1/" 
	    i=1
	   do while not rs.eof
	 ' if checkdistinctplate(rs("plate"),site,datefrom) = true then
     filename = year(rs("datetime")) & "/" & month(rs("datetime")) & "/" & day(rs("datetime")) & "/" & hour(rs("datetime")) & "/" & rs("picture")
     
   itemimagesrc="<img src='" & itemimageurl & filename & "' " & Replace(ImageResize(itemimage & filename,400, 120),"""","'") & " border='0' align='middle' valign='middle'>"
   
	        response.Write "<tr><td><a name=" & rs("registration") & "></a>" & rs("registration") & "</td><td>"&itemimagesrc & "</td><td><a href=""javascript:myPopup('editexplate.asp?id=" & rs("id") & "&origplate=" & rs("registration") & "', 'Editplate','400','400','100','100')"">Edit Plate</a></td></tr>"
	           response.flush
	  'end if
	  i=i+1
	   rs.movenext
	   loop
	   response.Write "</table>"
	   else
	    response.Write "No records matched search"
	  end if
	  	closers rs
			end if
			
			end sub
			
			sub plateimagessingle
			if request("cmd2")<>"search" then
			%>
			<form method=post action=admin.asp?cmd=plateimagessingle>
	<input type=hidden name=cmd2 value="search" />
	Date <input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>
Site
    <select name=sites>
<% 


'if session("adminsite")=true then
	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites
'end if
for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
    <br />
   
<input type=submit name=submit class=button value="Show Report">
	
	</form>

			
			<%else
		datefrom=request("datefrom")
	    site=request("sites")
	    if datefrom="" or site="" then
	    
	    response.Write "You must fill in a Date and Site to Search"
	    end if
	 
	    sortby=request("sortby")
	    if sortby="" then sortby="plate"
	   sql="exec spplateimagessingle @datesearch=" & tosql(datefrom,"date")& ",@site='" & site & "',@sortby='" &  sortby & "'"
	 ' response.Write sql
	    openrs rs,sql
	 if not rs.eof and not rs.bof then 
	 
	  response.Write "<table class=maintable2><tr><td>Plate</td><td>Image</td></tr>"
	 
	    itemimageurl=configweburl & "plateimages/server1/" 
	    i=1
	   do while not rs.eof
	  if checkdistinctplate(rs("plate"),site,datefrom) = true then
     filename = year(rs("date")) & "/" & month(rs("date")) & "/" & day(rs("date")) & "/" & hour(rs("date")) & "/" & rs("picture")
     
   itemimagesrc="<img src='" & itemimageurl & filename & "' " & Replace(ImageResize(itemimage & filename,400, 120),"""","'") & " border='0' align='middle' valign='middle'>"
   
	        response.Write "<tr><td><a name=" & rs("plate") & "></a>" & rs("plate") & "</td><td>"&itemimagesrc & "</td><td><a href=""javascript:myPopup('editplate.asp?id=" & rs("id") & "&origplate=" & rs("plate") & "', 'Editplate','400','400','100','100')"">Edit Plate</a></td></tr>"
	           response.flush
	  end if
	  i=i+1
	   rs.movenext
	   loop
	   response.Write "</table>"
	   else
	    response.Write "No records matched search"
	  end if
	  	closers rs
			end if
			
			end sub
			
function checkdistinctplate(plate,site,datefrom)
sqld="exec spcheckplatedistinct @datesearch=" & tosql(datefrom,"date")& ",@site='" & site & "',@plate='" & plate & "'"
'response.Write sqld
openrs rsd,sqld

if rsd("count")=1 then
    checkdistinctplate=true
 else
    checkdistinctplate=false
end if
'response.Write checkditinctplate
closers rsd
end function


sub triangletranscription


cmd2=request("cmd2")
if cmd2="" then
%>
    <form method=post action=admin.asp?cmd=triangletranscription&cmd2=search>
    <table class=maintable2>
   <tr><td>
From </td><td><input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To </td><td><input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)

</td></tr>
<tr><td colspan=2><input type=checkbox name=nt value=yes />Show only not transcribed</td></tr>

<tr><td colspan=2><input type=checkbox name=nm value=yes />Show only not 100% Match</td></tr>
<tr><td>Site</td><td>
<select name=site><option value="">All</option>
<option value=4380>Oxford</option>
<option value=3526>Triangle</option>
<option value=4586>Goldstone</otion>
<option value=4590>Ayr</option>


</select></td></tr>
<tr><td colspan=2 align=center>
<input type=submit name=submit value="Submit" class=button>
</td></tr></table>
    
    </form>
<%
'postto="http://clients.creativecarpark.co.uk/triangle/tempmove2.asp"
 'set xmlhttp = CreateObject("MSXML2.ServerXMLHTTP") 
  '  xmlhttp.open "GET", postto, false 
   ' xmlhttp.send
    
    'set xmlhttp = nothing 
end if
if cmd2="search" then



%>

<!--#include file=openconntriangle.asp-->

<%
    datefrom=request("datefrom")
    dateto=request("dateto")
    nt=request("nt")
    nm=request("nm")
site=request("site")
    sql="select id,PaymentReference,DateTime,Amount,Duration,Telephone,VehicleReg,sitecode from trianglepayment where 1=1 "
    if datefrom<>"" then sql=sql & " and datetime>=" &tosql(datefrom,"date")
    if dateto<>"" then sql=sql & " and datetime<='" &cisodate(dateto) & " 23:59'"
    if nt="yes" then sql=sql & " and vehiclereg is null"
    if request("nm")="yes" then sql=sql & " and (fuzzymatch<>100 or fuzzymatch is null)"
    if site<>"" then
        if site="087" then
             sql=sql & " and (sitecode='3526' or sitecode is null)"
        else
            sql=sql & " and sitecode='" & site & "'"
        end if     
        
    
  end if 
  '  sql=sql & " order by telephone,datetime"
  ' response.Write sql
    set rst=objconnt.execute(sql)
    if not rst.eof and not rst.bof then
        strpagename="admin.asp?cmd=triangletranscription&cmd2=search&datefrom=" & datefrom & "&dateto=" & dateto & "&nt=" & nt & "&nm=" & nm & "&site=" & site
         createSortableListtriangle2 objConnt,sql,"telephone,datetime",25,"class=maintable2 cellpadding=10",strpagename
    else
        response.Write "No records matched your search"
    end if
    rst.close
end if
end sub
sub triangleapprove
if request.Form("paymentref")="" then 
%>
<form method=post action=admin.asp?cmd=triangleapprove>
Payment Reference  <input type=text name=paymentref /> <input type=submit value="approve" />

</form>

<% else %>
<!--#include file=openconntriangle.asp-->
<%

paymentref=request("paymentref")
sql="select * from trianglepayment where paymentreference='" & paymentref & "'"
set rs=objconnt.execute(sql )
if rs.eof and rs.bof then   
    response.Write " payment reference does not exist"
else
    if rs("status")="successful" then
         response.Write "Status is already successful"
     else    
        sqlu="update trianglepayment set status='successful' where paymentreference='" & paymentref & "'"
        objconnt.execute sqlu
        response.Write "Status changed Sucessfully"
     end if   
end if
closers rs

end if 

end sub

sub trianglestatus
%>
<!--#include file=openconntriangle.asp-->
<%
if request("status")<>"" then 
sql="update trianglepayment set status=" & tosql(request("status"),"text") & " where id=" & request("id")
'response.Write sql
objconnt.execute sql

end if
sql="select * from trianglepayment where (status is null or status='') and datetime>'20090501'"
set rs=objconnt.execute(sql)
if rs.eof and rs.bof then 
    response.Write "No records"
else
response.Write "<table><tr><th>Payment Reference</th><th>Date</th></tr>"     
do while not rs.eof 
   response.Write "<tr><td>" & rs("paymentreference") & "</td><tD>" & rs("datetime") & "</td><td><a href=admin.asp?cmd=trianglestatus&id=" & rs("id") & "&status=Successful>Mark Success</a> <a href=admin.asp?cmd=trianglestatus&id=" & rs("id") & "&status=Declined>Mark Declined</a></td></tr>" 

rs.movenext 
loop
response.Write "</table>"

end if
closers rs 


end sub
Sub createSortableListtriangle(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
  '  response.write "xx" & strsort & "xx"			
    		Set RS = server.CreateObject("adodb.recordset")
   ' response.write strsql & " order by " & replace(strSort,"desc"," desc")
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"id"  then
				Response.Write "<Th align=center>" & vbcrlf
				
    fieldname=field.name
	
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</Th>"	& vbcrlf
    			end if
	
	Next
					
    		Response.Write "<th>Listen</th></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
						 if i mod 2=1 then
    				 		Response.Write "<TR>" & vbcrlf
						else
								Response.Write "<TR class=rowcolor2>" & vbcrlf
							end if
    			
					For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"id" then
					
						response.Write "<td>"
						 fieldvalue=field.value
						 if field.name="Duration" then fieldvalue=showduration(fieldvalue)
						 if field.name="Amount" then fieldvalue="£" & formatnumber(fieldvalue,2)
						 Response.Write fieldvalue
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
					'days=datediff("d",rs("Date"),date)
					'response.write "<td align=center>" & days & "</td>"
				
				telephone=rs("telephone")
mydate=rs("datetime")
mymonth=month(rs("datetime"))
if len(mymonth)<2 then mymonth="0" & mymonth
myday=day(rs("datetime"))
if len(myday)<2 then myday="0" & myday
mytime=rs("datetime")
myhour=hour(mytime)
if len(myhour)<2 then myhour="0" & myhour
myminute=minute(mytime)
if len(myminute)<2 then myminute="0" & myminute
mysecond=second(mytime)
if len(mysecond)<2 then mysecond="0" & mysecond
mytime=myhour & myminute & mysecond
filename=telephone & "_" & year(mydate) & "-" & mymonth & "-" & myday & "_" & replace(mytime,":","") & ".wav"
'response.Write filename

if servername="datacenter" then
    set fs=Server.CreateObject("Scripting.FileSystemObject")


        mypath="D:\Shared Folders and Data\Inetpub\wwwroot\creativeclients\triangle\messages\"



    if fs.FileExists(mypath &filename)=true then				
				    response.write "<td><a href=admin.asp?cmd=listentriangle&p=1&id=" & rs("id") &  " target=_new class=button>Listen</a></td>"
	    end if			
    	
	    set fs=nothing			
else
     '  response.Write filename
        postto= "http://clients.creativecarpark.co.uk/triangle/checkforfile.asp?filename=" & filename
     ' response.Write postto
        set xmlhttp = CreateObject("MSXML2.ServerXMLHTTP") 
        xmlhttp.open "GET", postto, false 
        xmlhttp.send
      
       rtext= xmlhttp.responseText 
          
        set xmlhttp = nothing 
        if rtext="exists" then
              response.write "<td><a href=admin.asp?cmd=listentriangle&p=1&id=" & rs("id") &  " target=_new class=button>Listen</a></td>"
        
        end if
      






end if				
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "</TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&page=2&sort="& strSort  & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing			
    	End Sub
    
  sub ktranscribe
set objConns=Server.Createobject("ADODB.Connection")


strcon="Driver={SQL Server};Server=10.10.1.11;Database=phoneandpay;UID=phoneandpay;PWD=eshterph0ne34"
	
objconns.open strcon

cmd2=request("cmd2")
response.Write "Transcribe:<Br>"
if cmd2="" then



    
    sql="select customerid,mobilenumber,created,voicemailref from customers where transcriptionfile is not null and transcriptionrecorded is null"
   
 
    set rst=objconns.execute(sql)
    if not rst.eof and not rst.bof then
        strpagename="admin.asp?cmd=ktranscribe"
                 createSortableListtrianglek objConns,sql,"mobilenumber",25,"class=maintable2 cellpadding=10",strpagename
    else
        response.Write "No records matched your search"
    end if
    rst.close
end if
'sql="select telephone,datetime,paymentreference,vehiclereg from transcribe where vehiclereg is not null"
'call outputexcel(sql)
end sub
Sub createSortableListtrianglek(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
  '  response.write "xx" & strsort & "xx"			
    		Set RS = server.CreateObject("adodb.recordset")
   ' response.write strsql & " order by " & replace(strSort,"desc"," desc")
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"id"  then
				Response.Write "<Th align=center>" & vbcrlf
				
    fieldname=field.name
	
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</Th>"	& vbcrlf
    			end if
	
	Next
					
    		Response.Write "<th>Listen</th></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
						 if i mod 2=1 then
    				 		Response.Write "<TR>" & vbcrlf
						else
								Response.Write "<TR class=rowcolor2>" & vbcrlf
							end if
    			
					For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"id" then
					
						response.Write "<td>"
						 fieldvalue=field.value
						 if field.name="Duration" then fieldvalue=showduration(fieldvalue)
						 if field.name="Amount" then 
						   if fieldvalue="" or isnull(fieldvalue) then fieldvalue=0
						    fieldvalue="£" & formatnumber(fieldvalue,2)
						   
						 end if   
						 Response.Write fieldvalue
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
					'days=datediff("d",rs("Date"),date)
					'response.write "<td align=center>" & days & "</td>"
				
				

              response.write "<td><a href=admin.asp?cmd=listentrianglek&p=1&id=" & rs("customerid") &  " target=_new class=button>Listen</a></td>"
    





			
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "</TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&page=2&sort="& strSort  & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing			
    	End Sub	
sub listentrianglek
    set objConns=Server.Createobject("ADODB.Connection")


strcon="Driver={SQL Server};Server=10.10.1.11;Database=phoneandpay;UID=phoneandpay;PWD=eshterph0ne34"
	
objconns.open strcon
     id=request("id")
     cmd2=request("cmd2")
     if cmd2="" then
        sql="select * from customers where customerid=" & id
       set rs=objconns.execute(sql)
       
        %>
       
        
        <form method=post action=admin.asp?cmd=listentrianglek>
        <input type=hidden name=cmd2 value=save />
        <input type=hidden name=id value=<%=id %> />
      
        
        <table class=maintable2>
      <% if request("phone")<>"yes" then  %>
        <tr><td colspan=2>
     <object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" width="300" height="50">
    <param name="src" value="http://www.phoneandpay.co.uk/messages/<%=rs("transcriptionfile")%>">
    <param name="autoplay" value="true">
    <embed type="audio/x-wav" src="http://www.phoneandpay.co.uk/messages/<%=rs("transcriptionfile")%>" autoplay="true" autostart="true" width="300" height="50">
</object>
		<input type="hidden" name="method" value="transcription" />

        </td></tr>
        <% else  %>
        <input type="hidden" name="method" value="phone" />
        <tr><td colspan=2>This Vehicle is not provided. Please call <%=telephone %></td></tr>
      
        <% end if %>
        <tr><td>Vehicle Registration</td><td><input type=text name=vehiclereg /></td></tr>
        <tr><td colspan=2 align=center><input type=submit name=submit value=Save class=button /></td></tr>

        </table>
        </form>
        <%
      '  closers rs
     end if 
     if cmd2="save" then
        
           response.write "<form method=post action=admin.asp?cmd=listentrianglek&cmd2=savesure name=myform>"
           response.Write "<input type=hidden name=id value=" & id & ">"
         
           vehiclereg=request("vehiclereg")
            vehiclereg=cleanplate(vehiclereg)
            vehiclereg=ucase(vehiclereg)
            if not isalphanumeric(vehiclereg) then response.write "<br><font class=red>Registration must be alphanumeric</font>"
           
            response.write "<br>Vehicle Registration:<br><input type=text name=vehiclereg value=" & vehiclereg & "><br><br>Are you sure you want to save?<bR>"
           
           cardetails=getcardetails(vehiclereg)
           acardetails=split(cardetails,",")
           response.Write "<br>Make:" & acardetails(0)
           response.Write "<br>Model:" & acardetails(1)
           response.Write "<br>Color:" & acardetails(2)
           
            response.Write "<br><a class=button href=""javascript:window.close()"">Cancel</a>&nbsp;<input type=submit name=submit value=Save class=button></form>"
            
       
     
     
   end if
 
    if cmd2="savesure" then
        
            vehiclereg=request("vehiclereg")
            fuzzymatch=0
          vehiclereg=cleanplate(vehiclereg)
            vehiclereg=ucase(vehiclereg)
             if not isalphanumeric(vehiclereg) then 
                response.write "<br><font class=red>Registration must be alphanumeric</font>"
                response.End
           end if
         sqltu="exec transcribe @vehicleplate=" & tosql(vehiclereg,"text")  & ",@customerid=" & id
         
            objconns.execute sqltu
            
                  
              response.Write "Saved<br><a href=admin.asp?cmd=ppsendsms&id=" & id & " class=button>Send SMS</a><br><a class=button href=""javascript:window.close()"">Close</a>"
            
     
     end if  
end sub 
  sub ppsendsms
  id=request("id")
  
  set objConns=Server.Createobject("ADODB.Connection")


strcon="Driver={SQL Server};Server=10.10.1.11;Database=phoneandpay;UID=phoneandpay;PWD=eshterph0ne34"
	
objconns.open strcon


  sql="select * from customers where customerid=" & id 
  'response.Write sql 
  set rs=objconns.execute(sql)
  number=rs("mobilenumber")
  vehiclereg=rs("vehicleplate")
  postto="http://www.phoneandpay.co.uk/sendsms.asp?number=" & number & "&plate=" & vehiclereg 
 ' response.Write postto
 set xmlhttp = CreateObject("MSXML2.ServerXMLHTTP") 
    xmlhttp.open "GET", postto, false 
    xmlhttp.send
    
    set xmlhttp = nothing 
   
  rs.close 
  set rs=nothing
  
  
  end sub 
    Sub createSortableListtriangle2(objConn,strSQL, strDefaultSort, intPageSize, strTableAttributes,strpagename)
session("sql")=strsql
'response.write strsql
    		Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
  '  response.write "xx" & strsort & "xx"			
    		Set RS = server.CreateObject("adodb.recordset")
   ' response.write strsql & " order by " & replace(strSort,"desc"," desc")
    		With RS
    			.CursorLocation=3
    			.Open strSQL & " order by " & replace(strSort,"desc"," desc"), objConn,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
    
			For Each field In RS.Fields 'loop through the fields in the recordset
  					if field.name<>"id"  then
				Response.Write "<Th align=center>" & vbcrlf
				
    fieldname=field.name
	
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name & "&page=" & intCurrentPage & ">" & field.name & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& field.name &"desc&page=" & intCurrentPage & ">" & field.name & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</Th>"	& vbcrlf
    			end if
	
	Next
					
    		Response.Write "<th>Listen</th></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
						 if i mod 2=1 then
    				 		Response.Write "<TR>" & vbcrlf
						else
								Response.Write "<TR class=rowcolor2>" & vbcrlf
							end if
    			
					For Each field In RS.Fields 'for each field in the recordset
				if field.name<>"id" then
					
						response.Write "<td>"
						 fieldvalue=field.value
						 if field.name="Duration" then fieldvalue=showduration(fieldvalue)
						 if field.name="Amount" then 
						   if fieldvalue="" or isnull(fieldvalue) then fieldvalue=0
						    fieldvalue="£" & formatnumber(fieldvalue,2)
						   
						 end if   
						 Response.Write fieldvalue
						 	
    				Response.Write "</TD>" & vbcrlf
				end if
    			Next
					'days=datediff("d",rs("Date"),date)
					'response.write "<td align=center>" & days & "</td>"
				
				telephone=rs("telephone")
				paymentref=rs("paymentreference")
mydate=rs("datetime")
mymonth=month(rs("datetime"))
if len(mymonth)<2 then mymonth="0" & mymonth
myday=day(rs("datetime"))
if len(myday)<2 then myday="0" & myday
mytime=rs("datetime")
myhour=hour(mytime)
if len(myhour)<2 then myhour="0" & myhour
myminute=minute(mytime)
if len(myminute)<2 then myminute="0" & myminute
mysecond=second(mytime)
if len(mysecond)<2 then mysecond="0" & mysecond
mytime=myhour & myminute & mysecond
filename=telephone & "_" & paymentref & ".wav"
'response.Write filename

if servername="datacenter" then
    set fs=Server.CreateObject("Scripting.FileSystemObject")


        mypath="D:\Shared Folders and Data\Inetpub\wwwroot\creativeclients\triangle\messages\"



    if fs.FileExists(mypath &filename)=true then				
				    response.write "<td><a href=admin.asp?cmd=listentriangle&p=1&id=" & rs("id") &  " target=_new class=button>Listen</a></td>"
	    end if			
    	
	    set fs=nothing			
else
     '   response.Write filename
        postto= "http://clients.creativecarpark.co.uk/triangle/checkforfile.asp?filename=" & filename
     ' response.write postto
        set xmlhttp = CreateObject("MSXML2.ServerXMLHTTP") 
        xmlhttp.open "GET", postto, false 
        xmlhttp.send
      
       rtext= xmlhttp.responseText 
          
        set xmlhttp = nothing 
        if rtext="exists" then
              response.write "<td><a href=admin.asp?cmd=listentriangle&p=1&id=" & rs("id") &  " target=_new class=button>Listen</a></td>"
        else
             response.write "<td><a href=admin.asp?cmd=listentriangle&phone=yes&p=1&id=" & rs("id") &  " target=_new class=button>View Phone</a></td>"
        end if
      






end if				
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "</TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&page=2&sort="& strSort  & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing			
    	End Sub	
sub listentriangle
%>

<!--#include file=openconntriangle.asp-->

<%
     id=request("id")
     cmd2=request("cmd2")
     if cmd2="" then
        sql="select * from trianglepayment where id=" & id
       set rs=objconnt.execute(sql)
        telephone=rs("telephone")
      mydate=rs("datetime")
      paymentref=rs("paymentreference")
      'sitecode=rs("sitecode")
mymonth=month(rs("datetime"))
if len(mymonth)<2 then mymonth="0" & mymonth
myday=day(rs("datetime"))
if len(myday)<2 then myday="0" & myday
mytime=rs("datetime")
myhour=hour(mytime)
if len(myhour)<2 then myhour="0" & myhour
myminute=minute(mytime)
if len(myminute)<2 then myminute="0" & myminute
mysecond=second(mytime)
if len(mysecond)<2 then mysecond="0" & mysecond
mytime=myhour & myminute & mysecond
filename=telephone & "_" & year(mydate) & "-" & mymonth & "-" & myday & "_" & replace(mytime,":","") & ".wav"
filename=telephone & "_" & paymentref & ".wav"
'response.Write "http://clients.creativecarpark.co.uk/triangle/messages/" & filename
        %>
        <form method=post action=admin.asp?cmd=listentriangle>
        <input type=hidden name=cmd2 value=save />
        <input type=hidden name=id value=<%=id %> />
        <input type=hidden name=datetime value="<%=mydate %>" />
        <input type=hidden name=telephone value="<%=telephone %>"
        
        <table class=maintable2>
      <% if request("phone")<>"yes" then  %>
        <tr><td colspan=2>
     <object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" width="300" height="50">
    <param name="src" value="http://clients.creativecarpark.co.uk/triangle/messages/<%=filename%>">
    <param name="autoplay" value="true">
    <embed type="audio/x-wav" src="http://clients.creativecarpark.co.uk/triangle/messages/<%=filename%>" autoplay="true" autostart="true" width="300" height="50">
</object>
		<input type="hidden" name="method" value="transcription" />

        </td></tr>
        <% else  %>
        <input type="hidden" name="method" value="phone" />
        <tr><td colspan=2>This Vehicle is not provided. Please call <%=telephone %></td></tr>
      
        <% end if %>
        <tr><td>Vehicle Registration</td><td><input type=text name=vehiclereg /></td></tr>
        <tr><td colspan=2 align=center><input type=submit name=submit value=Save class=button /></td></tr>

        </table>
        </form>
        <%
        closers rs
     end if 
     if cmd2="save" then
        '' check plate image 
        sqldate=cisodate(request("datetime"))
        method=request("method")
        vehiclereg=request("vehiclereg")
        sqlcp="select * from plateimages where plate=" & tosql(vehiclereg,"text") & " and  date >= DATEDIFF(dd,0,'" & sqldate & "') and date<DATEADD(dd, DATEDIFF(dd,0,'" & sqldate & "'), 1)"
       ' response.Write sqlcp
        openrs rscp,sqlcp
        if rscp.bof and rscp.eof then
        
           ' response.Write "need to put are you sure here<br>"
           response.write "<form method=post action=admin.asp?cmd=listentriangle&cmd2=savesure name=myform>"
           response.Write "<input type=hidden name=id value=" & id & ">"
         
            sqlfuzzies="SELECT distinct plate,dbo.FuzzyMatch(plate,'" & vehiclereg & "') as fuzzyaccuracy  FROM plateimages   WHERE site='087' and date >= DATEDIFF(dd,0,'" & sqldate & "') and date<DATEADD(dd, DATEDIFF(dd,0,'" & sqldate & "'), 1) and dbo.FuzzyMatch(plate,'" & vehiclereg & "')>=60"
            openrs rsfuzzies,sqlfuzzies
            if rsfuzzies.eof and rsfuzzies.bof then 
                response.Write "No Fuzzy Matches"
            else
                  response.Write "Possible Fuzzies:<br>Check Fuzzy match to use or leave unchecked to use the vehicle reg below<br>"
                do while not rsfuzzies.eof
                    response.Write "<input type=radio name=fuzzymatch value=" & rsfuzzies("plate") & "!" & rsfuzzies("fuzzyaccuracy") & ">" & rsfuzzies("plate") & "<br>"
                    
                rsfuzzies.movenext
                loop
                
            end if
            sqlnum="select * from trianglepayment where vehiclereg is not null and  telephone=" & tosql(request("telephone"),"text")
            set rsnum=objconnt.execute(sqlnum)
            if not rsnum.eof and not rsnum.bof then
            response.write "<br><b>Choose Vehicle reg of Previous transcribed for this Number:"
            do while not rsnum.eof
            response.Write "<br><input type=radio name=vehicleregp value='" & rsnum("vehiclereg") & "' onclick=""document.myform.vehiclereg.value ='" & rsnum("vehiclereg") & "'"">" & rsnum("vehiclereg") 
            
            rsnum.movenext
            loop
            
            end if
            
            rsnum.close
            vehiclereg=cleanplate(vehiclereg)
            vehiclereg=ucase(vehiclereg)
            if not isalphanumeric(vehiclereg) then response.write "<br><font class=red>Registration must be alphanumeric</font>"
           
            response.write "<br>Vehicle Registration:<br><input type=text name=vehiclereg value=" & vehiclereg & "><br><br>Are you sure you want to save?<bR>"
            response.Write "<a class=button href=""javascript:window.close()"">Cancel</a>&nbsp;<input type=submit name=submit value=Save class=button></form>"
            
        else
            vehiclereg=cleanplate(vehiclereg)
            vehiclereg=ucase(vehiclereg)
             if not isalphanumeric(vehiclereg) then 
                response.write "<br><font class=red>Registration must be alphanumeric</font>"
                response.End
           end if
            sqltu="update trianglepayment set useridtranscribed=" & session("userid") & ",datetranscribed=getdate(),vehiclereg=" & tosql(vehiclereg,"text") & ",fuzzymatch=100,method=" & tosql(method,"text") & " where id=" & id
           'd response.Write sqltu
            objconnt.execute sqltu
            response.Write "Saved<br><br><a class=button href=""javascript:window.close()"">Close</a>"
            
        end if
        closers rscp
     
     
     end if 
    if cmd2="savesure" then
        if request("fuzzymatch")<>"" then
            afuzzymatch=split(request("fuzzymatch"),"!")
            vehiclereg=afuzzymatch(0)
            fuzzymatch=afuzzymatch(1)
            
        else
            vehiclereg=request("vehiclereg")
            fuzzymatch=0
        end if   
         vehiclereg=cleanplate(vehiclereg)
            vehiclereg=ucase(vehiclereg)
             if not isalphanumeric(vehiclereg) then 
                response.write "<br><font class=red>Registration must be alphanumeric</font>"
                response.End
           end if
         sqltu="update trianglepayment set useridtranscribed=" & session("userid") & ",datetranscribed=getdate(),vehiclereg=" & tosql(vehiclereg,"text") & ",fuzzymatch=" & fuzzymatch & " where id=" & id
          '  response.Write sqltu
            objconnt.execute sqltu
             response.Write "Saved<br><br><a class=button href=""javascript:window.close()"">Close</a>"
            
     
     end if  
end sub 	
function showduration(duration)
if isnumeric(duration) then
    if duration=1 then
         showduration=duration & " hour"
    elseif duration<=24 then 
        showduration=duration & " hours"
    else    
        showduration=duration/24 & " days"
    end if
 else
    showduration=duration
  end if
end function
sub triangleexemptions


%>

<!--#include file="openconntriangle.asp"-->
<%
'first more permit holders
sql="select * from permitholders where addedexemptionsdate is null or lastupdated>addedexemptionsdate"
set rst=objconnt.execute(sql)
do while not rst.eof
    if not isnull(rst("vehicleregistration1")) then
    sql="exec addexemption @site='087',@registration=" & tosql(rst("vehicleregistration1"),"text") & ",@datetime=" & tosql(rst("createddate"),"date") & ",@addedfrom='staffpermits',@startdate=" & tosql(rst("validfrom"),"date") & ",@enddate=" & tosql(rst("validto"),"date")
    'response.Write sql & "<br>"
      objconn.execute sql
          end if
     if not isnull(rst("vehicleregistration2")) then
    sql="exec addexemption @site='087',@registration=" & tosql(rst("vehicleregistration2"),"text") & ",@datetime=" & tosql(rst("createddate"),"date") & ",@addedfrom='staffpermits',@startdate=" & tosql(rst("validfrom"),"date") & ",@enddate=" & tosql(rst("validto"),"date")
   ' response.Write sql & "<br>"
      objconn.execute sql
    end if
     if not isnull(rst("vehicleregistration3")) then
    sql="exec addexemption @site='087',@registration=" & tosql(rst("vehicleregistration3"),"text") & ",@datetime=" & tosql(rst("createddate"),"date") & ",@addedfrom='staffpermits',@startdate=" & tosql(rst("validfrom"),"date") & ",@enddate=" & tosql(rst("validto"),"date")
   ' response.Write sql & "<br>"
      objconn.execute sql
    end if
    sqlu="update permitholders set addedexemptionsdate=getdate() where permitholdersid=" & rst("permitholdersid")
    objconnt.execute sqlu
rst.movenext
loop
rst.close
set rst=nothing
'season ticket contacts

        sql2="select seasonticketpermits.id,contactid,[registration number],addedexemptiondate,validto,validfrom,dateofapplication from seasonticketpermits left join seasonticketcontacts on seasonticketcontacts.id=seasonticketpermits.contactid where addedexemptiondate is null" 
        set rsts2=objconnt.execute(sql2)
            do while not rsts2.eof
                 sql="exec addexemption @site='087',@registration=" & tosql(rsts2("Registration Number"),"text") & ",@datetime=" & tosql(rsts2("dateofapplication"),"date") & ",@addedfrom='season tickets',@startdate=" & tosql(rsts2("validfrom"),"date") & ",@enddate=" & tosql(rsts2("validto"),"date")
                '  response.Write sql & "<br>"
                   objconn.execute sql
                   sqlu="update seasonticketpermits set addedexemptiondate=getdate() where id=" & rsts2("id")
                   objconnt.execute sqlu
            rsts2.movenext
            loop
        rsts2.close
        set rsts2=nothing
   ''triangle payment
   sql="select * from trianglepayment where addedexemptiondate is null and vehiclereg is not null"
   set rs=objconnt.execute(sql)
   do while not rs.eof
   registration=rs("vehiclereg")
   datetime=rs("datetime")
  
   
  startdate=rs("startdate")
  duration=rs("duration")
     
   if isnull(duration) then duration=0
   enddate=dateadd("h",duration,startdate)
   sitecode=rs("sitecode")
   if sitecode="4380" then 
    site="258"
    elseif sitecode="4586" then 
        site="271"
    elseif sitecode="4590"   then
        site="273"
   else
    site="087"
    end if  
   
        sql="exec addexemption @site='" & site & "',@registration=" & tosql(registration,"text") & ",@datetime='" & cisodate(formatdatetime(datetime,2)) & " " & formatdatetime(datetime,4) & "',@addedfrom='triangle payment',@startdate='"  & cisodate(formatdatetime(startdate,2)) & " " & formatdatetime(startdate,4) & "',@enddate='"  & cisodate(formatdatetime(enddate,2)) & " " & formatdatetime(enddate,4)  & "'"
    'response.Write sql & "<br>"
   objconn.execute sql
   sqlu="update trianglepayment set addedexemptiondate=getdate() where id=" & rs("id")
   objconnt.execute sqlu
      response.flush
   rs.movenext
   loop
   
    rs.close
    set rs=nothing
    
    ''check recharged payments 
    sql="select* from trianglepayment where type='extension' and addedexemptiondate  is null"
    set rst=objconnt.execute(sql)
    do while not rst.eof
        'check if phone number has vehiclereg
        sql2="select top 1 * from trianglepayment where vehiclereg is not null and telephone='" & rst("telephone") & "' order by startdate desc"
        set rst2=objconnt.execute(sql2)
        if not rst2.eof then
            registration=rst2("vehiclereg")
               datetime=rst("datetime")
              
               
              startdate=rst("startdate")
              duration=rst("duration")
                  
               if isnull(duration) then duration=0
               enddate=dateadd("h",duration,startdate)
                    sql="exec addexemption @site='087',@registration=" & tosql(registration,"text") & ",@datetime='" & cisodate(formatdatetime(datetime,2)) & " " & formatdatetime(datetime,4) & "',@addedfrom='triangle payment',@startdate='"  & cisodate(formatdatetime(startdate,2)) & " " & formatdatetime(startdate,4) & "',@enddate='"  & cisodate(formatdatetime(enddate,2)) & " " & formatdatetime(enddate,4)  & "'"
              ' response.Write sql & "<br>"
               objconn.execute sql
               sqlu="update trianglepayment set addedexemptiondate=getdate() where id=" & rst("id")
               objconnt.execute sqlu

         
         end if
         rst2.close
         set rst2=nothing
    rst.movenext
    loop
    rst.close
    set rst=nothing
response.Write "Exemptions Added Successfully"

end sub






sub rivergateexemptions

mypost="http://traffica.cleartoneholdings.co.uk/traffica/addrivergate.asp"
'wscript.echo(mypost)

set xmlhttp = server.CreateObject("MSXML2.ServerXMLHTTP")
    xmlhttp.open "GET", mypost, false
   on error resume next
   xmlhttp.send
   on error goto 0
    set xmlhttp = nothing

end sub
sub triangleexemptionsexcel
sql="select registration,datetime,startdate,enddate,addedfrom from exemptions where site='087'"
call outputexcel(sql)

end sub

sub deltmpplateimages
if request.Form("cmd2")="" then
%>
<form method=post action=admin.asp?cmd=deltmpplateimages>
<input type=hidden name=cmd2 value="delete" />
Are you sure you want to Delete?
<br />
Deleting will delete any possible violators that were not saved
<br />
<input type=submit name=submit value="Delete" />


</form>


<%
else
userid=session("userid")
sql="delete from tmpanpossibleviolators where userid=" & session("userid")
objconn.execute sql
'response.Write sql
sql="delete from tmpplateimages where userid=" & session("userid")
objconn.execute sql
'response.Write sql
response.Write "Deleted"
end if
end sub
sub changetriangledate
%>

<!--#include file=openconntriangle.asp-->

<%
if request.Form("cmd2")="" then
%>
<form method=post action=admin.asp?cmd=changetriangledate>
<input type=hidden name=cmd2 value="save" />
<table class=maintable2>
<tr><td>Payment Reference</td><td><input type=text name=paymentreference value="" /></td></tr>
<tr><td>Date to Change it to</td><td><input type=text id=date name=date value="">
<a href="javascript:NewCal('date','ddmmyyyy',true,24)"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a></td></tr>
<tr><td align=center><input type=submit class=button value="Save" /></td></tr>
</table>

</form>


<%
else
paymentreference=request("paymentreference")
mydate=request("date")
'response.write mydate
amydate=split(mydate," ")
sql="update trianglepayment set startdate='" & cisodate(amydate(0)) & " " & amydate(1) & "',datetime='" & cisodate(amydate(0)) & " " & amydate(1) & "',addedexemptiondate=null where paymentreference='" & request("paymentreference") & "'"
'response.Write sql
objconnt.execute sql
response.Write "saved"
end if



end sub

sub searchexemptions
if request("cmd2")="" then
%>
<table class=maintable2>
<form method=post action=admin.asp?cmd=searchexemptions>
<input type=hidden name=cmd2 value="search" />
<tr><td>Site</td><td>


    <select name=site>
<% 


'if session("adminsite")=true then
	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites
'end if
for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
</td></tr>
<tr><td>Date</td><td>


 <input type=text id="date" name=date value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('date','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
<br>
</td></tr>
<tr><td colspan=2 align=center><input type=submit name=submit value="Submit" class="button" /></td></tr>

</form>


</table>


<%

else
'response.Write "here"
mydate=request("date")
site=request("site")
sql="select   registration, datetime, dateadded, site,  startdate, enddate  from exemptions where site=" & tosql(site,"text") 
if mydate<>"" then sql=sql & " and dateadd(day,datediff(day,0,startdate),0)<='" & cisodate(mydate) & "' and dateadd(day,datediff(day,0,enddate),0)>='" & cisodate(mydate) & "'"
'response.Write sql
call write_html(sql)
call outputexcel(sql)

end if
end sub
sub searchpermits
if request("cmd2")="" then
%>
<table class=maintable2>
<form method=post action=admin.asp?cmd=searchpermits>
<input type=hidden name=cmd2 value="search" />
<tr><td>Site</td><td>


    <select name=site>
<% 


'if session("adminsite")=true then
	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites
'end if
for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
</td></tr>

<tr><td colspan=2 align=center><input type=submit name=submit value="Submit" class="button" /></td></tr>

</form>


</table>


<%

else
'response.Write "here"

site=request("site")
sql="select registration,valid_date,temp,expiry_date,type from apermits where site=" & tosql(site,"text") 
'response.Write sql
call write_html(sql)
call outputexcel(sql)

end if
end sub
sub extract087

Const ForReading = 1, ForWriting = 2, ForAppending = 3
Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

' Create a filesystem object
Dim FSO
set FSO = server.createObject("Scripting.FileSystemObject")


strRootFolder = "D:\Shared Folders and Data\secureweb\db\087plates"
 Set objFolder = FSO.GetFolder(strRootFolder)
Dim objFile 
k=0
For Each objFile in objFolder.Files

		sfile=objfile
		filename=objfile.name
	'	response.Write sfile & "<hr>"
		k=k+1
		

if FSO.FileExists(sfile) Then

    ' Get a handle to the file
    Dim file    
    set file = FSO.GetFile(sfile)

    ' Get some info about the file
   
    ' Open the file
    Dim TextStream
    Set TextStream = file.OpenAsTextStream(ForReading, TristateUseDefault)

    ' Read the file line by line
    Do While Not TextStream.AtEndOfStream
        Dim Line
        Line = TextStream.readline
    
        ' Do something with "Line"

    aline=split(line,",")
    sql="insert into plateimages(plate,site,date,lane,accuracy) values('" & aline(0) & "','087','" & cisodate(aline(1)) & " " & aline(2) & "','" & aline(3) & "','" & aline(4) & "')"
   ' response.Write sql &  "<br>"
objconn.execute sql

      
    Loop


  '  Response.Write "</pre><hr>"
response.Write filename & " done<br>"
 ' fso.deleteFile(sfile)

    Set TextStream = nothing
    


End If
response.Flush
next
response.Write k & " file processed"
Set FSO = nothing
end sub





sub analysispv

server.ScriptTimeout=100000000
mypost="http://traffica.cleartoneholdings.co.uk/traffica/addrivergate.asp"
'wscript.echo(mypost)

set xmlhttp = server.CreateObject("MSXML2.ServerXMLHTTP")
    xmlhttp.open "GET", mypost, false
   on error resume next
   xmlhttp.send
   on error goto 0
    set xmlhttp = nothing
Const ForReading = 1, ForWriting = 2, ForAppending = 3
Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

' Create a filesystem object
Dim FSO
set FSO = server.createObject("Scripting.FileSystemObject")


strRootFolder = "D:\Shared Folders and Data\secureweb\db\platestoanalpv"
 Set objFolder = FSO.GetFolder(strRootFolder)
Dim objFile 
k=0
For Each objFile in objFolder.Files

		sfile=objfile
		'response.Write sfile & "<hr>"
		k=k+1
		
	
		

if FSO.FileExists(sfile) Then
    
    ' Get a handle to the file
    Dim file    
    set file = FSO.GetFile(sfile)
    filename=fso.getfilename(sfile)
    'response.Write filename
    site=left(filename,3)
    ' Get some info about the file
   
    ' Open the file
    Dim TextStream
    Set TextStream = file.OpenAsTextStream(ForReading, TristateUseDefault)

    ' Read the file line by line
   ' response.write "reading file"
    myfile="<xml>"
    myfile= myfile &  vbcrlf & "<rootelement>"
    
    Do While Not TextStream.AtEndOfStream
        Dim Line
        Line = TextStream.readline
    
        ' Do something with "Line"
'response.Write "in loop"
    aline=split(line,",")
    
    myfile= myfile & vbcrlf &   "<plateimage>"
		                     myfile= myfile &  vbcrlf & "<plate>" & aline(0)& "</plate>"
		                    myfile= myfile &  vbcrlf & "<date>" &  cisodate(aline(1)) & " " & aline(2) & "</date>"
		                         myfile= myfile &  vbcrlf & "<site>" & site & "</site>"
	                        myfile= myfile &  vbcrlf & "</plateimage>"
    				       


      
    Loop
myfile=myfile & vbcrlf & "</rootelement>"
myfile= myfile &  vbcrlf & "</xml>"
sql="exec uspanaltopv @xml='" & myfile & "'"
openrs rs,sql
'plate,date,site,startdate,enddate,duration,pv
do while not rs.EOF
pv=rs("pv")
site=rs("site")
startdate=rs("startdate")
duration=rs("duration")
siteduration=rs("siteduration")
if checknoparktimes(site,startdate)=1 then
    pv=1
       ' response.Write "pv for no parking"
else
    if duration<siteduration then pv=0
      if checkfreetimes(site,startdate,duration)=1 then pv=0

end if
if pv=1 then
    mystatus=""
    sqlr="select * from violators where registration='" & rs("plate") & "' and datediff(day,pcnsent,'" & cIsoDate(rs("date")) & "')<14"
	'response.write sqlr
	set  rsr=objconn.execute(sqlr)')<14" 
			if not rsr.eof and not rsr.bof then
			mystatus="repeat"
			'response.write "<hr>" & rsunmatched("plate") & " is a repeat"
			end if
			rsr.close
			
	
	sqlr="select * from dvlarep where dvla='" & rs("plate") & "' and datediff(day,dategenerated,'" & cIsoDate(rs("date")) & "')<14"
	'response.write sqlr
	set  rsr=objconn.execute(sqlr)')<14" 
			if not rsr.eof and not rsr.bof then
			mystatus="repeat sent dvla"
			'response.write "<hr>" & rsunmatched("plate") & " is a repeat"
			end if
			rsr.close
			
					
			p=checkpermit(rs("plate"))
			if site="092" then p=false
	if p=true then 	 mystatus="100% Permit"
sql="insert into anPossibleViolators(plate,d,t_in,t_out,duration,site,status)values("  & tosql(rs("plate"),"text") & "," & tosql(cisodate(rs("date")) &" " & cisotime(rs("date")),"text") & "," & tosql(cisodate(rs("startdate")) & " " & cisotime(rs("startdate")),"text") & "," & tosql(cisodate(rs("enddate")) & " " & cisotime(rs("enddate")),"text")  & ",dateadd(minute, " & duration & ", 0)," & tosql(site,"text") & "," &  tosql(mystatus,"text") & ")"
objconn.execute sql
end if
'response.Write "<br>" & rs("plate") & rs("startdate") & "-" & rs("enddate") & "-duration:" & rs("duration") & ":pv:" & pv

rs.movenext
loop
closers rs
  '  Response.Write "</pre><hr>"
response.Write filename & " done<br>"
    Set TextStream = nothing
    
Else

    Response.Write "<h3><i><font color=red> File " & Filename &_
                       " does not exist</font></i></h3>"

End If
if err.number=0 then
'response.Write sfile & " need to be deleted"
Set fs=Server.CreateObject("Scripting.FileSystemObject") 
  fs.DeleteFile(sfile)
set fs=nothing
end if
'response.Flush
next

response.Write k & " file processed"
Set FSO = nothing



end sub
function checkfreetimes(site,startdate,duration)
sqlcft="select sitecode, convert(varchar, starttime, 108) as starttime,convert(varchar, endtime, 108) as endtime,day,duration from sitefreetimes where sitecode='" & site & "'"
'response.write sqlcft
openrs rscft,sqlcft
checkfreetimes=0
do while not rscft.eof
'response.Write rscft("endtime")
'response.Write "<font color=blue>"
'response.Write datediff("s",formatdatetime(startdate,3),rscft("endtime")) & "<br>"

'response.Write "</font>"
 if   datediff("s",formatdatetime(startdate,3),rscft("starttime"))<1 and  datediff("s",formatdatetime(startdate,3),rscft("endtime"))>0  then
    if  isnull(rscft("day")) and isnull(rscft("duration"))  then
           checkfreetimes=1
    else
        if not isnull(rscft("duration")) then
            if duration<rscft("duration") then 
                checkfreetimes=1
            ' else
              '  checkfreetimes=0
             end if
          end if
          if not isnull(rscft("day")) then
               ' response.Write "day:" & weekday(startdate)
                     if weekday(startdate)=rscft("day") then 
                    if not isnull(rscft("duration")) then
                      '  response.Write "<font color=blue>Duration:" & duration & ":rsduration:" & rscft("duration") & "</font><br>"
                    if duration<rscft("duration") then 
                    '    response.Write "set checkfreetimes to 1"
                        checkfreetimes=1
                     
                     end if
                  end if
             
             end if
          
                
            
       
        end if
    end if

 end if
rscft.movenext
loop

closers rscft
'response.Write "<hr>end of fucntion - check free times is :" & checkfreetimes
end function
function checknoparktimes(site,startdate)
sqlcft="select sitecode, convert(varchar, starttime, 108) as starttime,convert(varchar, endtime, 108) as endtime,day from sitenopark where sitecode='" & site & "'"
openrs rscft,sqlcft
checknoparktimes=0
do while not rscft.eof
if isnull(rscft("day")) then
    if   datediff("s",formatdatetime(startdate,3),rscft("starttime"))<1 and  datediff("s",formatdatetime(startdate,3),rscft("endtime"))>0  then
        checknoparktimes=1
    end if
else
   
        if   datediff("s",formatdatetime(startdate,3),rscft("starttime"))<1 and  datediff("s",formatdatetime(startdate,3),rscft("endtime"))>0 and  weekday(startdate)=rscft("day") then 
        checknoparktimes=1
    end if
    

end if
rscft.movenext
loop   

end function
function checkpermit(plate)
sqlp="select * from apermits where registration='" & plate & "'"
openrs rsp,sqlp
if rsp.eof and rsp.bof then
	 checkpermit=false
else
		checkpermit=true
		'response.write plate & " has a permit<br>"
end if	
closers rsp
end function
sub addccpayment
    showpayform=true
                if request("cmd2")="process" then
                
                %>
                <!--include file="includes.asp" -->
                <%    
                strConnectTo="LIVE" 
                strVSPVendorName="civil" '"civil" '** Set this value to the VSPVendorName assigned to you by protx or chosen when you applied **
strCurrency="GBP" '** Set this to indicate the currency in which you wish to trade. You will need a merchant number in this currency **
strTransactionType="PAYMENT" '** This can be DEFERRED or AUTHENTICATE if your Protx account supports those payment types **

strProtocol="2.22"
if strConnectTo="LIVE" then
  strAbortURL="https://ukvps.protx.com/vspgateway/service/abort.vsp"
  strAuthoriseURL="https://ukvps.protx.com/vspgateway/service/authorise.vsp"
  strCancelURL="https://ukvps.protx.com/vspgateway/service/cancel.vsp"
  strPurchaseURL="https://ukvps.protx.com/vspgateway/service/vspdirect-register.vsp"
  strRefundURL="https://ukvps.protx.com/vspgateway/service/refund.vsp"
  strReleaseURL="https://ukvps.protx.com/vspgateway/service/release.vsp"
  strRepeatURL="https://ukvps.protx.com/vspgateway/service/repeat.vsp"
  strVoidURL="https://ukvps.protx.com/vspgateway/service/void.vsp"
  strVSPDirect3DCallbackPage="https://ukvps.protx.com/vspgateway/service/direct3dcallback.vsp"
elseif strConnectTo="TEST" then
  strAbortURL="https://ukvpstest.protx.com/vspgateway/service/abort.vsp"
  strAuthoriseURL="https://ukvpstest.protx.com/vspgateway/service/authorise.vsp"
  strCancelURL="https://ukvpstest.protx.com/vspgateway/service/cancel.vsp"
  strPurchaseURL="https://ukvpstest.protx.com/vspgateway/service/vspdirect-register.vsp"
  strRefundURL="https://ukvpstest.protx.com/vspgateway/service/refund.vsp"
  strReleaseURL="https://ukvpstest.protx.com/vspgateway/service/abort.vsp"
  strRepeatURL="https://ukvpstest.protx.com/vspgateway/service/repeat.vsp"
  strVoidURL="https://ukvpstest.protx.com/vspgateway/service/void.vsp"
  strVSPDirect3DCallbackPage="https://ukvpstest.protx.com/vspgateway/service/direct3dcallback.vsp"
else
  strAbortURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorAbortTx"
  strAuthoriseURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorAuthoriseTx"
  strCancelURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorCancelTx"
  strPurchaseURL="https://ukvpstest.protx.com/VSPSimulator/VSPDirectGateway.asp"
  strRefundURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorRefundTx"
  strReleaseURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorReleaseTx"
  strRepeatURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorRepeatTx"
  strVoidURL="https://ukvpstest.protx.com/VSPSimulator/VSPServerGateway.asp?Service=VendorVoidTx"
  strVSPDirect3DCallbackPage="https://ukvpstest.protx.com/VSPSimulator/VSPDirectCallback.asp"
end if


                
                 strCardHolder=left(request.form("CardHolder"),100)
		                strCardType=cleaninput(request.form("CardType"),"Text")	
		                strCardNumber=cleaninput(request.form("CardNumber"),"Number")
		           '     response.Write "<br>cardnumber:" & strcardnumber
		              '   response.Write "<br>start  date:" &request.form("StartDate")
		                strStartDate=cleaninput(request.form("StartDate"),"Number")
		               ' response.Write "<br>start  date:" & strstartdate
		                strExpiryDate=cleaninput(request.form("ExpiryDate"),"Number")
		                strIssueNumber=cleaninput(request.form("IssueNumber"),"Number")
		                strCV2=cleaninput(request.form("CV2"),"Number")
                		'response.Write "cv2:"  & strcv2
		                '** Right then... check em **
		                if strCardHolder="" then
			                strPageError="You must enter the name of the Card Holder."
		                elseif strCardType="" then
			                strPageError="You must select the type of card being used."
		                elseif strCardNumber="" or not(IsNumeric(strCardNumber)) then
			                strPageError="You must enter the full card number."
		                elseif strStartDate<>"" and (len(strStartDate)<>4 or not(IsNumeric(strStartDate))) then
			                strPageError="If you provide a Start Date, it should be in MMYY format, e.g. 1206 for December 2006."
		                elseif strExpiryDate="" or len(strExpiryDate)<>4 or not(IsNumeric(strExpiryDate)) then
			                strPageError="You must provide an Expiry Date in MMYY format, e.g. 1209 for December 2009."
		                elseif (strIssueNumber<>"" and not(IsNumeric(strIssueNumber))) then
			                strPageError="If you provide an Issue number, it should be numeric."
		                elseif strCV2="" then 'or not(IsNumeric(strCV2)) then
			                strPageError="You must provide a Card Verification Value.  This is the last 3 digits on the signature strip of your card (or for American Express cards, the 4 digits printed to the right of the main card number on the front of the card.)"
		                else
			                '** All required field are present, so first store the order in the database then format the POST to VSP Direct **
			                '** First we need to generate a unique VendorTxCode for this transaction **
			                '** We're using VendorName, time stamp and a random element.  You can use different mehtods if you wish **
			                '** but the VendorTxCode MUST be unique for each transaction you send to VSP Server **
			                Randomize
			               ' strVendorTxCode=cleaninput(strVSPVendorName &	"-" & right(DatePart("yyyy",Now()),2) &_
							               ' right("00" & DatePart("m",Now()),2) & right("00" & DatePart("d",Now()),2) &_
							                'right("00" & DatePart("h",Now()),2) & right("00" & DatePart("n",Now()),2) &_
							                'right("00" & DatePart("s",Now()),2) & "-" & cstr(round(rnd*100000)),"VendorTxCode")
                                mypcn=request("pcn")
			                 strVendorTxCode=cleaninput(strVSPVendorName &	"_PCN" & mypcn ,"VendorTxCode") & "_" & cstr(round(rnd*100))
			                
			                sngTotal=request("amount")
			               
			               
			                '** Now create the VSP Direct POST **
                				
			                '** Now to build the VSP Server POST.  For more details see the VSP Server Protocol 2.22 **
			                '** NB: Fields potentially containing non ASCII characters are URLEncoded when included in the POST **
			                strPost="VPSProtocol=" & strProtocol
			                strPost=strPost & "&TxType=" & strTransactionType '** PAYMENT by default.  You can change this in the includes file **
			                strPost=strPost & "&Vendor=" & strVSPVendorName
			                strPost=strPost & "&VendorTxCode=" & strVendorTxCode '** As generated above **
			                strPost=strPost & "&Amount=" & FormatNumber(sngTotal,2,-1,0,0) '** Formatted to 2 decimal places with leading digit but no commas or currency symbols **
			                strPost=strPost & "&Currency=" & strCurrency
			                '** Up to 100 chars of free format description **
			                strPost=strPost & "&Description=" & URLEncode("Payment " & strVSPVendorName)
			                '** The Notification URL is the page to which VSP Server calls back when a transaction completes **
			                '** You can change this for each transaction, perhaps passing a session ID or state flag if you wish **
			                strPost=strPost & "&CardHolder=" & strCardHolder
			                strPost=strPost & "&CardNumber=" & strCardNumber
			                if len(strStartDate)>0 then
				                strPost=strPost & "&StartDate=" & strStartDate
			                end if
			                strPost=strPost & "&ExpiryDate=" & strExpiryDate
			                if len(strIssueNumber)>0 then
				                strPost=strPost & "&IssueNumber=" & strIssueNumber
			                end if
			                strPost=strPost & "&CV2=" & strCV2
			                strPost=strPost & "&CardType=" & strCardType
			                strPost=strPost & "&BillingAddress=" & URLEncode(strBillingAddress)
			                strPost=strPost & "&BillingPostCode=" & URLEncode(strBillingPostCode)
			                if bolDeliverySame then
				                strPost=strPost & "&DeliveryAddress=" & URLEncode(strBillingAddress)
				                strPost=strPost & "&DeliveryPostCode=" & URLEncode(strBillingPostCode)
			                else
				                strPost=strPost & "&DeliveryAddress=" & URLEncode(strDeliveryAddress)
				                strPost=strPost & "&DeliveryPostCode=" & URLEncode(strDeliveryPostCode)
			                end if
			                strPost=strPost & "&CustomerName=" & server.URLEncode(strCustomerName)
                     '** Allow fine control over AVS/CV2 checks and rules by changing this value. 0 is Default **
			                '** It can be changed dynamically, per transaction, if you wish.  See the VSP Direct Protocol document **
			                if strTransactionType<>"AUTHENTICATE" then strPost=strPost & "&ApplyAVSCV2=0"
                			
			                '** Send the IP address of the person entering the card details **
			                strPost=strPost & "&ClientIPAddress=" & Request.ServerVariables("REMOTE_HOST")

			                '** Allow fine control over 3D-Secure checks and rules by changing this value. 0 is Default **
			                '** It can be changed dynamically, per transaction, if you wish.  See the VSP Server Protocol document **
			                strPost=strPost & "&Apply3DSecure=0"
                			
			                '** Send the account type to be used for this transaction.  Web sites should us E for e-commerce **
			                '** If you are developing back-office applications for Mail Order/Telephone order, use M **
			                '** If your back office application is a subscription system with recurring transactions, use C **
			                '** Your Protx account MUST be set up for the account type you choose.  If in doubt, use E **
			                strPost=strPost & "&AccountType=E"

			                '** The full transaction registration POST has now been built **
			                '** Use the Windows WinHTTP object to POST the data directly from this server to Protx **
			                '** Data is posted to strPurchaseURL which is set depending on whether you are using SIMULATOR, TEST or LIVE **
			          '  response.Write strpost
			                set httpRequest = CreateObject("WinHttp.WinHttprequest.5.1")
                		'response.Write strpurchaseurl
			                on error resume next
			                httpRequest.Open "POST", CStr(strPurchaseURL), false
			                httpRequest.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
			                httpRequest.send strPost
			                strResponse = httpRequest.responseText
                			'response.Write strresponse & "<hr>"
			                if Err.number <> 0 then    
				                '** An non zero Err.number indicates an error of some kind **
				                '** Check for the most common error... unable to reach the purchase URL **  
				                if Err.number = -2147012889 then
					                strPageError="Your server was unable to register this transaction with Protx." &_
								                "  Check that you do not have a firewall restricting the POST and " &_
								                "that your server can correctly resolve the address " & strPurchaseURL
				                else
					                strPageError="An Error has occurred whilst trying to register this transaction.<BR>Please try again soon"'
					                 '&_ 
					                 
					             '    response.Write               "The Error Number is: " & Err.number & "<BR>" &_
								             '  "The Description given is: " & Err.Description
				                end If 
                			  
			                else
				                '** No transport level errors, so the message got the Protx **
				                '** Analyse the response from VSP Direct to check that everything is okay **
				                '** Registration results come back in the Status and StatusDetail fields **
				                strStatus=findField("Status",strResponse)
				                strStatusDetail=findField("StatusDetail",strResponse)
                		
				                if strStatus="3DAUTH" then
					                '** This is a 3D-Secure transaction, so we need to redirect the customer to their bank **
					                '** for authentication.  First get the pertinent information from the response **
					                strMD=findField("MD",strResponse)
					                strACSURL=findField("ACSURL",strResponse)
					                strPAReq=findField("PAReq",strResponse)
					                strPageState="3DRedirect"

				                else

					                '** If this isn't 3D-Auth, then this is an authorisation result (either successful or otherwise) **
					                '** Get the results form the POST if they are there **
					                strVPSTxId=findField("VPSTxId",strResponse)
					                strSecurityKey=findField("SecurityKey",strResponse)
					                strTxAuthNo=findField("TxAuthNo",strResponse)
					                strAVSCV2=findField("AVSCV2",strResponse)
					                strAddressResult=findField("AddressResult",strResponse)
					                strPostCodeResult=findField("PostCodeResult",strResponse)
					                strCV2Result=findField("CV2Result",strResponse)
					                str3DSecureStatus=findField("3DSecureStatus",strResponse)
					                strCAVV=findField("CAVV",strResponse)
                					
					                '** Great, the signatures DO match, so we can update the database and redirect the user appropriately **
					                if strStatus="OK" then
						                strDBStatus="AUTHORISED - The transaction was successfully authorised with the bank."
					                elseif strStatus="MALFORMED" then
						                strDBStatus="MALFORMED - The StatusDetail was:" & SQLSafe(left(strStatusDetail,255))
					                elseif strStatus="INVALID" then
						                strDBStatus="INVALID - The StatusDetail was:" & SQLSafe(left(strStatusDetail,255))
					                elseif strStatus="NOTAUTHED" then
						                strDBStatus="DECLINED - The transaction was not authorised by the bank."
					                elseif strStatus="REJECTED" then
						                strDBStatus="REJECTED - The transaction was failed by your 3D-Secure or AVS/CV2 rule-bases."
					                elseif strStatus="AUTHENTICATED" then
						                strDBStatus="AUTHENTICATED - The transaction was successfully 3D-Secure Authenticated and can now be Authorised."
					                elseif strStatus="REGISTERED" then
						                strDBStatus="REGISTERED - The transaction was could not be 3D-Secure Authenticated, but has been registered to be Authorised."
					                elseif strStatus="ERROR" then
						                strDBStatus="ERROR - There was an error during the payment process.  The error details are: " & SQLSafe(strStatusDetail)
					                else
						                strDBStatus="UNKNOWN - An unknown status was returned from Protx.  The Status was: " & SQLSafe(strStatus) &_
									                 ", with StatusDetail:" & SQLSafe(strStatusDetail)
					                end if					
                           mypcn=request("pcn")
                				
                				showpayform=false

					                '** Work out where to send the customer **
					                Session("VendorTxCode")=strVendorTxCode
					                if strStatus="OK" or strStatus="AUTHENTICATED" or strStatus="REGISTERED" then
						                'strCompletionURL="orderSuccessful.asp"
						              '  response.Write strstatus & " we need to add payment record here"	
						     
						  

sql="insert into payments(pcn,plate,amount,method,received,employee) values(" & tosql(mypcn,"text") & "," & tosql(getplate(mypcn),"text") & "," & tosql(request("amount"),"Number") & "," & "'Manual Credit Card',getdate(),"  & session("userid") & ")"
'response.Write sql
objconn.execute sql
lsSQL = "SELECT @@IDENTITY AS NewID"
Set loRs = objConn.Execute(lsSQL)
ID = loRs.Fields("NewID").value 
sql="insert into manualpayments(amount,protxcode,paymentid,pcn,errorcode) values(" & tosql(request("amount"),"Number") & "," & tosql(strVendorTxCode,"text") & "," & id & ",'" & mypcn & "','" & strdbstatus & "')"
'response.Write sql
objconn.execute sql

response.Write "Payment received Successfully"'	&strstatus

			 		              
						              
					                else
					          '   response.Write strstatus & "<hr>"   
					                sql="insert into manualpayments(amount,protxcode,errorcode,pcn) values(" & tosql(request("amount"),"Number") & "," & tosql(strVendorTxCode,"text") & ",'" & strStatus &  "','" & mypcn & "')"
					             '   response.Write sql
objconn.execute sql

						               ' strCompletionURL="orderFailed.asp"
						               ' strPageError=strDBStatus
						               if left(strStatus,8)="DECLINED" then
		                                    strReason="You payment was declined by the bank.  This could be due to insufficient funds, or incorrect card details."
	                                    elseif left(strStatus,9)="MALFORMED" or left(strStatus,7)="INVALID" then
		                                    strReason="The Protx Payment Gateway rejected some of the information provided without forwarding it to the bank." & _
			                                    " Please let us know about this error so we can determine the reason it was rejected. ."
	                                    elseif left(strStatus,8)="REJECTED" then
		                                    strReason="Your order did not meet our minimum fraud screening requirements." &_
			                                    " If you have questions about our fraud screening rules, or wish to contact us to discuss this.."
	                                    elseif left(strStatus,5)="ERROR" then
		                                    strReason="We could not process your order because our Payment Gateway service was experiencing difficulties." &_
			                                    " You can place the order over the telephone instead by calling [your number]."
	                                    else
		                                    strReason="The transaction process failed.  We please contact us with the date and time of your order and we will investigate."
	                                    end if
	                                    response.Write strreason
                                    showpayform=true
					                end if
          
                					
				                end if
                				
			                end if
                		
			                on error goto 0
			                set httpRequest=nothing
                		
                end if                
                                
                
                end if
               response.Write strpageerror
                if showpayform then
                mypcn=request("pcn")
                if mypcn="" then
                    response.Write "You must supply a PCN"
                    exit sub
                 end if
                'myerror=""
              
              
           
            
            %>
            <form method=post action=admin.asp?cmd=addccpayment>
 
            <input type=hidden name=cmd2 value=process />
            <input type=hidden name=pcn value="<%=mypcn %>" />
            
            <table border=0>
            <tr><td><b>PCN</b></td><td><%=mypcn %></td></tr>
                <tr><td><b>Amount Owed</b></td><td><%=getamountowed(mypcn) %></td></tr>
            <tr><td><b>Amount</b></td><td><input type=text name=amount /></td>
            
            </tr>
          
				<tr>
					  <td class="subheader" align="center" colspan="2"><b>Enter Card Details</b></td>
				</tr>
				<tr> 
					<td  align="right" class="greybar"><strong>Card Holder Name:</strong></td>
				  	<td  width="70%" class="TDSmall"><input name="CardHolder" type="text" value="<%response.write strCardHolder%>" size="30" maxlength="100"></td>
				</tr>
				<tr> 
					<td  align="right" class="greybar"><strong>Card Type:</strong></td>
				  	<td align="left"  class="TDSmall"><SELECT NAME="CardType">
				  		<option value="VISA"<%if strCardType="VISA" then response.write " SELECTED"%>>VISA Credit</option>
				  		<option value="DELTA"<%if strCardType="DELTA" then response.write " SELECTED"%>>VISA Debit</option>
				  		<option value="UKE"<%if strCardType="UKE" then response.write " SELECTED"%>>VISA Electron</option>
				  		<option value="MC"<%if strCardType="MC" then response.write " SELECTED"%>>MasterCard</option>
				  		<option value="MAESTRO"<%if strCardType="MAESTRO" then response.write " SELECTED"%>>Maestro</option>
				  		<option value="AMEX"<%if strCardType="AMEX" then response.write " SELECTED"%>>American Express</option>
				  		<option value="DC"<%if strCardType="DC" then response.write " SELECTED"%>>Diner's Club</option>
				  		<option value="JCB"<%if strCardType="JCB" then response.write " SELECTED"%>>JCB Card</option>
					</SELECT>
			  	    
					</td>
				</tr>
				<tr> 
					<td  align="right" class="greybar"><strong>Card Number:</strong></td>
			  	  <td align="left"  class="TDSmall"><input name="CardNumber" type="text" value="" size="25" maxlength="24">
				  	  <br /><font size="1">(With no spaces or separators)</font></td>
				</tr>
				<tr> 
					<td  align="right" class="greybar"><strong>Start Date:</strong></td>
			  	  <td align="left" width="70%" class="TDSmall"><input name="StartDate" type="text" value="<%response.write strStartDate%>" size="5" maxlength="4">
				    <br /><font size="1">(Where available. Use MMYY format  e.g. 0207)</font></td>
				</tr>
				<tr> 
					<td width="30%" align="right" class="greybar"><strong>Expiry Date:</strong></td>
			  	  <td align="left" class="TDSmall"><input name="ExpiryDate" type="text" value="<%response.write strExpiryDate%>" size="5" maxlength="4">
				    <br /><font size="1">(Use MMYY format with no / or - separators e.g. 1109)</font></td>
				</tr>
				<tr> 
					<td align="right" class="greybar"><strong>Issue Number:</strong></td>
			  	  <td align="left" class="TDSmall"><input name="IssueNumber" type="text" value="<%response.write strIssueNumber%>" size="3" maxlength="2">
				    <br /><font size="1">(Older Switch cards only. 1 or 2 digits 
					  as printed on the card)</font></td>
				</tr>
				<tr> 
					<td  align="right" class="greybar"><strong>Card Verification Value:</strong></td>
			  	  <td align="left"  class="TDSmall"><input name="CV2" type="text" value="" size="5" maxlength="4">
				   <br /><font size="1">(Additional 3 digits on card signature strip, 4 on Amex cards)</font></td>
				</tr>

			
            <tr><td align=center colspan=2><input type=submit name=submit value="Enter Payment" /></td></tr>
            </table>
            
            
            </form>
            
            <%
          
end if


end sub

public function URLDecode(strString)
	For lngPos = 1 To Len(strString)
    	If Mid(strString, lngPos, 1) = "%" Then
            strUrlDecode = strUrlDecode & Chr("&H" & Mid(strString, lngPos + 1, 2))
            lngPos = lngPos + 2
        elseif Mid(strString, lngPos, 1) = "+" Then
            strUrlDecode = strUrlDecode & " "
        Else
            strUrlDecode = strUrlDecode & Mid(strString, lngPos, 1)
        End If
    Next
    UrlDecode = strUrlDecode
End Function

'** There is a URLEncode function, but wrap it up so keep the code clean **
public function URLEncode(strString)
	strEncoded=Server.URLEncode(strString)
	URLEncode=strEncoded
end function

function cleanInput(strRawText,strType)

	if strType="Number" then
		strClean="0123456789."
		bolHighOrder=false
	elseif strType="VendorTxCode" then
		strClean="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_."
		bolHighOrder=false
	else
  		strClean=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,'/{}@():?-_&£$=%~<>*+""" & vbCRLF
		bolHighOrder=true
	end if

	strCleanedText=""
	iCharPos = 1

	do while iCharPos<=len(strRawText)
    	'** Only include valid characters **
		chrThisChar=mid(strRawText,iCharPos,1)

		if instr(StrClean,chrThisChar)<>0 then 
			strCleanedText=strCleanedText & chrThisChar
		elseif bolHighOrder then
			'** Fix to allow accented characters and most high order bit chars which are harmless **
			if asc(chrThisChar)>=191 then strCleanedText=strCleanedText & chrThisChar
		end if

      	iCharPos=iCharPos+1
	loop       
  
	cleanInput = trim(strCleanedText)

end function


function findField( strFieldName, strThisResponse )
	arrItems = split( strThisResponse, vbCRLF )
	for iItem = LBound( arrItems ) to UBound( arrItems )
		if InStr(arrItems(iItem), strFieldName & "=" ) = 1 then
			findField = mid(arrItems(iItem),len(strFieldName)+2)
			exit For
		end if
	next 
end function

sub siteissues

if request("cmd2")="" then
set PDF = server.createobject("aspPDF.EasyPDF")
PDF.Page "A4", 1 
 PDF.SetMargins 10, 5, 5,5
 sql="select siteissues.id,site,[Name of Site] as sitename,issue,startdate,startnotes,endnotes,notes, DATEDIFF(day,startdate, getdate()) as numdays from siteissues left join [site information] on siteissues.site=[site information].sitecode where enddate is null order by issue,site"
openrs rs,sql
if not rs.eof and not rs.bof then
   mytable= "<table border=1><tr><th>Site</th><th>Name of site</th><th>Start Date</th><th>Issue</th><th>Notes</th><th>Days</th><Th>tickets</th></tr>"
   response.Write  "<table class=sortable id=mytable><tr><th>Site</th><th width=20>Name of Site</th><th>Issue</th><th>Notes</th><th width=10># days<br> open</th><Th># tickets</th></tr>"
   i=0
    do while not rs.eof
      startnotes=rs("startnotes")
      
      if i=0 then
        mytable=mytable & "<tr>"
        i=1
      else
        mytable=mytable & "<tr bgcolor=#DDDDDD>"  
        i=0    
      end if
      response.write "<tr>"
      
       mynotes=showsiteissuenotes(rs("id"))
       myrow= "<td>" & rs("site") & "</td><td width=20>" & rs("sitename") & "</td><td>" & rs("issue")& "</td><td width=250>" & mynotes & "</td><td>" & rs("numdays") & "</td>"
        response.Write myrow
        mytable=mytable & myrow
       
        response.write"<Td><a href=admin.asp?cmd=siteissues&cmd2=find&site=" & rs("site") & ">" & getnumtickets(rs("site")) & "</a></td><Td><a href=admin.asp?cmd=siteissues&cmd2=fix&id=" & rs("id") & " class=button>Fix</a><br><br><a href=admin.asp?cmd=siteissues&cmd2=addnote&id=" & rs("id") & " class=button>Add Note</a></td></tr>" & vbcrlf
       mytable=mytable &  "<Td>" & getnumtickets(rs("site")) & "</td></tr>"
        
    rs.movenext
    loop

end if
closers rs
mytable=mytable & "</table>"
response.Write "</table>"
response.Write "<a href=admin.asp?cmd=siteissues&cmd2=add class=button>Add new issue</a><br><br><a href=admin.asp?cmd=siteissues&cmd2=search class=button>Search Closed Issues</a>"
pdfname=month(date) & "_" & day(date) & ".pdf"
   pdf.addhtml mytable
        PDF.Save configwebdir & "traffica\excelfiles\" & pdfname 
 'PDF.BinaryWrite
set pdf = nothing
response.Write "<br><br><a href=excelfiles\" & pdfname & " class=button>Click Here to print PDF</a>"
end if
if request("cmd2")="add" then
%>
<form method=post action=admin.asp?cmd=siteissues&cmd2=savenew>
<table class=maintable2>
<tr><td>Issue:</td><td><select name=issue>
<% 
sql="select issuename from issues order by issuename"
openrs rsi,sql
do while not rsi.eof
    response.Write "<option value='" & rsi("issuename") & "'>" & rsi("issuename") & "</option>"
rsi.movenext
loop
closers rsi
%></select>
</td></tr>
<tr>
<td>
Site:</td><td>  <select name=site>
<% 
if session("adminsite")=true then
sqlsites="select * from [site information]"
else
sqlsites="select * from usersites where userid=" & session("userid")
end if
openrs rssites,sqlsites  ' check the sites he can see 
do while not rssites.eof
		response.write "<option value='" & rssites("sitecode") & "'>" & rssites("sitecode") & "</option>"
	rssites.movenext
loop 
closers rssites %>

</select>
</td></tr>

<tr><td>Start Note:</td><td><textarea name=startnotes></textarea></td></tr>
<tr><td colspan=2 align=center><input type=submit name=submit value="Add" class="button" /></td></tr>
</table>

</form>



<%
end if
if request("cmd2")="savenew" then
    site=request("site")
   
    startnotes=request("startnotes")
    issue=request("issue")
   sql="insert into siteissues(site,issue,startdate,startnotes,useridstart) values(" & tosql(site,"text") & "," & tosql(issue,"text") & ",getdate()," & tosql(startnotes,"text") & "," & session("userid") & ")"
 sql="exec spinsertsiteissue  @site=" & tosql(site,"text") & ",@issue=" & tosql(issue,"text") & ",@startnotes=" & tosql(startnotes,"text") & ",@userid=" & session("userid") 
 ' response.Write sql
    objconn.execute sql
  
    Frome="FaultReportingSystem@creativecarpark.co.uk"
    Subject= site & "  New Fault - " & issue
    body=startnotes
     call sendfaultreportingemail(subject,body)

    

    
    
    
    jsalert("Issue Added")
    jsredirect("admin.asp?cmd=siteissues")
end if

if request("cmd2")="fix" then
    sql="select * from siteissues where id=" & request("id")
  
    openrs rs1,sql
    response.Write "<form method=post action=admin.asp?cmd=siteissues&cmd2=fix2><table class=maintable2><tr><td>Site</td><td>" & rs1("site") & "</td></tr>"
    response.Write "<input type=hidden name=id value=" & request("id") & "><input type=hidden name=issue value='" & rs1("issue") & "'>"
    response.Write "<tr><td>Issue</td><td><select name=issue>"
    sql="select issuename from issues order by issuename"
openrs rsi,sql
do while not rsi.eof
    response.Write "<option value='" & rsi("issuename") & "'"
        if rs1("issue")=rsi("issuename") then response.Write " selected"
    response.write   ">" & rsi("issuename") & "</option>"
rsi.movenext
loop
closers rsi
response.write "</select>"
    
     response.write "</td></tr>"
    response.Write "<tr><td>Start Date</td><td>" & rs1("startdate") & "</td></tr>"
    response.Write "<tr><td>Start Note</td><td>" & rs1("startnotes") & "</td></tr>"
    response.Write "<tr><td>End Note</td><td><textarea name=endnotes></textarea></td></tr>"
    response.write "<input type=hidden name=site value=" & rs1("site") & ">"
    response.Write "<tr><td colspan=2 align=center><input type=submit name=submit value=""Fix""></form>"
    closers rs1


end if
if request("cmd2")="fix2" then
    sql="update siteissues set enddate=getdate(),useridend=" & session("userid") & ",issue=" & tosql(request("issue"),"text") & ",endnotes=" & tosql(request("endnotes"),"text") & " where id=" & request("id")
   ' response.Write sql
    objconn.execute sql
    sql="insert into siteissuenotes(siteissueid,userid,note,type) values(" & request("id") & "," & session("userid") & "," & tosql(request("endnotes"),"text") & ",'end')"
   ' response.Write sql 
    objconn.execute sql
        Frome="FaultReportingSystem@creativecarpark.co.uk"
    
    Subject= request("site") & "  Fixed -" & request("issue")
    body=request("endnotes")
      call sendfaultreportingemail(subject,body)

    jsalert("Issue Closed")
    jsredirect("admin.asp?cmd=siteissues")
    
end if

if request("cmd2")="addnote" then
    sql="select * from siteissues where id=" & request("id")
  
    openrs rs1,sql
    response.Write "<form method=post action=admin.asp?cmd=siteissues&cmd2=savenote><table class=maintable2><tr><td>Site</td><td>" & rs1("site") & "</td></tr>"
    response.Write "<input type=hidden name=id value=" & request("id") & ">"
    response.Write "<tr><td>Issue</td><td><select name=issue>"
    sql="select issuename from issues order by issuename"
openrs rsi,sql
do while not rsi.eof
    response.Write "<option value='" & rsi("issuename") & "'"
        if trim(rs1("issue"))=trim(rsi("issuename")) then response.Write " selected"
    response.write  ">" & rsi("issuename") & "</option>"
rsi.movenext
loop
closers rsi
response.write "</select>"
    
     response.write "</td></tr>"
    response.Write "<tr><td>Start Date</td><td>" & rs1("startdate") & "</td></tr>"
    response.Write "<tr><td>Start Note</td><td>" & rs1("startnotes") & "</td></tr>"
    response.Write "<tr><td>Notes</td><td><textarea name=notes></textarea></td></tr>"
    response.Write "<tr><td colspan=2 align=center><input type=submit name=submit value=""Save""></form>"
    closers rs1


end if
if request("cmd2")="savenote" then
    sql="insert into siteissuenotes (siteissueid,userid,note) values(" & request("id") & "," & session("userid") & "," &tosql(request("notes"),"text") & ")"
    
    'sql="update siteissues set notedate=getdate(),useridnotes=" & session("userid") & ",notes=" & tosql(request("notes"),"text") & " where id=" & request("id")
   ' response.Write sql
    objconn.execute sql
   sql="update siteissues set issue=" & tosql(request("issue"),"text") & " where id=" & request("id")
    objconn.execute sql 
    jsalert("Issue Updated")
    jsredirect("admin.asp?cmd=siteissues")
    
end if
if request("cmd2")="search" then
%>
<form method=post action="admin.asp?cmd=siteissues&cmd2=find">
<table class=maintable2>

<tr>
<td>
Site:</td><td>  <select name="site">
<option value="">All</option>
<% 
if session("adminsite")=true then
sqlsites="select * from [site information]"
else
sqlsites="select * from usersites where userid=" & session("userid")
end if
openrs rssites,sqlsites  ' check the sites he can see 
do while not rssites.eof
		response.write "<option value=""" & rssites("sitecode") & """>" & rssites("sitecode") & "</option>"
	rssites.movenext
loop 
closers rssites %>

</select>
</td></tr>
<tr><td align=center colspan=2><input type=submit name=submit value="Search" class="button" /></td></tr>

</table>


</form>


<%
end if
if request("cmd2")="find" then
sql="select siteissues.id,site,[Name of Site] as sitename,issue,startdate,startnotes as startnotes,endnotes,notes,enddate  from siteissues left join [site information] on siteissues.site=[site information].sitecode"
if request("site")<>"" then sql=sql & " where site=" & request("site")
'response.Write sql
openrs rs,sql
mytable=""
if not rs.eof and not rs.bof then
    response.Write "<table class=sortable id=mytable><tr><th>Site</th><th width=15>Nameof Site</th><th>Issue</th><th>Notes</th><th width=10># tickets per site </th><td></td></tr>"
    do while not rs.eof
         startnotes= rs("startnotes")
        endnotes=rs("endnotes")
         mynotes=showsiteissuenotes(rs("id"))
       'response.Write "<hr>" & startnotes
        notes=rs("notes")
        response.Write "<tr><td>" & rs("site") & "</td><Td>" & rs("sitename") & "</td><td>" & rs("issue")& "</td><td width=300>" &mynotes & "</td><Td><a href=admin.asp?cmd=siteissues&cmd2=find&site=" & rs("site") & ">" & getnumtickets(rs("site")) & "</a></td><td><a href=admin.asp?cmd=siteissues&cmd2=addnote&id=" & rs("id") & " class=button>Add Note</a></td></tr>"
        
    rs.movenext
    loop
else
    response.Write "No records"
end if
closers rs

end if
end sub




sub addpermituser
if request("username")="" then
%>
<form name=myform method=post action=admin.asp?cmd=addpermituser>
<table class=maintable2>
<tr><th>Username:</th><td><input type=text name=username /></td></tr>
<tr><th>Password:</th><td><input type=text name=password /><input type=button value="Generate password" onClick="document.myform.password.value =getPassword();"></td></tr>
<tr><th>Company Name:</th><td><input type=text name=companyname /></td></tr>
<tr><th>Maximum Temporary Permits:</th><td><input type=text name=maxtemp /></td></tr>
<tr><th>Maximum Permanent Permits:</th><td><input type=text name=maxperm /></td></tr>
<tr><th>Site:</th><td>

<select name=site>
<% 


	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites

for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	
next 
 %>

</select>
</td>
</tr>
<tr><td colspan=2 align=center><input type=submit name=submit value="Add User" class="button" /></td></tr>
</table>

</form>
<script language="JavaScript" type="text/javascript">
 var frmvalidator = new Validator("myform");
 frmvalidator.addValidation("username","req","Please enter Username");
 frmvalidator.addValidation("password","req","Please enter Password");
 
  frmvalidator.addValidation("maxtemp","numeric");
 frmvalidator.addValidation("maxperm","numeric");
 
</script>

<br />
<a href=admin.asp?cmd=permitusers>Clcik Here to see current list of permit users</a>
<%

else
        Set objconnc=server.createObject("ADODB.Connection") 
	strconc="Driver={SQL Server};Server=10.10.1.11;Database=trafficaclients;UID=cleartone;PWD=cleartone568"
	objconnc.commandtimeout=90
objconnc.Open strConc
    if err.number<>0 then
        response.Write "This feature is temporarily unavailable"
        exit sub
    end if
    username=request("username")
    password=request("password")
    companyname=request("companyname")
    maxtemp=request("maxtemp")
    maxperm=request("maxperm")
    site=request("site")
    t="text"
		sql="SET NOCOUNT ON insert into clientusers (userid,[password],companyname,clienttypeid,useridadded) values(" & tosql(username,"text") & "," & tosql(password,"text") & "," & tosql(companyname,"text") & ",13," & tosql(session("userid"),"Number") & ");"&_
        "SELECT @@IDENTITY AS NewID;"
	'response.write sql
	set rs=objconn.execute (sql)

	id=rs("NewID")
	sql="insert into clientusers (id,userid,[password],companyname,clienttypeid,useridadded) values(" & id & "," & tosql(username,"text") & "," & tosql(password,"text") & "," & tosql(companyname,"text") & ",13," & tosql(session("userid"),"Number") & ");"
        
	objconnc.Execute sql
	'response.write id
	
	sql="insert into permitadmin(userid,maxperm,maxtemp) values(" & id & ","  & tosql(maxperm,"Number") & "," & tosql(maxtemp,"Number") & ")"
	objconn.execute sql
	objconnc.Execute sql
	sql="insert into clientusersites(userid,sitecode) values(" & id & "," & tosql(site,"text") & ")"
	objconn.execute sql
	objconnc.Execute sql
	jsalert("User added successfully")
	jsredirect("admin.asp?cmd=permitusers")
end if
end sub
sub permitusers
sql="select clientusers.Userid,Password,Companyname,maxperm,maxtemp,sitecode from clientusers left join clientusersites on clientusers.id=clientusersites.userid left join permitadmin on clientusers.id=permitadmin.userid where clienttypeid=13"
%>
<table class=sortable id=mytable>
<tr><th>Username</th><th>Password</th><th>Company Name</th><th>Max Temporary</th><th>Max Permanent</th><th>Site</th></tr>

<%
openrs rs,sql
do while not rs.eof 
response.Write "<tr><Td>" & rs("userid") & "</td><td>" & rs("password") & "</td><Td>" & rs("companyname") & "</tD><td>"& rs("maxtemp") & "</td><td>" & rs("maxperm") & "</td><td>" & rs("sitecode") & "</td></tr>"
rs.movenext
loop
closers rs
%></table><%
end sub




sub reportvsiteweek
if request("cmd2")="" then
%>
<table class=maintable2 width=500>
<tr>
<td>
<form method=post action=admin.asp?cmd=reportvsiteweek>
	 From </td><td><input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To </td><td><input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)

</td></tr>
<tr><td>
Date</td><td> <select name=datetype><option value="offencedate">of Offence</option>
<option value="pcnsent">Ticket Issued</option></select></td></tr>
<input type=hidden name=cmd2 value="show">
<tr><td colspan=2 align=center>
<input type=submit name=submit value="Submit" class=button>
</form>
</td>
</tr>
</table>
<%
else
    datetype=request("datetype")
    if datetype="offencedate" then
	    sp="spSiteWeekViolatorbyoffence"
    else
		    sp="spSiteWeekViolatorbypcnsent"
    end if
    datefrom=request.form("datefrom")
    dateto=request.Form("dateto")
    datefrom=cisodate(datefrom)
    dateto=cisodate(dateto)
    sql="exec " & sp & " @startdate='" & datefrom & "',@enddate='" & dateto & "'"
    'response.Write sql
    openrs objrs,sql
    If Not objRS.EOF Then arrRS = objRS.GetRows() ' 2 calls here
    Set objRS = Nothing

    If IsArray(arrRS) Then
mytable=""
       
        mytable=mytable & "<table class=maintable2border><tr><td><b>Week Of</b></td>"
    sweeks=""
     For i = LBound(arrRS, 2) To UBound(arrRS, 2)
           if i=0 then
                oldsite=arrRS(0, i)
            else
                 oldsite=site
            end if     
            site = arrRS(0, i)
            if oldsite<>site then exit for
            myweek = arrRS(1, i)
         
          mytable=mytable & "<td><b>" & myweek & "</b></td>"
            
        Next
    'response.Write "</tr>"
    
    
    arrweeks=split(sweeks,",")
    
    oldsite=""
    For i = LBound(arrRS, 2) To UBound(arrRS, 2)
          
            site = arrRS(0, i)
             myweek = arrRS(1, i)
           numviolators =arrrs(2,i)
           if site<>oldsite then 
            mytable=mytable & "</tr>" & vbcrlf & "<tr><td><b>" & site  & "<b></td>"
            j=0
           end if 
        
            mytable=mytable & "<td>" & numviolators & "</td>"
              
          oldsite=site
        Next 
    
    
   mytable=mytable & "</table>"
        Erase arrRS
    end If 
    response.Write mytable
    call writetoexcel(mytable)

end if
end sub
sub cb
if request("cmd2")="" then
%>

<form method=post action=admin.asp?cmd=cb>
<table class=maintable2>
<tr>
<td>
Sites:</td><td> <select name=site><option value="">All Sites</option>
<% 


if session("adminsite")=true then
	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites
end if
for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
</td></tr>

<tr><td>
From </td><td><input type=text name=datefrom id="dateform" value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To </td><td><input type=text name=dateto id="dateto" value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td colspan=2 align=center>
<input type="hidden" name="cmd2" value="search" />
<input type=submit name=submit value="Submit" class=button>
</td></tr></table>
</form>

<%
else
site=request("site")
datefrom=request("datefrom")
dateto=request("dateto")
response.Write "<h2>Cancellation Breakdown</h2>"
	mytable="<table class=maintable2><tr><td>Reasons</td><td>12 Months</td><td>6 Months</td><td>	3 Months</td><td> 	30 Days</td></tr>"
	mydate=date
   	sql="select reason,count(id) as count from cancelled where cancelled>DATEADD (mm ,-12, '" & cisodate(mydate) & "' ) "
          if site<>"" then sql=sql & " and right(pcn,3)=" &tosql(site,"text")
       if datefrom<>"" then sql=sql & " and cancelled>" & tosql(cisodate(datefrom),"text")
       if dateto<>"" then sql=sql & " and cancelled<=" & tosql(cisodate(dateto) & " 23:59","text")
       sql =sql & " group by reason order by reason"
     '  response.Write sql
    openrs rs,sql
    do while not rs.eof
        mytable=mytable & "<tr><td>" & rs("reason") & "</td><td>" 
        sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (mm ,-12, '" & cisodate(mydate) & "' )"
       if site<>"" then sql2=sql2 & " and right(pcn,3)=" &tosql(site,"text")
       if datefrom<>"" then sql2=sql2 & " and cancelled>" & tosql(cisodate(datefrom),"text")
       if dateto<>"" then sql2=sql2 & " and cancelled<=" & tosql(cisodate(dateto) & " 23:59","text")
      
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td><td>"
        closers rs2
        
        sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (mm ,-6, '" & cisodate(mydate) & "' )"
             if site<>"" then sql2=sql2 & " and right(pcn,3)=" &tosql(site,"text")
       if datefrom<>"" then sql2=sql2 & " and cancelled>" & tosql(cisodate(datefrom),"text")
       if dateto<>"" then sql2=sql2 & " and cancelled<=" & tosql(cisodate(dateto) & " 23:59","text")
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td><td>"
        closers rs2
        
         sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (mm ,-3, '" & cisodate(mydate) & "' )"
             if site<>"" then sql2=sql2 & " and right(pcn,3)=" &tosql(site,"text")
       if datefrom<>"" then sql2=sql2 & " and cancelled>" & tosql(cisodate(datefrom),"text")
       if dateto<>"" then sql2=sql2 & " and cancelled<=" & tosql(cisodate(dateto) & " 23:59","text")
      '  response.Write sql2
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td><td>"
        closers rs2
    
        sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (dd ,-30, '" & cisodate(mydate) & "' )"
              if site<>"" then sql2=sql2 & " and right(pcn,3)=" &tosql(site,"text")
       if datefrom<>"" then sql2=sql2 & " and cancelled>" & tosql(cisodate(datefrom),"text")
       if dateto<>"" then sql2=sql2 & " and cancelled<=" & tosql(cisodate(dateto) & " 23:59","text")
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td>"
        closers rs2
        mytable=mytable & "</tr>"
    rs.movenext
    loop
    mytable=mytable & "</table>"
    closers rs

    response.write mytable

end if 
end sub 
sub cancelledsummary
if request("cmd2")="" then
%>
<table class=maintable2 width=500>
<tr>
<td>
<form method=post action=admin.asp?cmd=cancelledsummary>
	 From </td><td><input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To </td><td><input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)

</td></tr>
<tr><td>
</td><td> <select name=datetype><option value="Date of Offence">Date of Offence</option>
<option value="Date Ticket Issued">Date Ticket Issued</option><option value="Date Cancelled">Cancelled Date</option></select></td></tr>
<input type=hidden name=cmd2 value="show">
<tr><td colspan=2 align=center>
<input type=submit name=submit value="Submit" class=button>
</form>
</td>
</tr>
</table>
<%
else
    datetype=request("datetype")
   
	 if datetype="Date of Offence" then datetypem="[date of violation]"  
    if datetype="Date Ticket Issued" then datetypem="[pcnsent]"
    if datetype="Date Cancelled" then datetypem="[cancelled]"
    
    datefrom=request.form("datefrom")
    dateto=request.Form("dateto")
    datefrom=cisodate(datefrom)
    dateto=cisodate(dateto)
    sql="exec spSummarycanclled @datetype='" & datetypem & "', @startdate='" & datefrom & "',@enddate='" & dateto & "'"
    
    openrs objrs,sql
    If Not objRS.EOF Then arrRS = objRS.GetRows() ' 2 calls here
    closers objrs
  
      issued=arrrs(0,0)
      paid=arrrs(1,0)
      outstanding=arrrs(2,0)
      cancelled=arrrs(3,0)
      mt="<table class=maintable2><tr><th colspan=2>Summary of Cancelled<th></tr><tr><td colspan=2>From " & request("datefrom") & " To "& request("dateto") & " by " & datetype & "</tr></tr>"
       mt=mt & "<tr><td>Overall Issued:</tD><tD>" & issued & "</td></tr>"
        mt=mt & "<tr><td>Overall Paid:</tD><tD>" & paid & "</td></tr>"
         mt=mt & "<tr><td>Overall Outstanding:</tD><tD>" & outstanding & "</td></tr>"
      mt=mt & "<tr><td>Overall Cancelled:</tD><tD>" & cancelled & "</td></tr>"
      mt=mt& "<tr><td colspan=2><b>Breakdown by Reason</b></td></tr>"
      sqlr="select count(cancelled),reason from cancelled left join violators on cancelled.pcn=violators.pcn+violators.site where " & datetypem  & ">='" & datefrom & "' and " & datetypem& "<='" & dateto & "' group by reason order by reason"
         ' response.Write sqlr
          openrs objrs,sqlr
    If Not objRS.EOF Then arrRS = objRS.GetRows() ' 2 calls here
    closers objrs
    For i = LBound(arrRS, 2) To UBound(arrRS, 2)
       count=arrrs(0,i)
       reason=arrrs(1,i)
       mt=mt & "<tr><td>" &  reason & "(" & chr(i+65) & ")</td><td>" & count & "</td></tr>"
       next
      
      mt=mt & "</table>"
      mt=mt & "<table class=maintable2border width=500><tr><th>Site</th>"
      For i = LBound(arrRS, 2) To UBound(arrRS, 2)
         reason=arrrs(1,i)
       mt=mt & "<th>" &  chr(i+65) & "</th>"
       next
       mt=mt & "</tr>"
      sqlsites="select sitecode from [site information] order by sitecode"
      openrs rssites,sqlsites
      If Not rssites.EOF Then arrsites = rssites.GetRows() 
      closers rssites
      
      For n = LBound( arrsites, 2) To UBound( arrsites, 2)
                mt=mt & "<tr><td>" &  arrsites(0,n) & "</td>"
                For i = LBound(arrRS, 2) To UBound(arrRS, 2)
                 reason=arrrs(1,i)
                  sqlc="select count(cancelled) as count from cancelled left join violators on cancelled.pcn=violators.pcn+violators.site where " & datetypem  & ">='" & datefrom & "' and " & datetypem& "<='" & dateto & "' and violators.site='" & arrsites(0,n) & "' and reason='" & reason & "'"
                  openrs rsc,sqlc
                   mt=mt & "<td>" & rsc("count") & "</td>"
                   closers rsc
               next
                mt=mt & "</tr>"
                
                
       next
      mt=mt & "</table>"
      response.write mt
       call writetoexcel(mt)

      
      
 end if
    

end sub

sub upload070
if request("dirname")="" then
%>
<form method=post action=admin.asp?cmd=upload070>
<table class=maintable2>
<tr><td>Put directory in T:\Upload 070 and then type the directory name and choose site here </td></tr>
<tr><td><input type=text name=dirname></td></tr>
<tr><td><select name=site><option value="070">070</option><option value="280">280</option></select></td></tr>
<tr><td><input type=submit name=submit value="Submit" /></td></tr>


</table>

</form>

<%

else
    dirname=request("dirname")
    site=request("site")
    'on error resume next
        Dim objFSO
         Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
        Dim objFolder 
        Dim strRootFolder
        stype=dirname
           strrootfolder="D:\Shared Folders and Data\secureweb\db\Upload 070\" & stype & "\" 
      '    response.Write strrootfolder 
   if objfso.folderexists(strrootfolder ) =true then 
   
        
         Set objFolder = objFSO.GetFolder(strRootFolder)
        Dim objFile 
        i=0
           itemimageurl="D:\Shared Folders and Data\wwwroot\traffica\MILTON KEYNES\" & stype & "\"
           checkdirectory itemimageurl
        For Each objFile in objFolder.Files
		        filename=objfile.name
		     '   response.write "<hr>" & filename
		    if filename<>"Thumbs.db" then 
		        lm=objfile.DateLastModified
        		
                 mynewfile=strrootfolder & filename
                'Set Upload = Server.CreateObject("ASPFileSaverTrial.FileSave")
                Set JPG = Server.CreateObject("ASPPhotoResizer.Resize")
                 MaxWidth = 600
                    MaxHeight = 450
                    JPG.LoadFromFile mynewfile

                    JPG.ResizeBox MaxWidth, MaxHeight
                    JPG.SaveToFile itemimageurl & filename
                   ' response.write filenamer & " resized<bR>"

                filenamer=replace(filename,".JPG","k.jpg")
                'Set Upload = Server.CreateObject("ASPFileSaverTrial.FileSave")
                Set JPG = Server.CreateObject("ASPPhotoResizer.Resize")
                 MaxWidth = 200
                    MaxHeight = 150
                    JPG.LoadFromFile mynewfile

                    JPG.ResizeBox MaxWidth, MaxHeight
                    JPG.SaveToFile itemimageurl & filenamer
                   ' response.write filenamer & " resized<bR>"
               
               
               filenamesm=replace(filename,".JPG","sm.jpg")
                'Set Upload = Server.CreateObject("ASPFileSaverTrial.FileSave")
                Set JPG = Server.CreateObject("ASPPhotoResizer.Resize")
                 MaxWidth = 200
                    MaxHeight = 150
                    JPG.LoadFromFile mynewfile

                    JPG.ResizeBox MaxWidth, MaxHeight
                    JPG.SaveToFile itemimageurl & filenamesm
               
               
               
                set fs=nothing
                 ' on error goto 0 
                'end if

              '  set fs=Server.CreateObject("Scripting.FileSystemObject")
               ' if fs.FileExists( strrootfolder & filename) then
               ' fs.CopyFile strrootfolder & filename,itemimageurl
               'fs.CopyFile strrootfolder & filenamer,itemimageurl
                'response.Write filenamer & " copied <br>"
               
                'end if
		        response.flush
        		
        		
	        sql="insert into mk(picture,date,type,site) values(" & tosql(filename,"text") & ",'" & cisodate(lm)  & " " & cisotime(lm) & "','" & stype & "','" & site & "')"
	        objconn.execute sql
	        end if
        Next
        response.write "done<br>link to enter  plates is <a target=_new href=http://traffica/mk.asp?type=" & server.URLEncode(stype) & ">http://traffica/mk.asp?type=" & stype & "</a>"
    else
    
        response.Write "directory name does not exist"
    end if

end if
end sub


sub extractpermits
%>
Please put all permit files in the directory T:\permitstoextract  and click below <br /><br />
<a href=extractpermits.asp class="button">Extract</a>

<%

end sub
sub registerflats
set objConnmp=server.createobject("ADODB.connection")
objConnmp.Open "dsn=markspermits;PWD=CTadmin20"
cmd2=request("cmd2")
suserfields="number,flat,building"
suserfieldscaptions="number,flat,building"
suserfieldstype="t,t,r(buildingtypes)"
auserfields=split(suserfields,",")
auserfieldscaptions=split(suserfieldscaptions,",")
auserfieldstype=split(suserfieldstype,",")
idfield="id"
tablename="mnumbers"
strdefaultsort="flat"
intpagesize=20
c="registerflats"
if cmd2="" then
    sql="select " & idfield 
    for i=lbound(auserfields) to ubound(auserfields)
        if auserfieldstype(i)<>"c" then       sql=sql &  "," & auserfields(i) 
    next
    sql=sql & " from " & tablename
    'response.Write sql
    strTableAttributes="class=maintable2"
    strpagename="admin.asp?cmd=" & c
	Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
			
    

	'response.write "strsort:" & strsort	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
    			
    		Set RS = server.CreateObject("adodb.recordset")
    			
    		With RS
    			.CursorLocation=3
    			.Open SQL & " order by " & replace(strSort,"desc"," desc"), objConnmp,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
             for i=lbound(auserfields) to ubound(auserfields)
               if auserfieldstype(i)<>"c" then  
                Response.Write "<Th align=center>" & vbcrlf
                fieldname=auserfields(i)
                fieldstrname=auserfieldscaptions(i)
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& fieldname & "&page=" & intCurrentPage & ">" & fieldstrname & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& fieldname &"desc&page=" & intCurrentPage & ">" & fieldstrname & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</TD>"	& vbcrlf
             end if   
            next
			
					
					
					
    		Response.Write "<td>&nbsp;</td></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			Response.Write "<TR>" & vbcrlf
    			For Each field In RS.Fields 'for each field in the recordset
			        if field.name<>idfield then
					    Response.Write "<TD align=center>" & vbcrlf
        				
					     Response.Write field.value
    					 	
    				    Response.Write "</TD>" & vbcrlf
    				end if    
				
    			Next
				
				response.write "<td><a href=admin.asp?cmd=" & c & "&cmd2=edit&id=" & rs("id") & " class=button>Edit</a></td>"
				
				'&nbsp;<a href=admin.asp?cmd=" & c & "&cmd2=delete&id=" & rs("id") & " class=button onclick=""return confirm('Are you sure you want to delete')"">Delete</a></td>"
				
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "</TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing
				
	response.Write "<br><br><a href=admin.asp?cmd=" & c & "&cmd2=add class=button>Add</a>"
end if		
if cmd2="add" then
    %>
    <form method=post action=admin.asp?cmd=<%=c %>&cmd2=save>
    <table class=maintable2>
   <%
   for i=lbound(auserfields) to ubound(auserfields)
       response.Write "<tr><td>" & auserfieldscaptions(i) & "</td><td>" & createeditinput(auserfields(i),auserfieldstype(i),"")  & "</td></tr>"
    next
   
    %>
    <tr><td colspan=2 align=center><input type=submit name=submit value="Save" class="button" /></td></tr>
    </table>
    
    </form>
    <%
end if
if cmd2="save" then

sql="insert into " & tablename & " (" 
for i=lbound(auserfields) to ubound(auserfields)
	    sql=sql & "[" & auserfields(i) & "],"
	    
	next
sql=left(sql,len(sql)-1)
sql=sql & ") values (" 
   for i=lbound(auserfields) to ubound(auserfields)
	    sql=sql & tosqla(request.form(auserfields(i)),auserfieldstype(i))
	    sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & ")"
	'response.write sql
	objconnmp.execute sql
	
	jsalert("Added Successfully")
    jsredirect("admin.asp?cmd=" & c & "")

end if

if cmd2="delete" then
sql="delete from " & tablename & " where " & idfield & "=" & request("id")
objconnmp.execute sql
jsalert "Deleted"
  jsredirect("admin.asp?cmd=" & c & "")

end if
if cmd2="edit" then
	response.write "<form method=post action=admin.asp?cmd=" & c & "><table class=maintable2>"
		response.write "<input type=hidden name=id value=" & request("id") & "><input type=hidden name=cmd2 value=saveedit>"
		sqle="select * from " & tablename & " where " & idfield & "=" & request("id")
		set rse=objconnmp.execute(sqle)
		'openrs rse,sqle
		for i= lbound(auserfields) to ubound(auserfields)
		response.write "<tr><td>" & auserfieldscaptions(i) & "</td><td>" & createeditinput(auserfields(i),auserfieldstype(i),rse(auserfields(i))) & "</td></tr>"
		next
		closers rse
		response.write "<tr><td colspan=2 align=center><input type=submit value=Save class=button></td></tr></table></form>"
	

end if 
if cmd2="saveedit" then
	sql="update " & tablename & " set "
	for i=lbound(auserfields) to ubound(auserfields)
	sql=sql & "[" & auserfields(i) & "]=" & tosqla(request.form(auserfields(i)),auserfieldstype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & " where " & idfield & "=" & request("id")
	'response.write sql
	objconnmp.execute sql
	jsalert("Edited Successfully")
	jsredirect("admin.asp?cmd=" & c & "")
end if


end sub 
sub pvlist
if request("cmd2")="" then 
%>
<form method="post" action="admin.asp?cmd=pvlist">
<input type="hidden" name="cmd2" value="show" />
<table class=maintable2>
<tr>
<td>
Site:</td><td> <select name=site>
<% 


	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites

for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
</td></tr>
<tr><td>

<tr><td>
Date </td><td><input type=text name=date value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td colspan=2 align=center><input type="submit" name="submit" value="Show Potential Violators" class="button"></td></tr>


</table>


</form>



<%
else
sql="exec usppossibleviolators @site=" & tosql(request("site"),"text") & ",@date=" & tosql(request("date"),"date") 
'response.write sql 
'openrs rs,sql 
'response.write "<table><tr><th>Plate</th><th>Start Time</th><th>End Time</th>
'closers rs 
call write_html(sql)
call outputexcel(sql)
	
end if
end sub 



sub pvlistfuzzy
if request("cmd2")="" then 
%>
<form method="post" action="admin.asp?cmd=pvlistfuzzy">
<input type="hidden" name="cmd2" value="show" />
<table class=maintable2>
<tr>
<td>
Site:</td><td> <select name=site>
<% 


	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites

for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
</td></tr>
<tr><td>

<tr><td>
Date </td><td><input type=text name=date value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td colspan=2 align=center><input type="submit" name="submit" value="Show Potential Violators" class="button"></td></tr>


</table>


</form>



<%
else

site=request("site")
sql="exec usppossibleviolators @site=" & tosql(site,"text") & ",@date=" & tosql(request("date"),"date")
'response.Write sql 
mytable="<table class=sortable id=mytable>"
openrs rs,sql
if rs.eof and rs.bof then mytable="No Records"
do while not rs.eof
    
  mytable=mytable &  "<tr><td colspan=9><b>" & rs("plate") & "</b></td></tr>"
    if rs("permit")="1" then
         mytable=mytable &  "<tr><td colspan=9>100% PERMIT</td></tr>"
    else
        sqlf="exec spgetfuzzypermits @plate=" & tosql(rs("plate"),"text") & ",@site=" & tosql(site,"text") & ",@date="& tosql(request("date"),"date")
        openrs rsf,sqlf
        if not rsf.eof and not rsf.bof then mytable=mytable & "<tr><td>Plate</td><td>Fuzzy %<td>Valid Date</td><td>Temp</td><td>Expiry Date</td><td>Date Added</td><td>type</td><td>Drivers Name</td><td>Auth By</td></tr>"
        
        do while not rsf.eof
           if (len(rsf("registration"))>1 or (site<>"071" and site<>"072")) then   mytable=mytable & "<tr><td>" & rsf("registration") & "</td><td>" & rsf("fuzzyaccuracy") & "%</td><td>" & rsf("valid_date") & "</td><tD>" & rsf("temp") & "</td><td>" & rsf("expiry_date") & "</td><tD>" & rsf("dateentered") & "</td><tD>" & rsf("type") & "</td><td>" & rsf("driversname") & "</td><td>" & rsf("authby") & "</td></tr>"
            
            
            
           
        rsf.movenext
        loop    
        closers rsf
    end if   
   
    if rs("exemption")="1" then
         mytable=mytable &  "<tr><td colspan=9>100% EXEMPTION</td></tr>"
    else
     
        sqlf="exec spgetfuzzyexemptions @plate=" & tosql(rs("plate"),"text") & ",@site=" & tosql(site,"text") & ",@date="& tosql(request("date"),"date")
        ' response.Write "herE"
      ' response.write sqlf
       ' openrs rsf,sqlf
       on error goto 0
       set rsf=objconn.execute(sqlf)
         'response.Write "herE2"
        if not rsf.eof and not rsf.bof then 
            mytable=mytable & "<tr><td>Plate</td><td>Fuzzy %<td>Start Date</td><td>End Date</td><td>Added From</td><td>Notes</td></tr>"
          
         do while not rsf.eof
            if (len(rsf("registration"))>1 or (site<>"071" and site<>"072")) then  mytable=mytable & "<tr><td>" & rsf("registration") & "</td><td>" & rsf("fuzzyaccuracy") & "%</td><td>" & rsf("startdate") & "</td><tD>" & rsf("enddate") & "</td><td>" & rsf("addedfrom") & "</td><tD>" & rsf("notes") & "</td></tr>"
            
            
            
           
        rsf.movenext
        loop    
        end if
        closers rsf
    end if   
rs.movenext
loop
closers rs
mytable=mytable & "</table>"
	response.write mytable 
	if mytable<>"No Records</table>" then call writetoexcel(mytable)
end if
end sub 

sub blackoutdays

    arrform=array("site","datefrom","dateto","day","reason")
arrformcaption=array("Site","Date From","Date To","Day","Reason")
arrformtype=array("s(vwsites)","d","d","s(days)","ta")
cmd="blackoutdays"
tablename="blackoutdays"
if request("cmd2")="add" then
	response.write "<form method=post name=myform action=admin.asp?cmd=" & cmd & "&cmd2=savenew><table class=maintable2>"
	for i= lbound(arrform) to ubound(arrform)
	response.write "<tr><td>" & arrformcaption(i) & "</td><td>" & createeditinput(arrform(i),arrformtype(i),"") & "</td></tr>"
	next
	response.write "<tr><td colspan=2 align=center><input type=submit value=Save></td></tr></table></form>"
	%>
	
	<script language="JavaScript" type="text/javascript">
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("myform");
    frmvalidator.addValidation("site","req","Please choose a site");
      frmvalidator.addValidation("reason","req","Please enter a reason");
  //   frmvalidator.addValidation("type","req","Please choose a  Type");
</script>
	<%
end if
if request("cmd2")="savenew" then
	sql="insert into " & tablename & " (useridadded," & join(arrform,",") & ") values (" &session("userid") & ","
	for i=lbound(arrform) to ubound(arrform)
	sql=sql & tosqla(request.form(arrform(i)),arrformtype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & ")"
	'response.write sql
	objconn.execute sql
	jsalert( "Added Successfully")
	jsredirect("admin.asp?cmd=" & cmd )

end if
if request("cmd2")="edit" then
	if request("id")="" then
		response.write "<form method=post action=admin.asp?cmd=" & cmd & "&cmd2=edit name=myform>"
		response.write "<table class=maintable2><tr><td>Choose Blackout Date to Edit</td></tr>"
		response.write "<tr><td><select name=id>"
		sql="select * from " & tablename & " order by site"
		openrs rs,sql
		do while not rs.eof
		response.write "<option value='" & rs("id") & "'>" & rs("site") & "</option>"
		rs.movenext
		loop
		closers rs
		response.write "</select><input type=submit name=submit value=Edit></td></tr></table></form>"
		%>
	
	<script language="JavaScript" type="text/javascript">
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("myform");
    frmvalidator.addValidation("site","req","Please choose a site");
      frmvalidator.addValidation("reason","req","Please enter a reason");
  //   frmvalidator.addValidation("type","req","Please choose a  Type");
</script>
	<%
	else
		
		
		response.write "<form method=post action=admin.asp?cmd=" & cmd & "><table class=maintable2>"
		response.write "<input type=hidden name=id value=" & request("id") & "><input type=hidden name=cmd2 value=saveedit>"
		sqle="select * from " & tablename & " where id=" & request("id")
		openrs rse,sqle
		for i= lbound(arrform) to ubound(arrform)
		response.write "<tr><td>" & arrformcaption(i) & "</td><td>" & createeditinput(arrform(i),arrformtype(i),rse(arrform(i))) & "</td></tr>"
		next
		closers rse
		response.write "<tr><td colspan=2 align=center><input type=submit value=Save></td></tr></table></form>"
	end if
	end if
	if request("cmd2")="saveedit" then
	sql="update " & tablename & " set "
	for i=lbound(arrform) to ubound(arrform)
	sql=sql & arrform(i) & "=" & tosqla(request.form(arrform(i)),arrformtype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & " where id=" & request("id")
	'response.write sql
	objconn.execute sql
	jsalert( "Edited Successfully")
		jsredirect("admin.asp?cmd=" & cmd )
end if
if request("cmd2")="delete" then
	if request("id")="" then
		response.write "<form method=post action=admin.asp?cmd=" & cmd & "&cmd2=delete onsubmit=""return confirm('Are you sure you want to delete?')"">"
		response.write "<table class=maintable2><tr><td>Choose Blackout date to Delete</td></tr>"
		response.write "<tr><td><select name=id>"
		sql="select * from " & tablename & " order by site"
		openrs rs,sql
		do while not rs.eof
		response.write "<option value='" & rs("id") & "'>" & rs("country") & "</option>"
		rs.movenext
		loop
		closers rs
		response.write "</select><input type=submit name=submit value=Delete></td></tr></table></form>"
	else
		sqld="exec deleteblackoutday @id=" & request("id")
		objconn.execute sqld
		jsalert( "Deleted Successfully")
			jsredirect("admin.asp?cmd=" & cmd )
	end if
	end if
	if request("cmd2")="viewdeleted" then 
        sql="select * from blackoutdaysdeleted"
        openrs rs,sql
            response.Write "<table id='mytable' class='sortable'><tr>"
            for i=lbound(arrform) to ubound(arrform)
	            response.Write "<th>" & arrformcaption(i) & "</th>"
	        next
        '    response.Write "<td>Actions</td></tr>"
        response.Write "</tr>"
            do while not rs.eof
                     response.Write "<tr>"
                 for i=lbound(arrform) to ubound(arrform)
                    if arrform(i)="day" then 
                         response.Write "<td>" & getweekdayname(rs(arrform(i))) & "</td>"
                    else
                         response.Write "<td>" & rs(arrform(i)) & "</td>"
                    end if          
	           
	            next
	            
            '    response.Write "<td><a href=admin.asp?cmd=" & cmd & "&id=" & rs("id") & "&cmd2=delete class=button>Delete</a></td></tr>"
               response.Write "</tr>"
            rs.movenext
            loop
        closers rs
    end if    
    if request("cmd2")="" then 
        sql="select * from " & tablename 
        openrs rs,sql
            response.Write "<table id='mytable' class='sortable'><tr>"
            for i=lbound(arrform) to ubound(arrform)
	            response.Write "<th>" & arrformcaption(i) & "</th>"
	        next
        '    response.Write "<td>Actions</td></tr>"
        response.Write "</tr>"
            do while not rs.eof
                     response.Write "<tr>"
                 for i=lbound(arrform) to ubound(arrform)
                    if arrform(i)="day" then 
                         response.Write "<td>" & getweekdayname(rs(arrform(i))) & "</td>"
                    else
                         response.Write "<td>" & rs(arrform(i)) & "</td>"
                    end if          
	           
	            next
	            
                response.Write "<td><a href=admin.asp?cmd=" & cmd & "&id=" & rs("id") & "&cmd2=delete class=button>Delete</a></td></tr>"
               response.Write "</tr>"
            rs.movenext
            loop
        closers rs
        response.Write "</table><a href=admin.asp?cmd=" & cmd & "&cmd2=add class=button>Add</a><br><br>"
         response.Write "</table><a href=admin.asp?cmd=" & cmd & "&cmd2=viewdeleted class=button>View Deleted Blackout Days</a>"
    end if
   

end sub 

sub searchpermitex
if request("cmd2")="" then 
%>
<h2>Search Registration</h2>
<form method=post action=admin.asp?cmd=searchpermitex>
<input type=hidden name=cmd2 value="find" />
Registration <input type="text" name="registration" />
<input type="submit" name="submit" class="button" value="search" />

</form>
<%
else
registration=request("registration")
sql="select * from apermits where registration like '%" & registration & "%'"
openrs rs,sql
if not rs.eof and not rs.bof then 
    response.Write "<h3>Permits</h3>"
    response.Write "<table class='sortable' id='mytable'><tr><th>Registration</th><th>Site</th></tr>"
       do while not rs.eof 

            response.Write "<tr><td>" & rs("registration") & "</td><td>" & rs("site") & "</td><td><a href=admin.asp?cmd=viewpermitdetails&id=" & rs("id") & " target=_new>View Details</a></td></tr>"

        rs.movenext
        loop        
    response.Write "</table>"
else
    response.Write "No Permits Matched<br>"    
end if 
closers rs

sql="select * from exemptions where registration like '%" & registration & "%'"
openrs rs,sql
if not rs.eof and not rs.bof then 
    response.Write "<h3>Exemptions</h3>"
    response.Write "<table class='sortable' id='mytable'><tr><th>Registration</th><th>Site</th></tr>"
       do while not rs.eof 

            response.Write "<tr><td>" & rs("registration") & "</td><td>" & rs("site") & "</td><td><a href=admin.asp?cmd=viewexemptiondetails&id=" & rs("id") & " target=_new>View Details</a></td></tr>"

        rs.movenext
        loop        
    response.Write "</table>"
else
    response.Write "No Exemptions Matched<br>"    
end if 
closers rs
end if
end sub

sub viewpermitdetails
id=request("id")
sql="select * from apermits where id=" & id
openrs rs,sql
response.Write "<table class=maintable2>"
myfields="Registration, Valid_Date, Temp, Expiry_Date, Site, Other_Info, FILE_PAGE,  dateentered, employee, type, profile, disabled, reason, vehiclecolour, driversname, vehicletype,  email, authby"
myfieldsc="Registration, Valid Date, Temp, Expiry Date, Site, Other Info, FILE PAGE,  Date Entered, Employee, Type, Profile, Disabled, Reason, Vehicle Colour, Drivers Name, Vehicle Type, Email, Authorized by"
afields=split(myfields,",")
afieldsc=split(myfieldsc,",")
for i=lbound(afields) to ubound(afields)
    if rs(trim(afields(i)))<>"" then 
       response.Write "<tr><th>" & afieldsc(i) & "</th><td>" & rs(trim(afields(i))) & "</td></tr>"
    end if
next
response.Write "</table>"
closers rs
end sub

sub viewexemptiondetails
id=request("id")
sql="select * from exemptions where id=" & id
openrs rs,sql
response.Write "<table class=maintable2>"
myfields="registration, datetime, dateadded, site, startdate, enddate, addedfrom, notes"
myfieldsc="Registration, Date Time, Date Added, Site, Start Date, End Date, Added From, Notes"
afields=split(myfields,",")
afieldsc=split(myfieldsc,",")
for i=lbound(afields) to ubound(afields)
    if rs(trim(afields(i)))<>"" then 
       response.Write "<tr><th>" & afieldsc(i) & "</th><td>" & rs(trim(afields(i))) & "</td></tr>"
    end if
next
response.Write "</table>"
closers rs
end sub
sub pptransactions
set objConnpp=Createobject("ADODB.Connection")
strconpp="Driver={SQL Server};Server=10.10.1.11;Database=phoneandpay;UID=phoneandpay;PWD=eshterph0ne34"
objconnpp.open strconpp
if request("cmd2")<>"search" then 
response.Write "<form method=post action=admin.asp?cmd=pptransactions><input type=hidden name=cmd2 value=search>"
response.Write "<table><tr><th>Site:</th><select name=site>"
sqls="select sitecode,sitedescription,trafficguardsite from site where addexemption=1"
set rss=objconnpp.execute(sqls)
do while not rss.eof
    response.Write "<option value=" & rss("sitecode") & ">" & rss("sitedescription") & "-" & rss("sitecode") & "(" & rss("trafficguardsite") & ")</option>"
    
rss.movenext
loop
rss.close 
set rss=nothing
response.Write "</select></td></tr>"
%>
<tr><th>
Date: </th><td><input type=text name=sdate value="<%=FormatDateTime(date(),2)%>" id=sdate>
<a href="javascript:NewCal('sdate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td colspan=2><input type=submit name=submit value="View" /></td></tr>
<%
else
site=request("site")
sdate=request("sdate")
sql="select dc,charge,vehicleplate from transcations where sitecode='" & site & "' and dc>='" & cisodate(sdate) & "' and dc<'" & cisodate(sdate) & " 23:59'"
set rs=objconnpp.Execute(sql)

     strLine="<table><tr>" 'Initialize the variable for storing the filednames

     For each x in rs.fields
             strLine= strLine & "<td>" & x.name & "</td>"
     Next
	 	strline=strline&"</tr>"
       mytable=mytable & strline

        Do while Not rs.EOF
         strLine="<tr>"
         for each x in rs.Fields
	  	    cell=x.value
		    strLine= strLine & "<td>" & cell & "</td>"
         next
	 	strline=strline & "</tr>"
		'response.write strline & vbcrlf
        mytable=mytable & strline
    
	 
	 err.clear
     rs.MoveNext
  Loop
	mytable=mytable & strline & "</table>"

    
rs.close
set rs=nothing
response.Write mytable 
call outputexceltable(mytable)

    
end if

end sub

sub viewunmatched
if request("cmd2")="" then 
%>
<form method="post" action="admin.asp?cmd=viewunmatched">
<input type="hidden" name="cmd2" value="show" />
<table class=maintable2>
<tr>
<td>
Site:</td><td> <select name=site>
<% 


	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites

for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
</td></tr>
<tr><td>

<tr><td>
Date </td><td><input type=text name=date value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td colspan=2 align=center><input type="submit" name="submit" value="View Unmatched Records" class="button"></td></tr>


</table>


</form>



<%
else
sql="select plate,d as dateofviolation,convert(varchar,t,108) as timeofviolation,site,notes,direction from anunmatched where d=" & tosql(request("date"),"date") & " and site=" & tosql(request("site"),"text")
'response.Write sql 
'openrs rs,sql

'closers rs
call write_html(sql)
call outputexcel(sql)
end if

end sub

sub subemails
if request("cmd2")="" then call showinbox
if request("cmd2")="viewemail" then 

response.Write "<a href=admin.asp?cmd=emails class=button>Back to Inbox</a><br><br>"
    id=request("id")
    
     sqlstatus="select * from emailstatus order by status"
openrs rsstatus,sqlstatus
mystatus=""
do while not rsstatus.eof
    mystatus=mystatus&rsstatus("status") & ","
    
rsstatus.movenext
loop
closers rsstatus

mystatus=left(mystatus,len(mystatus)-1)
astatus=split(mystatus,",")

    sqle="select e.id,sfrom,sto,sbody,ssubject,flagged,pcn,status,sdate,en.emailsig  from emails e left join emailnames en on en.emailname=e.emailname   where e.id=" & id 
    openrs rse,sqle
     subject=rse("ssubject")
      spcn=rse("pcn")
     emailsig=rse("emailsig")
    myfrom=rse("sfrom")
    if left(rse("sfrom"),1)="<" then 
       myfrom=replace(myfrom,"<","")
         myfrom=replace(myfrom,">","")
       
    end if  
    
    myto=rse("sto")
    if left(myto,1)="<" then 
        myto=replace(myto,"<","")
        myto=replace(myto,">","")
        
    end if   
   
   
    ' response.write spcn
     
    %>
    Message:<br />
  <iframe src="showmessage.asp?id=<%=id %>" width=500></iframe>
    <table class="maintable2">
    <tr><th>From:</th><td><%=myfrom %></td></tr>
    <tr><th>To:</th><td><%=myto %></td></tr>
    <tr><th>subject:</th><td><%=subject %></td></tr>
  <% if not isnull(rse("pcn")) then
  spcn=linkpcn(spcn)
  %>
  <tr><th>PCN</th><td><%= spcn %></td></tr>
  
    <%
    end if
    sqlatt="select * from attachment where messageid=" & id
    openrs rsatt,sqlatt
    if not rsatt.eof and not rsatt.bof then
    response.Write "<tr><th>Attachements</th><td>"
    do while not rsatt.eof 
        response.Write "<a href=""emailattachments/" & id & "_" &  rsatt("filename") & """ target=_new>" & rsatt("filename") & "</a><br>"
    rsatt.movenext
    loop
    response.Write " </td></tr>"
    end if
    closers rsatt
     %>
   <tr><th valign=top>Status</th><td><%=rse("status") %> <form method=post action=admin.asp?cmd=emails&cmd2=changestatus><select name="status">
<% 
for s=0 to ubound(astatus)
    response.Write "<option value='" & astatus(s) & "'"
    if status=astatus(s) then response.Write " selected='selected'"
    response.write ">" &astatus(s) & "</option>"
    
next
%>
</select>
<input type=hidden name=id value="<%=rse("id") %>" />
<input name="submit" value="Change Status" class="button" type="submit"></form></td></tr>




   <tr><td colspan=2><a href=# onclick="toggle_visibility('replybox');" class="button">Reply</a></a></td></tr>
   
    </table>
    <%  'check for past replies 
    sqler="select * from emailreplies where emailid=" & id
    openrs rser,sqler
    if not rser.eof and not rser.bof then 
    response.Write "<h2>Replies to this Email</h2><table class='sortable' id='mytable'><tr><th>Date Replied</th><th>User Replied</th><th>Subject</th><th width=250>Reply</th></tr>"
    do while not rser.eof
     reply= rser("reply")
    mysubject=rser("subject")
   
   
    response.Write "<tr><tD>" & rser("datereplied") & "</td><tD>" & getusername(rser("userid")) & "</td><td>" & mysubject & "</td><td>" &reply & "</td></tr>"
    
    rser.movenext
    loop
    response.Write "</table>"
    end if
    closers rser 
     %>
    <div id="replybox" style="display:none;">
    <form method=post action=emailreply.asp ENCTYPE="multipart/form-data">
    <table>
    <tr><td colspan=2>Message:<br />
    <textarea name=replymessage id="replymessage"><br /><br /><%=emailsig %></textarea></td></tr>
 <tr><td>Subject: </td><td><input type=text name=subject value="<%=rse("ssubject") %>" />  
   </td></tr>
   <tr><td>Attachment</td><td><input type=file name=attachment /></td></tr>
   <tr><td colspan=2 align=center>
    <input type=hidden name=emailid value="<%=id %>" />
    <input type=submit name=submit value="Send Reply" class="button" />
    </td></tr>
    </table>
    </form>
    </div>
<%
    closers rse
end if 
if request("cmd2")="reply" then
sfile=""


id=form("emailid")
sqle="select * from emails where id=" & id
openrs rse,sqle
toemail=getemail(rse("sfrom"))
fromname=pcase(rse("emailname"))
fromemail=getemail(rse("sto"))
subject=form("subject")
message=form("replymessage")
''need to send message here
'response.Write "here"
'response.Write "need to send message:" & message & " to " & toemail & " from:" & fromemail & " subject:" & subject
'call SendhtmlEmail2( FromEmail,fromname, ToEmail, Subject, message )
'response.Write "here"
sql="exec spreply @id=" & id & ",@userid=" & session("userid") & ",@reply=" & tosql(message,"text") & ",@subject=" & tosql(subject,"text")& ",@attachment=" & tosql(sfile,"text")
objconn.execute sql
response.Write "Message Sent<br><br><br>"
call showinbox


closers rse
end if
if request("cmd2")="delete" then 
sql="update emails set deleted=1,deleteddate=getdate(),deletedby=" & session("userid") & " where id=" & request("id")
objconn.execute sql
response.Write "Email Deleted Successfully<br><bR>"
call showinbox

end if 
if request("cmd2")="changestatus" then 
id=request("id")
status=request("status")
sql="update emails set status=" & tosql(status,"text") & ",statuschanged=getdate() where id=" & id
objconn.execute(sql)
response.Write "Status Updated"
call showinbox

end if
end sub 

function linkpcn(spcn)
sqlcpcn="select * from violators where pcn+site=" & tosql(spcn,"text")
openrs rscpcn,sqlcpcn
if not rscpcn.eof and not rscpcn.bof then
spcn="<a href=admin.asp?cmd=viewrecord&p=1&id=" & rscpcn("id") & " target=_new>" & spcn & "</a>"

end if


closers rscpcn
linkpcn=spcn
end function



sub extractpermits
%>
Please put all permit files in the directory \\172.16.1.6\permitstoextract  and click below <br /><br />
<a href=extractpermits.asp class="button">Extract</a>

<%

end sub
sub registerflats
set objConnmp=server.createobject("ADODB.connection")
objConnmp.Open "dsn=markspermits;PWD=CTadmin20"
cmd2=request("cmd2")
suserfields="number,flat,building"
suserfieldscaptions="number,flat,building"
suserfieldstype="t,t,r(buildingtypes)"
auserfields=split(suserfields,",")
auserfieldscaptions=split(suserfieldscaptions,",")
auserfieldstype=split(suserfieldstype,",")
idfield="id"
tablename="mnumbers"
strdefaultsort="flat"
intpagesize=20
c="registerflats"
if cmd2="" then
    sql="select " & idfield 
    for i=lbound(auserfields) to ubound(auserfields)
        if auserfieldstype(i)<>"c" then       sql=sql &  "," & auserfields(i) 
    next
    sql=sql & " from " & tablename
    'response.Write sql
    strTableAttributes="class=maintable2"
    strpagename="admin.asp?cmd=" & c
	Dim RS,strSort, intCurrentPage 
    		Dim strTemp, field, strMoveFirst, strMoveNext, strMovePrevious, strMoveLast
    		Dim i, intTotalPages, intCurrentRecord, intTotalRecords 
    		i = 0
    		
    		strSort = request("sort")
    		intCurrentPage = request("page")
    
    			
    		if strSort = "" Then
    			strSort = strDefaultSort
    		End if
			
    

	'response.write "strsort:" & strsort	
			if intCurrentPage = "" Then
    			intCurrentPage = 1
    		End if
    			
    		Set RS = server.CreateObject("adodb.recordset")
    			
    		With RS
    			.CursorLocation=3
    			.Open SQL & " order by " & replace(strSort,"desc"," desc"), objConnmp,3 '3 is adOpenStatic
    			.PageSize = cint(intPageSize)
    			intTotalPages = .PageCount							
    			intCurrentRecord = .AbsolutePosition 
    			.AbsolutePage = intCurrentPage
    			intTotalRecords = .RecordCount
    		End With
		
    		Response.Write "<TABLE " & strTableAttributes & " >" & vbcrlf
    		
    		'table head
    		Response.Write "<TR>" & vbcrlf
             for i=lbound(auserfields) to ubound(auserfields)
               if auserfieldstype(i)<>"c" then  
                Response.Write "<Th align=center>" & vbcrlf
                fieldname=auserfields(i)
                fieldstrname=auserfieldscaptions(i)
	 
				if instr(strSort, "desc") Then 'check the sort order, if its currently ascending, make the link descending
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& fieldname & "&page=" & intCurrentPage & ">" & fieldstrname & "</A>" & vbcrlf
    			Else
    				Response.Write "<A href=" & strPageName & "&s=1&sort="& fieldname &"desc&page=" & intCurrentPage & ">" & fieldstrname & "</A>"	& vbcrlf		
    			End if	
    			Response.Write "</TD>"	& vbcrlf
             end if   
            next
			
					
					
					
    		Response.Write "<td>&nbsp;</td></TR>"
    		
    		'records		
    		For i = intCurrentRecord To RS.PageSize 'display from the current record to the pagesize
    			if Not RS.eof Then 
    			Response.Write "<TR>" & vbcrlf
    			For Each field In RS.Fields 'for each field in the recordset
			        if field.name<>idfield then
					    Response.Write "<TD align=center>" & vbcrlf
        				
					     Response.Write field.value
    					 	
    				    Response.Write "</TD>" & vbcrlf
    				end if    
				
    			Next
				
				response.write "<td><a href=admin.asp?cmd=" & c & "&cmd2=edit&id=" & rs("id") & " class=button>Edit</a></td>"
				
				'&nbsp;<a href=admin.asp?cmd=" & c & "&cmd2=delete&id=" & rs("id") & " class=button onclick=""return confirm('Are you sure you want to delete')"">Delete</a></td>"
				
				Response.Write "</TR>" & vbcrlf
    			RS.MoveNext
    			End if
    		Next
    				
    		Response.Write "</TABLE>" & vbcrlf
    		
    		'page navigation	
    		Select Case cint(intCurrentPage) 
    			Case cint(intTotalPages) 'if its the last page give only links to movefirst and move previous
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = ""
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"				
    				strMoveLast = "" 
    			Case 1 'if its the first page only give links To move Next and move last
    				strMoveFirst = ""
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"				
    				strMovePrevious = "" 
    				strMoveLast = "<A href=" & strPageName & "&s=1&ort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"
    			Case Else 
    				strMoveFirst = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=1 >"& "First" &"</A>"
    				strMoveNext = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage + 1 & " >"& "Next" &"</A>"
    				strMovePrevious = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intCurrentPage - 1 & " >"& "Prev" &"</A>"		
    				strMoveLast = "<A href=" & strPageName & "&s=1&sort="& strSort &"&page=" & intTotalPages & " >"& "Last" &"</A>"					
    		End Select 
    		
    		With response		
    			.Write strMoveFirst & " "
    			.Write strMovePrevious 
    			.Write " " & intCurrentPage & " of " & intTotalPages & " "
    			.Write strMoveNext & " "
    			.Write strMoveLast
    		End With		
    		
    		if RS.State = &H00000001 Then 'its open		
    			RS.Close
    		End if
    		Set RS = nothing
				
	response.Write "<br><br><a href=admin.asp?cmd=" & c & "&cmd2=add class=button>Add</a>"
end if		
if cmd2="add" then
    %>
    <form method=post action=admin.asp?cmd=<%=c %>&cmd2=save>
    <table class=maintable2>
   <%
   for i=lbound(auserfields) to ubound(auserfields)
       response.Write "<tr><td>" & auserfieldscaptions(i) & "</td><td>" & createeditinput(auserfields(i),auserfieldstype(i),"")  & "</td></tr>"
    next
   
    %>
    <tr><td colspan=2 align=center><input type=submit name=submit value="Save" class="button" /></td></tr>
    </table>
    
    </form>
    <%
end if
if cmd2="save" then

sql="insert into " & tablename & " (" 
for i=lbound(auserfields) to ubound(auserfields)
	    sql=sql & "[" & auserfields(i) & "],"
	    
	next
sql=left(sql,len(sql)-1)
sql=sql & ") values (" 
   for i=lbound(auserfields) to ubound(auserfields)
	    sql=sql & tosqla(request.form(auserfields(i)),auserfieldstype(i))
	    sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & ")"
	'response.write sql
	objconnmp.execute sql
	
	jsalert("Added Successfully")
    jsredirect("admin.asp?cmd=" & c & "")

end if

if cmd2="delete" then
sql="delete from " & tablename & " where " & idfield & "=" & request("id")
objconnmp.execute sql
jsalert "Deleted"
  jsredirect("admin.asp?cmd=" & c & "")

end if
if cmd2="edit" then
	response.write "<form method=post action=admin.asp?cmd=" & c & "><table class=maintable2>"
		response.write "<input type=hidden name=id value=" & request("id") & "><input type=hidden name=cmd2 value=saveedit>"
		sqle="select * from " & tablename & " where " & idfield & "=" & request("id")
		set rse=objconnmp.execute(sqle)
		'openrs rse,sqle
		for i= lbound(auserfields) to ubound(auserfields)
		response.write "<tr><td>" & auserfieldscaptions(i) & "</td><td>" & createeditinput(auserfields(i),auserfieldstype(i),rse(auserfields(i))) & "</td></tr>"
		next
		closers rse
		response.write "<tr><td colspan=2 align=center><input type=submit value=Save class=button></td></tr></table></form>"
	

end if 
if cmd2="saveedit" then
	sql="update " & tablename & " set "
	for i=lbound(auserfields) to ubound(auserfields)
	sql=sql & "[" & auserfields(i) & "]=" & tosqla(request.form(auserfields(i)),auserfieldstype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & " where " & idfield & "=" & request("id")
	'response.write sql
	objconnmp.execute sql
	jsalert("Edited Successfully")
	jsredirect("admin.asp?cmd=" & c & "")
end if


end sub 
sub pvlist
if request("cmd2")="" then 
%>
<form method="post" action="admin.asp?cmd=pvlist">
<input type="hidden" name="cmd2" value="show" />
<table class=maintable2>
<tr>
<td>
Site:</td><td> <select name=site>
<% 


	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites

for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
</td></tr>
<tr><td>

<tr><td>
Date </td><td><input type=text name=date value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td colspan=2 align=center><input type="submit" name="submit" value="Show Potential Violators" class="button"></td></tr>


</table>


</form>



<%
else
sql="exec usppossibleviolators @site=" & tosql(request("site"),"text") & ",@date=" & tosql(request("date"),"date") 
'response.write sql 
'openrs rs,sql 
'response.write "<table><tr><th>Plate</th><th>Start Time</th><th>End Time</th>
'closers rs 
call write_html(sql)
call outputexcel(sql)
	
end if
end sub 



sub pvlistfuzzy
if request("cmd2")="" then 
%>
<form method="post" action="admin.asp?cmd=pvlistfuzzy">
<input type="hidden" name="cmd2" value="show" />
<table class=maintable2>
<tr>
<td>
Site:</td><td> <select name=site>
<% 


	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites

for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
</td></tr>
<tr><td>

<tr><td>
Date </td><td><input type=text name=date value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td colspan=2 align=center><input type="submit" name="submit" value="Show Potential Violators" class="button"></td></tr>


</table>


</form>



<%
else

site=request("site")
sql="exec usppossibleviolators @site=" & tosql(site,"text") & ",@date=" & tosql(request("date"),"date")
'response.Write sql 
mytable="<table class=sortable id=mytable>"
openrs rs,sql
if rs.eof and rs.bof then mytable="No Records"
do while not rs.eof
    
  mytable=mytable &  "<tr><td colspan=9><b>" & rs("plate") & "</b></td></tr>"
    if rs("permit")="1" then
         mytable=mytable &  "<tr><td colspan=9>100% PERMIT</td></tr>"
    else
        sqlf="exec spgetfuzzypermits @plate=" & tosql(rs("plate"),"text") & ",@site=" & tosql(site,"text") & ",@date="& tosql(request("date"),"date")
        openrs rsf,sqlf
        if not rsf.eof and not rsf.bof then mytable=mytable & "<tr><td>Plate</td><td>Fuzzy %<td>Valid Date</td><td>Temp</td><td>Expiry Date</td><td>Date Added</td><td>type</td><td>Drivers Name</td><td>Auth By</td></tr>"
        
        do while not rsf.eof
           if (len(rsf("registration"))>1 or (site<>"071" and site<>"072")) then   mytable=mytable & "<tr><td>" & rsf("registration") & "</td><td>" & rsf("fuzzyaccuracy") & "%</td><td>" & rsf("valid_date") & "</td><tD>" & rsf("temp") & "</td><td>" & rsf("expiry_date") & "</td><tD>" & rsf("dateentered") & "</td><tD>" & rsf("type") & "</td><td>" & rsf("driversname") & "</td><td>" & rsf("authby") & "</td></tr>"
            
            
            
           
        rsf.movenext
        loop    
        closers rsf
    end if   
   
    if rs("exemption")="1" then
         mytable=mytable &  "<tr><td colspan=9>100% EXEMPTION</td></tr>"
    else
     
        sqlf="exec spgetfuzzyexemptions @plate=" & tosql(rs("plate"),"text") & ",@site=" & tosql(site,"text") & ",@date="& tosql(request("date"),"date")
        ' response.Write "herE"
      ' response.write sqlf
       ' openrs rsf,sqlf
       on error goto 0
       set rsf=objconn.execute(sqlf)
         'response.Write "herE2"
        if not rsf.eof and not rsf.bof then 
            mytable=mytable & "<tr><td>Plate</td><td>Fuzzy %<td>Start Date</td><td>End Date</td><td>Added From</td><td>Notes</td></tr>"
          
         do while not rsf.eof
            if (len(rsf("registration"))>1 or (site<>"071" and site<>"072")) then  mytable=mytable & "<tr><td>" & rsf("registration") & "</td><td>" & rsf("fuzzyaccuracy") & "%</td><td>" & rsf("startdate") & "</td><tD>" & rsf("enddate") & "</td><td>" & rsf("addedfrom") & "</td><tD>" & rsf("notes") & "</td></tr>"
            
            
            
           
        rsf.movenext
        loop    
        end if
        closers rsf
    end if   
rs.movenext
loop
closers rs
mytable=mytable & "</table>"
	response.write mytable 
	if mytable<>"No Records</table>" then call writetoexcel(mytable)
end if
end sub 

sub blackoutdays

    arrform=array("site","datefrom","dateto","day","reason")
arrformcaption=array("Site","Date From","Date To","Day","Reason")
arrformtype=array("s(vwsites)","d","d","s(days)","ta")
cmd="blackoutdays"
tablename="blackoutdays"
if request("cmd2")="add" then
	response.write "<form method=post name=myform action=admin.asp?cmd=" & cmd & "&cmd2=savenew><table class=maintable2>"
	for i= lbound(arrform) to ubound(arrform)
	response.write "<tr><td>" & arrformcaption(i) & "</td><td>" & createeditinput(arrform(i),arrformtype(i),"") & "</td></tr>"
	next
	response.write "<tr><td colspan=2 align=center><input type=submit value=Save></td></tr></table></form>"
	%>
	
	<script language="JavaScript" type="text/javascript">
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("myform");
    frmvalidator.addValidation("site","req","Please choose a site");
      frmvalidator.addValidation("reason","req","Please enter a reason");
  //   frmvalidator.addValidation("type","req","Please choose a  Type");
</script>
	<%
end if
if request("cmd2")="savenew" then
	sql="insert into " & tablename & " (useridadded," & join(arrform,",") & ") values (" &session("userid") & ","
	for i=lbound(arrform) to ubound(arrform)
	sql=sql & tosqla(request.form(arrform(i)),arrformtype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & ")"
	'response.write sql
	objconn.execute sql
	jsalert( "Added Successfully")
	jsredirect("admin.asp?cmd=" & cmd )

end if
if request("cmd2")="edit" then
	if request("id")="" then
		response.write "<form method=post action=admin.asp?cmd=" & cmd & "&cmd2=edit name=myform>"
		response.write "<table class=maintable2><tr><td>Choose Blackout Date to Edit</td></tr>"
		response.write "<tr><td><select name=id>"
		sql="select * from " & tablename & " order by site"
		openrs rs,sql
		do while not rs.eof
		response.write "<option value='" & rs("id") & "'>" & rs("site") & "</option>"
		rs.movenext
		loop
		closers rs
		response.write "</select><input type=submit name=submit value=Edit></td></tr></table></form>"
		%>
	
	<script language="JavaScript" type="text/javascript">
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("myform");
    frmvalidator.addValidation("site","req","Please choose a site");
      frmvalidator.addValidation("reason","req","Please enter a reason");
  //   frmvalidator.addValidation("type","req","Please choose a  Type");
</script>
	<%
	else
		
		
		response.write "<form method=post action=admin.asp?cmd=" & cmd & "><table class=maintable2>"
		response.write "<input type=hidden name=id value=" & request("id") & "><input type=hidden name=cmd2 value=saveedit>"
		sqle="select * from " & tablename & " where id=" & request("id")
		openrs rse,sqle
		for i= lbound(arrform) to ubound(arrform)
		response.write "<tr><td>" & arrformcaption(i) & "</td><td>" & createeditinput(arrform(i),arrformtype(i),rse(arrform(i))) & "</td></tr>"
		next
		closers rse
		response.write "<tr><td colspan=2 align=center><input type=submit value=Save></td></tr></table></form>"
	end if
	end if
	if request("cmd2")="saveedit" then
	sql="update " & tablename & " set "
	for i=lbound(arrform) to ubound(arrform)
	sql=sql & arrform(i) & "=" & tosqla(request.form(arrform(i)),arrformtype(i))
	sql=sql & ","
	next
	sql=left(sql,len(sql)-1)
	sql=sql   & " where id=" & request("id")
	'response.write sql
	objconn.execute sql
	jsalert( "Edited Successfully")
		jsredirect("admin.asp?cmd=" & cmd )
end if
if request("cmd2")="delete" then
	if request("id")="" then
		response.write "<form method=post action=admin.asp?cmd=" & cmd & "&cmd2=delete onsubmit=""return confirm('Are you sure you want to delete?')"">"
		response.write "<table class=maintable2><tr><td>Choose Blackout date to Delete</td></tr>"
		response.write "<tr><td><select name=id>"
		sql="select * from " & tablename & " order by site"
		openrs rs,sql
		do while not rs.eof
		response.write "<option value='" & rs("id") & "'>" & rs("country") & "</option>"
		rs.movenext
		loop
		closers rs
		response.write "</select><input type=submit name=submit value=Delete></td></tr></table></form>"
	else
		sqld="exec deleteblackoutday @id=" & request("id")
		objconn.execute sqld
		jsalert( "Deleted Successfully")
			jsredirect("admin.asp?cmd=" & cmd )
	end if
	end if
	if request("cmd2")="viewdeleted" then 
        sql="select * from blackoutdaysdeleted"
        openrs rs,sql
            response.Write "<table id='mytable' class='sortable'><tr>"
            for i=lbound(arrform) to ubound(arrform)
	            response.Write "<th>" & arrformcaption(i) & "</th>"
	        next
        '    response.Write "<td>Actions</td></tr>"
        response.Write "</tr>"
            do while not rs.eof
                     response.Write "<tr>"
                 for i=lbound(arrform) to ubound(arrform)
                    if arrform(i)="day" then 
                         response.Write "<td>" & getweekdayname(rs(arrform(i))) & "</td>"
                    else
                         response.Write "<td>" & rs(arrform(i)) & "</td>"
                    end if          
	           
	            next
	            
            '    response.Write "<td><a href=admin.asp?cmd=" & cmd & "&id=" & rs("id") & "&cmd2=delete class=button>Delete</a></td></tr>"
               response.Write "</tr>"
            rs.movenext
            loop
        closers rs
    end if    
    if request("cmd2")="" then 
        sql="select * from " & tablename 
        openrs rs,sql
            response.Write "<table id='mytable' class='sortable'><tr>"
            for i=lbound(arrform) to ubound(arrform)
	            response.Write "<th>" & arrformcaption(i) & "</th>"
	        next
        '    response.Write "<td>Actions</td></tr>"
        response.Write "</tr>"
            do while not rs.eof
                     response.Write "<tr>"
                 for i=lbound(arrform) to ubound(arrform)
                    if arrform(i)="day" then 
                         response.Write "<td>" & getweekdayname(rs(arrform(i))) & "</td>"
                    else
                         response.Write "<td>" & rs(arrform(i)) & "</td>"
                    end if          
	           
	            next
	            
                response.Write "<td><a href=admin.asp?cmd=" & cmd & "&id=" & rs("id") & "&cmd2=delete class=button>Delete</a></td></tr>"
               response.Write "</tr>"
            rs.movenext
            loop
        closers rs
        response.Write "</table><a href=admin.asp?cmd=" & cmd & "&cmd2=add class=button>Add</a><br><br>"
         response.Write "</table><a href=admin.asp?cmd=" & cmd & "&cmd2=viewdeleted class=button>View Deleted Blackout Days</a>"
    end if
   

end sub 

sub searchpermitex
if request("cmd2")="" then 
%>
<h2>Search Registration</h2>
<form method=post action=admin.asp?cmd=searchpermitex>
<input type=hidden name=cmd2 value="find" />
Registration <input type="text" name="registration" />
<input type="submit" name="submit" class="button" value="search" />

</form>
<%
else
registration=request("registration")
sql="select * from apermits where registration like '%" & registration & "%'"
openrs rs,sql
if not rs.eof and not rs.bof then 
    response.Write "<h3>Permits</h3>"
    response.Write "<table class='sortable' id='mytable'><tr><th>Registration</th><th>Site</th></tr>"
       do while not rs.eof 

            response.Write "<tr><td>" & rs("registration") & "</td><td>" & rs("site") & "</td><td><a href=admin.asp?cmd=viewpermitdetails&id=" & rs("id") & " target=_new>View Details</a></td></tr>"

        rs.movenext
        loop        
    response.Write "</table>"
else
    response.Write "No Permits Matched<br>"    
end if 
closers rs

sql="select * from exemptions where registration like '%" & registration & "%'"
openrs rs,sql
if not rs.eof and not rs.bof then 
    response.Write "<h3>Exemptions</h3>"
    response.Write "<table class='sortable' id='mytable'><tr><th>Registration</th><th>Site</th></tr>"
       do while not rs.eof 

            response.Write "<tr><td>" & rs("registration") & "</td><td>" & rs("site") & "</td><td><a href=admin.asp?cmd=viewexemptiondetails&id=" & rs("id") & " target=_new>View Details</a></td></tr>"

        rs.movenext
        loop        
    response.Write "</table>"
else
    response.Write "No Exemptions Matched<br>"    
end if 
closers rs
end if
end sub

sub viewpermitdetails
id=request("id")
sql="select * from apermits where id=" & id
openrs rs,sql
response.Write "<table class=maintable2>"
myfields="Registration, Valid_Date, Temp, Expiry_Date, Site, Other_Info, FILE_PAGE,  dateentered, employee, type, profile, disabled, reason, vehiclecolour, driversname, vehicletype,  email, authby"
myfieldsc="Registration, Valid Date, Temp, Expiry Date, Site, Other Info, FILE PAGE,  Date Entered, Employee, Type, Profile, Disabled, Reason, Vehicle Colour, Drivers Name, Vehicle Type, Email, Authorized by"
afields=split(myfields,",")
afieldsc=split(myfieldsc,",")
for i=lbound(afields) to ubound(afields)
    if rs(trim(afields(i)))<>"" then 
       response.Write "<tr><th>" & afieldsc(i) & "</th><td>" & rs(trim(afields(i))) & "</td></tr>"
    end if
next
response.Write "</table>"
closers rs
end sub

sub viewexemptiondetails
id=request("id")
sql="select * from exemptions where id=" & id
openrs rs,sql
response.Write "<table class=maintable2>"
myfields="registration, datetime, dateadded, site, startdate, enddate, addedfrom, notes"
myfieldsc="Registration, Date Time, Date Added, Site, Start Date, End Date, Added From, Notes"
afields=split(myfields,",")
afieldsc=split(myfieldsc,",")
for i=lbound(afields) to ubound(afields)
    if rs(trim(afields(i)))<>"" then 
       response.Write "<tr><th>" & afieldsc(i) & "</th><td>" & rs(trim(afields(i))) & "</td></tr>"
    end if
next
response.Write "</table>"
closers rs
end sub
sub pptransactions
set objConnpp=Createobject("ADODB.Connection")
strconpp="Driver={SQL Server};Server=109.109.225.154;Database=phoneandpay;UID=ppuser;PWD=ykpui"
objconnpp.open strconpp
if request("cmd2")<>"search" then 
response.Write "<form method=post action=admin.asp?cmd=pptransactions><input type=hidden name=cmd2 value=search>"
response.Write "<table><tr><th>Site:</th><select name=site>"
sqls="select sitecode,sitedescription,trafficguardsite from site where addexemption=1"
set rss=objconnpp.execute(sqls)
do while not rss.eof
    response.Write "<option value=" & rss("sitecode") & ">" & rss("sitedescription") & "-" & rss("sitecode") & "(" & rss("trafficguardsite") & ")</option>"
    
rss.movenext
loop
rss.close 
set rss=nothing
response.Write "</select></td></tr>"
%>
<tr><th>
Date: </th><td><input type=text name=sdate value="<%=FormatDateTime(date(),2)%>" id=sdate>
<a href="javascript:NewCal('sdate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td colspan=2><input type=submit name=submit value="View" /></td></tr>
<%
else
site=request("site")
sdate=request("sdate")
sql="select dc,charge,vehicleplate from transcations where sitecode='" & site & "' and dc>='" & cisodate(sdate) & "' and dc<'" & cisodate(sdate) & " 23:59'"
set rs=objconnpp.Execute(sql)

     strLine="<table><tr>" 'Initialize the variable for storing the filednames

     For each x in rs.fields
             strLine= strLine & "<td>" & x.name & "</td>"
     Next
	 	strline=strline&"</tr>"
       mytable=mytable & strline

        Do while Not rs.EOF
         strLine="<tr>"
         for each x in rs.Fields
	  	    cell=x.value
		    strLine= strLine & "<td>" & cell & "</td>"
         next
	 	strline=strline & "</tr>"
		'response.write strline & vbcrlf
        mytable=mytable & strline
    
	 
	 err.clear
     rs.MoveNext
  Loop
	mytable=mytable & strline & "</table>"

    
rs.close
set rs=nothing
response.Write mytable 
call outputexceltable(mytable)

    
end if

end sub

sub viewunmatched
if request("cmd2")="" then 
%>
<form method="post" action="admin.asp?cmd=viewunmatched">
<input type="hidden" name="cmd2" value="show" />
<table class=maintable2>
<tr>
<td>
Site:</td><td> <select name=site>
<% 


	sqlsites="select sitecode from [site information]"
	openrs rssites,sqlsites  ' check the sites he can see 
	arrsites = rssites.GetRows()
	totalsites=ubound(arrsites, 2)
	closers rssites

for each site in arrsites
		response.write "<option value='" & site & "'>" & site & "</option>"
	'rssites.movenext
'loop
next 
'closers rssites %>

</select>
</td></tr>
<tr><td>

<tr><td>
Date </td><td><input type=text name=date value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td colspan=2 align=center><input type="submit" name="submit" value="View Unmatched Records" class="button"></td></tr>


</table>


</form>



<%
else
sql="select plate,d as dateofviolation,convert(varchar,t,108) as timeofviolation,site,notes,direction from anunmatched where d=" & tosql(request("date"),"date") & " and site=" & tosql(request("site"),"text")
'response.Write sql 
'openrs rs,sql

'closers rs
call write_html(sql)
call outputexcel(sql)
end if

end sub

sub subemails
if request("cmd2")="" then call showinbox
if request("cmd2")="viewemail" then 

response.Write "<a href=admin.asp?cmd=emails class=button>Back to Inbox</a><br><br>"
    id=request("id")
    
     sqlstatus="select * from emailstatus order by status"
openrs rsstatus,sqlstatus
mystatus=""
do while not rsstatus.eof
    mystatus=mystatus&rsstatus("status") & ","
    
rsstatus.movenext
loop
closers rsstatus

mystatus=left(mystatus,len(mystatus)-1)
astatus=split(mystatus,",")

    sqle="select e.id,sfrom,sto,sbody,ssubject,flagged,pcn,status,sdate,en.emailsig  from emails e left join emailnames en on en.emailname=e.emailname   where e.id=" & id 
   'response.Write sqle
    openrs rse,sqle
     subject=rse("ssubject")
      spcn=rse("pcn")
     emailsig=rse("emailsig")
    myfrom=rse("sfrom")
    if left(rse("sfrom"),1)="<" then 
       myfrom=replace(myfrom,"<","")
         myfrom=replace(myfrom,">","")
       
    end if  
    
    myto=rse("sto")
    if left(myto,1)="<" then 
        myto=replace(myto,"<","")
        myto=replace(myto,">","")
        
    end if   
   
   sdate=rse("sdate")
   sbody=rse("sbody")
    ' response.write spcn
     
    %>
    
    <table class="maintable2email">
    <tr><th>From:</th><td><%=myfrom %></td></tr>
    <tr><th> To:</th><td><%=myto %></td></tr>
    <tr><th >subject:</th><td><%=subject %></td></tr>
  <% if not isnull(rse("pcn")) then
  spcn=linkpcn(spcn)
  %>
  <tr><th>PCN</th><td><%= spcn %></td></tr>
  
    <%
    end if
    sqlatt="select * from attachment where messageid=" & id
    openrs rsatt,sqlatt
    if not rsatt.eof and not rsatt.bof then
    response.Write "<tr><th>Attachements</th><td>"
    do while not rsatt.eof 
        response.Write "<a href=""emailattachments/" & id & "_" &  rsatt("filename") & """ target=_new>" & rsatt("filename") & "</a><br>"
    rsatt.movenext
    loop
    response.Write " </td></tr>"
    end if
    closers rsatt
     %>
   <tr><th valign=top>Status</th><td><%=rse("status") %> <form method=post action=admin.asp?cmd=emails&cmd2=changestatus><select name="status">
<% 
for s=0 to ubound(astatus)
    response.Write "<option value='" & astatus(s) & "'"
    if status=astatus(s) then response.Write " selected='selected'"
    response.write ">" &astatus(s) & "</option>"
    
next
%>
</select>
<input type=hidden name=id value="<%=rse("id") %>" />
<input name="submit" value="Change Status" class="button" type="submit"></form></td></tr>




   
   
    </table>
         <div id="replybox" style="display:none;">
    <form method=post action=emailreply.asp ENCTYPE="multipart/form-data">
    <table width=800>
    <tr><td colspan=2>Message:<br />
    <textarea name=replymessage id="replymessage" width=800 style="width:800px"><br /><br /><%=emailsig %>
    
    </textarea></td></tr>
 <tr><td>Subject: </td><td><input type=text name=subject value="<%=rse("ssubject") %>" size=115 />  
   </td></tr>
   <tr><td>Attachment</td><td><input type=file name=attachment /></td></tr>
   <tr><td colspan=2 align=center>
    <input type=hidden name=emailid value="<%=id %>" />
    <input type=submit name=submit value="Send Reply" class="button" />
    </td></tr>
    </table>
    </form>
    </div>
  <div id="message">  Message:  &nbsp;&nbsp;&nbsp;<a href=# onclick="showreply()" class="button">Reply</a><br />
  <iframe src="showmessage.asp?id=<%=id %>" width=800></iframe>
    </div>
    <%  'check for past replies 
    sqler="select * from emailreplies where emailid=" & id
    openrs rser,sqler
    if not rser.eof and not rser.bof then 
    response.Write "<h2>Replies to this Email</h2><table class='sortable' id='mytable'><tr><th>Date Replied</th><th>User Replied</th><th>Subject</th><th width=250>Reply</th></tr>"
    do while not rser.eof
     reply= rser("reply")
    mysubject=rser("subject")
   
   
    response.Write "<tr><tD>" & rser("datereplied") & "</td><tD>" & getusername(rser("userid")) & "</td><td>" & mysubject & "</td><td>" &reply & "</td></tr>"
    
    rser.movenext
    loop
    response.Write "</table>"
    end if
    closers rser 
     %>
     

    
<%
    closers rse
end if 
if request("cmd2")="reply" then
sfile=""


id=form("emailid")
sqle="select * from emails where id=" & id
openrs rse,sqle
toemail=getemail(rse("sfrom"))
fromname=pcase(rse("emailname"))
fromemail=getemail(rse("sto"))
subject=form("subject")
message=form("replymessage")
''need to send message here
'response.Write "here"
'response.Write "need to send message:" & message & " to " & toemail & " from:" & fromemail & " subject:" & subject
'call SendhtmlEmail2( FromEmail,fromname, ToEmail, Subject, message )
'response.Write "here"
sql="exec spreply @id=" & id & ",@userid=" & session("userid") & ",@reply=" & tosql(message,"text") & ",@subject=" & tosql(subject,"text")& ",@attachment=" & tosql(sfile,"text")
objconn.execute sql
response.Write "Message Sent<br><br><br>"
call showinbox


closers rse
end if
if request("cmd2")="delete" then 
sql="update emails set deleted=1,deleteddate=getdate(),deletedby=" & session("userid") & " where id=" & request("id")
objconn.execute sql
response.Write "Email Deleted Successfully<br><bR>"
call showinbox

end if 
if request("cmd2")="changestatus" then 
id=request("id")
status=request("status")
sql="update emails set status=" & tosql(status,"text") & ",statuschanged=getdate() where id=" & id
objconn.execute(sql)
response.Write "Status Updated"
call showinbox

end if
end sub 

function linkpcn(spcn)
sqlcpcn="select * from violators where pcn+site=" & tosql(spcn,"text")
openrs rscpcn,sqlcpcn
if not rscpcn.eof and not rscpcn.bof then
spcn="<a href=admin.asp?cmd=viewrecord&p=1&id=" & rscpcn("id") & " target=_new>" & spcn & "</a>"

end if


closers rscpcn
linkpcn=spcn
end function
sub showinbox

%>
Filter by Status:<form method="post" action="admin.asp?cmd=emails"> <select id="status" name=status onChange="Loademailist('emaillist.asp?x=1')">
<%  sqlstatus="select * from emailstatus order by status"
openrs rsstatus,sqlstatus
mystatus=""
do while not rsstatus.eof
    mystatus=mystatus&rsstatus("status") & ","
    response.Write "<option value='" & rsstatus("status") & "'"
    if request("status")=rsstatus("status") then response.Write " selected='selected'"
    response.write ">" & rsstatus("status") & "</option>"
    
rsstatus.movenext
loop
closers rsstatus
%>
</select>
</form>
<%
mystatus=left(mystatus,len(mystatus)-1)
astatus=split(mystatus,",")


 %>
 <div id="resultemail">
<%call showemaillist
response.Write "</div>"
end sub 
sub declinedpaymentsreport
'The information should include date, time, PCN, telephone (if we have it) amount, reason.

if request("cmd2")="" then
%>
<form method=post action=admin.asp?cmd=declinedpaymentsreport><input type=hidden name=cmd2 value=view /><table>

<tr><td>
From </td><td><input type=text name=datefrom value="<%=right(0 & day(Date),2) &"/" &  right(0 & month(Date),2)&"/" & year(Date)%>"><a href="javascript:NewCal('datefrom','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To </td><td><input type=text name=dateto value="<%= right(0 & day(Date),2)&"/" & right(0 & month(Date),2)& "/" &  year(Date)%>"><a href="javascript:NewCal('dateto','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr><tr><td colspan=2><input type=checkbox name=outstanding value="yes" checked> Amount still outstanding
</td></tr>
<tr><td colspan=2 align=center>
<input type=submit name=submit value="Submit" class=button>
</td></tr></table>
</form>
<%
else
    sql="select mydate as date,pcn,phone,amount,reason from declinedpayments where mydate>=" & tosql(request("datefrom"),"date")  & " and mydate<="& tosql(request("dateto"),"date")
    if request("outstanding")="yes" then
  sql="select d.mydate,d.pcn,d.phone,d.amount,d.reason from declinedpayments d left join payments p on p.pcn=d.pcn where   mydate>=" & tosql(request("datefrom"),"date")  & " and mydate<="& tosql(request("dateto"),"date") & " and  (p.received<mydate or p.received is null)  "

    end if
sql=sql & " order by mydate"
'response.Write sql
call outputtable(sql)
call outputexcel(sql)
end if

end sub
sub addpermitpcn(spcn)
sql="exec addpermitpcn @pcn=" & tosql(spcn,"text")
objconn.execute sql 
response.Write "Permit Added"
end sub



sub failedanalysis

sqlma="select * from usermenu where userid=" & session("userid") & " and menu='analysisstage1'"
	openrs rsma,sqlma
	if  rsma.eof and rsma.bof then
			exit sub
	end if
	closers rsma
	

if request("cmd2")="" then
	%>
	<form method=post action=admin.asp?cmd=failedanalysis&cmd2=search>
	<table>
	<tr><td>
From: </td><td><input type=text name=fromdate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('fromdate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr><td>
To:</td><td> <input type=text name=todate value="<%=FormatDateTime(date(),2)%>">
<a href="javascript:NewCal('todate','ddmmyyyy')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date"></a><br>(dd/mm/yyyy)
</td></tr>
<tr>
<td>
Site:</td><td>  <select name="site">

<% 
if session("adminsite")=true then
sqlsites="select * from [site information]"
else
sqlsites="select * from usersites where userid=" & session("userid")
end if
openrs rssites,sqlsites  ' check the sites he can see 
do while not rssites.eof
		response.write "<option value=""" & rssites("sitecode") & """>" & rssites("sitecode") & "</option>"
	rssites.movenext
loop 
closers rssites %>

</select>
</td></tr>
	<tr><td colspan=2 align="center"><input type=submit value=submit name=submit /></td></tr>
	</table></form>
	
	<%
else
	 site=request("site")
	 fromdate=request("fromdate")
	 todate=request("todate")
  sql="select id,plate,d,convert(varchar,t_in,114) as t_in,convert(varchar,t_out,114) as t_out,convert(varchar,Duration,114) as duration,site,notes,direction from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and possibleviolator=0 and confirmedviolator=0 and site='" & site & "' and d>='" & cIsoDate(fromdate) & "' and d<='" & cisodate(todate) & " 23:59' order by direction,plate"
  openrs rs,sql 
  'response.write "<form method=post action=admin.asp?cmd=analysisstage1&cmd2=confirm>"
  response.write "<table border=1 class='sortable' id='mytable' ><tr><th>Direction</th><th>Plate</th><th>D</th><th>T_In</th><th>T_Out</th><th>Duration</td><td>Site</th></tr>"
 
  do while not rs.eof
  	 response.write "<tr><td>" & rs("Direction") & "</td><td>" & rs("Plate") & "</td><td>" & rs("D") & "</td><td>" & rs("T_In") & "</td><td>" & rs("T_Out") & "</td><td>" & rs("Duration") & "</td><td>" & rs("Site") & "</td></tr>"
 		 
  rs.movenext
  loop
	'response.write "<input type=hidden name=ids value='" & ids & "'><tr><td colspan=5><input type=submit name=submit value=Save></form>"
  response.write "</table>"
  closers rs
  sql="select plate,d as [date arrival],convert(varchar,t_in,114) as [arrival time],convert(varchar,t_out,114) as [departure time],convert(varchar,Duration,114) as duration,site,notes,direction from anPossibleViolators where ((status!='100% Permit' and status!='repeat')  or status is null) and possibleviolator=0 and confirmedviolator=0 and site='" & site & "' and d>='" & cIsoDate(fromdate) & "' and d<='" & cisodate(todate) & " 23:59' order by direction,plate"
  
    call outputexcel(sql)

end if

end sub
call myerror
 %>
 </div>
</body>

</html>




