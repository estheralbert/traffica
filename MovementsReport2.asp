﻿<% Server.ScriptTimeout = 3600000 %>
<% Response.CacheControl = "no-cache" %>
<% Response.Expires = -1 %>
<%
 pStr = "private, no-cache, must-revalidate" 
    Response.ExpiresAbsolute = #2000-01-01# 
    Response.AddHeader "pragma", "no-cache" 
    Response.AddHeader "cache-control", pStr 
%>

<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Movements Summary</title>
    <style type="text/css">
        body {
            font-family: Arial,sans-serif ;
            font-size: 12px;           
        }
        h1 {
            font-weight:bold;
            font-size:18px;
            text-decoration:underline;
        }
        table.summary {
	        border-width: 0px;
	        border-spacing: 0px;
	        border-style: solid;
	        border-color: gray;
	        border-collapse: collapse;
	        text-align: center;
        }
        table.summary th {
	        border-width: 1px;
	        padding: 7px;
	        border-style: solid;
	        border-color: gray;
        }
        table.summary td {
	        border-width: 0px;
	        padding: 7px;
	        border-style: solid;
	        border-color: gray;
        }
        tr  {
	        border-style: solid;
	        border-color: gray;
	        border-width: 0px 2px 0px 2px;
        }
        tr.totals td {
            font-weight: bold;            
	        border-style: solid;
	        border-color: gray;
            border-bottom-width: 5px;
            border-top-width: 1px;
            border-bottom-style: double;
        }
        tr.even {
            background-color:#d0d0d0;
        }
        tr.topborder
        {
	        border-style: solid;
	        border-width: 2px;
            border-bottom-width: 0px;
       }
        tr td.alert {
            border-color: firebrick;
            border-width: 1px;
            background-color: salmon;
        }
        tr.btwrow {
	        border-style: none;
	        border-width: 0px;
        }
        tr.btwrow td {
	        padding: 0px;
        }
        tr td.btwcol {
	        padding: 2px;
	        margin: 2px;
            background-color: gray;
	        border-style: solid;
	        border-width: 2px;
        }
    </style>
</head>
<body>
    <h1>Engineers Movement Report</h1>
    <table class='summary'>
    <%
        on error resume next
        dim arr1
        dim arr2
        dim arr3
        dim arr4
        dim arr5
        
        dim btwcol
        dim topborder
        dim shade
        dim weeknum
        dim alert
        dim tdsetup
        dim skipcolumns
        
        skipcolumns = 5
        
        btwcol = "<td class='btwcol'></td>"
        
        sql="exec MovementSummary"
        openrs rs1,sql
        arr1 = rs1.GetRows()
                
        set rs1 = rs1.NextRecordset()
        arr2 = rs1.GetRows()

        set rs1 = rs1.NextRecordset()
        arr3 = rs1.GetRows()

        set rs1 = rs1.NextRecordset()
        arr4 = rs1.GetRows()
        
        set rs1 = rs1.NextRecordset()
        arr5 = rs1.GetRows()
        
            colscount = UBound(arr1, 1)
                  
            fieldcount = rs1.Fields.Count 
            dim arrtot2()
            dim arrtot3()
            dim arrtot4()
            redim arrtot2(fieldcount-skipcolumns-1,0)
            redim arrtot3(fieldcount-skipcolumns-1,0)
            redim arrtot4(fieldcount-skipcolumns-1,0)
            
            Response.Write "<tr class='topborder'>"
                Response.Write "<th colspan='" & fieldcount-skipcolumns+2 & "'>Arrival Times</th>"
                Response.Write btwcol
                Response.Write "<th colspan='" & fieldcount-skipcolumns & "'>Sites Visited</th>"
                Response.Write btwcol
                Response.Write "<th colspan='" & fieldcount-skipcolumns & "'>Time Spent on Site (minutes)</th>"
                Response.Write btwcol
                Response.Write "<th colspan='" & fieldcount-skipcolumns & "'>Time Spent Between Sites (minutes)</th>"
            Response.Write "</tr>"

            Response.Write "<tr>"
                Response.Write "<th>Date</th><th>Day</th>"
                For i = skipcolumns To fieldcount - 1
                    Response.Write "<th>" & rs1.Fields(i).Name & "</th>"
                Next 
                
                Response.Write btwcol
                
                For i = skipcolumns To fieldcount - 1
                    Response.Write "<th>" & rs1.Fields(i).Name & "</th>"
                Next 
                
                Response.Write btwcol
                
                For i = skipcolumns To fieldcount - 1
                    Response.Write "<th>" & rs1.Fields(i).Name & "</th>"
                Next 
                
                Response.Write btwcol
                
                For i = skipcolumns To fieldcount - 1
                    if i = fieldcount - 1 then
                        Response.Write "<th class='rightcol'>" & rs1.Fields(i).Name & "</th>"
                    else
                        Response.Write "<th>" & rs1.Fields(i).Name & "</th>"
                    end if
                Next                 
            Response.Write "</tr>"
            
            weeknum = 1
            for irow = 0 to UBound(arr1, 2)                
                if (irow+1 - ((weeknum-1)*7)) mod 2 = 0 then
                    Response.Write "<tr class='even'>"
                else
                    if (irow+1) mod 7 = 1 then
                        Response.Write "<tr class='topborder odd'>"
                    else
                        Response.Write "<tr class='odd'>"
                    end if
                end if
                   
                    tdsetup = "<td class='alert'>"
                    Response.Write "<td>" & arr1(0, irow) & "</td>"
                    Response.Write "<td>" & arr1(2, irow) & "</td>"
                                        
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = tdsetup
                        else
                            tdsetupCur = replace(tdsetup,"alert","")
                        end if
                        Response.Write tdsetupCur & arr1(icol, irow) & "</td>"
                    Next
                    
                    Response.Write btwcol
                    
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = tdsetup
                        else
                            tdsetupCur = replace(tdsetup,"alert","")
                        end if
                        Response.Write tdsetupCur & arr2(icol, irow) & "</td>"
                        
                        'Keep a running total for the week
                        arrtot2(icol-skipcolumns,0) = CLng(arrtot2(icol-skipcolumns,0))+CLng(arr2(icol, irow))
                    Next
                    
                    Response.Write btwcol
                    
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = tdsetup
                        else
                            tdsetupCur = replace(tdsetup,"alert","")
                        end if
                        Response.Write tdsetupCur & Minutes2Hours(arr3(icol, irow)) & "</td>"
                        
                        'Keep a running total for the week
                        arrtot3(icol-skipcolumns,0) = CLng(arrtot3(icol-skipcolumns,0))+CLng(arr3(icol, irow))
                    Next
                    
                    Response.Write btwcol
                    
                    For icol = skipcolumns To fieldcount - 1
                        if Clng(arr5(icol, irow)) = 1 then 
                            tdsetupCur = tdsetup
                        else
                            tdsetupCur = replace(tdsetup,"alert","")
                        end if
                        Response.Write tdsetupCur & Minutes2Hours(arr4(icol, irow)) & "</td>"

                        'Keep a running total for the week
                        arrtot4(icol-skipcolumns,0) = CLng(arrtot4(icol-skipcolumns,0))+CLng(arr4(icol, irow))
                    Next
                Response.Write "</tr>"
                
                if (irow+1) mod 7 = 0 then
                    Response.Write "<tr class='totals'>"
                        Response.Write "<td colspan='2' style='text-align:left;'>Week Total</td>"
                        
                        For icol = 0 To UBound(arrtot2,1)
                            Response.Write "<td>&nbsp;</td>"
                        Next
                        Response.Write btwcol
                        For icol = 0 To UBound(arrtot2,1)
                            Response.Write "<td>" & arrtot2(icol, 0) & "</td>"
                            arrtot2(icol, 0) = 0
                        Next
                        Response.Write btwcol
                        For icol = 0 To UBound(arrtot3,1)
                            Response.Write "<td>" & Minutes2Hours(arrtot3(icol, 0)) & "</td>"
                            arrtot3(icol, 0) = 0
                        Next
                        Response.Write btwcol
                        For icol = 0 To UBound(arrtot4,1)
                            Response.Write "<td>" & Minutes2Hours(arrtot4(icol, 0)) & "</td>"
                            arrtot4(icol, 0) = 0
                        Next
                        weeknum = weeknum + 1
                    Response.Write "</tr>"
                    Response.Write "<tr class='btwrow'><td colspan='" & (fieldcount + ((fieldcount-skipcolumns)*3)) & "'>&nbsp;</td></tr>"
                end if
            Next
       
        closers rs1
     %>
    </table>
</body>
</html>
<%
function iif(psdStr, trueStr, falseStr)
  if psdStr then
    iif = trueStr
  else 
    iif = falseStr
  end if
end function

function Minutes2Hours(minutes)
    dim hours
    dim mins
    dim retval
    
    On Error Resume Next
    hours = minutes \ 60
    mins = minutes mod 60
    
    retval = CStr(hours) & ":" & Right("0" & CStr(mins),2)
    if len(hours)=0 then 
        retval = minutes
    else
        if len(retval) < 5 then retval = Right("00" & retval, 5)
    end if
    Minutes2Hours = retval
end function
%>