<%@ language=vbscript %>
<html>
<head>
<title>csDrawGraph Demonstration</title>
</head>
<body bgcolor="#FFFFFF">
<p>This simple demonstration shows two graphs using the same data. The first is a bar chart:</p>
<p><img src="tempchartdata.asp?Type=Bar" width="400" height="300"></p>
<p>The second is a pie chart. The background colour is set to light grey to show the overall size of the image.</p>
<p><img src="tempchartdata.asp?Type=Pie" width="400" height="300"></p>
</body>
</html>

