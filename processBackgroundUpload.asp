  <!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="admininclude.asp"-->
<!--#include file="image.asp"-->

<% Dim Form: Set Form = New ASPForm %><!--#INCLUDE FILE="_upload.asp"--><%     
Form.SizeLimit = &H200000
Server.ScriptTimeout = 2000
Dim FSO
set FSO = server.createObject("Scripting.FileSystemObject")
response.write "dbg: " & Form.State
If Form.State = 0 Then
    mypath="C:\inetpub\wwwroot\traffica\pdfbackgrounds\"
    For Each File In Form.Files.Items        
        Form.Files.Save  mypath
        sfile=file.filename
        response.write "uploaded: " & sfile
    next
    sbmt = Form("sbmt")
    if sfile&"" <> "" and right(sfile,4)<>".pdf" then 
        msg = "file must be a pdf." 
        sbmt = ""
    end if

    if sbmt="Update" and Form("id")&"" <> "" and isnumeric(Form("id")) then
        bgID = Form("id")
        backgroundname = Form("bgname")        
        if sfile<>"" then 
            newfile=replace(sfile,".pdf","_" & bgID & ".pdf")
            call renamefile(mypath & sfile,mypath & newfile)    
        end if

        sql = "UPDATE templateBackgrounds SET backgroundname = " & tosql(backgroundname,"Text")
        if newfile&"" <> "" then sql = sql & ", filename = " & tosql(newfile,"Text")
        sql = sql & " WHERE id = " & tosql(bgID,"Number")
    
        objconn.execute sql
        msg = "updated"
    end if
    if sbmt = "Delete" and Form("id")&"" <> "" and isnumeric(Form("id")) then
        bgID = Form("id")
        sql = "DELETE FROM templateBackgrounds WHERE id = " & tosql(bgID,"Number")
        objconn.execute sql
        msg = "deleted"
    end if
    if sbmt = "Add" then
        bgname = Form("bgname")        
        sql = "INSERT INTO templateBackgrounds (backgroundname, filename) VALUES (" & tosql(bgname,"Text") & "," & tosql(sfile,"Text") & ")"
        response.write sql
        objconn.execute sql
        if sfile<>"" then 
            lsSQL = "SELECT @@IDENTITY AS NewID"
            Set loRs = objconn.Execute(lsSQL)
            bgid= loRs.Fields("NewID").value
            newfile=replace(sfile,".pdf","_" & bgid & ".pdf")
            call renamefile(mypath & sfile,mypath & newfile)
            sql="update templateBackgrounds set filename=" & tosql(newfile,"text") & " where id=" & bgid
            response.write sql
            objconn.execute sql
        end if
        msg = "added"
    end if
end if
url = "editTemplates.asp?popupmsg="&msg
Response.Redirect url
%>

    