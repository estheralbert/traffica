<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<%
set PDF = server.createobject("aspPDF.EasyPDF")
PDF.Page "A4", 0
PDF.SetMargins 10, 5, 5,5	
mydate=date()-1



myhtml="<P align=center><B><FONT size=6>Daily Report</FONT></B></p> "
myhtml=myhtml & "<P align=center><B>" & day(mydate) & " " & monthname(month(mydate)) & " " & year(mydate) & "</B></p>"
myhtml=myhtml & "<P>Overview <br>Site Issues <br>Cancellation Summary  </p>"
myhtml=myhtml & "<P><B>Summary </B></p>"

sql="select count(id) as numtickets from violators where pcnsent>=" & tosql(cisodate(mydate),"text") & " and pcnsent<=" & tosql(cisodate(mydate)&" 23:59","text")

openrs rs,sql
numtickets=rs("numtickets")
closers rs
sql="select count(id) as numcancelled from cancelled where cancelled>=" & tosql(cisodate(mydate),"text") & " and cancelled<=" & tosql(cisodate(mydate)&" 23:59","text")
openrs rs,sql
numcancelled=rs("numcancelled")
closers rs
sql="select count(id) as numpaid from payments where received>=" & tosql(cisodate(mydate),"text") & " and received<=" & tosql(cisodate(mydate)&" 23:59","text")
openrs rs,sql
numpaid=rs("numpaid")
closers rs
myhtml=myhtml & "<P>Tickets Issued " &numtickets & "<br>" & "Tickets Cancelled " & numcancelled & "<br>Tickets Paid " & numpaid
myhtml=myhtml & "<!-PAGE BREAK>"
pdfname="dr" & month(date) & "_" & day(date) & ".pdf"
   pdf.addhtml myhtml
   pdf.AddHTML "<b>Overview</b><br>"
   pdf.AddHTML "http://traffica/trafficsummary2.asp?print=1"
   pdf.AddHTML "<!-PAGE BREAK><b>Site Issues</b><br>"
   sql="select id,site,issue,startdate,startnotes,endnotes,notes, DATEDIFF(day,startdate, getdate()) as numdays from siteissues where enddate is null order by site"
openrs rs,sql
if not rs.eof and not rs.bof then
   mytable= "<table border=1 width=520><tr><th width=10>Site</th><th width=80>Start Date</th><th width=30>Issue</th><th width=140>Start Note</th><th width=230>Note</th><th width=10>Days</th><Th width=10>tickets</th></tr>"
  
   i=0
    do while not rs.eof
      startnotes=rs("startnotes")
      
      'if i=0 then
        mytable=mytable & "<tr>"
       ' i=1
     ' else
      '  i=0
       ' mytable=mytable & "<tr bgcolor=#DDDDDD>"  
           
      'end if
           mynotes=showsiteissuenotes(rs("id"))  
       myrow= "<td>" & rs("site") & "</td><td>" & rs("startdate") & "</td><td>" & rs("issue")& "</td><td width=140>" & startnotes & "</td><td width=230>" & mynotes & "</td><td>" & rs("numdays") & "</td>"
          mytable=mytable & myrow
      
       mytable=mytable &  "<Td>" & getnumtickets(rs("site")) & "</td></tr>"
        
    rs.movenext
    loop

end if
closers rs
mytable=mytable & "</table>"
   pdf.AddHTML mytable
    pdf.AddHTML "<!-PAGE BREAK><b>Cancellations Summary</b><br>"
   	mytable="<table border=1><tr><td>Reasons</td><td>12 Months</td><td>6 Months</td><td>	3 Months</td><td> 	30 Days</td></tr>"
   	sql="select reason,count(id) as count from cancelled where cancelled>DATEADD (mm ,-12, '" & cisodate(mydate) & "' ) group by reason order by reason"
    openrs rs,sql
    do while not rs.eof
        mytable=mytable & "<tr><td>" & rs("reason") & "</td><td>" & rs("count") & "</td><td>" 
        sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (mm ,-12, '" & cisodate(mydate) & "' )"
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td><td>"
        closers rs2
        
        sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (mm ,-6, '" & cisodate(mydate) & "' )"
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td><td>"
        closers rs2
    
        sql2="select count(id) as mycount from cancelled where reason=" & tosql(rs("reason"),"text") & " and cancelled>DATEADD (dd ,-30, '" & cisodate(mydate) & "' )"
        openrs rs2,sql2
        mytable=mytable & rs2("mycount")& "</td>"
        closers rs2
        mytable=mytable & "</tr>"
    rs.movenext
    loop
    mytable=mytable & "</table>"
    closers rs
   pdf.AddHTML mytable
   
     pdf.AddHTML "<!-PAGE BREAK><b>PCN notes</b><br>"
   mytable="<table cellpadding=10><tr><td>date</td><td>pcn</td><td>pcnnote</td><td>user</td></tr>"
   sql="select mydate,pcn,pcnnote,useraccess.userid from pcnnotes left join useraccess on useraccess.id=pcnnotes.userid where mydate>=" & tosql(mydate,"date") & " and mydate<='" & cisodate(mydate) & " 23:59'"
  'response.write sql 
   openrs rs,sql
   do while not rs.eof
    mytable=mytable & "<tr><td>" & rs("mydate") & "</td><td>" & rs("pcn") & "</td><td width=200>" & rs("pcnnote") & "</td><td>" & rs("userid") & "</td></tr>"
  
   rs.movenext
   loop
   mytable=mytable & "</table>"
   pdf.AddHTML mytable
   closers rs
   
   pdf.AddHTML "<!-PAGE BREAK><b>Site Issues - Opened today</b><br>"
   sql="select id,site,issue,startdate,startnotes,endnotes,notes, DATEDIFF(day,startdate, getdate()) as numdays,endnotes from siteissues where startdate >=" &  tosql(mydate,"date") & " and startdate<='" & cisodate(mydate) & " 23:59' order by site"
  ' response.Write sql & "<hr>"
openrs rs,sql
if not rs.eof and not rs.bof then
   mytable= "<table border=1 width=520><tr><th width=10>Site</th><th width=80>Start Date</th><th width=30>Issue</th><th width=140>Start Note</th><th width=230>Note</th><th width=10>Days</th><Th width=10>tickets</th></tr>"
  
   i=0
    do while not rs.eof
      startnotes=rs("startnotes")
      
      'if i=0 then
        mytable=mytable & "<tr>"
       ' i=1
     ' else
      '  i=0
       ' mytable=mytable & "<tr bgcolor=#DDDDDD>"  
           
      'end if
           mynotes=showsiteissuenotes(rs("id"))  
       myrow= "<td>" & rs("site") & "</td><td>" & rs("startdate") & "</td><td>" & rs("issue")& "</td><td width=150>" & startnotes & "</td><td width=150>" & mynotes & "</td><td width=150>" & rs("endnotes") & "</td>"
          mytable=mytable & myrow & "</tr>"'rad
      
      ' mytable=mytable &  "<Td>" & getnumtickets(rs("site")) & "</td></tr>"
        
    rs.movenext
    loop
    mytable=mytable & "</table>"
else
    mytable="No issues started today"
end if
closers rs

   pdf.AddHTML mytable
   
   
    pdf.AddHTML "<!-PAGE BREAK><b>Site Issues - Fixed today</b><br>"
   sql="select id,site,issue,startdate,startnotes,endnotes,notes, DATEDIFF(day,startdate, getdate()) as numdays,endnotes from siteissues where enddate >=" &  tosql(mydate,"date") & " and enddate<='" & cisodate(mydate) & " 23:59' order by site"
  '   response.Write sql & "<hr>"
openrs rs,sql
if not rs.eof and not rs.bof then
     mytable= "<table border=1 width=520><tr><th width=10>Site</th><th width=80>Start Date</th><th width=30>Issue</th><th width=140>Start Note</th><th width=230>Note</th><th width=10>Days</th><Th width=10>tickets</th></tr>"
  
   i=0
    do while not rs.eof
      startnotes=rs("startnotes")
      
      'if i=0 then
        mytable=mytable & "<tr>"
       ' i=1
     ' else
      '  i=0
       ' mytable=mytable & "<tr bgcolor=#DDDDDD>"  
           
      'end if
           mynotes=showsiteissuenotes(rs("id"))  
       myrow= "<td>" & rs("site") & "</td><td>" & rs("startdate") & "</td><td>" & rs("issue")& "</td><td width=150>" & startnotes & "</td><td width=150>" & mynotes & "</td><td width=150>" & rs("endnotes") & "</td>"
          mytable=mytable & myrow & "</tr>"'rad
      
      ' mytable=mytable &  "<Td>" & getnumtickets(rs("site")) & "</td></tr>"
        
    rs.movenext
    loop
    mytable=mytable & "</table>"
else
    mytable="No issues fixed today"
end if
closers rs

   pdf.AddHTML mytable
   'random 3 letters
    pdf.AddHTML "<!-PAGE BREAK><b>Random 3 letters today</b><br>"
   sqlr="select top 3 type,id  from vwoutletters where date>='" & cisodate(mydate) & "' and date<='" & cisodate(mydate) & " 23:59' ORDER BY NEWID()"
    openrs rsr,sqlr
    do while not rsr.eof
        ltype=rsr("type")
        sqll="select letter,date,violatorid from " & ltype & "letters where id=" & rsr("id")
        'response.Write sqll
        
        
        openrs rsl,sqll
        letter=rsl("letter")
       ' response.Write letter
             pdf.AddHTML ltype & " letter- " & rsl("date")& "<br>" & letter & "<br><br>"
             pcn=getviolatorpcn(rsl("violatorid"))
             pdf.AddHTML "Last incoming letter for pcn" & pcn 
             '
             sqli="select filename,mydate from incomingletters where  pcn='" & pcn & "' and  mydate<'" & cisodate(rsl("date")) & "' order by mydate desc"
           ' response.Write sqli
             openrs rsi,sqli
             if not rsi.eof and not rsi.bof then
                 ifilename=rsi("filename")   
                 iyear=year(rsi("mydate"))
                 imonth=month(rsi("mydate"))
                ' response.Write "<hr>" & ifilename & "<hr>"
             else
                ifilename=""
              end if                
             closers rsi
             NPage = PDF.PageCount
        
        if ifilename<>"" then  PDF.AddPDF "D:\Shared Folders and Data\wwwroot\traffica\incomingletters\" & iyear & "\" & imonth & "\"& ifilename , 0,0, ""
'response.Write "D:\Shared Folders and Data\wwwroot\traffica\incomingletters\" & iyear & "\" & imonth & "\"& ifilename
           '  response.Write rsl("letter")
             pdf.AddHTML "<br><br><hr><br>"
        closers rsl
    rsr.movenext
    loop
    closers rsr
     PDF.Save configwebdir & "traffica\excelfiles\" & pdfname 
 'PDF.BinaryWrite
set pdf = nothing
response.Write "PDF created"
response.Write "<br><br><a href=excelfiles\" & pdfname & " class=button>Click Here to print PDF</a>"


function getnumtickets(site)
sqls="select count(id) as count from siteissues where site=" & tosql(site,"text")
openrs rss,sqls
getnumtickets=rss("count")
closers rss
end function
%>