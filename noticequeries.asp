﻿<!--#include file="common.asp"-->
<!--#include file="openconn.asp"-->
<!--#include file="admininclude.asp"-->

<!doctype html>
<html lang="en">
<head>

<meta http-equiv="Content-type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" href="img/favicon.ico">
<link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
<link href="css/table.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.datatables.js"></script>
<script type="text/javascript" src="js/jquery.jeditable.js"></script>
<script type="text/javascript" src="js/jquery.blockui.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $("#celebs");
        var oTable = table.dataTable({
            "sPaginationType": "full_numbers", "bStateSave": true, "iDisplayLength": 50, });

        $(".editable", oTable.fnGetNodes()).editable("updatenotice.asp?cmd=edit", {
            "callback": function (sValue, y) {
                var fetch = sValue.split(",");
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(fetch[1], aPos[0], aPos[1]);
            },
            "submitdata": function (value, settings) {
                return {
                    "row_id": this.parentNode.getAttribute("id"),
                    "column": oTable.fnGetPosition(this)[2]
                };
            },
            "height": "14px"
        });

        $(document).on("click", ".delete", function () {
            var celeb_id = $(this).attr("id").replace("delete-", "");
            var parent = $("#" + celeb_id);
            $.ajax({
                type: "get",
                url: "updatenotice.asp?cmd=delete&id=" + celeb_id,
                data: "",
                beforeSend: function () {
                    table.block({
                        message: "",
                        css: {
                            border: "none",
                            backgroundColor: "none"
                        },
                        overlayCSS: {
                            backgroundColor: "#fff",
                            opacity: "0.5",
                            cursor: "wait"
                        }
                    });
                },
                success: function (response) {
                    table.unblock();
                    var get = response.split(",");
                    if (get[0] == "success") {
                        $(parent).fadeOut(200, function () {
                            $(parent).remove();
                        });
                    }
                }
            });
        });
    });
</script>
</head>
<body>
	<div class="header">
		Click in field directly to edit  - press enter to save the change 
	</div>

	
        <%
		sql="select j.id,j.claim,j.pcn,date,filename,jfdfile,noticeacnfile,noticeoftransferfilename,noticeofreturnfilename from judgementnotice j left join violators v on v.pcn+v.site=j.pcn where v.pcn is null"
        openrs rs,sql

        %>
    
     <table class="table" id="celebs">
			<thead>
                <tr><th>Claim Number</th><th>PCN</th><th>Date</th><th>files</th><th></th></tr>

			</thead>
         <tbody>

             

               <% do while not rs.eof %>
				<tr id='<%=rs("id") %>'> <td class="editable"><%=rs("claim") %></td>          	<td class="editable"><%=rs("pcn") %> </td><td class="editable"><%=rs("date") %></td>       <td class="hidden">
                    
                   <% if rs("filename")<>"" then  %> 
                   <a href="#" onClick=window.open("noticepdf.asp?filename=<%=rs("filename") %>","","width=650,height=400,left=400,top=360,toolbar=1,status=1,"); ><%=rs("filename") %><a/> 

                       <% end if %>

                         <% if rs("jfdfile")<>"" then  %> 
                   <a href="#" onClick=window.open("noticepdf.asp?filename=<%=rs("jfdfile") %>","","width=650,height=400,left=400,top=360,toolbar=1,status=1,"); ><%=rs("jfdfile") %><a/> 

                       <% end if %>

                        <% if rs("noticeacnfile")<>"" then  %> 
                   <a href="#" onClick=window.open("noticepdf.asp?filename=<%=rs("noticeacnfile") %>","","width=650,height=400,left=400,top=360,toolbar=1,status=1,"); ><%=rs("noticeacnfile") %><a/> 

                       <% end if %>
                         <% if rs("noticeoftransferfilename")<>"" then  %> 
                   <a href="#" onClick=window.open("noticepdf.asp?filename=<%=rs("noticeoftransferfilename") %>","","width=650,height=400,left=400,top=360,toolbar=1,status=1,"); ><%=rs("noticeoftransferfilename") %><a/> 

                       <% end if %>


                          <% if rs("noticeofreturnfilename")<>"" then  %> 
                   <a href="#" onClick=window.open("noticepdf.asp?filename=<%=rs("noticeofreturnfilename") %>","","width=650,height=400,left=400,top=360,toolbar=1,status=1,"); ><%=rs("noticeofreturnfilename") %><a/> 

                       <% end if %>
                  </td><td><a href="javascript:;" id='delete-<%=rs("id") %>' class="delete no-underline">x</a></td></tr>
				
        <%
          
            rs.movenext
            loop
             closers rs

          %>

</tbody>
         </table>






       

	
</body>
</html>